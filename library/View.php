<?php

class View extends Bootstrap
{
    /**
     * Construct an application compatible URL from the given params.
     * Params are specified as an array with the following keys: module, controller, action.
     * Also, additional params can be specified which will be interpreted as query string components.
     *
     * @param array|null $params
     * @return string
     */
    public function url($params)
    {
        return parent::url($params);
    }

    /**
     * Return the page matching the given identifier.
     * To be used in views.
     *
     * @param string $identifier
     * @return mixed
     */
    public function page($identifier)
    {
        return parent::page($identifier);
    }

    /**
     * Return the exact full image path, based only on the image's name.
     *
     * @param string $source
     * @param string $thumb
     * @return string
     */
    public function image($source, $thumb = 'default')
    {
        return parent::image($source, $thumb);
    }

    /**
     * Assign controller variables to the view.
     * Variables can either be specified as an array, or simply as name -> value.
     *
     * @param array|string $name
     * @param int|string $value
     */
    public function assign($name, $value = '')
    {
        if (is_array($name)) {
            foreach ($name as $key => $val) {
                Bootstrap::getInstance()->$key = $val;
            }
        } else {
            Bootstrap::getInstance()->$name = $value;
        }
    }

    /**
     * Include a given file into the layout or view.
     *
     * @param string $file
     */
    public function render($file)
    {
        parent::render($file);
    }

    /**
     * Set the SEO meta tags as view params to be used in the layout or view.
     *
     * @param string $name
     * @param string $value
     */
    public function setMeta($name, $value)
    {
        Bootstrap::getInstance()->meta[$name] = $value;
    }

    /**
     * Establish what layout a controller should implement.
     *
     * @param string $layout
     */
    public function setLayout($layout)
    {
        Bootstrap::getInstance()->_layoutPath = 'application/layouts/' . $layout;
    }

    /**
     * Establish the full path of the layout a controller should implement.
     *
     * @param string $layout
     */
    public function setFullLayout($layout)
    {
        Bootstrap::getInstance()->_layoutPath = $layout;
    }

    /**
     * Establish what view a controller should implement.
     *
     * @param string $view
     */
    public function setView($view)
    {
        Bootstrap::getInstance()->_fullViewPath = 'application/modules/' . $view;
    }

    /**
     * Establish the full path of the view a controller should implement.
     *
     * @param string $view
     */
    public function setFullView($view)
    {
        Bootstrap::getInstance()->_fullViewPath = $view;
    }

    /**
     * Choose to not load the assigned layout contents.
     * If disabled, only the view contents will be displayed in the browser.
     *
     * @set $_disableLayout
     */
    public function disableLayout()
    {
        Bootstrap::getInstance()->_disableLayout = true;
    }

    /**
     * Choose to not load the assigned view contents.
     * If disabled, only the layout contents will be displayed in the browser.
     *
     * @set $_disableView
     */
    public function disableView()
    {
        Bootstrap::getInstance()->_disableView = true;
    }

    /**
     * Assign CSS includes to the Placeholder helper to be used in the head.
     *
     * @param string $path
     */
    public function prependStyleSheet($path)
    {
        Bootstrap::getInstance()->_prependStyleSheet .= '<link href="' . $path . '" rel="stylesheet" type="text/css" />';
    }

    /**
     * Assign CSS rules to the Placeholder helper to be used in the head.
     *
     * @param string $code
     */
    public function prependStyleSheetInline($code)
    {
        Bootstrap::getInstance()->_prependStyleSheetInline .= $code;
    }

    /**
     * Assign JS includes to the Placeholder helper to be used in the head.
     *
     * @param string $path
     */
    public function prependScript($path)
    {
        Bootstrap::getInstance()->_prependScript .= '<script src="' . $path . '" type="text/javascript"></script>';
    }

    /**
     * Assign JS rules to the Placeholder helper to be used in the head.
     *
     * @param string $code
     */
    public function prependScriptInline($code)
    {
        Bootstrap::getInstance()->_prependScriptInline .= $code;
    }

    /**
     * Assign CSS includes to the Placeholder helper to be used in the footer.
     *
     * @param string $path
     */
    public function appendStyleSheet($path)
    {
        Bootstrap::getInstance()->_appendStyleSheet .= '<link href="' . $path . '" rel="stylesheet" type="text/css" />';
    }

    /**
     * Assign CSS rules to the Placeholder helper to be used in the footer.
     *
     * @param string $code
     */
    public function appendStyleSheetInline($code)
    {
        Bootstrap::getInstance()->_appendStyleSheetInline .= $code;
    }

    /**
     * Assign JS includes to the Placeholder helper to be used in the footer.
     *
     * @param string $path
     */
    public function appendScript($path)
    {
        Bootstrap::getInstance()->_appendScript .= '<script src="' . $path . '" type="text/javascript"></script>';
    }

    /**
     * Assign JS rules to the Placeholder helper to be used in the footer.
     *
     * @param string $code
     */
    public function appendScriptInline($code)
    {
        Bootstrap::getInstance()->_appendScriptInline .= $code;
    }
}