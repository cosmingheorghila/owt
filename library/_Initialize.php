<?php

/**
 | ----------------------------------------------------------------------------------------------
 | FLASH MESSAGE
 | ----------------------------------------------------------------------------------------------
 |
 | After displaying the message reset the session responsible with the message.
 |
 */
if (Session::exists('flash-message') && !Session::exists('flash-message-created')) {
    Session::set('flash-message-created', time());
}

if (Session::exists('flash-message') && (time() - Session::get('flash-message-created') > 1)) {
    Session::disable('flash-message-created');
    Session::disable('flash-message');
}

/**
 | ----------------------------------------------------------------------------------------------
 | REMEMBER FORM DATA
 | ----------------------------------------------------------------------------------------------
 |
 | Unset the data, so on success it does not remember it.
 |
 */
if (Session::exists('data') && !Session::exists('data-created')) {
    Session::set('data-created', time());
}

if (Session::exists('data') && (time() - Session::get('data-created') > 1)) {
    Session::disable('data-created');
    Session::disable('data');
}