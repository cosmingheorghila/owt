<?

class Placeholder
{
    /**
     * Virtual constructor to be used in bootstrapping the application.
     *
     * @return $this
     */
    public function placeholder()
    {
        return $this;
    }

    /**
     * Render all the JS & CSS that is supposed to be placed in the head.
     *
     * @return void
     */
    public function top()
    {
        echo Bootstrap::getInstance()->_prependStyleSheet;
        echo Bootstrap::getInstance()->_prependStyleSheetInline;
        echo Bootstrap::getInstance()->_prependScript;
        echo Bootstrap::getInstance()->_prependScriptInline;
    }

    /**
     * Render all the JS & CSS that is supposed to be placed in the footer.
     *
     * @return void
     */
    public function bottom()
    {
        echo Bootstrap::getInstance()->_appendStyleSheet;
        echo Bootstrap::getInstance()->_appendStyleSheetInline;
        echo Bootstrap::getInstance()->_appendScript;
        echo Bootstrap::getInstance()->_appendScriptInline;
    }
}
 