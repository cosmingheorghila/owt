<?php

class Redirect
{
    /**
     * Redirect the user to the given URL.
     *
     * @param string $url
     * @param array $parameters
     * @return void
     */
    public static function url($url, $parameters = [])
    {
        header('Location: ' . $url . (!empty($parameters) ? '?' . http_build_query($parameters) : ''));
        die;
    }

    /**
     * Redirect the user to the given page URL.
     * To get the page URL, the $identifier parameter is used.
     *
     * @param string $identifier
     * @param array $parameters
     * @return void
     */
    public static function page($identifier, $parameters = [])
    {
        header('Location: ' . Cms_Model_Page::getInstance()->load([
                'identifier' => $identifier
            ])->getUrl() . (!empty($parameters) ? '?' . http_build_query($parameters) : ''));
        die;
    }
}