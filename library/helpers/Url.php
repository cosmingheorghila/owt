<?php

class Url
{
    /**
     * Return the application prefix URL, found in the config.xml file
     *
     * @return string
     */
    public static function getPrefix()
    {
        return (string)Config::getConfig()->prefix;
    }

    /**
     * Return the current URI.
     *
     * @return string
     */
    public static function getUrl()
    {
        return Server::get('REQUEST_URI');
    }

    /**
     * Return the current URI without any query parameters.
     *
     * @return string
     */
    public static function getRawUrl()
    {
        return strtok(Server::get('REQUEST_URI'), '?');
    }

    /**
     * Return the home URI.
     *
     * @return string
     */
    public static function getHomeUrl()
    {
        return self::getPrefix() ? self::getPrefix() : '/';
    }

    /**
     * Return the admin URI.
     *
     * @return string
     */
    public static function getAdminUrl()
    {
        return self::getPrefix() ? self::getPrefix() . '/admin' : '/admin';
    }

    /**
     * Return the current URL.
     *
     * @return string
     */
    public static function getCurrentUrl()
    {
        return ((Server::get('HTTPS') && Server::get('HTTPS') !== 'off') || Server::get('SERVER_PORT') == 443 ? 'https://' : 'http://') . (Server::exists('HTTP_X_FORWARDED_HOST') ? Server::get('HTTP_X_FORWARDED_HOST') : Server::get('HTTP_HOST')) . Server::get('REQUEST_URI');
    }

    /**
     * Get the previous URL.
     *
     * @return mixed
     */
    public static function getPreviousUrl()
    {
        return Server::get('HTTP_REFERER');
    }

    /**
     * Return the host URL.
     *
     * @return string
     */
    public static function getHostUrl()
    {
        return ((Server::get('HTTPS') && Server::get('HTTPS') !== 'off') || Server::get('SERVER_PORT') == 443 ? 'https://' : 'http://') . (Server::exists('HTTP_X_FORWARDED_HOST') ? Server::get('HTTP_X_FORWARDED_HOST') : Server::get('HTTP_HOST'));
    }

    /**
     * Return the full URL.
     *
     * @return string
     */
    public static function getFullUrl()
    {
        return ((Server::get('HTTPS') && Server::get('HTTPS') !== 'off') || Server::get('SERVER_PORT') == 443 ? 'https://' : 'http://') . (Server::exists('HTTP_X_FORWARDED_HOST') ? Server::get('HTTP_X_FORWARDED_HOST') : Server::get('HTTP_HOST')) . Server::get('REQUEST_URI');
    }
}