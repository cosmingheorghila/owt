<?php

class Dir
{
    /**
     * Return the base application directory.
     *
     * @return string
     */
    public static function getRoot()
    {
        return Server::get('DOCUMENT_ROOT') . Url::getPrefix();
    }

    /**
     * Return the /config application directory.
     *
     * @return string
     */
    public static function getConfig()
    {
        return Server::get('DOCUMENT_ROOT') . Url::getPrefix() . '/application/config';
    }

    /**
     * Return the /public application directory.
     *
     * @return string
     */
    public static function getPublic()
    {
        return Server::get('DOCUMENT_ROOT') . Url::getPrefix() . '/public';
    }

    /**
     * Return the /library application directory.
     *
     * @return string
     */
    public static function getLibrary()
    {
        return Server::get('DOCUMENT_ROOT') . Url::getPrefix() . '/library';
    }

    /**
     * Return the /modules application directory.
     *
     * @return string
     */
    public static function getModules()
    {
        return Server::get('DOCUMENT_ROOT') . Url::getPrefix() . '/application/modules';
    }

    /**
     * Return the /database application directory.
     *
     * @return string
     */
    public static function getDatabase()
    {
        return Server::get('DOCUMENT_ROOT') . Url::getPrefix() . '/database';
    }

    /**
     * Return the /layouts application directory.
     *
     * @return string
     */
    public static function getLayouts()
    {
        return Server::get('DOCUMENT_ROOT') . Url::getPrefix() . '/application/layouts';
    }

    /**
     * Return the /migrations application directory.
     *
     * @return string
     */
    public static function getMigrations()
    {
        return Server::get('DOCUMENT_ROOT') . Url::getPrefix() . '/database/migrations';
    }

    /**
     * Return the /backups application directory.
     *
     * @return string
     */
    public static function getBackups()
    {
        return Server::get('DOCUMENT_ROOT') . Url::getPrefix() . '/database/backups';
    }

    /**
     * Return the /uploads application directory.
     *
     * @return string
     */
    public static function getUploads()
    {
        return Url::getPrefix() . '/public/uploads';
    }

    /**
     * Return the /assets application directory.
     *
     * @return string
     */
    public static function getAssets()
    {
        return Url::getPrefix() . '/public/assets';
    }

    /**
     * Return the /image application directory.
     *
     * @return string
     */
    public static function getImage()
    {
        return 'public/uploads/image/';
    }

    /**
     * Return the /original image application directory.
     *
     * @return string
     */
    public static function getOriginalImage()
    {
        return 'public/uploads/image/original/';
    }

    /**
     * Return the /video application directory.
     *
     * @return string
     */
    public static function getVideo()
    {
        return 'public/uploads/video/';
    }

    /**
     * Return the /thumbnails video application directory.
     *
     * @return string
     */
    public static function getVideoThumbnail()
    {
        return 'public/uploads/video/thumbnails/';
    }

    /**
     * Return the /audio application directory.
     *
     * @return string
     */
    public static function getAudio()
    {
        return 'public/uploads/audio/';
    }

    /**
     * Return the /file application directory.
     *
     * @return string
     */
    public static function getFile()
    {
        return 'public/uploads/file/';
    }

    /**
     * Return the /pdf application directory.
     *
     * @return string
     */
    public static function getPdf()
    {
        return 'public/uploads/pdf/';
    }
}