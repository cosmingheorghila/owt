<?php

class String
{
    /**
     * @const
     */
    const TRUNCATE_WORD         = 1;
    const TRUNCATE_CHARACTER    = 2;

    /**
     * Limit a given string to the specified number of characters.
     * You can also choose to append dots at the newly created string.
     *
     * @param string $text
     * @param int $length
     * @param bool $dots
     * @param int $type
     * @return string
     */
    public static function truncate($text, $length, $dots = true, $type = self::TRUNCATE_WORD)
    {
        switch ($type) {
            case self::TRUNCATE_WORD:
                return str_word_count($text, 0) > $length ? substr($text, 0, array_keys(str_word_count($text, 2))[$length]) . ($dots === true ? '...' : '') : $text;
                break;
            default:
                return strlen($text) > $length ? substr($text, 0, $length) . ($dots === true ? '...' : '') : $text;
                break;
        }
    }

    /**
     * Check if a given string contains the specified character set.
     *
     * @param string $haystack
     * @param string $needle
     * @return bool
     */
    public static function contains($haystack, $needle)
    {
        return strpos($haystack, $needle) !== false ? true : false;
    }

    /**
     * Check if a given string starts with the specified character set.
     *
     * @param string $haystack
     * @param string $needle
     * @return bool
     */
    public static function startsWith($haystack, $needle)
    {
        return (substr($haystack, 0, strlen($needle)) === $needle);
    }

    /**
     * Check if a given string ends with the specified character set.
     * @param string $haystack
     * @param string $needle
     * @return bool
     */
    public static function endsWith($haystack, $needle)
    {
        return strlen($needle) == 0 ? true : (substr($haystack, -strlen($needle)) === $needle);
    }

    /**
     * Generate a random string that could contain numbers, letters and special characters.
     * Also, you can specify the string length.
     *
     * @param int $length
     * @return string
     */
    public static function random($length = 10)
    {
        $characters     = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString   = '';

        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }

        return $randomString;
    }

    /**
     * Slugify a given string to make it URL friendly.
     *
     * @param string $text
     * @return mixed
     */
    public static function slugify($text)
    {
        return preg_replace('~[^-\w]+~', '', strtolower(iconv('utf-8', 'us-ascii//TRANSLIT', trim(preg_replace('~[^\\pL\d]+~u', '-', $text), '-'))));
    }

    /**
     * @param string|int $phoneNumber
     * @return string
     */
    public static function formatPhone($phoneNumber)
    {
        if (String::contains($phoneNumber, '+')) {
            if (preg_match('/^\+\d(\d{4})(\d{3})(\d{3})$/', $phoneNumber,  $matches)) {
                $result = $matches[1] . ' ' .$matches[2] . ' ' . $matches[3];

                return $result;
            }
        } else {
            if (preg_match('/(\d{4})(\d{3})(\d{3})$/', $phoneNumber,  $matches)) {
                $result = $matches[1] . ' ' .$matches[2] . ' ' . $matches[3];

                return $result;
            }
        }

        return $phoneNumber;
    }
}