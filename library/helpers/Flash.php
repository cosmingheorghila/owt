<?php

class Flash
{
    /**
     * Returns the success or error message.
     * Mostly for the purpose of displaying it to the user.
     *
     * @return mixed
     */
    public static function show()
    {
        return Session::get('flash-message');
    }

    /**
     * Create a success message.
     * Store it in a session, so it would be available on further browsing.
     *
     * @param string $message
     * @return void
     */
    public static function success($message)
    {
        Session::set('flash-message', '<div class="flash"><span class="success">' . $message . '</span></div>');
    }

    /**
     * Create an error message.
     * Store it in a session, so it would be available on further browsing.
     *
     * @param string $message
     * @return void
     */
    public static function error($message)
    {
        Session::set('flash-message', '<div class="flash"><span class="error">' . $message . '</span></div>');
    }

    /**
     * Returns only the success or error message without any formatting.
     *
     * @return mixed
     */
    public static function get()
    {
        return Session::exists('flash-message') ? str_replace(
            '<div class="flash"><span class="success">', '', str_replace(
                '<div class="flash"><span class="error">', '', str_replace(
                    '</span></div>', '', Session::get('flash-message')
                )
            )
        ) : null;
    }
}