<?php

class Arr
{
    /**
     * Add an element to the given array.
     * If the $key is not specified, then the value is assigned to an index in the array.
     *
     * @param array $array
     * @param string|null $key
     * @param string|null $value
     * @return array
     * @throws Exception
     */
    public static function add($array = [], $key = null, $value = null)
    {
        if (!is_array($array)) {
            throw new Exception('The ARRAY parameter must be of type array');
        }

        if (array_key_exists($key, $array)) {
            return $array;
        }

        $key === null ? $array[] = $value : $array[$key] = $value;

        return $array;
    }

    /**
     * Remove an element from the given array.
     * You can specify either only the $key or $value.
     * If both $key and $value are specified, the element with that $key, containing that $value will be removed.
     * If no $key - $value relation exists, then no element will be removed.
     *
     * @param array $array
     * @param string|null $key
     * @param string|null $value
     * @return array
     * @throws Exception
     */
    public static function remove($array = [], $key = null, $value = null)
    {
        if (!is_array($array)) {
            throw new Exception('The ARRAY parameter must be of type array');
        }

        if ($key && !array_key_exists($key, $array)) {
            return $array;
        }

        if ((!$key && $value) && ($key = array_search($value, $array)) !== false) {
            if ($key && !array_key_exists($key, $array)) {
                return $array;
            }
        }

        unset($array[$key]);

        return $array;
    }

    /**
     * Returns the first element of the given array, without deleting it from the initial array.
     *
     * @param array $array
     * @param bool $associative
     * @return array|mixed
     * @throws Exception
     */
    public static function first($array = [], $associative = false)
    {
        if (!is_array($array)) {
            throw new Exception('The ARRAY parameter must be of type array');
        }

        return $associative ? [key($array) => reset($array)] : reset($array);
    }

    /**
     * Returns the last element of the given array, without deleting it from the initial array.
     *
     * @param array $array
     * @param bool $associative
     * @return array|mixed
     * @throws Exception
     */
    public static function last($array = [], $associative = false)
    {
        if (!is_array($array)) {
            throw new Exception('The ARRAY parameter must be of type array');
        }

        return $associative ? [key(array_slice($array, -1, 1, true)) => end($array)] : end($array);
    }

    /**
     * Builds two arrays based on the given array.
     * One containing the keys of the given array and the other containing the values.
     *
     * @param array $array
     * @return array
     * @throws Exception
     */
    public static function divide($array = [])
    {
        if (!is_array($array)) {
            throw new Exception('The ARRAY parameter must be of type array');
        }

        return [
            array_keys($array),
            array_values($array)
        ];
    }

    /**
     * Merge two arrays into one, overwriting the keys of the first array if those keys exist in the second also.
     *
     * @param array $array1
     * @param array $array2
     * @return array
     * @throws Exception
     */
    public static function merge($array1 = [], $array2 = [])
    {
        if (!is_array($array1) || !is_array($array2)) {
            throw new Exception('The ARRAY_1 & ARRAY_2 parameters must be of type array');
        }

        return array_merge($array1, $array2);
    }

    /**
     * Convert an array to an object of instance stdClass
     *
     * @param array $array
     * @return mixed
     */
    public static function object($array)
    {
        return json_decode(json_encode($array), false);
    }

    /**
     * Convert an array to a json instance
     *
     * @param array $array
     * @return mixed
     */
    public static function json($array)
    {
        return json_encode($array);
    }
}