<?php

class File
{
    /**
     * Checks if a given file exists in the specified directory.
     *
     * @param string $path
     * @return bool
     */
    public static function exists($path)
    {
        return file_exists($path);
    }

    /**
     * Creates a file in the specified directory if not exists.
     * If the $content parameter is supplied, then the file will be populated with the contents of the $content parameter.
     *
     * @param string $path
     * @param string|null $content
     * @return int
     */
    public static function create($path, $content = null)
    {
        return file_put_contents($path, $content);
    }

    /**
     * Deletes a given file from the specified directory.
     *
     * @param string $path
     * @return bool
     */
    public static function delete($path)
    {
        return unlink($path);
    }

    /**
     * Returns the contents of a given file in the specified directory.
     *
     * @param string $path
     * @return int
     */
    public static function read($path)
    {
        return file_get_contents($path);
    }


    /**
     * Duplicates a given file in the specified directory.
     * The duplicated file will be placed in the given directory specified in the second parameter.
     *
     * @param string $currentPath
     * @param string $newPath
     * @return bool
     */
    public static function duplicate($currentPath, $newPath)
    {
        if (!copy($currentPath, $newPath)) {
            return false;
        }

        return true;
    }
}