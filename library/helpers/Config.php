<?php

class Config
{
    /**
     * Returns the contents of the specified XML node inside the config.xml file.
     * If multiple environments exist, the contents of the environment found in the env file will be returned.
     * If that node does not exist in the environment specified, inheritance comes into play.
     *
     * @param string $entity
     * @return mixed
     * @throws Exception
     */
    public static function get($entity)
    {
        if (!$entity) {
            throw new Exception('You must specify an ENTITY from the "config.xml" file');
        }

        $data   = simplexml_load_file('application/config/config.xml');
        $env    = @file_get_contents(BASE_PATH . '/env') ? file_get_contents(BASE_PATH . '/env') : 'production';

        if (!$data->$env->$entity) {
            $attributes = $data->$env->attributes();
            $parent     = (string)$attributes['extends'];

            if (!$data->$parent->$entity) {
                $attributes = $data->$parent->attributes();
                $parent     = (string)$attributes['extends'];

                return $data->$parent->$entity;
            }

            return $data->$parent->$entity;
        }

        return $data->$env->$entity;
    }

    /**
     * Returns the full contents of the config.xml file.
     *
     * @return SimpleXMLElement
     */
    public static function getConfig()
    {
        $data   = simplexml_load_file('application/config/config.xml');
        $env    = file_get_contents(BASE_PATH . '/env');

        return $data->$env;
    }

    /**
     * Returns the full contents of the acl.xml file.
     *
     * @return SimpleXMLElement
     */
    public static function getAcl()
    {
        return simplexml_load_file('application/config/acl.xml');
    }

    /**
     * Returns the full contents of the images.xml file.
     *
     * @return SimpleXMLElement
     */
    public static function getImages()
    {
        return simplexml_load_file('application/config/images.xml');
    }

    /**
     * Returns the full contents of the videos.xml file.
     *
     * @return SimpleXMLElement
     */
    public static function getVideos()
    {
        return simplexml_load_file('application/config/videos.xml');
    }
}