<?php

class Collection implements IteratorAggregate
{
    /**
     * Placeholder containing the number of instances the data holds.
     *
     * @var int
     */
    protected $count = 0;

    /**
     * Placeholder containing each instance of a record set.
     *
     * @var array
     */
    protected $data = [];

    /**
     * Helper method used to iterate through the data property.
     *
     * @return ArrayIterator
     */
    public function getIterator()
    {
        return new ArrayIterator($this->data);
    }

    /**
     * Return the data property.
     *
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Return the number of instances the data placeholder contains.
     *
     * @return int
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * Add a loaded model instance to the collection.
     *
     * @param Model $model
     * @return $this
     * @throws Exception
     */
    public function add(Model $model)
    {
        $this->data = Arr::add(
            $this->data, null, $model
        );

        $this->count++;

        return $this;
    }

    /**
     * Remove a model instance from the current collection set.
     *
     * @param Model $model
     * @return $this
     * @throws Exception
     */
    public function remove(Model $model)
    {
        $this->data = Arr::remove(
            $this->data, null, $model
        );

        $this->count--;

        return $this;
    }

    /**
     * Return the first model instance inside the data container.
     *
     * @return array|mixed
     * @throws Exception
     */
    public function first()
    {
        return Arr::first(
            $this->data
        );
    }

    /**
     * Return the last model instance inside the data container.
     *
     * @return array|mixed
     * @throws Exception
     */
    public function last()
    {
        return Arr::last(
            $this->data
        );
    }

    /**
     * Return only matched elements inside the existing collection.
     *
     * @param array $conditions
     * @return Collection
     */
    public function where($conditions = [])
    {
        $collection = new self;
        $status     = true;

        foreach ($this->data as $data) {
            foreach ($conditions as $key => $val) {
                if ($data->getData($key) != $val) {
                    $status = false;
                }
            }

            if ($status) {
                $collection->add($data);
            }
        }

        return $collection;
    }

    /**
     * Return only a limited number of elements according to the $limit and $offset parameters.
     * This method works like an SQL LIMIT QUERY.
     *
     * @param int|null $limit
     * @param int $offset
     * @return Collection
     */
    public function slice($limit = null, $offset = 0)
    {
        $collection = new self;
        $limit      = is_numeric($limit) ? $limit : $this->count;

        foreach ($this->data as $index => $data) {
            if ($index >= $offset && $collection->count < $limit) {
                $collection->add($data);
            }
        }

        return $collection;
    }

    /**
     * Convert a collection object into an array instance.
     * The resulting array will contain only the loaded model's data.
     *
     * @return array
     * @throws Exception
     */
    public function toArray()
    {
        $result = [];

        if (empty($this->data)) {
            return [];
        }

        foreach ($this->data as $item) {
            $result = Arr::add(
                $result, null, $item->getData()
            );
        }

        return $result;
    }

    /**
     * Convert a collection object into an associative array.
     * The associative array will look like key -> value and it will use the model data.
     * The resulting array will contain only the loaded model's data.
     * Also, an empty key -> value can pe prepended.
     *
     * @param bool $none
     * @param string $key
     * @param string $value
     * @return array
     */
    public function toSelectArray($none = false, $key = 'id', $value = 'name')
    {
        $result = [];

        if ($none) {
            $result['null'] = 'None';
        } elseif (empty($this->data)) {
            return [];
        }

        foreach ($this->data as $item) {
            $result[$item->getData($key)] = String::contains($value, '.') ? implode(' ', array_map(function ($val) use ($item) {
                return $item->getData($val);
            }, explode('.', $value))) : $item->getData($value);
        }

        return $result;
    }

    /**
     * Convert a collection object into an array instance.
     * The resulting array will contain only the loaded model's data.
     *
     * @return array
     * @throws Exception
     */
    public function toJson()
    {
        $result = [];

        if (empty($this->data)) {
            return json_encode([]);
        }

        foreach ($this->data as $item) {
            $result = Arr::add(
                $result, null, $item->getData()
            );
        }

        return json_encode($result);
    }

    /**
     * @param int $count
     * @param array $data
     * @return mixed
     */
    public function create($count = 0, array $data = [])
    {
        $this->count    = $count;
        $this->data     = $data;

        return $this;
    }

    /**
     * @param Collection $collection1
     * @param Collection $collection2
     * @return Collection
     */
    public function merge(Collection $collection1, Collection $collection2)
    {
        return $this->create(
            $collection1->getCount() + $collection2->getCount(),
            array_merge_recursive(
                $collection1->getData(),
                $collection2->getData()
            )
        );
    }

    /**
     * @param Collection $collection1
     * @param Collection $collection2
     * @return Collection
     */
    public function diff(Collection $collection1, Collection $collection2)
    {
        $diff = array_diff($collection1->getData(), $collection2->getData());

        return $this->create(count($diff), $diff);
    }
}