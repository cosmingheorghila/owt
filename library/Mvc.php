<?php

/**
 |
 | ---------------------------------------------------------------------------------------------------------------------
 | This class represents an extraction of helper methods for the Bootstrap class
 | ---------------------------------------------------------------------------------------------------------------------
 |
 | All of these methods are to be used either in the Bootstrap class, or in view or layout files.
 | ---------------------------------------------------------------------------------------------------------------------
 |
 */
class Mvc
{
    /**
     * Render the correct view in relation to the controller loaded.
     *
     * @throws Exception
     */
    public function view()
    {
        if (!$this->_disableView) {
            if (!File::exists($this->_fullViewPath)) {
                throw new Exception('VIEW FILE not found: ' . $this->_fullViewPath);
            }

            $this->render($this->_fullViewPath);
        }
    }

    /**
     * Require helper method that can inject external variables into the view required.
     *
     * @param string $view
     * @param array $params
     * @throws Exception
     */
    public function partial($view, $params = [])
    {
        if (!is_array($params)) {
            throw new Exception('The PARAMS parameter must be of type array');
        }

        if (!empty($params)) {
            foreach ($params as $key => $val) {
                Bootstrap::getInstance()->{$key} = $val;
            }
        }

        include('application/modules/' . $view);
    }

    /**
     * Require helper method.
     *
     * @param string $file
     */
    public function render($file)
    {
        require_once($file);
    }

    /**
     * Return a new Placeholder instance and the inline the include methods.
     *
     * @return Placeholder
     */
    public function placeholder()
    {
        return new Placeholder();
    }

    /**
     * Return the page matching the given identifier.
     * To be used in views.
     *
     * @param string $identifier
     * @return mixed
     */
    public function page($identifier)
    {
        return Cms_Model_Page::getInstance()->load(['identifier' => $identifier]);
    }

    /**
     * Return the exact full image path, based only on the image's name.
     *
     * @param string $source
     * @param string $thumb
     * @return string
     */
    public function image($source, $thumb = 'default')
    {
        return Url::getPrefix() . '/' . Dir::getImage() . explode('.', $source)[0] . '_' . $thumb . '.' . explode('.', $source)[1];
    }

    /**
     * Return the exact full video path, based only on the video's name.
     *
     * @param string $source
     * @return string
     */
    public function video($source)
    {
        return Url::getPrefix() . '/' . Dir::getVideo() . $source;
    }

    /**
     * Hard render a block inside a layout or view.
     *
     * @param string $class
     */
    public function block($class)
    {
        (new Behavior_Block())->block($class);
    }

    /**
     * Helper for using inside a layout or view for displaying the correct locations tabs on an admin edit page.
     *
     * @param Model|Database $model
     */
    public function blockTabs($model)
    {
        (new Behavior_Block())->blockTabs($model);
    }

    /**
     * Helper for using inside a layout or view.
     * Display the correct location containers or partials on an admin edit page.
     *
     * @param Model|Database $model
     */
    public function blockContainers($model)
    {
        (new Behavior_Block())->blockContainers($model);
    }

    /**
     * Helper for using inside a layout or view for displaying the correct blocks assigned.
     *
     * @param string $location
     * @throws Exception
     */
    public function blockHolder($location)
    {
        if ($this->page && $this->layout) {
            (new Behavior_Block())->blockHolder($location, $this->page, $this->layout);
        } elseif ($this->blockHolder && ($blockHolder = $this->blockHolder->getBehaviors('block'))) {
            if ($blockHolder['inherit_from']['entity'] == 'page') {
                $page = Cms_Model_Page::getInstance()->load([
                    'identifier' => $blockHolder['inherit_from']['identifier']
                ]);

                $layout = Cms_Model_Layout::getInstance()->load([
                    'id' => $page->getLayoutId()
                ]);
            } elseif ($blockHolder['inherit_from']['entity'] == 'layout') {
                $page   = null;
                $layout = Cms_Model_Layout::getInstance()->load([
                    'identifier' => $blockHolder['inherit_from']['identifier']
                ]);
            } else {
                throw new Exception('The BLOCK BEHAVIOR must have INHERIT_FROM -> ENTITY set to PAGE or LAYOUT');
            }

            if (in_array($location, $blockHolder['locations'])) {
                (new Behavior_Block())->blockHolder($location, $page, $layout);
            }
        }
    }

    /**
     * Compose a valid MVC URL.
     * The $params parameter must be an array containing the keys: module, controller, action.
     *
     * @param array|null $params
     * @return string
     */
    public function url($params = null)
    {
        if ($params === null) {
            return Url::getCurrentUrl();
        }

        if ($params['module'] == 'default' && $params['controller'] == 'index' && $params['action'] == 'index') {
            return Url::getPrefix() . '/';
        }

        $url = Url::getPrefix();

        if ((!isset($params['module']) || empty($params['module'])) && (!isset($params['controller']) || empty($params['controller'])) && (!isset($params['action']) || empty($params['action']))) {
            $url = Url::getUrl();
        } else {
            if ((!isset($params['module']) || empty($params['module'])) || $params['module'] == 'default') {
                $params['module'] = 'default';
                $url .= '';
            } else {
                $url .= '/' . $params['module'];
            }

            if (!isset($params['controller']) || empty($params['controller'])) {
                $params['controller'] = 'index';
                $url .= '/' . $params['controller'];
            } else {
                if (String::contains($params['controller'], '_')) {
                    $params['path']         = explode('_', $params['controller']);
                    $params['controller']   = array_pop($params['path']);
                    $params['path']         = implode('/', $params['path']);
                }
            }

            $url .= '/' . (isset($params['path']) && !empty($params['path']) ? $params['path'] . '/' : '') . $params['controller'];

            if (!isset($params['action']) || empty($params['action'])) {
                $params['action'] = 'index';
            } else {
                $url .= '/' . $params['action'];
            }
        }

        unset($params['module']);
        unset($params['controller']);
        unset($params['action']);
        unset($params['path']);

        if (!empty($params)) {
            $url .= '?' . http_build_query($params);
        }

        return $url;
    }

    /**
     * Compose a valid MVC URL.
     * The $params parameter must be an array containing the keys: module, controller, action.
     *
     * @param array|null $params
     * @return string
     */
    public function mixedUrl($params = null)
    {
        $url = Url::getCurrentUrl();

        if (!empty($params)) {
            if (Server::exists('QUERY_STRING')) {
                $url = array_shift(explode('?', $url));
                $arr = [];

                parse_str(Server::get('QUERY_STRING'), $arr);

                foreach ($arr as $key => $val) {
                    if (isset($queryParams[$key])) {
                        unset($arr[$key]);
                    }
                }

                $url .= '?' . (http_build_query($arr) != '' ? http_build_query($arr) . '&' : '') . http_build_query($params);
            } else {
                $url .= '?' . http_build_query($params);
            }
        }

        return $url;
    }

    /**
     * Compose all the MVC variables required on further load processing.
     * The $mvc parameter must be an array containing the keys: module, controller, action.
     *
     * @param array $mvc
     * @return array
     */
    public function mvc($mvc = [])
    {
        $result = [];

        $result['controllerAction']     = $this->mvcControllerAction($mvc['action']);
        $result['controllerClass']      = $this->mvcControllerClass($mvc['module'], $mvc['controller']);
        $result['controllerFile']       = $this->mvcControllerFile($mvc['controller']);
        $result['viewFile']             = $this->mvcViewFile($mvc['controller'], $mvc['action']);
        $result['fullControllerPath']   = 'application/modules/' . $mvc['module'] . '/controllers/' . $result['controllerFile'];
        $result['fullViewPath']         = 'application/modules/' . $mvc['module']. '/views/' . $result['viewFile'];

        return $result;
    }

    /**
     * Build the controller action based on the given mvc parameters.
     *
     * @param string $action
     * @return string
     */
    private function mvcControllerAction($action)
    {
        $controllerAction = $action;

        if (String::contains($action, '-') !== false) {
            $controllerAction = '';

            foreach (explode('-', $action) as $key => $val) {
                $controllerAction .= $key == 0 ? $val : ucwords($val);
            }
        }

        return $controllerAction . 'Action';
    }

    /**
     * Build the controller class based on the given mvc parameters.
     *
     * @param string $module
     * @param string $controller
     * @return mixed|string
     */
    private function mvcControllerClass($module, $controller)
    {
        $controllerClass = ucwords($module) . '_';

        if (String::contains($controller, '-') !== false) {
            foreach (explode('-', $controller) as $val) {
                $controllerClass .= ucwords($val);
            }
        } else {
            $controllerClass .= ucwords($controller);
        }

        if (String::contains($controllerClass, '_') !== false) {
            $items = explode('_', $controllerClass);
            $controllerClass = '';

            foreach ($items as $val) {
                $controllerClass .= ucwords($val) . '_';
            }
        }

        return str_replace('_Controller', 'Controller', $controllerClass . 'Controller');
    }

    /**
     * Build the controller file based on the given mvc parameters.
     *
     * @param string $controller
     * @return string
     */
    private function mvcControllerFile($controller)
    {
        if (String::contains($controller, '-') !== false) {
            $controllerFile = '';

            foreach (explode('-', $controller) as $val) {
                $controllerFile .= ucwords($val);
            }
        } else {
            $controllerFile = ucwords($controller);
        }

        if (String::contains($controllerFile, '_') !== false) {
            $controllerFile = '';

            foreach (explode('_', $controllerFile) as $key => $val) {
                $controllerFile .= count(explode('_', $controllerFile)) == $key + 1 ? ucwords($val) . '/' : strtolower($val) . '/';
            }
        }

        return str_replace('/Controller', 'Controller', $controllerFile . 'Controller.php');
    }

    /**
     * Build the view file based on the given mvc parameters.
     *
     * @param string $controller
     * @param string $action
     * @return string
     */
    private function mvcViewFile($controller, $action)
    {
        $viewFile = $controller;

        if (String::contains($viewFile, '_')) {
            $viewFile = '';

            foreach (explode('_', $viewFile) as $key => $val) {
                $viewFile .= count(explode('_', $viewFile)) == $key + 1 ? strtolower($val) : strtolower($val) . '/';
            }
        }

        if (String::contains($action, '-')) {
            foreach (explode('-', $action) as $key => $val) {
                $viewFile .= $key == 0 ? '/' . $val : '_' . $val;
            }
        } else {
            $viewFile .= '/' . $action;
        }

        return str_replace('/.phtml', '.phtml', $viewFile . '.phtml');
    }
}