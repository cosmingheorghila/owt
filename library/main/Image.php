<?php

class Image
{
    /**
     * Image extensions that can be parsed.
     *
     * @var array
     */
    public static $imageExtensions = [
        'jpg', 'jpeg', 'gif', 'png', 'bmp'
    ];

    /**
     * Available image content types.
     *
     * @var array
     */
    public static $contentTypes = [
        'jpg'   => 'image/jpeg',
        'jpeg'  => 'image/jpeg',
        'gif'   => 'image/gif',
        'png'   => 'image/png',
        'bmp'   => 'image/bmp',
    ];

    /**
     * The file handler.
     *
     * @var array
     */
    private $file;

    /**
     * The image handler.
     *
     * @var
     */
    private $image;

    /**
     * The actual image width.
     *
     * @var
     */
    private $width;

    /**
     * The actual image height.
     *
     * @var
     */
    private $height;

    /**
     * The image type according to the images.xml file.
     *
     * @var
     */
    private $type;

    /**
     * The specs container populated with the contents from the images.xml file.
     *
     * @var
     */
    private $specs;

    /**
     * The actual name of the image.
     *
     * @var
     */
    private $fileName;

    /**
     * The actual size of the image.
     *
     * @var
     */
    private $fileSize;

    /**
     * The actual temporary name of the image.
     *
     * @var
     */
    private $fileTmpName;

    /**
     * The maximum size allowed for uploading in MB.
     *
     * @var int
     */
    private $maxSize = 30;

    /**
     * Constants used when creating image watermarks.
     * They are used for the positioning of the watermarks.
     *
     * @const
     */
    const CENTER_CENTER = 0;
    const LEFT_TOP      = 1;
    const CENTER_TOP    = 2;
    const RIGHT_TOP     = 3;
    const RIGHT_CENTER  = 4;
    const RIGHT_BOTTOM  = 5;
    const CENTER_BOTTOM = 6;
    const LEFT_BOTTOM   = 7;
    const LEFT_CENTER   = 8;

    /**
     * Initialize the image attributes.
     * Also, set the actual thumbnail attributes obtained from the images.xml file.
     *
     * @param array $file
     * @param int $maxSize
     */
    public function __construct($file, $maxSize)
    {
        $this->file     = $file;
        $this->maxSize  = $maxSize;

        $this->init();

        $data = $this->prepare();

        if ($data !== false) {
            $this->image    = $data['image'];
            $this->width    = $data['width'];
            $this->height   = $data['height'];
        }
    }

    /**
     * Return the image width.
     *
     * @return mixed
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * Return the image height.
     *
     * @return mixed
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * Return the image type according to the images.xml file.
     *
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Return the file handler.
     *
     * @return mixed
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Return all available image extensions.
     *
     * @return array
     */
    public static function getExtensions()
    {
        return self::$imageExtensions;
    }

    /**
     * Return all available image content types.
     *
     * @return array
     */
    public static function getContentTypes()
    {
        return self::$contentTypes;
    }

    /**
     * Save the image file inside it's public image path.
     * Also set a session with the file name for future database processing.
     * If a type is specified and is found inside the images.xml file, the image will:
     * - be scaled at the best ratio according to the images.xml width and height
     * - be cropped to adjust the exact dimensions found in the images.xml file.
     * Also, an original image will be saved just for reference.
     *
     * @param int $quality
     * @param string $format
     * @param bool $progressive
     * @return bool
     */
    public function save($quality = 100, $format = 'jpeg', $progressive = false)
    {
        $this->errors($format);

        switch ($this->getType()) {
            case 'jpeg':
                $format = 'jpeg';
                break;
            case 'jpg':
                $format = 'jpeg';
                break;
            case 'gif':
                $format = 'gif';
                break;
            default:
                $format = 'png';
                break;
        }

        $function   = 'image' . $format;
        $fileName   = md5(String::random(25)) . '.' . $this->getType();
        $quality    = (int)$quality >= 0 ? (int)$quality : 0;
        $quality    = (int)$quality <= 100 ? (int)$quality : 100;

        while (File::exists(Dir::getOriginalImage() . $fileName)) {
            $fileName = md5(String::random(25)) . '.' . $this->getType();
        }

        switch ($format) {
            case 'png':
                $function($this->image, Dir::getOriginalImage() . $fileName);
                break;
            case 'gif':
                $function($this->image, Dir::getOriginalImage() . $fileName);
                break;
            default:
                $function($this->image, Dir::getOriginalImage() . $fileName, 100);
                break;
        }

        $this->scale(
            $this->specs['maxWidth'] ? $this->specs['maxWidth'] : 1920,
            $this->specs['maxHeight'] ? $this->specs['maxHeight'] : 1080
        );

        if ($this->specs['width'] && $this->specs['height']) {
            foreach ($this->specs['width'] as $thumb => $none) {
                $data = $this->prepare();

                if ($data !== false) {
                    $this->image    = $data['image'];
                    $this->width    = $data['width'];
                    $this->height   = $data['height'];
                }

                if ($this->specs['width'][$thumb] >= $this->specs['height'][$thumb]) {
                    $this->scale($this->specs['width'][$thumb] * 2, 0);
                } else {
                    $this->scale(0, $this->specs['height'][$thumb] * 2);
                }

                $this->crop($this->specs['width'][$thumb], $this->specs['height'][$thumb]);
                imageinterlace($this->image, $progressive ? 1 : 0);

                $function(
                    $this->image,
                    Dir::getImage() . str_replace('.' . $this->getType(), '_' . $thumb . '.' . $this->getType(), $fileName),
                    $format == 'png' || $format == 'gif' ? substr($quality - 10, 0, 1) : $quality
                );
            }
        } else {
            $function(
                $this->image,
                Dir::getImage() . str_replace('.' . $this->getType(), '_default.' . $this->getType(), $fileName),
                $format == 'png' || $format == 'gif' ? substr($quality - 10, 0, 1) : $quality
            );
        }

        if (!File::exists(Dir::getImage() . str_replace('.' . $this->getType(), '_default' . '.' . $this->getType(), $fileName))) {
            Flash::error('Image upload failed!');
            return false;
        }

        Session::set('upload-file', $fileName);
        return true;
    }

    /**
     * Identify the image's thumb if any, according to the images.xml file.
     *
     * @param $type
     * @return void
     */
    public static function thumb($type)
    {
        foreach (Config::getImages()->image as $image) {
            $attributes = $image->attributes();

            if ((string)$attributes['type'] == $type) {
                Session::set('image.maxWidth', Arr::first(explode('x', $image->max_size->attributes()['value'])));
                Session::set('image.maxHeight', Arr::last(explode('x', $image->max_size->attributes()['value'])));

                foreach ($image->thumbs as $thumbs) {
                    foreach ($thumbs as $name => $attributes) {
                        Session::set('image.width.' . $name, (int)$attributes['width']);
                        Session::set('image.height.' . $name, (int)$attributes['height']);
                    }
                }
            }
        }
    }

    /**
     * Display the image.
     *
     * @return void
     */
    public function show()
    {
        header('Content-type:image/png');
        imagepng($this->image);
        die;
    }

    /**
     * Scale the original image to the given width and height.
     * To make the image bigger then the original, the $force parameter must be set to true.
     *
     * @param int $width
     * @param int $height
     * @param bool $force
     * @return $this
     * @throws Exception
     */
    public function scale($width, $height, $force = false)
    {
        if (is_null($this->image)) {
            throw new Exception('File does not exist');
        }

        if (!$force && $this->width < $width && $this->height < $height) {
            return $this;
        }

        $oldRatio = $this->width / $this->height;

        if ($width === 0 && $height !== 0) {
            $width = $height * $oldRatio;
        } elseif ($width !== 0 && $height === 0) {
            $height = $width / $oldRatio;
        }

        $newRatio = $width / $height;

        if($oldRatio > $newRatio) {
            $trueWidth  = $width;
            $trueHeight = $width / $oldRatio;
        } else {
            $trueWidth  = $height * $oldRatio;
            $trueHeight = $height;
        }

        $image = imagecreatetruecolor($trueWidth, $trueHeight);

        imagesavealpha($image, true);
        imagefill($image, 0, 0, imagecolorallocatealpha($image, 0, 0, 0, 127));
        imagecopyresampled($image, $this->image, 0, 0, 0, 0, $trueWidth, $trueHeight, $this->width, $this->height);

        $this->image    = $image;
        $this->width    = $trueWidth;
        $this->height   = $trueHeight;

        return $this;
    }

    /**
     * Stretch the original image to the given width and height.
     * To make the image bigger then the original, the $force parameter must be set to true.
     *
     * @param int $width
     * @param int $height
     * @param bool $force
     * @return $this
     * @throws Exception
     */
    public function stretch($width, $height, $force = false)
    {
        if (is_null($this->image)) {
            throw new Exception('File does not exist');
        }

        if(!$force && $this->width < $width && $this->height < $height) {
            return $this;
        }

        $trueWidth  = $width;
        $trueHeight = $height;

        $image = imagecreatetruecolor($width, $height);

        imagesavealpha($image, true);
        imagefill($image, 0, 0, imagecolorallocatealpha($image, 0, 0, 0, 127));
        imagecopyresampled($image, $this->image, 0, 0, 0, 0, $width, $height, $this->width, $this->height);

        $this->image    = $image;
        $this->width    = $trueWidth;
        $this->height   = $trueHeight;

        return $this;
    }

    /**
     * Crop the original image to the given width and height.
     * To make the image bigger then the original, the $force parameter must be set to true.
     *
     * @param int $width
     * @param int $height
     * @param bool $force
     * @return $this
     * @throws Exception
     */
    public function crop($width, $height, $force = false)
    {
        if (is_null($this->image)) {
            throw new Exception('File does not exist');
        }

        if (!$force && ($this->width < $width || $this->height < $height)) {
            return $this->scale($width, $height);
        }

        $oldRatio = $this->width / $this->height;
        $newRatio = $width / $height;

        if ($oldRatio >= $newRatio) {
            $sourceWidth        = $this->height * $newRatio;
            $sourceHeight       = $this->height;
            $sourceHorizontal   = ($this->width - $sourceWidth) / 2;
            $sourceVertical     = 0;
        } else {
            $sourceWidth        = $this->width;
            $sourceHeight       = $this->width / $newRatio;
            $sourceHorizontal   = 0;
            $sourceVertical     = ($this->height - $sourceHeight) / 2;
        }

        $image = imagecreatetruecolor($width, $height);

        imagesavealpha($image, true);
        imagefill($image, 0, 0, imagecolorallocatealpha($image, 0, 0, 0, 127));
        imagecopyresampled($image, $this->image, 0, 0, $sourceHorizontal, $sourceVertical, $width, $height, $sourceWidth, $sourceHeight);

        $this->image    = $image;
        $this->width    = $width;
        $this->height   = $height;

        return $this;
    }

    /**
     * Transform an image by rotating it to the given angle.
     *
     * @param $angle
     * @return $this
     */
    public function rotate($angle)
    {
        if ($angle == 0) {
            return $this;
        }

        $this->image = imagerotate($this->image, 360 - $angle, imagecolorallocatealpha($this->image, 0, 0, 0, 127));

        return $this;
    }

    /**
     * Add a watermark on the image.
     * The watermark will be placed in the given position.
     * The default is the very center. For other position options please consult the constants.
     *
     * @param string $filename
     * @param int $position
     * @return $this
     * @throws Exception
     */
    public function watermark($filename, $position = self::CENTER_CENTER)
    {
        $attr = @getimagesize($filename);

        if ($attr === false) {
            throw new Exception('Unknown image type');
        }

        $mime   = explode('/', $attr['mime']);
        $method = 'imagecreatefrom' . $mime[1];
        $image  = $method($filename);

        imagesavealpha($image, true);

        $watermark = [
            'image'     => $image,
            'width'     => $attr[0],
            'height'    => $attr[1]
        ];

        if ($watermark === false) {
            return $this;
        }

        switch ($position) {
            case self::CENTER_CENTER:
                $x = round(($this->width - $watermark['width']) / 2);
                $y = round(($this->height - $watermark['height']) / 2);
                break;
            case self::LEFT_TOP:
                $x = 0;
                $y = 0;
                break;
            case self::CENTER_TOP:
                $x = round(($this->width - $watermark['width']) / 2);
                $y = 0;
                break;
            case self::RIGHT_TOP:
                $x = $this->width - $watermark['width'];
                $y = 0;
                break;
            case self::RIGHT_CENTER:
                $x = $this->width - $watermark['width'];
                $y = round(($this->height - $watermark['height']) / 2);
                break;
            case self::RIGHT_BOTTOM:
                $x = $this->width - $watermark['width'];
                $y = $this->height - $watermark['height'];
                break;
            case self::CENTER_BOTTOM:
                $x = round(($this->width - $watermark['width']) / 2);
                $y = $this->height - $watermark['height'];
                break;
            case self::LEFT_BOTTOM:
                $x = 0;
                $y = $this->height - $watermark['height'];
                break;
            case self::LEFT_CENTER:
                $x = 0;
                $y = round(($this->height - $watermark['height']) / 2);
                break;
        }

        imagecopy($this->image, $watermark['image'], $x, $y, 0, 0, $watermark['width'], $watermark['height']);
        imagedestroy($watermark['image']);

        return $this;
    }

    /**
     * Helper method for displaying the image file inside an admin edit view.
     *
     * @param string $image
     * @param string $thumb
     * @param array|null $options
     * @return string
     * @throws Exception
     */
    public static function display($image, $thumb = 'default', $options = null)
    {
        $optionString = '';

        if ($options) {
            if (!is_array($options)) {
                throw new Exception('The OPTIONS parameter must be a non-empty array');
            }

            foreach ($options as $key => $val) {
                $optionString .= $key . '="' . $val . '" ';
            }
        }

        $parts  = explode('.', $image);
        $source = $parts[0] . '_' . $thumb . '.' . $parts[1];

        return '<img src="' . Dir::getUploads() . '/image/' . $source . '" ' . $optionString . ' />';
    }

    /**
     * Initialize the file attributes.
     *
     * @return void
     */
    private function init()
    {
        $this->fileName     = is_array($this->file['name']) ? $this->file['name'][0] : $this->file['name'];
        $this->fileSize     = is_array($this->file['size']) ? $this->file['size'][0] : $this->file['size'];
        $this->fileTmpName  = is_array($this->file['tmp_name']) ? $this->file['tmp_name'][0] : $this->file['tmp_name'];
    }

    /**
     * Prepare the image object, as well as it's width and height.
     *
     * @return array
     * @throws Exception
     */
    private function prepare()
    {
        $attr = @getimagesize($this->fileTmpName);

        if ($attr === false) {
            throw new Exception('Unknown image type');
        }

        $mime   = explode('/', $attr['mime']);
        $method = 'imagecreatefrom' . $mime[1];
        $image  = $method($this->fileTmpName);

        $this->type = strtolower($mime[1]);

        imagesavealpha($image, true);

        return [
            'image'     => $image,
            'width'     => $attr[0],
            'height'    => $attr[1]
        ];
    }

    /**
     * Handle possible generic errors.
     *
     * @param string $format
     * @return bool
     */
    private function errors($format)
    {
        if(is_null($this->image)) {
            Flash::error('File does not exist! Please Upload a file.');
            Redirect::url(Url::getPreviousUrl());
        }

        if (!in_array(strtolower($format), self::$imageExtensions)) {
            Flash::error('Invalid image format ' . $format . '. Choose one of: ' . implode(', ', self::$imageExtensions));
            Redirect::url(Url::getPreviousUrl());
        }

        if ($this->fileSize > ($this->maxSize*1024*1024)) {
            Flash::error('The file is too large! Please try with a smaller file.');
            Redirect::url(Url::getPreviousUrl());
        }

        $this->specs = Session::get('image');

        if ($this->specs['width'] && (max($this->specs['width']) > $this->width)) {
            Flash::error('Please upload an image with the width of at least ' . max($this->specs['width']) . 'px.');
            Redirect::url(Url::getPreviousUrl());
        }

        if ($this->specs['height'] && (max($this->specs['height']) > $this->height)) {
            Flash::error('Please upload an image with the height of at least ' . max($this->specs['height']) . 'px.');
            Redirect::url(Url::getPreviousUrl());
        }
    }
}