<?php

class Pagination
{
    /**
     * The collection of items to be paginated.
     *
     * @var
     */
    private $items;

    /**
     * Limit identifier.
     *
     * @var
     */
    private $limit;

    /**
     * Initialize the pagination according to the items and limit specified.
     *
     * @param Model $model
     * @param int|null $limit
     * @return array
     */
    public function initPagination(Model $model, $limit = null)
    {
        $this->items = $count = Database::getInstance()->query($model->getSelect())->count();
        $this->limit = $limit ?: 999999999;

        $page = Get::exists('page') ? ($_GET['page'] > ceil($count / $limit) ? ceil($count / $limit) : (int)$_GET['page']) : 1;
        $model->limit($this->limit, ($page - 1) * $this->limit);

        return $model->getCollection();
    }

    /**
     * Display pagination logic.
     * Show 3 pages, alongside with the first, last, next and previous buttons.
     *
     * @return string
     */
    public function paginate()
    {
        $output = '';
        $page   = (Get::exists('page') && Get::get('page') != 0) ? Get::get('page') : 1;
        $count  = ceil($this->items / $this->limit);

        parse_str(Server::get('QUERY_STRING'), $opt);
        unset($opt['page']);

        $url = '?' . (http_build_query($opt) != '' ? http_build_query($opt) . '&' : '');

        for ($i = 1; $i <= $count; $i++) {
            if ($page > $count) {
                $page = $count;
            }

            if ($page == 1 && $page == $count) {
                $output = '';
            } elseif ($page == 1) {
                if (!empty($opt)) {
                    $output = '
                        <span class="paginate-first-disabled"><<</span>
                        <span class="paginate-prev-disabled"><</span>
                        <span class="paginate-current">' . $page . '</span>
                        ' . (($this->items > $this->limit * $page) ? '<a href="' . $url . 'page=' . ($page+1) . '" class="paginate">' . ($page+1) . '</a>' : '') . '
                        ' . (($this->items > $this->limit * ($page * 2)) ? '<a href="' . $url . 'page=' . ($page+2) . '" class="paginate">' . ($page+2) . '</a>' : '') . '
                        ' . (($this->items > $this->limit * $page) ? '<a href="' . $url . 'page=' . ($page+1) . '" class="paginate-next">></a>' : '') . '
                        ' . (($this->items > $this->limit * $page) ? '<a href="' . $url . 'page=' . ($count) . '" class="paginate-last">>></a>' : '');
                } else {
                    $output = '
                        <span class="paginate-first-disabled"><<</span>
                        <span class="paginate-prev-disabled"><</span>
                        <span class="paginate-current">' . $page . '</span>
                        ' . (($this->items > $this->limit * $page) ? '<a href="' . $url . 'page=' . ($page+1) . '" class="paginate">' . ($page+1) . '</a>' : '') . '
                        ' . (($this->items > $this->limit * ($page * 2)) ? '<a href="' . $url . 'page=' . ($page+2) . '" class="paginate">' . ($page+2) . '</a>' : '') . '
                        ' . (($this->items > $this->limit * $page) ? '<a href="' . $url . 'page=' . ($page+1) . '" class="paginate-next">></a>' : '') . '
                        ' . (($this->items > $this->limit * $page) ? '<a href="' . $url . 'page=' . ($count) . '" class="paginate-last">>></a>' : '');
                }
            } elseif ($page == $count) {
                if (!empty($opt)) {
                    $output = '
                        ' . (($this->items > $this->limit) ? '<a href="' . $url . 'page=1" class="paginate-first"><<</a>' : '') . '
                        ' . (($this->items > $this->limit) ? '<a href="' . $url . 'page=' . ($page-1) . '" class="paginate-prev"><</a>' : '') . '
                        ' . (($this->items > $this->limit * 2) ? '<a href="' . $url . 'page=' . ($page-2) . '" class="paginate">' . ($page-2) . '</a>' : '') . '
                        ' . (($this->items > $this->limit) ? '<a href="' . $url . 'page=' . ($page-1) . '" class="paginate">' . ($page-1) . '</a>' : '') . '
                        <span class="paginate-current">' . $page . '</span>
                        <span class="paginate-next-disabled">></span>
                        <span class="paginate-last-disabled">>></span>';
                } else {
                    $output = '
                        ' . (($this->items > $this->limit) ? '<a href="' . $url . 'page=1" class="paginate-first"><<</a>' : '') . '
                        ' . (($this->items > $this->limit) ? '<a href="' . $url . 'page=' . ($page-1) . '" class="paginate-prev"><</a>' : '') . '
                        ' . (($this->items > $this->limit * 2) ? '<a href="' . $url . 'page=' . ($page-2) . '" class="paginate">' . ($page-2) . '</a>' : '') . '
                        ' . (($this->items > $this->limit) ? '<a href="' . $url . 'page=' . ($page-1) . '" class="paginate">' . ($page-1) . '</a>' : '') . '
                        <span class="paginate-current">' . $page . '</span>
                        <span class="paginate-next-disabled">></span>
                        <span class="paginate-last-disabled">>></span>';
                }
            } else {
                if (!empty($opt)) {
                    $output = '
                        <a href="' . $url . 'page=1" class="paginate-first"><<</a>
                        <a href="' . $url . 'page=' . ($page-1) . '" class="paginate-prev"><</a>
                        <a href="' . $url . 'page=' . ($page-1) . '" class="paginate">' . ($page-1) . '</a>
                        <span class="paginate-current">' . $page . '</span>
                        <a href="' . $url . 'page=' . ($page+1) . '" class="paginate">' . ($page+1) . '</a>
                        <a href="' . $url . 'page=' . ($page+1) . '" class="paginate-next">></a>
                        <a href="' . $url . 'page=' . ($count) . '" class="paginate-last">>></a>';
                } else {
                    $output = '
                        <a href="' . $url . 'page=1" class="paginate-first"><<</a>
                        <a href="' . $url . 'page=' . ($page-1) . '" class="paginate-prev"><</a>
                        <a href="' . $url . 'page=' . ($page-1) . '" class="paginate">' . ($page-1) . '</a>
                        <span class="paginate-current">' . $page . '</span>
                        <a href="' . $url . 'page=' . ($page+1) . '" class="paginate">' . ($page+1) . '</a>
                        <a href="' . $url . 'page=' . ($page+1) . '" class="paginate-next">></a>
                        <a href="' . $url . 'page=' . ($count) . '" class="paginate-last">>></a>';
                }
            }
        }

        return $output;
    }
}