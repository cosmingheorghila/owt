<?php

class Migration
{
    /**
     * The name of the migration file.
     *
     * @var
     */
    public $file;

    /**
     * The migrations database handler.
     *
     * @var Database
     */
    public $database;

    /**
     * The empty database handler.
     *
     * @var Database
     */
    public $adapter;

    /**
     * The existing migrations.
     *
     * @var
     */
    public $migrations;

    /**
     * The current migration version timestamp.
     *
     * @var
     */
    public $version;

    /**
     * Set the database handlers.
     *
     * @set $database
     */
    public function __construct()
    {
        $this->database = new Database('cms_migrations');
        $this->adapter  = new Database();
    }

    /**
     * Create a migration file.
     * The file name is prepended with the current timestamp.
     *
     * @throws Exception
     */
    public function create()
    {
        if (!Get::exists('file')) {
            throw new Exception('Expected GET parameter: file');
        }

        $this->file = time() . '_' . Get::get('file') . '.php';
        $content    = "<?php

class Migration_" . array_shift(explode('_', $this->file)) . " extends Database
{
     /**
     * @return void
     */
    public function up()
    {

    }

    /**
     * @return void
     */
    public function down()
    {

    }
}";

        if (File::create(Dir::getMigrations() . '/' . $this->file, $content) !== false) {
            echo 'BEGIN<br /><br />';
            echo "Created file: <strong>" . $this->file . "</strong>";
            echo '<br /><br />FINISH';
        }
    }

    /**
     * Run all the migrations until a give time.
     * Before running, make an sql backup for security purposes.
     *
     * @throws Exception
     * @return void
     */
    public function run()
    {
        header('Content-type: text/html; charset=utf-8');

        try {
            $host       = Config::get('database')->host;
            $dbName     = Config::get('database')->db_name;
            $dbUser     = Config::get('database')->db_user;
            $dbPassword = Config::get('database')->db_password;

            $database = new PDO('mysql:host=' . $host . ';dbname=' . $dbName, $dbUser, $dbPassword);

            try {
                $database->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                $database->exec("CREATE TABLE IF NOT EXISTS cms_migrations(id INT(11) NOT NULL,timestamp INT(11) NOT NULL DEFAULT '0')");

                if ($database->query("SELECT * FROM cms_migrations WHERE id = '1'")->rowCount() == 0) {
                    $database->exec("INSERT INTO cms_migrations (`id`, `timestamp`) VALUES ('1', '0')");
                }
            } catch (PDOException $e) {
                echo $e->getMessage() . '<br /><br />';
            }
        } catch (PDOException $e) {
            throw new Exception($e->getMessage());
        }

        $this->backup();

        $this->version      = $this->database->load(1)->getTimestamp();
        $this->migrations   = [];

        foreach (glob(dirname(dirname(__DIR__)) . '/database/migrations/*.php') as $file) {
            $file       = array_pop(explode('/migrations/', $file));
            $version    = array_shift(explode('_', $file));

            if ($this->version < $version) {
                $this->migrations[] = $file;
            }
        }

        $count = 1;

        echo "BEGIN!<br />";

        if (!empty($this->migrations)) {
            foreach ($this->migrations as $migration) {
                echo "<br /><br />EXECUTING MIGRATION " . array_shift(explode('_', $migration)) . "<br />-----------------------------------------------<br />";
                require_once(Dir::getMigrations() . '/' . $migration);

                $migrationClass = 'Migration_' . array_shift(explode('_', $migration));

                $class = new $migrationClass();
                $class->up();

                if ($count == count($this->migrations)) {
                    $this->version = array_shift(explode('_', $migration));
                }

                $count++;
            }
        } else {
            echo "<br />Nothing to do...";
        }

        echo '<br /><br />FINISH!';

        $this->database->update(['timestamp' => $this->version])->where(['id' => 1])->save();
    }

    /**
     * Rollback migrations until a given timestamp.
     *
     * @throws Exception
     * @return void
     */
    public function rollback()
    {
        if (!Get::exists('version')) {
            throw new Exception('Expected GET parameter: version');
        }

        $this->backup();

        $this->version  = Get::get('version');
        $currentVersion = $this->database->load(1)->getTimestamp();

        foreach (array_reverse(glob(dirname(dirname(__DIR__)) . '/database/migrations/*.php')) as $file) {
            $file       = array_pop(explode('/migrations/', $file));
            $version    = array_shift(explode('_', $file));

            if ($this->version < $version && $currentVersion >= $version) {
                $this->migrations[] = $file;
            }
        }

        echo "BEGIN!<br />";

        if (!empty($this->migrations)) {
            foreach ($this->migrations as $migration) {
                echo "<br /><br />EXECUTING MIGRATION ROLLBACK " . array_shift(explode('_', $migration)) . "<br />---------------------------------------------------------------<br />";

                require_once(Dir::getMigrations() . '/' . $migration);

                $migrationClass = 'Migration_' . array_shift(explode('_', $migration));

                $class = new $migrationClass();
                $class->down();
            }
        } else {
            echo "<br /><br />Nothing to do...";
        }

        echo '<br /><br />FINISH!';

        $this->database->update(['timestamp' => $this->version])->where(['id' => 1])->save();
    }

    /**
     * Generate an sql backup containing the entire current database structure and contents.
     *
     * @return void
     */
    public function backup()
    {
        $host   = (string)Config::get('database')->host;
        $dbName = (string)Config::get('database')->db_name;
        $dbUser = (string)Config::get('database')->db_user;
        $dbPass = (string)Config::get('database')->db_password;

        $file = 'mysql___' . date('d-m-Y_H-i', time()) . '.sql';
        $path = Dir::getBackups() . '/' . $file;

        exec('/usr/bin/mysqldump --opt -u ' . $dbUser . ' -h ' . $host . ' -p' . $dbPass. ' ' . $dbName . '> '. $path);
    }
}