<?php

class PDF
{
    /**
     * Set the PDF handler.
     *
     * @set client
     */
    public function __construct()
    {
        $this->client = new \Anouar\Fpdf\Fpdf();
    }

    /**
     * Open the PDF.
     *
     * @return void
     */
    public function open()
    {
        $this->client->open();
    }

    /**
     * Close the PDF.
     *
     * @return void
     */
    public function close()
    {
        $this->client->close();
    }

    /**
     * Add a page to the current PDF document.
     *
     * @param string $orientation
     * @param string $size
     */
    public function addPage($orientation = '', $size = '')
    {
        $this->client->AddPage($orientation, $size);
    }

    /**
     * Get the number of pages of the current PDF document.
     *
     * return int
     */
    public function getPageNumber()
    {
        $this->client->PageNo();
    }

    /**
     * Write some text inside the PDF file.
     * $x and $y are used for text positioning inside the PDF document.
     *
     * @param int $x
     * @param int $y
     * @param string $text
     */
    public function text($x, $y, $text = '')
    {
        $this->client->Text($x, $y, $text);
    }

    /**
     * Write a link inside the PDF file.
     * $x, $y, $w and $h are used for text positioning inside the PDF document.
     *
     * @param string $link
     * @param string $destination
     * @param int $x
     * @param int $y
     * @param int $w
     * @param int $h
     */
    public function link($link, $destination, $x, $y, $w, $h)
    {
        $this->client->AddLink();
        $this->client->SetLink($destination);
        $this->client->Link($x, $y, $w, $h, $link);
    }

    /**
     * Write a text or link inside the PDF document.
     *
     * @param int $h
     * @param string $text
     * @param string $link
     */
    public function write($h, $text, $link = '')
    {
        $this->client->Write($h, $text, $link);
    }

    /**
     * Insert an image inside the PDF document.
     * $x, $y, $w and $h are used for text positioning inside the PDF document.
     *
     * @param string $file
     * @param int|null $x
     * @param int|null $y
     * @param int $w
     * @param int $h
     * @param string $type
     * @param string $link
     */
    public function image($file, $x = null, $y = null, $w = 0, $h = 0, $type = '', $link = '')
    {
        $this->client->Image($file, $x, $y, $w, $h, $type, $link);
    }

    /**
     * Output the PDF document.
     *
     * @return void
     */
    public function show()
    {
        $this->client->Output();
    }

    /**
     * Save the PDF document.
     *
     * @param string $name
     */
    public function save($name)
    {
        $this->client->Output(Dir::getPdf() . $name, 'F');
    }

    /**
     * Download the PDF document.
     *
     * @param string $name
     */
    public function download($name)
    {
        $this->client->Output($name, 'D');
    }

    /**
     * Set the text font for a PDF document.
     *
     * @param string $family
     * @param int $size
     * @param string $style
     */
    public function font($family, $size = 0, $style = '')
    {
        $this->client->SetFont($family, $style, $size);
    }

    /**
     * Set the title for a PDF document.
     *
     * @param string $title
     * @param bool $isUTF8
     */
    public function title($title, $isUTF8 = false)
    {
        $this->client->SetTitle($title, $isUTF8);
    }

    /**
     * Set the subject for a PDF document.
     *
     * @param string $subject
     * @param bool $isUTF8
     */
    public function subject($subject, $isUTF8 = false)
    {
        $this->client->SetSubject($subject, $isUTF8);
    }

    /**
     * Set the author for a PDF document.
     *
     * @param string $author
     * @param bool $isUFT8
     */
    public function author($author, $isUFT8 = false)
    {
        $this->client->SetAuthor($author, $isUFT8);
    }

    /**
     * Set the keywords for a PDF document.
     *
     * @param string $keywords
     * @param bool $isUTF8
     */
    public function keywords($keywords, $isUTF8 = false)
    {
        $this->client->SetKeywords($keywords, $isUTF8);
    }

    /**
     * Set the creator for a PDF document.
     *
     * @param string $creator
     * @param bool $isUFT8
     */
    public function creator($creator, $isUFT8 = false)
    {
        $this->client->SetCreator($creator, $isUFT8);
    }

    /**
     * Set the margins for a PDF document.
     *
     * @param int $top
     * @param int|null $left
     * @param int|null $right
     */
    public function margins($top, $left = null, $right = null)
    {
        $this->client->SetMargins($left, $top, $right);
    }

    /**
     * Set the display mode for a PDF document.
     *
     * @param string $zoom
     * @param string $layout
     * @throws Exception
     */
    public function displayMode($zoom, $layout = 'default')
    {
        $this->client->SetDisplayMode($zoom, $layout);
    }

    /**
     * Set the compression type of a PDF document.
     *
     * @param $compress
     */
    public function compress($compress)
    {
        $this->client->SetCompresion($compress);
    }

    /**
     * Set the text color for a PDF document.
     *
     * @param int $r
     * @param int|null $g
     * @param int|null $b
     */
    public function textColor($r, $g = null, $b = null)
    {
        $this->client->SetTextColor($r, $g, $b);
    }

    /**
     * Set the drawing color for a PDF document.
     *
     * @param int $r
     * @param int|null $g
     * @param int|null $b
     */
    public function drawColor($r, $g = null, $b = null)
    {
        $this->client->SetDrawColor($r, $g, $b);
    }

    /**
     * Set the filling color for a PDF document.
     *
     * @param int $r
     * @param int|null $g
     * @param int|null $b
     */
    public function fillColor($r, $g = null, $b = null)
    {
        $this->client->SetFillColor($r, $g, $b);
    }

    /**
     * Draw a line inside a PDF document.
     * $x1, $y1, $x2 and $y2 are used as points for drawing the line.
     *
     * @param int $x1
     * @param int $y1
     * @param int $x2
     * @param int $y2
     */
    public function drawLine($x1, $y1, $x2, $y2)
    {
        $this->client->Line($x1, $y1, $x2, $y2);
    }

    /**
     * Draw a rectangle inside a PDF document.
     * $x1, $y1, $w and $h are used as points for drawing the line.
     *
     * @param int $x
     * @param int $y
     * @param int $w
     * @param int $h
     * @param string $style
     */
    public function drawRectangle($x, $y, $w, $h, $style='')
    {
        $this->client->Rect($x, $y, $w, $h, $style);
    }
}