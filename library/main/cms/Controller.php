<?php

class Cms_Controller extends Controller
{
    /**
     * The layout handler.
     * A loaded model instance of the Cms_Model_Layout model.
     *
     * @var null
     */
    public $layout = null;

    /**
     * The page handler.
     * A loaded model instance of the Cms_Model_Page model.
     *
     * @var null
     */
    public $page = null;

    /**
     * The user handler.
     * A loaded model instance of the Users_Model_User model.
     *
     * @var null
     */
    public $user = null;

    /**
     * Set the page object if any.
     * Set the layout object if any.
     * Set the user object if any.
     * Set the cart object if any.
     *
     * These variables will then be used inside child controllers.
     * Also, these variables can be assigned to views.
     *
     * @return void
     */
    public function init()
    {
        if ($page = $this->_isPage()) {
            $this->_setPage($page);
        } elseif ($layout = $this->_isLayout()) {
            $this->_setLayout($layout);
        }

        $this->_setUser();
        $this->_setCart();
    }

    /**
     * Verify logged user helper.
     *
     * @return void
     */
    protected function verifyUser()
    {
        if (!Users_Model_User::isLogged()) {
            $this->flashError('Please login to see the page!');
            $this->redirectUrl(Url::getPreviousUrl());
        }
    }

    /**
     * Check if current route is a page.
     *
     * @return bool
     */
    private function _isPage()
    {
        return $this->getParam('id') ? (Cms_Model_Page::getInstance()->load($this->getParam('id')) ?: false) : false;
    }

    /**
     * Check if current route has a layout of it's own.
     *
     * @return bool
     */
    private function _isLayout()
    {
        return $this->_layoutPath ?: false;
    }

    /**
     * Set the page, if any.
     *
     * @param Cms_Model_Page|bool $page
     * @return void
     */
    private function _setPage($page)
    {
        $layout = Cms_Model_Layout::getInstance()->load($page->getLayoutId());

        $this->layout   = $layout;
        $this->page     = $page;

        $this->assign('layout', $layout);
        $this->assign('page', $page);
    }

    /**
     * Set the layout, if any.
     *
     * @param string $path
     * @return void
     */
    private function _setLayout($path)
    {
        $layout = Cms_Model_Layout::getInstance()->load([
            'file' => Arr::last(explode('/', $path))]
        );

        $this->layout = $layout;

        $this->assign('layout', $layout);
    }

    /**
     * Set the user, if any.
     *
     * @return void
     */
    private function _setUser()
    {
        if (Get::exists('logout')) {
            Users_Model_User::logout();
            $this->redirectUrl(Url::getHomeUrl());
        }

        $this->user = Users_Model_User::getUser();

        $this->assign('user', $this->user);
    }

    /**
     * set the cart, if any.
     *
     * @return void
     */
    public function _setCart()
    {
        $this->assign('cart', Shop_Model_Cart::getInstance()->getCart());
    }
}