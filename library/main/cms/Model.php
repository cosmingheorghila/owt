<?php

class Cms_Model extends Model
{
    /**
     * The database table a child model will use.
     *
     * @var
     */
    protected $table;

    /**
     * The loaded model instance item.
     *
     * @var
     */
    protected $item;

    /**
     * The behavior identifier.
     * Used as property of type array in child model classes.
     *
     * @var
     */
    protected $actsAs;

    /**
     * The validation relation identifier.
     * Used as property of type array in child model classes.
     *
     * @var
     */
    protected $validates;

    /**
     * Manage the add entity logic.
     *
     * @param array $data
     * @return bool
     */
    public function addEntity($data)
    {
        return parent::merge('insert', $data, null);
    }

    /**
     * Manage the edit entity logic.
     *
     * @param array $data
     * @return bool
     */
    public function editEntity($data)
    {
        return parent::merge('update', $data, [
            'id' => Post::get('identifier')
        ]);
    }

    /**
     * Manage the entity deletion logic.
     *
     * @return bool
     */
    public function deleteEntity()
    {
        if (Request::isGet() && Get::exists('delete-id') && Get::exists('delete-entity')) {
            if (Database::getInstance(Get::get('delete-entity'))->delete()->where(['id' => Get::get('delete-id')])->save()) {
                Flash::success('Record deleted successfully!');
                Redirect::url(Url::getPreviousUrl());
            } else {
                Flash::error('Could not delete the entry!');
            }
        }
    }

    /**
     * Basic logic to search inside a model's row sets.
     * Search of type including.
     *
     * @param string $keyword
     * @param array $columns
     * @param string $operator
     * @return $this
     */
    public function search($keyword, $columns, $operator = 'OR')
    {
        $conditions = '';

        if (is_array($columns) && !empty($columns)) {
            $conditions = implode(" {$operator} ", array_map(function ($key) use ($keyword) {
                return sprintf("%s LIKE %s", $key, "'%" . $this->escape($keyword) . "%'");
            }, $this->escape(array_values($columns))));
        }

        $this->sql .= (String::contains($this->sql, 'WHERE') ? " AND " : " WHERE ") . "({$conditions})";

        return $this;
    }

    /**
     * Advanced logic to search inside a model's row sets.
     * Based on an array, the logic matches the keys with the values and returns the record set.
     * Search of type excluding.
     *
     * @param array $data
     * @param string $operator
     * @return $this
     */
    public function searchAdvanced($data, $operator = 'AND')
    {
        $this->sql .= " WHERE ";
        $count = 1;

        foreach ($data as $column => $value) {
            $this->sql .= $column . " LIKE '%" . $this->escape($value) . "%'" . (
                $count == count($data) ?: " " . strtoupper($operator) . " "
            );

            $count++;
        }

        return $this;
    }

    /**
     * Full text search logic.
     *
     * @param string $keyword
     * @param string $column
     * @return $this
     */
    public function searchFullText($keyword, $column)
    {
        $this->query("ALTER TABLE " . $this->getTable() . " ADD FULLTEXT(" . $column . ")");
        $this->sql = "SELECT *, MATCH(" . $column . ") AGAINST ('" . $this->escape($keyword) . "') AS relevant FROM " . $this->getTable() . " WHERE MATCH(" . $column . ") AGAINST ('" . $this->escape($keyword) . "') ORDER BY relevant DESC";

        return $this;
    }

    /**
     * Helper for verifying a valid admin user.
     *
     * @return bool
     */
    public function verifyValidUser()
    {
        if (!Session::exists('admin-user')) {
            Session::set('redirect-url', Url::getUrl());
            Redirect::url(Url::getAdminUrl());
        }

        return true;
    }

    /**
     * Helper for verifying the admin user's permissions.
     *
     * @return bool
     */
    public function verifyAccessLevel()
    {
        if (!Admin_Access::checkAccessLevel($this->table)) {
            Flash::error('Access denied!');
            Redirect::url(Url::getAdminUrl());
        }

        return true;
    }

    /**
     * Helper for verifying the correct single item of the entity.
     *
     * @return bool|mixed
     */
    public function verifyExistentItem()
    {
        switch ($this->getClass()) {
            case 'Admin_Model_Analytic':
                Post::set('identifier', 1);

                $this->item = $this->load(1);
                $redirect   = Url::getAdminUrl();

                break;
            default:
                $this->item = $this->load((int)Get::get('id'));
                $redirect   = preg_replace('/edit\?id=\d+\w+/', 'index', Server::get('REQUEST_URI'));

                break;
        }

        if (!$this->item) {
            Flash::error('There is no record with that ID!');
            Redirect::url($redirect);
        }

        return $this->item;
    }

    /**
     * Get a single records according to the specified where conditions.
     *
     * @param array $where
     * @return mixed
     */
    public function getRecord($where)
    {
        return parent::load($where, $this->class);
    }

    /**
     * Get all records according to the specified where conditions.
     * This method can also search through records.
     * Ultimately, this method includes individual sorting conditions of any admin list entity.
     *
     * @param array $searchFields
     * @param array|string|null $where
     * @param string $orderField
     * @param string $orderOrder
     * @return PDOStatement
     */
    public function getRecords($searchFields = null, $where = null, $orderField = 'id', $orderOrder = 'asc')
    {
        if ($where) {
            $this->where($where);
        }

        if (Request::isGet() && (Get::exists('data.search') || Get::exists('data.sort') || Get::exists('data.order'))) {
            if (Get::exists('data.search')) {
                $this->search(Get::get('data.search'), $searchFields);
            }

            //page visible filter
            if (Get::exists('data.page_visible.0')) {
                $this->where([
                    'visible' => Get::get('data.page_visible.0')
                ]);
            }

            //block types filter
            if (Get::exists('data.types.0')) {
                $this->where([
                    'type' => Get::get('data.types.0')
                ]);
            }

            //admin users super filter
            if (Get::exists('data.admin_user_super.0')) {
                $this->where([
                    'super_admin' => Get::get('data.admin_user_super.0')
                ]);
            }

            //users type filter
            if (Get::exists('data.users_user_type.0')) {
                $this->where([
                    'type' => Get::get('data.users_user_type.0')
                ]);
            }

            //blog author filter
            if (Get::exists('data.blog_posts_author.0')) {
                $this->where([
                    'author_id' => Get::get('data.blog_posts_author.0')
                ]);
            }

            //blog category filter
            if (Get::exists('data.blog_posts_category.0')) {
                $this->where(
                    "id IN (SELECT post_id FROM blog_posts_categories_ring WHERE category_id = '" . Get::get('data.blog_posts_category.0') . "')"
                );
            }

            //blog post filter
            if (Get::exists('data.blog_posts_post.0')) {
                $this->where([
                    'post_id' => Get::get('data.blog_posts_post.0')
                ]);
            }

            //blog post flag filter
            if (Get::exists('data.blog_posts_flag.0')) {
                $this->where([
                    'flagged' => Get::get('data.blog_posts_flag.0')
                ]);
            }

            //shop orders status filter
            if (Get::exists('data.shop_orders_status.0')) {
                $this->where([
                    'status' => Get::get('data.shop_orders_status.0')
                ]);
            }

            //shop orders viewed filter
            if (Get::exists('data.shop_orders_viewed.0')) {
                $this->where([
                    'viewed' => Get::get('data.shop_orders_viewed.0')
                ]);
            }

            //shop type filter
            if (Get::exists('data.shop_product_types.0')) {
                $this->where([
                    'type_id' => Get::get('data.shop_product_types.0')
                ]);
            }

            //shop tax applicability filter
            if (Get::exists('data.shop_taxes_applicability.0')) {
                $this->where([
                    'applicability' => Get::get('data.shop_taxes_applicability.0')
                ]);
            }

            //shop tax type filter
            if (Get::exists('data.shop_taxes_type.0')) {
                $this->where([
                    'type' => Get::get('data.shop_taxes_type.0')
                ]);
            }

            //shop tax active filter
            if (Get::exists('data.shop_taxes_active.0')) {
                $this->where([
                    'active' => Get::get('data.shop_taxes_active.0')
                ]);
            }

            //shop discount type filter
            if (Get::exists('data.shop_discounts_type.0')) {
                $this->where([
                    'type' => Get::get('data.shop_discounts_type.0')
                ]);
            }

            //shop discount active filter
            if (Get::exists('data.shop_discounts_active.0')) {
                $this->where([
                    'active' => Get::get('data.shop_discounts_active.0')
                ]);
            }

            //shop discount applicability filter
            if (Get::exists('data.shop_discounts_applicability.0')) {
                $this->where([
                    'applicability' => Get::get('data.shop_discounts_applicability.0')
                ]);
            }

            //shop voucher type filter
            if (Get::exists('data.shop_vouchers_type.0')) {
                $this->where([
                    'type' => Get::get('data.shop_vouchers_type.0')
                ]);
            }

            //shop voucher active filter
            if (Get::exists('data.shop_vouchers_active.0')) {
                $this->where([
                    'active' => Get::get('data.shop_vouchers_active.0')
                ]);
            }

            //shop attributes set filter
            if (Get::exists('data.shop_attributes_set.0')) {
                $this->where([
                    'set_id' => Get::get('data.shop_attributes_set.0')
                ]);
            }

            //shop attributes filterable filter
            if (Get::exists('data.shop_attributes_filterable.0')) {
                $this->where([
                    'filterable' => Get::get('data.shop_attributes_filterable.0')
                ]);
            }

            //shop attributes type filter
            if (Get::exists('data.shop_attributes_type.0')) {
                $this->where([
                    'type' => Get::get('data.shop_attributes_type.0')
                ]);
            }

            //shop products category filter
            if (Get::exists('data.shop_products_category.0')) {
                $this->where("id IN (SELECT product_id FROM shop_products_categories_ring WHERE category_id = '" . Get::get('data.shop_products_category.0') . "')");
            }

            //shop products vendor filter
            if (Get::exists('data.shop_products_vendor.0')) {
                $this->where("id IN (SELECT product_id FROM shop_products_vendors_ring WHERE vendor_id = '" . Get::get('data.shop_products_vendor.0') . "')");
            }

            //shop products active filter
            if (Get::exists('data.shop_products_active.0')) {
                $this->where([
                    'active' => Get::get('data.shop_products_active.0')
                ]);
            }

            //shop products stock filter
            if (Get::exists('data.shop_products_stock.0')) {
                $this->where([
                    'virtual_stock' => Get::get('data.shop_products_stock.0')
                ]);
            }

            //car brand filter
            if (Get::exists('data.car_brand.0')) {
                $this->where([
                    'brand_id' => Get::get('data.car_brand.0')
                ]);
            }

            //car model filter
            if (Get::exists('data.car_model.0')) {
                $this->where([
                    'model_id' => Get::get('data.car_model.0')
                ]);
            }

            //car brand filter
            if (Get::exists('data.car_stats_important.0')) {
                $this->where([
                    'important' => Get::get('data.car_stats_important.0')
                ]);
            }

            //ads active filter
            if (Get::exists('data.car_ads_active.0')) {
                $this->where([
                    'active' => Get::get('data.car_ads_active.0')
                ]);
            }

            //ads converted filter
            if (Get::exists('data.car_ads_converted.0')) {
                $this->where([
                    'converted' => Get::get('data.car_ads_converted.0')
                ]);
            }

            //ads brand filter
            if (Get::exists('data.car_ads_brand.0')) {
                $this->where([
                    'brand_id' => Get::get('data.car_ads_brand.0')
                ]);
            }

            //ads model filter
            if (Get::exists('data.car_ads_model.0')) {
                $this->where([
                    'model_id' => Get::get('data.car_ads_model.0')
                ]);
            }

            //counties country filter
            if (Get::exists('data.counties_countries.0')) {
                $this->where([
                    'country_id' => Get::get('data.counties_countries.0')
                ]);
            }

            //cities county filter
            if (Get::exists('data.cities_counties.0')) {
                $this->where([
                    'county_id' => Get::get('data.cities_counties.0')
                ]);
            }

            //car orders status filter
            if (Get::exists('data.car_orders_status.0')) {
                $this->where([
                    'status' => Get::get('data.car_orders_status.0')
                ]);
            }

            //car orders active filter
            if (Get::exists('data.car_orders_active.0')) {
                $this->where([
                    'active' => Get::get('data.car_orders_active.0')
                ]);
            }

            //car orders payment filter
            if (Get::exists('data.car_orders_payment.0')) {
                $this->where([
                    'payment' => Get::get('data.car_orders_payment.0')
                ]);
            }

            //car followers filter
            if (Get::exists('data.car_followers_car.0')) {
                $this->where([
                    'car_id' => Get::get('data.car_followers_car.0')
                ]);
            }

            $this->order(Get::get('data.sort.0'), Get::get('data.order.0'));
        } else {
            $this->order($orderField, $orderOrder);
        }

        return $this;
    }
}