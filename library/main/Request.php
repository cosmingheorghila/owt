<?php

class Request
{
    /**
     * The database table which the post works with.
     *
     * @var
     */
    protected $table;

    /**
     * The database handler.
     *
     * @var Database
     */
    protected $database;

    /**
     * The loaded model instance used for behaviors operations.
     *
     * @var
     */
    private $model;

    /**
     * The loaded model instance used for unique checks.
     *
     * @var
     */
    private $row;

    /**
     * The identifier returned in case of an insert operation.
     *
     * @var
     */
    private $id;

    /**
     * The sql query used for future operations.
     *
     * @var
     */
    private $sql;

    /**
     * The name of the post operation.
     * It can be either insert or update.
     *
     * @var
     */
    private $operation;

    /**
     * The data prepared for future operations.
     *
     * @var
     */
    private $data;

    /**
     * The where clause for an update operation.
     *
     * @var
     */
    private $where;

    /**
     * The validation placeholder.
     *
     * @var
     */
    private $validation;

    /**
     * The unique placeholder.
     *
     * @var
     */
    private $unique;

    /**
     * The URL placeholder for entities that support the URL Behavior.
     *
     * @var
     */
    private $url;

    /**
     * Set the database adapter and the database table.
     *
     * @param string|null $table
     * @throws Exception
     */
    public function __construct($table = null)
    {
        if ($table) {
            $this->database = new Database($table);
            $this->table    = $table;
        }
    }

    /**
     * Check if the request is of type POST.
     *
     * @return bool
     */
    public static function isPost()
    {
        return strtolower(Server::get('REQUEST_METHOD')) == 'post' ? true : false;
    }

    /**
     * Check if the request is of type GET.
     *
     * @return bool
     */
    public static function isGet()
    {
        return strtolower(Server::get('REQUEST_METHOD')) == 'get' ? true : false;
    }

    /**
     * This method should be used for automatic handling of any database operation with an entity.
     * Some model behaviors will also be executed, if any.
     *
     * Manage the data validation. Manage the file uploading. Manage the entire database operation.
     * Execute the behaviors related to the model instance.
     *
     * @param string $operation
     * @param array $data
     * @param string|array|null $where
     * @param array|null $validation
     * @param array|null $unique
     * @param Model|null $model
     * @return bool|int
     * @throws Exception
     */
    public function post($operation, $data, $where = null, $validation = null, $unique = null, $model = null)
    {
        $this->operation    = $operation;
        $this->data         = $data;
        $this->where        = $where;
        $this->validation   = $validation;
        $this->unique       = $unique;
        $this->model        = $model;
        $this->url          = $this->data['url'];

        if ($this->model->getBehaviors() && in_array('url', $this->model->getBehaviors())) {
            unset($this->data['url']);
        }

        $this->errors();

        if ($this->validation) {
            if (!Validator::validate($this->validation)) {
                return false;
            }
        }

        if ($_FILES) {
            foreach ($_FILES as $key => $val) {
                $success = 1;

                if (is_array($_FILES[$key]['error'])) {
                    foreach ($_FILES[$key]['error'] as $error) {
                        if ($error !== 0) {
                            $success++;
                            break;
                        }
                    }
                }

                if ((is_array($_FILES[$key]['error']) && $success === 1) || (!is_array($_FILES[$key]['error']) && $_FILES[$key]['error'] === 0)) {
                    if ($_FILES[$key]['error'] === 0) {
                        $this->data[$key] = (new Upload())->all($_FILES[$key]) ? Session::get('upload-file') : '';
                    } else {
                        Flash::error('Upload error!');
                    }
                }
            }
        }

        Session::disable('image');

        switch (strtolower($this->operation)) {
            case 'insert':
                $this->behaviorMetadata('insert');
                $this->behaviorTimestamp('insert');

                
                if (!$this->database->{$this->operation}($this->data, $this->unique)) {
                    return false;
                }

                $this->sql = $this->database->{$this->operation}($this->data, $this->unique);
                break;
            case 'update':
                $this->row = $model->getItem() ?: $model->load($where);

                $this->behaviorMetadata('update');
                $this->behaviorTimestamp('update');

                if (!$this->database->{$this->operation}($this->data, $this->unique, $this->row)) {
                    return false;
                }

                $this->sql = $this->database->{$this->operation}($this->data, $this->unique, $this->row)->where($this->where);
                break;
        }

        if (!$this->sql || !($this->id = $this->sql->save())) {
            Flash::error('Save failed! Check your fields values.');
            return false;
        }

        $this->behaviorUrl($operation);

        return $this->id;
    }

    /**
     * Set the URL for the entity, if the model instance presents the URL behavior.
     *
     * @param string $operation
     * @throws Exception
     */
    private function behaviorUrl($operation)
    {
        $actsAs = $this->model->getBehaviors();

        if (!empty($actsAs) && (in_array('url', $actsAs)  || !empty($actsAs['url']))) {
            (new Behavior_Url())->setUrl($this->url, $this->model->getTable(), strtolower($operation) == 'insert' ? $this->id : $this->model->getItem()->getId(), strtolower($operation));
        }
    }

    /**
     * Set the Timestamp for the entity, if the model instance presents the Timestamp behavior.
     *
     * @param string $operation
     */
    private function behaviorTimestamp($operation)
    {
        $actsAs = $this->model->getBehaviors();

        if (!empty($actsAs) && (in_array('timestamp', $actsAs))) {
            $timestamp  = Behavior_Timestamp::execute(empty($actsAs['timestamp']) ? null : $actsAs['timestamp'], strtolower($operation) == 'insert' ? true : false);
            $this->data = array_merge($this->data, $timestamp);
        }
    }

    /**
     * Set the Metadata for the entity, if the model instance presents the Metadata behavior.
     *
     * @param string $operation
     */
    private function behaviorMetadata($operation)
    {
        $actsAs = $this->model->getBehaviors();

        switch (strtolower($operation)) {
            case 'insert':
                if (!empty($actsAs) && in_array('metadata', $actsAs)) {
                    $this->data = Behavior_Metadata::parse($this->data);
                }
                break;
            case 'update':
                if (!empty($actsAs) && in_array('metadata', $actsAs)) {
                    $oldData = [];

                    foreach (unserialize($this->row->getMetadata()) as $key => $val) {
                        $oldData['metadata_' . $key] = $val;
                    }

                    $this->data = Arr::merge($oldData, $this->data);
                    $this->data = Behavior_Metadata::parse($this->data);
                }
                break;
        }
    }

    /**
     * Manage generic errors.
     *
     * @throws Exception
     */
    private function errors()
    {
        if (strtolower($this->operation) != 'insert' && strtolower($this->operation) != 'update') {
            throw new Exception('The OPERATION parameter must be insert or update');
        }

        if (strtolower($this->operation) == 'update' && !$this->where) {
            throw new Exception('For the update operation, the WHERE parameter must not be null');
        }

        if (!is_array($this->data) || empty($this->data)) {
            throw new Exception('The DATA parameter must be a non-empty array');
        }

        if ($this->validation && (!is_array($this->validation) || empty($this->validation))) {
            throw new Exception('The VALIDATION parameter must be non-empty array');
        }
    }
}