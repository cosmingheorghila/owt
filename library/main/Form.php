<?php

class Form
{
    /**
     * Handler for managing the current values of the inputs if any.
     * Should be represented as a loaded model instance.
     * Mostly used in an edit scenario.
     *
     * @var
     */
    private $row;

    /**
     * Handler for managing the constructing of different field types.
     *
     * @var
     */
    private $field;

    /**
     * Set the loaded instance model if any, for current values awareness.
     *
     * @param $row
     */
    public function __construct($row = null)
    {
        $this->row = $row;
    }

    /**
     * @param string $type
     * @param string $name
     * @param array|null $options
     * @param string|bool $value
     * @param string|null $selected
     * @param array|null $selectOptions
     * @param string|null $placeholder
     * @param bool $multiple
     * @param array|null $accept
     * @return string
     * @throws Exception
     */
    public function field($type, $name, $options = null, $value = null, $selected = null, $selectOptions = null, $placeholder = null, $multiple = false, $accept = null)
    {
        $optionString = '';

        if ($value === null) {
            $value = $this->row ? $this->row->getData($name) : Session::get('data.' . $name);
        }

        if ($options) {
            if (!is_array($options)) {
                throw new Exception('The OPTIONS parameter must be a non-empty array');
            }

            foreach ($options as $key => $val) {
                $optionString .= $key . '="' . $val . '" ';
            }
        }

        switch ($type) {
            case 'submit':
                $this->field = '<input type="' . $type . '" name="' . $name . '" value="' . $value . '" ' . $optionString . ' />';
                break;
            case 'reset':
                $this->field = '<input type="' . $type . '" name="' . $name . '" value="' . $value . '" ' . $optionString . ' />';
                break;
            case 'textarea':
                $this->field = '<textarea name="' . (String::startsWith($name, '.') ? substr($name, 1) : 'data[' . $name . ']') . '"' . $optionString . '>' . htmlentities($value) . '</textarea>';
                break;
            case 'file':
                $this->field = '<input type="' . $type . '" name="' . $name . '" value="' . $value . '" ' . $optionString . ($multiple ? ' multiple' : '');

                if ($accept) {
                    if (!is_array($accept)) {
                        throw new Exception('The ACCEPT parameter must be a non-empty array or null');
                    }

                    $this->field .= ' accept="';
                    $accepts = count($accept);

                    foreach ($accept as $key => $val) {
                        $this->field .= $key + 1 < $accepts ? ''.$val.'/*|' : ''.$val.'/*"';
                    }
                }

                $this->field .= ' />';
                break;
            case 'select':
                if ($placeholder) {
                    if (String::startsWith($name, '.')) {
                        $this->field = '<select name="' . substr($name, 1) . ($multiple ? '[]' : '') . '" class="chosen-select" data-placeholder="' . $placeholder . '" ' . $optionString . '' . ($multiple ? ' multiple' : '') . '>';
                    } else {
                        $this->field = '<select name="data[' . $name . ']' . ($multiple ? '[]' : '') . '" class="chosen-select" data-placeholder="' . $placeholder . '" ' . $optionString . '' . ($multiple ? ' multiple' : '') . '>';
                    }
                } else {
                    if (String::startsWith($name, '.')) {
                        $this->field = '<select name="' . substr($name, 1) . '" ' . $optionString . '>';
                    } else {
                        $this->field = '<select name="data[' . $name . ']" ' . $optionString . '>';
                    }
                }

                if (!$selectOptions) {
                    throw new Exception('You must specify the OPTIONS parameter');
                }

                if (!is_array($selectOptions)) {
                    throw new Exception('The OPTIONS parameter must be a non-empty array');
                }

                foreach ($selectOptions as $key => $val) {
                    if (is_array($val)) {
                        $this->field .= '<optgroup label="' . $key . '">';

                        foreach ($val as $value => $label) {
                            if (is_array($selected)) {
                                $this->field .= '<option value="' . $value . '"' . (in_array($value, $selected) ? 'selected="selected"' : '') . '>' . $label . '</option>';
                            } else {
                                $this->field .= '<option value="' . $value . '"' . ($selected == $value ? 'selected="selected"' : '') . '>' . $label . '</option>';
                            }
                        }

                        $this->field .= '</optgroup>';
                    } else {
                        if (is_array($selected)) {
                            $this->field .= '<option value="' . $key . '"' . (in_array($key, $selected) ? 'selected="selected"' : '') . '>' . $val . '</option>';
                        } else {
                            $this->field .= '<option value="' . $key . '"' . ($selected == $key ? 'selected="selected"' : '') . '>' . $val . '</option>';
                        }
                    }
                }

                $this->field .= '</select>';
                break;
            default:
                $this->field = '<input type="' . $type . '" name="' . (String::startsWith($name, '.') ? substr($name, 1) : 'data[' . $name . ']') . '" value="' . htmlentities($value) . '" ' . $optionString . ' />';
                break;

        }

        return $this->field;
    }

    /**
     * Helper method for creating an input of type text.
     * The $options parameter acts as a placeholder for additional input HTML attributes.
     *
     * @param string $name
     * @param array|null $options
     * @param string|null $value
     * @return string
     */
    public function text($name, $options = null, $value = null)
    {
        return $this->field('text', $name, $options, $value);
    }

    /**
     * Helper method for creating an input of type password.
     * The $options parameter acts as a placeholder for additional input HTML attributes.
     *
     * @param string $name
     * @param array|null $options
     * @param string|null $value
     * @return string
     */
    public function password($name, $options = null, $value = null)
    {
        return $this->field('password', $name, $options, $value);
    }

    /**
     * Helper method for creating an input of type color.
     * The $options parameter acts as a placeholder for additional input HTML attributes.
     *
     * @param string $name
     * @param array|null $options
     * @param string|null $value
     * @return string
     */
    public function color($name, $options = null, $value = null)
    {
        return $this->field('color', $name, $options, $value);
    }

    /**
     * Helper method for creating an input of type date.
     * The $options parameter acts as a placeholder for additional input HTML attributes.
     *
     * @param string $name
     * @param array|null $options
     * @param string|null $value
     * @return string
     */
    public function date($name, $options = null, $value = null)
    {
        return $this->field('date', $name, $options, $value);
    }

    /**
     * Helper method for creating an input of type email.
     * The $options parameter acts as a placeholder for additional input HTML attributes.
     *
     * @param string $name
     * @param array|null $options
     * @param string|null $value
     * @return string
     */
    public function email($name, $options = null, $value = null)
    {
        return $this->field('email', $name, $options, $value);
    }

    /**
     * Helper method for creating an input of type number.
     * The $options parameter acts as a placeholder for additional input HTML attributes.
     *
     * @param string $name
     * @param array|null $options
     * @param string|null $value
     * @return string
     */
    public function number($name, $options = null, $value = null)
    {
        return $this->field('number', $name, $options, $value);
    }

    /**
     * Helper method for creating an input of type hidden.
     * The $options parameter acts as a placeholder for additional input HTML attributes.
     *
     * @param string $name
     * @param array|null $options
     * @param string|null $value
     * @return string
     */
    public function hidden($name, $options = null, $value = null)
    {
        return $this->field('hidden', $name, $options, $value);
    }

    /**
     * Helper method for creating an input of type submit.
     * The $options parameter acts as a placeholder for additional input HTML attributes.
     *
     * @param string $name
     * @param string|null $value
     * @param bool $options
     * @return string
     */
    public function submit($name, $value = null, $options = null)
    {
        return $this->field('submit', $name, $options, $value);
    }

    /**
     * Helper method for creating an input of type reset.
     * The $options parameter acts as a placeholder for additional input HTML attributes.
     *
     * @param string $name
     * @param array|null $value
     * @param string|bool $options
     * @return string
     */
    public function reset($name, $value = null, $options = null)
    {
        return $this->field('reset', $name, $options, $value);
    }

    /**
     * Helper method for creating an input of type file.
     * The $options parameter acts as a placeholder for additional input HTML attributes.
     * Also, a multiple input file could be specified for multi-uploading files simultaneously.
     *
     * @param string $name
     * @param array|null $options
     * @param array|null $accept
     * @param bool $multiple
     * @param string|null $value
     * @return string
     */
    public function file($name, $options = null, $accept = null, $multiple = false, $value = null)
    {
        return $this->field('file', $name . ($multiple ? '[]' : ''), $options, $value, null, null, null, $multiple, $accept);
    }

    /**
     * Helper method for creating an input of type file that accepts only audio files.
     * The $options parameter acts as a placeholder for additional input HTML attributes.
     * Also, a multiple input file could be specified for multi-uploading audio simultaneously.
     *
     * @param string $name
     * @param array|null $options
     * @param array|null $accept
     * @param bool $multiple
     * @param string|null $value
     * @return string
     */
    public function audio($name, $options = null, $accept = ['audio'], $multiple = false, $value = null)
    {
        return $this->field('file', $name . ($multiple ? '[]' : ''), $options, $value, null, null, null, $multiple, $accept);
    }

    /**
     * Helper method for creating an input of type file that accepts only video files.
     * Also, this method is responsible for setting the video thumbnail according to it's type, as specified in the videos.xml file.
     * The $options parameter acts as a placeholder for additional input HTML attributes.
     * Also, a multiple input file could be specified for multi-uploading videos simultaneously.
     *
     * @param string $name
     * @param string|null $type
     * @param array|null $options
     * @param array|null $accept
     * @param bool $multiple
     * @param string|null $value
     * @return string
     */
    public function video($name, $type = null, $options = null, $accept = ['video'], $multiple = false, $value = null)
    {
        if ($type !== null) {
            Video::thumb($type);
        }

        return $this->field('file', $name . ($multiple ? '[]' : ''), $options, $value, null, null, null, $multiple, $accept);
    }

    /**
     * Helper method for creating an input of type file that accepts only image files.
     * Also, this method is responsible for setting the image size according to it's type, as specified in the images.xml file.
     * The $options parameter acts as a placeholder for additional input HTML attributes.
     * Also, a multiple input file could be specified for multi-uploading images simultaneously.
     *
     * @param string $name
     * @param images.xml|string $type
     * @param array|null $options
     * @param string|null $value
     * @param bool $multiple
     * @param array $accept
     * @return string
     */
    public function image($name, $type, $options = null, $value = null, $multiple = false, $accept = ['image'])
    {
        if ($type !== null) {
            Image::thumb($type);
        }

        return $this->field('file', $name . ($multiple ? '[]' : ''), $options, $value, null, null, null, $multiple, $accept);
    }

    /**
     * Helper method for creating an input of type textarea.
     * The $options parameter acts as a placeholder for additional input HTML attributes.
     *
     * @param string $name
     * @param array|null $options
     * @param string|null $value
     * @return string
     */
    public function textarea($name, $options = null, $value = null)
    {
        return $this->field('textarea', $name, $options, $value);
    }

    /**
     * Helper method for creating an input of type select.
     * The $options parameter acts as a placeholder for additional input HTML attributes.
     *
     * @param string $name
     * @param array $selectOptions
     * @param string $selected
     * @param array|null $options
     * @param string|null $value
     * @return string
     */
    public function select($name, $selectOptions, $selected, $options = null, $value = null)
    {
        return $this->field('select', $name, $options, $value, $selected, $selectOptions);
    }

    /**
     * Helper method for creating an input of type select using the chosen plugin.
     * The $options parameter acts as a placeholder for additional input HTML attributes.
     * Also, a multiple input select could be specified for multi-selecting values simultaneously.
     *
     * @param string $name
     * @param array|string $selectOptions
     * @param string $selected
     * @param string $placeholder
     * @param bool $multiple
     * @param array|null $options
     * @param string|null $value
     * @return string
     */
    public function selectChosen($name, $selectOptions = '', $selected, $placeholder, $multiple = false, $options = null, $value = null)
    {
        $options ? (isset($options['class']) ? $options['class'] .= ' chosen-select' : $options['class'] = ' chosen-select') : $options['class'] = 'chosen-select';

        return $this->field('select', $name, $options, $value, $selected, $selectOptions, $placeholder, $multiple);
    }

    /**
     * Helper method for creating an input of type date using the date-picker plugin.
     * The $options parameter acts as a placeholder for additional input HTML attributes.
     *
     * @param string $name
     * @param array|null $options
     * @param string|null $value
     * @return string
     */
    public function calendar($name, $options = null, $value = null)
    {
        $options ? (isset($options['class']) ? $options['class'] .= ' datepicker' : $options['class'] = ' datepicker') : $options['class'] = 'datepicker';
        $value = is_numeric($value) ? date('d M, Y', $value) : $value;


        return $this->field('text', $name, $options, $value);
    }

    /**
     * Helper method for creating an input of type date using the time-picker plugin.
     * The $options parameter acts as a placeholder for additional input HTML attributes.
     *
     * @param string $name
     * @param array|null $options
     * @param string|null $value
     * @return string
     */
    public function time($name, $options = null, $value = null)
    {
        $options ? (isset($options['class']) ? $options['class'] .= ' timepicker' : $options['class'] = ' timepicker') : $options['class'] = 'timepicker';
        $value = is_numeric($value) ? date('H:i', $value) : $value;

        return $this->field('text', $name, $options, $value);
    }

    /**
     * Helper method for creating an input of type textarea using the tinymce plugin.
     * The $options parameter acts as a placeholder for additional input HTML attributes.
     *
     * @param string $name
     * @param array|null $options
     * @param string|null $value
     * @return string
     */
    public function editor($name, $options = null, $value = null)
    {
        $options ? (isset($options['class']) ? $options['class'] .= ' editor' : $options['class'] = ' editor') : $options['class'] = 'editor';

        return $this->field('textarea', $name, $options, $value);
    }
}