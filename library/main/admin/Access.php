<?php

class Admin_Access
{
    /**
     * Get all admin access levels defined in the acl.xml file.
     *
     * @return array
     */
    public static function getAccessLevels()
    {
        $result = [];

        foreach (Config::getAcl()->category as $level) {
            foreach ($level->item as $item) {
                $result[(string)$level->attributes()][(string)$item->attributes()['name']] = (string)$item->attributes()['label'];
            }
        }

        return $result;
    }

    /**
     * Cross-reference a given access level with the admin user's permissions.
     *
     * @param string $level
     * @return bool
     * @throws Exception
     */
    public static function checkAccessLevel($level)
    {
        $admin = Admin_Model_User::getInstance()->load(Session::get('admin-user'));

        return $admin->isSuperAdmin() ? true : (in_array(
            $level, explode(',', $admin->getAccessLevel())
        ) ? true : false);
    }
}