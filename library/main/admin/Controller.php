<?php

class Admin_Controller extends Controller
{
    /**
     * The actual model class string.
     * Used in child controllers.
     *
     * @var
     */
    protected $modelClass;

    /**
     * The actual model instance.
     *
     * @var
     */
    protected $model;

    /**
     * The current admin user.
     *
     * @var
     */
    public $admin;

    /**
     * The identifier returned by the AddEntity method.
     *
     * @var
     */
    public $id;

    /**
     * The records returned by the GetRecords method.
     * According to the sql conditions.
     *
     * @var
     */
    public $records;

    /**
     * The paginate formatted records.
     *
     * @var
     */
    public $items;

    /**
     * The title variable to be assigned to the view.
     *
     * @var
     */
    public $title;

    /**
     * The breadcrumbs variable to be assigned to the view.
     *
     * @var
     */
    public $breadcrumbs;

    /**
     * The URL to redirect after a successful request.
     *
     * @var
     */
    public $redirectUrl;

    /**
     * The paginate conditioner.
     * To be used in the child controller's actions.
     *
     * @var bool
     */
    public $paginate = true;

    /**
     * The movable conditioner.
     * To be used in the child controller's actions.
     *
     * @var bool
     */
    public $movable = false;

    /**
     * The data container for future database operations.
     * This can be merged inside an AfterAdd or AfterEdit method.
     *
     * @var array
     */
    public $data = [];

    /**
     * Return the current model.
     *
     * @return mixed
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Return a new instance of the model specified in the child class.
     *
     * @return mixed
     */
    public function getNewModel()
    {
        return new $this->modelClass();
    }

    /**
     * Assign the current admin user to a variable to cross-reference his permissions.
     *
     * @return void
     */
    public function setAdmin()
    {
        $this->admin = Session::exists('admin-user') ? Admin_Model_User::getInstance()->load(Session::get('admin-user')) : null;
    }

    /**
     * Assign the current page query parameter to a Session.
     * After save, the script will redirect with that param in place.
     *
     * @return void
     */
    public function setPage()
    {
        if (!$this->getRequest()->isPost()) {
            parse_str(Arr::last(explode('?', Server::get('HTTP_REFERER'))), $arr);
            Session::set('redirect-url-page', $arr['page']);
        }
    }

    /**
     * Core admin list logic.
     *
     * This method takes care of the basic logic and aspects such as:
     * Verifying a user and it's access level.
     * Deleting a specified entity row set.
     * Fetching records and paginating them.
     * Manual sorting rows.
     * Assigning common variables to the view.
     *
     *
     * @param Model|null $model
     * @return void
     */
    public function index($model = null)
    {
        $this->setAdmin();

        $model = $model ?: $this->getNewModel();

        $model->verifyValidUser();
        $model->verifyAccessLevel();
        $model->deleteEntity();

        $this->records  = $this->records ?: $model->getRecords();
        $this->items    = $this->paginate($this->records, $this->paginate ? 10 : null);

        if ($this->movable) {
            $this->movable($model);
        }

        $this->assign('title', $this->title);
        $this->assign('breadcrumbs', $this->breadcrumbs);
        $this->assign('admin', $this->admin);
        $this->assign('items', $this->items);
        $this->assign('records', $this->records);
        $this->assign('entity', $model->getTable());
    }

    /**
     * Core admin add logic.
     *
     * This method takes care of the basic logic and aspects such as:
     * Verifying a user and it's access level.
     * Constructing the final data to be processed.
     * Adding a new record set of that entity.
     * Assigning common variables to the view.
     *
     * @param Model|null $model
     * @throws Exception
     * @return void
     */
    public function add($model = null)
    {
        $this->setAdmin();
        $this->setPage();

        $model = $model ?: $this->getNewModel();

        $model->verifyValidUser();
        $model->verifyAccessLevel();

        if ($this->getRequest()->isPost()) {
            $this->data = Arr::merge((array)Post::get('data'), $this->data);

            Session::set('data', $this->data);
            $this->beforeAdd($model);

            if ($this->id = $this->model->addEntity($this->data)) {
                $this->afterAdd($model, $this->id);
                $this->relations('add');
                $this->flashSuccess('Your record was successfully added!');
                $this->redirect();
            } else {
                $this->redirectUrl(Url::getPreviousUrl());
            }
        }

        $this->assign('admin', $this->admin);
        $this->assign('form', new Form());
        $this->assign('title', $this->title);
        $this->assign('breadcrumbs', $this->breadcrumbs);
    }

    /**
     * Core edit admin logic.
     *
     * This method takes care of the basic logic and aspects such as:
     * Verifying a user and it's access level.
     * Constructing the final data to be processed.
     * Modifying the record set of that entity.
     * Assigning custom CSS & JS required only on edit views.
     * Assigning common variables to the view.
     *
     * @param Model|null $model
     * @throws Exception
     * @return void
     */
    public function edit($model = null)
    {
        $this->setAdmin();
        $this->setPage();

        $model = $model ?: $this->getNewModel();

        $model->verifyValidUser();
        $model->verifyAccessLevel();
        $model->verifyExistentItem();

        if ($this->getRequest()->isPost() && Post::exists('identifier')) {
            $this->data = Arr::merge(Post::get('data'), $this->data);

            $this->beforeEdit($model);

            if ($this->model->editEntity($this->data)) {
                $this->afterEdit($model);
                $this->relations('edit');

                $this->flashSuccess('Your record was successfully modified!');
                $this->redirect();
            } else {
                $this->redirectUrl(Url::getPreviousUrl());
            }
        }

        $this->addBlock($model);
        $this->deleteBlock();
        $this->orderBlocks($model);

        $this->addAttribute();
        $this->editAttribute();
        $this->deleteAttribute();

        $this->addVendor();
        $this->deleteVendor();
        $this->orderVendors($model);

        $this->addDiscount();
        $this->deleteDiscount();
        $this->orderDiscounts($model);

        $this->addTax();
        $this->deletetax();
        $this->orderTaxes($model);

        $this->deleteOrderProduct();

        $this->deleteImage($model);
        $this->deleteVideo($model);
        $this->deleteAudio($model);
        $this->deleteFile($model);

        $this->deleteMetaImage($model);
        $this->deleteMetaVideo($model);
        $this->deleteMetaAudio($model);
        $this->deleteMetaFile($model);

        $this->prependStyleSheet(Dir::getAssets() . '/admin/css/partials/tree-table.css');
        $this->appendScript(Dir::getAssets() . '/admin/js/partials/tree-table.js');
        $this->appendScript(Dir::getAssets() . '/admin/js/partials/draggable.js');

        $this->assign('admin', $this->admin);
        $this->assign('item', $model->getItem());
        $this->assign('form', new Form($model->getItem()));
        $this->assign('metadata', unserialize($model->getItem()->getMetadata()));
        $this->assign('title', $this->title);
        $this->assign('breadcrumbs', $this->breadcrumbs);
    }

    /**
     * Method injection.
     * This method is to be used in child classes for extra logic.
     *
     * @return void
     */
    protected function beforeAdd() {}

    /**
     * Method injection.
     * This method is to be used in child classes for extra logic.
     *
     * @return void
     */
    protected function afterAdd() {}

    /**
     * Method injection.
     * This method is to be used in child classes for extra logic.
     *
     * @return void
     */
    protected function beforeEdit() {}

    /**
     * Method injection.
     * This method is to be used in child classes for extra logic.
     *
     * @return void
     */
    protected function afterEdit() {}

    /**
     * Initialize the movable behavior on the current entity.
     *
     * @param Model|null $model
     * @throws Exception
     */
    protected function movable($model = null)
    {
        $actsAs = $model->getBehaviors();

        if (!empty($actsAs) && (!empty($actsAs['draggable']) || in_array('draggable', $actsAs))) {
            Behavior_Draggable::sort($model->getTable(), !empty($actsAs['draggable']) ? $actsAs['draggable']['field'] : null);

            $this->prependStyleSheet(Dir::getAssets() . '/admin/css/partials/tree-table.css');
            $this->appendScript(Dir::getAssets() . '/admin/js/partials/tree-table.js');
            $this->appendScript(Dir::getAssets() . '/admin/js/partials/draggable.js');
        }
    }

    /**
     * Manage the success redirect on edit action.
     *
     * @return void
     */
    protected function redirect()
    {
        if (Post::exists('save-stay')) {
            $this->redirectUrl(Url::getPreviousUrl());
        }

        $this->redirectUrl(Url::getAdminUrl() . '/' . $this->redirectUrl . (Session::exists('redirect-url-page') ? '?page=' . Session::get('redirect-url-page') : ''));
    }

    /**
     * Manage the HasAndBelongsToMany relations of the current entity.
     * Basically it updates the pivot table accordingly.
     * For this method to take place, the HasAndBelongsToMany must be declared with enable_rel = true.
     *
     * @param string $action
     * @return void
     */
    protected function relations($action)
    {
        $hasAndBelongsToMany = $this->model->getRelation('hasAndBelongsToMany');

        if (is_array($hasAndBelongsToMany) && !empty($hasAndBelongsToMany)) {
            foreach ($hasAndBelongsToMany as $name => $params) {
                if ($hasAndBelongsToMany[$name]['enable_rel'] === true) {
                    if ($action == 'edit') {
                        $this->getAdapter($params['table'])->delete()->where([
                            $params['local_key'] => Post::get('identifier')
                        ])->save();
                    }

                    if ($_POST[$name] && is_array($_POST[$name]) & !empty($_POST[$name])) {
                        foreach ($_POST[$name] as $value) {
                            $this->getAdapter($params['table'])->insert([
                                $params['local_key']    => $action == 'add' ? $this->id : Post::get('identifier'),
                                $params['remote_key']   => $value
                            ])->save();
                        }
                    }
                }
            }
        }
    }

    /**
     * @param Model|null $model
     */
    private function addBlock($model = null)
    {
        if ($this->getRequest()->isPost() && Post::exists('add_block')) {
            $data['entity_id']  = $model->getItem()->getId();
            $data['entity']     = $model->getTable();
            $data['block_id']   = Post::get('block_id');
            $data['location']   = Post::get('location');

            $database = new Database('cms_blocks_relations');
            $database->insert($data);

            if ($id = $database->save()) {
                echo json_encode(['status' => true, 'id' => $id,]);
                die;
            } else {
                echo json_encode(['status' => false]);
                die;
            }
        }
    }

    /**
     * @return void
     */
    private function deleteBlock()
    {
        if ($this->getRequest()->isPost() && Post::exists('delete_block')) {
            $database = new Database('cms_blocks_relations');
            $database->delete()->where([
                'id' => Post::get('block_id')
            ]);

            if ($database->save()) {
                echo json_encode(['status' => true]);
                die;
            } else {
                echo json_encode(['status' => false]);
                die;
            }
        }
    }

    /**
     * @param Model|null $model
     */
    private function orderBlocks($model = null)
    {
        if ($this->getRequest()->isPost() && Post::exists('order_blocks')) {
            Behavior_Draggable::sort('cms_blocks_relations', [
                'entity_id' => $model->getItem()->getId(),
                'entity'    => $model->getTable(),
            ]);
        }
    }

    /**
     * @return void
     */
    private function addAttribute()
    {
        if ($this->getRequest()->isPost() && Post::exists('add_attribute')) {
            $data = [];

            $data['product_id']     = Post::get('product_id');
            $data['attribute_id']   = Post::get('attribute_id');
            $data['value']          = Post::get('attribute_value');

            foreach ($data as $key => $value) {
                if ($key != 'value' && ($value === null || $value == '')) {
                    echo json_encode(['status' => false, 'message' => 'You must specify the ' . strtoupper(str_replace('_', ' ', $key))]);
                    die;
                }
            }

            $database = new Database('shop_products_attributes_ring');
            $database->insert($data);

            try {
                $id = $database->save();

                echo json_encode(['status' => true, 'id' => $id]);
                die;
            } catch (PDOException $e) {
                echo json_encode(['status' => false, 'message' => Flash::get()]);
                die;
            } catch (Exception $e) {
                echo json_encode(['status' => false, 'message' => Flash::get()]);
                die;
            }
        }
    }

    /**
     * @return void
     */
    private function editAttribute()
    {
        if ($this->getRequest()->isPost() && Post::exists('edit_attribute')) {
            $database = new Database('shop_products_attributes_ring');
            $database->update(['value' => Post::get('value')])->where(['id' => Post::get('id')]);

            if ($database->save()) {
                echo json_encode(['status' => true]);
                die;
            } else {
                echo json_encode(['status' => false]);
                die;
            }
        }
    }

    /**
     * @return void
     */
    private function deleteAttribute()
    {
        if ($this->getRequest()->isPost() && Post::exists('delete_attribute')) {
            $database = new Database('shop_products_attributes_ring');
            $database->delete()->where(['id' => Post::get('attribute_id')]);

            if ($database->save()) {
                echo json_encode(['status' => true]);
                die;
            } else {
                echo json_encode(['status' => false]);
                die;
            }
        }
    }

    /**
     * @return void
     */
    private function addVendor()
    {
        if ($this->getRequest()->isPost() && Post::exists('add_vendor')) {
            $data = [];

            $data['product_id'] = Post::get('product_id');
            $data['vendor_id']  = Post::get('vendor_id');
            $data['sku']        = Post::get('vendor_sku');
            $data['price']      = Post::get('vendor_price');
            $data['quantity']   = Post::get('vendor_quantity');

            foreach ($data as $key => $value) {
                if ($value === null || $value == '') {
                    echo json_encode(['status' => false, 'message' => 'You must specify the ' . strtoupper(str_replace('_', ' ', $key))]);
                    die;
                }
            }

            foreach (Shop_Model_Product::getInstance()->where("id != {$data['product_id']}")->getCollection() as $product) {
                if ($product->getSku() == $data['sku']) {
                    echo json_encode(['status' => false, 'message' => 'There is already another product with that SKU']);
                    die;
                }
            }

            if (!is_numeric($data['price'])) {
                echo json_encode(['status' => false, 'message' => 'The PRICE must be numeric']);
                die;
            }

            if (!is_numeric($data['quantity'])) {
                echo json_encode(['status' => false, 'message' => 'The QUANTITY must be numeric']);
                die;
            }

            $database = new Database('shop_products_vendors_ring');
            $database->insert($data);

            try {
                $id = $database->save();
                $database->update(['ord' => $id])->where(['id' => $id])->save();

                $product = Shop_Model_Product::getInstance()->load($data['product_id']);
                $product->updateVirtualData();

                echo json_encode(['status' => true, 'id' => $id]);
                die;
            } catch (PDOException $e) {
                echo json_encode(['status' => false, 'message' => Flash::get() ?: $e->getMessage()]);
                die;
            } catch (Exception $e) {
                echo json_encode(['status' => false, 'message' => Flash::get() ?: $e->getMessage()]);
                die;
            }
        }
    }

    /**
     * @return void
     */
    private function deleteVendor()
    {
        if ($this->getRequest()->isPost() && Post::exists('delete_vendor')) {
            $database = new Database('shop_products_vendors_ring');
            $database->delete()->where(['id' => Post::get('vendor_id')]);

            if ($database->save()) {
                $product = Shop_Model_Product::getInstance()->load(Post::get('product_id'));
                $product->updateVirtualData();

                echo json_encode(['status' => true]);
                die;
            } else {
                echo json_encode(['status' => false]);
                die;
            }
        }
    }

    /**
     * @param Model|null $model
     */
    private function orderVendors($model = null)
    {
        if ($this->getRequest()->isPost() && Post::exists('order_vendors')) {
            Behavior_Draggable::sort('shop_products_vendors_ring', [
                'product_id' => $model->getItem()->getId()
            ]);
        }
    }

    /**
     * @return void
     */
    private function addDiscount()
    {
        if ($this->getRequest()->isPost() && Post::exists('add_discount')) {
            $data = [];

            $data['product_id']     = Post::get('product_id');
            $data['discount_id']    = Post::get('discount_id');
            $data['vendor_id']      = Post::exists('vendor_id') ? Post::get('vendor_id') : 'null';

            $database = new Database('shop_products_discounts_ring');
            $database->insert($data);

            try {
                $id = $database->save();
                $database->update(['ord' => $id])->where(['id' => $id])->save();

                echo json_encode(['status' => true, 'id' => $id]);
                die;
            } catch (PDOException $e) {
                echo json_encode(['status' => false, 'message' => Flash::get() ?: $e->getMessage()]);
                die;
            } catch (Exception $e) {
                echo json_encode(['status' => false, 'message' => Flash::get() ?: $e->getMessage()]);
                die;
            }
        }
    }

    /**
     * @return void
     */
    private function deleteDiscount()
    {
        if ($this->getRequest()->isPost() && Post::exists('delete_discount')) {
            $database = new Database('shop_products_discounts_ring');
            $database->delete()->where(['id' => Post::get('discount_id')]);

            if ($database->save()) {
                echo json_encode(['status' => true]);
                die;
            } else {
                echo json_encode(['status' => false]);
                die;
            }
        }
    }

    /**
     * @param Model|null $model
     */
    private function orderDiscounts($model = null)
    {
        if ($this->getRequest()->isPost() && Post::exists('order_discounts')) {
            Behavior_Draggable::sort('shop_products_discounts_ring', [
                'product_id' => $model->getItem()->getId()
            ]);
        }
    }

    /**
     * @return void
     */
    private function addTax()
    {
        if ($this->getRequest()->isPost() && Post::exists('add_tax')) {
            $data = [];

            $data['product_id'] = Post::get('product_id');
            $data['tax_id']     = Post::get('tax_id');
            $data['vendor_id']  = Post::exists('vendor_id') ? Post::get('vendor_id') : 'null';

            $database = new Database('shop_products_taxes_ring');
            $database->insert($data);

            try {
                $id = $database->save();
                $database->update(['ord' => $id])->where(['id' => $id])->save();

                echo json_encode(['status' => true, 'id' => $id]);
                die;
            } catch (PDOException $e) {
                echo json_encode(['status' => false, 'message' => Flash::get() ?: $e->getMessage()]);
                die;
            } catch (Exception $e) {
                echo json_encode(['status' => false, 'message' => Flash::get() ?: $e->getMessage()]);
                die;
            }
        }
    }

    /**
     * @return void
     */
    private function deleteTax()
    {
        if ($this->getRequest()->isPost() && Post::exists('delete_tax')) {
            $database = new Database('shop_products_taxes_ring');
            $database->delete()->where(['id' => Post::get('tax_id')]);

            if ($database->save()) {
                echo json_encode(['status' => true]);
                die;
            } else {
                echo json_encode(['status' => false]);
                die;
            }
        }
    }

    /**
     * @param Model|null $model
     */
    private function orderTaxes($model = null)
    {
        if ($this->getRequest()->isPost() && Post::exists('order_taxes')) {
            Behavior_Draggable::sort('shop_products_taxes_ring', [
                'product_id' => $model->getItem()->getId()
            ]);
        }
    }

    /**
     * @return void
     */
    private function deleteOrderProduct()
    {
        if ($this->getRequest()->isPost() && Post::exists('delete_order_product')) {
            $database = new Database('shop_orders_items');
            $database->delete()->where(['id' => Post::get('product_id')]);

            if ($database->save()) {
                echo json_encode(['status' => true]);
                die;
            } else {
                echo json_encode(['status' => false]);
                die;
            }
        }
    }

    /**
     * @param Model|null $model
     */
    private function deleteImage($model = null)
    {
        if ($this->getRequest()->isPost() && Post::exists('delete_image')) {
            $parts      = explode('.', $model->getItem()->{'get' . ucwords(Get::get('field'))}());
            $database   = new Database($model->getItem()->getTable());
            $delete     = array_map('unlink', glob(Dir::getPublic() . '/uploads/image/' . $parts[0] . '*.*'));

            if ($database->update([Get::get('field') => ''])->where(['id' => $model->getItem()->getId()])->save()) {
                echo json_encode(['deletedRecord' => true]);
            } else {
                echo json_encode(['deletedRecord' => false]);
            }

            if (File::delete(Dir::getPublic() . '/uploads/image/original/' . $model->getItem()->{'get' . ucwords(Get::get('field'))}())) {
                echo json_encode(['deletedOriginalImage' => true]);
            } else {
                echo json_encode(['deleteOriginalImage' => false]);
            }

            if (count(array_unique($delete)) === 1 && end($delete) === true) {
                echo json_encode(['deletedImage' => true]);
            } else {
                echo json_encode(['deleteImage' => false]);
            }

            die;
        }
    }

    /**
     * @param Model|null $model
     */
    private function deleteVideo($model = null)
    {
        if ($this->getRequest()->isPost() && Post::exists('delete_video')) {
            $database   = new Database($model->getItem()->getTable());
            $video      = explode('.', $model->getItem()->{'get' . ucwords(Get::get('field'))}());

            if ($database->update([Get::get('field') => ''])->where(['id' => $model->getItem()->getId()])->save()) {
                echo json_encode(['deletedRecord' => true]);
            } else {
                echo json_encode(['deletedRecord' => false]);
            }

            if (File::delete(Dir::getPublic() . '/uploads/video/thumbnails/' . $video[0] . '.jpg')) {
                echo json_encode(['deletedThumbnail' => true]);
            } else {
                echo json_encode(['deletedThumbnail' => false]);
            }

            if (File::delete(Dir::getPublic() . '/uploads/video/' . $model->getItem()->{'get' . ucwords(Get::get('field'))}())) {
                echo json_encode(['deletedVideo' => true]);
            } else {
                echo json_encode(['deleteVideo' => false]);
            }

            die;
        }
    }

    /**
     * @param Model|null $model
     */
    private function deleteAudio($model = null)
    {
        if ($this->getRequest()->isPost() && Post::exists('delete_audio')) {
            $database = new Database($model->getItem()->getTable());

            if ($database->update([Get::get('field') => ''])->where(['id' => $model->getItem()->getId()])->save()) {
                echo json_encode(['deletedRecord' => true]);
            } else {
                echo json_encode(['deletedRecord' => false]);
            }

            if (File::delete(Dir::getPublic() . '/uploads/audio/' . $model->getItem()->{'get' . ucwords(Get::get('field'))}())) {
                echo json_encode(['deletedAudio' => true]);
            } else {
                echo json_encode(['deleteAudio' => false]);
            }

            die;
        }
    }

    /**
     * @param Model|null $model
     */
    private function deleteFile($model = null)
    {
        if ($this->getRequest()->isPost() && Post::exists('delete_file')) {
            $database = new Database($model->getItem()->getTable());

            if ($database->update([Get::get('field') => ''])->where(['id' => $model->getItem()->getId()])->save()) {
                echo json_encode(['deletedRecord' => true]);
            } else {
                echo json_encode(['deletedRecord' => false]);
            }

            if (File::delete(Dir::getPublic() . '/uploads/file/' . $model->getItem()->{'get' . ucwords(Get::get('field'))}())) {
                echo json_encode(['deletedFile' => true]);
            } else {
                echo json_encode(['deleteFile' => false]);
            }

            die;
        }
    }

    /**
     * @param Model|null $model
     */
    private function deleteMetaImage($model = null)
    {
        if ($this->getRequest()->isPost() && Post::exists('delete_meta_image')) {
            $database = new Database($model->getItem()->getTable());
            $metadata = unserialize($model->getItem()->getMetadata());

            $name = $metadata[Get::get('field')];
            $metadata[Get::get('field')] = '';

            $parts  = explode('.', $name);
            $delete = array_map('unlink', glob(Dir::getPublic() . '/uploads/image/' . $parts[0] . '*.*'));

            if ($database->update(['metadata' => serialize($metadata)])->where(['id' => $model->getItem()->getId()])->save()) {
                echo json_encode(['deletedRecord' => true]);
            } else {
                echo json_encode(['deletedRecord' => false]);
            }

            if (File::delete(Dir::getPublic() . '/uploads/image/original/' . $name)) {
                echo json_encode(['deletedOriginalImage' => true]);
            } else {
                echo json_encode(['deleteOriginalImage' => false]);
            }

            if (count(array_unique($delete)) === 1 && end($delete) === true) {
                echo json_encode(['deletedImage' => true]);
            } else {
                echo json_encode(['deleteImage' => false]);
            }

            die;
        }
    }

    /**
     * @param Model|null $model
     */
    private function deleteMetaVideo($model = null)
    {
        if ($this->getRequest()->isPost() && Post::exists('delete_meta_video')) {
            $database = new Database($model->getItem()->getTable());
            $metadata = unserialize($model->getItem()->getMetadata());

            $name   = $metadata[Get::get('field')];
            $video  = explode('.', $name);

            $metadata[Get::get('field')] = '';

            if ($database->update(['metadata' => serialize($metadata)])->where(['id' => $model->getItem()->getId()])->save()) {
                echo json_encode(['deletedRecord' => true]);
            } else {
                echo json_encode(['deletedRecord' => false]);
            }

            if (File::delete(Dir::getPublic() . '/uploads/video/thumbnails/' . $video[0] . '.jpg')) {
                echo json_encode(['deletedThumbnail' => true]);
            } else {
                echo json_encode(['deletedThumbnail' => false]);
            }

            if (File::delete(Dir::getPublic() . '/uploads/video/' . $name)) {
                echo json_encode(['deletedVideo' => true]);
            } else {
                echo json_encode(['deleteVideo' => false]);
            }

            die;
        }
    }

    /**
     * @param Model|null $model
     */
    private function deleteMetaAudio($model = null)
    {
        if ($this->getRequest()->isPost() && Post::exists('delete_meta_audio')) {
            $database = new Database($model->getItem()->getTable());
            $metadata = unserialize($model->getItem()->getMetadata());

            $name = $metadata[Get::get('field')];
            $metadata[Get::get('field')] = '';


            if ($database->update(['metadata' => serialize($metadata)])->where(['id' => $model->getItem()->getId()])->save()) {
                echo json_encode(['deletedRecord' => true]);
            } else {
                echo json_encode(['deletedRecord' => false]);
            }

            if (File::delete(Dir::getPublic() . '/uploads/audio/' . $name)) {
                echo json_encode(['deletedAudio' => true]);
            } else {
                echo json_encode(['deleteAudio' => false]);
            }

            die;
        }
    }

    /**
     * @param Model|null $model
     */
    private function deleteMetaFile($model = null)
    {
        if ($this->getRequest()->isPost() && Post::exists('delete_meta_file')) {
            $database = new Database($model->getItem()->getTable());
            $metadata = unserialize($model->getItem()->getMetadata());

            $name = $metadata[Get::get('field')];
            $metadata[Get::get('field')] = '';

            if ($database->update(['metadata' => serialize($metadata)])->where(['id' => $model->getItem()->getId()])->save()) {
                echo json_encode(['deletedRecord' => true]);
            } else {
                echo json_encode(['deletedRecord' => false]);
            }

            if (File::delete(Dir::getPublic() . '/uploads/file/' . $name)) {
                echo json_encode(['deletedFile' => true]);
            } else {
                echo json_encode(['deleteFile' => false]);
            }

            die;
        }
    }
}