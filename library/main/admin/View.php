<?php

class Admin_View
{
    /**
     * Compose an anchor tag with the purpose of guiding the user to the add route of the same entity.
     *
     * @param string|null $params
     * @return string
     */
    public static function topAddButton($params = null)
    {
        return '<a class="btn add blue right" href="' . Arr::first(explode('/index', Url::getCurrentUrl())) . '/add' . ($params ? '?' . $params : '') . '">Add</a>';
    }

    /**
     * Compose an anchor tag with the purpose of guiding the user to the add route of the same entity.
     *
     * @param int $id
     * @return string
     */
    public static function topAddChildrenButton($id)
    {
        return '<a class="btn add blue right" href="' . Arr::first(explode('/index', Url::getCurrentUrl())) . '/add' . (!$id ?: '?parent_id=' . $id) . '">Add</a>';
    }

    /**
     * Compose an anchor tag with the purpose of guiding the user to the add route of the same entity.
     *
     * @return string
     */
    public static function bottomAddButton()
    {
        return '<a class="btn add blue left" href="' . Arr::first(explode('/index', Url::getCurrentUrl())) . '/add">Add</a>';
    }

    /**
     * Compose an anchor tag with the purpose of guiding the user to the add route of the same entity.
     *
     * @param int $id
     * @return string
     */
    public static function bottomAddChildrenButton($id)
    {
        return '<a class="btn add blue left" href="' . Arr::first(explode('/index', Url::getCurrentUrl())) . '/add' . (!$id ?: '?parent_id=' . $id) . '">Add</a>';
    }

    /**
     * Compose an anchor tag with the purpose of updating the current view.
     *
     * @return string
     */
    public static function topUpdateButton()
    {
        return '<a class="btn update black right" onclick="window.location.reload()">Update</a>';
    }

    /**
     * Compose an anchor tag with the purpose of updating the current view.
     *
     * @return string
     */
    public static function bottomUpdateButton()
    {
        return '<a class="btn update black left" onclick="window.location.reload()">Update</a>';
    }

    /**
     * Compose an anchor tag with the purpose of guiding the user to the edit route of the same entity.
     *
     * @param int $id
     * @return string
     */
    public static function editButton($id)
    {
        return '<a href="' . Arr::first(explode('/index', Url::getCurrentUrl())) . '/edit?id=' . $id . '" class="btn edit green margin-right">Edit</a>';
    }

    /**
     * Compose an anchor tag with the purpose of guiding the user to the edit route of the same entity.
     *
     * @param int $id
     * @param int $parent_id
     * @return string
     */
    public static function editChildButton($id, $parent_id)
    {
        return '<a href="' . Arr::first(explode('/index', Url::getCurrentUrl())) . '/edit?id=' . $id . (!$parent_id ?: '&parent_id=' . $parent_id) .'" class="btn edit green margin-right">Edit</a>';
    }

    /**
     * @param int $id
     * @return string
     * @throws Exception
     */
    public static function duplicateButton($id)
    {
        return '<a href="' . Arr::first(explode('/edit', Url::getCurrentUrl())) . '/duplicate?id=' . $id . '" class="btn add orange right" style="margin-right: 15px;" onclick="return confirm(\'Are you sure you want to duplicate this entity?\')">Duplicate</a>';
    }

    /**
     * Compose an anchor tag with the purpose of guiding the user to the delete route of the same entity.
     *
     * @param string $entity
     * @param int $id
     * @return string
     */
    public static function deleteButton($entity, $id)
    {
        return '<a href="' . Url::getCurrentUrl() . (!Server::exists('QUERY_STRING') ? '?' : '&') . 'delete-id=' . $id . '&delete-entity=' . $entity . '" class="btn delete red" onclick="return confirm(\'Are you sure you want to delete this entry?\')">Delete</a>';
    }

    /**
     * Compose an anchor tag with the purpose of guiding the user to the view route of the same entity.
     *
     * @param int $id
     * @return string
     */
    public static function viewButton($id)
    {
        return '<a href="' . Arr::first(explode('?', Url::getCurrentUrl())) . '/edit?id=' . $id . '" class="btn view orange margin-right">View</a>';
    }

    /**
     * Compose an anchor tag with the purpose of guiding the user to the view route of the same entity.
     *
     * @param int $id
     * @return string
     */
    public static function detailsButton($id)
    {
        return '<a href="' . Arr::first(explode('/index', Url::getCurrentUrl())) . '/view?id=' . $id . '" class="btn view orange margin-right">View</a>';
    }

    /**
     * Compose an anchor tag with the purpose of guiding the user to the save form route of the same entity.
     *
     * @return string
     */
    public static function saveButton()
    {
        return '<a href="#" id="save" class="btn save blue right" onclick="saveSubmit(event)" style="margin-left: 15px;">Save</a>';
    }

    /**
     * Compose an anchor tag with the purpose of guiding the user to the save form route of the same entity and redirecting to the same page.
     *
     * @return string
     */
    public static function saveStayButton()
    {
        return '<a href="#" id="save-stay" class="btn save-stay green right" onclick="saveStaySubmit(event)">Save & Stay</a>';
    }

    /**
     * Compose an anchor tag with the purpose of guiding the user to the cancel route of the same entity.
     *
     * @return string
     */
    public static function cancelButton()
    {
        $array = explode('/', Url::getUrl());

        array_shift($array);
        array_pop($array);

        return '<a href="/' . implode('/', $array) . '/index" class="btn delete red left">Cancel</a>';
    }

    /**
     * Compose an anchor tag with the purpose of guiding the user to the cancel route of the same entity.
     *
     * @return string
     */
    public static function continueButton()
    {
        $array = explode('/', Url::getUrl());

        array_shift($array);
        array_pop($array);

        return '<a href="/' . implode('/', $array) . '/index" class="btn save blue right">Continue</a>';
    }

    /**
     * Compose the breadcrumbs partial.
     *
     * @param array|string|null $breadcrumbs
     * @return string
     */
    public static function breadcrumbs($breadcrumbs = null)
    {
        if (!$breadcrumbs) {
            return null;
        }

        if (is_string($breadcrumbs)) {
            return '<span style="margin-left: 12px;">' . $breadcrumbs . '</span>';
        }

        $current    = array_pop($breadcrumbs);
        $result     = '';

        if (empty($breadcrumbs)) {
            $result .= '<span style="margin-left: 12px;">' . $current . '</span>';
        } else {
            foreach ($breadcrumbs as $breadcrumb) {
                $result .= '<a>' . $breadcrumb . '</a>';
            }

            $result .= '<span>' . $current . '</span>';
        }

        return $result;
    }

    /**
     * Compose the records partial.
     *
     * @param int $limit
     * @param int $items
     * @return string
     */
    public static function recordsNumber($limit = 0, $items = 0)
    {
        return '- showing ' . $limit . ' out of ' . ($items ? $items : $limit) . ' records';
    }

    /**
     * Display an image on the edit admin page.
     *
     * @param string $imageField
     * @param string $imageName
     * @return string
     */
    public static function showImage($imageField, $imageName)
    {
        $parts      = explode('.', $imageName);
        $imageName  = $parts[0] . '_default.' . $parts[1];

        return '
            <div class="row-container">
                <p>Current Image</p>
                <div class="label-container">
                    <a href="' . Url::getCurrentUrl() . '&field=' . $imageField . '" class="image-delete">Remove</a>
                    <br /><br /><br />
                    <img src="' . Dir::getUploads() . '/image/' . $imageName . '" />
                </div>
            </div>
        ';
    }

    /**
     * Display a meta image on the edit admin page.
     *
     * @param string $imageField
     * @param string $imageName
     * @return string
     */
    public static function showMetaImage($imageField, $imageName)
    {
        $parts      = explode('.', $imageName);
        $imageName  = $parts[0] . '_default.' . $parts[1];

        return '
            <div class="row-container">
                <p>Current Image</p>
                <div class="label-container">
                    <a href="' . Url::getCurrentUrl() . '&field=' . $imageField . '" class="meta-image-delete">Remove</a>
                    <img src="' . Dir::getUploads() . '/image/' . $imageName . '" />
                </div>
            </div>
        ';
    }

    /**
     * Display a video on the edit admin page.
     *
     * @param string $videoField
     * @param string $videoName
     * @return string
     */
    public static function showVideo($videoField, $videoName)
    {
        return '
            <div class="row-container">
                <p>Current Video</p>
                <div class="label-container">
                    <a href="' . Url::getCurrentUrl() . '&field=' . $videoField . '" class="video-delete">Remove</a>
                    '. Video::display($videoName, 500, 350, 'float: right;') .'
                </div>
            </div>
        ';
    }

    /**
     * Display a meta video on the edit admin page.
     *
     * @param string $videoField
     * @param string $videoName
     * @return string
     */
    public static function showMetaVideo($videoField, $videoName)
    {
        return '
            <div class="row-container">
                <p>Current Video</p>
                <div class="label-container">
                    <a href="' . Url::getCurrentUrl() . '&field=' . $videoField . '" class="meta-video-delete">Remove</a>
                    '. Video::display($videoName, 300, 200, 'float: right;') .'
                </div>
            </div>
        ';
    }

    /**
     * Display an audio on the edit admin page.
     *
     * @param string $audioField
     * @param string $audioName
     * @return string
     */
    public static function showAudio($audioField, $audioName)
    {
        return '
            <div class="row-container">
                <p>Current Audio</p>
                <div class="label-container">
                    <a href="' . Url::getCurrentUrl() . '&field=' . $audioField . '" class="audio-delete">Remove</a>
                    '. Audio::display($audioName, ['style' => 'margin-top: 50px; width: 300px; float: right']) .'
                </div>
            </div>
        ';
    }

    /**
     * Display a meta audtio on the edit admin page.
     *
     * @param string $audioField
     * @param string $audioName
     * @return string
     */
    public static function showMetaAudio($audioField, $audioName)
    {
        return '
            <div class="row-container">
                <p>Current Audio</p>
                <div class="label-container">
                    <a href="' . Url::getCurrentUrl() . '&field=' . $audioField . '" class="meta-audio-delete">Remove</a>
                    '. Audio::display($audioName, ['style' => 'margin-top: 50px; width: 300px; float: right']) .'
                </div>
            </div>
        ';
    }

    /**
     * Display a file on the edit admin page.
     *
     * @param string $fileField
     * @param string $fileName
     * @return string
     */
    public static function showFile($fileField, $fileName)
    {
        return '
            <div class="row-container">
                <p>Current File</p>
                <div class="label-container">
                    <a href="' . Url::getCurrentUrl() . '&field=' . $fileField . '" class="file-delete">Remove</a>
                    <a href="'. Dir::getUploads() .'/file/'. $fileName .'" target="_blank" class="btn black view right" style="margin-right: 110px;">View File</a>
                </div>
            </div>
        ';
    }

    /**
     * Display a meta file on the edit admin page.
     *
     * @param string $fileField
     * @param string $fileName
     * @return string
     */
    public static function showMetaFile($fileField, $fileName)
    {
        return '
            <div class="row-container">
                <p>Current File</p>
                <div class="label-container">
                    <a href="' . Url::getCurrentUrl() . '&field=' . $fileField . '" class="meta-file-delete">Remove</a>
                    <a href="'. Dir::getUploads() .'/file/'. $fileName .'" target="_blank" class="btn black view right" style="margin-right: 110px;">View File</a>
                </div>
            </div>
        ';
    }

    /**
     * Filter the records by the specified needs.
     *
     * @param array $sortFields
     * @return string
     * @throws Exception
     */
    public static function recordsFilter($sortFields)
    {
        if (!is_array($sortFields) || empty($sortFields)) {
            throw new Exception('The SORTFIELDS parameter must be a non-empty array');
        }

        $result = '';

        $result .= '<form action="#" method="get">';
            $result .= '<input type="text" name="data[search]" placeholder="Search" value="' . Get::get('data.search') . '" />';

            $result .= '<span class="sort">Sort by:</span>';
            $result .= '<select name="data[sort][]">';

            foreach($sortFields as $key => $val) {
                $result .= '<option value="' . $key . '"' . ($key == Get::get('data.sort.0') ? ' selected' : '') . '>' . $val . '</option>';
            }

            $result .= '</select>';

            $result .= '<select name="data[order][]">';
                $result .= '<option value="ASC"' . (Get::get('data.order.0') == 'ASC' ? ' selected' : '') . '>ASC</option>';
                $result .= '<option value="DESC"' . (Get::get('data.order.0') == 'DESC' ? ' selected' : '') . '>DESC</option>';
            $result .= '</select>';

            $result .= '<input type="submit" name="search" value="" />';

            if (!empty($_GET['data'])) {
                $result .= '<a href="' . Url::getRawUrl() . '" class="clear"></a>';
            }

        $result .= '</form>';

        return $result;
    }

    /**
     * Filter the records by the specified needs.
     *
     * @param array $sortFields
     * @return string
     * @throws Exception
     */
    public static function pageRecordsFilter($sortFields)
    {
        if (!is_array($sortFields) || empty($sortFields)) {
            throw new Exception('The SORTFIELDS parameter must be a non-empty array');
        }

        $result = '';

        $result .= '<form action="#" method="get">';
            $result .= '<input type="text" name="data[search]" placeholder="Search" value="' . Get::get('data.search') . '" />';

            $result .= '<select name="data[page_visible][]" class="desktopOnly">';
                $result .= '<option value=""' . (Get::get('data.page_visible.0') == '' ? ' selected' : '') . '>Visible</option>';
                $result .= '<option value="0"' . (Get::get('data.page_visible.0') == '0' ? ' selected' : '') . '>No</option>';
                $result .= '<option value="1"' . (Get::get('data.page_visible.0') == '1' ? ' selected' : '') . '>Yes</option>';
            $result .= '</select>';

            $result .= '<span class="sort">Sort by:</span>';

            $result .= '<select name="data[sort][]">';

            foreach($sortFields as $key => $val) {
                $result .= '<option value="' . $key . '"' . ($key == Get::get('data.sort.0') ? ' selected' : '') . '>' . $val . '</option>';
            }

            $result .= '</select>';

            $result .= '<select name="data[order][]">';
                $result .= '<option value="ASC"' . (Get::get('data.order.0') == 'ASC' ? ' selected' : '') . '>ASC</option>';
                $result .= '<option value="DESC"' . (Get::get('data.order.0') == 'DESC' ? ' selected' : '') . '>DESC</option>';
            $result .= '</select>';

            $result .= '<input type="submit" name="search" value="" />';

            if (!empty($_GET['data'])) {
                $result .= '<a href="' . Url::getRawUrl() . '" class="clear"></a>';
            }

        $result .= '</form>';

        return $result;
    }

    /**
     * Filter the records by the specified needs.
     *
     * @param array $sortFields
     * @return string
     * @throws Exception
     */
    public static function menuRecordsFilter($sortFields)
    {
        if (!is_array($sortFields) || empty($sortFields)) {
            throw new Exception('The SORTFIELDS parameter must be a non-empty array');
        }

        $result = '';

        $result .= '<form action="#" method="get">';
            $result .= '<input type="text" name="data[search]" placeholder="Search" value="' . Get::get('data.search') . '" />';
            $result .= '<span class="sort">Sort by:</span>';

            $result .= '<select name="data[sort][]">';

            foreach($sortFields as $key => $val) {
                $result .= '<option value="' . $key . '"' . ($key == Get::get('data.sort.0') ? ' selected' : '') . '>' . $val . '</option>';
            }

            $result .= '</select>';

            $result .= '<select name="data[order][]">';
                $result .= '<option value="ASC"' . (Get::get('data.order.0') == 'ASC' ? ' selected' : '') . '>ASC</option>';
                $result .= '<option value="DESC"' . (Get::get('data.order.0') == 'DESC' ? ' selected' : '') . '>DESC</option>';
            $result .= '</select>';

            $result .= '<input type="hidden" name="location" value="' . Get::get('location') . '" />';
            $result .= '<input type="hidden" name="parent_id" value="' . Get::get('parent_id') . '" />';
            $result .= '<input type="submit" name="search" value="" />';

            if (!empty($_GET['data'])) {
                $result .= '<a href="' . Url::getRawUrl() . '?location=' . Get::get('location') . '&parent_id=' . Get::get('parent_id') . '" class="clear"></a>';
            }

        $result .= '</form>';

        return $result;
    }

    /**
     * Filter the records by the specified needs.
     *
     * @param array $sortFields
     * @return string
     * @throws Exception
     */
    public static function blockRecordsFilter($sortFields)
    {
        if (!is_array($sortFields) || empty($sortFields)) {
            throw new Exception('The SORTFIELDS parameter must be a non-empty array');
        }

        $result = '';

        $result .= '<form action="#" method="get">';
            $result .= '<input type="text" name="data[search]" placeholder="Search" value="' . Get::get('data.search') . '" />';

            $result .= '<select name="data[types][]" class="desktopOnly">';
                $result .= '<option value="">Type</option>';

                foreach (Behavior_Block::getTypes() as $type => $label) {
                    $result .= '<option value="' . $type . '"' . (Get::get('data.types.0') == $type ? ' selected' : '') . '>' . $label . '</option>';
                }

            $result .= '</select>';
            $result .= '<span class="sort">Sort by:</span>';
            $result .= '<select name="data[sort][]">';

            foreach($sortFields as $key => $val) {
                $result .= '<option value="' . $key . '"' . ($key == Get::get('data.sort.0') ? ' selected' : '') . '>' . $val . '</option>';
            }

            $result .= '</select>';

            $result .= '<select name="data[order][]">';
                $result .= '<option value="ASC"' . (Get::get('data.order.0') == 'ASC' ? ' selected' : '') . '>ASC</option>';
                $result .= '<option value="DESC"' . (Get::get('data.order.0') == 'DESC' ? ' selected' : '') . '>DESC</option>';
            $result .= '</select>';

            $result .= '<input type="submit" name="search" value="" />';

            if (!empty($_GET['data'])) {
                $result .= '<a href="' . Url::getRawUrl() . '" class="clear"></a>';
            }

        $result .= '</form>';

        return $result;
    }

    /**
     * Filter the records by the specified needs.
     *
     * @param array $sortFields
     * @return string
     * @throws Exception
     */
    public static function adminUsersRecordsFilter($sortFields)
    {
        if (!is_array($sortFields) || empty($sortFields)) {
            throw new Exception('The SORTFIELDS parameter must be a non-empty array');
        }

        $result = '';

        $result .= '<form action="#" method="get">';
        $result .= '<input type="text" name="data[search]" placeholder="Search" value="' . Get::get('data.search') . '" />';

        $result .= '<select name="data[admin_user_super][]" class="desktopOnly">';
            $result .= '<option value=""' . (Get::get('data.admin_user_super.0') == '' ? ' selected' : '') . '>Super Admin</option>';
            $result .= '<option value="0"' . (Get::get('data.admin_user_super.0') == '0' ? ' selected' : '') . '>No</option>';
            $result .= '<option value="1"' . (Get::get('data.admin_user_super.0') == '1' ? ' selected' : '') . '>Yes</option>';
        $result .= '</select>';

        $result .= '<span class="sort">Sort by:</span>';
        $result .= '<select name="data[sort][]">';

        foreach($sortFields as $key => $val) {
            $result .= '<option value="' . $key . '"' . ($key == Get::get('data.sort.0') ? ' selected' : '') . '>' . $val . '</option>';
        }

        $result .= '</select>';

        $result .= '<select name="data[order][]">';
            $result .= '<option value="ASC"' . (Get::get('data.order.0') == 'ASC' ? ' selected' : '') . '>ASC</option>';
            $result .= '<option value="DESC"' . (Get::get('data.order.0') == 'DESC' ? ' selected' : '') . '>DESC</option>';
        $result .= '</select>';

        $result .= '<input type="submit" name="search" value="" />';

        if (!empty($_GET['data'])) {
            $result .= '<a href="' . Url::getRawUrl() . '" class="clear"></a>';
        }

        $result .= '</form>';

        return $result;
    }

    /**
     * Filter the records by the specified needs.
     *
     * @param array $sortFields
     * @return string
     * @throws Exception
     */
    public static function usersRecordsFilter($sortFields)
    {
        if (!is_array($sortFields) || empty($sortFields)) {
            throw new Exception('The SORTFIELDS parameter must be a non-empty array');
        }

        $result = '';

        $result .= '<form action="#" method="get">';
        $result .= '<input type="text" name="data[search]" placeholder="Search" value="' . Get::get('data.search') . '" />';

        $result .= '<select name="data[users_user_type][]" class="desktopOnly">';
        $result .= '<option value=""' . (Get::get('data.users_user_type.0') == '' ? ' selected' : '') . '>Type</option>';

        foreach (Users_Model_User::$types as $type => $name) {
            $result .= '<option value="' . $type . '"' . (Get::get('data.users_user_type.0') == $type ? ' selected' : '') . '>' . $name . '</option>';
        }

        $result .= '</select>';

        $result .= '<span class="sort">Sort by:</span>';
        $result .= '<select name="data[sort][]">';

        foreach($sortFields as $key => $val) {
            $result .= '<option value="' . $key . '"' . ($key == Get::get('data.sort.0') ? ' selected' : '') . '>' . $val . '</option>';
        }

        $result .= '</select>';

        $result .= '<select name="data[order][]">';
            $result .= '<option value="ASC"' . (Get::get('data.order.0') == 'ASC' ? ' selected' : '') . '>ASC</option>';
            $result .= '<option value="DESC"' . (Get::get('data.order.0') == 'DESC' ? ' selected' : '') . '>DESC</option>';
        $result .= '</select>';

        $result .= '<input type="submit" name="search" value="" />';

        if (!empty($_GET['data'])) {
            $result .= '<a href="' . Url::getRawUrl() . '" class="clear"></a>';
        }

        $result .= '</form>';

        return $result;
    }

    /**
     * Filter the records by the specified needs.
     *
     * @param array $sortFields
     * @return string
     * @throws Exception
     */
    public static function blogPostsRecordsFilter($sortFields)
    {
        if (!is_array($sortFields) || empty($sortFields)) {
            throw new Exception('The SORTFIELDS parameter must be a non-empty array');
        }

        $author     = new Blog_Model_Author();
        $category   = new Blog_Model_Category();

        $result = '';

        $result .= '<form action="#" method="get">';
            $result .= '<input type="text" name="data[search]" placeholder="Search" value="' . Get::get('data.search') . '" />';

            /*if ($category->getCollection()->getCount() > 0) {
                $result .= '<select name="data[blog_posts_category][]" class="desktopOnly">';
                $result .= '<option value="">Category</option>';

                foreach ($category->getCollection() as $category) {
                    $result .= '<option value="' . $category->getId() . '"' . (Get::get('data.blog_posts_category.0') == $category->getId() ? ' selected' : '') . '>' . $category->getName() . '</option>';
                }

                $result .= '</select>';
            }*/

            if ($author->getCollection()->getCount() > 0) {
                $result .= '<select name="data[blog_posts_author][]" class="desktopOnly">';
                $result .= '<option value="">Author</option>';

                foreach ($author->getCollection() as $author) {
                    $result .= '<option value="' . $author->getId() . '"' . (Get::get('data.blog_posts_author.0') == $author->getId() ? ' selected' : '') . '>' . $author->getName() . '</option>';
                }

                $result .= '</select>';
            }

            $result .= '<span class="sort">Sort by:</span>';
            $result .= '<select name="data[sort][]">';

            foreach($sortFields as $key => $val) {
                $result .= '<option value="' . $key . '"' . ($key == Get::get('data.sort.0') ? ' selected' : '') . '>' . $val . '</option>';
            }

            $result .= '</select>';

            $result .= '<select name="data[order][]">';
                $result .= '<option value="ASC"' . (Get::get('data.order.0') == 'ASC' ? ' selected' : '') . '>ASC</option>';
                $result .= '<option value="DESC"' . (Get::get('data.order.0') == 'DESC' ? ' selected' : '') . '>DESC</option>';
            $result .= '</select>';

            $result .= '<input type="submit" name="search" value="" />';

            if (!empty($_GET['data'])) {
                $result .= '<a href="' . Url::getRawUrl() . '" class="clear"></a>';
            }

        $result .= '</form>';

        return $result;
    }

    /**
     * Filter the records by the specified needs.
     *
     * @param array $sortFields
     * @return string
     * @throws Exception
     */
    public static function blogCommentsRecordsFilter($sortFields)
    {
        if (!is_array($sortFields) || empty($sortFields)) {
            throw new Exception('The SORTFIELDS parameter must be a non-empty array');
        }

        $post = new Blog_Model_Post();

        $result = '';

        $result .= '<form action="#" method="get">';
        $result .= '<input type="text" name="data[search]" placeholder="Search" value="' . Get::get('data.search') . '" />';

        if ($post->getCollection()->getCount() > 0) {
            $result .= '<select name="data[blog_posts_post][]" class="desktopOnly">';
            $result .= '<option value="">Post</option>';

            foreach ($post->getCollection() as $post) {
                $result .= '<option value="' . $post->getId() . '"' . (Get::get('data.blog_posts_post.0') == $post->getId() ? ' selected' : '') . '>' . $post->getName() . '</option>';
            }

            $result .= '</select>';
        }

        $result .= '<select name="data[blog_posts_flag][]" class="desktopOnly">';
            $result .= '<option value=""' . (Get::get('data.blog_posts_flag.0') == '' ? ' selected' : '') . '>Flagged</option>';
            $result .= '<option value="0"' . (Get::get('data.blog_posts_flag.0') == '0' ? ' selected' : '') . '>No</option>';
            $result .= '<option value="1"' . (Get::get('data.blog_posts_flag.0') == '1' ? ' selected' : '') . '>Yes</option>';
        $result .= '</select>';

        $result .= '<span class="sort">Sort by:</span>';
        $result .= '<select name="data[sort][]">';

        foreach($sortFields as $key => $val) {
            $result .= '<option value="' . $key . '"' . ($key == Get::get('data.sort.0') ? ' selected' : '') . '>' . $val . '</option>';
        }

        $result .= '</select>';

        $result .= '<select name="data[order][]">';
        $result .= '<option value="ASC"' . (Get::get('data.order.0') == 'ASC' ? ' selected' : '') . '>ASC</option>';
        $result .= '<option value="DESC"' . (Get::get('data.order.0') == 'DESC' ? ' selected' : '') . '>DESC</option>';
        $result .= '</select>';

        $result .= '<input type="submit" name="search" value="" />';

        if (!empty($_GET['data'])) {
            $result .= '<a href="' . Url::getRawUrl() . '" class="clear"></a>';
        }

        $result .= '</form>';

        return $result;
    }

    /**
     * Filter the records by the specified needs.
     *
     * @param array $sortFields
     * @return string
     * @throws Exception
     */
    public static function shopOrdersRecordsFilter($sortFields)
    {
        if (!is_array($sortFields) || empty($sortFields)) {
            throw new Exception('The SORTFIELDS parameter must be a non-empty array');
        }

        $result = '';

        $result .= '<form action="#" method="get">';
        $result .= '<input type="text" name="data[search]" placeholder="Search" value="' . Get::get('data.search') . '" />';

        $result .= '<select name="data[shop_orders_status][]" class="desktopOnly">';
        $result .= '<option value=""' . (Get::get('data.shop_orders_status.0') == '' ? ' selected' : '') . '>Status</option>';

        foreach (Shop_Model_Order::$statuses as $status => $name) {
            $result .= '<option value="' . $status . '"' . (Get::get('data.shop_orders_status.0') == $status && Get::get('data.shop_orders_status.0') != '' ? ' selected' : '') . '>' . $name . '</option>';
        }

        $result .= '</select>';

        $result .= '<select name="data[shop_orders_viewed][]" class="desktopOnly">';
        $result .= '<option value=""' . (Get::get('data.shop_orders_viewed.0') == '' ? ' selected' : '') . '>Viewed</option>';
        $result .= '<option value="0"' . (Get::get('data.shop_orders_viewed.0') == '0' ? ' selected' : '') . '>No</option>';
        $result .= '<option value="1"' . (Get::get('data.shop_orders_viewed.0') == '1' ? ' selected' : '') . '>Yes</option>';
        $result .= '</select>';

        $result .= '<span class="sort">Sort by:</span>';
        $result .= '<select name="data[sort][]">';

        foreach($sortFields as $key => $val) {
            $result .= '<option value="' . $key . '"' . ($key == Get::get('data.sort.0') ? ' selected' : '') . '>' . $val . '</option>';
        }

        $result .= '</select>';

        $result .= '<select name="data[order][]">';
        $result .= '<option value="ASC"' . (Get::get('data.order.0') == 'ASC' ? ' selected' : '') . '>ASC</option>';
        $result .= '<option value="DESC"' . (Get::get('data.order.0') == 'DESC' ? ' selected' : '') . '>DESC</option>';
        $result .= '</select>';

        $result .= '<input type="submit" name="search" value="" />';

        if (!empty($_GET['data'])) {
            $result .= '<a href="' . Url::getRawUrl() . '" class="clear"></a>';
        }

        $result .= '</form>';

        return $result;
    }

    /**
     * Filter the records by the specified needs.
     *
     * @param array $sortFields
     * @return string
     * @throws Exception
     */
    public static function shopCategoriesRecordsFilter($sortFields)
    {
        if (!is_array($sortFields) || empty($sortFields)) {
            throw new Exception('The SORTFIELDS parameter must be a non-empty array');
        }

        $type = new Shop_Model_Category_Type();

        $result = '';

        $result .= '<form action="#" method="get">';
        $result .= '<input type="text" name="data[search]" placeholder="Search" value="' . Get::get('data.search') . '" />';

        if ($type->getCollection()->getCount() > 0) {
            $result .= '<select name="data[shop_product_types][]" class="desktopOnly">';
            $result .= '<option value="">Type</option>';

            foreach ($type->getCollection() as $type) {
                $result .= '<option value="' . $type->getId() . '"' . (Get::get('data.shop_product_types.0') == $type->getId() ? ' selected' : '') . '>' . $type->getName() . '</option>';
            }

            $result .= '</select>';
        }

        $result .= '<span class="sort">Sort by:</span>';
        $result .= '<select name="data[sort][]">';

        foreach($sortFields as $key => $val) {
            $result .= '<option value="' . $key . '"' . ($key == Get::get('data.sort.0') ? ' selected' : '') . '>' . $val . '</option>';
        }

        $result .= '</select>';

        $result .= '<select name="data[order][]">';
        $result .= '<option value="ASC"' . (Get::get('data.order.0') == 'ASC' ? ' selected' : '') . '>ASC</option>';
        $result .= '<option value="DESC"' . (Get::get('data.order.0') == 'DESC' ? ' selected' : '') . '>DESC</option>';
        $result .= '</select>';

        $result .= '<input type="submit" name="search" value="" />';

        if (!empty($_GET['data'])) {
            $result .= '<a href="' . Url::getRawUrl() . '" class="clear"></a>';
        }

        $result .= '</form>';

        return $result;
    }

    /**
     * Filter the records by the specified needs.
     *
     * @param array $sortFields
     * @return string
     * @throws Exception
     */
    public static function shopProductsRecordsFilter($sortFields)
    {
        if (!is_array($sortFields) || empty($sortFields)) {
            throw new Exception('The SORTFIELDS parameter must be a non-empty array');
        }

        $result = '';

        $result .= '<form action="#" method="get">';
        $result .= '<input type="text" name="data[search]" placeholder="Search" value="' . Get::get('data.search') . '" />';

        if (Shop_Model_Category::getInstance()->getCollection()->getCount() > 0) {
            $result .= '<select name="data[shop_products_category][]" class="desktopOnly">';
            $result .= '<option value="">Category</option>';

            foreach (Shop_Model_Category::getInstance()->getCollection() as $category) {
                $result .= '<option value="' . $category->getId() . '"' . (Get::get('data.shop_products_category.0') == $category->getId() ? ' selected' : '') . '>' . $category->getName() . '</option>';
            }

            $result .= '</select>';
        }

        if (Shop_Model_Vendor::getInstance()->getCollection()->getCount() > 0) {
            $result .= '<select name="data[shop_products_vendor][]" class="desktopOnly">';
            $result .= '<option value="">Vendor</option>';

            foreach (Shop_Model_Vendor::getInstance()->getCollection() as $vendor) {
                $result .= '<option value="' . $vendor->getId() . '"' . (Get::get('data.shop_products_vendor.0') == $vendor->getId() ? ' selected' : '') . '>' . $vendor->getName() . '</option>';
            }

            $result .= '</select>';
        }

        $result .= '<select name="data[shop_products_active][]" class="desktopOnly">';
        $result .= '<option value=""' . (Get::get('data.shop_products_active.0') == '' ? ' selected' : '') . '>Active</option>';

        foreach (Shop_Model_Product::$availability as $type => $name) {
            $result .= '<option value="' . $type . '"' . (Get::get('data.shop_products_active.0') == $type && Get::get('data.shop_products_active.0') != '' ? ' selected' : '') . '>' . $name . '</option>';
        }

        $result .= '</select>';

        $result .= '<select name="data[shop_products_stock][]" class="desktopOnly">';
        $result .= '<option value=""' . (Get::get('data.shop_products_stock.0') == '' ? ' selected' : '') . '>Stock</option>';

        foreach (Shop_Model_Product::$stocks as $stock => $name) {
            $result .= '<option value="' . $stock . '"' . (Get::get('data.shop_products_stock.0') == $stock && Get::get('data.shop_products_stock.0') != '' ? ' selected' : '') . '>' . $name . '</option>';
        }

        $result .= '</select>';

        $result .= '<span class="sort">Sort by:</span>';
        $result .= '<select name="data[sort][]">';

        foreach($sortFields as $key => $val) {
            $result .= '<option value="' . $key . '"' . ($key == Get::get('data.sort.0') ? ' selected' : '') . '>' . $val . '</option>';
        }

        $result .= '</select>';

        $result .= '<select name="data[order][]">';
        $result .= '<option value="ASC"' . (Get::get('data.order.0') == 'ASC' ? ' selected' : '') . '>ASC</option>';
        $result .= '<option value="DESC"' . (Get::get('data.order.0') == 'DESC' ? ' selected' : '') . '>DESC</option>';
        $result .= '</select>';

        $result .= '<input type="submit" name="search" value="" />';

        if (!empty($_GET['data'])) {
            $result .= '<a href="' . Url::getRawUrl() . '" class="clear"></a>';
        }

        $result .= '</form>';

        return $result;
    }

    /**
     * Filter the records by the specified needs.
     *
     * @param array $sortFields
     * @return string
     * @throws Exception
     */
    public static function shopTaxesRecordsFilter($sortFields)
    {
        if (!is_array($sortFields) || empty($sortFields)) {
            throw new Exception('The SORTFIELDS parameter must be a non-empty array');
        }

        $result = '';

        $result .= '<form action="#" method="get">';
        $result .= '<input type="text" name="data[search]" placeholder="Search" value="' . Get::get('data.search') . '" />';

        $result .= '<select name="data[shop_taxes_applicability][]" class="desktopOnly">';
        $result .= '<option value=""' . (Get::get('data.shop_taxes_applicability.0') == '' ? ' selected' : '') . '>Applicability</option>';

        foreach (Shop_Model_Product_Tax::$applicabilities as $applicability => $name) {
            $result .= '<option value="' . $applicability . '"' . (Get::get('data.shop_taxes_applicability.0') == $applicability ? ' selected' : '') . '>' . $name . '</option>';
        }

        $result .= '</select>';

        $result .= '<select name="data[shop_taxes_type][]" class="desktopOnly">';
        $result .= '<option value=""' . (Get::get('data.shop_taxes_type.0') == '' ? ' selected' : '') . '>Type</option>';

        foreach (Shop_Model_Product_Tax::$types as $type => $name) {
            $result .= '<option value="' . $type . '"' . (Get::get('data.shop_taxes_type.0') == $type ? ' selected' : '') . '>' . $name . '</option>';
        }

        $result .= '</select>';

        $result .= '<select name="data[shop_taxes_active][]" class="desktopOnly">';
        $result .= '<option value=""' . (Get::get('data.shop_taxes_active.0') == '' ? ' selected' : '') . '>Active</option>';
        $result .= '<option value="0"' . (Get::get('data.shop_taxes_active.0') == '0' ? ' selected' : '') . '>No</option>';
        $result .= '<option value="1"' . (Get::get('data.shop_taxes_active.0') == '1' ? ' selected' : '') . '>Yes</option>';
        $result .= '</select>';

        $result .= '<span class="sort">Sort by:</span>';
        $result .= '<select name="data[sort][]">';

        foreach($sortFields as $key => $val) {
            $result .= '<option value="' . $key . '"' . ($key == Get::get('data.sort.0') ? ' selected' : '') . '>' . $val . '</option>';
        }

        $result .= '</select>';

        $result .= '<select name="data[order][]">';
        $result .= '<option value="ASC"' . (Get::get('data.order.0') == 'ASC' ? ' selected' : '') . '>ASC</option>';
        $result .= '<option value="DESC"' . (Get::get('data.order.0') == 'DESC' ? ' selected' : '') . '>DESC</option>';
        $result .= '</select>';

        $result .= '<input type="submit" name="search" value="" />';

        if (!empty($_GET['data'])) {
            $result .= '<a href="' . Url::getRawUrl() . '" class="clear"></a>';
        }

        $result .= '</form>';

        return $result;
    }

    /**
     * Filter the records by the specified needs.
     *
     * @param array $sortFields
     * @return string
     * @throws Exception
     */
    public static function shopDiscountsRecordsFilter($sortFields)
    {
        if (!is_array($sortFields) || empty($sortFields)) {
            throw new Exception('The SORTFIELDS parameter must be a non-empty array');
        }

        $result = '';

        $result .= '<form action="#" method="get">';
        $result .= '<input type="text" name="data[search]" placeholder="Search" value="' . Get::get('data.search') . '" />';

        $result .= '<select name="data[shop_discounts_applicability][]" class="desktopOnly">';
        $result .= '<option value=""' . (Get::get('data.shop_discounts_applicability.0') == '' ? ' selected' : '') . '>Applicability</option>';

        foreach (Shop_Model_Product_Discount::$applicabilities as $applicability => $name) {
            $result .= '<option value="' . $applicability . '"' . (Get::get('data.shop_discounts_applicability.0') == $applicability ? ' selected' : '') . '>' . $name . '</option>';
        }

        $result .= '</select>';

        $result .= '<select name="data[shop_discounts_type][]" class="desktopOnly">';
        $result .= '<option value=""' . (Get::get('data.shop_discounts_type.0') == '' ? ' selected' : '') . '>Type</option>';

        foreach (Shop_Model_Product_Discount::$types as $type => $name) {
            $result .= '<option value="' . $type . '"' . (Get::get('data.shop_discounts_type.0') == $type ? ' selected' : '') . '>' . $name . '</option>';
        }

        $result .= '</select>';
        $result .= '<select name="data[shop_discounts_active][]" class="desktopOnly">';
        $result .= '<option value=""' . (Get::get('data.shop_discounts_active.0') == '' ? ' selected' : '') . '>Active</option>';
        $result .= '<option value="0"' . (Get::get('data.shop_discounts_active.0') == '0' ? ' selected' : '') . '>No</option>';
        $result .= '<option value="1"' . (Get::get('data.shop_discounts_active.0') == '1' ? ' selected' : '') . '>Yes</option>';
        $result .= '</select>';

        $result .= '<span class="sort">Sort by:</span>';
        $result .= '<select name="data[sort][]">';

        foreach($sortFields as $key => $val) {
            $result .= '<option value="' . $key . '"' . ($key == Get::get('data.sort.0') ? ' selected' : '') . '>' . $val . '</option>';
        }

        $result .= '</select>';

        $result .= '<select name="data[order][]">';
        $result .= '<option value="ASC"' . (Get::get('data.order.0') == 'ASC' ? ' selected' : '') . '>ASC</option>';
        $result .= '<option value="DESC"' . (Get::get('data.order.0') == 'DESC' ? ' selected' : '') . '>DESC</option>';
        $result .= '</select>';

        $result .= '<input type="submit" name="search" value="" />';

        if (!empty($_GET['data'])) {
            $result .= '<a href="' . Url::getRawUrl() . '" class="clear"></a>';
        }

        $result .= '</form>';

        return $result;
    }

    /**
     * Filter the records by the specified needs.
     *
     * @param array $sortFields
     * @return string
     * @throws Exception
     */
    public static function shopVouchersRecordsFilter($sortFields)
    {
        if (!is_array($sortFields) || empty($sortFields)) {
            throw new Exception('The SORTFIELDS parameter must be a non-empty array');
        }

        $result = '';

        $result .= '<form action="#" method="get">';
        $result .= '<input type="text" name="data[search]" placeholder="Search" value="' . Get::get('data.search') . '" />';

        $result .= '<select name="data[shop_vouchers_type][]" class="desktopOnly">';
        $result .= '<option value=""' . (Get::get('data.shop_vouchers_type.0') == '' ? ' selected' : '') . '>Type</option>';

        foreach (Shop_Model_Order_Voucher::$types as $type => $name) {
            $result .= '<option value="' . $type . '"' . (Get::get('data.shop_vouchers_type.0') == $type ? ' selected' : '') . '>' . $name . '</option>';
        }

        $result .= '</select>';
        $result .= '<select name="data[shop_vouchers_active][]" class="desktopOnly">';
        $result .= '<option value=""' . (Get::get('data.shop_vouchers_active.0') == '' ? ' selected' : '') . '>Active</option>';
        $result .= '<option value="0"' . (Get::get('data.shop_vouchers_active.0') == '0' ? ' selected' : '') . '>No</option>';
        $result .= '<option value="1"' . (Get::get('data.shop_vouchers_active.0') == '1' ? ' selected' : '') . '>Yes</option>';
        $result .= '</select>';

        $result .= '<span class="sort">Sort by:</span>';
        $result .= '<select name="data[sort][]">';

        foreach($sortFields as $key => $val) {
            $result .= '<option value="' . $key . '"' . ($key == Get::get('data.sort.0') ? ' selected' : '') . '>' . $val . '</option>';
        }

        $result .= '</select>';

        $result .= '<select name="data[order][]">';
        $result .= '<option value="ASC"' . (Get::get('data.order.0') == 'ASC' ? ' selected' : '') . '>ASC</option>';
        $result .= '<option value="DESC"' . (Get::get('data.order.0') == 'DESC' ? ' selected' : '') . '>DESC</option>';
        $result .= '</select>';

        $result .= '<input type="submit" name="search" value="" />';

        if (!empty($_GET['data'])) {
            $result .= '<a href="' . Url::getRawUrl() . '" class="clear"></a>';
        }

        $result .= '</form>';

        return $result;
    }

    /**
     * Filter the records by the specified needs.
     *
     * @param array $sortFields
     * @return string
     * @throws Exception
     */
    public static function shopAttributesRecordsFilter($sortFields)
    {
        if (!is_array($sortFields) || empty($sortFields)) {
            throw new Exception('The SORTFIELDS parameter must be a non-empty array');
        }

        $result = '';

        $result .= '<form action="#" method="get">';
        $result .= '<input type="text" name="data[search]" placeholder="Search" value="' . Get::get('data.search') . '" />';

        $result .= '<select name="data[shop_attributes_set][]" class="desktopOnly">';
        $result .= '<option value=""' . (Get::get('data.shop_discounts_type.0') == '' ? ' selected' : '') . '>Set</option>';

        foreach (Shop_Model_Attribute_Set::getInstance()->getCollection() as $item) {
            $result .= '<option value="' . $item->getId() . '"' . (Get::get('data.shop_attributes_set.0') == $item->getId() ? ' selected' : '') . '>' . $item->getName() . '</option>';
        }

        $result .= '</select>';

        $result .= '<select name="data[shop_attributes_filterable][]" class="desktopOnly">';
        $result .= '<option value=""' . (Get::get('data.shop_attributes_filterable.0') == '' ? ' selected' : '') . '>Filterable</option>';
        $result .= '<option value="0"' . (Get::get('data.shop_attributes_filterable.0') == '0' ? ' selected' : '') . '>No</option>';
        $result .= '<option value="1"' . (Get::get('data.shop_attributes_filterable.0') == '1' ? ' selected' : '') . '>Yes</option>';
        $result .= '</select>';

        $result .= '<select name="data[shop_attributes_type][]" class="desktopOnly">';
        $result .= '<option value=""' . (Get::get('data.shop_attributes_type.0') == '' ? ' selected' : '') . '>Type</option>';

        foreach (Shop_Model_Attribute::$types as $type => $name) {
            $result .= '<option value="' . $type . '"' . (Get::get('data.shop_attributes_type.0') == $type ? ' selected' : '') . '>' . $name . '</option>';
        }

        $result .= '</select>';

        $result .= '<span class="sort">Sort by:</span>';
        $result .= '<select name="data[sort][]">';

        foreach($sortFields as $key => $val) {
            $result .= '<option value="' . $key . '"' . ($key == Get::get('data.sort.0') ? ' selected' : '') . '>' . $val . '</option>';
        }

        $result .= '</select>';

        $result .= '<select name="data[order][]">';
        $result .= '<option value="ASC"' . (Get::get('data.order.0') == 'ASC' ? ' selected' : '') . '>ASC</option>';
        $result .= '<option value="DESC"' . (Get::get('data.order.0') == 'DESC' ? ' selected' : '') . '>DESC</option>';
        $result .= '</select>';

        $result .= '<input type="submit" name="search" value="" />';

        if (!empty($_GET['data'])) {
            $result .= '<a href="' . Url::getRawUrl() . '" class="clear"></a>';
        }

        $result .= '</form>';

        return $result;
    }

    /**
     * Filter the records by the specified needs.
     *
     * @param array $sortFields
     * @return string
     * @throws Exception
     */
    public static function carModelsRecordsFilter($sortFields)
    {
        if (!is_array($sortFields) || empty($sortFields)) {
            throw new Exception('The SORTFIELDS parameter must be a non-empty array');
        }

        $result = '';

        $result .= '<form action="#" method="get">';
        $result .= '<input type="text" name="data[search]" placeholder="Search" value="' . Get::get('data.search') . '" />';

        $result .= '<select name="data[car_brand][]" class="desktopOnly">';
        $result .= '<option value=""' . (Get::get('data.car_brand.0') == '' ? ' selected' : '') . '>Brand</option>';

        foreach (Car_Model_Brand::getInstance()->order('ord')->getCollection() as $item) {
            $result .= '<option value="' . $item->getId() . '"' . (Get::get('data.car_brand.0') == $item->getId() ? ' selected' : '') . '>' . $item->getName() . '</option>';
        }

        $result .= '</select>';

        $result .= '<span class="sort">Sort by:</span>';
        $result .= '<select name="data[sort][]">';

        foreach($sortFields as $key => $val) {
            $result .= '<option value="' . $key . '"' . ($key == Get::get('data.sort.0') ? ' selected' : '') . '>' . $val . '</option>';
        }

        $result .= '</select>';

        $result .= '<select name="data[order][]">';
        $result .= '<option value="ASC"' . (Get::get('data.order.0') == 'ASC' ? ' selected' : '') . '>ASC</option>';
        $result .= '<option value="DESC"' . (Get::get('data.order.0') == 'DESC' ? ' selected' : '') . '>DESC</option>';
        $result .= '</select>';

        $result .= '<input type="submit" name="search" value="" />';

        if (!empty($_GET['data'])) {
            $result .= '<a href="' . Url::getRawUrl() . '" class="clear"></a>';
        }

        $result .= '</form>';

        return $result;
    }

    /**
     * Filter the records by the specified needs.
     *
     * @param array $sortFields
     * @return string
     * @throws Exception
     */
    public static function carColorsRecordsFilter($sortFields)
    {
        if (!is_array($sortFields) || empty($sortFields)) {
            throw new Exception('The SORTFIELDS parameter must be a non-empty array');
        }

        $result = '';

        $result .= '<form action="#" method="get">';
        $result .= '<input type="text" name="data[search]" placeholder="Search" value="' . Get::get('data.search') . '" />';

        $result .= '<select name="data[car_model][]" class="desktopOnly">';
        $result .= '<option value=""' . (Get::get('data.car_model.0') == '' ? ' selected' : '') . '>Model</option>';

        foreach (Car_Model_Model::getInstance()->order('name')->getCollection() as $item) {
            $result .= '<option value="' . $item->getId() . '"' . (Get::get('data.car_model.0') == $item->getId() ? ' selected' : '') . '>' . $item->getName() . '</option>';
        }

        $result .= '</select>';

        $result .= '<span class="sort">Sort by:</span>';
        $result .= '<select name="data[sort][]">';

        foreach($sortFields as $key => $val) {
            $result .= '<option value="' . $key . '"' . ($key == Get::get('data.sort.0') ? ' selected' : '') . '>' . $val . '</option>';
        }

        $result .= '</select>';

        $result .= '<select name="data[order][]">';
        $result .= '<option value="ASC"' . (Get::get('data.order.0') == 'ASC' ? ' selected' : '') . '>ASC</option>';
        $result .= '<option value="DESC"' . (Get::get('data.order.0') == 'DESC' ? ' selected' : '') . '>DESC</option>';
        $result .= '</select>';

        $result .= '<input type="submit" name="search" value="" />';

        if (!empty($_GET['data'])) {
            $result .= '<a href="' . Url::getRawUrl() . '" class="clear"></a>';
        }

        $result .= '</form>';

        return $result;
    }

    /**
     * Filter the records by the specified needs.
     *
     * @param array $sortFields
     * @return string
     * @throws Exception
     */
    public static function carStatsRecordsFilter($sortFields)
    {
        if (!is_array($sortFields) || empty($sortFields)) {
            throw new Exception('The SORTFIELDS parameter must be a non-empty array');
        }

        $result = '';

        $result .= '<form action="#" method="get">';
        $result .= '<input type="text" name="data[search]" placeholder="Search" value="' . Get::get('data.search') . '" />';

        $result .= '<select name="data[car_stats_important][]" class="desktopOnly">';
        $result .= '<option value=""' . (Get::get('data.car_stats_important.0') == '' ? ' selected' : '') . '>Important</option>';
        $result .= '<option value="0"' . (Get::get('data.car_stats_important.0') == '0' ? ' selected' : '') . '>No</option>';
        $result .= '<option value="1"' . (Get::get('data.car_stats_important.0') == '1' ? ' selected' : '') . '>Yes</option>';
        $result .= '</select>';

        $result .= '<span class="sort">Sort by:</span>';
        $result .= '<select name="data[sort][]">';

        foreach($sortFields as $key => $val) {
            $result .= '<option value="' . $key . '"' . ($key == Get::get('data.sort.0') ? ' selected' : '') . '>' . $val . '</option>';
        }

        $result .= '</select>';

        $result .= '<select name="data[order][]">';
        $result .= '<option value="ASC"' . (Get::get('data.order.0') == 'ASC' ? ' selected' : '') . '>ASC</option>';
        $result .= '<option value="DESC"' . (Get::get('data.order.0') == 'DESC' ? ' selected' : '') . '>DESC</option>';
        $result .= '</select>';

        $result .= '<input type="submit" name="search" value="" />';

        if (!empty($_GET['data'])) {
            $result .= '<a href="' . Url::getRawUrl() . '" class="clear"></a>';
        }

        $result .= '</form>';

        return $result;
    }

    /**
     * Filter the records by the specified needs.
     *
     * @param array $sortFields
     * @return string
     * @throws Exception
     */
    public static function carAdsRecordsFilter($sortFields)
    {
        if (!is_array($sortFields) || empty($sortFields)) {
            throw new Exception('The SORTFIELDS parameter must be a non-empty array');
        }

        $result = '';

        $result .= '<form action="#" method="get">';
        $result .= '<input type="text" name="data[search]" placeholder="Search" value="' . Get::get('data.search') . '" />';

        $result .= '<select name="data[car_ads_active][]" class="desktopOnly">';
        $result .= '<option value=""' . (Get::get('data.car_ads_active.0') == '' ? ' selected' : '') . '>Active</option>';
        $result .= '<option value="0"' . (Get::get('data.car_ads_active.0') == '0' ? ' selected' : '') . '>No</option>';
        $result .= '<option value="1"' . (Get::get('data.car_ads_active.0') == '1' ? ' selected' : '') . '>Yes</option>';
        $result .= '</select>';

        $result .= '<select name="data[car_ads_converted][]" class="desktopOnly">';
        $result .= '<option value=""' . (Get::get('data.car_ads_converted.0') == '' ? ' selected' : '') . '>Converted</option>';
        $result .= '<option value="0"' . (Get::get('data.car_ads_converted.0') == '0' ? ' selected' : '') . '>No</option>';
        $result .= '<option value="1"' . (Get::get('data.car_ads_converted.0') == '1' ? ' selected' : '') . '>Yes</option>';
        $result .= '</select>';

        if (Car_Model_Brand::getInstance()->getCollection()->getCount() > 0) {
            $result .= '<select name="data[car_ads_brand][]" class="desktopOnly">';
            $result .= '<option value="">Brand</option>';

            foreach (Car_Model_Brand::getInstance()->getCollection() as $item) {
                $result .= '<option value="' . $item->getId() . '"' . (Get::get('data.car_ads_brand.0') == $item->getId() ? ' selected' : '') . '>' . $item->getName() . '</option>';
            }

            $result .= '</select>';
        }

        if (Car_Model_Model::getInstance()->getCollection()->getCount() > 0) {
            $result .= '<select name="data[car_ads_model][]" class="desktopOnly">';
            $result .= '<option value="">Model</option>';

            foreach (Car_Model_Model::getInstance()->getCollection() as $item) {
                $result .= '<option value="' . $item->getId() . '"' . (Get::get('data.car_ads_model.0') == $item->getId() ? ' selected' : '') . '>' . $item->getName() . '</option>';
            }

            $result .= '</select>';
        }

        $result .= '<span class="sort">Sort by:</span>';
        $result .= '<select name="data[sort][]">';

        foreach($sortFields as $key => $val) {
            $result .= '<option value="' . $key . '"' . ($key == Get::get('data.sort.0') ? ' selected' : '') . '>' . $val . '</option>';
        }

        $result .= '</select>';

        $result .= '<select name="data[order][]">';
        $result .= '<option value="ASC"' . (Get::get('data.order.0') == 'ASC' ? ' selected' : '') . '>ASC</option>';
        $result .= '<option value="DESC"' . (Get::get('data.order.0') == 'DESC' ? ' selected' : '') . '>DESC</option>';
        $result .= '</select>';

        $result .= '<input type="submit" name="search" value="" />';

        if (!empty($_GET['data'])) {
            $result .= '<a href="' . Url::getRawUrl() . '" class="clear"></a>';
        }

        $result .= '</form>';

        return $result;
    }

    /**
     * Filter the records by the specified needs.
     *
     * @param array $sortFields
     * @return string
     * @throws Exception
     */
    public static function countriesRecordsFilter($sortFields)
    {
        if (!is_array($sortFields) || empty($sortFields)) {
            throw new Exception('The SORTFIELDS parameter must be a non-empty array');
        }

        $result = '';

        $result .= '<form action="#" method="get">';
        $result .= '<input type="text" name="data[search]" placeholder="Search" value="' . Get::get('data.search') . '" />';

        if (Default_Model_Country::getInstance()->getCollection()->getCount() > 0) {
            $result .= '<select name="data[counties_countries][]" class="desktopOnly">';
            $result .= '<option value="">Country</option>';

            foreach (Default_Model_Country::getInstance()->getCollection() as $item) {
                $result .= '<option value="' . $item->getId() . '"' . (Get::get('data.counties_countries.0') == $item->getId() ? ' selected' : '') . '>' . $item->getName() . '</option>';
            }

            $result .= '</select>';
        }

        $result .= '<span class="sort">Sort by:</span>';
        $result .= '<select name="data[sort][]">';

        foreach($sortFields as $key => $val) {
            $result .= '<option value="' . $key . '"' . ($key == Get::get('data.sort.0') ? ' selected' : '') . '>' . $val . '</option>';
        }

        $result .= '</select>';

        $result .= '<select name="data[order][]">';
        $result .= '<option value="ASC"' . (Get::get('data.order.0') == 'ASC' ? ' selected' : '') . '>ASC</option>';
        $result .= '<option value="DESC"' . (Get::get('data.order.0') == 'DESC' ? ' selected' : '') . '>DESC</option>';
        $result .= '</select>';

        $result .= '<input type="submit" name="search" value="" />';

        if (!empty($_GET['data'])) {
            $result .= '<a href="' . Url::getRawUrl() . '" class="clear"></a>';
        }

        $result .= '</form>';

        return $result;
    }

    /**
     * Filter the records by the specified needs.
     *
     * @param array $sortFields
     * @return string
     * @throws Exception
     */
    public static function citiesRecordsFilter($sortFields)
    {
        if (!is_array($sortFields) || empty($sortFields)) {
            throw new Exception('The SORTFIELDS parameter must be a non-empty array');
        }

        $result = '';

        $result .= '<form action="#" method="get">';
        $result .= '<input type="text" name="data[search]" placeholder="Search" value="' . Get::get('data.search') . '" />';

        if (Default_Model_County::getInstance()->getCollection()->getCount() > 0) {
            $result .= '<select name="data[cities_counties][]" class="desktopOnly">';
            $result .= '<option value="">County</option>';

            foreach (Default_Model_County::getInstance()->order('name', 'asc')->getCollection() as $item) {
                $result .= '<option value="' . $item->getId() . '"' . (Get::get('data.cities_counties.0') == $item->getId() ? ' selected' : '') . '>' . $item->getName() . '</option>';
            }

            $result .= '</select>';
        }

        $result .= '<span class="sort">Sort by:</span>';
        $result .= '<select name="data[sort][]">';

        foreach($sortFields as $key => $val) {
            $result .= '<option value="' . $key . '"' . ($key == Get::get('data.sort.0') ? ' selected' : '') . '>' . $val . '</option>';
        }

        $result .= '</select>';

        $result .= '<select name="data[order][]">';
        $result .= '<option value="ASC"' . (Get::get('data.order.0') == 'ASC' ? ' selected' : '') . '>ASC</option>';
        $result .= '<option value="DESC"' . (Get::get('data.order.0') == 'DESC' ? ' selected' : '') . '>DESC</option>';
        $result .= '</select>';

        $result .= '<input type="submit" name="search" value="" />';

        if (!empty($_GET['data'])) {
            $result .= '<a href="' . Url::getRawUrl() . '" class="clear"></a>';
        }

        $result .= '</form>';

        return $result;
    }

    /**
     * Filter the records by the specified needs.
     *
     * @param array $sortFields
     * @return string
     * @throws Exception
     */
    public static function carFollowersRecordsFilter($sortFields)
    {
        if (!is_array($sortFields) || empty($sortFields)) {
            throw new Exception('The SORTFIELDS parameter must be a non-empty array');
        }

        $result = '';

        $result .= '<form action="#" method="get">';
        $result .= '<input type="text" name="data[search]" placeholder="Search" value="' . Get::get('data.search') . '" />';

        if (Car_Model_Car::getInstance()->getCollection()->getCount() > 0) {
            $result .= '<select name="data[car_followers_car][]" class="desktopOnly">';
            $result .= '<option value="">Car</option>';

            foreach (Car_Model_Car::getInstance()->order('ord', 'asc')->getCollection() as $item) {
                $result .= '<option value="' . $item->getId() . '"' . (Get::get('data.car_followers_car.0') == $item->getId() ? ' selected' : '') . '>' . $item->getName() . '</option>';
            }

            $result .= '</select>';
        }

        $result .= '<span class="sort">Sort by:</span>';
        $result .= '<select name="data[sort][]">';

        foreach($sortFields as $key => $val) {
            $result .= '<option value="' . $key . '"' . ($key == Get::get('data.sort.0') ? ' selected' : '') . '>' . $val . '</option>';
        }

        $result .= '</select>';

        $result .= '<select name="data[order][]">';
        $result .= '<option value="ASC"' . (Get::get('data.order.0') == 'ASC' ? ' selected' : '') . '>ASC</option>';
        $result .= '<option value="DESC"' . (Get::get('data.order.0') == 'DESC' ? ' selected' : '') . '>DESC</option>';
        $result .= '</select>';

        $result .= '<input type="submit" name="search" value="" />';

        if (!empty($_GET['data'])) {
            $result .= '<a href="' . Url::getRawUrl() . '" class="clear"></a>';
        }

        $result .= '</form>';

        return $result;
    }

    /**
     * Filter the records by the specified needs.
     *
     * @param array $sortFields
     * @return string
     * @throws Exception
     */
    public static function carOrdersRecordsFilter($sortFields)
    {
        if (!is_array($sortFields) || empty($sortFields)) {
            throw new Exception('The SORTFIELDS parameter must be a non-empty array');
        }

        $result = '';

        $result .= '<form action="#" method="get">';
        $result .= '<input type="text" name="data[search]" placeholder="Search" value="' . Get::get('data.search') . '" />';

        $result .= '<select name="data[car_orders_status][]" class="desktopOnly">';
        $result .= '<option value=""' . (Get::get('data.car_orders_status.0') == '' ? ' selected' : '') . '>Status</option>';

        foreach (Car_Model_Order::$statuses as $key => $val) {
            $result .= '<option value="' . $key . '"' . (Get::get('data.car_orders_status.0') == $key && Get::get('data.car_orders_status.0') != '' ? ' selected' : '') . '>' . $val . '</option>';
        }

        $result .= '</select>';

        $result .= '<select name="data[car_orders_active][]" class="desktopOnly">';
        $result .= '<option value=""' . (Get::get('data.car_orders_active.0') == '' ? ' selected' : '') . '>Active</option>';

        foreach (Car_Model_Order::$active as $key => $val) {
            $result .= '<option value="' . $key . '"' . (Get::get('data.car_orders_active.0') == $key && Get::get('data.car_orders_active.0') != '' ? ' selected' : '') . '>' . $val . '</option>';
        }

        $result .= '</select>';

        $result .= '<select name="data[car_orders_payment][]" class="desktopOnly">';
        $result .= '<option value=""' . (Get::get('data.car_orders_payment.0') == '' ? ' selected' : '') . '>Payment</option>';

        foreach (Car_Model_Order::$payments as $key => $val) {
            $result .= '<option value="' . $key . '"' . (Get::get('data.car_orders_payment.0') == $key && Get::get('data.car_orders_payment.0') != '' ? ' selected' : '') . '>' . $val . '</option>';
        }

        $result .= '</select>';

        $result .= '<span class="sort">Sort by:</span>';
        $result .= '<select name="data[sort][]">';

        foreach($sortFields as $key => $val) {
            $result .= '<option value="' . $key . '"' . ($key == Get::get('data.sort.0') ? ' selected' : '') . '>' . $val . '</option>';
        }

        $result .= '</select>';

        $result .= '<select name="data[order][]">';
        $result .= '<option value="ASC"' . (Get::get('data.order.0') == 'ASC' ? ' selected' : '') . '>ASC</option>';
        $result .= '<option value="DESC"' . (Get::get('data.order.0') == 'DESC' ? ' selected' : '') . '>DESC</option>';
        $result .= '</select>';

        $result .= '<input type="submit" name="search" value="" />';

        if (!empty($_GET['data'])) {
            $result .= '<a href="' . Url::getRawUrl() . '" class="clear"></a>';
        }

        $result .= '</form>';

        return $result;
    }
}