<?php

class Sitemap extends XMLWriter
{
    /**
     * Build the application's site map.
     * Based on CMS Pages found.
     *
     * @return void
     */
    public function build()
    {
        $this->buildHeader();

        foreach (Cms_Model_Page::getInstance()->getCollection() as $page) {
            $this->buildContainer($page);
        }

        $this->buildFooter();
    }

    /**
     * Build the xml header responsible for the site map.
     *
     * @return void
     */
    private function buildHeader()
    {
        $this->openUri('sitemap.xml');
        $this->startDocument('1.0','UTF-8');
        $this->setIndent(4);

        $this->startElement('urlset');
        $this->writeAttribute('xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9');
    }

    /**
     * Build the xml container responsible for the site map.
     * Using the CMS pages and their child - parent relations.
     *
     * @param Model|Database $page
     * @return void
     */
    private function buildContainer($page)
    {
        $this->startElement('url');
        $this->writeElement('loc', Url::getHostUrl() . $page->getUrl());
        $this->writeElement('lastmod', date('Y-m-d', $page->getUpdatedAt()));

        if ($page->getIdentifier() == 'home') {
            $this->writeElement('changefreq', 'daily');
        } elseif ($page->isSuperParent()) {
            $this->writeElement('changefreq', 'weekly');
        } else {
            $this->writeElement('changefreq', 'monthly');
        }

        if ($page->getIdentifier() == 'home') {
            $this->writeElement('priority', '1.0');
        } elseif ($page->isSuperParent()) {
            $this->writeElement('priority', '0.8');
        } elseif ($page->isParent()) {
            $this->writeElement('priority', '0.6');
        } else {
            $this->writeElement('priority', '0.4');
        }

        $this->endElement();
    }

    /**
     * Build the xml footer responsible for the site map.
     *
     * @return void
     */
    private function buildFooter()
    {
        $this->endElement();
    }
}