<?php

class Audio
{
    /**
     * Audio extensions that can be parsed.
     *
     * @var array
     */
    public static $audioExtensions = [
        'mp3', 'aac', 'wav', 'ogg'
    ];

    /**
     * Available audio content types.
     *
     * @var array
     */
    public static $contentTypes = [
        'mp3'   => 'audio/mp3',
        'aac'   => 'audio/aac',
        'wav'   => 'audio/wav',
        'ogg'   => 'video/x-ogg',
    ];

    /**
     * The file handler.
     *
     * @var
     */
    private $file;

    /**
     * The actual name of the file.
     *
     * @var
     */
    private $fileName;

    /**
     * The actual size of the file.
     *
     * @var
     */
    private $fileSize;

    /**
     * The actual temporary name of the file.
     *
     * @var
     */
    private $fileTmpName;

    /**
     * The actual file extension.
     *
     * @var
     */
    private $extension;

    /**
     * The maximum size allowed for uploading in MB.
     *
     * @var int
     */
    private $maxSize = 30;

    /**
     * Initialize the audio attributes.
     *
     * @param string $file
     * @param int $maxSize
     */
    public function __construct($file, $maxSize = 30)
    {
        $this->file     = $file;
        $this->maxSize  = $maxSize;

        $this->init();
    }

    /**
     * Return all available audio extensions.
     *
     * @return array
     */
    public static function getExtensions()
    {
        return self::$audioExtensions;
    }

    /**
     * Return all available audio content types.
     *
     * @return array
     */
    public static function getContentTypes()
    {
        return self::$contentTypes;
    }

    /**
     * Save the audio file inside it's public audio path.
     * Also set a session with the file name for future database processing.
     *
     * @return bool
     */
    public function save()
    {
        if ($this->fileName) {
            $this->errors();

            $fileName = md5(String::random(25));

            while (File::exists(Dir::getAudio() . $fileName . '.' . $this->extension)) {
                $fileName = md5(String::random(25));
            }

            move_uploaded_file($this->fileTmpName, Dir::getAudio() . $fileName . '.' . $this->extension);
            exec('/usr/bin/ffmpeg -i ' . Dir::getAudio() . $fileName . '.' .$this->extension . ' -ab 32000 -ar 44100 ' . Dir::getAudio() . $fileName . '.mp3', $output, $ret);

            if ($this->extension != 'mp3') {
                File::delete(Dir::getAudio() . $fileName . '.' . $this->extension);
            }

            if (!File::exists(Dir::getAudio() . $fileName . '.mp3')) {
                Flash::error('Upload failed!');
                return false;
            }

            Session::set('upload-file', $fileName . '.mp3');
            return true;
        }

        Flash::error('Upload failed! Please select a file.');
        Redirect::url(Url::getPreviousUrl());

        return false;
    }

    /**
     * Helper method for displaying the audio file inside an admin edit view.
     *
     * @param string $audio
     * @param array|null $options
     * @return string
     * @throws Exception
     */
    public static function display($audio, $options = null)
    {
        $audio          = Url::getHostUrl() . Dir::getUploads() . '/audio/' . $audio;
        $extension      = Arr::last(explode('.', $audio));
        $optionString   = '';

        if ($options) {
            if (!is_array($options) || empty($options)) {
                throw new Exception('The OPTIONS parameter must be a non-empty array');
            }

            foreach ($options as $key => $val) {
                $optionString .= $key . '="' . $val . '" ';
            }
        }

        return '
            <audio controls ' . $optionString . '>
                <source src="' . $audio . '" type="audio/' . $extension . '">
                Your browser does not support the audio element.
            </audio>
        ';
    }

    /**
     * Initialize the file attributes.
     *
     * @return void
     */
    private function init()
    {
        $this->fileName     = is_array($this->file['name']) ? $this->file['name'][0] : $this->file['name'];
        $this->fileSize     = is_array($this->file['size']) ? $this->file['size'][0] : $this->file['size'];
        $this->fileTmpName  = is_array($this->file['tmp_name']) ? $this->file['tmp_name'][0] : $this->file['tmp_name'];
    }

    /**
     * Handle possible generic errors.
     *
     * @return bool
     */
    private function errors()
    {
        preg_match('/\.([^.]+)$/i', $this->fileName, $this->extension);
        $this->extension  = end($this->extension);

        if (!in_array(strtolower($this->extension), self::$audioExtensions)) {
            Flash::error('Invalid audio format ' . $this->extension . '. Choose one of: ' . implode(', ', self::$audioExtensions));
            Redirect::url(Url::getPreviousUrl());
        }

        if ($this->fileSize > ($this->maxSize * 1024 * 1024)) {
            Flash::error('The file is too large! Please try with a smaller file.');
            Redirect::url(Url::getPreviousUrl());
        }
    }
}