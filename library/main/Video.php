<?php

class Video
{
    /**
     * Video extensions that can be parsed.
     *
     * @var array
     */
    public static $videoExtensions = [
        'avi', 'flv', 'mp4', 'ogg', 'ogv', 'webm', 'mov', 'mpeg', 'mpg', 'mkv', 'acc'
    ];

    /**
     * Available audio content types.
     *
     * @var array
     */
    public static $contentTypes = [
        'avi'   => 'video/avi',
        'flv'   => 'video/x-flv',
        'mp4'   => 'video/mp4',
        'ogg'   => 'video/x-ogg',
        'mov'   => 'video/quicktime',
        'mpeg'  => 'video/mpeg',
        'mpg'   => 'video/mpeg',
        'mkv'   => 'video/x-matroska',
        'aac'   => 'audio/aac',
    ];

    /**
     * The file handler.
     *
     * @var
     */
    private $file;

    /**
     * The actual name of the file.
     *
     * @var
     */
    private $fileName;

    /**
     * The actual size of the file.
     *
     * @var
     */
    private $fileSize;

    /**
     * The actual temporary name of the file.
     *
     * @var
     */
    private $fileTmpName;

    /**
     * The actual file extension.
     *
     * @var
     */
    private $extension;

    /**
     * The maximum size allowed for uploading in MB.
     *
     * @var int
     */
    private $maxSize = 30;

    /**
     * Initialize the video attributes.
     *
     * @param string $file
     * @param int $maxSize
     */
    public function __construct($file, $maxSize = 30)
    {
        $this->file     = $file;
        $this->maxSize  = $maxSize;

        $this->init();
    }

    /**
     * Return all available video extensions.
     *
     * @return array
     */
    public static function getExtensions()
    {
        return self::$videoExtensions;
    }

    /**
     * Return all available video content types.
     *
     * @return array
     */
    public static function getContentTypes()
    {
        return self::$contentTypes;
    }

    /**
     * Save the video file inside it's public video path.
     * Also set a session with the file name for future database processing.
     *
     * @return bool
     */
    public function save()
    {
        if ($this->fileName) {
            $this->errors();

            $fileName = md5(String::random(25));

            while (File::exists(Dir::getVideo() . $fileName . '.' . $this->extension)) {
                $fileName = md5(String::random(25));
            }

            move_uploaded_file($this->fileTmpName, Dir::getVideo() . $fileName . '.' . $this->extension);
            exec('/usr/bin/ffmpeg -i ' . Dir::getVideo() . $fileName . '.' .$this->extension . ' -b 250k -ar 44100 -s ' . (Session::exists('video') ? Session::get('video.width') . 'x' . Session::get('video.height') . ' ' : '') . Dir::getVideo() . $fileName . '.' . $this->extension, $output, $ret);

            $this->thumbnail($fileName);

            Session::disable('video');

            if (!File::exists(Dir::getVideo() . $fileName . '.' . $this->extension)) {
                Flash::error('Upload failed!');
                return false;
            }

            Session::set('upload-file', $fileName . '.' . $this->extension);

            return true;
        }

        Flash::error('Upload failed! Select a file.');
        Redirect::url(Url::getPreviousUrl());

        return false;
    }

    /**
     * Create a random video thumbnail.
     *
     * @param string $fileName
     * @return void
     */
    public function thumbnail($fileName)
    {
        $image = Dir::getVideoThumbnail() . $fileName . '.jpg';
        $video = Dir::getVideo() . $fileName . '.' . $this->extension;

        $cmd    = "ffmpeg.exe -i $video 2>&1";
        $second = 3;

        if(preg_match('/Duration: ((\d+):(\d+):(\d+))/s', `$cmd`, $time)){
            $total  = ($time[2] * 3600) + ($time[3] * 60) + $time[4];
            $second = rand(1, ($total - 1));
        }

        exec('/usr/bin/ffmpeg -i ' . $video . ' -deinterlace -an -ss ' . $second . ' -t 00:00:01 -r 2 -y -s ' . (Session::exists('video') ? Session::get('video.width') . 'x' . Session::get('video.height') . ' ' : '') . '-vcodec mjpeg -f mjpeg ' . $image . ' 2>&1', $output, $ret);
    }

    /**
     * Identify the video's thumb if any, according to the videos.xml file.
     *
     * @param $type
     * @return void
     */
    public static function thumb($type)
    {
        foreach (Config::getVideos()->video as $video) {
            $attributes = $video->attributes();

            if ((string)$attributes['type'] == $type) {
                Session::set('video.width', $video->thumbs->default->attributes()['width']);
                Session::set('video.height', $video->thumbs->default->attributes()['height']);
            }
        }
    }

    /**
     * Helper method for displaying the audio file inside an admin edit view.
     *
     * @param string $video
     * @param int $width
     * @param int $height
     * @param string $style
     * @return string
     */
    public static function display($video, $width = 500, $height = 350, $style = '')
    {
        $swf    = Url::getHostUrl() . Url::getPrefix() . '/public/helpers/videoplayer.swf';
        $image  = Url::getHostUrl() . Url::getPrefix() . '/public/uploads/video/thumbnails/' . array_shift(array_values(explode('.', $video))) . '.jpg';
        $video  = Url::getHostUrl() . Dir::getUploads() . '/video/' . $video;

        $result = '<div style="' . $style . '">';

        $result .= '<object id="player" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" name="modieus" width="' . $width . '" height="' . $height . '">';
        $result .= '<param name="movie" value="' . $swf . '" />';
        $result .= '<param name="allowfullscreen" value="true" />';
        $result .= '<param name="autoplay" value="true" />';
        $result .= '<param name="wmode" value="transparent">';
        $result .= '<param name="allowscriptaccess" value="always" />';
        $result .= '<param name="flashvars" value="image=' . $image . '&file=' . $video . '&stretching=fill" />';

        $result .= '<object type="application/x-shockwave-flash" data="' . $swf . '" width="' . $width . '" height="' . $height . '">';
        $result .= '<param name="movie" value="' . $swf . '" />';
        $result .= '<param name="allowfullscreen" value="true" />';
        $result .= '<param name="wmode" value="transparent">';
        $result .= '<param name="allowscriptaccess" value="always" />';
        $result .= '<param name="flashvars" value="image=' . $image . '&file=' . $video . '&stretching=fill" />';
        $result .= '<p><a href="http://get.adobe.com/flashplayer"><b>Download Adobe Flash Player</b></a> to see this video.</p>';
        $result .= '</object>';
        $result .= '</object>';
        $result .= '</div>';

        return $result;
    }

    /**
     * Initialize the file attributes.
     *
     * @return void
     */
    private function init()
    {
        $this->fileName     = is_array($this->file['name']) ? $this->file['name'][0] : $this->file['name'];
        $this->fileSize     = is_array($this->file['size']) ? $this->file['size'][0] : $this->file['size'];
        $this->fileTmpName  = is_array($this->file['tmp_name']) ? $this->file['tmp_name'][0] : $this->file['tmp_name'];
    }

    /**
     * Handle possible generic errors.
     *
     * @return bool
     */
    private function errors()
    {
        preg_match('/\.([^.]+)$/i', $this->fileName, $this->extension);
        $this->extension  = end($this->extension);

        if (!in_array(strtolower($this->extension), self::$videoExtensions)) {
            Flash::error('Invalid video format ' . $this->extension . '. Choose one of: ' . implode(', ', self::$videoExtensions));
            Redirect::url(Url::getPreviousUrl());
        }

        if ($this->fileSize > ($this->maxSize * 1024 * 1024)) {
            Flash::error('The file is too large! Please try with a smaller file.');
            Redirect::url(Url::getPreviousUrl());
        }
    }
}