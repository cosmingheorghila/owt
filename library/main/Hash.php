<?php

class Hash
{
    /**
     * Hash the given value using the bcrypt method.
     *
     * @param string $value
     * @return bool|string
     */
    public static function make($value)
    {
        $hash = password_hash($value, PASSWORD_BCRYPT);

        if ($hash === false) {
            throw new RuntimeException('BCRYPT hashing not supported');
        }

        return $hash;
    }

    /**
     * Check if the given value matches the given hash.
     *
     * @param $value
     * @param $hash
     * @return bool
     */
    public static function check($value, $hash)
    {
        if (strlen($hash) === 0) {
            return false;
        }

        return password_verify($value, $hash);
    }
}