<?php

class Behavior_Draggable
{
    /**
     * The predefined defaults of this behavior.
     *
     * @var array
     */
    protected static $defaults = [
        'field' => 'ord'
    ];

    /**
     * Database record handler.
     *
     * @var
     */
    private static $row;

    /**
     * Sort rows logic.
     * Sort the rows based on a simplified lft/rgt algorithm.
     * It event accepts conditions in case of multi-sorting the same entity, or partial sorting by an identifier.
     *
     * @param string $table
     * @param array|null $conditions
     * @throws Exception
     */
    public static function sort($table, $conditions = null)
    {
        $count = 0;

        if (Request::isPost()) {
            $database = new Database($table);

            foreach (Post::get('items') as $ord => $item) {
                if (empty($item)) {
                    continue;
                }

                if ($conditions && $count == 0) {
                    self::$row = $database->load($item)->{$conditions};
                }

                $database->reset();
                $database->update([
                    self::$defaults['field'] => $ord
                ])->where([
                    'id' => $item
                ]);

                if (is_array($conditions) && !empty($conditions)) {
                    $database->where($conditions);
                } elseif ($conditions) {
                    $database->where([
                        $conditions => self::$row
                    ]);
                }

                if (!$database->save()) {
                    throw new Exception('Database update failed');
                }

                $count++;
            }
        }
    }
}