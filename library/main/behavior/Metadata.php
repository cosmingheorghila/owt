<?php

class Behavior_Metadata
{
    /**
     * The predefined defaults of this behavior.
     *
     * @var array
     */
    protected static $defaults = [
        'field' => 'metadata'
    ];

    /**
     * Metadata fields container.
     *
     * @var array
     */
    private static $fields = [];

    /**
     * Figure out which input values to include in the metadata logic.
     * Serialize the correct input values and schedule them for metadata parsing.
     *
     * @param array|string $values
     * @return mixed
     */
    public static function parse($values)
    {
        foreach ($values as $key => $val) {
            if (String::startsWith($key, self::$defaults['field'] . '_')) {
                self::$fields[substr($key, strlen(self::$defaults['field'] . '_'))] = $val;
            }
        }

        foreach (self::$fields as $key => $val) {
            unset($values[self::$defaults['field'] . '_' . $key]);
        }

        $values[self::$defaults['field']] = serialize(self::$fields);

        return $values;
    }
}