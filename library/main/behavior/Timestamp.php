<?php

class Behavior_Timestamp
{
    /**
     * The predefined defaults of this behavior.
     *
     * @var array
     */
    protected static $defaults = [
        'created'       => true,
        'updated'       => true,
        'createdColumn' => 'created_at',
        'updatedColumn' => 'updated_at',
    ];

    /**
     * Static time value.
     *
     * @var array
     */
    public static $time = [];

    /**
     * Automatically update the timestamp keys on any table.
     *
     * @param array|null $options
     * @param bool $firstTime
     * @return array
     * @throws Exception
     */
    public static function execute($options = null, $firstTime = true)
    {
        $options = $options ? Arr::merge(self::$defaults, $options) : self::$defaults;

        if ($firstTime) {
            self::$time[$options['createdColumn']] = time();
        }

        self::$time[$options['updatedColumn']] = time();

        return self::$time;
    }
}