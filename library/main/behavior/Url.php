<?php

class Behavior_Url
{
    /**
     * Database table responsible with the URLs.
     *
     * @var string
     */
    private static $table = 'cms_urls';

    /**
     * Database adapter.
     *
     * @var
     */
    private $database;

    /**
     * URL handler.
     *
     * @var
     */
    public $url;

    /**
     * Initialize details for future behavior operations.
     *
     * return void
     */
    public function __construct()
    {
        $this->database = new Database(self::$table);
    }

    /**
     * Return the URL according to the specified where conditions.
     *
     * @param array $where
     * @return mixed
     */
    public function getUrl($where)
    {
        return $this->database->load($where);
    }

    /**
     * Prepare the URL value to be used.
     *
     * Set the actual URL responsible for that particular entity row.
     * Save it in the URL database table for future use.
     *
     * @param string $url
     * @param string|null $entity
     * @param int|null $entityId
     * @param string|null $action
     * @return bool
     * @throws Exception
     * @internal param array $data
     */
    public function setUrl($url, $entity = null, $entityId = null, $action = null)
    {
        if (!$action || (strtolower($action) != 'insert' && strtolower($action) != 'update')) {
            throw new Exception('The ACTION parameter must be INSERT or UPDATE');
        }

        if ($entity === null || $entityId === null) {
            throw new Exception('The ENTITY and ENTITY_ID parameters can\'t be empty');
        }

        if (strtolower($action) == 'insert') {
            $addUrl = $this->database->insert(['url' => $url, 'entity' => $entity, 'entity_id' => $entityId,], ['url' => $url]);

            if (!$addUrl) {
                Database::getInstance($entity)->delete()->where(['id' => $entityId])->save();
                Redirect::url(Url::getPreviousUrl());
            }
        } elseif (strtolower($action) == 'update') {
            $oldUrl = $this->database->load(['entity' => $entity, 'entity_id' => $entityId]);

            if ($oldUrl) {
                $this->database->update(['url' => $url], ['url' => $url], $oldUrl)->where(['entity' => $entity, 'entity_id' => $entityId]);
            } else {
                $this->database->insert(['entity_id' => $entityId, 'entity' => $entity, 'url' => $url], ['url' => $url]);
            }
        }

        return $this->database->save() ? true : false;
    }
}