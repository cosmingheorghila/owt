<?php

class Behavior_Block
{
    /**
     * The view handler.
     * To be used in block's logic or views.
     *
     * @var
     */
    public $view;

    /**
     * The model instance.
     * Can be either of instance Layout or Page.
     *
     * @var null
     */
    public static $model = null;

    /**
     * The available locations of a given block.
     *
     * @var array
     */
    public static $blockLocations = [];

    /**
     * The available types of a given block.
     *
     * @var array
     */
    public static $blockTypes = [];

    /**
     * Manage the block fetching method.
     *
     * @param $method
     * @param $args
     * @return mixed|null
     */
    public function __call($method, $args)
    {
        if (String::startsWith($method, 'get')) {
            return self::get(
                strtolower(preg_replace('/(\p{Ll})(\p{Lu})/u', '\1_\2', end(explode('get', $method)))),
                $args[0] !== null ? $args[0] : null
            );
        } else {
            return null;
        }
    }

    /**
     * Set the block data to be used in the block view.
     *
     * @param string $name
     * @param string|array $value
     */
    public function set($name, $value)
    {
        Bootstrap::getInstance()->data[$name] = $value;
    }

    /**
     * Get the block data.
     * Usable inside a block view.
     *
     * @param string $name
     * @param string|null $key
     * @return mixed
     */
    public function get($name, $key = null)
    {
        return $key === null ? Bootstrap::getInstance()->data[$name] : Bootstrap::getInstance()->data[$name][$key];
    }

    /**
     * Un-serialize the metadata attribute obtained from the Database.
     *
     * @param Model|Database $model
     * @return mixed
     */
    public function metadata($model)
    {
        return unserialize(str_replace('\"', '"', $model->getMetadata()));
    }

    /**
     * Load a block entity.
     *
     * @param array|string|int $where
     * @return mixed
     */
    public function getBlock($where)
    {
        return Cms_Model_Block::getInstance()->load($where);
    }

    /**
     * Get a block entity collection.
     *
     * @param array|string|null $where
     * @return PDOStatement
     */
    public function getCollection($where)
    {
        return Cms_Model_Block::getInstance()->where($where)->order('name')->getCollection();
    }

    /**
     * Get a formatted block entity collection.
     *
     * @param Database|PDOStatement|bool $collection
     * @return PDOStatement
     */
    public function getBlocks($collection)
    {
        $ids    = [];
        $count  = [];
        $blocks = [];

        foreach ($collection as $row) {
            $ids[] = $row->getBlockId();
        }

        foreach ($ids as $val) {
            $count[$val][] = $val;
        }

        foreach ($count as $key => $val) {
            $block = Cms_Model_Block::getInstance()->load($key);

            for ($i = 0; $i < count($val); $i++) {
                $blocks[] = $block;
            }
        }

        return $blocks;
    }

    /**
     * Return all the block types defined in the block model.
     *
     * @return array
     */
    public static function getTypes()
    {
        $types = [];

        foreach (Cms_Model_Block::$types as $type => $options) {
            $types[$type] = ucwords(implode(' ', explode('_', $type)));
        }

        ksort($types, SORT_ASC);

        return $types;
    }

    /**
     * Return the class of a given block type.
     *
     * @param string $type
     * @return string|null
     */
    public static function getTypesClass($type)
    {
        foreach (Cms_Model_Block::$types as $key => $val) {
            if ($key == $type) {
                return $val['class'];
            }
        }

        return null;
    }

    /**
     * Return the label of a given block type.
     *
     * @param string $type
     * @return string|null
     */
    public static function getTypesLabel($type)
    {
        foreach (Cms_Model_Block::$types as $key => $val) {
            if ($key == $type) {
                return $val['label'];
            }
        }

        return null;
    }

    /**
     * Check if a block location on the page entity has inherited any block locations from the layout.
     *
     * @param string $location
     * @return bool
     * @throws Exception
     */
    public function hasParents($location)
    {
        if (!self::$model->getTable() == 'cms_pages') {
            throw new Exception('You can\'t use HAS_PARENTS on the Layouts entity');
        }

        $layouts = new Database('cms_blocks_relations');
        $layouts->select()->where([
            'entity_id' => self::$model->getLayoutId(),
            'location'  => $location,
            'entity'    => 'cms_layouts'
        ]);

        $pages = new Database('cms_blocks_relations');
        $pages->select()->where([
            'entity_id' => self::$model->getId(),
            'location'  => $location,
            'entity'    => 'cms_pages'
        ]);

        return $layouts->fetch()->rowCount() > 0 && $pages->fetch()->rowCount() == 0 ? true : false;
    }

    /**
     * Check if the page or layout entity has at least one location defined by the developer.
     *
     * @return bool
     */
    public function hasDefinedLocations()
    {
        switch (self::$model->getTable()) {
            case 'cms_layouts':
                $existingBlocks = self::$model->getBlockLocations();
                break;
            default:
                $existingBlocks = Cms_Model_Layout::getInstance()->load(self::$model->getLayoutId())->getBlockLocations();
                break;
        }

        return empty($existingBlocks) ? false : true;
    }

    /**
     * Check if a page has blocks directly assigned to it, according to the location specified.
     *
     * @param string $location
     * @param Cms_Model_Page|null $page
     * @return bool|PDOStatement
     */
    public function pageHasBlocks($location, $page = null)
    {
        if ($page) {
            $database   = new Database('cms_blocks_relations');
            $collection = $database->select()->where([
                'entity_id' => $page->getId(),
                'location'  => $location,
                'entity'    => 'cms_pages'
            ])->order('ord')->fetch();

            return $collection->rowCount() > 0 ? $collection : false;
        }

        return false;
    }

    /**
     * Check if a layout has blocks directly assigned to it, without any block assigned to a page that it extends.
     *
     * @param string $location
     * @param Cms_Model_Layout|null $layout
     * @param Cms_Model_Page|null $page
     * @return bool|PDOStatement
     */
    public function layoutHasBlocks($location, $page = null, $layout = null)
    {
        $database = new Database('cms_blocks_relations');

        if ($page) {
            $collection = $database->select()->where([
                'entity_id' => $page->getLayoutId(),
                'location'  => $location,
                'entity'    => 'cms_layouts'
            ])->order('ord')->fetch();

            return $collection->rowCount() > 0 ? $collection : false;
        } elseif ($layout) {
            $collection = $database->select()->where([
                'entity_id' => $layout->getId(),
                'location'  => $location,
                'entity'    => 'cms_layouts'
            ])->fetch();

            return $collection->rowCount() > 0 ? $collection : false;
        }

        return false;
    }

    /**
     * Render blocks logic.
     * Display the assigned blocks in the given order.
     * Consider the layout -> page inheritance.
     *
     * @param array|PDOStatement $blocks
     */
    public function displayBlocks($blocks)
    {
        if (is_array($blocks) && !empty($blocks)) {
            foreach ($blocks as $block) {
                $viewPath   = 'application/modules/cms/views/blocks/';
                $classPath  = 'application/modules/cms/blocks/';
                $className  = 'Cms_Block_' . $block->getClass();

                if(!class_exists($className)) {
                    require_once($classPath . $block->getClass() . '.php');
                }

                $blockClass = new $className();
                $blockClass->execute($block);

                $this->set('view', Bootstrap::getInstance());

                include($viewPath . $blockClass->template);
            }
        }
    }

    /**
     * Hard render a block inside a layout or view.
     *
     * @param string $class
     */
    public function block($class)
    {
        $viewPath   = 'application/modules/cms/views/blocks/';
        $classPath  = 'application/modules/cms/blocks/';
        $className  = 'Cms_Block_' . $class;

        if(!class_exists($className)) {
            require_once($classPath . $class . '.php');
        }

        $blockClass = new $className();
        $blockClass->execute();

        $this->set('view', Bootstrap::getInstance());

        include($viewPath . $blockClass->template);
    }

    /**
     * Helper for using inside a layout or view for displaying the correct blocks assigned.
     *
     * @param string $location
     * @param Cms_Model_Page|null $page
     * @param Cms_Model_Layout|null $layout
     * @return void
     */
    public function blockHolder($location, $page = null, $layout = null)
    {
        if ($pageBlocks = self::pageHasBlocks($location, $page)) {
            self::displayBlocks(self::getBlocks($pageBlocks));
        } elseif ($layoutBlocks = self::layoutHasBlocks($location, $page, $layout)) {
            self::displayBlocks(self::getBlocks($layoutBlocks));
        }
    }

    /**
     * Helper for using inside a layout or view for displaying the correct locations tabs on an admin edit page.
     *
     * @param Model|Database $model
     * @return void
     * @throws Exception
     */
    public function blockTabs($model)
    {
        if ($model->getTable() != 'cms_layouts' && $model->getTable() != 'cms_pages') {
            throw new Exception('The BlockHolder behavior is available only on Layouts & Pages');
        }

        self::$model = $model;

        if (self::hasDefinedLocations()) {
            $sections = null;

            switch ($model->getTable()) {
                case 'cms_layouts':
                    $sections = explode(',', self::$model->getBlockLocations());
                    break;
                case 'cms_pages':
                    $sections = explode(',', Cms_Model_Layout::getInstance()->load(self::$model->getLayoutId())->getBlockLocations());
                    break;
            }

            foreach ($sections as $section) {
                self::$blockLocations[] = explode(' => ', trim($section))[0];
            }

            foreach (self::$blockLocations as $index => $location) {
                echo '<a href="#tab-' . ($index + 100) . '" class="tab">' . ucwords($location) . ' Blocks</a>';
            }
        }
    }

    /**
     * Helper for using inside a layout or view for displaying the correct location containers or partials on an admin edit page.
     *
     * @param Model|Database $model
     * @throws Exception
     * @return void
     */
    public function blockContainers($model)
    {
        if ($model->getTable() != 'cms_layouts' && $model->getTable() != 'cms_pages') {
            throw new Exception('The BlockHolder behavior is available only on Layouts & Pages');
        }

        self::$model = $model;

        if (self::hasDefinedLocations()) {
            $sections   = null;
            $locations  = [];
            $result     = [];

            switch ($model->getTable()) {
                case 'cms_layouts':
                    $sections = explode(',', self::$model->getBlockLocations());
                    break;
                case 'cms_pages':
                    $sections = explode(',', Cms_Model_Layout::getInstance()->load(self::$model->getLayoutId())->getBlockLocations());
                    break;
            }

            foreach ($sections as $section) {
                $locations[] = explode(' => ', trim($section))[0];
            }

            foreach ($locations as $index => $location) {
                $result[] = '<div id="tab-' . ($index + 100) . '" class="hide">';

                if (self::$model->getTable() == 'cms_pages') {
                    if (self::hasParents($location)) {
                        $result[] = '<div class="partial-error">';
                        $result[] = '<p>This page inherits the following blocks from the layout: ';

                        $blocks = [];

                        foreach (self::getInheritedBlocks($location) as $rel) {
                            $block      = self::getBlock($rel->getBlockId());
                            $blocks[]   = '<strong>' . $block->getName() . '</strong>';
                        }

                        $result[] = implode(', ', $blocks);
                        $result[] = '</p>';
                        $result[] = '</div>';
                    }
                }

                $result[] = '<table cellspacing="0" cellpadding="15" border="0" class="table-partial table-block">';
                $result[] = '<thead>';
                $result[] = '<tr class="dark nodrag nodrop">';
                $result[] = '<td>Name</td>';
                $result[] = '<td>Type</td>';
                $result[] = '<td class="actions">Actions</td>';
                $result[] = '</tr>';
                $result[] = '</thead>';
                $result[] = '<tbody>';

                $relations = self::getLocationRelations($location);

                if ($relations->rowCount() > 0) {
                    foreach ($relations as $relation) {
                        $block = self::getBlock(
                            $relation->getBlockId()
                        );

                        $result[] = '<tr id="' . $relation->getId() . '" class="light">';
                        $result[] = '<td>' . $block->getName() . '</td>';
                        $result[] = '<td>' . $block->getLabel() . '</td>';
                        $result[] = '<td>';
                        $result[] = '<a target="_blank" href="' . Url::getAdminUrl() . '/cms/blocks/edit?id=' . $block->getId() . '" class="btn view orange margin-right">View</a>';
                        $result[] = '<a data-id="' . $relation->getId() . '" class="delete-block btn delete red">Remove</a>';
                        $result[] = '</td>';
                        $result[] = '</tr>';
                    }
                } else {
                    $result[] = '<tr class="no-records light nodrag nodrop"><td colspan="10">No blocks assigned to this location</td></tr>';
                }

                $result[] = '</tbody>';
                $result[] = '<tfoot>';
                $result[] = '<tr class="dark nodrag nodrop">';
                $result[] = '<td colspan="10">';
                $result[] = '<select class="blocks-list">';

                foreach (self::getLocationTypes($location) as $type => $label) {
                    $result[] = '<optgroup label="' . $label . '">';

                    foreach (self::getCollection(['type' => $type]) as $block) {
                        $result[] = '<option value="' . $block->getId() . '">' . $block->getName() . '</option>';
                    }

                    $result[] = '</optgroup>';
                }

                $result[] = '</select>';
                $result[] = '<a class="add-block record-btn add green margin-left">Add</a>';
                $result[] = '</td>';
                $result[] = '</tr>';
                $result[] = '</tfoot>';
                $result[] = '</table>';
                $result[] = '</div>';
            }

            $result[] = self::getBlockScript();

            echo implode("\n", $result);
        }
    }

    /**
     * Get a page inherited blocks from the layout, if any.
     *
     * @param string $location
     * @return PDOStatement
     * @throws Exception
     */
    private function getInheritedBlocks($location)
    {
        if (self::$model->getTable() != 'cms_layouts' && self::$model->getTable() != 'cms_pages') {
            throw new Exception('The BlockHolder behavior is available only on Layouts & Pages');
        }

        switch (self::$model->getTable()) {
            case 'cms_pages':
                $entityId = self::$model->getLayoutId();
                break;
            default:
                $entityId = self::$model->getId();
                break;
        }

        return Database::getInstance('cms_blocks_relations')->select()->where([
            'entity_id' => $entityId,
            'entity'    => 'cms_layouts',
            'location'  => $location
        ])->order('ord')->fetch();
    }

    /**
     * Fetch the correct blocks belonging to the location specified.
     *
     * @param string $location
     * @return PDOStatement
     * @throws Exception
     */
    private function getLocationRelations($location)
    {
        if (self::$model->getTable() != 'cms_layouts' && self::$model->getTable() != 'cms_pages') {
            throw new Exception('The BlockHolder behavior is available only on Layouts & Pages');
        }

        return Database::getInstance('cms_blocks_relations')->select()->where([
            'entity_id' => self::$model->getId(),
            'entity'    => self::$model->getTable(),
            'location' => $location
        ])->order('ord')->fetch();
    }

    /**
     * Fetch the correct block types belonging to the location specified.
     *
     * @param string $location
     * @return array
     */
    private function getLocationTypes($location)
    {
        self::$blockTypes   = [];
        $sections           = null;
        $result             = null;
        $blockTypes         = null;

        switch (self::$model->getTable()) {
            case 'cms_layouts':
                $sections = explode(',', self::$model->getBlockLocations());
                break;
            case 'cms_pages':
                $sections = explode(',', Cms_Model_Layout::getInstance()->load(self::$model->getLayoutId())->getBlockLocations());
                break;
        }

        foreach ($sections as $section) {
            if (explode(' => ', trim($section))[0] == $location) {
                $blockTypes = explode(' ', explode(' => ', trim($section))[1]);
            }
        }

        if ((is_array($blockTypes) && !empty($blockTypes)) && !empty($blockTypes[0])) {
            foreach ($blockTypes as $type) {
                self::$blockTypes[$type] = ucwords(implode(' ', explode('_', $type)));
            }
        } else {
            self::$blockTypes = self::getTypes();
        }

        ksort(self::$blockTypes, SORT_ASC);

        return self::$blockTypes;
    }

    /**
     * Append the block add/delete/move dynamics to the footer.
     *
     * @return string
     */
    private function getBlockScript()
    {
        Bootstrap::getInstance()->_appendScriptInline .= "
            <script type=\"text/javascript\">
                $('.add-block').click(function(e){
                    e.preventDefault();

                    var blockId     = $('form .hide:visible .blocks-list').val();
                    var blockName   = $('form .hide:visible .blocks-list option:selected').text();
                    var blockLabel  = $('form .hide:visible .blocks-list option:selected').parent().attr('label');
                    var blockType   = blockLabel.toLowerCase().replace(' ', '_');

                    var locations   = '" . json_encode(Cms_Model_Layout::getBlocksLocations(Get::get('id'))) . "';
                    var location    = $('.tabs .selected').text().replace(' Blocks', '').toLowerCase().replace(' ', '_');

                    var editUrl     = '" . Url::getAdminUrl() . "/cms/blocks/edit?id=' + blockId;
                    var insertUrl   = '" . Url::getCurrentUrl() . "';

                    $.ajax({
                        type: 'POST',
                        url: insertUrl,
                        dataType: 'json',
                        data: {
                            add_block   : true,
                            block_id    : blockId,
                            location    : location
                        },
                        success: function(data) {
                            if (data.status) {
                                $('form .hide:visible .table-block tbody tr.no-records').hide();
                                $('form .hide:visible .table-block tbody').append(
                                    '<tr id=\"' + data.id + '\" class=\"light\" style=\"cursor: move;\">' +
                                        '<td>' + blockName + '</td>' +
                                        '<td>' + blockLabel + '</td>' +
                                        '<td>' +
                                            '<a target=\"_blank\" href=\"' + editUrl + '\" class=\"btn view orange margin-right\">View</a>' +
                                            '<a data-id=\"' + data.id + '\" class=\"delete-block btn delete red\">Remove</a>' +
                                        '</td>' +
                                    '</tr>'
                                );
                            }
                        }
                    });
                });

                $(document).on('click', '.delete-block', function(e){
                    e.preventDefault();

                    var blockId     = $(this).attr('data-id');
                    var deleteUrl   = '" . Url::getCurrentUrl() . "';
                    var tableRow    = $(this).closest('tr');

                    $.ajax({
                        type: 'POST',
                        url: deleteUrl,
                        dataType: 'json',
                        data: {
                            delete_block   : true,
                            block_id    : blockId
                        },
                        success: function(data) {
                            if (data.status) {
                                tableRow.hide();

                                if ($('form .hide:visible .table-block tbody tr:visible').length == 0) {
                                    $('form .hide:visible .table-block tbody tr.no-records').show();
                                }
                            }
                        }
                    });
                });
            </script>
        ";
    }
}