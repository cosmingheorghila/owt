<?php

class Upload
{
    /**
     * Image extensions that can be parsed.
     *
     * @var array
     */
    private $imageExtensions = [
        'jpg', 'jpeg', 'gif', 'png', 'bmp'
    ];

    /**
     * Video extensions that can be parsed.
     *
     * @var array
     */
    private $videoExtensions = [
        'avi', 'flv', 'mp4', 'ogg', 'ogv', 'webm', 'mov', 'mpeg', 'mpg', 'mkv', 'acc'
    ];

    /**
     * Audio extensions that can be parsed.
     *
     * @var array
     */
    private $audioExtensions = [
        'mp3', 'aac', 'wav', 'ogg'
    ];

    /**
     * File extensions that can be parsed.
     *
     * @var array
     */
    private $fileExtensions = [
        'pdf', 'doc', 'docx', 'ppt', 'pptx', 'xls', 'xlsx', 'xlsm', 'txt', 'csv'
    ];

    /**
     * Logic to upload any type of file.
     * Can upload one or multiple files with no parameter changing needed.
     *
     * @param array $file
     * @return bool
     */
    public function all($file)
    {
        $success = 1;

        if (count($file['name']) > 1) {
            foreach ($this->group($file) as $value) {
                $success = $this->up($value);
            }
        } else {
            $success = $this->up($file);
        }

        return $success == 1 ? true : false;
    }

    /**
     * Logic to upload an image file.
     *
     * @param array $file
     * @param int $maxSize
     * @return bool
     */
    public function image($file, $maxSize = 30)
    {
        return (new Image($file, $maxSize))->save();
    }

    /**
     * Logic to upload a video file.
     *
     * @param array $file
     * @param int $maxSize
     * @return mixed
     */
    public function video($file, $maxSize = 30)
    {
        return (new Video($file, $maxSize))->save();
    }

    /**
     * Logic to upload an audio file.
     *
     * @param array $file
     * @param int $maxSize
     * @return bool
     */
    public function audio($file, $maxSize = 30)
    {
        return (new Audio($file, $maxSize))->save();
    }

    /**
     * Logic to upload a document file.
     *
     * @param array $file
     * @param int $maxSize
     * @return bool
     */
    public function file($file, $maxSize = 30)
    {
        return (new Document($file, $maxSize))->save();
    }

    /**
     * Manage the file errors, if any.
     * Identify what type of file is and then upload it.
     *
     * @param string $file
     * @param int $success
     * @return bool
     */
    private function up($file, $success = 1)
    {
        if ($file['error'] === 4) {
            Flash::error('File does not exist! Please Upload a file.');
            Redirect::url(Url::getPreviousUrl());
        } elseif ($file['error'] !== 0) {
            Flash::error('Problem with file! Please try again later.');
            Redirect::url(Url::getPreviousUrl());
        }

        preg_match('/\.([^.]+)$/i', is_array($file['name']) ? $file['name'][0] : $file['name'], $extension);
        $extension = end($extension);

        if (in_array(strtolower($extension), $this->imageExtensions)) {
            $this->image($file) ? $success = 1 : $success++;
        } elseif (in_array(strtolower($extension), $this->videoExtensions)) {
            $this->video($file) ? $success = 1 : $success++;
        } elseif (in_array(strtolower($extension), $this->audioExtensions)) {
            $this->audio($file) ? $success = 1 : $success++;
        } elseif (in_array(strtolower($extension), $this->fileExtensions)) {
            $this->file($file) ? $success = 1 : $success++;
        } else {
            Flash::error('File type not supported!');
            return false;
        }

        return $success;
    }

    /**
     * Identify if single or multiple files.
     * Parse the file(s) for making the upload friendly.
     *
     * @param string $file
     * @return array
     */
    private function group(&$file)
    {
        $fileArray = [];

        for ($i = 0; $i < count($file['name']); $i++) {
            foreach (array_keys($file) as $key) {
                $fileArray[$i][$key] = $file[$key][$i];
            }
        }

        return $fileArray;
    }
}