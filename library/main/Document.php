<?php

class Document
{
    /**
     * File extensions that can be parsed.
     *
     * @var array
     */
    public static $fileExtensions = [
        'pdf', 'doc', 'docx', 'ppt', 'pptx', 'xls', 'xlsx', 'xlsm', 'txt', 'csv'
    ];

    /**
     * Available file content types.
     *
     * @var array
     */
    public static $contentTypes = [
        'pdf'   => 'application/pdf',
        'doc'   => 'application/msword',
        'docx'  => 'application/msword',
        'ppt'   => 'application/vnd.ms-powerpoint',
        'pptx'  => 'application/vnd.ms-powerpoint',
        'xls'   => 'application/vnd.ms-excel',
        'xlsx'  => 'application/vnd.ms-excel',
        'txt'   => 'text/plain',
    ];

    /**
     * The file handler.
     *
     * @var
     */
    private $file;

    /**
     * The actual name of the file.
     *
     * @var
     */
    private $fileName;

    /**
     * The actual size of the file.
     *
     * @var
     */
    private $fileSize;

    /**
     * The actual temporary name of the file.
     *
     * @var
     */
    private $fileTmpName;

    /**
     * The actual file extension.
     *
     * @var
     */
    private $extension;

    /**
     * The maximum size allowed for uploading in MB.
     *
     * @var int
     */
    private $maxSize = 30;

    /**
     * Initialize the file attributes.
     *
     * @param string $file
     * @param int $maxSize
     */
    public function __construct($file, $maxSize = 30)
    {
        $this->file     = $file;
        $this->maxSize  = $maxSize;

        $this->init();
    }

    /**
     * Return all available file extensions.
     *
     * @return array
     */
    public static function getExtensions()
    {
        return self::$fileExtensions;
    }

    /**
     * Return all available file content types.
     *
     * @return array
     */
    public static function getContentTypes()
    {
        return self::$contentTypes;
    }

    /**
     * Save the document file inside it's public document path.
     * Also set a session with the file name for future database processing.
     *
     * @return bool
     */
    public function save()
    {
        if ($this->fileName) {
            $this->errors();

            $fileName = md5(String::random(25)) . '.' . $this->extension;

            while (File::exists(Dir::getFile() . $fileName)) {
                $fileName = md5(String::random(25)) . '.' . $this->extension;
            }

            move_uploaded_file($this->fileTmpName, Dir::getFile() . $fileName);

            if (!File::exists(Dir::getFile() . $fileName)) {
                Flash::error('Upload failed!');
                return false;
            }

            Session::set('upload-file', $fileName);
            return true;
        }

        Flash::error('Upload failed! Select a file.');
        Redirect::url(Url::getPreviousUrl());

        return false;
    }

    /**
     * Helper method for displaying the document file inside an admin edit view.
     *
     * @param string $document
     * @param array|null $options
     * @return string
     * @throws Exception
     */
    public static function display($document, $options = null)
    {
        $optionString = '';

        if ($options) {
            if (!is_array($options)) {
                throw new Exception('The OPTIONS parameter must be a non-empty array');
            }

            foreach ($options as $key => $val) {
                $optionString .= $key . '="' . $val . '" ';
            }
        }

        return '<a href="' . Dir::getUploads() . '/file/' . $document . '" target="_blank" ' . $optionString . '>View File</a>';
    }

    /**
     * Initialize the file attributes.
     *
     * @return void
     */
    private function init()
    {
        $this->fileName     = is_array($this->file['name']) ? $this->file['name'][0] : $this->file['name'];
        $this->fileSize     = is_array($this->file['size']) ? $this->file['size'][0] : $this->file['size'];
        $this->fileTmpName  = is_array($this->file['tmp_name']) ? $this->file['tmp_name'][0] : $this->file['tmp_name'];
    }

    /**
     * Handle possible generic errors.
     *
     * @return bool
     */
    public function errors()
    {
        preg_match('/\.([^.]+)$/i', $this->fileName, $this->extension);
        $this->extension  = end($this->extension);

        if (!in_array(strtolower($this->extension), self::$fileExtensions)) {
            Flash::error('Invalid file format ' . $this->extension . '. Choose one of: ' . implode(', ', self::$fileExtensions));
            Redirect::url(Url::getPreviousUrl());
        }

        if ($this->fileSize > ($this->maxSize * 1024 * 1024)) {
            Flash::error('The file is too large! Please try with a smaller file.');
            Redirect::url(Url::getPreviousUrl());
        }
    }
}