<?php

class Validator
{
    /**
     * The field handler for validation.
     *
     * @var
     */
    private static $field;

    /**
     * The field name based on the type of request.
     * POST or GET.
     *
     * @var
     */
    private static $fieldName;

    /**
     * Set the field and field name based on the type of request.
     *
     * @param string $field
     */
    public static function setField($field)
    {
        self::$fieldName = $field;

        foreach (Request::isPost() ? $_POST : $_GET as $name => $values) {
            if (isset($values[$field])) {
                self::$field = $values[$field];
                break;
            }
        }
    }

    /**
     * Validate of every type the given fields.
     * The $fields parameter must be an array of this type:
     * - array("field" => array("validation_type_1", "validation_type_2"))
     *
     * @param array $fields
     * @return bool|mixed
     * @throws Exception
     */
    public static function validate($fields)
    {
        if (!is_array($fields) || empty($fields)) {
            throw new Exception('The FIELDS parameter must be a non-empty array');
        }

        $response = [];

        foreach (array_reverse($fields) as $field => $types) {
            foreach ($types as $type) {
                $function   = 'validate' . ucwords($type);
                $response[] = self::$function($field) ? true : false;
            }
        }

        return count(array_unique($response)) === 1 ? current($response) : false;
    }

    /**
     * Validate if the given field exists.
     * Used for required fields.
     *
     * @param string $field
     * @return bool
     */
    public static function validatePresence($field)
    {
        self::setField($field);

        if (!isset(self::$field) || self::$field == '') {
            self::$fieldName = str_replace('_', ' ', self::$fieldName);

            Flash::error('The field <strong>' . ucwords(self::$fieldName) . '</strong> must not be empty!');
            return false;
        }

        return true;
    }

    /**
     * Validate if the given field is a number.
     *
     * @param string $field
     * @return bool
     */
    public static function validateNumeric($field)
    {
        self::setField($field);

        if (self::$field) {
            if (!is_numeric(preg_replace('/\s+/', '', self::$field))) {
                self::$fieldName = str_replace('_', ' ', self::$fieldName);

                Flash::error('The field <strong>' . ucwords(self::$fieldName) . '</strong> must contain only numbers!');
                return false;
            }

            return true;
        }

        return true;
    }

    /**
     * Validate if the given field contains only letters.
     *
     * @param string $field
     * @return bool
     */
    public static function validateLetter($field)
    {
        self::setField($field);

        if (self::$field) {
            if (ctype_alpha(preg_replace('/\s+/', '', self::$field))) {
                if (is_numeric(trim(self::$field))) {
                    self::$fieldName = str_replace('_', ' ', self::$fieldName);

                    Flash::error('The field <strong>' . ucwords(self::$fieldName) . '</strong> must contain only letters!');
                    return false;
                }

                return true;
            } else {
                self::$fieldName = str_replace('_', ' ', self::$fieldName);

                Flash::error('The field <strong>' . ucwords(self::$fieldName) . '</strong> must contain only letters!');
                return false;
            }
        }

        return true;
    }

    /**
     * Validate if the given field is of type email.
     *
     * @param string $field
     * @return bool
     */
    public static function validateEmail($field)
    {
        self::setField($field);

        if (self::$field) {
            if (!filter_var(self::$field, FILTER_VALIDATE_EMAIL)) {
                self::$fieldName = str_replace('_', ' ', self::$fieldName);

                Flash::error('The field <strong>' . ucwords(self::$fieldName) . '</strong> is not a valid email format!');
                return false;
            }

            return true;
        }

        return true;
    }
}