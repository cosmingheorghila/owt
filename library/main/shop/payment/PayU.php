<?php

require_once(Dir::getLibrary() . '/main/shop/clients/PayU/PayU.php');

class Shop_Payment_PayU
{
    /**
     * Identifier to know if the payment functionality is in testing or not.
     *
     * @var bool
     */
    public $test = true;

    /**
     * The URL where to send the PayU generated data.
     *
     * @var string
     */
    public $url = 'https://secure.payu.ro/order/lu.php';

    /**
     * The secret key of the merchant found in:
     * Control Panel -> Account Management -> Account Settings section
     *
     * @var string
     */
    public $key = '';

    /**
     * The merchant id found in:
     * Control Panel -> Account Management -> Account Settings section
     *
     * @var string
     */
    public $merchant = '';

    /**
     * The handler for the PayU handshake.
     *
     * @var
     */
    public $client;

    /**
     * All the PayU available payment methods.
     *
     * @const
     */
    const PAY_METHOD_CARD           = 'CCVISAMC';
    const PAY_METHOD_BRD_FINANCE    = 'BRDF';
    const PAY_METHOD_STAR_BT        = 'STAR_BT';
    const PAY_METHOD_CARD_AVANTAJ   = 'CARD_AVANTAJ';
    const PAY_METHOD_CLICK_24       = 'ITRANSFER_BCR';
    const PAY_METHOD_BT_24          = 'ITRANSFER_BT';
    const PAY_METHOD_ZEBRA_PAY      = 'ZEBRA_PAY';
    const PAY_METHOD_PAY_PAL        = 'PAYPAL';
    const PAY_METHOD_TRANSFER       = 'WIRE';

    /**
     * All the PayU available payment methods options.
     *
     * @var array
     */
    public static $paymentMethods = [
        self::PAY_METHOD_CARD           => 'Credit Card',
        self::PAY_METHOD_BRD_FINANCE    => 'BRD Finance',
        self::PAY_METHOD_STAR_BT        => 'Star BT',
        self::PAY_METHOD_CARD_AVANTAJ   => 'Card Avantaj',
        self::PAY_METHOD_CLICK_24       => 'Click 24',
        self::PAY_METHOD_BT_24          => 'BT 24',
        self::PAY_METHOD_ZEBRA_PAY      => 'Zebra Pay',
        self::PAY_METHOD_PAY_PAL        => 'PayPal',
        self::PAY_METHOD_TRANSFER       => 'Bank Transfer',
    ];

    /**
     * Set the client handler for further requests.
     *
     * return void
     */
    public function __construct()
    {
        $this->client = new LiveUpdate($this->key);
    }

    /**
     * Payment method using PayU.
     * This method populates all the data required by PayU.
     * Use the result of this method to create a form pointing to the PayU secured URL:
     *
     * https://secure.payu.ro/order/lu.php
     *
     * @param Shop_Model_Order $order
     * @param array $cart
     * @param array $customer
     * @return LiveUpdate
     * @throws Exception
     */
    public function pay(Shop_Model_Order $order, $cart = [], $customer = [])
    {
        if (!$cart || empty($cart)) {
            throw new Exception('The PRODUCTS parameter must be a non-empty array, each element containing a cart model instance');
        }

        $this->setOrder($order, $cart);
        $this->setAddresses($customer);

        $this->client->setTestMode($this->test);

        return $this->client;
    }

    /**
     * Set the order products as instructed in the PayU manual.
     *
     * @param Shop_Model_Order $order
     * @param array $cart
     */
    private function setOrder(Shop_Model_Order $order, $cart = [])
    {
        $productNames       = [];
        $productCodes       = [];
        $productInfo        = [];
        $productPrices      = [];
        $productPriceTypes  = [];
        $productQuantities  = [];
        $productVats        = [];

        $total      = 0;
        $discount   = 0;
        $tax        = 0;

        foreach ($cart as $item) {
            $product = $item->getProduct();
            $total  += $item->getQuantity() * $product->getFinalPrice();

            $productNames[]         = String::truncate($product->getName(), 150);
            $productCodes[]         = $product->hasVendors() ? $product->getVendor()->getSku() : $product->getSku();
            $productInfo[]          = $product->getDescription();
            $productPrices[]        = number_format($product->getFinalPrice(), 2, '.', '');
            $productPriceTypes[]    = 'GROSS';
            $productQuantities[]    = $item->getQuantity();
            $productVats[]          = $product->getIncludesTva() !== null && $product->getIncludesTva() == Shop_Model_Product::TVA_NO ? 24 : 0;
        }

        if ((float)$order->getData('grand_total') > (float)$total) {
            $tax = (float)$order->getData('grand_total') - (float)$total;

            $productNames[]         = $order->getIdentifier() . ' - Additional Taxes';
            $productCodes[]         = $order->getIdentifier() . '|tax';
            $productInfo[]          = 'Other additional taxes coming from PcBit S.R.L.';
            $productPrices[]        = number_format($tax, 2, '.', '');
            $productPriceTypes[]    = 'GROSS';
            $productQuantities[]    = 1;
            $productVats[]          = 0;
        } else {
            $discount = (float)$total - (float)$order->getData('grand_total');
        }

        $this->client->setMerchant($this->merchant);
        $this->client->setOrderRef($order->getIdentifier());
        $this->client->setOrderDate(date('Y-m-d H:i:s', time()));

        $this->client->setPayMethod(self::PAY_METHOD_CARD);
        $this->client->setPricesCurrency('RON');
        $this->client->setDiscount(number_format($discount, 2, '.', ''));

        $this->client->setOrderPName($productNames);
        $this->client->setOrderPCode($productCodes);
        $this->client->setOrderPrice($productPrices);
        $this->client->setOrderPType($productPriceTypes);
        $this->client->setOrderQTY($productQuantities);
        $this->client->setOrderVAT($productVats);
    }

    /**
     * Set the billing address and delivery address based on the POST data for a customer.
     * If the customer data passed to the pay() method differs from the one below, feel free to change it.
     *
     * The customer data must have the keys from the right side of the method below.
     *
     * @param array $customer
     */
    private function setAddresses($customer = [])
    {
        $billing    = [];
        $delivery   = [];

        $billing['billFName']       = isset($customer['first_name']) ? $customer['first_name'] : '';
        $billing['billLName']       = isset($customer['last_name']) ? $customer['last_name'] : '';
        $billing['billCISerial']    = isset($customer['ci_series']) ? $customer['ci_series'] : '';
        $billing['billCINumber']    = isset($customer['ci_nr']) ? $customer['ci_nr'] : '';
        $billing['billCIIssuer']    = isset($customer['ci_issuer']) ? $customer['ci_issuer'] : '';
        $billing['billCNP']         = isset($customer['cnp']) ? $customer['cnp'] : '';
        $billing['billCompany']     = isset($customer['company_name']) ? $customer['company_name'] : '';
        $billing['billFiscalCode']  = isset($customer['company_cui']) ? $customer['company_cui'] : '';
        $billing['billRegNumber']   = isset($customer['company_nr']) ? $customer['company_nr'] : '';
        $billing['billBank']        = isset($customer['bank_name']) ? $customer['bank_name'] : '';
        $billing['billBankAccount'] = isset($customer['bank_account']) ? $customer['bank_account'] : '';
        $billing['billBankAccount'] = isset($customer['bank_account']) ? $customer['bank_account'] : '';
        $billing['billEmail']       = isset($customer['email']) ? $customer['email'] : '';
        $billing['billPhone']       = isset($customer['phone']) ? $customer['phone'] : '';
        $billing['billFax']         = isset($customer['fax']) ? $customer['fax'] : '';
        $billing['billAddress1']    = isset($customer['billing_address']) ? $customer['billing_address'] : '';
        $billing['billAddress2']    = '';
        $billing['billZipCode']     = isset($customer['billing_postal_code']) ? $customer['billing_postal_code'] : '';
        $billing['billCity']        = isset($customer['billing_city']) ? $customer['billing_city'] : '';
        $billing['billState']       = isset($customer['billing_county']) ? $customer['billing_county'] : '';
        $billing['billCountryCode'] = '';

        $delivery['deliveryFName']          = isset($customer['first_name']) ? $customer['first_name'] : '';
        $delivery['deliveryLName']          = isset($customer['last_name']) ? $customer['last_name'] : '';
        $delivery['deliveryCompany']        = isset($customer['company_name']) ? $customer['company_name'] : '';
        $delivery['deliveryPhone']          = isset($customer['phone']) ? $customer['phone'] : '';
        $delivery['deliveryAddress1']       = isset($customer['shipping_address']) ? $customer['shipping_address'] : '';
        $delivery['deliveryAddress2']       = '';
        $delivery['deliveryZipCode']        = isset($customer['shipping_postal_code']) ? $customer['shipping_postal_code'] : '';
        $delivery['deliveryCity']           = isset($customer['shipping_city']) ? $customer['shipping_city'] : '';
        $delivery['deliveryState']          = isset($customer['shipping_county']) ? $customer['shipping_county'] : '';
        $delivery['deliveryCountryCode']    = '';

        $this->client->setBilling($billing);
        $this->client->setDelivery($delivery);
    }
}