<?php

require_once(Dir::getLibrary() . '/main/shop/clients/Mobilpay/Payment/Request/Abstract.php');
require_once(Dir::getLibrary() . '/main/shop/clients/Mobilpay/Payment/Request/Card.php');
require_once(Dir::getLibrary() . '/main/shop/clients/Mobilpay/Payment/Request/Notify.php');
require_once(Dir::getLibrary() . '/main/shop/clients/Mobilpay/Payment/Invoice.php');
require_once(Dir::getLibrary() . '/main/shop/clients/Mobilpay/Payment/Address.php');

class Shop_Payment_MobilPay
{
    /**
     * This property is used to know which private.key and public.cer to include.
     * This property can have the value sandbox or production
     *
     * @var string
     */
    public static $environment = 'production';

    /**
     * Unique key that identifies the sales account with the payment process.
     * www.mobilpay.ro Admin -> Conturi de comerciant -> Detalii -> Setari securitate
     *
     * @var string
     */
    private $signature = 'FSK2-4SLX-NXNJ-RG2K-MKAX';

    /**
     * The payment URL.
     * To go into testing mode, change this to http://sandboxsecure.mobilpay.ro.
     * To go into live mode, change this to https://secure.mobilpay.ro
     *
     * @var string
     */
    public $paymentUrl = 'https://secure.mobilpay.ro';

    /**
     * The URL where mobilPay will send the payment result.
     * This URL will always be called first.
     *
     * @var
     */
    public $confirmUrl;

    /**
     * The URL where mobilPay redirects the client once the payment process is finished.
     * Not to be mistaken for a "successURL" nor "cancelURL".
     *
     * @var
     */
    public $returnUrl;

    /**
     * The client handler used in the payment initiation.
     *
     * @var
     */
    public $client;

    /**
     * Path to the private key file used for authentication.
     * You may download this from Admin -> Conturi de comerciant -> Detalii -> Setari securitate
     *
     * @var string
     */
    private $publicKeyPath;

    /**
     * Path to the private key file used for authentication.
     * You may download this from Admin -> Conturi de comerciant -> Detalii -> Setari securitate
     *
     * @var string
     */
    private $privateKeyPath;

    /**
     * @param string|null $confirmUrl
     * @param string|null $returnUrl
     */
    public function __construct($confirmUrl = null, $returnUrl = null)
    {
        $this->confirmUrl   = Url::getHostUrl() . $confirmUrl;
        $this->returnUrl    = Url::getHostUrl() . $returnUrl;

        $this->publicKeyPath    = Dir::getLibrary() . '/main/shop/clients/Mobilpay/' . self::$environment . '_public.cer';
        $this->privateKeyPath   = Dir::getLibrary() . '/main/shop/clients/Mobilpay/' . self::$environment . '_private.key';

        $this->client = new Mobilpay_Payment_Request_Card();
    }

    /**
     *
     * ------------------------------------------------------------------------------------------------------------------------
     * --- $invoice
     * Must be an array containing the keys:
     * amount, currency, details
     * ------------------------------------------------------------------------------------------------------------------------
     *
     * ------------------------------------------------------------------------------------------------------------------------
     * --- $billing
     * Must be an array containing the keys:
     * type(person/company), first_name, last_name, address, email, phone
     * ------------------------------------------------------------------------------------------------------------------------
     *
     * ------------------------------------------------------------------------------------------------------------------------
     * --- $delivery
     * Must be an array containing the keys:
     * type(person/company), first_name, last_name, address, email, phone
     * ------------------------------------------------------------------------------------------------------------------------
     *
     * @param string|null $orderId
     * @param array $invoice
     * @param array $billing
     * @param array $delivery
     * @throws Exception
     */
    public function pay($orderId = null, $invoice = [], $billing = [], $delivery = [])
    {
        if (!$invoice['amount'] || $invoice['amount'] == '' || !$invoice['currency'] || $invoice['currency'] == '') {
            throw new Exception('The INVOICE parameter must be an array containing at least the keys: amount, currency');
        }

        if (!$billing['type'] || $billing['type'] == '' || !$billing['first_name'] || $billing['first_name'] == '' || !$billing['last_name'] || $billing['last_name'] == '' || !$billing['address'] || $billing['address'] == '' || !$billing['email'] || $billing['email'] == '' || !$billing['phone'] || $billing['phone'] == '') {
            throw new Exception('The BILLING parameter must be an array containing the keys: type, first_name, last_name, address, email, phone');
        }

        if (!$delivery['type'] || $delivery['type'] == '' || !$delivery['first_name'] || $delivery['first_name'] == '' || !$delivery['last_name'] || $delivery['last_name'] == '' || !$delivery['address'] || $delivery['address'] == '' || !$delivery['email'] || $delivery['email'] == '' || !$delivery['phone'] || $delivery['phone'] == '') {
            throw new Exception('The BILLING parameter must be an array containing the keys: type, first_name, last_name, address, email, phone');
        }

        try {
            srand((double) microtime() * 1000000);

            $this->client->signature   = $this->signature;
            $this->client->orderId     = $orderId ? $orderId : md5(uniqid(rand()));
            $this->client->confirmUrl  = $this->confirmUrl;
            $this->client->returnUrl   = $this->returnUrl;

            $this->client->invoice             = new Mobilpay_Payment_Invoice();
            $this->client->invoice->currency   = $invoice['currency'];
            $this->client->invoice->amount     = (float)$invoice['amount'];
            $this->client->invoice->details    = $invoice['details'] ?: '';

            $billingAddress 				= new Mobilpay_Payment_Address();
            $billingAddress->type			= $billing['type'];
            $billingAddress->firstName		= $billing['first_name'];
            $billingAddress->lastName		= $billing['last_name'];
            $billingAddress->address		= $billing['address'];
            $billingAddress->email			= $billing['email'];
            $billingAddress->mobilePhone	= $billing['phone'];

            $this->client->invoice->setBillingAddress($billingAddress);

            $shippingAddress 				= new Mobilpay_Payment_Address();
            $shippingAddress->type			= $delivery['type'];
            $shippingAddress->firstName		= $delivery['first_name'];
            $shippingAddress->lastName		= $delivery['last_name'];
            $shippingAddress->address		= $delivery['address'];
            $shippingAddress->email		    = $delivery['email'];
            $shippingAddress->mobilePhone	= $delivery['phone'];

            $this->client->invoice->setShippingAddress($shippingAddress);
            $this->client->encrypt($this->publicKeyPath);
        } catch(Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * ATTENTION!
     * ------------------------------------------------------------------------------------------------------------------------
     * This method is not to be used just as is, because it requires extra custom logic.
     * To implement the confirmURL logic, copy this code into a controller action and add the extra logic.
     * This method is just an example from MobilPay of how a confirmURL should handle the response data sent by MobilPay.
     * ------------------------------------------------------------------------------------------------------------------------
     *
     * MobilPay code for the confirmURL.
     * The confirmURL is the URL that MobilPay accesses it asynchronously in order to update an order's status.
     *
     * Follow the comments supplied by MobilPay to know what to do and how to implement.
     * ------------------------------------------------------------------------------------------------------------------------
     *
     * @return void
     */
    private function confirm()
    {
        if (strcasecmp($_SERVER['REQUEST_METHOD'], 'post') == 0) {
            if(isset($_POST['env_key']) && isset($_POST['data'])) {
                try {
                    $objPmReq   = Mobilpay_Payment_Request_Abstract::factoryFromEncrypted($_POST['env_key'], $_POST['data'], Dir::getLibrary() . '/main/shop/clients/Mobilpay/' . self::$environment . '_private.key');
                    $errorCode  = $objPmReq->objPmNotify->errorCode;

                    if ($errorCode == "0") {
                        switch($objPmReq->objPmNotify->action) {
                            //Any action comes with an error code and an error message. These can be called like this: $objPmReq->objPmNotify->errorCode and $objPmReq->objPmNotify->errorMessage
                            //To get the order id use: $objPmReq->orderId;
                            case 'confirmed':
                                //When action is "confirmed", we know that the money's or the way. We update the order with the "paid" status and make the delivery.
                                //UPDATE TABLE SET status = "confirmed/captured"
                                $errorMessage = $objPmReq->objPmNotify->errorMessage;
                                break;
                            case 'confirmed_pending':
                                //When action is "confirmed_pending" it means that the transaction is verified anti-fraud. We don't deliver.
                                //UPDATE TABLE SET status = "pending"
                                $errorMessage = $objPmReq->objPmNotify->errorMessage;
                                break;
                            case 'paid_pending':
                                //When action is "paid_pending" it means that the transaction is in the validation process. We don't deliver.
                                //UPDATE TABLE SET status = "pending"
                                $errorMessage = $objPmReq->objPmNotify->errorMessage;
                                break;
                            case 'paid':
                                //When action is "paid" it means that the transaction is in the validation process. We don't deliver.
                                //UPDATE TABLE SET status = "open/preauthorized"
                                $errorMessage = $objPmReq->objPmNotify->errorMessage;
                                break;
                            case 'canceled':
                                //When action is "canceled" it means that the transaction has been canceled. We don't deliver.
                                //UPDATE TABLE SET status = "canceled"
                                $errorMessage = $objPmReq->objPmNotify->errorMessage;
                                break;
                            case 'credit':
                                //When action is "credit" it means that the money have been returned to the payer. We don't deliver or reverse if already delivered.
                                //UPDATE TABLE SET status = "refunded"
                                $errorMessage = $objPmReq->objPmNotify->errorMessage;
                                break;
                            default:
                                $errorType		= Mobilpay_Payment_Request_Abstract::CONFIRM_ERROR_TYPE_PERMANENT;
                                $errorCode 		= Mobilpay_Payment_Request_Abstract::ERROR_CONFIRM_INVALID_ACTION;
                                $errorMessage 	= 'mobilpay_refference_action paramaters is invalid';
                                break;
                        }
                    } else {
                        //UPDATE TABLE SET status = "rejected"
                        $errorMessage = $objPmReq->objPmNotify->errorMessage;
                    }
                } catch(Exception $e) {
                    $errorType 		= Mobilpay_Payment_Request_Abstract::CONFIRM_ERROR_TYPE_TEMPORARY;
                    $errorCode		= $e->getCode();
                    $errorMessage 	= $e->getMessage();
                }
            } else {
                $errorType 		= Mobilpay_Payment_Request_Abstract::CONFIRM_ERROR_TYPE_PERMANENT;
                $errorCode		= Mobilpay_Payment_Request_Abstract::ERROR_CONFIRM_INVALID_POST_PARAMETERS;
                $errorMessage 	= 'mobilpay.ro posted invalid parameters';
            }
        } else {
            $errorType 		= Mobilpay_Payment_Request_Abstract::CONFIRM_ERROR_TYPE_PERMANENT;
            $errorCode		= Mobilpay_Payment_Request_Abstract::ERROR_CONFIRM_INVALID_POST_METHOD;
            $errorMessage 	= 'invalid request metod for payment confirmation';
        }

        header('Content-type: application/xml');

        echo "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";

        if($errorCode == 0) {
            echo "<crc>{$errorMessage}</crc>";
        } else {
            echo "<crc error_type=\"{$errorType}\" error_code=\"{$errorCode}\">{$errorMessage}</crc>";
        }
    }
}