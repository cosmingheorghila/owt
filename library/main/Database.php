<?php

class Database extends PDO
{
    /**
     * The table that the Database is working with.
     *
     * @var null|string
     */
    protected $table;

    /**
     * The database handler.
     *
     * @var PDO
     */
    protected $db;

    /**
     * The sql handler.
     *
     * @var
     */
    protected $sql;

    /**
     * Database mirror object
     *
     * @var
     */
    private static $instance;

    /**
     * The database connection verifier.
     * If set to true, then it means a connection to the database has already been established.
     *
     * @var PDO
     */
    private static $connection;

    /**
     * @var array
     */
    private $escapes = array(
        '"', "'", ';', '--', '(', ')'
    );

    /**
     * Get a fresh new instance of the Database object.
     *
     * @param string|null $table
     * @return Database
     */
    public static function getInstance($table = null)
    {
        return self::$instance = new self($table);
    }

    /**
     * Manage fetching of PDO attributes.
     *
     * @param string $method
     * @param array|null $args
     * @return bool|mixed|PDOStatement
     * @throws Exception
     */
    public function __call($method, $args = null)
    {
        if (String::startsWith($method, 'get')) {
            return $this->{strtolower(preg_replace('/(\p{Ll})(\p{Lu})/u', '\1_\2', end(explode('get', $method))))};
        } else {
            throw new Exception('MAGIC METHOD unknown. Try get{Field}');
        }
    }

    /**
     * Set the table the database will work with.
     * Establish a new connection if necessary or persist the existing one.
     *
     * @param string|null $table
     * @throws Exception
     */
    public function __construct($table = null)
    {
        $this->table = $table;

        if (!self::$connection) {
            try {
                $host   = (string)Config::get('database')->host;
                $dbName = (string)Config::get('database')->db_name;
                $dbUser = (string)Config::get('database')->db_user;
                $dbPass = (string)Config::get('database')->db_password;

                $this->db = new PDO('mysql:host=' . $host . ';dbname=' . $dbName, $dbUser, $dbPass, [
                    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
                ]);

                self::$connection = $this->db;
            } catch (PDOException $e) {
                throw new Exception($e->getMessage());
            }
        } else {
            $this->db = self::$connection;
        }
    }

    /**
     * Manually sets the database table, ignoring the table set in __construct.
     *
     * @param string $table
     * @return $this
     */
    public function setTable($table)
    {
        $this->table = $table;

        return $this;
    }

    /**
     * Return the database table name.
     *
     * @return null|string
     */
    public function getTable()
    {
        return $this->table;
    }

    /**
     * Return the entire sql query that has been constructed.
     * Mostly for debugging purposes.
     *
     * @return mixed
     */
    public function getSelect()
    {
        return $this->sql;
    }

    /**
     * Escape the input values, wrapping them in single quotes.
     *
     * @param array|string|int $value
     * @return string
     */
    public function quote($value)
    {
        $result = [];

        if (is_array($value) && !empty($value)) {
            foreach ($value as $val) {
                if ($this->table == 'cms_blocks' || $this->table == 'admin_settings' || $this->table == 'admin_analytics') {
                    $result[] = $this->db->quote($val);
                } else {
                    $result[] = $this->db->quote(str_replace($this->escapes, '', $val));
                }
            }

            return $result;
        }

        if ($this->table == 'cms_blocks' || $this->table == 'admin_settings' || $this->table == 'admin_analytics') {
            return $this->db->quote($value);
        } else {
            return $this->db->quote(str_replace($this->escapes, '', $value));
        }
    }

    /**
     * Real escape the input values.
     * Should be used whenever working with user input values.
     *
     * @param array|string|int $value
     * @return string
     */
    public function escape($value)
    {
        $result = [];

        if (is_array($value) && !empty($value)) {
            foreach ($value as $val) {
                if ($this->table == 'cms_blocks' || $this->table == 'admin_settings' || $this->table == 'admin_analytics') {
                    $result[] = substr(substr($this->db->quote($val), 1), 0, -1);
                } else {
                    $result[] = substr(substr($this->db->quote(str_replace($this->escapes, '', $val)), 1), 0, -1);
                }
            }

            return $result;
        }

        if ($this->table == 'cms_blocks' || $this->table == 'admin_settings' || $this->table == 'admin_analytics') {
            return substr(substr($this->db->quote($value), 1), 0, -1);
        } else {
            return substr(substr($this->db->quote(str_replace($this->escapes, '', $value)), 1), 0, -1);
        }
    }

    /**
     * Reset the sql query.
     * Useful when using the same Database instance for multiple operations.
     *
     * @return $this
     */
    public function reset()
    {
        $this->sql = '';

        return $this;
    }

    /**
     * Construct an entire sql query which can later be parsed.
     * Mostly used for complex queries, where the ORM will be useless.
     *
     * @param string $value
     * @return PDOStatement
     */
    public function query($value)
    {
        $this->sql = $value;

        return $this;
    }

    /**
     * Prepares a statement for execution and returns a statement object.
     * The statement must be a valid sql query.
     *
     * @param $statement
     * @return PDOStatement
     */
    public function prepare($statement)
    {
        return $this->db->prepare($statement);
    }

    /**
     * Helper method for generating a select query.
     * Supports method chaining with other helper methods.
     *
     * @param string $columns
     * @param string|null $table
     * @return $this
     */
    public function select($columns = '*', $table = null)
    {
        if (is_array($columns)) {
            $columns = implode(',', $columns);
        }

        $table      = $table ?: $this->table;
        $this->sql  = "SELECT {$columns} FROM {$table}";

        return $this;
    }

    /**
     * Helper method for creating an insert sql query.
     * Also, supports a unique row set check. The $unique parameter should be an associative array of column => value type.
     * Supports method chaining with other helper methods.
     *
     * @param array $values
     * @param array|bool $unique
     * @return $this|bool
     * @throws Exception
     */
    public function insert($values, $unique = null)
    {
        if (!is_array($values)) {
            throw new Exception('The VALUES parameter must be a non-empty array');
        }

        if ($unique && !is_array($unique)) {
            throw new Exception('The UNIQUE parameter must be an array or null');
        }

        if ($unique) {
            $where = implode(' OR ', array_map(function ($key, $val) {
                return sprintf("%s = %s", $key, $val);
            }, $this->escape(array_keys(array_filter($unique))), $this->quote(array_values(array_filter($unique)))));

            if ($this->db->query("SELECT * FROM {$this->table} WHERE {$where}")->rowCount() > 0) {
                Flash::error('There is already an entry with that <strong>' . ucwords(str_replace('_', ' ', current(array_keys($unique)))) . '</strong>');
                return false;
            }
        }

        $_keys      = implode(",", array_keys($values));
        $_values    = str_replace("'null'", "NULL", implode(",", $this->quote(array_values($values))));

        $this->sql = "INSERT INTO {$this->table} ({$_keys}) VALUES ({$_values})";

        return $this;
    }

    /**
     * Helper method for creating an update sql query.
     * Also, supports a unique row set check. The $unique parameter should be a loaded instance of a model object.
     * Supports method chaining with other helper methods.
     *
     * @param array $values
     * @param array|null $unique
     * @param Model|Database|null $row
     * @return $this
     * @throws Exception
     */
    public function update($values, $unique = null, $row = null)
    {
        if (!is_array($values) && !is_string($values)) {
            throw new Exception('The VALUES parameter must be a non-empty array or a string');
        }

        if ($unique && !is_array($unique)) {
            throw new Exception('The UNIQUE parameter must be an array or null');
        }

        if ($unique && !$row) {
            throw new Exception('For UNIQUE operations you must specify the ROW parameter');
        }

        if ($unique) {
            $where = implode(' OR ', array_map(function ($key, $val) use ($row) {
                return sprintf("(%s = %s AND %s != '%s')", $key, $val, $key, strtolower($key) == 'url' && $row->getClass() != 'Cms_Model_Menu' ? $row->getUrl() : $row->getData($key));
            }, $this->escape(array_keys(array_filter($unique))), $this->quote(array_values(array_filter($unique)))));

            if ($this->db->query("SELECT * FROM {$this->table} WHERE {$where}")->rowCount() > 0) {
                Flash::error('There is already an entry with that <strong>' . ucwords(str_replace('_', ' ', current(array_keys($unique)))) . '</strong>');
                return false;
            }
        }

        $this->sql = "UPDATE {$this->table} SET ";

        if (is_array($values) && !empty($values)) {
            $this->sql .= str_replace("'null'", "NULL", implode(', ', array_map(function ($key, $val) {
                return isset($val) ? sprintf("%s = %s", $key, $val) : null;
            }, $this->escape(array_keys($values)), $this->quote(array_values($values)))));
        } elseif (is_string($values)) {
            $this->sql .= $values;
        }

        return $this;
    }

    /**
     * Helper method for creating a delete query.
     * Supports method chaining with other helper methods.
     *
     * @return $this
     */
    public function delete()
    {
        $this->sql = "DELETE FROM {$this->table}";

        return $this;
    }

    /**
     * Helper method for creating where sql queries.
     * Conditions can be supplied as a string or an associative array.
     * Supports method chaining with other helper methods.
     *
     * @param array|string $conditions
     * @param string $operator
     * @return $this
     * @throws Exception
     */
    public function where($conditions, $operator = 'AND')
    {
        if (is_array($conditions) && !empty($conditions)) {
            $conditions = implode(" {$operator} ", array_map(function ($key, $val) {
                return strtoupper($val) == "'NULL'" ? sprintf("%s IS NULL", $key) : sprintf("%s = %s", $key, $val);
            }, $this->escape(array_keys($conditions)), $this->quote(array_values($conditions))));
        }

        $this->sql .= String::contains($this->sql, 'WHERE') ? " AND ({$conditions})" : " WHERE ({$conditions})";

        return $this;
    }

    /**
     * Helper method for creating a join sql query.
     * Supports method chaining with other helper methods.
     * Although, the on() method should come right after this.
     *
     * @param string $table
     * @param string $type
     * @return $this
     * @throws Exception
     */
    public function join($table, $type = 'INNER')
    {
        $this->sql .= " {$type} JOIN {$table}";

        return $this;
    }

    /**
     * Helper method for establishing where conditions on a join sql query.
     * Supports method chaining with other helper methods.
     *
     * @param array|string $conditions
     * @return $this
     * @throws Exception
     */
    public function on($conditions)
    {
        $this->sql .= " ON ";

        if (is_array($conditions)) {
            $this->sql .= implode(" AND ", array_map(function ($key, $val) {
                return sprintf("%s = %s", $key, $val);
            }, $this->escape(array_keys($conditions)), $this->escape(array_values($conditions))));
        } else {
            $this->sql .= "{$conditions}";
        }

        return $this;
    }

    /**
     * Helper method for creating an order sql query.
     * For multi-field sorting, just use the $fields parameter as an array.
     * If the $fields parameter is an array, then the $key will be the column and the $value will be the direction.
     * Supports method chaining with other helper methods.
     *
     * @param array|string $fields
     * @param string $direction
     * @return $this
     * @throws Exception
     */
    public function order($fields, $direction = 'ASC')
    {
        $this->sql .= " ORDER BY ";

        if ($fields == 'Rand()') {
            $this->sql .= 'Rand()';
        } else if (is_array($fields) && !empty($fields)) {
            $this->sql .= implode(", ", array_map(function ($field, $direction) {
                return $field . " " . strtoupper($direction);
            }, $this->escape(array_keys($fields)), $this->escape(array_values($fields))));
        } else {
            $this->sql .= $fields. ' ' . strtoupper($direction);
        }

        return $this;
    }

    /**
     * Helper method for creating a limit sql query.
     * Supports method chaining with other helper methods.
     *
     * @param int $limit
     * @param int $offset
     * @return $this
     * @throws Exception
     */
    public function limit($limit, $offset = 0)
    {
        $this->sql .= " LIMIT {$limit} OFFSET {$offset}";

        return $this;
    }

    /**
     * Helper method for creating a group sql query.
     * Supports method chaining with other helper methods.
     *
     * @param string $column
     * @return $this
     */
    public function group($column)
    {
        $this->sql .= " GROUP BY {$column}";

        return $this;
    }

    /**
     * Helper method for creating a distinct sql query.
     * Supports method chaining with other helper methods.
     *
     * @return $this
     */
    public function distinct()
    {
        $this->sql = substr($this->sql, 0, 6) . " DISTINCT" . substr($this->sql, 6);

        return $this;
    }

    /**
     * Get the records count based on the SQL constructed to this point.
     * The count ignores the LIMIT parameter in the SQL.
     *
     * @return int
     */
    public function count()
    {
        $statement = $this->prepare(preg_replace('/ LIMIT \d+ OFFSET \d+/', "", str_replace('SELECT *', 'SELECT COUNT(*)', $this->sql)));
        $statement->execute();

        return (int)$statement->fetchColumn();
    }

    /**
     * Helper method for creating a having sql query.
     * Supports method chaining with other helper methods.
     *
     * @param string $syntax
     * @return $this
     */
    public function having($syntax)
    {
        $this->sql .= " HAVING {$syntax}";

        return $this;
    }

    /**
     * Handler for initializing a database transaction.
     *
     * @return $this
     */
    public function begin()
    {
        $this->db->beginTransaction();

        return $this;
    }

    /**
     * Handler for finalizing a database transaction.
     *
     * @return $this
     */
    public function commit()
    {
        $this->db->commit();

        return $this;
    }

    /**
     * Handler for canceling a database transaction.
     *
     * @return $this
     */
    public function rollback()
    {
        $this->db->rollback();

        return $this;
    }

    /**
     * Adapter for actual saving an sql command to the database.
     * On insert, returns the id of the row that has just been inserted.
     *
     * @return bool
     */
    public function save()
    {
        if ($this->db->query($this->sql)) {
            if ($this->db->lastInsertId() != '0') {
                return $this->db->lastInsertId();
            }

            return true;
        }

        return false;
    }

    /**
     * Adapter for fetching record sets from database according to the constructed sql query.
     * Returns the collection as an instance of the $class parameter. By default it's Database.
     *
     * @param string|null $class
     * @return PDOStatement
     */
    public function fetch($class = null)
    {
        $collection = $this->db->query($this->query ?: $this->sql);
        $collection->setFetchMode(
            PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE,
            $class === null ? 'Database' : $class,
            [$this->table]
        );

        return $collection;
    }

    /**
     * Adapter for returning a single record set from the database, according to the where conditions.
     * Returns an object with the instance of the $class parameter. By default it's Database.
     *
     * @param array|int|string $where
     * @param string|null $class
     * @return mixed
     * @throws Exception
     */
    public function load($where, $class = null)
    {
        $this->sql = "SELECT * FROM {$this->table}";

        if (is_array($where)) {
            $this->sql .= " WHERE " . implode(" AND ", array_map(function ($key, $val) {
                return strtoupper($val) == "'NULL'" ? sprintf("%s IS NULL", $key) : sprintf("%s = %s", $key, $val);
            }, $this->escape(array_keys($where)), $this->quote(array_values($where))));
        } elseif (is_numeric($where)) {
            $this->sql .= " WHERE id = {$this->quote((int)$where)}";
        } else {
            $this->sql .= " WHERE {$where}";
        }

        $this->sql .= " LIMIT 1";

        $collection = $this->db->query($this->sql);
        $collection->setFetchMode(
            PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE,
            $class !== null ? $class : 'Database',
            [$this->table]
        );

        return Arr::first($collection->fetchAll());
    }

    /**
     * Count the columns of a given table.
     *
     * @return $this
     */
    public function columnCount()
    {
        $this->db->columnCount();

        return $this;
    }

    /**
     * Check if a given column exists in the given database table.
     *
     * @param string $table
     * @param string $column
     * @return bool
     */
    public function columnExists($table, $column)
    {
        $columns        = $this->db->query("SELECT * FROM {$table} LIMIT 0");
        $fieldsArray    = [];

        for ($i = 0; $i < $columns->columnCount(); $i++) {
            $fieldsArray[] = $columns->getColumnMeta($i)['name'];
        }

        if (in_array($column, $fieldsArray)) {
            return true;
        }

        return false;
    }

    /**
     * Check if a given table exists.
     *
     * @param string $table
     * @return bool
     */
    public function tableExists($table)
    {
        try {
            return $this->db->query("SELECT 1 FROM {$table} LIMIT 1") !== false;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Helper method for constructing a create table sql query.
     * This method also executes the constructed sql query.
     *
     * @param string $table
     * @param array $params
     * @throws PDOException
     */
    public function createTable($table, $params = [])
    {
        try {
            $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            $count      = 1;
            $_count     = 1;
            $_params    = [];
            $sql        = "CREATE TABLE {$table}(";

            foreach ($params as $field => $options) {
                $sql .= $field;

                if ($options['type']) {
                    if ($options['type'] == 'int') {
                        $sql .= " " . strtoupper($options['type']) . '(11)';
                    } elseif ($options['type'] == 'varchar') {
                        $sql .= " " . strtoupper($options['type']) . '(255)';
                    } elseif ($options['type'] == 'tinyint') {
                        $sql .= " " . strtoupper($options['type']) . '(1)';
                    } else {
                        $sql .= " " . strtoupper($options['type']);
                    }

                }

                if ($options['unsigned'] === true) {
                    $sql .= " UNSIGNED";
                }

                if ($options['null']) {
                    $sql .= " NULL";
                } else {
                    $sql .= " NOT NULL";
                }

                if ($options['default'] == 'null') {
                    $sql .= " DEFAULT " . strtoupper($options['default']);
                } elseif ($options['default']) {
                    $sql .= " DEFAULT '" . strtoupper($options['default']) . "'";
                }

                if ($options['auto_increment'] === true) {
                    $sql .= " AUTO_INCREMENT";
                }

                if ($options['primary'] === true) {
                    $sql .= " PRIMARY KEY";
                }

                if ($foreign = $options['foreign']) {
                    $hasForeignKey = true;
                    $_params[$field] = $options;
                }

                if ($count == count($params)) {
                    if ($hasForeignKey) {
                        $sql .= ", ";

                        foreach ($params as $_field => $_options) {
                            if ($foreign = $_options['foreign']) {
                                $sql .= "INDEX " . $_field . "_idx (" . $_field . ")";
                                $sql .= ", FOREIGN KEY (" . $_field . ")";

                                if ($foreign['table']) {
                                    $sql .= " REFERENCES " . $foreign['table'];
                                }

                                if ($foreign['column']) {
                                    $sql .= " (" . $foreign['column'] . ")";
                                }

                                if ($foreign['delete']) {
                                    $sql .= " ON DELETE " . $foreign['delete'];
                                }

                                if ($foreign['update']) {
                                    $sql .= " ON UPDATE " . $foreign['update'];
                                }

                                if ($_count == count($_params)) {
                                    $sql .= ")";
                                } else {
                                    $sql .= ", ";
                                }

                                $_count++;
                            }
                        }
                    } else {
                        $sql .= ")";
                    }

                } else {
                    $sql .= ", ";
                }

                $count++;
            }

            $sql .= " ENGINE=INNODB;";

            $this->db->exec($sql);

            echo "Created table: <strong>{$table}</strong><br />";
        } catch (PDOException $e) {
            echo $e->getMessage() . '<br /><br />';
        }
    }

    /**
     * Helper method for constructing a drop table sql query.
     * This method also executes the constructed sql query.
     *
     * @param string $table
     */
    public function dropTable($table)
    {
        try {
            $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->db->exec("DROP TABLE {$table}");

            echo "Deleted table: <strong>{$table}</strong><br />";
        } catch (PDOException $e) {
            echo $e->getMessage() . '<br /><br />';
        }
    }

    /**
     * Helper method for constructing an add column sql query.
     * This method also executes the constructed sql query.
     *
     * @param string $table
     * @param string $column
     * @param array $params
     */
    public function addColumn($table, $column, $params)
    {
        try {
            $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            $sql = "ALTER TABLE {$table} ADD COLUMN `{$column}`";

            if ($params['type']) {
                if ($params['type'] == 'int') {
                    $sql .= " " . strtoupper($params['type']) . '(11)';
                } elseif ($params['type'] == 'varchar') {
                    $sql .= " " . strtoupper($params['type']) . '(255)';
                } elseif ($params['type'] == 'tinyint') {
                    $sql .= " " . strtoupper($params['type']) . '(1)';
                } else {
                    $sql .= " " . strtoupper($params['type']);
                }
            }

            if ($params['null']) {
                $sql .= " NULL";
            } else {
                $sql .= " NOT NULL";
            }

            if ($params['default'] == 'null') {
                $sql .= " DEFAULT " . strtoupper($params['default']);
            } elseif ($params['default']) {
                $sql .= " DEFAULT '" . strtoupper($params['default']) . "'";
            }

            if ($params['auto_increment'] === true) {
                $sql .= " AUTO_INCREMENT";
            }

            if ($params['primary'] === true) {
                $sql .= " PRIMARY KEY";
            }

            if ($params['after']) {
                $sql .= " AFTER `{$params['after']}`";
            }

            $this->db->exec($sql);

            echo "Added column: <strong>{$column}</strong> to table <strong>{$table}</strong><br />";
        } catch (PDOException $e) {
            echo $e->getMessage() . '<br />';
        }
    }

    /**
     * Helper method for constructing a drop column sql query.
     * This method also executes the constructed sql query.
     *
     * @param string $table
     * @param string $column
     */
    public function dropColumn($table, $column)
    {
        try {
            $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->db->exec("ALTER TABLE {$table} DROP COLUMN {$column}");

            echo "Deleted column: <strong>{$column}</strong> from table <strong>{$table}</strong><br />";
        } catch (PDOException $e) {
            echo $e->getMessage() . '<br /><br />';
        }
    }
}