<?php

class Setting
{
    /**
     * Manage fetching of a system setting by it's name
     *
     * @param string $method
     * @param array $args
     * @return mixed
     */
    public function __call($method, $args)
    {
        if (String::startsWith($method, 'get')) {
            return self::getValue(strtolower(implode('-', preg_split('/(?=[A-Z])/', substr($method, 3), -1, PREG_SPLIT_NO_EMPTY))));
        }

        return null;
    }

    /**
     * Get the entire setting model instance by it's name
     *
     * @param string $name
     * @return mixed
     */
    public static function get($name)
    {
        return Admin_Model_Setting::getInstance()->load(['name' => $name]);
    }

    /**
     * Get the value of a setting by it's name.
     *
     * @param string $name
     * @return mixed
     */
    public static function getValue($name)
    {
        return Admin_Model_Setting::getInstance()->load(['name' => $name])->getValue();
    }
}