<?php

class Email
{
    /**
     * Email handler for future requests.
     *
     * @var
     */
    private $client;

    /**
     * Initialize the client for handshake.
     * There are two possible types of services that can be provided inside the $service parameter.
     * Postmark or PHPMailer
     *
     * @param string $service
     * @throws Exception
     */
    public function __construct($service)
    {
        if (strtolower($service) == 'postmark') {
            $this->client = new Postmark();
        } elseif (strtolower($service) == 'phpmailer') {
            $this->client = new PHPMail();
        } else {
            throw new Exception('The SERVICE parameter must have the value: POSTMARK or PHPMAILER');
        }
    }

    /**
     * Set the recipients email addresses.
     *
     * @param array|string $emails
     * @param null|string $name
     * @return $this|void
     */
    public function to($emails, $name = null)
    {
        $this->client->to($emails, $name);

        return $this;
    }

    /**
     * Set the sender email address.
     *
     * @param string $email
     * @param string|null $name
     * @return $this|void
     */
    public function from($email, $name = null)
    {
        $this->client->from($email, $name);

        return $this;
    }

    /**
     * Set the reply email address.
     *
     * @param string $email
     * @param string|null $name
     * @return $this|void
     */
    public function reply($email, $name = null)
    {
        $this->client->reply($email, $name);

        return $this;
    }

    /**
     * Set the subject of the email.
     *
     * @param string $subject
     * @return $this|void
     */
    public function subject($subject)
    {
        $this->client->subject($subject);

        return $this;
    }

    /**
     * Set the body of the email.
     *
     * @param string $message
     * @return $this
     */
    public function message($message)
    {
        $this->client->message($message);

        return $this;
    }

    /**
     * Set the body of the email.
     * The $layout parameter must be a valid path to any layout (eg. emails/default.phtml).
     *
     * @param string $layout
     * @param array $vars
     * @return $this
     * @throws Exception
     */
    public function messageHtml($layout, $vars = [])
    {
        if (!File::exists('application/layouts/' . $layout)) {
            throw new Exception('Layout file does not exist: application/layouts/' . $layout);
        }

        if (!File::exists('application/layouts/' . $layout)) {
            throw new Exception('Could not read the layout file: application/layouts/' . $layout);
        }

        foreach ($vars as $key => $val) {
            Bootstrap::getInstance()->$key = $val;
        }

        ob_start();
        include('application/layouts/' . $layout);

        $output = ob_get_clean();

        $this->client->message($output);

        return $this;
    }

    /**
     * Set the carbon copies for the email.
     *
     * @param array|string $emails
     * @param string|null $name
     * @return $this|void
     */
    public function cc($emails, $name = null)
    {
        $this->client->cc($emails, $name);

        return $this;
    }

    /**
     * Set the blind carbon copies for the email.
     *
     * @param array|string $emails
     * @param string|null $name
     * @return $this|void
     */
    public function bcc($emails, $name = null)
    {
        $this->client->bcc($emails, $name);

        return $this;
    }

    /**
     * Set the attachment files that come with the email.
     *
     * @param string $file
     * @param string $name
     * @return $this|void
     */
    public function attachment($file, $name = '')
    {
        $this->client->attachment($file, $name);

        return $this;
    }

    /**
     * Set the email headers.
     *
     * @param string $name
     * @param string $value
     * @return $this|void
     */
    public function headers($name, $value)
    {
        $this->client->headers($name, $value);

        return $this;
    }

    /**
     * Send the email.
     *
     * @return bool
     */
    public function send()
    {
        return $this->client->send();
    }
}