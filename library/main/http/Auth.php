<?php

class Http_Auth
{
    /**
     * The AUTH_USER found in the config.xml file.
     *
     * @var
     */
    private static $username;

    /**
     * The AUTH_PW found in the config.xml file
     *
     * @var
     */
    private static $password;

    /**
     * Check if HTTP AUTH is needed on the current environment.
     * The verification is done according to the config.xml file.
     *
     * @return bool
     */
    public static function required()
    {
        return Config::get('http_auth')->active != 0 ? true : false;
    }

    /**
     * Authorize an end-user to access a protected page.
     *
     * @param callable $function
     */
    public static function authorize($function)
    {
        self::$username = (string)Config::get('http_auth')->username;
        self::$password = (string)Config::get('http_auth')->password;

        if (!Server::exists('PHP_AUTH_USER')) {
            header('WWW-Authenticate: Basic realm="You don\'t have permission to view this page!"');
            header('HTTP/1.0 401 Unauthorized');

            echo 'You are not authorized to access this page!';
            exit;
        } else {
            if (Server::get('PHP_AUTH_USER') == self::$username && Server::get('PHP_AUTH_PW') == self::$password) {
                $function();
            } else {
                header('WWW-Authenticate: Basic realm="You don\'t have permission to view this page!"');
                header('HTTP/1.0 401 Unauthorized');

                echo 'You are not authorized to access this page!';
                exit;
            }
        }
    }
}