<?php

class Controller extends View
{
    /**
     * Get the Database object for further processing.
     *
     * @param string|null $table
     * @return Database
     */
    public function getAdapter($table = null)
    {
        return new Database($table);
    }

    /**
     * Get the Request object for further processing.
     *
     * @return Request
     */
    public function getRequest()
    {
        return new Request();
    }

    /**
     * Get a Bootstrap instance param value.
     *
     * @param string|array|null $name
     * @return mixed
     */
    public function getParam($name)
    {
        return Bootstrap::getInstance()->params[$name];
    }

    /**
     * Get all Bootstrap instance param value.
     *
     * @return array
     */
    public function getAllParams()
    {
        return Bootstrap::getInstance()->params;
    }

    /**
     * Set a Bootstrap instance param value.
     *
     * @param string|array|null $name
     * @param string|array|null $value
     */
    public function setParam($name, $value)
    {
        Bootstrap::getInstance()->params[$name] = $value;
    }

    /**
     * Unset a Bootstrap instance param value.
     *
     * @param string|array|null $name
     */
    public function unsetParam($name)
    {
        unset(Bootstrap::getInstance()->params[$name]);
    }

    /**
     * Redirect the user to the given URL.
     *
     * @param string $url
     * @param array $parameters
     * @return void
     */
    public function redirectUrl($url, $parameters = [])
    {
        Redirect::url($url, $parameters);
    }

    /**
     * Redirect the user to the given page URL.
     * To get the page URL, the $identifier parameter is used.
     *
     * @param string $identifier
     * @param array $parameters
     * @return void
     */
    public function redirectPage($identifier, $parameters = [])
    {
        Redirect::page($identifier, $parameters);
    }

    /**
     * Create a success message.
     * Store it in a session, so it would be available on further browsing.
     *
     * @param string $message
     * @return void
     */
    public function flashSuccess($message)
    {
        Flash::success($message);
    }

    /**
     * Create an error message.
     * Store it in a session, so it would be available on further browsing.
     *
     * @param string $message
     * @return void
     */
    public function flashError($message)
    {
        Flash::error($message);
    }

    /**
     * Handle the pagination of a collection.
     * Automatically assign a paginate variable so it can be displayed in views.
     *
     * @param Model $model
     * @param int|null $limit
     * @return array
     */
    public function paginate(Model $model, $limit = null)
    {
        $pagination = new Pagination();
        $collection = $pagination->initPagination($model, $limit);

        $this->assign('paginate', $pagination->paginate());

        return $collection;
    }
}