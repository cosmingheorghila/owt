<?php

use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookRequest;
use Facebook\FacebookRequestException;
use Facebook\FacebookSession;
use Facebook\GraphUser;

class Facebook
{
    /**
     * Facebook handler for future requests.
     *
     * @var array
     */
    private $session;

    /**
     * Facebook credentials from the config.xml file.
     *
     * @var string
     */
    private static $data;

    /**
     * Initialize the Facebook application.
     *
     * @param string|null $url
     * @throws Exception
     */
    public function __construct($url = null)
    {
        FacebookSession::setDefaultApplication(
            (string)Config::get('facebook')->app_id,
            (string)Config::get('facebook')->app_secret
        );

        $this->initSession($url);
    }

    /**
     * Prepare the conditions for a Facebook handshake.
     * Initialize the session property with data of the logged user.
     *
     * @param string|null $url
     * @throws Exception
     * @return void
     */
    public function initSession($url = null)
    {
        $helper = new FacebookRedirectLoginHelper($url ? $url : Url::getHostUrl() . Url::getHomeUrl());

        if (!Get::exists('code')) {
            Redirect::url($helper->getLoginUrl([
                'scope' => 'public_profile, email, user_about_me, user_birthday, user_hometown, user_likes, user_location, publish_actions'
            ]));
        }

        try {
            $this->session = $helper->getSessionFromRedirect();

            if (!$this->session) {
                Redirect::url($helper->getLoginUrl([
                    'scope' => 'public_profile, email, user_about_me, user_birthday, user_hometown, user_likes, user_location, publish_actions'
                ]));
            }
        } catch(FacebookRequestException $e) {
            if ($e->getCode() == 100) {
                Redirect::url($helper->getLoginUrl());
            } else {
                throw new Exception($e->getCode());
            }
        } catch(Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Return the Facebook user based on the session property already established.
     *
     * @return bool|mixed
     * @throws Exception
     */
    public function getUser()
    {
        if ($this->session) {
            try {
                return (new FacebookRequest(
                    $this->session, 'GET', '/me'
                ))->execute()->getGraphObject(GraphUser::className());
            } catch(FacebookRequestException $e) {
                throw new Exception($e->getCode() . ' --- ' . $e->getMessage());
            }
        }

        return false;
    }

    /**
     * Post a Facebook wall post based on the session property already established.
     *
     * @param string|null $message
     * @param string|null $link
     * @return bool
     * @throws Exception
     */
    public function postMessage($message = null, $link = null)
    {
        if($this->session) {
            try {
                $message = (new FacebookRequest(
                    $this->session, 'POST', '/me/feed', [
                        'link' => $link ?: null,
                        'message' => $message ?: null
                    ]
                ))->execute()->getGraphObject();

                return $message->getProperty('id') ? $message->getProperty('id') : false;
            } catch(FacebookRequestException $e) {
                throw new Exception($e->getCode() . ' --- ' . $e->getMessage());
            }
        }

        return false;
    }

    /**
     * Upload a Facebook photo based on the session property already established.
     *
     * @param string $file
     * @param string|null $message
     * @param int|null $albumId
     * @return bool
     * @throws Exception
     */
    public function uploadPhoto($file, $message = null, $albumId = null)
    {
        if($this->session) {
            try {
                $upload = (new FacebookRequest(
                    $this->session, 'POST', $albumId ? '/me/photos/' . $albumId : '/me/photos', [
                        'source' => new CURLFile($file, 'image/png'),
                        'message' => $message
                    ]
                ))->execute()->getGraphObject();

                return $upload->getProperty('id') ? $upload->getProperty('id') : false;
            } catch(FacebookRequestException $e) {
                throw new Exception($e->getCode() . ' --- ' . $e->getMessage());
            }
        }

        return false;
    }

    /**
     * Initialize a Facebook like button.
     *
     * @param string $url
     * @throws Exception
     */
    public static function likeButton($url)
    {
        self::buttonsInit();

        echo '<div id="fb-root"></div><script>(function(d, s, id) {var js, fjs = d.getElementsByTagName(s)[0];if (d.getElementById(id))return;js = d.createElement(s); js.id = id;js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=' . self::$data->app_id . '";fjs.parentNode.insertBefore(js, fjs);}(document, \'script\', \'facebook-jssdk\'))</script>';
        echo '<div class="fb-like" data-href="' . $url . '" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div>';
    }

    /**
     * Initialize a Facebook share button.
     *
     * @param string $url
     * @throws Exception
     */
    public static function shareButton($url)
    {
        self::buttonsInit();

        echo '<div id="fb-root"></div><script>(function(d, s, id) {var js, fjs = d.getElementsByTagName(s)[0];if (d.getElementById(id))return;js = d.createElement(s); js.id = id;js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=' . self::$data->app_id . '";fjs.parentNode.insertBefore(js, fjs);}(document, \'script\', \'facebook-jssdk\'))</script>';
        echo '<div class="fb-share-button" data-href="' . $url . '" data-type="button_count"></div>';
    }

    /**
     * Initialize a Facebook follow button.
     *
     * @param string $url
     * @throws Exception
     */
    public static function followButton($url)
    {
        self::buttonsInit();

        echo '<div id="fb-root"></div><script>(function(d, s, id) {var js, fjs = d.getElementsByTagName(s)[0];if (d.getElementById(id))return;js = d.createElement(s); js.id = id;js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=' . self::$data->app_id . '";fjs.parentNode.insertBefore(js, fjs);}(document, \'script\', \'facebook-jssdk\'))</script>';
        echo '<div class="fb-follow" data-href="' . $url . '" data-colorscheme="light" data-layout="box_count" data-show-faces="false"></div>';
    }

    /**
     * Initialize a Facebook like box.
     *
     * @param string $url
     * @throws Exception
     */
    public static function likeBox($url)
    {
        self::buttonsInit();

        echo '<div id="fb-root"></div><script>(function(d, s, id) {var js, fjs = d.getElementsByTagName(s)[0];if (d.getElementById(id))return;js = d.createElement(s); js.id = id;js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=' . self::$data->app_id . '";fjs.parentNode.insertBefore(js, fjs);}(document, \'script\', \'facebook-jssdk\'))</script>';
        echo '<div class="fb-like-box" data-href="' . $url . '" data-colorscheme="light" data-show-faces="true" data-header="false" data-stream="false" data-show-border="false"></div>';
    }

    /**
     * Populate $data property with the Facebook credentials found in the config.xml file.
     *
     * @return void;
     */
    private static function buttonsInit()
    {
        self::$data = Config::get('facebook');
    }
}