<?php

class Postmark
{
    /**
     * Postmark handler for future requests.
     *
     * @var string
     */
    private $postmark;

    /**
     * The sender email address.
     *
     * @var
     */
    public $from;

    /**
     * The recipient email address.
     *
     * @var
     */
    public $to;

    /**
     * The reply email address.
     *
     * @var
     */
    public $reply;

    /**
     * The carbon copies of the email.
     * Represented as an array of emails.
     *
     * @var
     */
    public $cc;

    /**
     * The blind carbon copies of the email.
     * Represented as an array of emails.
     *
     * @var
     */
    public $bcc;

    /**
     * The subject of the email.
     *
     * @var null
     */
    public $subject = null;

    /**
     * The message of the email.
     *
     * @var null
     */
    public $message = null;

    /**
     * The HTML message of the email.
     * Advanced message, containing HTML tags.
     *
     * @var null
     */
    public $htmlMessage = null;

    /**
     * The headers of the email.
     *
     * @var array
     */
    public $headers = [];

    /**
     * The attachments of the email.
     *
     * @var array
     */
    public $attachments = [];

    /**
     * Initialize the Postmark client for handshake.
     *
     * return void
     */
    public function __construct()
    {
        $this->postmark = new \Postmark\PostmarkClient((string)Config::get('postmark')->key);
    }

    /**
     * Set the recipients email addresses.
     *
     * @param array|string $emails
     * @param string|null $name
     * @return $this
     */
    public function to($emails, $name = null)
    {
        $this->to = is_array($emails) ? implode(',', $emails) : $emails;

        return $this;
    }

    /**
     * Set the sender email address.
     *
     * @param string $email
     * @param string|null $name
     * @return $this
     */
    public function from($email, $name = null)
    {
        $this->from = $email;

        return $this;
    }

    /**
     * Set the reply email address.
     *
     * @param string $email
     * @param string|null $name
     * @return $this
     */
    public function reply($email, $name = null)
    {
        $this->reply = $email;

        return $this;
    }

    /**
     * Set the subject of the email.
     *
     * @param string $subject
     * @return $this
     */
    public function subject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Set the body of the email.
     *
     * @param string $message
     * @return $this
     * @throws Exception
     */
    public function message($message)
    {
        preg_match("/<[^<]+>/", $message, $m) != 0 ? $this->htmlMessage = $message : $this->message = $message;

        return $this;
    }

    /**
     * Set the carbon copies for the email.
     *
     * @param array|string $emails
     * @param string|null $name
     * @return $this
     */
    public function cc($emails, $name = null)
    {
        $this->cc = is_array($emails) ? implode(',', $emails) : $emails;

        return $this;
    }

    /**
     * Set the blind carbon copies for the email.
     *
     * @param array|string $emails
     * @param string|null $name
     * @return $this
     */
    public function bcc($emails, $name)
    {
        $this->bcc = is_array($emails) ? implode(',', $emails) : $emails;

        return $this;
    }

    /**
     * Set the attachment files that come with the email.
     *
     * @param string $file
     * @param string $name
     * @return $this
     */
    public function attachment($file, $name = '')
    {
        $this->attachments[] = \Postmark\Models\PostmarkAttachment::fromFile($file, end(explode('/', $file)));

        return $this;
    }

    /**
     * Set the email headers.
     *
     * @param string $name
     * @param string $value
     * @return $this
     */
    public function headers($name, $value)
    {
        $this->headers[$name] = $value;

        return $this;
    }

    /**
     * Send the email.
     *
     * @return bool
     */
    public function send()
    {
        return $this->postmark->sendEmail(
            $this->from, $this->to, $this->subject, $this->htmlMessage, $this->message, null, true, $this->reply, $this->cc, $this->bcc, $this->headers, $this->attachments
        ) ? true : false;
    }
}