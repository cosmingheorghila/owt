<?php

class PHPMail extends PHPMailer
{
    /**
     * Set the recipients email addresses.
     *
     * @param array|string $emails
     * @param string|null $name
     */
    public function to($emails, $name = null)
    {
        if (is_array($emails)) {
            foreach ($emails as $email => $name) {
                parent::addAddress($email, $name);
            }
        } else {
            parent::addAddress($emails, $name);
        }
    }

    /**
     * Set the sender email address.
     *
     * @param string $email
     * @param string|null $name
     */
    public function from($email, $name = null)
    {
        parent::setFrom($email, $name);
    }

    /**
     * Set the reply email address.
     *
     * @param string $email
     * @param string|null $name
     */
    public function reply($email, $name = null)
    {
        parent::addReplyTo($email, $name);
    }

    /**
     * Set the subject of the email.
     *
     * @param string $subject
     */
    public function subject($subject)
    {
        $this->Subject = $subject;
    }

    /**
     * Set the body of the email.
     *
     * @param string $message
     */
    public function message($message)
    {
        parent::msgHTML($message);
    }

    /**
     * Set the carbon copies for the email.
     *
     * @param array|string $emails
     * @param string $name
     */
    public function cc($emails, $name = null)
    {
        if (is_array($emails)) {
            foreach ($emails as $email => $name) {
                parent::AddCC($email, $name);
            }
        } else {
            parent::AddCC($emails, $name);
        }
    }

    /**
     * Set the blind carbon copies for the email.
     *
     * @param array|string $emails
     * @param string $name
     */
    public function bcc($emails, $name)
    {
        if (is_array($emails)) {
            foreach ($emails as $email => $name) {
                parent::AddBCC($email, $name);
            }
        } else {
            parent::AddBCC($emails, $name);
        }
    }

    /**
     * Set the attachment files that come with the email.
     *
     * @param string $file
     * @param string $name
     */
    public function attachment($file, $name = '')
    {
        parent::AddAttachment($file, $name);
    }

    /**
     * Set the email headers.
     *
     * @param string $name
     * @param string $value
     */
    public function headers($name, $value)
    {
        parent::addCustomHeader($name, $value);
    }

    /**
     * Send the email.
     *
     * @return bool
     * @throws Exception
     */
    public function send()
    {
        return parent::send() ? true : false;
    }
}