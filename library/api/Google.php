<?php

class Google
{
    /**
     * Google handler for future requests.
     *
     * @var string
     */
    private $client;

    /**
     * Google credentials from the config.xml file.
     *
     * @var mixed
     */
    private $data;

    /**
     * Initialize the Google client for handshake.
     * Populate $data property with the Google credentials found in the config.xml file.
     *
     * return void
     */
    public function __construct()
    {
        $this->client   = new Google_Client();
        $this->data     = Config::get('google');
    }

    /**
     * Return the Google Analytics code.
     *
     * @return mixed
     */
    public static function getAnalyticsCode()
    {
        return Admin_Model_Analytic::getInstance()->load(1)->getCode();
    }

    /**
     * Prepare Google Analytics information.
     *
     * @return array
     * @throws Exception
     */
    public function analytics()
    {
        $analytics  = [];
        $service    = $this->getServiceAnalytics();

        $results = $service->data_ga->get(
            'ga:' . (string)$this->data->analytics_id, '30daysAgo', 'today',
            'ga:users,ga:newUsers,ga:sessions,ga:pageviews'
        );

        foreach ($results->getRows() as $index => $result) {
            if ($index == 0) {
                $analytics['users']         = $result[0];
                $analytics['newUsers']      = $result[1];
                $analytics['visits']        = $result[2];
                $analytics['totalVisits']   = $result[3];

                break;
            }
        }

        return $analytics;
    }

    /**
     * Automatically generate a Google Map based on latitude and longitude coordinates.
     *
     * @param float $latitude
     * @param float $longitude
     * @param array|null $style
     * @param int $zoom
     * @return string
     * @throws Exception
     */
    public static function map($latitude, $longitude, $style = null, $zoom = 15)
    {
        if (!is_numeric($latitude) || !is_numeric($longitude)) {
            throw new Exception('The LATITUDE and LONGITUDE parameters must be of type numeric');
        }

        $location       = json_decode(file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?latlng=' . $latitude . ',' . $longitude . '&sensor=true'));
        $locationName   = $location->results[0]->formatted_address;

        Bootstrap::getInstance()->_appendScript         .= '<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=' . (string)Config::get('google')->api_key . '&sensor=true&v=3.exp"></script>';
        Bootstrap::getInstance()->_appendScriptInline   .= '
            <script type="text/javascript">
                function initialize() {
                    var mapOptions = {
                        center: new google.maps.LatLng(' . $latitude . ', ' . $longitude . '),
                        zoom: ' . $zoom . '
                    };

                    var map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);

                    var marker = new google.maps.Marker({
                        position: new google.maps.LatLng(' . $latitude . ', ' . $longitude . '),
                        map: map
                    });

                    var info = new google.maps.InfoWindow({
                        content: "' . $locationName . '"
                    });

                    google.maps.event.addListener(marker, \'click\', function() {
                        info.open(map,marker);
                    });
                }

                google.maps.event.addDomListener(window, \'load\', initialize);
            </script>
        ';

        if ($style) {
            if (!is_array($style) || empty($style)) {
                throw new Exception('The STYLE parameter must be a non-empty array');
            }

            if (count($style) != 1) {
                throw new Exception('The STYLE parameter must have one key (style) and one value (style css attributes)');
            }

            $result = '<div id="map-canvas" ' . array_shift(array_keys($style)) . '="' . array_shift(array_values($style)) . '"></div>';
        } else {
            $result = '<div id="map-canvas"></div>';
        }

        return $result;
    }

    /**
     * Initialize the Google Analytics service provider.
     *
     * @return Google_Service_Analytics
     */
    private function getServiceAnalytics()
    {
        $this->client->setApplicationName('Analytics');

        $analytics  = new Google_Service_Analytics($this->client);
        $file       = file_get_contents(Dir::getConfig() . '/google.p12');

        $credentials = new Google_Auth_AssertionCredentials((string)$this->data->service_account_email, [Google_Service_Analytics::ANALYTICS_READONLY], $file);

        $this->client->setAssertionCredentials($credentials);
        $this->client->setClientId((string)$this->data->client_id);

        json_decode(Session::get('access_token'));

        if (Session::exists('access_token') && !json_last_error()) {
            $this->client->setAccessToken(Session::get('google-service-token'));
        }

        if($this->client->getAuth()->isAccessTokenExpired()) {
            $this->client->getAuth()->refreshTokenWithAssertion($credentials);
        }

        Session::set('google-service-token', $this->client->getAccessToken());

        return $analytics;
    }
}