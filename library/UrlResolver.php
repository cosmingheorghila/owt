<?php

class UrlResolver
{
    /**
     * The URL handler.
     *
     * @var
     */
    protected $url;

    /**
     * Populate the $url with the current URI's value.
     *
     * @set url
     */
    public function __construct()
    {
        $this->url = Server::get('REQUEST_URI');
    }

    /**
     * Get the current URI.
     *
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Eliminate the prefix URL from the existing URL value.
     *
     * @return string
     */
    public function stripPrefix()
    {
        if (String::startsWith($this->url, Url::getPrefix())) {
            $this->url = substr($this->url, strlen(Url::getPrefix()));
        }

        return $this;
    }

    /**
     * Eliminate the file extension from the existing URL value.
     *
     * @return mixed
     */
    public function stripExtension()
    {
        if (String::contains($this->url, '.php')) {
            $this->url = str_replace('.php', '', $this->url);
        }

        return $this;
    }

    /**
     * Eliminate any query strings that the current URL might contain.
     *
     * @return mixed
     */
    public function stripQueryString()
    {
        if (String::contains($this->url, '?')) {
            $this->url = array_shift(explode('?', $this->url));
        }

        return $this;
    }

    /**
     * Eliminate any unnecessary backslashes from the beginning and end of the URI.
     *
     * @return string
     */
    public function stripBackSlashes()
    {
        $this->url = trim($this->url, '/');

        return $this;
    }
}