<?php

/**
 * Registers load function as the autoloader.
 */
spl_autoload_register('load');

/**
 * Loads whatever classes necessary according to the PSR0 standard.
 *
 * @param string $class
 * @return void
 */
function load($class)
{
    $parts = explode('/', str_replace('_', '/', $class) . '.php');
    $files = [];

    foreach ($parts as $key => $value) {
        $value != 'Model' ?: $value = 'Models';
        $key + 1 == count($parts) ? $files[] = $value : $files[] =  strtolower($value);
    }

    $file = implode('/', $files);

    //load vendor's autoload for dependencies usage
    if (file_exists(dirname(__DIR__) . '/vendor/autoload.php')) {
        require_once(dirname(__DIR__) . '/vendor/autoload.php');
    }

    //load root library classes
    if (file_exists(__DIR__ . '/' . $file)) {
        require_once(__DIR__ . '/' . $file);
    }

    //load main library classes
    if (file_exists(__DIR__ . '/main/' . $file)) {
        require_once(__DIR__ . '/main/' . $file);
    }

    //load global helper classes
    if (file_exists(__DIR__ . '/globals/' . $file)) {
        require_once(__DIR__ . '/globals/' . $file);
    }

    //load library helper classes
    if (file_exists(__DIR__ . '/helpers/' . $file)) {
        require_once(__DIR__ . '/helpers/' . $file);
    }

    //load library api classes
    if (file_exists(__DIR__ . '/api/' . $file)) {
        require_once(__DIR__ . '/api/' . $file);
    }

    //load specific application classes
    if (file_exists(dirname(__DIR__) . '/application/modules/' . $file)) {
        require_once(dirname(__DIR__) . '/application/modules/' . $file);
    }
}