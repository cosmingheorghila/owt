<?php

class Bootstrap extends Mvc
{
    /**
     * The URL handler for further use.
     *
     * @var
     */
    public $url;

    /**
     * The parameters available in controller.
     *
     * @var array
     */
    public $params = [];

    /**
     * Actions that ignore the init() method
     *
     * @var array
     */
    public $freeActions = [];

    /**
     * The meta placeholder.
     * It contains data about the meta tags.
     *
     * @var array
     */
    public $meta    = [
        'title'         => '',
        'description'   => '',
        'keywords'      => '',
    ];

    /**
     * The current module according to the URL.
     *
     * @var
     */
    protected $_module;

    /**
     * Placeholder containing parts for building the controller file.
     *
     * @var
     */
    protected $_paths;

    /**
     * The current URL.
     *
     * @var
     */
    protected $_url;

    /**
     * The current full URL.
     *
     * @var
     */
    protected $_fullUrl;

    /**
     * The current page URL.
     *
     * @var
     */
    protected $_pageUrl;

    /**
     * The path to the layout file.
     *
     * @var
     */
    protected $_layoutPath;

    /**
     * The path to the controller file.
     *
     * @var
     */
    protected $_controllerPath;

    /**
     * The path to the view file.
     *
     * @var
     */
    protected $_viewPath;

    /**
     * The full path to the controller file.
     *
     * @var
     */
    protected $_fullControllerPath;

    /**
     * The full path to the view file.
     *
     * @var
     */
    protected $_fullViewPath;

    /**
     * The controller file placeholder.
     *
     * @var
     */
    protected $_controllerFile;

    /**
     * The view file placeholder.
     *
     * @var
     */
    protected $_viewFile;

    /**
     * The controller action placeholder.
     *
     * @var
     */
    protected $_controllerAction;

    /**
     * The controller class placeholder.
     *
     * @var
     */
    protected $_controllerClass;

    /**
     * Disable layout identifier.
     * If set to true, the layout part won't be rendered on display.
     *
     * @var bool
     */
    protected $_disableLayout = false;

    /**
     * Disable view identifier.
     * If set to true, the view part won't be rendered on display.
     *
     * @var bool
     */
    protected $_disableView = false;

    /**
     * The prepend stylesheet identifier.
     * Should contain a string. That string will be prepended to the head.
     *
     * @var
     */
    public $_prependStyleSheet;

    /**
     * The prepend script identifier.
     * Should contain a string. That string will be prepended to the head.
     *
     * @var
     */
    public $_prependScript;

    /**
     * The prepend stylesheet inline identifier.
     * Should contain a string. That string will be prepended to the head.
     *
     * @var
     */
    public $_prependStyleSheetInline;

    /**
     * The prepend script inline identifier.
     * Should contain a string. That string will be prepended to the head.
     *
     * @var
     */
    public $_prependScriptInline;

    /**
     * The append stylesheet identifier.
     * Should contain a string. That string will be appended to the footer.
     *
     * @var
     */
    public $_appendStyleSheet;

    /**
     * The append script identifier.
     * Should contain a string. That string will be appended to the footer.
     *
     * @var
     */
    public $_appendScript;

    /**
     * The append stylesheet inline identifier.
     * Should contain a string. That string will be appended to the footer.
     *
     * @var
     */
    public $_appendStyleSheetInline;

    /**
     * The append script inline identifier.
     * Should contain a string. That string will be appended to the footer.
     *
     * @var
     */
    public $_appendScriptInline;

    /**
     * Bootstrap mirror object.
     *
     * @var
     */
    private static $_instance;

    /**
     * Get a fresh new instance of the Bootstrap object.
     *
     * @return Bootstrap
     */
    public static function getInstance()
    {
        if (!isset(self::$_instance)) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    /**
     * Set the module and layout path.
     *
     * @set module
     */
    public function __construct()
    {
        $this->_module      = 'default';
        $this->_layoutPath  = 'application/layouts/default/default.phtml';
    }

    /**
     * Initialize the page request.
     *
     * @return void
     */
    public function execute()
    {
        $url = (new UrlResolver())->stripPrefix()->stripExtension()->stripQueryString()->stripBackSlashes();

        $this->setUrl($url->getUrl())->setModule()->setFiles()->load();
    }

    /**
     * Set all the URIs: url, fullUrl, pageUrl
     *
     * @param string $url
     * @return $this
     */
    public function setUrl($url)
    {
        $this->_url = $this->_fullUrl = $this->_pageUrl = $url;

        return $this;
    }

    /**
     * Set the module based on the first part of the URI.
     *
     * @return $this
     */
    public function setModule()
    {
        foreach (glob(Dir::getModules() . '/*') as $directory) {
            $module = Arr::last(explode('/', $directory));

            if ($module == explode('/', $this->_url)[0]) {
                $this->_url    = substr($this->_url, strlen($module) + 1);
                $this->_module = $module;

                break;
            }
        }

        if ($this->_module == 'admin') {
            $this->_layoutPath = 'application/layouts/admin/default.phtml';
        }

        return $this;
    }

    /**
     * Set all the files.
     *
     * @return $this
     */
    public function setFiles()
    {
        $this->setControllerAction();
        $this->setControllerFile();
        $this->setControllerPath();
        $this->setControllerClass();

        $this->setViewFile();
        $this->setViewPath();

        return $this;
    }

    /**
     * Load the correct controller file and execute the action in it's class.
     * The loading takes into consideration a potential page URL or a URL behavior entity.
     * If no route is matched based on the given URL, then a 404 page is displayed.
     *
     * @return $this
     * @throws Exception
     */
    public function load()
    {
        $this->setFullPaths();

        if (Database::getInstance()->tableExists('cms_urls')) {
            $url = (new Behavior_Url)->getUrl(['url' => '/' . $this->_fullUrl]);
        }

        if (isset($url) && $url) {
            $this->loadUrlController($url);
        } elseif (File::exists($this->_fullControllerPath)) {
            $this->loadController();
        } else {
            Redirect::page('not-found');
        }

        if ($this->_disableLayout) {
            $this->view();
        } else {
            if ($url) {
                $this->loadUrlView($url);
            } elseif (File::exists($this->_fullViewPath)) {
                $this->loadView();
            }
        }

        return $this;
    }

    /**
     * Set the controller and view full paths.
     *
     * @return void
     */
    private function setFullPaths()
    {
        $this->_fullControllerPath  = 'application/modules/' . $this->_module . '/controllers/' . (empty($this->_controllerPath) ? '' : $this->_controllerPath . '/') . $this->_controllerFile;
        $this->_fullViewPath        = 'application/modules/' . $this->_module . '/views/' . (empty($this->_viewPath) ? '' : $this->_viewPath . '/') . $this->_viewFile;
    }

    /**
     * Set the controller path.
     *
     * @return void
     */
    private function setControllerPath()
    {
        $this->_controllerPath = implode('/', $this->_paths);
    }

    /**
     * Set the view path.
     *
     * @return void
     */
    private function setViewPath()
    {
        $this->_viewPath = implode('/', $this->_paths) . '/' . strtolower(preg_replace('/(\p{Ll})(\p{Lu})/u', '\1-\2', Arr::first(explode('Controller.php', $this->_controllerFile))));
    }

    /**
     * Set the controller file.
     *
     * @return void
     */
    private function setControllerFile()
    {
        if (empty($this->_url)) {
            $this->_controllerFile = 'IndexController.php';
        } else {
            $this->_controllerFile = ucwords(array_pop($this->_paths)) . 'Controller.php';

            if (String::contains($this->_controllerFile, '-') !== false) {
                $controllerFile         = $this->_controllerFile;
                $this->_controllerFile  = '';

                foreach (explode('-', $controllerFile) as $val) {
                    $this->_controllerFile .= ucwords($val);
                }
            }
        }
    }

    /**
     * Set the view file.
     *
     * @return void
     */
    private function setViewFile()
    {
        $this->_viewFile = strtolower(preg_replace('/(\p{Ll})(\p{Lu})/u', '\1_\2', Arr::first(explode('Action', $this->_controllerAction)))) . '.phtml';
    }

    /**
     * Set the controller class.
     *
     * @return void
     */
    private function setControllerClass()
    {
        $this->_controllerClass = ucwords($this->_module) . '_';

        if (String::contains($this->_controllerClass, '-')) {
            $this->_controllerClass = '';

            foreach (explode('-', $this->_controllerClass) as $val) {
                $this->_controllerClass .= ucwords($val);
            }
        }

        $class = [];

        foreach (explode('/', $this->_controllerPath) as $key => $val) {
            $class[$key] = ucwords($val);

            if (String::contains($val, '-')) {
                $val = '';

                foreach (explode('-', $val) as $v) {
                    $val .= ucwords($v);
                }

                $class[$key] = $val;
            }
        }

        $class = implode('_', $class);

        $this->_controllerClass .= $class . (empty($class) ? '' : '_') . array_shift(explode('.', $this->_controllerFile));
    }

    /**
     * Set the controller action.
     *
     * @return void
     */
    private function setControllerAction()
    {
        $this->_paths = explode('/', $this->_url);

        if (empty($this->_url) || count($this->_paths) == 1) {
            $this->_controllerAction = 'indexAction';
        } elseif (count($this->_paths) > 1) {
            $this->_controllerAction = array_pop($this->_paths) . 'Action';

            if (String::contains($this->_controllerAction, '-') !== false) {
                $controllerAction           = $this->_controllerAction;
                $this->_controllerAction    = '';

                foreach (explode('-', $controllerAction) as $key => $val) {
                    $this->_controllerAction .= $key == 0 ? $val : ucwords($val);
                }
            }
        }
    }

    /**
     * Load the correct controller class and execute it's action.
     *
     * @throws Exception
     */
    private function loadController()
    {
        if (!File::exists($this->_fullControllerPath)) {
            throw new Exception('CONTROLLER file not found: ' . $this->_fullControllerPath);
        }

        $this->render($this->_fullControllerPath);
        $class = new $this->_controllerClass();

        if (!method_exists($class, $this->_controllerAction)) {
            throw new Exception('Undefined METHOD ' . $this->_controllerAction . ' in CONTROLLER ' . $this->_fullControllerPath);
        }

        if (method_exists($class, 'init') && !in_array($this->_controllerAction, $class->freeActions)) {
            call_user_func([$class, 'init']);
        }

        call_user_func([$class, $this->_controllerAction]);
    }

    /**
     * Load the correct url controller class and execute its action.
     *
     * @param Model|Database $url
     * @throws Exception
     */
    private function loadUrlController($url)
    {
        $file = 'application/modules/cms/url/' . str_replace(' ', '', ucwords(implode(' ', explode('_', $url->getEntity())))) . '.php';

        if (!File::exists($file)) {
            throw new Exception('URL VENDOR file not found: ' . $file);
        }

        $this->render($file);

        $class  = 'Cms_Url_' . str_replace(' ', '', ucwords(str_replace('_', ' ', $url->getEntity())));
        $object = new $class();
        $params = $object->resolve($url);

        $mvc = $this->mvc([
            'module'        => $params['module'],
            'controller'    => $params['controller'],
            'action'        => $params['action'],
        ]);

        $this->params['id'] = (int)$params['id'];

        $this->render($mvc['fullControllerPath']);

        $class = new $mvc['controllerClass']();

        if (!method_exists($class, $mvc['controllerAction'])) {
            throw new Exception('Undefined METHOD ' . $mvc['controllerAction'] . ' in CONTROLLER ' . $mvc['controllerClass']);
        }

        if (method_exists($class, 'init')) {
            $class->init();
        }

        $class->{$mvc['controllerAction']}();
    }

    /**
     * Load the view.
     *
     * @throws Exception
     * @return void
     */
    private function loadView()
    {
        if (!File::exists($this->_layoutPath)) {
            throw new Exception('VIEW file not found: ' . $this->_layoutPath);
        }

        $this->render($this->_layoutPath);
    }

    /**
     * Load the URL view.
     *
     * @param Model|Database $url
     * @throws Exception
     */
    private function loadUrlView($url)
    {
        $view = 'application/modules/cms/url/' . str_replace(' ', '', ucwords(str_replace('_', ' ', $url->getEntity()))) . '.php';

        if (!File::exists($view)) {
            throw new Exception('VIEW file not found: ' . $view);
        }

        $this->render($view);

        $class  = 'Cms_Url_' . str_replace(' ', '', ucwords(str_replace('_', '', $url->getEntity())));
        $object = new $class();
        $params = $object->resolve($url);

        if ($url->getEntity() == 'cms_pages' && $page = Cms_Model_Page::getInstance()->load($params['id'])->hasForwarding()) {
            $mvc = $this->mvc([
                'module'        => $page->getModule(),
                'controller'    => $page->getController(),
                'action'        => $page->getAction(),
            ]);
        } else {
            $mvc = $this->mvc([
                'module'        => $params['module'],
                'controller'    => $params['controller'],
                'action'        => $params['action'],
            ]);
        }

        $this->_fullViewPath = $mvc['fullViewPath'];

        $this->render($this->_layoutPath);
    }
}