<?php

class Model extends Database
{
    /**
     * The database table a child model will use.
     *
     * @var
     */
    protected $table;

    /**
     * The newly saved loaded model inside the afterSave() method.
     *
     * @var
     */
    protected $model;

    /**
     * The loaded model instance item.
     *
     * @var
     */
    protected $item;

    /**
     * The data available for future database operations.
     *
     * @var
     */
    protected $data;

    /**
     * The data available inside the beforeSave() method.
     *
     * @var
     */
    protected $params;

    /**
     * The class of the child model.
     *
     * @var
     */
    protected $class;

    /**
     * The HasMany relation identifier.
     * Used as property of type array in child model classes.
     *
     * @var
     */
    protected $hasMany;

    /**
     * The HasOne relation identifier.
     * Used as property of type array in child model classes.
     *
     * @var
     */
    protected $hasOne;

    /**
     * The BelongsTo relation identifier.
     * Used as property of type array in child model classes.
     *
     * @var
     */
    protected $belongsTo;

    /**
     * The HasAndBelongsToMany relation identifier.
     * Used as property of type array in child model classes.
     *
     * @var
     */
    protected $hasAndBelongsToMany;

    /**
     * The behavior identifier.
     * Used as property of type array in child model classes.
     *
     * @var
     */
    protected $actsAs;

    /**
     * The validation relation identifier.
     * Used as property of type array in child model classes.
     *
     * @var
     */
    protected $validates;

    /**
     * The unique relation identifier.
     * Used as property of type array in child model classes.
     *
     * @var
     */
    protected $unique;

    /**
     * The magic method name.
     *
     * @var
     */
    protected $method;

    /**
     * The magic method arguments.
     *
     * @var
     */
    protected $arguments;

    /**
     * Manage the relation fetching methods.
     * Manage the real fetching of the URL.
     * Manage the $data fetching attributes.
     *
     * @param string $method
     * @param array|null $args
     * @return bool|mixed|PDOStatement
     * @throws Exception
     */
    public function __call($method, $args = null)
    {
        $this->method       = strtolower(preg_replace('/(\p{Ll})(\p{Lu})/u', '\1_\2', end(explode('get', $method))));
        $this->arguments    = $args !== null ? $args[0] : null;

        if (is_array($this->hasMany) && !empty($this->hasMany) && in_array($this->hasMany[$this->method], $this->hasMany)) {
            return $this->hasMany($method);
        } elseif (is_array($this->hasOne) && !empty($this->hasOne) && in_array($this->hasOne[$this->method], $this->hasOne)) {
            return $this->hasOne($method);
        } elseif (is_array($this->belongsTo) && !empty($this->belongsTo) && in_array($this->belongsTo[$this->method], $this->belongsTo)) {
            return $this->belongsTo($method);
        } elseif (is_array($this->hasAndBelongsToMany) && !empty($this->hasAndBelongsToMany) && in_array($this->hasAndBelongsToMany[$this->method], $this->hasAndBelongsToMany)) {
            return $this->hasAndBelongsToMany($method);
        } elseif ($method == 'getUrl') {
            return Url::getPrefix() . Database::getInstance('cms_urls')->load(['entity_id' => $this->getId(), 'entity' => $this->getTable()])->getUrl();
        } elseif (String::startsWith($method, 'get')) {
            return $this->getData($this->method);
        } else {
            throw new Exception('MAGIC METHOD unknown. Try get{Field} or check the model\'s RELATIONS');
        }
    }

    /**
     * @return null|string|void
     */
    public function __toString()
    {
        return $this->isLoaded() ? $this->getId() : $this->getTable();
    }

    /**
     * Return a new Model instance.
     *
     * @return Model
     */
    public static function getInstance()
    {
        return new static();
    }

    /**
     * Establish a Database connection with the Model.
     * Assume that the model is used for fetching results.
     *
     * @set Database
     */
    public function __construct()
    {
        parent::__construct($this->table);

        return $this->select();
    }

    /**
     * Get the table of a model.
     *
     * @return null|string|void
     */
    public function getTable()
    {
        return $this->table;
    }

    /**
     * Get the class of a model.
     *
     * @return mixed
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * Get the loaded item of a model, containing valid data.
     *
     * @return mixed
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * Get a specified relation of a model.
     *
     * @param string $relation
     * @return mixed
     */
    public function getRelation($relation)
    {
        return $this->{$relation};
    }

    /**
     * Get the validators belonging to a model.
     *
     * @return mixed
     */
    public function getValidator()
    {
        return $this->validates;
    }

    /**
     * Get the behaviors assigned to a model.
     *
     * @param string|null $behavior
     * @return mixed
     */
    public function getBehaviors($behavior = null)
    {
        return $behavior ? $this->actsAs[$behavior] : $this->actsAs;
    }

    /**
     * Get the data returned by the model.
     *
     * @param string|null $key
     * @return mixed
     */
    public function getData($key = null)
    {
        return $key ? $this->data[$key] : $this->data;
    }

    /**
     * Set the data belonging to a model.
     *
     * @param string|null $key
     * @param string|array|int|null $value
     */
    public function setData($key = null, $value = null)
    {
        $key ? $this->data[$key] = $value : $this->data = $value;
    }

    /**
     * Check if a model has a loaded database instance inside
     *
     * @return bool
     */
    public function isLoaded()
    {
        return $this->getData() ? true : false;
    }

    /**
     * Get a single row from the Database according to the specified where conditions.
     * Return a loaded model instance.
     *
     * @param array|string $where
     * @return mixed
     */
    public function load($where)
    {
        $item       = parent::load($where, $this->class);
        $properties = function($object) {
            return call_user_func('get_object_vars', $object);
        };

        if ($item) {
            $item->setData(null, $properties($item));

            foreach ($properties($item) as $property => $value) {
                unset($item->{$property});
            }

            return $item;
        }

        return null;
    }

    /**
     * Get all Database row sets according to the specified query.
     * Parse the records and add them to the Collection object.
     *
     * @return array
     */
    public function getCollection()
    {
        $collection = new Collection();

        foreach (parent::fetch($this->class) as $item) {
            $properties = function($object) {
                return call_user_func('get_object_vars', $object);
            };

            $item->setData(null, $properties($item));

            foreach ($properties($item) as $property => $value) {
                unset($item->{$property});
            }

            $collection->add($item);
        }

        return $collection;
    }

    /**
     * Create or edit a model instance automatically.
     * All dependencies, validations and behaviors included.
     *
     * @param string $operation
     * @param array $data
     * @param array|string|null $where
     * @return bool
     */
    public function merge($operation, $data, $where = null)
    {
        $request    = new Request($this->table);
        $unique     = null;

        if (is_array($this->validates['unique']) && !empty($this->validates['unique'])) {
            $unique = [];

            foreach ($this->validates['unique'] as $val) {
                $unique[$val] = $data[$val];
            }
        }

        $this->beforeSave();

        $this->params = Arr::merge($data, $this->params);

        $id = $request->post($operation, $this->params, $where, $this->validates['validation'], $unique, $this);

        $this->afterSave($id);

        return $id ?: false;
    }

    /**
     * Method to be executed right before the model is saved to the database.
     *
     * @return void
     */
    protected function beforeSave()
    {
        if (Post::exists('data')) {
            $this->params = Post::get('data');
        }

        if ($this->getItem()) {
            $this->model = $this->getItem();
        }
    }

    /**
     * Method to be executed right after the model is saved to the database.
     * The $id parameter represents the ID from the database of the newly saved record.
     *
     * @param int|null $id
     * @return null
     */
    protected function afterSave($id = null)
    {
        if ($id && is_numeric($id)) {
            $this->model = $this->load($id);
        }
    }

    /**
     * Manage the HasMany relations between models.
     * This relation should be used on a one-to-many model logic.
     * This relation can receive an where clause as the parameter.
     * Also, on relation definition, a limit and order can be supplied.
     *
     * @param string $method
     * @return mixed
     * @throws Exception
     */
    protected function hasMany($method)
    {
        $relation = [];

        foreach ($this->hasMany as $rel => $params) {
            if (!$params['model'] || !$params['remote_key']) {
                throw new Exception('Each HAS_MANY relation must have an array containing MODEL and REMOTE_KEY');
            }

            if ('get' . str_replace(' ', '', ucwords(str_replace('_', ' ', $rel))) == $method) {
                $relation[$rel] = $this->hasMany[$rel];
            }
        }

        if (($identifier = $this->method) && !empty($relation)) {
            $model = new $relation[$identifier]['model']();
            $where = $this->arguments;

            $model->where([
                $relation[$identifier]['remote_key'] => $this->{'get' . str_replace(' ', '', ucwords(str_replace('_', ' ', isset($relation[$identifier]['local_key']) ? $relation[$identifier]['local_key'] : 'id')))}()
            ]);

            if ($where) {
                $model->where($where);
            }

            if (isset($relation[$identifier]['order_field'])) {
                $model->order($relation[$identifier]['order_field'], $relation[$identifier]['order_dir'] ?: 'asc');
            }

            if (isset($relation[$identifier]['limit'])) {
                $model->limit($relation[$identifier]['limit']);
            }

            return $model->getCollection();
        }

        return null;
    }

    /**
     * Manage the HasOne relations between models.
     * This relation should be used on one-to-one model logic.
     *
     * @param string $method
     * @return mixed
     * @throws Exception
     */
    protected function hasOne($method)
    {
        $relation = [];

        foreach ($this->hasOne as $rel => $params) {
            if (!$params['model'] || !$params['remote_key']) {
                throw new Exception('Each BELONGS_TO relation must have an array containing MODEL and REMOTE_KEY');
            }

            if ('get' . str_replace(' ', '', ucwords(str_replace('_', ' ', $rel))) == $method) {
                $relation[$rel] = $this->hasOne[$rel];
            }

        }

        if (($identifier = $this->method) && !empty($relation)) {
            $model = new $relation[$identifier]['model']();

            return $model->load([
                $relation[$identifier]['remote_key'] => $this->{'get' . str_replace(' ', '', ucwords(str_replace('_', ' ', isset($relation[$identifier]['local_key']) ? $relation[$identifier]['local_key'] : 'id')))}()
            ]);
        }

        return null;
    }

    /**
     * Manage the BelongsTo relations between models
     * This relations should be used on a many-to-one model logic.
     *
     * @param string $method
     * @return mixed
     * @throws Exception
     */
    protected function belongsTo($method)
    {
        $relation = [];

        foreach ($this->belongsTo as $rel => $params) {
            if (!$params['model'] || !$params['remote_key']) {
                throw new Exception('Each BELONGS_TO relation must have an array containing MODEL and REMOTE_KEY');
            }

            if ('get' . str_replace(' ', '', ucwords(str_replace('_', ' ', $rel))) == $method) {
                $relation[$rel] = $this->belongsTo[$rel];
            }

        }

        if (($identifier = $this->method) && !empty($relation)) {
            $model = new $relation[$identifier]['model']();

            return $model->load([
                isset($relation[$identifier]['local_key']) ? $relation[$identifier]['local_key'] : 'id' => $this->{'get' . str_replace(' ', '', ucwords(str_replace('_', ' ', $relation[$identifier]['remote_key'])))}()
            ]);
        }

        return null;
    }

    /**
     * Manage the HasAndBelongsToMany relations between models.
     * This relation can receive an where clause as the parameter.
     * Also, on relation definition, a limit and order can be supplied.
     * For updating the Database record sets responsible for this relation, an enable_rel parameter must be specified when instantiating.
     *
     * @param string $method
     * @return mixed
     * @throws Exception
     */
    protected function hasAndBelongsToMany($method)
    {
        $relation = [];

        foreach ($this->hasAndBelongsToMany as $rel => $params) {
            if (!$params['model'] || !$params['table'] || !$params['local_key'] || !$params['remote_key']) {
                throw new Exception('Each HABTM relation must have an array containing MODEL, TABLE, LOCAL_KEY and REMOTE_KEY');
            }

            if ('get' . str_replace(' ', '', ucwords(str_replace('_', ' ', $rel))) == $method) {
                $relation[$rel] = $this->hasAndBelongsToMany[$rel];
            }
        }

        if (($identifier = $this->method) && !empty($relation)) {
            $model  = new $relation[$identifier]['model']();
            $table  = $relation[$identifier]['table'];
            $where  = $this->arguments;

            $model->where("id IN (SELECT {$relation[$identifier]['remote_key']} FROM {$table} WHERE {$relation[$identifier]['local_key']} = '{$this->getId()}')");

            if ($where) {
                $model->where($where);
            }

            if (isset($relation[$identifier]['order_field'])) {
                $model->order($relation[$identifier]['order_field'], $relation[$identifier]['order_dir'] ?: 'asc');
            }

            if (isset($relation[$identifier]['limit'])) {
                $model->limit($relation[$identifier]['limit']);
            }

            return $model->getCollection();
        }

        return null;
    }
}