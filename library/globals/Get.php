<?php

class Get
{
    /**
     * Simply present so the GET method is not interpreted as the constructor.
     *
     * @do nothing
     */
    public function __construct(){}

    /**
     * Helper method to get a specific GET value according to the $key given.
     * For getting multi-level-in GET values, the $key parameter must be a string containing the dot "." character.
     * The dot "." acts as a delimiter, splitting the $key into a multi level array.
     *
     * @param string $key
     * @return mixed
     */
    public static function get($key)
    {
        if (String::contains($key, '.')) {
            return eval(
                'return $_GET["' . implode('"]["', explode('.', $key)) . '"];'
            );
        }

        return $_GET[$key];
    }

    /**
     * Helper method to set a specific GET key and value.
     * For setting multi-level-in GET, the $key parameter must be a string containing the dot "." character.
     * The dot "." acts as a delimiter, splitting the $key into a multi level array.
     *
     * @param string $key
     * @param string $value
     */
    public static function set($key, $value)
    {
        if (String::contains($key, '.')) {
            eval(
                '$_GET["' . implode('"]["', explode('.', $key)) . '"] = ' . $value . ';'
            );
        } else {
            $_GET[$key] = $value;
        }
    }

    /**
     * Helper method for checking if a GET exists according to the key given.
     * For checking multi-level-in GET, the $key parameter must be a string containing the dot "." character.
     * The dot "." acts as a delimiter, splitting the $key into a multi level array.
     *
     * @param string $key
     * @return bool
     */
    public static function exists($key)
    {
        if (String::contains($key, '.')) {
            $get = eval(
                'return $_GET["' . implode('"]["', explode('.', $key)) . '"];'
            );

            return isset($get) && $get != '' ? true : false;
        }

        return isset($_GET[$key]) && $_GET[$key] != '' ? true : false;
    }

    /**
     * Helper method for disabling a GET according to the key given.
     * For checking multi-level-in GET, the $key parameter must be a string containing the dot "." character.
     * The dot "." acts as a delimiter, splitting the $key into a multi level array.
     *
     * @param string $key
     */
    public static function disable($key)
    {
        if (String::contains($key, '.')) {
            $get = eval(
                'return $_GET["' . implode('"]["', explode('.', $key)) . '"];'
            );

            unset($get);
        } else {
            unset($_GET[$key]);
        }
    }
}