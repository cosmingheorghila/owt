<?php

class Post
{
    /**
     * Helper method to get a specific POST value according to the $key given.
     * For getting multi-level-in POST values, the $key parameter must be a string containing the dot "." character.
     * The dot "." acts as a delimiter, splitting the $key into a multi level array.
     *
     * @param string $key
     * @return mixed
     */
    public static function get($key)
    {
        if (String::contains($key, '.')) {
            return eval(
                'return $_POST["' . implode('"]["', explode('.', $key)) . '"];'
            );
        }

        return $_POST[$key];
    }

    /**
     * Helper method to set a specific POST key and value.
     * For setting multi-level-in POST, the $key parameter must be a string containing the dot "." character.
     * The dot "." acts as a delimiter, splitting the $key into a multi level array.
     *
     * @param string $key
     * @param string $value
     */
    public static function set($key, $value)
    {
        if (String::contains($key, '.')) {
            eval(
                '$_POST["' . implode('"]["', explode('.', $key)) . '"] = ' . $value . ';'
            );
        } else {
            $_POST[$key] = $value;
        }
    }

    /**
     * Helper method for checking if a POST exists according to the key given.
     * For checking multi-level-in POST, the $key parameter must be a string containing the dot "." character.
     * The dot "." acts as a delimiter, splitting the $key into a multi level array.
     *
     * @param string $key
     * @return bool
     */
    public static function exists($key)
    {
        if (String::contains($key, '.')) {
            $post = eval(
                'return $_POST["' . implode('"]["', explode('.', $key)) . '"];'
            );

            return isset($post) && $post != '' ? true : false;
        }

        return isset($_POST[$key]) && $_POST[$key] != '' ? true : false;
    }

    /**
     * Helper method for disabling a POST according to the key given.
     * For checking multi-level-in POST, the $key parameter must be a string containing the dot "." character.
     * The dot "." acts as a delimiter, splitting the $key into a multi level array.
     *
     * @param string $key
     */
    public static function disable($key)
    {
        if (String::contains($key, '.')) {
            $post = eval(
                'return $_POST["' . implode('"]["', explode('.', $key)) . '"];'
            );

            unset($post);
        } else {
            unset($_POST[$key]);
        }
    }
}