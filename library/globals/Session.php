<?php

class Session
{
    /**
     * Helper method to get a specific session value according to the $key given.
     * For getting multi-level-in session values, the $key parameter must be a string containing the dot "." character.
     * The dot "." acts as a delimiter, splitting the $key into a multi level array.
     *
     * @param string $key
     * @return mixed
     */
    public static function get($key)
    {
        if (String::contains($key, '.')) {
            return eval(
                'return $_SESSION["' . implode('"]["', explode('.', $key)) . '"];'
            );
        }

        return $_SESSION[$key];
    }

    /**
     * Helper method to set a specific session key and value.
     * For setting multi-level-in sessions, the $key parameter must be a string containing the dot "." character.
     * The dot "." acts as a delimiter, splitting the $key into a multi level array.
     *
     * @param string $key
     * @param string $value
     */
    public static function set($key, $value)
    {
        if (String::contains($key, '.')) {
            eval(
                '$_SESSION["' . implode('"]["', explode('.', $key)) . '"] = ' . $value . ';'
            );
        } else {
            $_SESSION[$key] = $value;
        }
    }

    /**
     * Helper method for checking if a session exists according to the key given.
     * For checking multi-level-in sessions, the $key parameter must be a string containing the dot "." character.
     * The dot "." acts as a delimiter, splitting the $key into a multi level array.
     *
     * @param string $key
     * @return bool
     */
    public static function exists($key)
    {
        if (String::contains($key, '.')) {
            $session = eval(
                'return $_SESSION["' . implode('"]["', explode('.', $key)) . '"];'
            );

            return isset($session) && $session != '' ? true : false;
        }

        return isset($_SESSION[$key]) && $_SESSION[$key] != '' ? true : false;
    }

    /**
     * Helper method for disabling a session according to the key given.
     * For checking multi-level-in sessions, the $key parameter must be a string containing the dot "." character.
     * The dot "." acts as a delimiter, splitting the $key into a multi level array.
     *
     * @param string $key
     */
    public static function disable($key)
    {
        if (String::contains($key, '.')) {
            $session = eval(
                'return $_SESSION["' . implode('"]["', explode('.', $key)) . '"];'
            );

            unset($session);
        } else {
            unset($_SESSION[$key]);
        }
    }
}