<?php

class Server
{
    /**
     * Helper method to get a specific server value according to the $key given.
     * For getting multi-level-in server values, the $key parameter must be a string containing the dot "." character.
     * The dot "." acts as a delimiter, splitting the $key into a multi level array.
     *
     * @param string $key
     * @return mixed
     */
    public static function get($key)
    {
        if (String::contains($key, '.')) {
            return eval(
                'return $_SERVER["' . implode('"]["', explode('.', $key)) . '"];'
            );
        }

        return $_SERVER[$key];
    }

    /**
     * Helper method to set a specific server key and value.
     * For setting multi-level-in servers, the $key parameter must be a string containing the dot "." character.
     * The dot "." acts as a delimiter, splitting the $key into a multi level array.
     *
     * @param string $key
     * @param string $value
     */
    public static function set($key, $value)
    {
        if (String::contains($key, '.')) {
            eval(
                '$_SERVER["' . implode('"]["', explode('.', $key)) . '"] = ' . $value . ';'
            );
        } else {
            $_SERVER[$key] = $value;
        }
    }

    /**
     * Helper method for checking if a server exists according to the key given.
     * For checking multi-level-in servers, the $key parameter must be a string containing the dot "." character.
     * The dot "." acts as a delimiter, splitting the $key into a multi level array.
     *
     * @param string $key
     * @return bool
     */
    public static function exists($key)
    {
        if (String::contains($key, '.')) {
            $server = eval(
                'return $_SERVER["' . implode('"]["', explode('.', $key)) . '"];'
            );

            return isset($server) && $server != '' ? true : false;
        }

        return isset($_SERVER[$key]) && $_SERVER[$key] != '' ? true : false;
    }

    /**
     * Helper method for disabling a server according to the key given.
     * For checking multi-level-in servers, the $key parameter must be a string containing the dot "." character.
     * The dot "." acts as a delimiter, splitting the $key into a multi level array.
     *
     * @param string $key
     */
    public static function disable($key)
    {
        if (String::contains($key, '.')) {
            $server = eval(
                'return $_SERVER["' . implode('"]["', explode('.', $key)) . '"];'
            );

            unset($server);
        } else {
            unset($_SERVER[$key]);
        }
    }
}