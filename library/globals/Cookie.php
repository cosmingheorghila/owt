<?php

class Cookie
{
    /**
     * Helper method to get a specific cookie value according to the $key given.
     * For getting multi-level-in cookie values, the $key parameter must be a string containing the dot "." character.
     * The dot "." acts as a delimiter, splitting the $key into a multi level array.
     *
     * @param string $key
     * @return mixed
     */
    public static function get($key)
    {
        if (String::contains($key, '.')) {
            return eval(
                'return $_COOKIE["' . implode('"]["', explode('.', $key)) . '"];'
            );
        }

        return $_COOKIE[$key];
    }

    /**
     * Helper method to set a specific cookie key and value.
     * For setting multi-level-in cookies, the $key parameter must be a string containing the dot "." character.
     * The dot "." acts as a delimiter, splitting the $key into a multi level array.
     *
     * @param string $key
     * @param string $value
     */
    public static function set($key, $value)
    {
        if (String::contains($key, '.')) {
            eval(
                '$_COOKIE["' . implode('"]["', explode('.', $key)) . '"] = ' . $value . ';'
            );
        } else {
            $_COOKIE[$key] = $value;
        }
    }

    /**
     * Helper method for checking if a cookie exists according to the key given.
     * For checking multi-level-in cookies, the $key parameter must be a string containing the dot "." character.
     * The dot "." acts as a delimiter, splitting the $key into a multi level array.
     *
     * @param string $key
     * @return bool
     */
    public static function exists($key)
    {
        if (String::contains($key, '.')) {
            $cookie = eval(
                'return $_COOKIE["' . implode('"]["', explode('.', $key)) . '"];'
            );

            return isset($cookie) && $cookie != '' ? true : false;
        }

        return isset($_COOKIE[$key]) && $_COOKIE[$key] != '' ? true : false;
    }

    /**
     * Helper method for disabling a cookie according to the key given.
     * For checking multi-level-in cookies, the $key parameter must be a string containing the dot "." character.
     * The dot "." acts as a delimiter, splitting the $key into a multi level array.
     *
     * @param string $key
     */
    public static function disable($key)
    {
        if (String::contains($key, '.')) {
            $cookie = eval(
                'return $_COOKIE["' . implode('"]["', explode('.', $key)) . '"];'
            );

            unset($cookie);
        } else {
            unset($_COOKIE[$key]);
        }
    }
}