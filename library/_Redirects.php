<?php

$url    = Url::getUrl();
$prefix = Url::getPrefix();
$map    = [
    //Url::getPrefix() . '/admin' => Url::getPrefix() . '/admin/users'
];

if ($url == $prefix . '/index.php') {
    header('HTTP/1.1 301 Moved Permanently');
    header('Location: ' . ($prefix ?: '/'));
    die;
}

if (!isset($prefix) || empty($prefix)) {
    $path = $url;
} else {
    $path = explode($prefix, $url);
}

if (substr($url, -1) == '/' && substr($path[1], 0, -1) != '') {
    $map[$url] = rtrim($url, '/');
}

if (strpos($url, '.php') !== false) {
    $_url = str_replace('.php', '', $url);
    $_URL = str_replace($prefix, '', $_url);

    $map[$url] = $prefix . $_URL;
}

if (in_array($url, array_keys($map))) {
    header('HTTP/1.1 301 Moved Permanently');
    header('Location: ' . $map[$url]);
    die;
}