<?php

class Migration_1483953946 extends Database
{
     /**
     * @return void
     */
    public function up()
    {
        $this->createTable('cms_newsletter_subscribers', [
            'id' => ['type' => 'int', 'unsigned' => true, 'auto_increment' => true, 'primary' => true],
            'email' => ['type' => 'varchar'],
            'created_at' => ['type' => 'int'],
            'updated_at' => ['type' => 'int'],
        ]);

        $this->createTable('cms_newsletter_lists', [
            'id' => ['type' => 'int', 'unsigned' => true, 'auto_increment' => true, 'primary' => true],
            'subject' => ['type' => 'varchar'],
            'message' => ['type' => 'longtext'],
            'created_at' => ['type' => 'int'],
            'updated_at' => ['type' => 'int'],
        ]);
    }

    /**
     * @return void
     */
    public function down()
    {
        $this->dropTable('cms_newsletter_lists');
        $this->dropTable('cms_newsletter_subscribers');
    }
}