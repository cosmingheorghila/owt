<?php

class Migration_1405194819 extends Database
{
    /**
     * @return void
     */
    public function up()
    {
        // CMS
        $this->createTable('cms_layouts', [
            'id' => ['type' => 'int', 'unsigned' => true, 'auto_increment' => true, 'primary' => true],
            'identifier' => ['type' => 'varchar'],
            'name' => ['type' => 'varchar'],
            'file' => ['type' => 'varchar'],
            'block_locations' => ['type' => 'longtext'],
        ]);

        $this->createTable('cms_pages', [
            'id' => ['type' => 'int', 'unsigned' => true, 'auto_increment' => true, 'primary' => true],
            'parent_id' => ['type' => 'int', 'unsigned' => true, 'null' => true, 'default' => 'null', 'foreign' => ['table' => 'cms_pages', 'column' => 'id', 'delete' => 'RESTRICT', 'update' => 'CASCADE']],
            'layout_id' => ['type' => 'int', 'unsigned' => true, 'foreign' => ['table' => 'cms_layouts', 'column' => 'id', 'delete' => 'RESTRICT', 'update' => 'CASCADE']],
            'identifier' => ['type' => 'varchar'],
            'name' => ['type' => 'varchar'],
            'h1' => ['type' => 'varchar'],
            'subtitle' => ['type' => 'varchar'],
            'content' => ['type' => 'longtext'],
            'meta_title' => ['type' => 'varchar'],
            'meta_keywords' => ['type' => 'varchar'],
            'meta_description' => ['type' => 'longtext'],
            'visible' => ['type' => 'tinyint'],
            'module' => ['type' => 'varchar'],
            'controller' => ['type' => 'varchar'],
            'action' => ['type' => 'varchar'],
            'parameters' => ['type' => 'varchar'],
            'created_at' => ['type' => 'int'],
            'updated_at' => ['type' => 'int'],
            'ord' => ['type' => 'int'],
        ]);

        $this->createTable('cms_menus_locations', [
            'id' => ['type' => 'int', 'unsigned' => true, 'auto_increment' => true, 'primary' => true],
            'location' => ['type' => 'varchar'],
        ]);

        $this->createTable('cms_menus', [
            'id' => ['type' => 'int', 'unsigned' => true, 'auto_increment' => true, 'primary' => true],
            'parent_id' => ['type' => 'int', 'unsigned' => true, 'null' => true, 'default' => 'null', 'foreign' => ['table' => 'cms_menus', 'column' => 'id', 'delete' => 'RESTRICT', 'update' => 'CASCADE']],
            'location_id' => ['type' => 'int', 'unsigned' => true, 'foreign' => ['table' => 'cms_menus_locations', 'column' => 'id', 'delete' => 'CASCADE', 'update' => 'CASCADE']],
            'entity_id' => ['type' => 'int', 'null' => true, 'default' => 'null'],
            'entity' => ['type' => 'varchar'],
            'name' => ['type' => 'varchar'],
            'url' => ['type' => 'varchar'],
            'new_window' => ['type' => 'tinyint'],
            'ord' => ['type' => 'int'],
        ]);

        $this->createTable('cms_blocks', [
            'id' => ['type' => 'int', 'unsigned' => true, 'auto_increment' => true, 'primary' => true],
            'name' => ['type' => 'varchar'],
            'label' => ['type' => 'varchar'],
            'type' => ['type' => 'varchar'],
            'class' => ['type' => 'varchar'],
            'metadata' => ['type' => 'longtext'],
        ]);

        $this->createTable('cms_blocks_relations', [
            'id' => ['type' => 'int', 'unsigned' => true, 'auto_increment' => true, 'primary' => true],
            'block_id' => ['type' => 'int', 'unsigned' => true, 'foreign' => ['table' => 'cms_blocks', 'column' => 'id', 'delete' => 'CASCADE', 'update' => 'CASCADE']],
            'entity_id' => ['type' => 'int', 'null' => true, 'default' => 'null'],
            'location' => ['type' => 'varchar'],
            'entity' => ['type' => 'varchar'],
            'ord' => ['type' => 'int'],
        ]);

        $this->createTable('cms_urls', [
            'id' => ['type' => 'int', 'unsigned' => true, 'auto_increment' => true, 'primary' => true],
            'entity_id' => ['type' => 'int', 'null' => true, 'default' => 'null'],
            'entity' => ['type' => 'varchar'],
            'url' => ['type' => 'varchar'],
        ]);

        $this->createTable('cms_countries', [
            'id' => ['type' => 'int', 'unsigned' => true, 'auto_increment' => true, 'primary' => true],
            'iso' => ['type' => 'varchar'],
            'name' => ['type' => 'varchar'],
        ]);

        $this->createTable('cms_counties', [
            'id' => ['type' => 'int', 'unsigned' => true, 'auto_increment' => true, 'primary' => true],
            'country_id' => ['type' => 'int', 'unsigned' => true, 'foreign' => ['table' => 'cms_countries', 'column' => 'id', 'delete' => 'CASCADE', 'update' => 'CASCADE']],
            'name' => ['type' => 'varchar'],
            'slug' => ['type' => 'varchar'],
            'delivery_cost' => ['type' => 'float'],
        ]);

        $this->createTable('cms_cities', [
            'id' => ['type' => 'int', 'unsigned' => true, 'auto_increment' => true, 'primary' => true],
            'county_id' => ['type' => 'int', 'unsigned' => true, 'foreign' => ['table' => 'cms_counties', 'column' => 'id', 'delete' => 'CASCADE', 'update' => 'CASCADE']],
            'name' => ['type' => 'varchar'],
            'slug' => ['type' => 'varchar'],
            'valid' => ['type' => 'tinyint'],
        ]);

        // ADMIN
        $this->createTable('admin_users', [
            'id' => ['type' => 'int', 'unsigned' => true, 'auto_increment' => true, 'primary' => true],
            'username' => ['type' => 'varchar'],
            'password' => ['type' => 'varchar'],
            'email' => ['type' => 'varchar'],
            'first_name' => ['type' => 'varchar'],
            'last_name' => ['type' => 'varchar'],
            'access_level' => ['type' => 'longtext'],
            'super_admin' => ['type' => 'tinyint'],
        ]);

        $this->createTable('admin_settings', [
            'id' => ['type' => 'int', 'unsigned' => true, 'auto_increment' => true, 'primary' => true],
            'name' => ['type' => 'varchar'],
            'value' => ['type' => 'varchar'],
        ]);

        $this->createTable('admin_analytics', [
            'id' => ['type' => 'int', 'unsigned' => true, 'auto_increment' => true, 'primary' => true],
            'code' => ['type' => 'longtext'],
        ]);

        // CAR
        $this->createTable('cars_brands', [
            'id' => ['type' => 'int', 'unsigned' => true, 'auto_increment' => true, 'primary' => true],
            'name' => ['type' => 'varchar'],
            'logo' => ['type' => 'varchar'],
            'logo_dark' => ['type' => 'varchar'],
            'real_logo' => ['type' => 'varchar'],
            'ord' => ['type' => 'int'],
        ]);

        $this->createTable('cars_models', [
            'id' => ['type' => 'int', 'unsigned' => true, 'auto_increment' => true, 'primary' => true],
            'brand_id' => ['type' => 'int', 'unsigned' => true, 'foreign' => ['table' => 'cars_brands', 'column' => 'id', 'delete' => 'CASCADE', 'update' => 'CASCADE']],
            'name' => ['type' => 'varchar'],
            'ord' => ['type' => 'int'],
        ]);

        $this->createTable('cars_colors', [
            'id' => ['type' => 'int', 'unsigned' => true, 'auto_increment' => true, 'primary' => true],
            'model_id' => ['type' => 'int', 'unsigned' => true, 'null' => true, 'foreign' => ['table' => 'cars_models', 'column' => 'id', 'delete' => 'SET NULL', 'update' => 'CASCADE']],
            'name' => ['type' => 'varchar'],
            'color' => ['type' => 'varchar'],
            'ord' => ['type' => 'int'],
        ]);

        $this->createTable('cars_stats', [
            'id' => ['type' => 'int', 'unsigned' => true, 'auto_increment' => true, 'primary' => true],
            'name' => ['type' => 'varchar'],
            'important' => ['type' => 'tinyint', 'default' => 0],
            'ord' => ['type' => 'int'],
        ]);

        $this->createTable('cars_ads', [
            'id' => ['type' => 'int', 'unsigned' => true, 'auto_increment' => true, 'primary' => true],
            'brand_id' => ['type' => 'int', 'unsigned' => true, 'foreign' => ['table' => 'cars_brands', 'column' => 'id', 'delete' => 'CASCADE', 'update' => 'CASCADE']],
            'model_id' => ['type' => 'int', 'unsigned' => true, 'foreign' => ['table' => 'cars_models', 'column' => 'id', 'delete' => 'CASCADE', 'update' => 'CASCADE']],
            'color_id' => ['type' => 'int', 'unsigned' => true, 'foreign' => ['table' => 'cars_colors', 'column' => 'id', 'delete' => 'CASCADE', 'update' => 'CASCADE']],
            'city_id' => ['type' => 'int', 'unsigned' => true, 'foreign' => ['table' => 'cms_cities', 'column' => 'id', 'delete' => 'CASCADE', 'update' => 'CASCADE']],
            'identifier' => ['type' => 'varchar'],
            'vin' => ['type' => 'varchar'],
            'kilometers' => ['type' => 'int'],
            'year' => ['type' => 'int'],
            'active' => ['type' => 'tinyint'],
            'converted' => ['type' => 'tinyint'],
            'created_at' => ['type' => 'int'],
            'updated_at' => ['type' => 'int'],
        ]);

        $this->createTable('cars_ads_owners', [
            'id' => ['type' => 'int', 'unsigned' => true, 'auto_increment' => true, 'primary' => true],
            'ad_id' => ['type' => 'int', 'unsigned' => true, 'foreign' => ['table' => 'cars_ads', 'column' => 'id', 'delete' => 'CASCADE', 'update' => 'CASCADE']],
            'first_name' => ['type' => 'varchar'],
            'last_name' => ['type' => 'varchar'],
            'email' => ['type' => 'varchar'],
            'phone' => ['type' => 'varchar'],
        ]);

        $this->createTable('cars_ads_addresses', [
            'id' => ['type' => 'int', 'unsigned' => true, 'auto_increment' => true, 'primary' => true],
            'ad_id' => ['type' => 'int', 'unsigned' => true, 'foreign' => ['table' => 'cars_ads', 'column' => 'id', 'delete' => 'CASCADE', 'update' => 'CASCADE']],
            'address' => ['type' => 'longtext'],
        ]);

        $this->createTable('cars_ads_availabilities', [
            'id' => ['type' => 'int', 'unsigned' => true, 'auto_increment' => true, 'primary' => true],
            'ad_id' => ['type' => 'int', 'unsigned' => true, 'foreign' => ['table' => 'cars_ads', 'column' => 'id', 'delete' => 'CASCADE', 'update' => 'CASCADE']],
            'month' => ['type' => 'varchar'],
            'day' => ['type' => 'varchar'],
            'hour' => ['type' => 'varchar'],
        ]);

        $this->createTable('cars_ads_stats', [
            'id' => ['type' => 'int', 'unsigned' => true, 'auto_increment' => true, 'primary' => true],
            'ad_id' => ['type' => 'int', 'unsigned' => true, 'foreign' => ['table' => 'cars_ads', 'column' => 'id', 'delete' => 'CASCADE', 'update' => 'CASCADE']],
            'name' => ['type' => 'longtext'],
        ]);

        $this->createTable('cars_mechanics', [
            'id' => ['type' => 'int', 'unsigned' => true, 'auto_increment' => true, 'primary' => true],
            'first_name' => ['type' => 'varchar'],
            'last_name' => ['type' => 'varchar'],
            'email' => ['type' => 'varchar'],
            'image' => ['type' => 'varchar'],
            'created_at' => ['type' => 'int'],
            'updated_at' => ['type' => 'int'],
        ]);

        $this->createTable('cars_certifications', [
            'id' => ['type' => 'int', 'unsigned' => true, 'auto_increment' => true, 'primary' => true],
            'name' => ['type' => 'varchar'],
            'image' => ['type' => 'varchar'],
            'ord' => ['type' => 'int'],
            'created_at' => ['type' => 'int'],
            'updated_at' => ['type' => 'int'],
        ]);

        $this->createTable('cars_mechanics_certifications_ring', [
            'id' => ['type' => 'int', 'unsigned' => true, 'auto_increment' => true, 'primary' => true],
            'mechanic_id' => ['type' => 'int', 'unsigned' => true, 'null' => true, 'foreign' => ['table' => 'cars_mechanics', 'column' => 'id', 'delete' => 'CASCADE', 'update' => 'CASCADE']],
            'certification_id' => ['type' => 'int', 'unsigned' => true, 'null' => true, 'foreign' => ['table' => 'cars_certifications', 'column' => 'id', 'delete' => 'CASCADE', 'update' => 'CASCADE']],
        ]);

        $this->createTable('cars', [
            'id' => ['type' => 'int', 'unsigned' => true, 'auto_increment' => true, 'primary' => true],
            'brand_id' => ['type' => 'int', 'unsigned' => true, 'foreign' => ['table' => 'cars_brands', 'column' => 'id', 'delete' => 'CASCADE', 'update' => 'CASCADE']],
            'model_id' => ['type' => 'int', 'unsigned' => true, 'foreign' => ['table' => 'cars_models', 'column' => 'id', 'delete' => 'CASCADE', 'update' => 'CASCADE']],
            'mechanic_id' => ['type' => 'int', 'unsigned' => true, 'null' => true, 'foreign' => ['table' => 'cars_mechanics', 'column' => 'id', 'delete' => 'SET NULL', 'update' => 'CASCADE']],
            'exterior_color_id' => ['type' => 'int', 'unsigned' => true, 'null' => true, 'foreign' => ['table' => 'cars_colors', 'column' => 'id', 'delete' => 'SET NULL', 'update' => 'CASCADE']],
            'interior_color_id' => ['type' => 'int', 'unsigned' => true, 'null' => true, 'foreign' => ['table' => 'cars_colors', 'column' => 'id', 'delete' => 'SET NULL', 'update' => 'CASCADE']],
            'regular_color_id' => ['type' => 'int', 'unsigned' => true, 'null' => true, 'foreign' => ['table' => 'cars_colors', 'column' => 'id', 'delete' => 'SET NULL', 'update' => 'CASCADE']],

            'name' => ['type' => 'varchar'],
            'price' => ['type' => 'float'],
            'pay_now' => ['type' => 'float'],
            'kilometers' => ['type' => 'int'],
            'year' => ['type' => 'int'],
            'vin' => ['type' => 'varchar'],
            'type' => ['type' => 'varchar'],
            'horse_power' => ['type' => 'int'],
            'gears' => ['type' => 'int'],

            'fuel' => ['type' => 'int'],
            'body' => ['type' => 'int'],
            'transmission' => ['type' => 'int'],
            'traction' => ['type' => 'int'],
            'pollution' => ['type' => 'int'],

            'mixt_consumption' => ['type' => 'float'],
            'urban_consumption' => ['type' => 'float'],
            'rural_consumption' => ['type' => 'float'],

            'origin_country' => ['type' => 'varchar'],
            'motorisation' => ['type' => 'float'],
            'door_number' => ['type' => 'int'],
            'cylinder_capacity' => ['type' => 'int'],
            'cylinder_number' => ['type' => 'int'],
            'couple' => ['type' => 'varchar'],
            'turbine_type' => ['type' => 'varchar'],

            'interior_defects' => ['type' => 'varchar'],
            'exterior_defects' => ['type' => 'varchar'],

            'report' => ['type' => 'varchar'],
            'brochure' => ['type' => 'varchar'],

            'v_exterior_plafon' => ['type' => 'tinyint', 'default' => '1'],
            'v_exterior_parbriz' => ['type' => 'tinyint', 'default' => '1'],
            'v_exterior_stergatoare' => ['type' => 'tinyint', 'default' => '1'],
            'v_exterior_capota' => ['type' => 'tinyint', 'default' => '1'],
            'v_exterior_faruri' => ['type' => 'tinyint', 'default' => '1'],
            'v_exterior_bumper_fata' => ['type' => 'tinyint', 'default' => '1'],
            'v_exterior_aripi_fata' => ['type' => 'tinyint', 'default' => '1'],
            'v_exterior_stare_vopsea' => ['type' => 'tinyint', 'default' => '1'],
            'v_exterior_proiectoare' => ['type' => 'tinyint', 'default' => '1'],
            'v_exterior_sigla' => ['type' => 'tinyint', 'default' => '1'],
            'v_exterior_grilaj' => ['type' => 'tinyint', 'default' => '1'],
            'v_exterior_luneta' => ['type' => 'tinyint', 'default' => '1'],
            'v_exterior_stergator_spate' => ['type' => 'tinyint', 'default' => '1'],
            'v_exterior_iluminare_spate' => ['type' => 'tinyint', 'default' => '1'],
            'v_exterior_stopuri' => ['type' => 'tinyint', 'default' => '1'],
            'v_exterior_aliniament' => ['type' => 'tinyint', 'default' => '1'],
            'v_exterior_praguri' => ['type' => 'tinyint', 'default' => '1'],
            'v_interior_cotiere_usi' => ['type' => 'tinyint', 'default' => '1'],
            'v_interior_manere_usi' => ['type' => 'tinyint', 'default' => '1'],
            'v_interior_mocheta' => ['type' => 'tinyint', 'default' => '1'],
            'v_interior_suport_pahare' => ['type' => 'tinyint', 'default' => '1'],
            'v_interior_scaune_centuri' => ['type' => 'tinyint', 'default' => '1'],
            'v_interior_stalpi' => ['type' => 'tinyint', 'default' => '1'],
            'v_interior_interior_portbagaj' => ['type' => 'tinyint', 'default' => '1'],
            'v_interior_scule_portbagaj' => ['type' => 'tinyint', 'default' => '1'],
            'v_interior_consola_principal' => ['type' => 'tinyint', 'default' => '1'],
            'v_interior_compartimente' => ['type' => 'tinyint', 'default' => '1'],
            'v_interior_climatizare' => ['type' => 'tinyint', 'default' => '1'],
            'v_interior_dezaburire' => ['type' => 'tinyint', 'default' => '1'],
            'v_interior_computer_bord' => ['type' => 'tinyint', 'default' => '1'],
            'v_interior_lumini_interioare' => ['type' => 'tinyint', 'default' => '1'],
            'v_interior_geamuri' => ['type' => 'tinyint', 'default' => '1'],
            'v_interior_trapa' => ['type' => 'tinyint', 'default' => '1'],
            'v_interior_oglinzi' => ['type' => 'tinyint', 'default' => '1'],
            'v_interior_claxon' => ['type' => 'tinyint', 'default' => '1'],
            'v_interior_semnalizare' => ['type' => 'tinyint', 'default' => '1'],
            'v_interior_airbag' => ['type' => 'tinyint', 'default' => '1'],
            'v_interior_directie' => ['type' => 'tinyint', 'default' => '1'],
            'v_interior_comenzi_volan' => ['type' => 'tinyint', 'default' => '1'],
            'v_interior_inchidere_centrala' => ['type' => 'tinyint', 'default' => '1'],
            'v_interior_boxe' => ['type' => 'tinyint', 'default' => '1'],
            'v_interior_pedale' => ['type' => 'tinyint', 'default' => '1'],
            'v_interior_frana_mana' => ['type' => 'tinyint', 'default' => '1'],
            'v_interior_schimbator' => ['type' => 'tinyint', 'default' => '1'],
            'v_interior_sistem_audio' => ['type' => 'tinyint', 'default' => '1'],
            'v_interior_navigatie' => ['type' => 'tinyint', 'default' => '1'],
            'v_subcapota_instalatie_racire' => ['type' => 'tinyint', 'default' => '1'],
            'v_subcapota_curele' => ['type' => 'tinyint', 'default' => '1'],
            'v_subcapota_transmisie' => ['type' => 'tinyint', 'default' => '1'],
            'v_subcapota_fluide' => ['type' => 'tinyint', 'default' => '1'],
            'v_subcapota_semne_rugina' => ['type' => 'tinyint', 'default' => '1'],
            'v_subcapota_balamale' => ['type' => 'tinyint', 'default' => '1'],
            'v_subcapota_elemente_fabrica' => ['type' => 'tinyint', 'default' => '1'],
            'v_subcapota_ulei' => ['type' => 'tinyint', 'default' => '1'],
            'v_subcapota_servodirectie' => ['type' => 'tinyint', 'default' => '1'],
            'v_subcapota_ac' => ['type' => 'tinyint', 'default' => '1'],
            'v_subcapota_baterie' => ['type' => 'tinyint', 'default' => '1'],
            'v_subcapota_lichid_frana' => ['type' => 'tinyint', 'default' => '1'],
            'v_roti_cauciucuri' => ['type' => 'tinyint', 'default' => '1'],
            'v_roti_jante' => ['type' => 'tinyint', 'default' => '1'],
            'v_roti_suruburi' => ['type' => 'tinyint', 'default' => '1'],
            'v_roti_roata_rezerva' => ['type' => 'tinyint', 'default' => '1'],
            'v_roti_placute_frana' => ['type' => 'tinyint', 'default' => '1'],
            'v_roti_presiune_cauciucuri' => ['type' => 'tinyint', 'default' => '1'],
            'v_roti_stare_cauciucuri' => ['type' => 'tinyint', 'default' => '1'],
            'v_roti_suspensie' => ['type' => 'tinyint', 'default' => '1'],
            'v_submasina_sasiu' => ['type' => 'tinyint', 'default' => '1'],
            'v_submasina_evacuare' => ['type' => 'tinyint', 'default' => '1'],
            'v_submasina_dieferential' => ['type' => 'tinyint', 'default' => '1'],
            'v_inmers_stare_motor' => ['type' => 'tinyint', 'default' => '1'],
            'v_inmers_stare_suspensii' => ['type' => 'tinyint', 'default' => '1'],
            'v_inmers_mers_stare_cauciucuri' => ['type' => 'tinyint', 'default' => '1'],
            'v_inmers_mers_evacuare' => ['type' => 'tinyint', 'default' => '1'],
            'v_inmers_temperatura_motor' => ['type' => 'tinyint', 'default' => '1'],
            'v_inmers_incalzire' => ['type' => 'tinyint', 'default' => '1'],
            'v_inmers_sunete_vibratii' => ['type' => 'tinyint', 'default' => '1'],

            'meta_title' => ['type' => 'varchar'],
            'meta_description' => ['type' => 'longtext'],
            'meta_keywords' => ['type' => 'longtext'],

            'active' => ['type' => 'tinyint', 'default' => 0],
            'ord' => ['type' => 'int'],
            'created_at' => ['type' => 'int'],
            'updated_at' => ['type' => 'int'],
        ]);

        $this->createTable('cars_orders', [
            'id' => ['type' => 'int', 'unsigned' => true, 'auto_increment' => true, 'primary' => true],
            'car_id' => ['type' => 'int', 'unsigned' => true, 'null' => true, 'foreign' => ['table' => 'cars', 'column' => 'id', 'delete' => 'SET NULL', 'update' => 'CASCADE']],
            'user_id' => ['type' => 'varchar'],
            'identifier' => ['type' => 'varchar'],
            'name' => ['type' => 'varchar'],
            'paid' => ['type' => 'float'],
            'remaining' => ['type' => 'float'],
            'first_name' => ['type' => 'varchar'],
            'last_name' => ['type' => 'varchar'],
            'email' => ['type' => 'varchar'],
            'phone' => ['type' => 'varchar'],
            'county' => ['type' => 'varchar'],
            'city' => ['type' => 'varchar'],
            'street' => ['type' => 'varchar'],
            'street_no' => ['type' => 'varchar'],
            'viewed' => ['type' => 'tinyint'],
            'status' => ['type' => 'tinyint'],
            'payment' => ['type' => 'tinyint'],
            'message' => ['type' => 'longtext'],
            'active' => ['type' => 'tinyint'],
            'created_at' => ['type' => 'int'],
            'updated_at' => ['type' => 'int'],
        ]);

        $this->createTable('cars_images', [
            'id' => ['type' => 'int', 'unsigned' => true, 'auto_increment' => true, 'primary' => true],
            'car_id' => ['type' => 'int', 'unsigned' => true, 'null' => true, 'foreign' => ['table' => 'cars', 'column' => 'id', 'delete' => 'SET NULL', 'update' => 'CASCADE']],
            'image' => ['type' => 'varchar'],
            'first_image' => ['type' => 'tinyint'],
            'third_image' => ['type' => 'tinyint'],
            'second_image' => ['type' => 'tinyint'],
            'ord' => ['type' => 'int'],
            'created_at' => ['type' => 'int'],
            'updated_at' => ['type' => 'int'],
        ]);

        $this->createTable('cars_perks', [
            'id' => ['type' => 'int', 'unsigned' => true, 'auto_increment' => true, 'primary' => true],
            'car_id' => ['type' => 'int', 'unsigned' => true, 'null' => true, 'foreign' => ['table' => 'cars', 'column' => 'id', 'delete' => 'SET NULL', 'update' => 'CASCADE']],
            'image' => ['type' => 'varchar'],
            'name' => ['type' => 'varchar'],
            'content' => ['type' => 'longtext'],
            'ord' => ['type' => 'int'],
            'created_at' => ['type' => 'int'],
            'updated_at' => ['type' => 'int'],
        ]);

        $this->createTable('cars_options', [
            'id' => ['type' => 'int', 'unsigned' => true, 'auto_increment' => true, 'primary' => true],
            'name' => ['type' => 'varchar'],
            'ord' => ['type' => 'int'],
            'created_at' => ['type' => 'int'],
            'updated_at' => ['type' => 'int'],
        ]);

        $this->createTable('cars_options_ring', [
            'id' => ['type' => 'int', 'unsigned' => true, 'auto_increment' => true, 'primary' => true],
            'car_id' => ['type' => 'int', 'unsigned' => true, 'null' => true, 'foreign' => ['table' => 'cars', 'column' => 'id', 'delete' => 'CASCADE', 'update' => 'CASCADE']],
            'option_id' => ['type' => 'int', 'unsigned' => true, 'null' => true, 'foreign' => ['table' => 'cars_options', 'column' => 'id', 'delete' => 'CASCADE', 'update' => 'CASCADE']],
        ]);

        $this->createTable('cars_features', [
            'id' => ['type' => 'int', 'unsigned' => true, 'auto_increment' => true, 'primary' => true],
            'name' => ['type' => 'varchar'],
            'image' => ['type' => 'varchar'],
            'hover_image' => ['type' => 'varchar'],
            'show_in_filters' => ['type' => 'tinyint'],
            'ord' => ['type' => 'int'],
            'created_at' => ['type' => 'int'],
            'updated_at' => ['type' => 'int'],
        ]);

        $this->createTable('cars_features_ring', [
            'id' => ['type' => 'int', 'unsigned' => true, 'auto_increment' => true, 'primary' => true],
            'car_id' => ['type' => 'int', 'unsigned' => true, 'null' => true, 'foreign' => ['table' => 'cars', 'column' => 'id', 'delete' => 'CASCADE', 'update' => 'CASCADE']],
            'feature_id' => ['type' => 'int', 'unsigned' => true, 'null' => true, 'foreign' => ['table' => 'cars_features', 'column' => 'id', 'delete' => 'CASCADE', 'update' => 'CASCADE']],
        ]);

        $this->createTable('cars_followers', [
            'id' => ['type' => 'int', 'unsigned' => true, 'auto_increment' => true, 'primary' => true],
            'car_id' => ['type' => 'int', 'unsigned' => true, 'null' => true, 'foreign' => ['table' => 'cars', 'column' => 'id', 'delete' => 'SET NULL', 'update' => 'CASCADE']],
            'email' => ['type' => 'varchar'],
            'created_at' => ['type' => 'int'],
            'updated_at' => ['type' => 'int'],
        ]);

        // USER
        $this->createTable('users', [
            'id' => ['type' => 'int', 'unsigned' => true, 'auto_increment' => true, 'primary' => true],
            'username' => ['type' => 'varchar'],
            'password' => ['type' => 'varchar'],
            'email' => ['type' => 'varchar'],
            'first_name' => ['type' => 'varchar'],
            'last_name' => ['type' => 'varchar'],
            'phone' => ['type' => 'varchar'],
            'avatar' => ['type' => 'varchar'],
            'address_county' => ['type' => 'varchar'],
            'address_city' => ['type' => 'varchar'],
            'address_street' => ['type' => 'varchar'],
            'address_street_no' => ['type' => 'varchar'],
            'code' => ['type' => 'varchar'],
            'type' => ['type' => 'tinyint', 'default' => 1],
            'status' => ['type' => 'tinyint', 'default' => 0],
            'created_at' => ['type' => 'int'],
            'updated_at' => ['type' => 'int'],
        ]);

        // SHOP
        $this->createTable('shop_currencies', [
            'id' => ['type' => 'int', 'unsigned' => true, 'auto_increment' => true, 'primary' => true],
            'name' => ['type' => 'varchar'],
            'code' => ['type' => 'varchar'],
        ]);

        $this->createTable('shop_types', [
            'id' => ['type' => 'int', 'unsigned' => true, 'auto_increment' => true, 'primary' => true],
            'name' => ['type' => 'varchar'],
            'description' => ['type' => 'longtext'],
            'ord' => ['type' => 'int'],
        ]);

        $this->createTable('shop_categories', [
            'id' => ['type' => 'int', 'unsigned' => true, 'auto_increment' => true, 'primary' => true],
            'type_id' => ['type' => 'int', 'unsigned' => true, 'null' => true, 'foreign' => ['table' => 'shop_types', 'column' => 'id', 'delete' => 'SET NULL', 'update' => 'CASCADE']],
            'name' => ['type' => 'varchar'],
            'description' => ['type' => 'longtext'],
            'show_home' => ['type' => 'tinyint'],
            'meta_title' => ['type' => 'varchar'],
            'meta_description' => ['type' => 'longtext'],
            'meta_keywords' => ['type' => 'longtext'],
            'ord' => ['type' => 'int'],
        ]);

        $this->createTable('shop_products', [
            'id' => ['type' => 'int', 'unsigned' => true, 'auto_increment' => true, 'primary' => true],
            'car_id' => ['type' => 'int', 'unsigned' => true, 'null' => true, 'foreign' => ['table' => 'cars', 'column' => 'id', 'delete' => 'SET NULL', 'update' => 'CASCADE']],
            'currency_id' => ['type' => 'int', 'unsigned' => true, 'null' => true, 'foreign' => ['table' => 'shop_currencies', 'column' => 'id', 'delete' => 'SET NULL', 'update' => 'CASCADE']],
            'sku' => ['type' => 'varchar'],
            'name' => ['type' => 'varchar'],
            'description' => ['type' => 'longtext'],
            'price' => ['type' => 'float'],
            'virtual_price' => ['type' => 'float'],
            'stock' => ['type' => 'tinyint'],
            'virtual_stock' => ['type' => 'tinyint'],
            'quantity' => ['type' => 'int'],
            'virtual_quantity' => ['type' => 'int'],
            'active' => ['type' => 'tinyint'],
            'view_count' => ['type' => 'int'],
            'sell_count' => ['type' => 'int'],
            'meta_title' => ['type' => 'varchar'],
            'meta_description' => ['type' => 'longtext'],
            'meta_keywords' => ['type' => 'longtext'],
            'created_at' => ['type' => 'int'],
            'updated_at' => ['type' => 'int'],
        ]);

        $this->createTable('shop_cart', [
            'id' => ['type' => 'int', 'unsigned' => true, 'auto_increment' => true, 'primary' => true],
            'session_id' => ['type' => 'varchar', 'null' => true],
            'user_id' => ['type' => 'int', 'unsigned' => true, 'null' => true, 'foreign' => ['table' => 'users', 'column' => 'id', 'delete' => 'CASCADE', 'update' => 'CASCADE']],
            'product_id' => ['type' => 'int', 'unsigned' => true, 'null' => true, 'foreign' => ['table' => 'shop_products', 'column' => 'id', 'delete' => 'CASCADE', 'update' => 'CASCADE']],
            'quantity' => ['type' => 'int'],
            'created_at' => ['type' => 'int'],
            'updated_at' => ['type' => 'int'],
        ]);

        $this->createTable('shop_orders', [
            'id' => ['type' => 'int', 'unsigned' => true, 'auto_increment' => true, 'primary' => true],
            'identifier' => ['type' => 'varchar'],
            'user_id' => ['type' => 'int', 'unsigned' => true, 'null' => true, 'default' => 'null', 'foreign' => ['table' => 'users', 'column' => 'id', 'delete' => 'CASCADE', 'update' => 'CASCADE']],
            'currency' => ['type' => 'varchar'],
            'raw_total' => ['type' => 'float'],
            'sub_total' => ['type' => 'float'],
            'grand_total' => ['type' => 'float'],
            'details' => ['type' => 'longtext'],
            'delivery_date' => ['type' => 'int', 'null' => true, 'default' => 'null'],
            'shipping_option' => ['type' => 'tinyint', 'null' => true, 'default' => 'null'],
            'payment_type' => ['type' => 'tinyint', 'null' => true, 'default' => 'null'],
            'status' => ['type' => 'tinyint'],
            'viewed' => ['type' => 'tinyint'],
            'created_at' => ['type' => 'int'],
            'updated_at' => ['type' => 'int'],
        ]);

        $this->createTable('shop_orders_items', [
            'id' => ['type' => 'int', 'unsigned' => true, 'auto_increment' => true, 'primary' => true],
            'order_id' => ['type' => 'int', 'unsigned' => true, 'foreign' => ['table' => 'shop_orders', 'column' => 'id', 'delete' => 'CASCADE', 'update' => 'CASCADE']],
            'product_id' => ['type' => 'int', 'unsigned' => true, 'foreign' => ['table' => 'shop_products', 'column' => 'id', 'delete' => 'CASCADE', 'update' => 'CASCADE']],
            'currency_id' => ['type' => 'int', 'unsigned' => true, 'null' => true, 'foreign' => ['table' => 'shop_currencies', 'column' => 'id', 'delete' => 'SET NULL', 'update' => 'CASCADE']],
            'name' => ['type' => 'varchar'],
            'quantity' => ['type' => 'int'],
            'price' => ['type' => 'float'],
            'total' => ['type' => 'float'],
        ]);

        $this->createTable('shop_attributes_sets', [
            'id' => ['type' => 'int', 'unsigned' => true, 'auto_increment' => true, 'primary' => true],
            'name' => ['type' => 'varchar'],
            'label' => ['type' => 'varchar'],
            'ord' => ['type' => 'int'],
        ]);

        $this->createTable('shop_attributes', [
            'id' => ['type' => 'int', 'unsigned' => true, 'auto_increment' => true, 'primary' => true],
            'set_id' => ['type' => 'int', 'unsigned' => true, 'foreign' => ['table' => 'shop_attributes_sets', 'column' => 'id', 'delete' => 'CASCADE', 'update' => 'CASCADE']],
            'name' => ['type' => 'varchar'],
            'label' => ['type' => 'varchar'],
            'value' => ['type' => 'longtext'],
            'type' => ['type' => 'varchar', 'default' => 'null', 'null' => true],
            'filterable' => ['type' => 'tinyint'],
            'filterable_everywhere' => ['type' => 'tinyint'],
            'ord' => ['type' => 'int'],
            'ord2' => ['type' => 'int'],
        ]);

        $this->createTable('shop_taxes', [
            'id' => ['type' => 'int', 'unsigned' => true, 'auto_increment' => true, 'primary' => true],
            'name' => ['type' => 'varchar'],
            'rate' => ['type' => 'float'],
            'type' => ['type' => 'tinyint'],
            'active' => ['type' => 'tinyint'],
            'applicability' => ['type' => 'tinyint'],
            'start_date' => ['type' => 'int', 'default' => 'null', 'null' => true],
            'end_date' => ['type' => 'int', 'default' => 'null', 'null' => true],
            'maximum_value' => ['type' => 'int', 'default' => 'null', 'null' => true],
        ]);

        $this->createTable('shop_discounts', [
            'id' => ['type' => 'int', 'unsigned' => true, 'auto_increment' => true, 'primary' => true],
            'name' => ['type' => 'varchar'],
            'rate' => ['type' => 'float'],
            'type' => ['type' => 'tinyint'],
            'active' => ['type' => 'tinyint'],
            'applicability' => ['type' => 'tinyint'],
            'start_date' => ['type' => 'int', 'default' => 'null', 'null' => true],
            'end_date' => ['type' => 'int', 'default' => 'null', 'null' => true],
            'minimum_value' => ['type' => 'int', 'default' => 'null', 'null' => true],
        ]);

        $this->createTable('shop_vouchers', [
            'id' => ['type' => 'int', 'unsigned' => true, 'auto_increment' => true, 'primary' => true],
            'name' => ['type' => 'varchar'],
            'code' => ['type' => 'varchar'],
            'rate' => ['type' => 'float'],
            'type' => ['type' => 'tinyint'],
            'active' => ['type' => 'tinyint'],
            'maximum_usage' => ['type' => 'int', 'default' => 'null', 'null' => true],
            'current_usage' => ['type' => 'int', 'default' => '0'],
            'minimum_value' => ['type' => 'int', 'default' => 'null', 'null' => true],
            'start_date' => ['type' => 'int', 'default' => 'null', 'null' => true],
            'end_date' => ['type' => 'int', 'default' => 'null', 'null' => true],
            'created_at' => ['type' => 'int'],
            'updated_at' => ['type' => 'int'],
        ]);

        $this->createTable('shop_vendors', [
            'id' => ['type' => 'int', 'unsigned' => true, 'auto_increment' => true, 'primary' => true],
            'name' => ['type' => 'varchar'],
        ]);

        $this->createTable('shop_customers', [
            'id' => ['type' => 'int', 'unsigned' => true, 'auto_increment' => true, 'primary' => true],
            'order_id' => ['type' => 'int', 'unsigned' => true, 'foreign' => ['table' => 'shop_orders', 'column' => 'id', 'delete' => 'CASCADE', 'update' => 'CASCADE']],
            'user_id' => ['type' => 'int', 'unsigned' => true, 'null' => true, 'default' => 'null', 'foreign' => ['table' => 'users', 'column' => 'id', 'delete' => 'CASCADE', 'update' => 'CASCADE']],
            'first_name' => ['type' => 'varchar'],
            'last_name' => ['type' => 'varchar'],
            'email' => ['type' => 'varchar'],
            'phone' => ['type' => 'varchar'],
            'address_county' => ['type' => 'varchar'],
            'address_city' => ['type' => 'varchar'],
            'address_street' => ['type' => 'varchar'],
            'shipping_street_nr' => ['type' => 'varchar'],
            'created_at' => ['type' => 'int'],
            'updated_at' => ['type' => 'int'],
        ]);

        $this->createTable('shop_wishlist', [
            'id' => ['type' => 'int', 'unsigned' => true, 'auto_increment' => true, 'primary' => true],
            'user_id' => ['type' => 'int', 'unsigned' => true, 'foreign' => ['table' => 'users', 'column' => 'id', 'delete' => 'CASCADE', 'update' => 'CASCADE']],
            'product_id' => ['type' => 'int', 'unsigned' => true, 'foreign' => ['table' => 'shop_products', 'column' => 'id', 'delete' => 'CASCADE', 'update' => 'CASCADE']],
            'created_at' => ['type' => 'int'],
            'updated_at' => ['type' => 'int'],
        ]);

        $this->createTable('shop_invoices', [
            'id' => ['type' => 'int', 'unsigned' => true, 'auto_increment' => true, 'primary' => true],
            'order_id' => ['type' => 'int', 'unsigned' => true, 'foreign' => ['table' => 'shop_orders', 'column' => 'id', 'delete' => 'CASCADE', 'update' => 'CASCADE']],
            'identifier' => ['type' => 'varchar'],
            'file' => ['type' => 'varchar'],
            'created_at' => ['type' => 'int'],
            'updated_at' => ['type' => 'int'],
        ]);

        $this->createTable('shop_products_history', [
            'id' => ['type' => 'int', 'unsigned' => true, 'auto_increment' => true, 'primary' => true],
            'session_id' => ['type' => 'varchar', 'null' => true],
            'user_id' => ['type' => 'int', 'unsigned' => true, 'null' => true, 'foreign' => ['table' => 'users', 'column' => 'id', 'delete' => 'CASCADE', 'update' => 'CASCADE']],
            'product_id' => ['type' => 'int', 'unsigned' => true, 'null' => true, 'foreign' => ['table' => 'shop_products', 'column' => 'id', 'delete' => 'CASCADE', 'update' => 'CASCADE']],
            'created_at' => ['type' => 'int'],
            'updated_at' => ['type' => 'int'],
        ]);

        $this->createTable('shop_products_media', [
            'id' => ['type' => 'int', 'unsigned' => true, 'auto_increment' => true, 'primary' => true],
            'product_id' => ['type' => 'int', 'unsigned' => true, 'null' => true, 'foreign' => ['table' => 'shop_products', 'column' => 'id', 'delete' => 'SET NULL', 'update' => 'CASCADE']],
            'media' => ['type' => 'varchar'],
            'type' => ['type' => 'varchar'],
            'ord' => ['type' => 'int'],
        ]);

        $this->createTable('shop_products_enquiries', [
            'id' => ['type' => 'int', 'unsigned' => true, 'auto_increment' => true, 'primary' => true],
            'product_id' => ['type' => 'int', 'unsigned' => true, 'null' => true, 'foreign' => ['table' => 'shop_products', 'column' => 'id', 'delete' => 'CASCADE', 'update' => 'CASCADE']],
            'email' => ['type' => 'varchar'],
            'phone' => ['type' => 'varchar'],
            'viewed' => ['type' => 'tinyint'],
            'created_at' => ['type' => 'int'],
            'updated_at' => ['type' => 'int'],
        ]);

        $this->createTable('shop_categories_attributes_sets_ring', [
            'id' => ['type' => 'int', 'unsigned' => true, 'auto_increment' => true, 'primary' => true],
            'category_id' => ['type' => 'int', 'unsigned' => true, 'foreign' => ['table' => 'shop_categories', 'column' => 'id', 'delete' => 'CASCADE', 'update' => 'CASCADE']],
            'set_id' => ['type' => 'int', 'unsigned' => true, 'foreign' => ['table' => 'shop_attributes_sets', 'column' => 'id', 'delete' => 'CASCADE', 'update' => 'CASCADE']],
        ]);

        $this->createTable('shop_categories_attributes_ring', [
            'id' => ['type' => 'int', 'unsigned' => true, 'auto_increment' => true, 'primary' => true],
            'category_id' => ['type' => 'int', 'unsigned' => true, 'foreign' => ['table' => 'shop_categories', 'column' => 'id', 'delete' => 'CASCADE', 'update' => 'CASCADE']],
            'attribute_id' => ['type' => 'int', 'unsigned' => true, 'foreign' => ['table' => 'shop_attributes', 'column' => 'id', 'delete' => 'CASCADE', 'update' => 'CASCADE']],
        ]);

        $this->createTable('shop_products_categories_ring', [
            'id' => ['type' => 'int', 'unsigned' => true, 'auto_increment' => true, 'primary' => true],
            'category_id' => ['type' => 'int', 'unsigned' => true, 'null' => true, 'foreign' => ['table' => 'shop_categories', 'column' => 'id', 'delete' => 'CASCADE', 'update' => 'CASCADE']],
            'product_id' => ['type' => 'int', 'unsigned' => true, 'null' => true, 'foreign' => ['table' => 'shop_products', 'column' => 'id', 'delete' => 'CASCADE', 'update' => 'CASCADE']],
        ]);

        $this->createTable('shop_products_attributes_ring', [
            'id' => ['type' => 'int', 'unsigned' => true, 'auto_increment' => true, 'primary' => true],
            'product_id' => ['type' => 'int', 'unsigned' => true, 'null' => true, 'foreign' => ['table' => 'shop_products', 'column' => 'id', 'delete' => 'CASCADE', 'update' => 'CASCADE']],
            'attribute_id' => ['type' => 'int', 'unsigned' => true, 'null' => true, 'foreign' => ['table' => 'shop_attributes', 'column' => 'id', 'delete' => 'CASCADE', 'update' => 'CASCADE']],
            'value' => ['type' => 'longtext']
        ]);

        $this->createTable('shop_products_taxes_ring', [
            'id' => ['type' => 'int', 'unsigned' => true, 'auto_increment' => true, 'primary' => true],
            'product_id' => ['type' => 'int', 'unsigned' => true, 'null' => true, 'foreign' => ['table' => 'shop_products', 'column' => 'id', 'delete' => 'CASCADE', 'update' => 'CASCADE']],
            'tax_id' => ['type' => 'int', 'unsigned' => true, 'null' => true, 'foreign' => ['table' => 'shop_taxes', 'column' => 'id', 'delete' => 'CASCADE', 'update' => 'CASCADE']],
            'vendor_id' => ['type' => 'int', 'unsigned' => true, 'null' => true, 'foreign' => ['table' => 'shop_vendors', 'column' => 'id', 'delete' => 'CASCADE', 'update' => 'CASCADE']],
            'ord' => ['type' => 'int']
        ]);

        $this->createTable('shop_products_discounts_ring', [
            'id' => ['type' => 'int', 'unsigned' => true, 'auto_increment' => true, 'primary' => true],
            'product_id' => ['type' => 'int', 'unsigned' => true, 'null' => true, 'foreign' => ['table' => 'shop_products', 'column' => 'id', 'delete' => 'CASCADE', 'update' => 'CASCADE']],
            'discount_id' => ['type' => 'int', 'unsigned' => true, 'null' => true, 'foreign' => ['table' => 'shop_discounts', 'column' => 'id', 'delete' => 'CASCADE', 'update' => 'CASCADE']],
            'vendor_id' => ['type' => 'int', 'unsigned' => true, 'null' => true, 'foreign' => ['table' => 'shop_vendors', 'column' => 'id', 'delete' => 'CASCADE', 'update' => 'CASCADE']],
            'ord' => ['type' => 'int']
        ]);

        $this->createTable('shop_products_vendors_ring', [
            'id' => ['type' => 'int', 'unsigned' => true, 'auto_increment' => true, 'primary' => true],
            'product_id' => ['type' => 'int', 'unsigned' => true, 'null' => true, 'foreign' => ['table' => 'shop_products', 'column' => 'id', 'delete' => 'CASCADE', 'update' => 'CASCADE']],
            'vendor_id' => ['type' => 'int', 'unsigned' => true, 'null' => true, 'foreign' => ['table' => 'shop_vendors', 'column' => 'id', 'delete' => 'CASCADE', 'update' => 'CASCADE']],
            'sku' => ['type' => 'varchar'],
            'quantity' => ['type' => 'int'],
            'price' => ['type' => 'float'],
            'ord' => ['type' => 'tinyint'],
        ]);

        // BLOG
        $this->createTable('blog_authors', [
            'id' => ['type' => 'int', 'unsigned' => true, 'auto_increment' => true, 'primary' => true],
            'name' => ['type' => 'varchar'],
            'position' => ['type' => 'varchar'],
            'image' => ['type' => 'varchar'],
        ]);

        $this->createTable('blog_posts', [
            'id' => ['type' => 'int', 'unsigned' => true, 'auto_increment' => true, 'primary' => true],
            'author_id' => ['type' => 'int', 'unsigned' => true, 'null' => true, 'foreign' => ['table' => 'blog_authors', 'column' => 'id', 'delete' => 'SET NULL', 'update' => 'CASCADE']],
            'name' => ['type' => 'varchar'],
            'intro' => ['type' => 'longtext'],
            'content' => ['type' => 'longtext'],
            'meta_title' => ['type' => 'varchar'],
            'meta_description' => ['type' => 'longtext'],
            'meta_keywords' => ['type' => 'longtext'],
            'created_at' => ['type' => 'int'],
            'updated_at' => ['type' => 'int'],
        ]);

        $this->createTable('blog_posts_media', [
            'id' => ['type' => 'int', 'unsigned' => true, 'auto_increment' => true, 'primary' => true],
            'post_id' => ['type' => 'int', 'unsigned' => true, 'null' => true, 'foreign' => ['table' => 'blog_posts', 'column' => 'id', 'delete' => 'SET NULL', 'update' => 'CASCADE']],
            'media' => ['type' => 'varchar'],
            'type' => ['type' => 'varchar'],
            'ord' => ['type' => 'int'],
        ]);

        // CUSTOM
        $this->createTable('team', [
            'id' => ['type' => 'int', 'unsigned' => true, 'auto_increment' => true, 'primary' => true],
            'image' => ['type' => 'varchar'],
            'first_name' => ['type' => 'varchar'],
            'last_name' => ['type' => 'varchar'],
            'job_title' => ['type' => 'varchar'],
            'facebook' => ['type' => 'varchar'],
            'twitter' => ['type' => 'varchar'],
            'instagram' => ['type' => 'varchar'],
            'linkedin' => ['type' => 'varchar'],
            'created_at' => ['type' => 'int'],
            'updated_at' => ['type' => 'int'],
            'ord' => ['type' => 'int'],
        ]);

        $this->createTable('partners', [
            'id' => ['type' => 'int', 'unsigned' => true, 'auto_increment' => true, 'primary' => true],
            'name' => ['type' => 'varchar'],
            'image' => ['type' => 'varchar'],
            'url' => ['type' => 'varchar'],
            'ord' => ['type' => 'int'],
        ]);

        $this->createTable('sell_slides', [
            'id' => ['type' => 'int', 'unsigned' => true, 'auto_increment' => true, 'primary' => true],
            'name' => ['type' => 'varchar'],
            'intro' => ['type' => 'longtext'],
            'image' => ['type' => 'varchar'],
            'primary_button_text' => ['type' => 'varchar'],
            'primary_button_url' => ['type' => 'varchar'],
            'secondary_button_text' => ['type' => 'varchar'],
            'secondary_button_url' => ['type' => 'varchar'],
            'ord' => ['type' => 'int'],
        ]);

        $this->createTable('buy_slides', [
            'id' => ['type' => 'int', 'unsigned' => true, 'auto_increment' => true, 'primary' => true],
            'name' => ['type' => 'varchar'],
            'intro' => ['type' => 'longtext'],
            'image' => ['type' => 'varchar'],
            'button_text' => ['type' => 'varchar'],
            'button_url' => ['type' => 'varchar'],
            'ord' => ['type' => 'int'],
            'created_at' => ['type' => 'int'],
            'updated_at' => ['type' => 'int'],
        ]);

        Fixture::layouts();
        Fixture::pages();
        Fixture::urls();
        Fixture::menuLocations();
        Fixture::menus();
        Fixture::adminUsers();
        Fixture::settings();
        Fixture::analytics();
        Fixture::countries();
        Fixture::counties();
        Fixture::cities();
        Fixture::currencies();
    }

    /**
     * @return void
     */
    public function down() {}
}