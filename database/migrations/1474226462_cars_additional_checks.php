<?php

class Migration_1474226462 extends Database
{
     /**
     * @return void
     */
    public function up()
    {
        $this->addColumn('cars', 'v_exterior_bara_spate', ['type' => 'tinyint', 'default' => '1', 'after' => 'v_exterior_stopuri']);
        $this->addColumn('cars', 'v_exterior_aripi_spate', ['type' => 'tinyint', 'default' => '1', 'after' => 'v_exterior_stopuri']);
        $this->addColumn('cars', 'v_exterior_teava_esapament', ['type' => 'tinyint', 'default' => '1', 'after' => 'v_exterior_stopuri']);
        $this->addColumn('cars', 'v_exterior_sigla_spate', ['type' => 'tinyint', 'default' => '1', 'after' => 'v_exterior_stopuri']);
        $this->addColumn('cars', 'v_exterior_antena_radio', ['type' => 'tinyint', 'default' => '1', 'after' => 'v_exterior_stopuri']);
        $this->addColumn('cars', 'v_exterior_usa_rezervor', ['type' => 'tinyint', 'default' => '1', 'after' => 'v_exterior_stopuri']);
        $this->addColumn('cars', 'v_exterior_fete_usi', ['type' => 'tinyint', 'default' => '1', 'after' => 'v_exterior_stopuri']);
        $this->addColumn('cars', 'v_exterior_semn_accident', ['type' => 'tinyint', 'default' => '1', 'after' => 'v_exterior_stopuri']);

        $this->addColumn('cars', 'v_interior_cheie', ['type' => 'tinyint', 'default' => '1', 'after' => 'v_interior_navigatie']);
        $this->addColumn('cars', 'v_interior_erori_bord', ['type' => 'tinyint', 'default' => '1', 'after' => 'v_interior_navigatie']);

        $this->addColumn('cars', 'v_functionabile_faruri', ['type' => 'tinyint', 'default' => '1', 'after' => 'v_interior_navigatie']);
        $this->addColumn('cars', 'v_functionabile_semnalizari', ['type' => 'tinyint', 'default' => '1', 'after' => 'v_interior_navigatie']);
        $this->addColumn('cars', 'v_functionabile_stop_frana', ['type' => 'tinyint', 'default' => '1', 'after' => 'v_interior_navigatie']);
        $this->addColumn('cars', 'v_functionabile_avarii', ['type' => 'tinyint', 'default' => '1', 'after' => 'v_interior_navigatie']);
        $this->addColumn('cars', 'v_functionabile_oglinzi', ['type' => 'tinyint', 'default' => '1', 'after' => 'v_interior_navigatie']);
        $this->addColumn('cars', 'v_functionabile_stopuri_ceata', ['type' => 'tinyint', 'default' => '1', 'after' => 'v_interior_navigatie']);
        $this->addColumn('cars', 'v_functionabile_proiectoare', ['type' => 'tinyint', 'default' => '1', 'after' => 'v_interior_navigatie']);
        $this->addColumn('cars', 'v_functionabile_manere', ['type' => 'tinyint', 'default' => '1', 'after' => 'v_interior_navigatie']);
        $this->addColumn('cars', 'v_functionabile_curatatoare_faruri', ['type' => 'tinyint', 'default' => '1', 'after' => 'v_interior_navigatie']);
        $this->addColumn('cars', 'v_functionabile_sistem_decapotare', ['type' => 'tinyint', 'default' => '1', 'after' => 'v_interior_navigatie']);
        $this->addColumn('cars', 'v_functionabile_comenzi_telecomanda', ['type' => 'tinyint', 'default' => '1', 'after' => 'v_interior_navigatie']);
        $this->addColumn('cars', 'v_functionabile_alarma', ['type' => 'tinyint', 'default' => '1', 'after' => 'v_interior_navigatie']);

        $this->addColumn('cars', 'v_subcapota_aliniere_elemente', ['type' => 'tinyint', 'default' => '1', 'after' => 'v_subcapota_lichid_frana']);

        $this->addColumn('cars', 'v_roti_kit_schimbare_anvelopa', ['type' => 'tinyint', 'default' => '1', 'after' => 'v_roti_suspensie']);

        $this->addColumn('cars', 'v_submasina_axe', ['type' => 'tinyint', 'default' => '1', 'after' => 'v_submasina_dieferential']);
        $this->addColumn('cars', 'v_submasina_amortizoare', ['type' => 'tinyint', 'default' => '1', 'after' => 'v_submasina_dieferential']);
        $this->addColumn('cars', 'v_submasina_bielete', ['type' => 'tinyint', 'default' => '1', 'after' => 'v_submasina_dieferential']);

        $this->addColumn('cars', 'v_inmers_performanta_motor', ['type' => 'tinyint', 'default' => '1', 'after' => 'v_inmers_sunete_vibratii']);
        $this->addColumn('cars', 'v_inmers_performanta_directie', ['type' => 'tinyint', 'default' => '1', 'after' => 'v_inmers_sunete_vibratii']);
        $this->addColumn('cars', 'v_inmers_demaraj', ['type' => 'tinyint', 'default' => '1', 'after' => 'v_inmers_sunete_vibratii']);
        $this->addColumn('cars', 'v_inmers_transmisie', ['type' => 'tinyint', 'default' => '1', 'after' => 'v_inmers_sunete_vibratii']);
        $this->addColumn('cars', 'v_inmers_ambreiaj', ['type' => 'tinyint', 'default' => '1', 'after' => 'v_inmers_sunete_vibratii']);
        $this->addColumn('cars', 'v_inmers_performanta_cutie_viteze', ['type' => 'tinyint', 'default' => '1', 'after' => 'v_inmers_sunete_vibratii']);
        $this->addColumn('cars', 'v_inmers_directie_linie_dreapta', ['type' => 'tinyint', 'default' => '1', 'after' => 'v_inmers_sunete_vibratii']);
        $this->addColumn('cars', 'v_inmers_directie_franare_brusca', ['type' => 'tinyint', 'default' => '1', 'after' => 'v_inmers_sunete_vibratii']);
        $this->addColumn('cars', 'v_inmers_sunet_anvelope', ['type' => 'tinyint', 'default' => '1', 'after' => 'v_inmers_sunete_vibratii']);
        $this->addColumn('cars', 'v_inmers_sunet_suspensii', ['type' => 'tinyint', 'default' => '1', 'after' => 'v_inmers_sunete_vibratii']);
        $this->addColumn('cars', 'v_inmers_testare_moduri_masina', ['type' => 'tinyint', 'default' => '1', 'after' => 'v_inmers_sunete_vibratii']);
        $this->addColumn('cars', 'v_inmers_testare_navigatie_gps', ['type' => 'tinyint', 'default' => '1', 'after' => 'v_inmers_sunete_vibratii']);
    }

    /**
     * @return void
     */
    public function down()
    {

    }
}