<?php

class Car_Model_Brand extends Cms_Model
{
    /**
     * @var string
     */
    protected $table = 'cars_brands';

    /**
     * @var array
     */
    protected $validates = [
        'validation' => [
            'name' => [
                'presence',
            ],
        ],
        'unique' => [
            'name',
        ],
    ];

    /**
     * @var array
     */
    protected $actsAs = [
        'draggable',
    ];

    /**
     * @var array
     */
    protected $hasMany = [
        'models' => [
            'model'         => 'Car_Model_Brand',
            'remote_key'    => 'brand_id',
        ],
    ];

    /**
     * @set $class
     */
    public function __construct()
    {
        parent::__construct();

        $this->class = get_class();
    }

    /**
     * @return array
     */
    public static function toAutoComplete()
    {
        $model  = new self();
        $brands = [];

        $brands['suggestions']  = [];
        $brands['data']         = [];

        foreach ($model->search(Get::get('query'), ['name'])->order('ord')->getCollection() as $brand) {
            $brands['suggestions'][] = [
                'id'    => $brand->getId(),
                'value' => $brand->getName(),
                'logo' => Url::getPrefix() . '/' . Dir::getImage() . explode('.', $brand->getLogoDark())[0] . '_default.' . explode('.', $brand->getLogoDark())[1],
            ];

            $brands['data'][] = [
                'id'    => $brand->getId(),
                'value' => $brand->getName(),
                'logo' => Url::getPrefix() . '/' . Dir::getImage() . explode('.', $brand->getLogoDark())[0] . '_default.' . explode('.', $brand->getLogoDark())[1],
            ];
        }

        return $brands;
    }
}