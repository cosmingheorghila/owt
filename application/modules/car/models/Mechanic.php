<?php

class Car_Model_Mechanic extends Cms_Model
{
    /**
     * @var string
     */
    protected $table = 'cars_mechanics';

    /**
     * @var array
     */
    protected $validates = [
        'validation' => [
            'first_name' => [
                'presence',
            ],
            'last_name' => [
                'presence',
            ],
            'email' => [
                'presence',
                'email',
            ],
        ],
    ];

    /**
     * @var array
     */
    protected $actsAs = [
        'timestamp',
    ];

    /**
     * @var array
     */
    protected $hasAndBelongsToMany = [
        'certifications' => [
            'model'         => 'Car_Model_Certification',
            'table'         => 'cars_mechanics_certifications_ring',
            'local_key'     => 'mechanic_id',
            'remote_key'    => 'certification_id',
            'order_field'   => 'ord',
            'order_dir'     => 'asc',
            'enable_rel'    => true,
        ],
    ];

    /**
     * @set $class
     */
    public function __construct()
    {
        parent::__construct();

        $this->class = get_class();
    }
}