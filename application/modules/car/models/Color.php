<?php

class Car_Model_Color extends Cms_Model
{
    /**
     * @var string
     */
    protected $table = 'cars_colors';

    /**
     * @var array
     */
    protected $validates = [
        'validation' => [
            'name' => [
                'presence',
            ],
            'color' => [
                'presence',
            ],
        ],
    ];

    /**
     * @var array
     */
    protected $actsAs = [
        'draggable' => [
            'field' => 'model_id',
        ],
    ];

    /**
     * @var array
     */
    protected $belongsTo = [
        'model' => [
            'model'         => 'Car_Model_Model',
            'remote_key'    => 'model_id',
        ],
    ];

    /**
     * @const
     */
    const SHOW_YES  = 1;
    const SHOW_NO   = 2;

    /**
     * @var array
     */
    public static $show = [
        self::SHOW_YES  => 'Yes',
        self::SHOW_NO   => 'No',
    ];

    /**
     * @set $class
     */
    public function __construct()
    {
        parent::__construct();

        $this->class = get_class();
    }
}