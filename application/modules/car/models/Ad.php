<?php

class Car_Model_Ad extends Cms_Model
{
    /**
     * @var string
     */
    protected $table = 'cars_ads';

    /**
     * @const
     */
    const ACTIVE_NO     = 0;
    const ACTIVE_YES    = 1;

    /**
     * @const
     */
    const CONVERTED_NO  = 0;
    const CONVERTED_YES = 1;

    /**
     * @var array
     */
    protected $validates = [
        'validation' => [
            'brand_id' => [
                'presence',
                'numeric',
            ],
            'model_id' => [
                'presence',
                'numeric',
            ],
            'city_id' => [
                'presence',
                'numeric',
            ],
            'color_id' => [
                'presence',
                'numeric',
            ],
            'identifier' => [
                'presence',
            ],
            'kilometers' => [
                'presence',
            ],
            'year' => [
                'presence',
            ],
        ],
        'unique' => [
            'identifier',
        ],
    ];

    /**
     * @var array
     */
    protected $actsAs = [
        'timestamp',
    ];

    /**
     * @var array
     */
    protected $belongsTo = [
        'brand' => [
            'model'         => 'Car_Model_Brand',
            'remote_key'    => 'brand_id',
        ],
        'model' => [
            'model'         => 'Car_Model_Model',
            'remote_key'    => 'model_id',
        ],
        'city' => [
            'model'         => 'Default_Model_City',
            'remote_key'    => 'city_id',
        ],
        'color' => [
            'model'         => 'Car_Model_Color',
            'remote_key'    => 'color_id',
        ],
    ];

    /**
     * @var array
     */
    protected $hasOne = [
        'owner' => [
            'model'         => 'Car_Model_Ad_Owner',
            'remote_key'    => 'ad_id',
        ],
        'address' => [
            'model'         => 'Car_Model_Ad_Address',
            'remote_key'    => 'ad_id',
        ],
    ];

    /**
     * @var array
     */
    protected $hasMany = [
        'availabilities' => [
            'model'         => 'Car_Model_Ad_Availability',
            'remote_key'    => 'ad_id',
        ],
        'stats' => [
            'model'         => 'Car_Model_Ad_Stat',
            'remote_key'    => 'ad_id',
        ],
    ];

    /**
     * @var string
     */
    protected $apiKey       = '156b21958099';
    protected $secretKey    = '3685523bee';

    /**
     * @var array
     */
    public $active = [
        self::ACTIVE_NO     => 'No',
        self::ACTIVE_YES    => 'Yes',
    ];

    /**
     * @var array
     */
    public $converted = [
        self::CONVERTED_NO  => 'No',
        self::CONVERTED_YES => 'Yes',
    ];

    /**
     * @set $class
     */
    public function __construct()
    {
        parent::__construct();

        $this->class = get_class();
    }

    /**
     * @return bool
     */
    public function convert()
    {
        if ($this->getConverted() == self::CONVERTED_YES) {
            return false;
        }

        try {
            $name = $this->getYear() . ' ' . $this->getBrand()->getName() . ' ' . $this->getModel()->getName() . ' | ' . $this->getVin();

            $carId = Car_Model_Car::getInstance()->insert([
                'name' => $name,
                'brand_id' => $this->getBrandId(),
                'model_id' => $this->getModelId(),
                'exterior_color_id' => $this->getColorId(),
                'price' => 0,
                'year' => $this->getYear(),
                'kilometers' => $this->getKilometers(),
                'vin' => $this->getVin(),
            ])->save();

            if ($carId) {
                $count = 1;
                $url = Cms_Model_Page::getInstance()->load([
                    'identifier' => 'buy'
                ])->getUri() . '/' . String::slugify($name);

                while (Database::getInstance('cms_urls')->load(['url' => $url])) {
                    $url = $url . '-' . $count;
                    $count++;
                }

                Database::getInstance('cms_urls')->insert([
                    'entity_id' => $carId,
                    'entity' => 'cars',
                    'url' => $url,
                ])->save();

                $this->update([
                    'converted' => self::CONVERTED_YES
                ])->where([
                    'id' => $this->getId()
                ])->save();
            }

            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * @param string|null $vin
     * @return bool
     */
    public function validateVin($vin = null)
    {
        //get vin data
        $data = file_get_contents("https://api.vindecoder.eu/2.0/{$this->apiKey}/" . substr(sha1("info-{$vin}|{$this->apiKey}|{$this->secretKey}"), 0, 10) . "/decode/info/{$vin}.json", false);
        $result = json_decode($data);

        if ($result->error === true) {
            return false;
        }

        if (isset($result->decode) && is_array($result->decode)) {
            if (in_array('Stolen', $result->decode)) {

                //decode vin
                $data = file_get_contents("https://api.vindecoder.eu/2.0/{$this->apiKey}/" . substr(sha1("{$vin}|{$this->apiKey}|{$this->secretKey}"), 0, 10) . "/decode/{$vin}.json", false);
                $result = json_decode($data);

                if (!isset($result->decode)) {
                    return false;
                }

                return false;
            }
        }

        return true;
    }

    /**
     * @param array $data
     * @param int|null $ad
     * @return mixed
     */
    public function saveData($data = [], $ad = null)
    {
        $this->saveDataErrors($data);

        $data['identifier'] = String::random();

        while ($this->load(['identifier' => $data['identifier']])) {
            $data['identifier'] = String::random();
        }

        return $this->merge($ad ? 'update' : 'insert', $data, $ad ? ['id' => $ad] : null);
    }

    /**
     * @param array $data
     * @return void
     */
    private function saveDataErrors($data = [])
    {
        $city = Default_Model_City::getInstance()->load($data['city_id']);

        if (!$city || $city->getValid() == Default_Model_City::VALID_NO) {
            Redirect::page('sell-city-error');
        }

        if (!isset($data['kilometers']) || (int)$data['kilometers'] > 180000) {
            Redirect::page('sell-km-error');
        }
    }
}