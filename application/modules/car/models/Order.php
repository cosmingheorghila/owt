<?php

class Car_Model_Order extends Cms_Model
{
    /**
     * @var string
     */
    protected $table = 'cars_orders';

    /**
     * @var array
     */
    protected $actsAs = [
        'timestamp',
    ];

    /**
     * @var array
     */
    protected $belongsTo = [
        'car' => [
            'model'         => 'Car_Model_Car',
            'remote_key'    => 'car_id',
        ],
    ];

    /**
     * @const
     */
    const STATUS_CANCELED   = 0;
    const STATUS_PENDING    = 1;
    const STATUS_COMPLETED  = 2;

    /**
     * @const
     */
    const PAYMENT_REJECTED  = 0;
    const PAYMENT_CONFIRMED = 1;
    const PAYMENT_PENDING   = 2;
    const PAYMENT_OPEN      = 3;
    const PAYMENT_CANCELED  = 4;
    const PAYMENT_REFUNDED  = 5;

    /**
     * @const
     */
    const ACTIVE_NO     = 0;
    const ACTIVE_YES    = 1;

    /**
     * @const
     */
    const VIEWED_NO     = 0;
    const VIEWED_YES    = 1;

    /**
     * @var array
     */
    public static $orderData = [
        'car_id',
        'user_id',
        'identifier',
        'name',
        'paid',
        'remaining',
        'first_name',
        'last_name',
        'email',
        'phone',
        'county',
        'city',
        'street',
        'street_no',
        'status',
        'payment',
        'active',
        'viewed',
    ];

    /**
     * @var array
     */
    public static $statuses = [
        self::STATUS_CANCELED   => 'Canceled',
        self::STATUS_PENDING    => 'Pending',
        self::STATUS_COMPLETED  => 'Completed',
    ];

    /**
     * @var array
     */
    public static $payments = [
        self::PAYMENT_REJECTED  => 'Rejected',
        self::PAYMENT_CONFIRMED => 'Confirmed',
        self::PAYMENT_PENDING   => 'Pending',
        self::PAYMENT_OPEN      => 'Open',
        self::PAYMENT_CANCELED  => 'Canceled',
        self::PAYMENT_REFUNDED  => 'Refunded',
    ];

    /**
     * @var array
     */
    public static $active = [
        self::ACTIVE_NO     => 'No',
        self::ACTIVE_YES    => 'Yes',
    ];

    /**
     * @var array
     */
    public static $viewed = [
        self::VIEWED_NO     => 'No',
        self::VIEWED_YES    => 'Yes',
    ];

    /**
     * @var
     */
    public static $identifier   = null;
    public static $exists       = null;

    /**
     * @set $class
     */
    public function __construct()
    {
        parent::__construct();

        $this->class = get_class();
    }

    /**
     * @param Car_Model_Car $car
     * @return bool
     */
    public static function step1(Car_Model_Car $car)
    {
        try {
            self::verifyExistence($car);
            self::setIdentifier();

            $data = [
                'car_id'        => $car->getId(),
                'user_id'       => session_id(),
                'identifier'    => self::$identifier,
                'name'          => $car->getName(),
                'remaining'     => $car->getTotalPrice(),
                'created_at'    => time(),
                'updated_at'    => time(),
            ];

            if (self::$exists === null) {
                $query = self::getInstance()->insert($data);
            } else {
                $query = self::getInstance()->update($data)->where(self::$exists);
            }

            if ($query->save()) {
                $_SESSION['orders'][] = [
                    'identifier'    => $data['identifier'],
                    'car_id'        => $data['car_id'],
                    'user_id'       => $data['user_id'],
                ];

                if (self::$exists === null) {
                    (new Email('postmark'))
                        ->from(Setting::get('company-email')->getValue())
                        ->to(['ionut.mares@owt-studio.ro', Setting::get('company-email')->getValue()])
                        ->subject('OWTG - New Order')
                        ->messageHtml('emails/new_order.phtml', ['theUrl' => Url::getHostUrl() . Url::getAdminUrl() . '/car/orders/index'])
                        ->send();
                }

                return true;
            }

            return false;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * @param Car_Model_Car $car
     * @param array|null $data
     * @return bool
     */
    public static function step2(Car_Model_Car $car, array $data = null)
    {
        try {
            self::verifyExistence($car);

            if (self::$exists === null || !isset($data['county_id'])) {
                return false;
            }

            $county = Default_Model_County::getInstance()->load($data['county_id']);

            foreach ($data as $key => $val) {
                if (!in_array($key, self::$orderData)) {
                    unset($data[$key]);
                }
            }

            $data['remaining'] = $car->getTotalPrice($county->getDeliveryCost());

            return self::getInstance()->update($data)->where(self::$exists)->save();
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * @param Car_Model_Car $car
     * @return void
     */
    private static function verifyExistence(Car_Model_Car $car)
    {
        if (Session::exists('orders') && is_array(Session::get('orders')) && !empty(Session::get('orders'))) {
            foreach (Session::get('orders') as $order) {
                if ($order['car_id'] == $car->getId() && $order['user_id'] == session_id()) {
                    self::$exists = ['identifier' => $order['identifier']];
                }
            }
        }
    }

    /**
     * @return void
     */
    private static function setIdentifier()
    {
        self::$identifier = String::random(25);

        while (self::getInstance()->load(['identifier' => self::$identifier])) {
            self::$identifier = String::random(15);
        }
    }
}