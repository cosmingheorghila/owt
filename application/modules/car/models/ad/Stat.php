<?php

class Car_Model_Ad_Stat extends Cms_Model
{
    /**
     * @var string
     */
    protected $table = 'cars_ads_stats';

    /**
     * @var array
     */
    protected $validates = [
        'validation' => [
            'name' => [
                'presence',
            ],
        ],
    ];

    /**
     * @var array
     */
    protected $belongsTo = [
        'ad' => [
            'model'         => 'Car_Model_Ad',
            'remote_key'    => 'ad_id',
        ],
    ];

    /**
     * @set $class
     */
    public function __construct()
    {
        parent::__construct();

        $this->class = get_class();
    }

    /**
     * @param array $data
     * @return bool
     */
    public function saveData($data = [])
    {
        if (!$this->saveDataErrors($data)) {
            return false;
        }

        $db = new Database();

        try {
            $db->begin();
            $db->setTable($this->getTable());

            $db->delete()->where(['ad_id' => $data['ad_id']])->save();

            foreach ($data['stats'] as $stat) {
                $db->insert([
                    'ad_id' => $data['ad_id'],
                    'name' => $stat,
                ])->save();
            }

            $db->commit();
        } catch (Exception $e) {
            Flash::error('Se pare ca a fost o problema. Incearca, te rugam, inca o data.');
            return false;
        }

        return true;
    }

    /**
     * @param array $data
     * @return bool
     */
    private function saveDataErrors($data = [])
    {
        if (!isset($data['stats']) || empty($data['stats'])) {
            return false;
        }

        foreach (Car_Model_Stat::getInstance()->getCollection() as $stat) {
            if ($stat->getImportant() == Car_Model_Stat::IMPORTANT_YES && !in_array($stat->getName(), $data['stats'])) {
                return false;
            }
        }

        return true;
    }
}