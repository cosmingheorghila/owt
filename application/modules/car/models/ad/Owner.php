<?php

class Car_Model_Ad_Owner extends Cms_Model
{
    /**
     * @var string
     */
    protected $table = 'cars_ads_owners';

    /**
     * @var array
     */
    protected $validates = [
        'validation' => [
            'first_name' => [
                'presence',
            ],
            'last_name' => [
                'presence',
            ],
            'email' => [
                'presence',
                'email',
            ],
        ],
    ];

    /**
     * @var array
     */
    protected $belongsTo = [
        'ad' => [
            'model'         => 'Car_Model_Ad',
            'remote_key'    => 'ad_id',
        ],
    ];

    /**
     * @set $class
     */
    public function __construct()
    {
        parent::__construct();

        $this->class = get_class();
    }

    /**
     * @param array $data
     * @return bool
     */
    public function saveData($data = [])
    {
        $db = new Database();

        try {
            $db->begin();
            $db->setTable($this->getTable());

            $db->delete()->where(['ad_id' => $data['ad_id']])->save();
            $id = $db->insert($data)->save();

            $db->commit();

            return $id;
        } catch (Exception $e) {
            Flash::error('Se pare ca a fost o problema. Incearca, te rugam, inca o data.');
            return false;
        }
    }
}