<?php

class Car_Model_Ad_Availability extends Cms_Model
{
    /**
     * @var string
     */
    protected $table = 'cars_ads_availabilities';

    /**
     * @var array
     */
    protected $validates = [
        'validation' => [
            'month' => [
                'presence',
            ],
            'day' => [
                'presence',
            ],
            'hour' => [
                'presence',
            ],
        ],
    ];

    /**
     * @var array
     */
    protected $belongsTo = [
        'ad' => [
            'model'         => 'Car_Model_Ad',
            'remote_key'    => 'ad_id',
        ],
    ];

    /**
     * @set $class
     */
    public function __construct()
    {
        parent::__construct();

        $this->class = get_class();
    }

    /**
     * @param array $data
     * @param int|null $adId
     * @return bool
     */
    public function saveData($data = [], $adId = null)
    {
        if (!$adId) {
            return false;
        }

        $db = new Database();

        try {
            $db->begin();
            $db->setTable($this->getTable());

            $db->delete()->where(['ad_id' => $adId])->save();

            foreach ($data as $index => $item) {
                $item['ad_id'] = $adId;

                $db->insert($item)->save();
            }

            $db->commit();
        } catch (Exception $e) {
            Flash::error('Se pare ca a fost o problema. Incearca, te rugam, inca o data.');
            return false;
        }

        return true;
    }
}