<?php

class Car_Model_Feature extends Cms_Model
{
    /**
     * @var string
     */
    protected $table = 'cars_features';

    /**
     * @var array
     */
    protected $validates = [
        'validation' => [
            'name' => [
                'presence',
            ],
        ],
    ];

    /**
     * @var array
     */
    protected $actsAs = [
        'timestamp',
        'draggable',
    ];

    /**
     * @var array
     */
    protected $hasAndBelongsToMany = [
        'cars' => [
            'model'         => 'Car_Model_Car',
            'table'         => 'cars_features_ring',
            'local_key'     => 'feature_id',
            'remote_key'    => 'car_id',
            'order_field'   => 'ord',
            'order_dir'     => 'asc',
            'enable_rel'    => true,
        ],
    ];

    /**
     * @const
     */
    const FILTER_NO     = 0;
    const FILTER_YES    = 1;

    /**
     * @var array
     */
    public static $filters = [
        self::FILTER_NO     => 'No',
        self::FILTER_YES    => 'Yes',
    ];

    /**
     * @set $class
     */
    public function __construct()
    {
        parent::__construct();

        $this->class = get_class();
    }
}