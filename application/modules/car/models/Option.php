<?php

class Car_Model_Option extends Cms_Model
{
    /**
     * @var string
     */
    protected $table = 'cars_options';

    /**
     * @var array
     */
    protected $validates = [
        'validation' => [
            'name' => [
                'presence',
            ],
        ],
    ];

    /**
     * @var array
     */
    protected $actsAs = [
        'timestamp',
        'draggable',
    ];

    /**
     * @var array
     */
    protected $hasAndBelongsToMany = [
        'cars' => [
            'model'         => 'Car_Model_Car',
            'table'         => 'cars_options_ring',
            'local_key'     => 'option_id',
            'remote_key'    => 'car_id',
            'order_field'   => 'ord',
            'order_dir'     => 'asc',
            'enable_rel'    => true,
        ],
    ];

    /**
     * @set $class
     */
    public function __construct()
    {
        parent::__construct();

        $this->class = get_class();
    }
}