<?php

class Car_Model_Car_Image extends Cms_Model
{
    /**
     * @var string
     */
    protected $table = 'cars_images';

    /**
     * @var array
     */
    protected $actsAs = [
        'draggable',
        'timestamp',
    ];

    /**
     * @var array
     */
    protected $belongsTo = [
        'car' => [
            'model'         => 'Car_Model_Car',
            'remote_key'    => 'car_id',
        ],
    ];

    /**
     * @set $class
     */
    public function __construct()
    {
        parent::__construct();

        $this->class = get_class();
    }

    /**
     * @return void
     */
    protected function beforeSave()
    {
        parent::beforeSave();

        $this->params['car_id']  = Get::get('car');
    }
}