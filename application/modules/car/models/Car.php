<?php

class Car_Model_Car extends Cms_Model
{
    /**
     * @var string
     */
    protected $table = 'cars';

    /**
     * @var array
     */
    protected $validates = [
        'validation' => [
            'name' => [
                'presence',
            ],
            'brand_id' => [
                'presence',
                'numeric',
            ],
            'model_id' => [
                'presence',
                'numeric',
            ],
            'price' => [
                'presence',
                'numeric',
            ],
            'year' => [
                'presence',
                'numeric',
            ],
            'kilometers' => [
                'presence',
                'numeric',
            ],
        ],
        'unique' => [
            'name',
        ],
    ];

    /**
     * @var array
     */
    protected $actsAs = [
        'timestamp',
        'draggable',
        'url',
    ];

    /**
     * @var array
     */
    protected $belongsTo = [
        'brand' => [
            'model'         => 'Car_Model_Brand',
            'remote_key'    => 'brand_id',
        ],
        'model' => [
            'model'         => 'Car_Model_Model',
            'remote_key'    => 'model_id',
        ],
        'mechanic' => [
            'model'         => 'Car_Model_Mechanic',
            'remote_key'    => 'mechanic_id',
        ],
        'currency' => [
            'model'         => 'Shop_Model_Currency',
            'remote_key'    => 'currency_id',
        ],
        'exterior_color' => [
            'model'         => 'Car_Model_Color',
            'remote_key'    => 'exterior_color_id',
        ],
        'interior_color' => [
            'model'         => 'Car_Model_Color',
            'remote_key'    => 'interior_color_id',
        ],
    ];

    /**
     * @var array
     */
    protected $hasOne = [
        'order' => [
            'model'         => 'Car_Model_Order',
            'remote_key'    => 'car_id',
        ],
        'mechanic' => [
            'model'         => 'Car_Model_Mechanic',
            'local_key'     => 'mechanic_id',
            'remote_key'    => 'id',
        ],
    ];

    /**
     * @var array
     */
    protected $hasMany = [
        'images' => [
            'model'         => 'Car_Model_Car_Image',
            'remote_key'    => 'car_id',
            'order_field'   => 'ord',
            'order_dir'     => 'asc',
        ],
        'perks' => [
            'model'         => 'Car_Model_Car_Perk',
            'remote_key'    => 'car_id',
            'order_field'   => 'ord',
            'order_dir'     => 'asc',
        ],
        'followers' => [
            'model'         => 'Car_Model_Follower',
            'remote_key'    => 'car_id',
            'order_field'   => 'created_at',
            'order_dir'     => 'desc',
        ],
    ];

    /**
     * @var array
     */
    protected $hasAndBelongsToMany = [
        'options' => [
            'model'         => 'Car_Model_Option',
            'table'         => 'cars_options_ring',
            'local_key'     => 'car_id',
            'remote_key'    => 'option_id',
            'order_field'   => 'ord',
            'order_dir'     => 'asc',
            'enable_rel'    => true,
        ],
        'features' => [
            'model'         => 'Car_Model_Feature',
            'table'         => 'cars_features_ring',
            'local_key'     => 'car_id',
            'remote_key'    => 'feature_id',
            'order_field'   => 'ord',
            'order_dir'     => 'asc',
            'enable_rel'    => true,
        ],
    ];

    /**
     * @const
     */
    const ACTIVE_NO     = 0;
    const ACTIVE_YES    = 1;

    /**
     * @const
     */
    const VERIFICATION_YES  = 1;
    const VERIFICATION_NO   = 2;
    const VERIFICATION_NA   = 3;

    /**
     * @const
     */
    const FUEL_DIESEL   = 1;
    const FUEL_GAS      = 2;
    const FUEL_HYBRID   = 3;
    const FUEL_ELECTRIC = 4;
    const FUEL_GPL = 4;

    /**
     * @const
     */
    const BODY_SEDAN        = 1;
    const BODY_COUPE        = 2;
    const BODY_CABRIO       = 3;
    const BODY_HATCHBACK    = 4;
    const BODY_SUV          = 5;
    const BODY_BREAK        = 6;
    const BODY_OFF_ROAD     = 7;

    /**
     * @const
     */
    const TRANSMISSION_MANUAL           = 1;
    const TRANSMISSION_AUTOMATIC        = 2;
    const TRANSMISSION_SEMI             = 3;
    const TRANSMISSION_AUTOMATIC_CVT    = 4;

    /**
     * @const
     */
    const TRACTION_FRONT    = 1;
    const TRACTION_BACK     = 2;
    const TRACTION_INTEGRAL = 3;

    /**
     * @const
     */
    const POLLUTION_EURO1 = 1;
    const POLLUTION_EURO2 = 2;
    const POLLUTION_EURO3 = 3;
    const POLLUTION_EURO4 = 4;
    const POLLUTION_EURO5 = 5;
    const POLLUTION_EURO6 = 6;

    /**
     * @var array
     */
    public static $active = [
        self::ACTIVE_NO     => 'No',
        self::ACTIVE_YES    => 'Yes',
    ];

    /**
     * @var array
     */
    public static $verifications = [
        self::VERIFICATION_YES  => 'Yes',
        self::VERIFICATION_NO   => 'No',
        self::VERIFICATION_NA   => 'N/A',
    ];

    /**
     * @var array
     */
    public static $fuels = [
        self::FUEL_DIESEL   => 'Diesel',
        self::FUEL_GAS      => 'Benzina',
        self::FUEL_HYBRID   => 'Hybrid',
        self::FUEL_ELECTRIC => 'Electric',
        self::FUEL_GPL      => 'GPL',
    ];

    /**
     * @var array
     */
    public static $bodies = [
        self::BODY_SEDAN        => 'Sedan',
        self::BODY_COUPE        => 'Coupe',
        self::BODY_CABRIO       => 'Cabrio',
        self::BODY_HATCHBACK    => 'Hatchback',
        self::BODY_SUV          => 'SUV',
        self::BODY_BREAK        => 'Break',
        self::BODY_OFF_ROAD     => 'Off-Road',
    ];

    /**
     * @var array
     */
    public static $transmissions = [
        self::TRANSMISSION_MANUAL           => 'Manuala',
        self::TRANSMISSION_AUTOMATIC        => 'Automata',
        self::TRANSMISSION_SEMI             => 'Semi-A',
        self::TRANSMISSION_AUTOMATIC_CVT    => 'Automata CVT',
    ];

    /**
     * @var array
     */
    public static $tractor = [
        self::TRACTION_FRONT    => 'Fata',
        self::TRACTION_BACK     => 'Spate',
        self::TRACTION_INTEGRAL => 'Integrala',
    ];

    /**
     * @var array
     */
    public static $pollutions = [
        self::POLLUTION_EURO1   => 'EURO 1',
        self::POLLUTION_EURO2   => 'EURO 2',
        self::POLLUTION_EURO3   => 'EURO 3',
        self::POLLUTION_EURO4   => 'EURO 4',
        self::POLLUTION_EURO5   => 'EURO 5',
        self::POLLUTION_EURO6   => 'EURO 6',
    ];

    /**
     * @set $class
     */
    public function __construct()
    {
        parent::__construct();

        $this->class = get_class();
    }

    /**
     * @param float|null $min
     * @param float|null $max
     * @return $this
     */
    public function wherePrice($min = null, $max = null)
    {
        if ($min > $max) {
            return $this;
        }

        if ($min) {
            $this->where('price >= ' . (float)$min);
        }

        if ($max) {
            $this->where('price <= ' . (float)$max);
        }

        return $this;
    }

    /**
     * @param int|null $min
     * @param int|null $max
     * @return $this
     */
    public function whereYear($min = null, $max = null)
    {
        if ($min > $max) {
            return $this;
        }

        if ($min) {
            $this->where('year >= ' . (int)$min);
        }

        if ($max) {
            $this->where('year <= ' . (int)$max);
        }

        return $this;
    }

    /**
     * @param int|null $value
     * @return $this
     */
    public function whereKm($value = null)
    {
        if ($value) {
            $this->where('kilometers <= ' . (int)$value);
        }

        return $this;
    }

    /**
     * @param array|null $values
     * @return $this
     */
    public function whereColor(array $values = null)
    {
        if (is_array($values) && !empty($values)) {
            $this->where('regular_color_id IN (' . implode(',', $values) . ')');
        }

        return $this;
    }

    /**
     * @param array $values
     * @return $this
     */
    public function whereBody(array $values = [])
    {
        if (is_array($values) && !empty($values)) {
            $this->where('body IN (' . implode(',', $values) . ')');
        }

        return $this;
    }

    /**
     * @param array $values
     * @return $this
     */
    public function whereFuel(array $values = [])
    {
        if (is_array($values) && !empty($values)) {
            $this->where('fuel IN (' . implode(',', $values) . ')');
        }

        return $this;
    }

    /**
     * @param int|null $min
     * @param int|null $max
     * @return $this
     */
    public function whereMotorisation($min = null, $max = null)
    {
        if ($min > $max) {
            return $this;
        }

        if ($min) {
            $this->where('motorisation >= ' . (float)$min);
        }

        if ($max) {
            $this->where('motorisation <= ' . (float)$max);
        }

        return $this;
    }

    /**
     * @param int|null $min
     * @param int|null $max
     * @return $this
     */
    public function whereHorsePower($min = null, $max = null)
    {
        if ($min > $max) {
            return $this;
        }

        if ($min) {
            $this->where('horse_power >= ' . (int)$min);
        }

        if ($max) {
            $this->where('horse_power <= ' . (int)$max);
        }

        return $this;
    }

    /**
     * @param array $values
     * @return $this
     */
    public function whereTraction(array $values = [])
    {
        if (is_array($values) && !empty($values)) {
            $this->where('traction IN (' . implode(',', $values) . ')');
        }

        return $this;
    }

    /**
     * @param array $values
     * @return $this
     */
    public function whereTransmission(array $values = [])
    {
        if (is_array($values) && !empty($values)) {
            $this->where('transmission IN (' . implode(',', $values) . ')');
        }

        return $this;
    }

    /**
     * @param array $values
     * @return $this
     */
    public function whereFeatures(array $values = [])
    {
        if (is_array($values) && !empty($values)) {
            $this->where("id IN (SELECT car_id FROM cars_features_ring WHERE feature_id IN (" . implode(',', $values) . ") GROUP BY car_id)");
        }

        return $this;
    }

    /**
     * @return void
     */
    protected function beforeSave()
    {
        parent::beforeSave();

        $this->params['url'] = Cms_Model_Page::getInstance()->load([
            'identifier' => 'buy'
        ])->getUri() . '/' . String::slugify($this->params['name']);
    }

    /**
     * @return array|mixed
     * @throws Exception
     */
    public function getFirstImage()
    {
        $firstImage = $this->getImages("first_image = 1");

        if ($firstImage->getCount() > 0) {
            return Arr::first($firstImage->getData());
        } else {
            $image = Car_Model_Car_Image::getInstance()->where([
                'car_id' => $this->getId()
            ])->order('ord', 'asc')->limit(1)->getCollection();

            if ($image->getCount() > 0) {
                return Arr::first($image->getData());
            }

            return null;
        }
    }

    /**
     * @return array|mixed
     * @throws Exception
     */
    public function getSecondImage()
    {
        $secondImage = $this->getImages("second_image = 1");

        if ($secondImage->getCount() > 0) {
            return Arr::first($secondImage->getData());
        } else {
            $image = Car_Model_Car_Image::getInstance()->where([
                'car_id' => $this->getId()
            ])->order('ord', 'asc')->limit(1, 1)->getCollection();

            if ($image->getCount() > 0) {
                return Arr::first($image->getData());
            }

            return null;
        }
    }

    /**
     * @return array|mixed
     * @throws Exception
     */
    public function getThirdImage()
    {
        $thirdImage = $this->getImages("third_image = 1");

        if ($thirdImage->getCount() > 0) {
            return Arr::first($thirdImage->getData());
        } else {
            $image = Car_Model_Car_Image::getInstance()->where([
                'car_id' => $this->getId()
            ])->order('ord', 'asc')->limit(1, 2)->getCollection();

            if ($image->getCount() > 0) {
                return Arr::first($image->getData());
            }

            return null;
        }
    }

    /**
     * @return int
     */
    public function getTaxesPrice()
    {
        return (int)Setting::get('car-registration-documents')->getValue() + (int)Setting::get('car-finishing-touches')->getValue();
    }

    /**
     * @return float
     */
    public function getVoucherPrice()
    {
        $price = $this->getPrice();

        if (Session::exists('shop-voucher') && Session::get('shop-voucher-car') == $this->getId()) {
            $voucher = Shop_Model_Order_Voucher::getInstance()->load(Session::get('shop-voucher'));

            switch ($voucher->getType()) {
                case Shop_Model_Order_Voucher::TYPE_FIXED:
                    $price -= $voucher->getRate();
                    break;
                case Shop_Model_Order_Voucher::TYPE_PERCENT:
                    $price -= ($voucher->getRate() / 100) * $price;
                    break;
            }
        }

        return $price;
    }

    /**
     * @return float
     */
    public function getVoucherDiscount()
    {
        $price = $this->getPrice();
        $voucherPrice = $this->getVoucherPrice();
        $value = '';

        if (Session::exists('shop-voucher') && Session::get('shop-voucher-car') == $this->getId()) {
            $voucher = Shop_Model_Order_Voucher::getInstance()->load(Session::get('shop-voucher'));

            switch ($voucher->getType()) {
                case Shop_Model_Order_Voucher::TYPE_FIXED:
                    $value = '&euro;' . $voucher->getRate();
                    break;
                case Shop_Model_Order_Voucher::TYPE_PERCENT:
                    $value = $voucher->getRate() . '%' . '(&euro;' . ($price - $voucherPrice) . ')';
                    break;
            }
        }

        return $value;
    }

    /**
     * @param int $deliveryPrice
     * @return mixed
     */
    public function getTotalPrice($deliveryPrice = 0)
    {
        if (Session::exists('shop-voucher') && Session::get('shop-voucher-car') == $this->getId()) {
            $carPrice   = $this->getPrice();
            $voucher    = Shop_Model_Order_Voucher::getInstance()->load(Session::get('shop-voucher'));

            switch ($voucher->getType()) {
                case Shop_Model_Order_Voucher::TYPE_FIXED:
                    $carPrice -= $voucher->getRate();
                    break;
                case Shop_Model_Order_Voucher::TYPE_PERCENT:
                    $carPrice -= ($voucher->getRate() / 100) * $carPrice;
                    break;
            }
        } else {
            $carPrice = $this->getPrice();
        }

        return $carPrice + $deliveryPrice + (int)Setting::get('car-registration-documents')->getValue() + (int)Setting::get('car-finishing-touches')->getValue();
    }

    /**
     * @return bool|void
     */
    public function deleteEntity()
    {
        if (Request::isGet() && Get::exists('delete-id') && Get::exists('delete-entity')) {
            $this->deleteUrl();

            parent::deleteEntity();
        }
    }

    /**
     * @return bool
     */
    public function deleteUrl()
    {
        if (!Database::getInstance('cms_urls')->delete()->where(['entity_id' => Get::get('delete-id'), 'entity' => Get::get('delete-entity')])->save()) {
            Flash::error('Could not delete the URL assigned for this car!');
            Redirect::url(Url::getPreviousUrl());
        }
    }
}