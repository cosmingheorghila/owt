<?php

class Car_Model_Model extends Cms_Model
{
    /**
     * @var string
     */
    protected $table = 'cars_models';

    /**
     * @var array
     */
    protected $validates = [
        'validation' => [
            'name' => [
                'presence',
            ],
        ],
    ];

    /**
     * @var array
     */
    protected $actsAs = [
        'draggable' => [
            'field' => 'brand_id',
        ],
    ];

    /**
     * @var array
     */
    protected $belongsTo = [
        'brand' => [
            'model'         => 'Car_Model_Brand',
            'remote_key'    => 'brand_id',
        ],
    ];

    /**
     * @var array
     */
    protected $hasMany = [
        'colors' => [
            'model'         => 'Car_Model_Color',
            'remote_key'    => 'model_id',
        ],
    ];

    /**
     * @set $class
     */
    public function __construct()
    {
        parent::__construct();

        $this->class = get_class();
    }

    /**
     * @return Collection
     */
    public function getAllColors()
    {
        return (new Collection)->merge(
            $this->getColors(),
            Car_Model_Color::getInstance()->where(
                "model_id IS NULL AND show_sell = '" . Car_Model_Color::SHOW_YES . "'"
            )->getCollection()
        );
    }

    /**
     * @return array
     */
    public static function toAutoComplete()
    {
        $model  = new Car_Model_Model();
        $models = [];

        $models['suggestions']  = [];
        $models['data']         = [];

        foreach ($model->where(['brand_id' => Get::get('brand_id')])->search(Get::get('query'), ['name'])->order('ord')->getCollection() as $model) {
            $models['suggestions'][] = [
                'id'    => $model->getId(),
                'value' => $model->getName(),
            ];

            $models['data'][] = [
                'id'    => $model->getId(),
                'value' => $model->getName(),
            ];
        }

        return $models;
    }
}