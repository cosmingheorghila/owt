<?php

class Car_Model_Certification extends Cms_Model
{
    /**
     * @var string
     */
    protected $table = 'cars_certifications';

    /**
     * @var array
     */
    protected $validates = [
        'validation' => [
            'name' => [
                'presence',
            ],
        ],
    ];

    /**
     * @var array
     */
    protected $actsAs = [
        'draggable',
        'timestamp',
    ];

    /**
     * @var array
     */
    protected $hasAndBelongsToMany = [
        'mechanics' => [
            'model'         => 'Car_Model_Mechanic',
            'table'         => 'cars_certifications_ring',
            'local_key'     => 'certification_id',
            'remote_key'    => 'mechanic_id',
            'order_field'   => 'ord',
            'order_dir'     => 'asc',
        ],
    ];

    /**
     * @set $class
     */
    public function __construct()
    {
        parent::__construct();

        $this->class = get_class();
    }
}