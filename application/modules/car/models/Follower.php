<?php

class Car_Model_Follower extends Cms_Model
{
    /**
     * @var string
     */
    protected $table = 'cars_followers';

    /**
     * @var array
     */
    protected $validates = [
        'validation' => [
            'email' => [
                'presence',
            ],
        ],
    ];

    /**
     * @var array
     */
    protected $actsAs = [
        'timestamp'
    ];

    /**
     * @var array
     */
    protected $belongsTo = [
        'car' => [
            'model'         => 'Car_Model_Car',
            'remote_key'    => 'car_id',
        ],
    ];

    /**
     * @set $class
     */
    public function __construct()
    {
        parent::__construct();

        $this->class = get_class();
    }
}