<?php

class Car_Model_Stat extends Cms_Model
{
    /**
     * @var string
     */
    protected $table = 'cars_stats';

    /**
     * @var array
     */
    protected $validates = [
        'validation' => [
            'name' => [
                'presence',
            ],
        ],
        'unique' => [
            'name',
        ],
    ];

    /**
     * @var array
     */
    protected $actsAs = [
        'draggable',
    ];

    /**
     * @const
     */
    const IMPORTANT_NO  = 0;
    const IMPORTANT_YES = 1;

    /**
     * @var array
     */
    public static $important = [
        self::IMPORTANT_NO  => 'No',
        self::IMPORTANT_YES => 'Yes',
    ];

    /**
     * @set $class
     */
    public function __construct()
    {
        parent::__construct();

        $this->class = get_class();
    }
}