<?php

class Car_SellController extends Cms_Controller
{
    /**
     * @return void
     */
    public function indexAction() {}

    /**
     * @return void
     */
    public function step1Action()
    {
        $adModel = new Car_Model_Ad();
        $ownerModel = new Car_Model_Ad_Owner();

        if ($this->getRequest()->isPost()) {
            $adData = Post::get('data');
            $ownerData = Post::get('owner');

            if ($ad = $adModel->saveData($adData, Get::exists('ad') ? Get::get('ad') : null)) {
                $ownerData['ad_id'] = $ad;

                if ($ownerModel->saveData($ownerData)) {
                    $mail = new Email('postmark');
                    $mail->from(
                        Setting::get('company-email')->getValue()
                    )->to(
                        Setting::get('company-email')->getValue()
                    )->subject(
                        'OWT-Garage - Un nou anunt a fost adaugat'
                    )->messageHtml(
                        'emails/new_ad.phtml', [
                            'theUrl' => Url::getAdminUrl() . '/car/ads/index',
                        ]
                    )->send();

                    setcookie('ad', $ad, time()+60*60*24*30);
                    Redirect::page('sell-step-2');
                }
            } else {
                Flash::error('Se pare ca a fost o problema. Incearca, te rugam, inca o data.');
                Redirect::url(Url::getPreviousUrl());
            }
        }

        $breadcrumbs = [
            0 => ['name' => 'Spune-ne despre masina ta', 'current' => true,],
            1 => ['name' => 'Programeaza inspectia masinii', 'current' => false,],
            2 => ['name' => 'Fii liber sa faci ce vrei', 'current' => false,],
        ];

        $this->assign('ad', Get::exists('ad') ? $adModel->load(Get::get('ad')) : null);
        $this->assign('owner', Get::exists('ad') ? $ownerModel->load(['ad_id' => Get::get('ad')]) : null);
        $this->assign('breadcrumbs', $breadcrumbs);
        $this->assign('form', new Form());
    }

    /**
     * @return void
     */
    public function step2Action()
    {
        $adModel = new Car_Model_Ad();
        $adParam = Cookie::exists('ad') ? (int)Cookie::get('ad') : null;

        if ($adParam && ($ad = $adModel->load($adParam))) {
            setcookie('ad', $adParam, time()+60*60*24*30);

            if ($this->getRequest()->isPost()) {
                $model  = new Car_Model_Ad_Stat();
                $data   = Post::get('data');

                $data['ad_id'] = $ad->getId();

                if ($model->saveData($data)) {
                    Redirect::page('sell-step-3');
                } else {
                    Redirect::page('sell-stat-error');
                }
            }

            $stats          = Car_Model_Stat::getInstance()->order('ord')->getCollection();
            $breadcrumbs    = [
                0 => ['name' => 'Spune-ne despre masina ta', 'current' => true,],
                1 => ['name' => 'Programeaza inspectia masinii', 'current' => false,],
                2 => ['name' => 'Fii liber sa faci ce vrei', 'current' => false,],
            ];

            $this->assign('ad', $ad);
            $this->assign('stats', $stats);
            $this->assign('breadcrumbs', $breadcrumbs);
            $this->assign('form', new Form());
        } else {
            Flash::error('Se pare ca anuntul a fost sters. Daca ai nevoie de ajutor, contacteaza-ne!');
            Redirect::page('home');
        }
    }

    /**
     * @return void
     */
    public function step3Action()
    {
        $adModel = new Car_Model_Ad();
        $adParam = Cookie::exists('ad') ? (int)Cookie::get('ad') : null;

        $selectedAddress = Car_Model_Ad_Address::getInstance()->load(['ad_id' => $adParam]);

        if ($adParam && ($ad = $adModel->load($adParam))) {
            setcookie('ad', $adParam, time()+60*60*24*30);

            if ($this->getRequest()->isPost()) {
                $addressModel   = new Car_Model_Ad_Address();
                $addressData    = Post::get('address');

                $addressData['ad_id'] = $ad->getId();

                if ($address = $addressModel->saveData($addressData)) {
                    Redirect::page('sell-step-4');
                } else {
                    Session::set('address', $addressData);
                    Redirect::url(Url::getPreviousUrl());
                }
            }

            $breadcrumbs    = [
                0 => ['name' => 'Spune-ne despre masina ta', 'current' => false,],
                1 => ['name' => 'Programeaza inspectia masinii', 'current' => true,],
                2 => ['name' => 'Fii liber sa faci ce vrei', 'current' => false,],
            ];

            $this->assign('ad', $ad);
            $this->assign('address', $selectedAddress ?: null);
            $this->assign('breadcrumbs', $breadcrumbs);
            $this->assign('form', new Form());
        } else {
            Flash::error('Se pare ca anuntul a fost sters. Daca ai nevoie de ajutor, contacteaza-ne!');
            Redirect::page('home');
        }
    }

    /**
     * @return void
     */
    public function step4Action()
    {
        $adModel = new Car_Model_Ad();
        $adParam = Cookie::exists('ad') ? (int)Cookie::get('ad') : null;

        $currentAvailabilities  = Car_Model_Ad_Availability::getInstance()->where(['ad_id' => $adParam])->getCollection();
        $selectedAvailabilities = [];

        if ($currentAvailabilities->getCount() > 0) {
            foreach ($currentAvailabilities as $availability) {
                $selectedAvailabilities[] = $availability->getMonth() . $availability->getDay() . $availability->getHour();
            }
        }

        if ($adParam && ($ad = $adModel->load($adParam))) {
            setcookie('ad', $adParam, time()+60*60*24*30);

            if ($this->getRequest()->isPost()) {
                $model  = new Car_Model_Ad_Availability();
                $data   = Post::get('data');

                if ($model->saveData($data, $ad->getId())) {
                    try {
                        $mail = new Email('postmark');
                        $mail->from(
                            Setting::get('company-email')->getValue()
                        )->to(
                            $ad->getOwner()->getEmail()
                        )->subject(
                            'OWT-Garage - Seria VIN'
                        )->messageHtml(
                            'emails/sell_vin.phtml', [
                                'theUrl' => Url::getHostUrl() . $this->page('sell-step-5')->getUrl(),
                                'theContactUrl' => Url::getHostUrl() . $this->page('support')->getUrl(),
                            ]
                        );

                        if ($mail->send()) {
                            Redirect::page('sell-step-5');
                        } else {
                            Flash::error('Nu ti-am putut trimite instructiunile pentru introducerea VIN-ului. Asigura-te ca datele tale au fost introduse corect.');
                            Redirect::page('sell-step-3');
                        }
                    } catch (\Postmark\Models\PostmarkException $e) {
                        $this->flashError('Te rugam introdu un email valid.');
                        $this->redirectUrl($this->page('sell-step-1')->getUrl());
                    }
                } else {
                    Redirect::url(Url::getPreviousUrl());
                }
            }

            $dateBegin      = new DateTime(date('Y-m-d', time() + (86400 * 2)));
            $dateEnd        = new DateTime(date('Y-m-d', strtotime('+7 days', time() + (86400 * 2))));
            $dateInterval   = DateInterval::createFromDateString('1 day');
            $datePeriod     = new DatePeriod($dateBegin, $dateInterval, $dateEnd);
            $breadcrumbs    = [
                0 => ['name' => 'Spune-ne despre masina ta', 'current' => false,],
                1 => ['name' => 'Programeaza inspectia masinii', 'current' => true,],
                2 => ['name' => 'Fii liber sa faci ce vrei', 'current' => false,],
            ];

            $this->assign('ad', $ad);
            $this->assign('period', $datePeriod);
            $this->assign('availabilities', $currentAvailabilities);
            $this->assign('selectedAvailabilities', $selectedAvailabilities);
            $this->assign('breadcrumbs', $breadcrumbs);
            $this->assign('form', new Form());
        } else {
            Flash::error('Se pare ca anuntul a fost sters. Daca ai nevoie de ajutor, contacteaza-ne!');
            Redirect::page('home');
        }
    }

    /**
     * @return void
     */
    public function step5Action()
    {
        $adModel = new Car_Model_Ad();
        $adParam = Cookie::exists('ad') ? (int)Cookie::get('ad') : null;

        if ($adParam && ($ad = $adModel->load($adParam))) {
            setcookie('ad', $adParam, time()+60*60*24);

            if ($this->getRequest()->isPost()) {
                if ($ad->validateVin(Post::get('vin'))) {
                    $ad->update([
                        'vin'       => Post::get('vin'),
                        'active'    => Car_Model_Ad::ACTIVE_YES
                    ])->where([
                        'id' => $ad->getId()
                    ])->save();

                    unset($_COOKIE['ad']);

                    $this->redirectUrl($this->page('sell-success')->getUrl());
                }

                $this->redirectUrl($this->page('sell-vin-error')->getUrl());
            }

            $breadcrumbs    = [
                0 => ['name' => 'Spune-ne despre masina ta', 'current' => false,],
                1 => ['name' => 'Programeaza inspectia masinii', 'current' => false,],
                2 => ['name' => 'Fii liber sa faci ce vrei', 'current' => true,],
            ];

            $this->assign('ad', $ad);
            $this->assign('breadcrumbs', $breadcrumbs);
        } else {
            Flash::error('Se pare ca anuntul a fost sters. Daca ai nevoie de ajutor, contacteaza-ne!');
            Redirect::page('home');
        }
    }

    /**
     * @return void
     */
    public function successAction()
    {
        $breadcrumbs = [
            0 => ['name' => 'Spune-ne despre masina ta', 'current' => false,],
            1 => ['name' => 'Programeaza inspectia masinii', 'current' => false,],
            2 => ['name' => 'Fii liber sa faci ce vrei', 'current' => true,],
        ];

        $this->assign('breadcrumbs', $breadcrumbs);
    }

    /**
     * @return void
     */
    public function vinErrorAction()
    {
        $breadcrumbs = [
            0 => ['name' => 'Spune-ne despre masina ta', 'current' => false,],
            1 => ['name' => 'Programeaza inspectia masinii', 'current' => false,],
            2 => ['name' => 'Fii liber sa faci ce vrei', 'current' => true,],
        ];

        $this->assign('breadcrumbs', $breadcrumbs);
    }

    /**
     * @return void
     */
    public function cityErrorAction()
    {
        if ($this->getRequest()->isPost()) {
            $model  = new Cms_Model_Newsletter_Subscriber();
            $data   = Post::get('data');

            if ($model->subscribe($data['email'])) {
                Flash::success('Imediat ce vom ajunge acolo, iti vom da de veste!');
                Redirect::page('home');
            } else {
                Redirect::url(Url::getPreviousUrl());
            }
        }

        $breadcrumbs = [
            0 => ['name' => 'Spune-ne despre masina ta', 'current' => false,],
            1 => ['name' => 'Programeaza inspectia masinii', 'current' => false,],
            2 => ['name' => 'Fii liber sa faci ce vrei', 'current' => true,],
        ];

        $this->assign('breadcrumbs', $breadcrumbs);
    }

    /**
     * @return void
     */
    public function kmErrorAction()
    {
        $breadcrumbs = [
            0 => ['name' => 'Spune-ne despre masina ta', 'current' => false,],
            1 => ['name' => 'Programeaza inspectia masinii', 'current' => false,],
            2 => ['name' => 'Fii liber sa faci ce vrei', 'current' => true,],
        ];

        $this->assign('breadcrumbs', $breadcrumbs);
    }

    /**
     * @return void
     */
    public function statErrorAction()
    {
        $breadcrumbs = [
            0 => ['name' => 'Spune-ne despre masina ta', 'current' => false,],
            1 => ['name' => 'Programeaza inspectia masinii', 'current' => false,],
            2 => ['name' => 'Fii liber sa faci ce vrei', 'current' => true,],
        ];

        $this->assign('breadcrumbs', $breadcrumbs);
    }

    /**
     * @return void
     */
    public function getBrandsAction()
    {
        $this->disableLayout();
        $this->disableView();

        echo json_encode(Car_Model_Brand::toAutoComplete());die;
    }

    /**
     * @return void
     */
    public function getModelsAction()
    {
        $this->disableLayout();
        $this->disableView();

        echo json_encode(Car_Model_Model::toAutoComplete());die;
    }

    /**
     * @return void
     */
    public function getCitiesAction()
    {
        $this->disableLayout();
        $this->disableView();

        echo json_encode(Default_Model_City::toAutoComplete());die;
    }

    /**
     * @return void
     */
    public function getColorsAction()
    {
        $this->disableLayout();
        $this->disableView();

        if ($model = Car_Model_Model::getInstance()->load(Get::get('model_id'))) {
            $colors = $model->getAllColors();

            if ($colors->getCount() > 0) {
                $content = '';

                foreach ($colors as $color) {
                    $content .= '<a href="#" data-color="' . $color->getId() . '" class="car-color"><div class="color-icon" style="background-color: ' . $color->getColor() . ';"></div><br />' . $color->getName() . '</a>';
                }
            } else {
                $content = '<p>Nu exista culori disponibile pentru aceasta marca de masina</p>';
            }

            echo json_encode(['status' => true, 'content' => $content]);die;
        } else {
            echo json_encode(['status' => false]);die;
        }
    }
}