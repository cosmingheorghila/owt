<?php

class Car_BuyController extends Cms_Controller
{
    /**
     * @return void
     */
    public function indexAction()
    {
        $car    = new Car_Model_Car();
        $other  = new Car_Model_Car();

        if (Get::exists('cauta')) {
            $car->search(Get::get('cauta'), ['name', 'vin']);
        }

        if (Get::exists('pret-min') || Get::exists('pret-max')) {
            $car->wherePrice(Get::get('pret-min'), Get::get('pret-max'));
        }

        if (Get::exists('an-min') || Get::exists('an-max')) {
            $car->whereYear(Get::get('an-min'), Get::get('an-max'));
        }

        if (Get::exists('kilometri')) {
            $car->whereKm(Get::get('kilometri'));
        }

        if (Get::exists('culoare')) {
            $car->whereColor(Get::get('culoare'));
        }

        if (Get::exists('caroserie')) {
            $car->whereBody(Get::get('caroserie'));
        }

        if (Get::exists('combustibil')) {
            $car->whereFuel(Get::get('combustibil'));
        }
        
        if (Get::exists('motorizare-min') || Get::exists('motorizare-max')) {
            $car->whereMotorisation(Get::get('motorizare-min'), Get::get('motorizare-max'));
        }

        if (Get::exists('cai-min') || Get::exists('cai-max')) {
            $car->whereHorsePower(Get::get('cai-min'), Get::get('cai-max'));
        }

        if (Get::exists('tractiune')) {
            $car->whereTraction(Get::get('tractiune'));
        }

        if (Get::exists('transmisie')) {
            $car->whereTransmission(Get::get('transmisie'));
        }

        if (Get::exists('dotari')) {
            $car->whereFeatures(Get::get('dotari'));
        }

        $car->where([
            'active' => Car_Model_Car::ACTIVE_YES
        ])->order('ord');

        $cars = $car->getCollection();

        if ($cars->getCount() < 9) {
            $recommendations = $other->where([
                'active' => Car_Model_Car::ACTIVE_YES
            ])->order('ord')->limit(27)->getCollection();

            $recommendations = (new Collection())->diff($recommendations, $cars);
        } else {
            $recommendations = [];
        }

        $this->assign([
            'cars'      => $cars,
            'other'     => $recommendations,
            'features'  => Car_Model_Feature::getInstance()->where(['show_in_filters' => Car_Model_Feature::FILTER_YES])->order('ord')->getCollection(),
            'colors'    => Car_Model_Color::getInstance()->where("model_id IS NULL")->order('ord')->getCollection(),
            'form'      => new Form(),
        ]);
    }

    /**
     * @return void
     */
    public function viewAction()
    {
        $model  = new Car_Model_Car();
        $id     = $this->getParam('id');
        
        if ($id && $car = $model->load($id)) {
            $this->setLayout('default/car.phtml');

            $this->setMeta('title', $car->getMetaTitle() ? strip_tags($car->getMetaTitle()) : strip_tags($car->getName()));
            $this->setMeta('description', $car->getMetaDescription() ? strip_tags($car->getMetaDescription()) : strip_tags($car->getDescription()));
            $this->setMeta('keywords', $car->getMetaKeywords() ? strip_tags($car->getMetaKeywords()) : '');

            $this->assign([
                'car'       => $car,
                'brand'     => $car->getBrand(),
                'model'     => $car->getModel(),
                'mechanic'  => $car->getMechanic(),
                'images'    => $car->getImages(),
                'features'  => $car->getFeatures(),
                'options'   => $car->getOptions(),
                'perks'     => $car->getPerks(),
                'e_color'   => $car->getExteriorColor(),
                'i_color'   => $car->getInteriorColor(),
                'form'      => new Form(),
            ]);
        } else {
            $this->redirectUrl(Url::getHomeUrl());
        }
    }

    /**
     * @return void
     */
    public function successAction()
    {
        $id = Get::get('masina');
        $model = new Car_Model_Car();

        if ($id && ($car = $model->load($id))) {
            $this->assign([
                'car'       => $car,
                'brand'     => $car->getBrand(),
                'model'     => $car->getModel(),
                'images'    => $car->getImages(),
                'perks'     => $car->getPerks(),
                'form'      => new Form(),
            ]);
        } else {
            $this->redirectUrl(Url::getHomeUrl());
        }
    }

    /**
     * @return void
     */
    public function galleryAction()
    {
        $model  = new Car_Model_Car();
        $id     = Get::exists('car') ? Get::get('car') : null;

        if ($id && $car = $model->load($id)) {
            $images = $car->getImages();

            if ($images->getCount() > 0) {
                $this->assign([
                    'car' => $car,
                    'images' => $images,
                    'first' => $car->getFirstImage(),
                ]);
            } else {
                $this->redirectUrl(Url::getPreviousUrl());
            }
        } else {
            $this->redirectUrl(Url::getHomeUrl());
        }
    }

    /**
     * @return void
     */
    public function followAction()
    {
        $id = Post::exists('car') ? Post::get('car') : null;

        if ($id && $car = Car_Model_Car::getInstance()->load($id)) {
            $model  = new Car_Model_Follower();
            $data   = Post::get('data');

            if (!$data['email']) {
                $this->flashError('Ceva este in neregula cu adresa ta de email. Incearca sa o reintroduci!!');
                $this->redirectUrl($car->getUrl());
            }

            try {
                if ($model->where(['email' => $data['email']])->getCollection()->getCount() > 0) {
                    $this->flashError('Stim, o iubesti! Dar aceasta masina o urmaresti deja :)');
                    $this->redirectUrl($car->getUrl());
                }

                $model->merge('insert', [
                    'car_id'    => $car->getId(),
                    'email'     => $data['email']
                ]);

                $this->flashSuccess('Super! Imediat ce avem noutati, iti dam de veste!');
                $this->redirectUrl($car->getUrl());
            } catch (Exception $e) {
                $this->flashError('Ceva este in neregula cu adresa ta de email.');
                $this->redirectUrl($car->getUrl());
            }
        } else {
            $this->flashError('Hmm…ceva se pare ca nu este in regula. Contacteaza-ne!');
            $this->redirectUrl(Url::getHomeUrl());
        }
    }

    /**
     * @return void
     */
    public function reportAction()
    {
        if ($this->getRequest()->isPost()) {
            $model  = new Car_Model_Car();
            $data   = Post::get('data');

            if (isset($data['car']) && $car = $model->load($data['car'])) {
                if (!isset($data['email']) || !filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
                    $this->flashError('Adresa ta de email nu pare valida. Incearca sa o reintroduci!');
                    $this->redirectUrl(Url::getPreviousUrl());
                }

                if (!$car->getReport()) {
                    $this->flashError('Momentan nu exista un raport legat de istoricul acestei masini.');
                    $this->redirectUrl(Url::getPreviousUrl());
                }

                $mail = new Email('postmark');
                $mail
                    ->from(Setting::get('company-email')->getValue())
                    ->to($data['email'])
                    ->subject('Car Report - ' . $car->getName())
                    ->messageHtml('emails/car_report.phtml', ['theCarName' => $car->getName()])
                    ->attachment(Url::getHostUrl() . Dir::getUploads() . '/file/' . $car->getReport(), 'Report - ' . $car->getName());

                if ($mail->send()) {
                    $this->flashSuccess('Ti-am trimis un email ce contine raportul masinii. Il gasesti chiar acum, in inbox-ul tau.');
                } else {
                    $this->flashError('Din pacate, raportul nu a fost trimis. Incearca putin mai tarziu.');
                }

                $this->redirectUrl($car->getUrl());
            } else {
                $this->redirectUrl(Url::getPreviousUrl());
            }
        } else {
            $this->redirectUrl(Url::getHomeUrl());
        }
    }

    /**
     * @return void
     */
    public function askAction()
    {
        $id = Post::exists('car') ? Post::get('car') : null;

        if ($id && $car = Car_Model_Car::getInstance()->load($id)) {
            $mail = new Email('postmark');
            $data = Post::get('data');

            if (!$data['email']) {
                $this->flashError('Ceva este in neregula cu adresa ta de email. Incearca sa o reintroduci!');
                $this->redirectUrl($car->getUrl());
            }

            if (!$data['phone']) {
                $this->flashError('Se pare ca ai uitat sa introduci numarul de telefon.');
                $this->redirectUrl($car->getUrl());
            }

            if (!$data['message']) {
                $this->flashError('Se pare ca ai uitat sa ne trimiti intrebarea ta.');
                $this->redirectUrl($car->getUrl());
            }

            $mail
                ->from(Setting::get('company-email')->getValue())
                ->to([$car->getMechanic()->getEmail(), Setting::get('company-email')->getValue()])
                ->subject('Car Question - ' . $car->getName())
                ->messageHtml('emails/car_question.phtml', [
                    'theCarName'    => $car->getName(),
                    'theCarUrl'     => Url::getHostUrl() . $car->getUrl(),
                    'theEmail'      => $data['email'],
                    'thePhone'      => $data['phone'],
                    'theMessage'    => $data['message'],
                ]);

            if ($mail->send()) {
                $this->flashSuccess('Am primit intrebarea ta. Ofera-ne cateva ore pentru a-ti raspunde.');
            } else {
                $this->flashError('Din pacate, mesajul nu a fost trimis. Incearca putin mai tarziu.');
            }

            $this->redirectUrl($car->getUrl());
        } else {
            $this->flashError('Hmm…ceva se pare ca nu este in regula. Contacteaza-ne!');
            $this->redirectUrl(Url::getHomeUrl());
        }
    }
}