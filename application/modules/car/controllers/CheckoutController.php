<?php

require_once(Dir::getLibrary() . '/main/shop/clients/Mobilpay/Payment/Request/Abstract.php');
require_once(Dir::getLibrary() . '/main/shop/clients/Mobilpay/Payment/Request/Card.php');
require_once(Dir::getLibrary() . '/main/shop/clients/Mobilpay/Payment/Request/Notify.php');
require_once(Dir::getLibrary() . '/main/shop/clients/Mobilpay/Payment/Invoice.php');
require_once(Dir::getLibrary() . '/main/shop/clients/Mobilpay/Payment/Address.php');

class Car_CheckoutController extends Cms_Controller
{
    /**
     * @var
     */
    public $car;

    /**
     * @var array
     */
    public $freeActions = [
        'confirmAction',
        'returnAction',
        'getCountiesAction',
        'getCitiesAction'
    ];

    /**
     * @return void
     */
    public function init()
    {
        if ($this->getRequest()->isPost()) {
            $data = Post::get('data');

            if (isset($data['car_id'])) {
                $this->car = Car_Model_Car::getInstance()->load($data['car_id']);
            }

            if (isset($data['voucher'])) {
                Shop_Model_Order_Voucher::getInstance()->apply(
                    $data['voucher'], Car_Model_Car::getInstance()->load($this->car->getId())
                );
            }
        } else {
            $this->flashError('Nu ne dam seama ce incerci sa faci. Contacteaza-ne!');
            $this->redirectUrl(Url::getHomeUrl());
        }

        $this->assign([
            'car'       => $this->car,
            'brand'     => $this->car->getBrand(),
            'model'     => $this->car->getModel(),
            'images'    => $this->car->getImages(),
            'perks'     => $this->car->getPerks(),
            'form'      => new Form(),
        ]);
    }

    /**
     * @return void
     */
    public function step1Action()
    {
        $this->check();
    }

    /**
     * @return void
     */
    public function step2Action()
    {
        $this->check();

        if ($this->getRequest()->isPost()) {
            if (!Car_Model_Order::step1($this->car)) {
                $this->flashError('Se pare ca a fost o problema. Incearca mai tarziu sau contacteaza-ne!');
                $this->redirectUrl($this->car->getUrl());
            }
        }
    }

    /**
     * @return void
     */
    public function step3Action()
    {
        $this->check();

        $order = $county = null;

        if ($this->getRequest()->isPost()) {
            $data = Post::get('data');

            if (!isset($data['county_id'])) {
                $this->flashError('Se pare ca a fost o problema. Incearca mai tarziu sau contacteaza-ne!');
                $this->redirectUrl($this->car->getUrl());
            }

            if (Car_Model_Order::step2($this->car, $data)) {
                if (Session::exists('orders') && is_array(Session::get('orders')) && !empty(Session::get('orders'))) {
                    foreach (Session::get('orders') as $session) {
                        if ($session['car_id'] == $data['car_id'] && $session['user_id'] == session_id()) {
                            $order = Car_Model_Order::getInstance()->load([
                                'car_id'    => $session['car_id'],
                                'user_id'   => $session['user_id'],
                            ]);
                        }
                    }
                }

                $county = Default_Model_County::getInstance()->load($data['county_id']);
            } else {
                $this->flashError('Se pare ca a fost o problema. Incearca mai tarziu sau contacteaza-ne!');
                $this->redirectUrl($this->car->getUrl());
            }
        }

        $this->assign('county', $county ?: null);
        $this->assign('order', $order ?: null);
    }

    /**
     * @return void
     */
    public function payAction()
    {
        $this->disableLayout();

        if ($this->getRequest()->isPost()) {
            $data = Post::get('data');

            if (!isset($data['order'])) {
                $this->flashError('Se pare ca a fost o problema. Incearca mai tarziu sau contacteaza-ne!');
                $this->redirectUrl($this->car->getUrl());
            }

            if ($order = Car_Model_Order::getInstance()->load($data['order'])) {
                $payment = new Shop_Payment_MobilPay(
                    $this->page('checkout-confirm')->getUrl(),
                    $this->page('checkout-return')->getUrl()
                );

                $payment->pay($order->getIdentifier(), array(
                    'amount'    => number_format(Shop_Model_Currency::convert($this->car->getPayNow(), 'EUR', 'RON'), 2, '.', ''),
                    'currency'  => 'RON',
                    'details'   => ''
                ), array(
                    'type'          => 'person',
                    'first_name'    => $order->getFirstName(),
                    'last_name'     => $order->getLastName(),
                    'address'       => $order->getCounty() . ' ' . $order->getCity(),
                    'email'         => $order->getEmail(),
                    'phone'         => $order->getPhone(),
                ), array(
                    'type'          => 'person',
                    'first_name'    => $order->getFirstName(),
                    'last_name'     => $order->getLastName(),
                    'address'       => $order->getCounty() . ' ' . $order->getCity(),
                    'email'         => $order->getEmail(),
                    'phone'         => $order->getPhone()
                ));

                $this->assign(array(
                    'url'   => $payment->paymentUrl,
                    'key'   => $payment->client->getEnvKey(),
                    'data'  => $payment->client->getEncData()
                ));
            }
        }
    }

    /**
     * @return void
     */
    public function returnAction()
    {
        $this->disableLayout();
        $this->disableView();

        $db = new Database('cars_orders');

        if ($_GET['orderId'] && ($item = $db->load(array('identifier' => $_GET['orderId'])))) {
            if ($item->getPayment() == Car_Model_Order::PAYMENT_CONFIRMED) {
                $this->redirectPage('buy-success', ['masina' => $item->getCarId()]);
            } elseif ($item->getPayment() == Car_Model_Order::PAYMENT_PENDING || $item->getPayment() == Car_Model_Order::PAYMENT_OPEN) {
                $this->redirectPage('buy-success', ['masina' => $item->getCarId()]);
            } elseif ($item->getMessage() == Car_Model_Order::PAYMENT_REFUNDED) {
                $this->flashError('Tranzactia a fost rambursata! --- ' . $item->getMessage());
            } elseif ($item->getPayment() == Car_Model_Order::PAYMENT_REJECTED || $item->getPayment() == Car_Model_Order::PAYMENT_CANCELED) {
                $this->flashError('Plata nu a putut fi procesata! --- ' . $item->getMessage());
            }
        } else {
            $this->flashError('Plata nu a putut fi procesata!');
        }

        $this->redirectPage('home');
    }

    /**
     * @return void
     */
    public function confirmAction()
    {
        $this->disableLayout();
        $this->disableView();

        $database   = new Database('cars_orders');
        $data       = array();

        if (strcasecmp($_SERVER['REQUEST_METHOD'], 'post') == 0) {
            if(isset($_POST['env_key']) && isset($_POST['data'])) {
                try {
                    $objPmReq   = Mobilpay_Payment_Request_Abstract::factoryFromEncrypted($_POST['env_key'], $_POST['data'], Dir::getLibrary() . '/main/shop/clients/Mobilpay/' . Shop_Payment_MobilPay::$environment . '_private.key');
                    $errorCode  = $objPmReq->objPmNotify->errorCode;

                    $data['identifier'] = $objPmReq->orderId;

                    if ($errorCode == "0") {
                        switch($objPmReq->objPmNotify->action) {
                            case 'confirmed':
                                if ($order = Car_Model_Order::getInstance()->load(['identifier' => $data['identifier']])) {
                                    Session::disable('shop-voucher');
                                    Session::disable('shop-voucher-car');

                                    if ($car = Car_Model_Car::getInstance()->load($order->getCarId())) {
                                        $database->setTable('cars')->update([
                                            'active' => Car_Model_Car::ACTIVE_NO
                                        ])->where([
                                            'id' => $car->getId()
                                        ])->save();

                                        $database->setTable('cars_orders')->update([
                                            'paid'      => $car->getPayNow(),
                                            'remaining' => $order->getRemaining() - $car->getPayNow(),
                                            'status'    => Car_Model_Order::STATUS_PENDING,
                                            'active'    => Car_Model_Order::ACTIVE_YES,
                                        ])->where([
                                            'identifier' => $data['identifier']
                                        ])->save();
                                    }
                                }
                                
                                $data['payment'] = Car_Model_Order::PAYMENT_CONFIRMED;
                                $data['message'] = $objPmReq->objPmNotify->errorMessage;
                                $errorMessage = $objPmReq->objPmNotify->errorMessage;
                                break;
                            case 'confirmed_pending':
                                $data['payment'] = Car_Model_Order::PAYMENT_PENDING;
                                $data['message'] = $objPmReq->objPmNotify->errorMessage;
                                $errorMessage   = $objPmReq->objPmNotify->errorMessage;
                                break;
                            case 'paid_pending':
                                $data['payment'] = Car_Model_Order::PAYMENT_PENDING;
                                $data['message'] = $objPmReq->objPmNotify->errorMessage;
                                $errorMessage   = $objPmReq->objPmNotify->errorMessage;
                                break;
                            case 'paid':
                                $data['payment'] = Car_Model_Order::PAYMENT_OPEN;
                                $data['message'] = $objPmReq->objPmNotify->errorMessage;
                                $errorMessage   = $objPmReq->objPmNotify->errorMessage;
                                break;
                            case 'canceled':
                                $data['payment'] = Car_Model_Order::PAYMENT_CANCELED;
                                $data['message'] = $objPmReq->objPmNotify->errorMessage;
                                $errorMessage   = $objPmReq->objPmNotify->errorMessage;
                                break;
                            case 'credit':
                                $data['payment'] = Car_Model_Order::PAYMENT_REFUNDED;
                                $data['message'] = $objPmReq->objPmNotify->errorMessage;
                                $errorMessage   = $objPmReq->objPmNotify->errorMessage;
                                break;
                            default:
                                $errorType		= Mobilpay_Payment_Request_Abstract::CONFIRM_ERROR_TYPE_PERMANENT;
                                $errorCode 		= Mobilpay_Payment_Request_Abstract::ERROR_CONFIRM_INVALID_ACTION;
                                $errorMessage 	= 'mobilpay_refference_action paramaters is invalid';
                                break;
                        }
                    } else {
                        $data['payment'] = Car_Model_Order::PAYMENT_REJECTED;
                        $data['message'] = $objPmReq->objPmNotify->errorMessage;
                        $errorMessage   = $objPmReq->objPmNotify->errorMessage;
                    }
                } catch(Exception $e) {
                    $errorType 		= Mobilpay_Payment_Request_Abstract::CONFIRM_ERROR_TYPE_TEMPORARY;
                    $errorCode		= $e->getCode();
                    $errorMessage 	= $e->getMessage();
                }
            } else {
                $errorType 		= Mobilpay_Payment_Request_Abstract::CONFIRM_ERROR_TYPE_PERMANENT;
                $errorCode		= Mobilpay_Payment_Request_Abstract::ERROR_CONFIRM_INVALID_POST_PARAMETERS;
                $errorMessage 	= 'MobilPay.ro posted invalid parameters';
            }
        } else {
            $errorType 		= Mobilpay_Payment_Request_Abstract::CONFIRM_ERROR_TYPE_PERMANENT;
            $errorCode		= Mobilpay_Payment_Request_Abstract::ERROR_CONFIRM_INVALID_POST_METHOD;
            $errorMessage 	= 'Invalid request method for payment confirmation';
        }

        header('Content-type: application/xml');

        echo "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";

        if($errorCode == 0) {
            echo "<crc>{$errorMessage}</crc>";
        } else {
            echo "<crc error_type=\"{$errorType}\" error_code=\"{$errorCode}\">{$errorMessage}</crc>";
        }

        if (isset($data['identifier'])) {
            if ($database->select()->where(array('identifier' => $data['identifier']))->fetch()->rowCount() > 0) {
                $database->update(['payment' => $data['payment'] ?: '', 'message' => $data['message'] ?: ''])->where(['identifier' => $data['identifier']])->save();
            } else {
                if (!empty($data)) {
                    $database->insert($data)->save();
                }
            }
        }
    }

    /**
     * return void
     */
    public function getCountiesAction()
    {
        $this->disableLayout();
        $this->disableView();

        echo json_encode(Default_Model_County::toAutoComplete());die;
    }

    /**
     * return void
     */
    public function getCitiesAction()
    {
        $this->disableLayout();
        $this->disableView();

        echo json_encode(Default_Model_City::toAutoComplete(Get::exists('county_id') ? Get::get('county_id') : null));die;
    }

    /**
     * @return void
     */
    private function check()
    {
        if (!$this->car) {
            $this->redirectUrl(Url::getHomeUrl());
        }
    }
}