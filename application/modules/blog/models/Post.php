<?php

class Blog_Model_Post extends Cms_Model
{
    /**
     * @var string
     */
    protected $table = 'blog_posts';

    /**
     * @var array
     */
    protected $validates = [
        'validation' => [
            'name' => [
                'presence',
            ],
            'content' => [
                'presence',
            ],
        ],
        'unique' => [
            'name',
        ],
    ];

    /**
     * @var array
     */
    protected $actsAs = [
        'timestamp',
        'url'
    ];

    /**
     * @var array
     */
    protected $belongsTo = [
        'author' => [
            'model'         => 'Blog_Model_Author',
            'remote_key'    => 'author_id',
        ],
    ];

    /**
     * @var array
     */
    protected $hasMany = [
        /*'comments' => [
            'model'         => 'Blog_Model_Post_Comment',
            'remote_key'    => 'post_id',
            'order_field'   => 'updated_at',
            'order_dir'     => 'desc',
        ],*/
        'media' => [
            'model'         => 'Blog_Model_Post_Media',
            'remote_key'    => 'post_id',
            'order_field'   => 'ord',
            'order_dir'     => 'asc',
        ],
    ];

    /**
     * @var array
     */
    /*protected $hasAndBelongsToMany = [
        'categories' => [
            'model'         => 'Blog_Model_Category',
            'table'         => 'blog_posts_categories_ring',
            'local_key'     => 'post_id',
            'remote_key'    => 'category_id',
            'order_field'   => 'ord',
            'order_dir'     => 'asc',
            'enable_rel'    => true,
        ],
    ];*/

    /**
     * @set $class
     */
    public function __construct()
    {
        parent::__construct();

        $this->class = get_class();
    }

    /**
     * @return void
     */
    protected function beforeSave()
    {
        parent::beforeSave();

        $this->params['url'] = Cms_Model_Page::getInstance()->load([
            'identifier' => 'blog'
        ])->getUri() . '/' . String::slugify($this->params['name']);
    }

    /**
     * @param int $limit
     * @return array
     */
    public static function getLatestPosts($limit = 10)
    {
        return self::getInstance()->order('updated_at', 'desc')->limit($limit)->getCollection();
    }

    /**
     * @return mixed
     */
    public function getFirstImage()
    {
        return $this->getMedia(['type' => 'image'])->first();
    }

    /**
     * @return mixed
     */
    public function getFirstCategory()
    {
        return Arr::first(Blog_Model_Category::getInstance()->where(
            "id IN (SELECT category_id FROM blog_posts_categories_ring WHERE post_id = {$this->getId()})"
        )->order('ord', 'asc')->limit(1)->getCollection());
    }

    /**
     * @return bool|void
     */
    public function deleteEntity()
    {
        if (Request::isGet() && Get::exists('delete-id') && Get::exists('delete-entity')) {
            $this->deleteUrl();

            parent::deleteEntity();
        }
    }

    /**
     * @return bool
     */
    public function deleteUrl()
    {
        if (!Database::getInstance('cms_urls')->delete()->where(['entity_id' => Get::get('delete-id'), 'entity' => Get::get('delete-entity')])->save()) {
            Flash::error('Could not delete the URL assigned for this blog post!');
            Redirect::url(Url::getPreviousUrl());
        }
    }
}