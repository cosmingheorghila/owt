<?php

class Blog_Model_Author extends Cms_Model
{
    /**
     * @var string
     */
    protected $table = 'blog_authors';

    /**
     * @var array
     */
    protected $validates = [
        'validation' => [
            'name' => [
                'presence',
            ],
        ],
    ];

    /**
     * @var array
     */
    protected $hasMany = [
        'posts' => [
            'model'         => 'Blog_Model_Post',
            'remote_key'    => 'author_id',
            'order_field'   => 'updated_at',
            'order_dir'     => 'desc',
        ],
    ];

    /**
     * @set $class
     */
    public function __construct()
    {
        parent::__construct();

        $this->class = get_class();
    }
}