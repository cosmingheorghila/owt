<?php

class Blog_Model_Post_Media extends Cms_Model
{
    /**
     * @var string
     */
    protected $table = 'blog_posts_media';

    /**
     * @var array
     */
    protected $validates = [
        'validation' => [
            'type' => [
                'presence',
            ],
        ],
    ];

    /**
     * @var array
     */
    protected $actsAs = [
        'draggable'
    ];

    /**
     * @var array
     */
    protected $belongsTo = [
        'product' => [
            'model'         => 'Blog_Model_Post',
            'remote_key'    => 'post_id',
        ],
    ];

    /**
     * @const
     */
    const TYPE_IMAGE    = 'image';
    const TYPE_VIDEO    = 'video';
    const TYPE_AUDIO    = 'audio';
    const TYPE_FILE     = 'file';

    /**
     * @var array
     */
    public static $types = [
        self::TYPE_IMAGE    => 'Image',
        self::TYPE_VIDEO    => 'Video',
        self::TYPE_AUDIO    => 'Audio',
        self::TYPE_FILE     => 'File',
    ];

    /**
     * @set $class
     */
    public function __construct()
    {
        parent::__construct();

        $this->class = get_class();
    }

    /**
     * @return void
     */
    protected function beforeSave()
    {
        parent::beforeSave();

        $this->params['post_id']  = Get::get('post');
    }

    /**
     * @return null|string
     */
    public function showAdminMedia()
    {
        switch ($this->getType()) {
            case self::TYPE_IMAGE:
                return Image::display($this->getMedia(), 'default', ['style' => 'max-width: 150px; max-height: 100px;']);
                break;
            case self::TYPE_VIDEO:
                return Video::display($this->getMedia(), 150, 100);
                break;
            case self::TYPE_AUDIO:
                return Audio::display($this->getMedia(), ['style' => 'max-width: 150px; max-height: 100px;']);
                break;
            case self::TYPE_FILE:
                return Document::display($this->getMedia(), ['class' => 'btn black view right']);
                break;
            default:
                return null;
                break;
        }
    }

    /**
     * @return null|string
     */
    public function showMedia()
    {
        switch ($this->getType()) {
            case self::TYPE_IMAGE:
                return Image::display($this->getMedia());
                break;
            case self::TYPE_VIDEO:
                return Video::display($this->getMedia());
                break;
            case self::TYPE_AUDIO:
                return Audio::display($this->getMedia());
                break;
            case self::TYPE_FILE:
                return Document::display($this->getMedia());
                break;
            default:
                return null;
                break;
        }
    }
}