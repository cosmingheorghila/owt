<?php

class Blog_Model_Post_Comment extends Cms_Model
{
    /**
     * @var string
     */
    protected $table = 'blog_posts_comments';

    /**
     * @var array
     */
    protected $validates = [
        'validation' => [
            'name' => [
                'presence',
            ],
            'content' => [
                'presence',
            ],
        ],
    ];

    /**
     * @var array
     */
    protected $actsAs = [
        'timestamp',
    ];

    /**
     * @var array
     */
    protected $belongsTo = [
        'post' => [
            'model'         => 'Blog_Model_Post',
            'remote_key'    => 'post_id',
        ],
    ];

    /**
     * @const
     */
    const FLAGGED_NO    = 0;
    const FLAGGED_YES   = 1;

    /**
     * @var array
     */
    public static $flags = [
        self::FLAGGED_NO    => 'No',
        self::FLAGGED_YES   => 'Yes',
    ];

    /**
     * @set $class
     */
    public function __construct()
    {
        parent::__construct();

        $this->class = get_class();
    }

    /**
     * @return bool
     */
    public function flag()
    {
        return $this->merge('update', [
            'flagged' => self::FLAGGED_YES
        ]);
    }
}