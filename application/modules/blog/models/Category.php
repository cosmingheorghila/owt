<?php

class Blog_Model_Category extends Cms_Model
{
    /**
     * @var string
     */
    protected $table = 'blog_categories';

    /**
     * @var array
     */
    protected $validates = [
        'validation' => [
            'name' => [
                'presence',
            ],
        ],
        'unique' => [
            'name',
        ],
    ];

    /**
     * @var array
     */
    protected $actsAs = [
        'draggable',
        'url'
    ];

    /**
     * @var array
     */
    protected $hasAndBelongsToMany = [
        'posts' => [
            'model'         => 'Blog_Model_Post',
            'table'         => 'blog_posts_categories_ring',
            'local_key'     => 'category_id',
            'remote_key'    => 'post_id',
            'order_field'   => 'updated_at',
            'order_dir'     => 'desc',
        ],
    ];

    /**
     * @set $class
     */
    public function __construct()
    {
        parent::__construct();

        $this->class = get_class();
    }

    /**
     * @return void
     */
    protected function beforeSave()
    {
        parent::beforeSave();

        $this->params['url'] = Cms_Model_Page::getInstance()->load([
            'identifier' => 'blog'
        ])->getUri() . '/' . String::slugify($this->params['name']);
    }

    /**
     * @return mixed
     */
    public function getFirstPost()
    {
        return Arr::first(Blog_Model_Post::getInstance()->where(
            "id IN (SELECT post_id FROM blog_posts_categories_ring WHERE category_id = {$this->getId()})"
        )->order('updated_at', 'desc')->limit(1)->getCollection());
    }

    /**
     * @return bool|void
     */
    public function deleteEntity()
    {
        if (Request::isGet() && Get::exists('delete-id') && Get::exists('delete-entity')) {
            $this->verifyPosts();
            $this->deleteUrl();

            parent::deleteEntity();
        }
    }

    public function verifyPosts()
    {
        if (Database::getInstance('blog_posts_categories_ring')->select()->where(['category_id' => Get::get('delete-id')])->count() > 0) {
            Flash::error('Could not delete the Blog Category because it is assigned to Blog Posts!');
            Redirect::url(Url::getPreviousUrl());
        }
    }

    /**
     * @return bool
     */
    public function deleteUrl()
    {
        if (!Database::getInstance('cms_urls')->delete()->where(['entity_id' => Get::get('delete-id'), 'entity' => Get::get('delete-entity')])->save()) {
            Flash::error('Could not delete the URL assigned for this blog category!');
            Redirect::url(Url::getPreviousUrl());
        }
    }
}