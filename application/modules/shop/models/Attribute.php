<?php

class Shop_Model_Attribute extends Cms_Model
{
    /**
     * @var string
     */
    protected $table = 'shop_attributes';

    /**
     * @var array
     */
    protected $validates = [
        'validation' => [
            'name' => [
                'presence',
            ],
            'type' => [
                'presence',
            ],
            'filterable' => [
                'presence',
            ],
        ],
    ];

    /**
     * @var array
     */
    protected $actsAs = [
        'draggable' => [
            'field' => 'set_id',
        ],
    ];

    /**
     * @var array
     */
    protected $belongsTo = [
        'set' => [
            'model'         => 'Shop_Model_Attribute_Set',
            'remote_key'    => 'set_id',
        ],
    ];

    /**
     * @var array
     */
    protected $hasAndBelongsToMany = [
        'categories' => [
            'model'         => 'Shop_Model_Category',
            'table'         => 'shop_categories_attributes_ring',
            'local_key'     => 'attribute_id',
            'remote_key'    => 'category_id',
            'enable_rel'    => true,
        ],
        'products' => [
            'model'         => 'Shop_Model_Product',
            'table'         => 'shop_products_attributes_ring',
            'local_key'     => 'attribute_id',
            'remote_key'    => 'product_id',
        ],
    ];

    /**
     * @const
     */
    const FILTER_NO     = 0;
    const FILTER_YES    = 1;

    /**
     * @const
     */
    const FILTER_EVERYWHERE_NO  = 0;
    const FILTER_EVERYWHERE_YES = 1;

    /**
     * @const
     */
    const TYPE_TEXT     = 'text';
    const TYPE_TEXTAREA = 'textarea';
    const TYPE_NUMBER   = 'number';
    const TYPE_EDITOR   = 'editor';
    const TYPE_CALENDAR = 'calendar';
    const TYPE_TIME     = 'time';
    const TYPE_FILE     = 'file';
    const TYPE_IMAGE    = 'image';
    const TYPE_VIDEO    = 'video';
    const TYPE_AUDIO    = 'audio';

    /**
     * @var array
     */
    public static $filters = [
        self::FILTER_NO     => 'No',
        self::FILTER_YES    => 'Yes',
    ];

    /**
     * @var array
     */
    public static $everywhereFilters = [
        self::FILTER_EVERYWHERE_NO  => 'No',
        self::FILTER_EVERYWHERE_YES => 'Yes',
    ];

    /**
     * @var array
     */
    public static $types = [
        self::TYPE_TEXT     => 'Text',
        self::TYPE_TEXTAREA => 'Textarea',
        self::TYPE_NUMBER   => 'Number',
        self::TYPE_EDITOR   => 'Editor',
        self::TYPE_CALENDAR => 'Calendar',
        self::TYPE_TIME     => 'Time',
        self::TYPE_FILE     => 'File',
        self::TYPE_IMAGE    => 'Image',
        self::TYPE_VIDEO    => 'Video',
        self::TYPE_AUDIO    => 'Audio',
    ];

    /**
     * @set $class
     */
    public function __construct()
    {
        parent::__construct();

        $this->class = get_class();
    }

    /**
     * @return void
     */
    protected function beforeSave()
    {
        parent::beforeSave();

        $this->params['ord2'] = Shop_Model_Attribute_Set::getInstance()->load($this->params['set_id'])->getOrd();
    }

    /**
     * @param int|null $id
     * @return void
     */
    protected function afterSave($id)
    {
        parent::afterSave($id);

        Database::getInstance('shop_attributes')->update([
            'name'  => $this->params['name'],
            'label' => $this->params['label']
        ])->where(
            "BINARY name = '" . Post::get('old_attr_name') . "' AND set_id = '" . Post::get('data.set_id') . "'"
        )->save();
    }

    /**
     * @return mixed
     */
    public function getFirstProduct()
    {
        return Arr::first(Shop_Model_Product::getInstance()->where(
            "id IN (SELECT product_id FROM shop_products_attributes_ring WHERE attribute_id = {$this->getId()})"
        )->order('updated_at', 'desc')->limit(1)->getCollection());
    }

    /**
     * @param string|null $conditions
     * @param Model|int|null $category
     * @return array
     */
    public function getFilterAttributes($conditions = null, $category = null)
    {
        return $this->where([
            'filterable' => self::FILTER_YES
        ])->where(
            "id IN (SELECT attribute_id FROM shop_products_attributes_ring" . ($conditions ? " " . $conditions : "") . ")"
        )->where(
            "filterable_everywhere = '" . self::FILTER_EVERYWHERE_YES . "'" . (
                $category ? " OR id IN (SELECT attribute_id FROM shop_categories_attributes_ring WHERE category_id = {$category->getId()})" : ""
            )
        )->order([
            'ord2'  => 'asc',
            'ord'   => 'asc',
        ])->getCollection();
    }

    /**
     * @param array|null $conditions
     * @param Model|int|null $category
     * @return array|null
     */
    public function getFilterAttributesArray($conditions = null, $category = null)
    {
        $attributes = [];

        foreach ($this->getFilterAttributes($conditions, $category) as $attribute) {
            $attributes[$attribute->getName()][$attribute->getId()] = $attribute->getValue();
        }

        return !empty($attributes) ? $attributes : null;
    }

    /**
     * @return int|bool
     * @throws Exception
     */
    public function duplicate()
    {
        if (!$this->getId()) {
            return false;
        }

        try {
            $this->begin();

            $this->insert([
                'set_id'                => $this->getSetId(),
                'name'                  => $this->getName(),
                'label'                 => $this->getLabel(),
                'type'                  => $this->getType(),
                'filterable'            => $this->getFilterable(),
                'filterable_everywhere' => $this->getFilterableEverywhere(),
            ]);

            if ($id = $this->save()) {
                $this->setTable('shop_categories_attributes_ring');

                $categories = $this->getCategories();

                if (!empty($categories)) {
                    foreach ($categories as $category) {
                        $this->insert([
                            'category_id'   => $category->getId(),
                            'attribute_id'  => $id,
                        ])->save();
                    }
                }
            }

            $this->commit();

            return $id;
        } catch (Exception $e) {
            $this->rollback();

            return false;
        }
    }
}