<?php

class Shop_Model_WishList extends Cms_Model
{
    /**
     * @var string
     */
    protected $table = 'shop_wishlist';

    /**
     * @var array
     */
    protected $actsAs = [
        'timestamp'
    ];

    /**
     * @var array
     */
    protected $belongsTo = [
        'user' => [
            'model'         => 'Users_Model_User',
            'remote_key'    => 'user_id',
        ],
        'product' => [
            'model'         => 'Shop_Model_Product',
            'remote_key'    => 'product_id',
        ],
    ];

    /**
     * @set $class
     */
    public function __construct()
    {
        parent::__construct();

        $this->class = get_class();
    }

    /**
     * @return array|bool
     */
    public function getWishList()
    {
        if (!Session::exists('user')) {
            Flash::error('You must be logged in to access your WishList');
            return false;
        }

        return $this->where([
            'user_id' => Session::get('user')
        ])->getCollection();
    }
}