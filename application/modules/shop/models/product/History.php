<?php

class Shop_Model_Product_History extends Cms_Model
{
    /**
     * @var string
     */
    protected $table = 'shop_products_history';

    /**
     * @var array
     */
    protected $actsAs = [
        'timestamp'
    ];

    /**
     * @var array
     */
    protected $belongsTo = [
        'user' => [
            'model'         => 'Users_Model_User',
            'remote_key'    => 'user_id'
        ],
        'product' => [
            'model'         => 'Shop_Model_Product',
            'remote_key'    => 'product_id'
        ],
    ];

    /**
     * @set $class
     */
    public function __construct()
    {
        parent::__construct();

        $this->class = get_class();
    }

    /**
     * @param int|null $limit
     * @return array
     */
    public function getHistory($limit = null)
    {
        if (Session::exists('user')) {
            $this->where([
                'user_id' => (int)$_SESSION['user']
            ]);
        } else {
            $this->where([
                'user_id'       => 'null',
                'session_id'    => session_id()
            ]);
        }

        if ($limit) {
            $this->limit($limit);
        }

        return $this->getCollection();
    }
}