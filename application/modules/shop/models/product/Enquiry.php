<?php

class Shop_Model_Product_Enquiry extends Cms_Model
{
    /**
     * @var string
     */
    protected $table = 'shop_products_enquiries';

    /**
     * @var array
     */
    protected $actsAs = [
        'timestamp',
    ];

    /**
     * @var array
     */
    protected $validates = [
        'validation' => [
            'email' => [
                'presence',
                'email',
            ],
            'phone' => [
                'presence',
            ],
        ],
    ];

    /**
     * @var array
     */
    protected $belongsTo = [
        'product' => [
            'model'         => 'Shop_Model_Product',
            'remote_key'    => 'product_id',
        ],
    ];

    /**
     * @const
     */
    const VIEWED_NO     = '0';
    const VIEWED_YES    = '1';

    /**
     * @set $class
     */
    public function __construct()
    {
        parent::__construct();

        $this->class = get_class();
    }
}