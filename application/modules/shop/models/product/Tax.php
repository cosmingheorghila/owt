<?php

class Shop_Model_Product_Tax extends Cms_Model
{
    /**
     * @var string
     */
    protected $table = 'shop_taxes';

    /**
     * @var array
     */
    protected $validates = [
        'validation' => [
            'name' => [
                'presence',
            ],
            'rate' => [
                'presence',
                'numeric',
            ],
            'type' => [
                'presence',
            ],
        ],
    ];

    /**
     * @var array
     */
    protected $hasAndBelongsToMany = [
        'products' => [
            'model'         => 'Shop_Model_Product',
            'table'         => 'shop_products_taxes_ring',
            'local_key'     => 'tax_id',
            'remote_key'    => 'product_id',
            'order_field'   => 'ord',
            'order_dir'     => 'asc',
        ],
    ];

    /**
     * @const
     */
    const TYPE_FIXED    = 1;
    const TYPE_PERCENT  = 2;

    /**
     * @const
     */
    const APPLICABILITY_ORDER   = 1;
    const APPLICABILITY_PRODUCT = 2;

    /**
     * @const
     */
    const ACTIVE_NO     = 0;
    const ACTIVE_YES    = 1;

    /**
     * @var array
     */
    public static $types = [
        self::TYPE_FIXED    => 'Fixed',
        self::TYPE_PERCENT  => 'Percent',
    ];

    /**
     * @var array
     */
    public static $applicabilities = [
        self::APPLICABILITY_ORDER   => 'Order',
        self::APPLICABILITY_PRODUCT => 'Product',
    ];

    /**
     * @var array
     */
    public static $availability = [
        self::ACTIVE_NO     => 'No',
        self::ACTIVE_YES    => 'Yes',
    ];

    /**
     * @set $class
     */
    public function __construct()
    {
        parent::__construct();

        $this->class = get_class();
    }

    /**
     * @return void
     */
    protected function beforeSave()
    {
        parent::beforeSave();

        $date = Post::get('date');

        $this->params['maximum_value']  = ($this->params['maximum_value'] && $this->params['maximum_value'] != '') ? $this->params['maximum_value'] : 'null';
        $this->params['start_date']     = ($date['start_date_day'] && $date['start_date_day'] != '') ? strtotime($date['start_date_time'] . ' ' . $date['start_date_day']) : 'null';
        $this->params['end_date']       = ($date['end_date_day'] && $date['end_date_day'] != '') ? strtotime($date['end_date_time'] . ' ' . $date['end_date_day']) : 'null';

        if ($this->params['start_date'] != 'null' && $this->params['end_date'] != 'null' && $this->params['start_date'] >= $this->params['end_date']) {
            Flash::error('The Start Date must be smaller than the End Date!');
            Redirect::url(Url::getPreviousUrl());
        }
    }

    /**
     * @return mixed
     */
    public function getFirstProduct()
    {
        return Arr::first(Shop_Model_Product::getInstance()->where(
            "id IN (SELECT product_id FROM shop_products_taxes_ring WHERE tax_id = {$this->getId()})"
        )->order('updated_at', 'desc')->limit(1)->getCollection());
    }
}