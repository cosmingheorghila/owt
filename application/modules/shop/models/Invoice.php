<?php

class Shop_Model_Invoice extends Cms_Model
{
    /**
     * @var string
     */
    protected $table = 'shop_invoices';

    /**
     * @var array
     */
    protected $validates = [
        'validation' => [
            'identifier' => [
                'presence',
            ],
        ],
    ];

    /**
     * @var array
     */
    protected $actsAs = [
        'timestamp',
    ];

    /**
     * @var array
     */
    protected $belongsTo = [
        'order' => [
            'model'         => 'Shop_Model_Order',
            'remote_key'    => 'order_id',
        ],
    ];

    /**
     * @set $class
     */
    public function __construct()
    {
        parent::__construct();

        $this->class = get_class();
    }
}