<?php

class Shop_Model_Order_Item extends Cms_Model
{
    /**
     * @var string
     */
    protected $table = 'shop_orders_items';

    /**
     * @var array
     */
    protected $validates = [
        'validation' => [
            'name' => [
                'presence'
            ],
            'quantity' => [
                'presence',
                'numeric'
            ],
            'raw_total' => [
                'presence',
                'numeric'
            ],
            'sub_total' => [
                'presence',
                'numeric'
            ],
            'grand_total' => [
                'presence',
                'numeric'
            ],
        ],
    ];

    /**
     * @var array
     */
    protected $belongsTo = [
        'order' => [
            'model'         => 'Shop_Model_Order',
            'remote_key'    => 'order_id'
        ],
        'product' => [
            'model'         => 'Shop_Model_Product',
            'remote_key'    => 'product_id'
        ],
        'currency' => [
            'model'         => 'Shop_Model_Currency',
            'remote_key'    => 'currency_id'
        ],
    ];

    /**
     * @set $class
     */
    public function __construct()
    {
        parent::__construct();

        $this->class = get_class();
    }
}