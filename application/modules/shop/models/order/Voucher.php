<?php

class Shop_Model_Order_Voucher extends Cms_Model
{
    /**
     * @var string
     */
    protected $table = 'shop_vouchers';

    /**
     * @var array
     */
    protected $validates = [
        'validation' => [
            'name' => [
                'presence',
            ],
            'code' => [
                'presence',
            ],
            'maximum_usage' => [
                'numeric',
            ],
            'rate' => [
                'presence',
                'numeric',
            ],
            'type' => [
                'presence',
            ],
        ],
    ];

    /**
     * @var array
     */
    protected $actsAs = [
        'timestamp',
    ];

    /**
     * @const
     */
    const TYPE_FIXED    = 1;
    const TYPE_PERCENT  = 2;

    /**
     * @const
     */
    const ACTIVE_NO     = 0;
    const ACTIVE_YES    = 1;

    /**
     * @var array
     */
    public static $types = [
        self::TYPE_FIXED    => 'Fixed',
        self::TYPE_PERCENT  => 'Percent',
    ];

    /**
     * @var array
     */
    public static $availability = [
        self::ACTIVE_NO     => 'No',
        self::ACTIVE_YES    => 'Yes',
    ];

    /**
     * @set $class
     */
    public function __construct()
    {
        parent::__construct();

        $this->class = get_class();
    }

    /**
     * @return void
     */
    protected function beforeSave()
    {
        parent::beforeSave();

        $date = Post::get('date');

        $this->params['maximum_usage'] = ($this->params['maximum_usage'] && $this->params['maximum_usage'] != '') ? $this->params['maximum_usage'] : 'null';
        $this->params['minimum_value'] = ($this->params['minimum_value'] && $this->params['minimum_value'] != '') ? $this->params['minimum_value'] : 'null';

        $this->params['start_date'] = ($date['start_date_day'] && $date['start_date_day'] != '') ? strtotime($date['start_date_time'] . ' ' . $date['start_date_day']) : 'null';
        $this->params['end_date']   = ($date['end_date_day'] && $date['end_date_day'] != '') ? strtotime($date['end_date_time'] . ' ' . $date['end_date_day']) : 'null';

        if ($this->params['start_date'] != 'null' && $this->params['end_date'] != 'null' && $this->params['start_date'] >= $this->params['end_date']) {
            Flash::error('The Start Date must be smaller than the End Date!');
            Redirect::url(Url::getPreviousUrl());
        }
    }

    /**
     * @param string $code
     * @param $car
     * @return bool
     * @throws Exception
     */
    public function apply($code, $car)
    {
        if ($voucher = $this->load("BINARY code = '{$code}' AND active = " . self::ACTIVE_YES)) {
            if (!$this->verify($voucher, $car)) {
                return false;
            }

            $this->update(
                "current_usage = current_usage + 1"
            )->where([
                'id' => $voucher->getId()
            ])->save();

            Session::set('shop-voucher', $voucher->getId());
            Session::set('shop-voucher-car', $car->getId());
            Flash::success('Codul de pe cardul cadou a fost aplicat cu succes.');

            return true;
        }

        Flash::error('Codul nu corespunde nici unui card cadou valabil.');
        return false;
    }

    /**
     * @param Shop_Model_Order_Voucher $voucher
     * @param Car_Model_Car $car
     * @return bool
     */
    public function verify(Shop_Model_Order_Voucher $voucher, Car_Model_Car $car)
    {
        if ($voucher->getMaximumUsage() !== null && $voucher->getMaximumUsage() <= $voucher->getCurrentUsage()) {
            Flash::error('Acest card cadou si-a atins limita maxima de utilizari.');
            return false;
        }

        if ($voucher->getStartDate() !== null && time() < $voucher->getStartDate()) {
            Flash::error('Acest card cadou este valabil doar dupa: ' . date('d M, Y', $voucher->getStartDate()));
            return false;
        }

        if ($voucher->getEndDate() !== null && time() > $voucher->getEndDate()) {
            Flash::error('Acest card cadou este valabil doar pana cand: ' . date('d M, Y', $voucher->getEndDate()));
            return false;
        }

        if ($voucher->getMinimumValue() !== null && $car->getPrice() < $voucher->getMinimumValue()) {
            Flash::error('The order total must be of at least ' . $voucher->getMinimumValue() . ' for this Voucher Code to apply');
            return false;
        }

        return true;
    }
}