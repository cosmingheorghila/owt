<?php

class Shop_Model_Order extends Cms_Model
{
    /**
     * @var string
     */
    protected $table = 'shop_orders';

    /**
     * @var array
     */
    protected $validates = [
        'validation' => [
            'identifier' => [
                'presence'
            ],
            'raw_total' => [
                'presence',
                'numeric',
            ],
            'sub_total' => [
                'presence',
                'numeric',
            ],
            'grand_total' => [
                'presence',
                'numeric',
            ],
        ],
    ];

    /**
     * @var array
     */
    protected $actsAs = [
        'timestamp',
    ];

    /**
     * @var array
     */
    protected $belongsTo = [
        'user' => [
            'model'         => 'Users_Model_User',
            'remote_key'    => 'user_id',
        ],
    ];

    /**
     * @var array
     */
    protected $hasMany = [
        'items' => [
            'model'         => 'Shop_Model_Order_Item',
            'remote_key'    => 'order_id',
        ],
        'invoices' => [
            'model'         => 'Shop_Model_Invoice',
            'remote_key'    => 'order_id',
            'order_field'   => 'updated_at',
            'order_dir'     => 'desc',
        ],
    ];

    /**
     * @var array
     */
    protected $hasOne = [
        'customer' => [
            'model'         => 'Shop_Model_Customer',
            'remote_key'    => 'order_id',
        ],
    ];

    /**
     * @var
     */
    public $order;

    /**
     * @var array
     */
    public $cart = [];

    /**
     * @var
     */
    public $cartItems;

    /**
     * @var array
     */
    public $orderData       = [];
    public $itemsData       = [];
    public $customerData    = [];

    /**
     * @const
     */
    const STATUS_PENDING    = 1;
    const STATUS_CANCELED   = 2;
    const STATUS_COMPLETED  = 3;

    /**
     * @const
     */
    const PAYMENT_CASH_DELIVERY = 1;
    const PAYMENT_CREDIT_CARD   = 2;

    /**
     * @const
     */
    const SHIPPING_NORMAL_COURIER   = 1;
    const SHIPPING_EXPRESS_COURIER  = 2;

    /**
     * @const
     */
    const VIEWED_NO     = 0;
    const VIEWED_YES    = 1;

    /**
     * @var array
     */
    public static $statuses = [
        self::STATUS_PENDING    => 'Pending',
        self::STATUS_CANCELED   => 'Canceled',
        self::STATUS_COMPLETED  => 'Completed',
    ];

    /**
     * @var array
     */
    public static $paymentTypes = [
        self::PAYMENT_CASH_DELIVERY => 'Cash on Delivery',
        self::PAYMENT_CREDIT_CARD   => 'Credit Card',
    ];

    /**
     * @var array
     */
    public static $shippingOptions = [
        self::SHIPPING_NORMAL_COURIER   => 'Normal Courier',
        self::SHIPPING_EXPRESS_COURIER  => 'Express Courier',
    ];

    /**
     * @set $class
     */
    public function __construct()
    {
        parent::__construct();

        $this->class = get_class();
    }

    /**
     * @return void
     */
    protected function beforeSave()
    {
        parent::beforeSave();

        $this->params['created_at']     = ($this->params['created_at'] && $this->params['created_at'] != '') ? strtotime($this->params['created_at']) : 'null';
        $this->params['delivery_date']  = ($this->params['delivery_date'] && $this->params['delivery_date'] != '') ? strtotime($this->params['delivery_date']) : 'null';

        if (!Shop_Model_Customer::getInstance()->update(Post::get('customers'))->where(['order_id' => Post::get('identifier')])->save()) {
            Flash::error(Flash::show());
            Redirect::url(Post::exists('save-stay') ? Url::getPreviousUrl() : Url::getAdminUrl() . '/shop/orders/index');
        }
    }

    /**
     * @return bool
     */
    public function hasDiscounts()
    {
        return Shop_Model_Product_Discount::getInstance()->where([
            'applicability' => Shop_Model_Product_Discount::APPLICABILITY_ORDER,
            'active'        => Shop_Model_Product_Discount::ACTIVE_YES,
        ])->getCollection()->getCount() > 0 ? true : false;
    }

    /**
     * @return bool
     */
    public function hasTaxes()
    {
        return Shop_Model_Product_Tax::getInstance()->where([
            'applicability' => Shop_Model_Product_Tax::APPLICABILITY_ORDER,
            'active'        => Shop_Model_Product_Tax::ACTIVE_YES,
        ])->getCollection()->getCount() > 0 ? true : false;
    }

    /**
     * @return bool
     */
    public function hasVouchers()
    {
        return Shop_Model_Order_Voucher::getInstance()->where([
            'active' => Shop_Model_Order_Voucher::ACTIVE_YES,
        ])->getCollection()->getCount() > 0 ? true : false;
    }

    /**
     * @return array|null
     */
    public function getDiscounts()
    {
        if (!$this->hasDiscounts()) {
            return [];
        }

        return Shop_Model_Product_Discount::getInstance()->where([
            'applicability' => Shop_Model_Product_Discount::APPLICABILITY_ORDER,
            'active'        => Shop_Model_Product_Discount::ACTIVE_YES,
        ])->getCollection();
    }

    /**
     * @return array|null
     */
    public function getTaxes()
    {
        if (!$this->hasTaxes()) {
            return [];
        }

        return Shop_Model_Product_Tax::getInstance()->where([
            'applicability' => Shop_Model_Product_Tax::APPLICABILITY_ORDER,
            'active'        => Shop_Model_Product_Tax::ACTIVE_YES,
        ])->getCollection();
    }

    /**
     * @return array|null
     */
    public function getVouchers()
    {
        if (!$this->hasVouchers()) {
            return [];
        }

        return Shop_Model_Order_Voucher::getInstance()->where([
            'active' => Shop_Model_Order_Voucher::ACTIVE_YES,
        ])->getCollection();
    }

    /**
     * @param string $currency
     * @return float|int
     */
    public function getTotal($currency = 'RON')
    {
        $total = 0;

        foreach (Shop_Model_Cart::getInstance()->getCart() as $item) {
            $total += $item->getQuantity() * $item->getProduct()->getFinalPrice($currency);
        }

        return $total;
    }

    /**
     * @param string $currency
     * @return float|int
     */
    public function getSubTotal($currency = 'RON')
    {
        $total = $this->getTotal($currency);

        foreach ($this->getDiscounts() as $discount) {
            if (!$this->verifyDiscount($discount, $currency)) {
                continue;
            }

            switch ($discount->getType()) {
                case Shop_Model_Product_Discount::TYPE_FIXED:
                    $total -= $discount->getRate();
                    break;
                case Shop_Model_Product_Discount::TYPE_PERCENT:
                    $total -= ($discount->getRate() / 100) * $total;
                    break;
            }
        }

        if (Session::exists('shop-voucher')) {
            $voucher = Shop_Model_Order_Voucher::getInstance()->load([
                "BINARY code = '" . Session::get('shop-voucher') . "'"
            ]);

            switch ($voucher->getType()) {
                case Shop_Model_Order_Voucher::TYPE_FIXED:
                    $total -= $voucher->getRate();
                    break;
                case Shop_Model_Order_Voucher::TYPE_PERCENT:
                    $total -= ($voucher->getRate() / 100) * $total;
                    break;
            }
        }

        return $total;
    }

    /**
     * @param string $currency
     * @return float|int
     */
    public function getGrandTotal($currency = 'RON')
    {
        $total = $this->getTotal($currency);

        if (Session::exists('shop-voucher')) {
            $voucher = Shop_Model_Order_Voucher::getInstance()->load([
                "BINARY code = '" . Session::get('shop-voucher') . "'"
            ]);

            switch ($voucher->getType()) {
                case Shop_Model_Order_Voucher::TYPE_FIXED:
                    $total -= $voucher->getRate();
                    break;
                case Shop_Model_Order_Voucher::TYPE_PERCENT:
                    $total -= ($voucher->getRate() / 100) * $total;
                    break;
            }
        }

        foreach ($this->getDiscounts() as $discount) {
            if (!$this->verifyDiscount($discount, $currency)) {
                continue;
            }

            switch ($discount->getType()) {
                case Shop_Model_Product_Discount::TYPE_FIXED:
                    $total -= $discount->getRate();
                    break;
                case Shop_Model_Product_Discount::TYPE_PERCENT:
                    $total -= ($discount->getRate() / 100) * $total;
                    break;
            }
        }

        foreach ($this->getTaxes() as $tax) {
            if (!$this->verifyTax($tax, $currency)) {
                continue;
            }

            switch ($tax->getType()) {
                case Shop_Model_Product_Tax::TYPE_FIXED:
                    $total += $tax->getRate();
                    break;
                case Shop_Model_Product_Tax::TYPE_PERCENT:
                    $total += ($tax->getRate() / 100) * $total;
                    break;
            }
        }

        return $total;
    }

    /**
     * @param string $currency
     * @return float|int
     */
    public function getTotalWithDiscounts($currency = 'RON')
    {
        $total = $this->getTotal($currency);

        foreach ($this->getDiscounts() as $discount) {
            if (!$this->verifyDiscount($discount, $currency)) {
                continue;
            }

            switch ($discount->getType()) {
                case Shop_Model_Product_Discount::TYPE_FIXED:
                    $total -= $discount->getRate();
                    break;
                case Shop_Model_Product_Discount::TYPE_PERCENT:
                    $total -= ($discount->getRate() / 100) * $total;
                    break;
            }
        }

        return $total;
    }

    /**
     * @param string $currency
     * @return float|int
     */
    public function getTotalWithTaxes($currency = 'RON')
    {
        $total = $this->getTotal($currency);

        foreach ($this->getTaxes() as $tax) {
            if (!$this->verifyTax($tax, $currency)) {
                continue;
            }

            switch ($tax->getType()) {
                case Shop_Model_Product_Tax::TYPE_FIXED:
                    $total += $tax->getRate();
                    break;
                case Shop_Model_Product_Tax::TYPE_PERCENT:
                    $total += ($tax->getRate() / 100) * $total;
                    break;
            }
        }

        return $total;
    }

    /**
     * @param string $currency
     * @return float|int
     */
    public function getTotalWithVouchers($currency = 'RON')
    {
        $total = $this->getTotal($currency);

        if (Session::exists('shop-voucher')) {
            $voucher = Shop_Model_Order_Voucher::getInstance()->load([
                "BINARY code = '" . Session::get('shop-voucher') . "'"
            ]);

            switch ($voucher->getType()) {
                case Shop_Model_Order_Voucher::TYPE_FIXED:
                    $total -= $voucher->getRate();
                    break;
                case Shop_Model_Order_Voucher::TYPE_PERCENT:
                    $total -= ($voucher->getRate() / 100) * $total;
                    break;
            }
        }

        return $total;
    }

    /**
     * @param Shop_Model_Product_Discount $discount
     * @param string $currency
     * @return bool
     */
    public function verifyDiscount(Shop_Model_Product_Discount $discount, $currency = 'RON')
    {
        if (
            ($discount->getMinimumValue() !== null && $this->getTotal($currency) < $discount->getMinimumValue()) ||
            ($discount->getStartDate() !== null && time() < $discount->getStartDate()) ||
            ($discount->getEndDate() !== null && time() > $discount->getEndDate())
        ) {
            return false;
        }

        return true;
    }

    /**
     * @param Shop_Model_Product_Tax $tax
     * @param string $currency
     * @return bool
     */
    public function verifyTax(Shop_Model_Product_Tax $tax, $currency = 'RON')
    {
        if (
            ($tax->getMaximumValue() !== null && $this->getTotal($currency) > $tax->getMaximumValue()) ||
            ($tax->getStartDate() !== null && time() < $tax->getStartDate()) ||
            ($tax->getEndDate() !== null && time() > $tax->getEndDate())
        ) {
            return false;
        }

        return true;
    }

    /**
     * @param array $orderData
     * @param array $customerData
     * @return bool
     */
    public function finalize($orderData = [], $customerData = [])
    {
        $this->orderData    = $orderData;
        $this->customerData = $customerData;

        $this->cartItems = Shop_Model_Cart::getInstance()->getCart();

        if ($this->cartItems->getCount() == 0) {
            Flash::error('Cannot make an order containing 0 products');
            return false;
        }

        if (!$this->isValidData()) {
            Flash::error('Please fill in all the required data in order to place an order!');
            return false;
        }

        $this->parseOrderData();

        try {
            $this->begin();

            if ($this->order = $this->merge('insert', $this->orderData)) {
                $this->parseCustomerData();
                $this->parseItemsData();

                if (Shop_Model_Customer::getInstance()->merge('insert', $this->customerData)) {
                    foreach ($this->itemsData as $itemData) {
                        Shop_Model_Order_Item::getInstance()->merge('insert', $itemData);
                    }

                    foreach ($this->cartItems as $item) {
                        $product = $item->getProduct();
                        $product->incrementSells();
                    }

                    Shop_Model_Cart::getInstance()->delete()->where("id IN (" . implode(',', $this->cart) . ")")->save();
                } else {
                    Flash::error('Please fill in all the required data to place an order!');
                    return false;
                }
            }

            $this->commit();

            return $this->order;
        } catch (Exception $e) {
            $this->rollback();

            Flash::error('The order could not be processed! Please try again later.');
            return false;
        }
    }

    /**
     * @return bool
     */
    private function isValidData()
    {
        if (
            (!$this->orderData || !is_array($this->orderData) || empty($this->orderData)) ||
            (!$this->customerData || !is_array($this->customerData) || empty($this->customerData))
        ) {
            return false;
        }

        return true;
    }

    /**
     * @return void
     */
    private function parseOrderData()
    {
        if (!$this->orderData['identifier']) {
            $this->orderData['identifier'] = String::random(15);

            while ($this->load(['identifier' => $this->orderData['identifier']])) {
                $this->orderData['identifier'] = String::random(15);
            }
        }

        if (!$this->orderData['user_id']) {
            $this->orderData['user_id'] = Session::exists('user') ? Session::get('user') : 'null';
        }

        if (!$this->orderData['status']) {
            $this->orderData['status'] = self::STATUS_PENDING;
        }

        if (!$this->orderData['currency']) {
            $this->orderData['currency'] = 'RON';
        }
    }

    /**
     * @return void
     */
    private function parseCustomerData()
    {
        if (!$this->customerData['order_id']) {
            $this->customerData['order_id'] = $this->order ?: 'null';
        }

        if (!$this->customerData['user_id']) {
            $this->customerData['user_id'] = Session::exists('user') ? Session::get('user') : 'null';
        }
    }

    /**
     * @return void
     */
    private function parseItemsData()
    {
        foreach ($this->cartItems as $index => $item) {
            $product = $item->getProduct();

            $this->cart[] = $item->getId();

            $this->itemsData[$index]['order_id']    = $this->order;
            $this->itemsData[$index]['product_id']  = $item->getProductId();
            $this->itemsData[$index]['currency_id'] = $product->getCurrencyId();
            $this->itemsData[$index]['name']        = $product->getName();
            $this->itemsData[$index]['quantity']    = $item->getQuantity();
            $this->itemsData[$index]['price']       = $product->getFinalPrice();
            $this->itemsData[$index]['total']       = $item->getQuantity() * $product->getFinalPrice();
        }
    }
}