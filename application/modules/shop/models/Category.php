<?php

class Shop_Model_Category extends Cms_Model
{
    /**
     * @var string
     */
    protected $table = 'shop_categories';

    /**
     * @var array
     */
    protected $validates = [
        'validation' => [
            'name' => [
                'presence',
            ],
        ],
        'unique' => [
            'name',
        ],
    ];

    /**
     * @var array
     */
    protected $actsAs = [
        'url',
        'draggable' => [
            'field' => 'type_id',
        ],
        'block' => [
            'locations'     => [
                'content',
                'sidebar',
            ],
            'inherit_from'  => [
                'entity'        => 'page',
                'identifier'    => 'shop',
            ],
        ],
    ];

    /**
     * @var array
     */
    protected $belongsTo = [
        'type' => [
            'model'         => 'Shop_Model_Category_Type',
            'remote_key'    => 'type_id',
        ],
    ];

    /**
     * @var array
     */
    protected $hasMany = [
        'import_categories' => [
            'model'         => 'Import_Model_Category',
            'remote_key'    => 'category_id',
        ],
    ];

    /**
     * @var array
     */
    protected $hasAndBelongsToMany = [
        'sets' => [
            'model'         => 'Shop_Model_Attribute_Set',
            'table'         => 'shop_categories_attributes_sets_ring',
            'local_key'     => 'category_id',
            'remote_key'    => 'set_id',
            'enable_rel'    => true,
        ],
        'attributes' => [
            'model'         => 'Shop_Model_Attribute',
            'table'         => 'shop_categories_attributes_ring',
            'local_key'     => 'category_id',
            'remote_key'    => 'attribute_id',
        ],
        'products' => [
            'model'         => 'Shop_Model_Product',
            'table'         => 'shop_products_categories_ring',
            'local_key'     => 'category_id',
            'remote_key'    => 'product_id',
        ],
    ];

    /**
     * @set $class
     */
    public function __construct()
    {
        parent::__construct();

        $this->class = get_class();
    }

    /**
     * @return void
     */
    protected function beforeSave()
    {
        parent::beforeSave();

        $this->params['url'] = Cms_Model_Page::getInstance()->load([
            'identifier' => 'shop'
        ])->getUri() . '/' . String::slugify($this->params['name']);
    }

    /**
     * @return bool
     */
    public function hasProducts()
    {
        return $this->where(
            "id IN (SELECT category_id FROM shop_products_categories_ring WHERE category_id = {$this->getId()})"
        )->getCollection()->getCount() > 0 ? true : false;
    }

    /**
     * @return mixed
     */
    public function getFirstProduct()
    {
        return Arr::first(Shop_Model_Product::getInstance()->where(
            "id IN (SELECT product_id FROM shop_products_categories_ring WHERE category_id = {$this->getId()})"
        )->order('updated_at', 'desc')->limit(1)->getCollection());
    }

    /**
     * @param bool $none
     * @return array|null
     */
    public function getCategoriesArray($none = false)
    {
        $categories = [];

        if ($none === true) {
            $categories['null'] = 'None';
        }

        if (Shop_Model_Category_Type::getInstance()->getCollection()->getCount() > 0) {
            foreach (Shop_Model_Category_Type::getInstance()->order('ord')->getCollection() as $type) {
                foreach ($type->getCategories() as $category) {
                    $categories[$type->getName()][$category->getId()] = $category->getName();
                }
            }
        } elseif ($this->getCollection()->getCount() > 0) {
            foreach ($this->order('ord')->getCollection() as $category) {
                $categories[$category->getId()] = $category->getName();
            }
        }

        return !empty($categories) ? $categories : null;
    }

    /**
     * @return bool|void
     */
    public function deleteEntity()
    {
        if (Request::isGet() && Get::get('delete-id') && Get::get('delete-entity')) {
            $this->deleteUrl();

            parent::deleteEntity();
        }
    }

    /**
     * @return bool
     */
    public function deleteUrl()
    {
        if (!Database::getInstance('cms_urls')->delete()->where(['entity_id' => Get::get('delete-id'), 'entity' => Get::get('delete-entity')])->save()) {
            Flash::error('Could not delete the URL assigned for this product category!');
            Redirect::url(Url::getPreviousUrl());
        }
    }
}