<?php

class Shop_Model_Product extends Cms_Model
{
    /**
     * @var string
     */
    protected $table = 'shop_products';

    /**
     * @var array
     */
    protected $validates = [
        'validation' => [
            'sku' => [
                'presence',
            ],
            'name' => [
                'presence',
            ],
        ],
        'unique' => [
            'sku',
            'name',
        ],
    ];

    /**
     * @var array
     */
    protected $actsAs = [
        'timestamp',
        'url',
        'block' => [
            'locations'     => [
                'content',
                'sidebar',
            ],
            'inherit_from'  => [
                'entity'        => 'page',
                'identifier'    => 'shop',
            ],
        ],
    ];

    /**
     * @var array
     */
    protected $belongsTo = [
        'currency' => [
            'model'         => 'Shop_Model_Currency',
            'remote_key'    => 'currency_id',
        ],
    ];

    /**
     * @var array
     */
    protected $hasMany = [
        'items' => [
            'model'         => 'Shop_Model_Order_Item',
            'remote_key'    => 'product_id',
        ],
        'carts' => [
            'model'         => 'Shop_Model_Cart',
            'remote_key'    => 'product_id',
            'order_field'   => 'updated_at',
            'order_dir'     => 'desc',
        ],
        'media' => [
            'model'         => 'Shop_Model_Product_Media',
            'remote_key'    => 'product_id',
            'order_field'   => 'ord',
            'order_dir'     => 'asc',
        ],
    ];

    /**
     * @var array
     */
    protected $hasAndBelongsToMany = [
        'categories' => [
            'model'         => 'Shop_Model_Category',
            'table'         => 'shop_products_categories_ring',
            'local_key'     => 'product_id',
            'remote_key'    => 'category_id',
            'order_field'   => 'ord',
            'order_dir'     => 'asc',
            'enable_rel'    => true,
        ],
        'vendors' => [
            'model'         => 'Shop_Model_Vendor',
            'table'         => 'shop_products_vendors_ring',
            'local_key'     => 'product_id',
            'remote_key'    => 'vendor_id',
            'order_field'   => 'ord',
            'order_dir'     => 'asc',
        ],
        'attributes' => [
            'model'         => 'Shop_Model_Attribute',
            'table'         => 'shop_products_attributes_ring',
            'local_key'     => 'product_id',
            'remote_key'    => 'attribute_id',
            'order_field'   => 'ord',
            'order_dir'     => 'asc',
        ],
        'taxes' => [
            'model'         => 'Shop_Model_Product_Tax',
            'table'         => 'shop_products_taxes_ring',
            'local_key'     => 'product_id',
            'remote_key'    => 'tax_id',
            'order_field'   => 'ord',
            'order_dir'     => 'asc',
        ],
        'discounts' => [
            'model'         => 'Shop_Model_Product_Discount',
            'table'         => 'shop_products_discounts_ring',
            'local_key'     => 'product_id',
            'remote_key'    => 'discount_id',
            'order_field'   => 'ord',
            'order_dir'     => 'asc',
        ],
    ];

    /**
     * @const
     */
    const STOCK_NO      = '0';
    const STOCK_YES     = '1';
    const STOCK_LIMITED = '2';

    /**
     * @const
     */
    const ACTIVE_NO     = '0';
    const ACTIVE_YES    = '1';

    /**
     * @const
     */
    const TVA_NO    = '0';
    const TVA_YES   = '1';

    /**
     * @const
     */
    const IS_IMPORTED_NO    = '0';
    const IS_IMPORTED_YES   = '1';

    /**
     * @var array
     */
    public static $stocks = [
        self::STOCK_NO      => 'No',
        self::STOCK_YES     => 'Yes',
        self::STOCK_LIMITED => 'Limited',
    ];

    /**
     * @var array
     */
    public static $availability = [
        self::ACTIVE_NO     => 'No',
        self::ACTIVE_YES    => 'Yes',
    ];

    /**
     * @var array
     */
    public static $includes = [
        self::TVA_NO    => 'No',
        self::TVA_YES   => 'Yes',
    ];

    /**
     * @var array
     */
    public static $imports = [
        self::IS_IMPORTED_NO    => 'No',
        self::IS_IMPORTED_YES   => 'Yes',
    ];

    /**
     * @set $class
     */
    public function __construct()
    {
        parent::__construct();

        $this->class = get_class();
    }

    /**
     * @return void
     */
    protected function beforeSave()
    {
        parent::beforeSave();

        if (!ctype_alnum($this->params['sku'])) {
            Session::set('data', $this->params);

            Flash::error('The SKU must not contain spaces or special characters!');
            Redirect::url(Url::getPreviousUrl());
        }

        if (!Post::exists('categories')) {
            if (!$this->model) {
                Session::set('data', $this->params);
            }

            Flash::error('You must specify at least one Category for the product!');
            Redirect::url(Url::getPreviousUrl());
        }

        /*if ($this->model) {
            $model = (new self())->load(Get::get('id'));
            $model->updateVirtualData();
        }*/

        $this->params['url'] = Cms_Model_Page::getInstance()->load([
            'identifier' => 'shop'
        ])->getUri() . '/' . String::slugify($this->params['name']);
    }

    /**
     * @param int|null $id
     * @return void
     */
    protected function afterSave($id)
    {
        parent::afterSave($id);

        $this->model->updateVirtualData();
    }

    /**
     * @return $this
     */
    public function whereInStock()
    {
        $this->where("virtual_stock != 0 AND virtual_quantity > 0");

        return $this;
    }

    /**
     * @return $this
     */
    public function whereInDiscounts()
    {
        $this->where("id IN (SELECT DISTINCT product_id FROM shop_products_discounts_ring)");

        return $this;
    }

    /**
     * @return $this
     */
    public function whereInTaxes()
    {
        $this->where("id IN (SELECT DISTINCT product_id FROM shop_products_taxes_ring)");

        return $this;
    }

    /**
     * @param array $ids
     * @return $this
     */
    public function whereInAttributes($ids = [])
    {
        if ($ids && is_array($ids) && !empty($ids)) {
            $this->where("id IN (SELECT product_id FROM shop_products_attributes_ring WHERE attribute_id IN (" . implode(',', $ids) . ") GROUP BY product_id)");
        }

        return $this;
    }

    /**
     * @param float|null $startPrice
     * @param float|null $endPrice
     * @return $this
     */
    public function whereInPriceRange($startPrice = null, $endPrice = null)
    {
        if ($startPrice && !$endPrice) {
            $this->where("virtual_price >= {$startPrice}");
        } elseif ($endPrice && !$startPrice) {
            $this->where("virtual_price <= {$endPrice}");
        } elseif ($startPrice && $endPrice) {
            $this->where("virtual_price >= {$startPrice} AND virtual_price <= {$endPrice}");
        }

        return $this;
    }

    /**
     * @return bool
     */
    public function isNew()
    {
        return $this->getUpdatedAt() > strtotime("-1 month", time()) ? true : false;
    }

    /**
     * @return bool
     */
    public function hasDiscounts()
    {
        if (($vendor = $this->getVendor())/* && $this->getData('stock') == 0 && $this->getData('quantity') == 0*/) {
            return Shop_Model_Product_Discount::getInstance()->where(
                "id IN (SELECT discount_id FROM shop_products_discounts_ring WHERE product_id = '{$this->getId()}' AND vendor_id IS NOT NULL)"
            )->where([
                'applicability' => Shop_Model_Product_Discount::APPLICABILITY_PRODUCT,
                'active'        => Shop_Model_Product_Discount::ACTIVE_YES
            ])->getCollection()->getCount() > 0 ? true : false;
        }

        return Shop_Model_Product_Discount::getInstance()->where(
            "id IN (SELECT discount_id FROM shop_products_discounts_ring WHERE product_id = '{$this->getId()}' AND vendor_id IS NULL)"
        )->where([
            'applicability' => Shop_Model_Product_Discount::APPLICABILITY_PRODUCT,
            'active'        => Shop_Model_Product_Discount::ACTIVE_YES
        ])->getCollection()->getCount() > 0 ? true : false;
    }

    /**
     * @return bool
     */
    public function hasTaxes()
    {
        if (($vendor = $this->getVendor())/* && $this->getData('stock') == 0 && $this->getData('quantity') == 0*/) {
            return Shop_Model_Product_Tax::getInstance()->where(
                "id IN (SELECT tax_id FROM shop_products_taxes_ring WHERE product_id = '{$this->getId()}' AND vendor_id IS NOT NULL)"
            )->where([
                'applicability' => Shop_Model_Product_Tax::APPLICABILITY_PRODUCT,
                'active'        => Shop_Model_Product_Tax::ACTIVE_YES
            ])->getCollection()->getCount() > 0 ? true : false;
        }

        return Shop_Model_Product_Tax::getInstance()->where(
            "id IN (SELECT tax_id FROM shop_products_taxes_ring WHERE product_id = '{$this->getId()}' AND vendor_id IS NULL)"
        )->where([
            'applicability' => Shop_Model_Product_Tax::APPLICABILITY_PRODUCT,
            'active'        => Shop_Model_Product_Tax::ACTIVE_YES
        ])->getCollection()->getCount() > 0 ? true : false;
    }

    /**
     * @return bool
     */
    public function hasVendors()
    {
        return Shop_Model_Vendor::getInstance()->where(
            "id IN (SELECT vendor_id FROM shop_products_vendors_ring WHERE product_id = '{$this->getId()}')"
        )->getCollection()->getCount() > 0 ? true : false;
    }

    /**
     * @return bool
     */
    public function hasAttributes()
    {
        return Shop_Model_Attribute::getInstance()->where(
            "id IN (SELECT attribute_id FROM shop_products_attributes_ring WHERE product_id = '{$this->getId()}')"
        )->getCollection()->getCount() > 0 ? true : false;
    }

    /**
     * @return mixed
     */
    public function getFirstCategory()
    {
        return Arr::first(Shop_Model_Category::getInstance()->where(
            "id IN (SELECT category_id FROM shop_products_categories_ring WHERE product_id = {$this->getId()})"
        )->order('ord', 'asc')->limit(1)->getCollection());
    }

    /**
     * @return mixed
     */
    public function getFirstVendor()
    {
        return Arr::first(Shop_Model_Vendor::getInstance()->where(
            "id IN (SELECT vendor_id FROM shop_products_vendors_ring WHERE product_id = {$this->getId()})"
        )->order('ord', 'asc')->limit(1)->getCollection());
    }

    /**
     * @return mixed
     */
    public function getFirstAttribute()
    {
        return Arr::first(Shop_Model_Attribute::getInstance()->where(
            "id IN (SELECT attribute_id FROM shop_products_attributes_ring WHERE product_id = {$this->getId()})"
        )->order('ord', 'asc')->limit(1)->getCollection());
    }

    /**
     * @return mixed
     */
    public function getFirstTax()
    {
        return Arr::first(Shop_Model_Product_Tax::getInstance()->where(
            "id IN (SELECT tax_id FROM shop_products_taxes_ring WHERE product_id = {$this->getId()})"
        )->order('id', 'asc')->limit(1)->getCollection());
    }

    /**
     * @return mixed
     */
    public function getFirstDiscount()
    {
        return Arr::first(Shop_Model_Product_Discount::getInstance()->where(
            "id IN (SELECT discount_id FROM shop_products_discounts_ring WHERE product_id = {$this->getId()})"
        )->order('id', 'asc')->limit(1)->getCollection());
    }

    /**
     * @param int|null $limit
     * @param bool $random
     * @return array
     */
    public function getProducts($limit = null, $random = false)
    {
        $this->where(['active' => self::ACTIVE_YES]);

        if ($random) {
            $this->order('Rand()');
        } else {
            $this->order('updated_at', 'desc');
        }

        if ($limit && is_numeric($limit)) {
            $this->limit($limit);
        }

        return $this->getCollection();
    }

    /**
     * @param int|null $limit
     * @param bool $random
     * @return array
     */
    public function getRelatedProducts($limit = null, $random = false)
    {
        $this->where("id != {$this->getId()} AND id IN (SELECT product_id FROM shop_products_categories_ring WHERE category_id = {$this->getFirstCategory()->getId()})");
        $this->where([
            'active' => self::ACTIVE_YES
        ]);

        if ($random) {
            $this->order('Rand()');
        } else {
            $this->order('updated_at', 'desc');
        }

        if ($limit && is_numeric($limit)) {
            $this->limit($limit);
        }

        return $this->getCollection();
    }

    /**
     * @param int|null $limit
     * @param bool $random
     * @return array
     */
    public function getProductsWithDiscounts($limit = null, $random = false)
    {
        $this->where("id IN (SELECT product_id FROM shop_products_discounts_ring)");
        $this->where([
            'active' => self::ACTIVE_YES
        ]);

        if ($random) {
            $this->order('Rand()');
        } else {
            $this->order('updated_at', 'desc');
        }

        if ($limit && is_numeric($limit)) {
            $this->limit($limit);
        }

        return $this;
    }

    /**
     * @param int|null $limit
     * @param bool $random
     * @return array
     */
    public function getProductsWithTaxes($limit = null, $random = false)
    {
        $this->where("id IN (SELECT product_id FROM shop_products_taxes_ring)");
        $this->where([
            'active' => self::ACTIVE_YES
        ]);

        if ($random) {
            $this->order('Rand()');
        } else {
            $this->order('updated_at', 'desc');
        }

        if ($limit && is_numeric($limit)) {
            $this->limit($limit);
        }

        return $this->getCollection();
    }

    /**
     * @return Shop_Model_Vendor|null
     */
    public function getVendor()
    {
        $vendor     = null;
        $vendors    = Shop_Model_Vendor::getInstance()->select(
            'shop_vendors.*, shop_products_vendors_ring.*'
        )->join(
            'shop_products_vendors_ring'
        )->on([
            'shop_products_vendors_ring.vendor_id'  => 'shop_vendors.id',
            'shop_products_vendors_ring.product_id' => $this->getId()
        ])->order([
            'price'     => 'asc',
            'quantity'  => 'desc'
        ])->limit(2)->getCollection();

        if (count($vendors) > 1) {
            foreach ($vendors as $v) {
                if ($v->getQuantity() > 0 && $v->getPrice() > 0) {
                    $vendor = $v;
                    break;
                }
            }

            if (!$vendor) {
                foreach ($vendors as $v) {
                    if ($v->getQuantity() > 0) {
                        $vendor = $v;
                        break;
                    }
                }
            }
        }

        return $vendor ?: ($vendors->first() ?: null);
    }

    /**
     * @return array
     */
    public function getVendors()
    {
        if (!$this->hasVendors()) {
            return [];
        }

        return Shop_Model_Vendor::getInstance()->select(
            'shop_vendors.*, shop_products_vendors_ring.*'
        )->join(
            'shop_products_vendors_ring'
        )->on([
            'shop_products_vendors_ring.vendor_id'  => 'shop_vendors.id',
            'shop_products_vendors_ring.product_id' => $this->getId()
        ])->order('ord', 'asc')->getCollection();
    }

    /**
     * @return array
     */
    public function getDiscounts()
    {
        if (!$this->hasDiscounts()) {
            return [];
        }

        if ($this->hasVendors()/* && $this->getData('quantity') == 0 && $this->getData('stock') == self::STOCK_NO*/) {
            return Shop_Model_Product_Discount::getInstance()->select(
                'shop_discounts.name AS discount_name, shop_discounts.rate AS discount_rate, shop_discounts.type AS discount_type, shop_discounts.active AS discount_active, shop_discounts.start_date AS discount_start_date, shop_discounts.end_date AS discount_end_date, shop_discounts.minimum_value AS discount_minimum_value, shop_vendors.name AS vendor_name, shop_products_discounts_ring.*',
                'shop_products_discounts_ring'
            )->join('shop_vendors')->on([
                'shop_products_discounts_ring.vendor_id' => 'shop_vendors.id'
            ])->join('shop_discounts')->on([
                'shop_products_discounts_ring.discount_id' => 'shop_discounts.id'
            ])->where([
                'shop_products_discounts_ring.product_id'   => $this->getId(),
                'shop_discounts.applicability'              => Shop_Model_Product_Discount::APPLICABILITY_PRODUCT,
                'shop_discounts.active'                     => Shop_Model_Product_Discount::ACTIVE_YES
            ])->order('ord', 'asc')->getCollection();
        }

        return Shop_Model_Product_Discount::getInstance()->select(
            'shop_discounts.name AS discount_name, shop_discounts.rate AS discount_rate, shop_discounts.type AS discount_type, shop_discounts.active AS discount_active, shop_discounts.start_date AS discount_start_date, shop_discounts.end_date AS discount_end_date, shop_discounts.minimum_value AS discount_minimum_value, shop_products_discounts_ring.*',
            'shop_products_discounts_ring'
        )->join('shop_discounts')->on([
            'shop_products_discounts_ring.discount_id' => 'shop_discounts.id'
        ])->where([
            'shop_products_discounts_ring.product_id'   => $this->getId(),
            'shop_products_discounts_ring.vendor_id'    => 'null',
            'shop_discounts.applicability'              => Shop_Model_Product_Discount::APPLICABILITY_PRODUCT,
            'shop_discounts.active'                     => Shop_Model_Product_Discount::ACTIVE_YES
        ])->order('ord', 'asc')->getCollection();
    }

    /**
     * @param int $vendorId
     * @return null
     */
    public function getVendorDiscounts($vendorId)
    {
        if (!$this->hasDiscounts()) {
            return [];
        }

        return Shop_Model_Product_Discount::getInstance()->select(
            'shop_discounts.name AS discount_name, shop_discounts.rate AS discount_rate, shop_discounts.type AS discount_type, shop_discounts.active AS discount_active, shop_discounts.start_date AS discount_start_date, shop_discounts.end_date AS discount_end_date, shop_discounts.minimum_value AS discount_minimum_value, shop_vendors.name AS vendor_name, shop_products_discounts_ring.*',
            'shop_products_discounts_ring'
        )->join('shop_vendors')->on([
            'shop_products_discounts_ring.vendor_id' => 'shop_vendors.id'
        ])->join('shop_discounts')->on([
            'shop_products_discounts_ring.discount_id' => 'shop_discounts.id'
        ])->where([
            'shop_products_discounts_ring.product_id'   => $this->getId(),
            'shop_products_discounts_ring.vendor_id'    => $vendorId,
            'shop_discounts.applicability'              => Shop_Model_Product_Discount::APPLICABILITY_PRODUCT,
            'shop_discounts.active'                     => Shop_Model_Product_Discount::ACTIVE_YES
        ])->where(
            "(shop_discounts.start_date <= UNIX_TIMESTAMP() AND shop_discounts.end_date >= UNIX_TIMESTAMP()) OR (shop_discounts.start_date IS NULL AND shop_discounts.end_date IS NULL)"
        )->order('ord', 'asc')->getCollection();
    }

    /**
     * @return array
     */
    public function getTaxes()
    {
        if (!$this->hasTaxes()) {
            return [];
        }

        if ($this->hasVendors()/* && $this->getData('quantity') == 0 && $this->getData('stock') == self::STOCK_NO*/) {
            return Shop_Model_Product_Tax::getInstance()->select(
                'shop_taxes.name AS tax_name, shop_taxes.rate AS tax_rate, shop_taxes.type AS tax_type, shop_taxes.active AS tax_active, shop_taxes.start_date AS tax_start_date, shop_taxes.end_date AS tax_end_date, shop_taxes.maximum_value AS tax_maximum_value, shop_vendors.name AS vendor_name, shop_products_taxes_ring.*',
                'shop_products_taxes_ring'
            )->join('shop_vendors')->on([
                'shop_products_taxes_ring.vendor_id' => 'shop_vendors.id'
            ])->join('shop_taxes')->on([
                'shop_products_taxes_ring.tax_id' => 'shop_taxes.id'
            ])->where([
                'shop_products_taxes_ring.product_id'   => $this->getId(),
                'shop_taxes.active'                     => Shop_Model_Product_Tax::ACTIVE_YES
            ])->order('ord', 'asc')->getCollection();
        }

        return Shop_Model_Product_Tax::getInstance()->select(
            'shop_taxes.name AS tax_name, shop_taxes.rate AS tax_rate, shop_taxes.type AS tax_type, shop_taxes.active AS tax_active, shop_taxes.start_date AS tax_start_date, shop_taxes.end_date AS tax_end_date, shop_taxes.maximum_value AS tax_maximum_value, shop_products_taxes_ring.*',
            'shop_products_taxes_ring'
        )->join('shop_taxes')->on([
            'shop_products_taxes_ring.tax_id' => 'shop_taxes.id'
        ])->where([
            'shop_products_taxes_ring.product_id'   => $this->getId(),
            'shop_products_taxes_ring.vendor_id'    => 'null',
            'shop_taxes.active'                     => Shop_Model_Product_Tax::ACTIVE_YES
        ])->order('ord', 'asc')->getCollection();
    }

    /**
     * @param int $vendorId
     * @return null
     */
    public function getVendorTaxes($vendorId)
    {
        if (!$this->hasTaxes()) {
            return [];
        }

        return Shop_Model_Product_Tax::getInstance()->select(
            'shop_taxes.name AS tax_name, shop_taxes.rate AS tax_rate, shop_taxes.type AS tax_type, shop_taxes.active AS tax_active, shop_taxes.start_date AS tax_start_date, shop_taxes.end_date AS tax_end_date, shop_taxes.maximum_value AS tax_maximum_value, shop_vendors.name AS vendor_name, shop_products_taxes_ring.*',
            'shop_products_taxes_ring'
        )->join('shop_vendors')->on([
            'shop_products_taxes_ring.vendor_id' => 'shop_vendors.id'
        ])->join('shop_taxes')->on([
            'shop_products_taxes_ring.tax_id' => 'shop_taxes.id'
        ])->where([
            'shop_products_taxes_ring.product_id'   => $this->getId(),
            'shop_products_taxes_ring.vendor_id'    => $vendorId
        ])->order('ord', 'asc')->getCollection();
    }

    /**
     * @param string $currency
     * @return float|int
     */
    public function getPrice($currency = 'RON')
    {
        if (($vendor = $this->getVendor())/* && $this->getData('quantity') == 0 && $this->getData('stock') == self::STOCK_NO*/) {
            return Shop_Model_Currency::convert($vendor->getPrice(), $this->getCurrency()->getCode(), $currency);
        }

        return Shop_Model_Currency::convert($this->getData('price'), $this->getCurrency()->getCode(), $currency);
    }

    /**
     * @param string $currency
     * @return float|int
     */
    public function getPriceWithDiscounts($currency = 'RON')
    {
        $vendor = $this->getVendor();
        $price  = $this->getPrice($currency);

        if (!$this->hasDiscounts()) {
            return $price;
        }

        if ($vendor) {
            foreach ($this->getVendorDiscounts($vendor->getVendorId()) as $discount) {
                if (!$this->verifyDiscount($discount)) {
                    continue;
                }

                switch ($discount->getDiscountType()) {
                    case Shop_Model_Product_Discount::TYPE_FIXED:
                        $price -= $discount->getDiscountRate();
                        break;
                    case Shop_Model_Product_Discount::TYPE_PERCENT:
                        $price -= ($discount->getDiscountRate() / 100) * $price;
                        break;
                }
            }
        } else {
            foreach ($this->getDiscounts() as $discount) {
                if (!$this->verifyDiscount($discount)) {
                    continue;
                }

                switch ($discount->getDiscountType()) {
                    case Shop_Model_Product_Discount::TYPE_FIXED:
                        $price -= $discount->getDiscountRate();
                        break;
                    case Shop_Model_Product_Discount::TYPE_PERCENT:
                        $price -= ($discount->getDiscountRate() / 100) * $price;
                        break;
                }
            }
        }

        return $price;
    }

    /**
     * @param string $currency
     * @return float|int
     */
    public function getPriceWithTaxes($currency = 'RON')
    {
        $vendor = $this->getVendor();
        $price  = $this->getPrice($currency);

        if (!$this->hasTaxes()) {
            return $price;
        }

        if ($this->hasVendors()/* && $this->getData('quantity') == 0 && $this->getData('stock') == self::STOCK_NO*/) {
            foreach ($this->getVendorTaxes($vendor->getVendorId()) as $tax) {
                if (!$this->verifyTax($tax)) {
                    continue;
                }

                switch ($tax->getTaxType()) {
                    case Shop_Model_Product_Tax::TYPE_FIXED:
                        $price += $tax->getTaxRate();
                        break;
                    case Shop_Model_Product_Tax::TYPE_PERCENT:
                        $price += ($tax->getTaxRate() / 100) * $price;
                        break;
                }
            }
        } else {
            foreach ($this->getTaxes() as $tax) {
                if (!$this->verifyTax($tax)) {
                    continue;
                }

                switch ($tax->getTaxType()) {
                    case Shop_Model_Product_Tax::TYPE_FIXED:
                        $price += $tax->getTaxRate();
                        break;
                    case Shop_Model_Product_Tax::TYPE_PERCENT:
                        $price += ($tax->getTaxRate() / 100) * $price;
                        break;
                }
            }
        }

        return $price;
    }

    /**
     * @param string $currency
     * @return float|int
     */
    public function getFinalPrice($currency = 'RON')
    {
        $price  = $this->getPrice($currency);
        $vendor = $this->getVendor();

        if ($this->hasVendors()/* && $this->getData('quantity') == 0 && $this->getData('stock') == self::STOCK_NO*/) {
            foreach ($this->getVendorDiscounts($vendor->getVendorId()) as $discount) {
                if (!$this->verifyDiscount($discount)) {
                    continue;
                }

                switch ($discount->getDiscountType()) {
                    case Shop_Model_Product_Discount::TYPE_FIXED:
                        $price -= $discount->getDiscountRate();
                        break;
                    case Shop_Model_Product_Discount::TYPE_PERCENT:
                        $price -= ($discount->getDiscountRate() / 100) * $price;
                        break;
                }
            }

            foreach ($this->getVendorTaxes($vendor->getVendorId()) as $tax) {
                if (!$this->verifyTax($tax)) {
                    continue;
                }

                switch ($tax->getTaxType()) {
                    case Shop_Model_Product_Tax::TYPE_FIXED:
                        $price += $tax->getTaxRate();
                        break;
                    case Shop_Model_Product_Tax::TYPE_PERCENT:
                        $price += ($tax->getTaxRate() / 100) * $price;
                        break;
                }
            }
        } else {
            foreach ($this->getDiscounts() as $discount) {
                if (!$this->verifyDiscount($discount)) {
                    continue;
                }

                switch ($discount->getDiscountType()) {
                    case Shop_Model_Product_Discount::TYPE_FIXED:
                        $price -= $discount->getDiscountRate();
                        break;
                    case Shop_Model_Product_Discount::TYPE_PERCENT:
                        $price -= ($discount->getDiscountRate() / 100) * $price;
                        break;
                }
            }

            foreach ($this->getTaxes() as $tax) {
                if (!$this->verifyTax($tax)) {
                    continue;
                }

                switch ($tax->getTaxType()) {
                    case Shop_Model_Product_Tax::TYPE_FIXED:
                        $price += $tax->getTaxRate();
                        break;
                    case Shop_Model_Product_Tax::TYPE_PERCENT:
                        $price += ($tax->getTaxRate() / 100) * $price;
                        break;
                }
            }
        }

        return $price;
    }

    /**
     * @return mixed
     */
    public function getStock()
    {
        if (($vendor = $this->getVendor())/* && $this->getData('quantity') == 0 && $this->getData('stock') == self::STOCK_NO*/) {
            if ($vendor->getQuantity() > 10) {
                return self::$stocks[self::STOCK_YES];
            } elseif ($vendor->getQuantity() == 0) {
                return self::$stocks[self::STOCK_NO];
            } else {
                return self::$stocks[self::STOCK_LIMITED];
            }
        }

        return self::$stocks[$this->getData('stock')];
    }

    /**
     * @return mixed
     */
    public function getQuantity()
    {
        if ($vendor = $this->getVendor()) {
            return $vendor->getQuantity();
        }

        return $this->getData('quantity');
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return ($image = Shop_Model_Product_Media::getInstance()->where([
            'product_id'    => $this->getId(),
            'type'          => Shop_Model_Product_Media::TYPE_IMAGE
        ])->order('ord','asc')->limit(1)->getCollection()) ? $image->first() : null;
    }

    /**
     * @param array|string|null $where
     * @return array
     */
    public function getAttributesJoined($where = null)
    {
        if (!$this->hasAttributes()) {
            return [];
        }

        $attributes = new Shop_Model_Attribute();
        $attributes->select(
            'shop_attributes.*, shop_products_attributes_ring.value AS attribute_value, shop_products_attributes_ring.id AS attribute_id'
        )->join(
            'shop_products_attributes_ring'
        )->on([
            'shop_products_attributes_ring.attribute_id'    => 'shop_attributes.id',
            'shop_products_attributes_ring.product_id'      => $this->getId()
        ]);

        if ($where) {
            $attributes->where($where);
        }

        return $attributes->order('ord', 'asc')->getCollection();
    }

    /**
     * @return array
     */
    public function getSpecifications()
    {
        $specs  = [];
        $sets   = [];
        $order  = [];

        foreach ($this->getAttributes() as $attribute) {
            $sets[$attribute->getSet()->getId()] = $attribute->getSet()->getName();
        }

        $sets = array_unique($sets);

        foreach ($sets as $id => $set) {
            foreach ($this->getAttributesJoined(['set_id' => $id]) as $attribute) {
                $specs[$id][$set][$attribute->getName()] = $attribute->getAttributeValue() ?: $attribute->getValue();
            }
        }

        foreach (Shop_Model_Attribute_Set::getInstance()->order('ord', 'asc')->getCollection() as $set) {
            $order[] = $set->getId();
        }

        uksort($specs, function($a, $b) use ($order){
            $valA = array_search($a, $order);
            $valB = array_search($b, $order);

            if ($valA === false) return -1;
            if ($valB === false) return 0;
            if ($valA > $valB) return 1;
            if ($valA < $valB) return -1;

            return 0;
        });

        return $specs;
    }

    /**
     * @param Shop_Model_Product_Discount $discount
     * @return bool
     */
    public function verifyDiscount(Shop_Model_Product_Discount $discount)
    {
        $cart = new Shop_Model_Cart();

        if (
            ($discount->getDiscountStartDate() !== null && time() < $discount->getDiscountStartDate()) ||
            ($discount->getDiscountEndDate() !== null && time() > $discount->getDiscountEndDate()) ||
            ($discount->getDiscountMinimumValue() !== null && !$cart->hasProductDiscount($this, $discount))
        ) {
            return false;
        }

        return true;
    }

    /**
     * @param Shop_Model_Product_Tax $tax
     * @return bool
     */
    public function verifyTax(Shop_Model_Product_Tax $tax)
    {
        if (
            ($tax->getTaxStartDate() !== null && time() < $tax->getTaxStartDate()) ||
            ($tax->getTaxEndDate() !== null && time() > $tax->getTaxEndDate()) ||
            ($tax->getTaxMaximumValue() !== null && !Shop_Model_Cart::getInstance()->hasProductTax($this, $tax))
        ) {
            return false;
        }

        return true;
    }

    /**
     * @return bool
     */
    public function addToCart()
    {
        $cart = new Shop_Model_Cart();

        $item = $cart->load([
            Session::exists('user') ? 'user_id' : 'session_id' => Session::exists('user') ? Session::get('user') : session_id(),
            'product_id' => $this->getId(),
        ]);

        if ($item === null) {
            $action = $cart->merge('insert', [
                'session_id'    => session_id(),
                'user_id'       => Session::exists('user') ? Session::get('user') : 'null',
                'product_id'    => $this->getId(),
                'quantity'      => 1
            ]);

            if ($this->getQuantity() > 0 && $action) {
                if (($vendor = $this->getVendor())/* && $this->getData('quantity') == 0*/) {
                    Database::getInstance('shop_products_vendors_ring')->update(
                        "quantity = quantity - 1"
                    )->where([
                        'id' => $vendor->getId()
                    ])->save();
                } else {
                    Database::getInstance('shop_products')->update(
                        "quantity = quantity - 1"
                    )->where([
                        'id' => $this->getId(),
                    ])->save();
                }

                $product    = $this->load($this->getId());
                $model      = new self();

                if (!$product->hasVendors() && $product->getData('quantity') == 0) {
                    $model->update([
                        'stock' => self::STOCK_NO
                    ]);
                } elseif (!$product->hasVendors() && $product->getData('quantity') <= 10) {
                    $model->update([
                        'stock' => self::STOCK_LIMITED
                    ]);
                } elseif (!$product->hasVendors() && $product->getData('quantity') > 10) {
                    $model->update([
                        'stock' => self::STOCK_YES
                    ]);
                }

                $model->where([
                    'id' => $product->getId()
                ])->save();

                $this->updateVirtualData();

                return true;
            } else {
                Flash::error('Operation failed! Please try again later.');
                return false;
            }
        } else {
            if (($product = Shop_Model_Product::getInstance()->load($item->getProductId())) && $product->getVirtualQuantity() > 0) {
                Database::getInstance('shop_products_vendors_ring')->update(
                    "quantity = quantity - 1"
                )->where([
                    'id' => $product->getVendor()->getId()
                ])->save();

                Shop_Model_Product::getInstance()->update(
                    "quantity = quantity - 1, virtual_quantity = virtual_quantity - 1"
                )->where([
                    'id' => $product->getId()
                ])->save();

                Shop_Model_Cart::getInstance()->update(
                    "quantity = quantity + 1"
                )->where([
                    'id' => $item->getId()
                ])->save();

                $p = Shop_Model_Product::getInstance()->load($product->getId());

                if ($p->getVirtualQuantity() == 0) {
                    Shop_Model_Product::getInstance()->update([
                        'stock'         => Shop_Model_Product::STOCK_NO,
                        'virtual_stock' => Shop_Model_Product::STOCK_NO
                    ]);
                } elseif ($p->getVirtualQuantity() <= 10) {
                    Shop_Model_Product::getInstance()->update([
                        'stock'         => Shop_Model_Product::STOCK_LIMITED,
                        'virtual_stock' => Shop_Model_Product::STOCK_LIMITED
                    ]);
                } elseif ($p->getVirtualQuantity() > 10) {
                    Shop_Model_Product::getInstance()->update([
                        'stock'         => Shop_Model_Product::STOCK_YES,
                        'virtual_stock' => Shop_Model_Product::STOCK_YES
                    ]);
                }

                $this->where([
                    'id' => $p->getId()
                ])->save();

                Flash::success('The product has been added to cart!');
                return true;
            }

            Flash::error('This product is already added to cart!');
            return false;
        }
    }

    /**
     * @return bool
     */
    public function removeFromCart()
    {
        $cart = new Shop_Model_Cart();

        if (Session::exists('user')) {
            $item = $cart->load([
                'user_id'       => Session::get('user'),
                'product_id'    => $this->getId()
            ]);

            $action = $cart->delete()->where([
                'user_id'       => Session::get('user'),
                'product_id'    => $this->getId()
            ])->save();
        } else {
            $item = $cart->load([
                'session_id'    => session_id(),
                'user_id'       => 'null',
                'product_id'    => $this->getId()
            ]);

            $action = $cart->delete()->where([
                'session_id'    => session_id(),
                'user_id'       => 'null',
                'product_id'    => $this->getId()
            ])->save();
        }

        if ($action) {
            if (($vendor = $this->getVendor())/* && $this->getData('quantity') == 0*/) {
                Database::getInstance('shop_products_vendors_ring')->update(
                    "quantity = quantity + {$item->getQuantity()}"
                )->where([
                    'id' => $vendor->getId()
                ])->save();
            } else {
                Database::getInstance('shop_products')->update(
                    "quantity = quantity + {$item->getQuantity()}"
                )->where([
                    'id' => $this->getId(),
                ])->save();
            }

            $product    = $this->load($this->getId());
            $model      = new self();

            if (!$product->hasVendors() && $product->getData('quantity') == 0) {
                $model->update([
                    'stock' => self::STOCK_NO
                ]);
            } elseif (!$product->hasVendors() && $product->getData('quantity') <= 10) {
                $model->update([
                    'stock' => self::STOCK_LIMITED
                ]);
            } elseif (!$product->hasVendors() && $product->getData('quantity') > 10) {
                $model->update([
                    'stock' => self::STOCK_YES
                ]);
            }

            $model->where([
                'id' => $product->getId()
            ])->save();

            $this->updateVirtualData();

            return true;
        } else {
            Flash::error('Operation failed! Please try again later.');
            return false;
        }
    }

    /**
     * @param int $quantity
     * @return bool
     * @throws Exception
     */
    public function modifyInCart($quantity)
    {
        if (!$quantity || !is_numeric($quantity)) {
            throw new Exception('You must specify a numeric value for the QUANTITY parameter');
        }

        $cart = new Shop_Model_Cart();

        if (Session::exists('user')) {
            $item = $cart->load([
                'user_id'       => Session::get('user'),
                'product_id'    => $this->getId()
            ]);

            if ($quantity > $item->getQuantity() + $this->getQuantity()) {
                Flash::error('There are only ' . $this->getQuantity() . ' more products in stock.');
                return false;
            }

            $action = $cart->merge('update', [
                'quantity' => $quantity
            ], [
                'user_id'       => Session::get('user'),
                'product_id'    => $this->getId()
            ]);
        } else {
            $item = $cart->load([
                'session_id'    => session_id(),
                'user_id'       => 'null',
                'product_id'    => $this->getId()
            ]);

            if ($quantity > $item->getQuantity() + $this->getQuantity()) {
                Flash::error('There are only ' . $this->getQuantity() . ' more products in stock.');
                return false;
            }

            $action = $cart->merge('update', [
                'quantity' => $quantity
            ], [
                'session_id'    => session_id(),
                'user_id'       => 'null',
                'product_id'    => $this->getId()
            ]);
        }

        if ($action) {
            if (($vendor = $this->getVendor())/* && $this->getData('quantity') == 0*/) {
                if ($quantity > $item->getQuantity()) {
                    Database::getInstance('shop_products_vendors_ring')->update(
                        "quantity = quantity - " . ($quantity - $item->getQuantity())
                    )->where([
                        'id' => $vendor->getId()
                    ])->save();
                } elseif ($quantity < $item->getQuantity()) {
                    Database::getInstance('shop_products_vendors_ring')->update(
                        "quantity = quantity + " . ($item->getQuantity() - $quantity)
                    )->where([
                        'id' => $vendor->getId()
                    ])->save();
                }
            } else {
                if ($quantity > $item->getQuantity()) {
                    Database::getInstance('shop_products')->update(
                        "quantity = quantity - " . ($quantity - $item->getQuantity())
                    )->where([
                        'id' => $this->getId()
                    ])->save();
                } elseif ($quantity < $item->getQuantity()) {
                    Database::getInstance('shop_products')->update(
                        "quantity = quantity + " . ($item->getQuantity() - $quantity)
                    )->where([
                        'id' => $this->getId()
                    ])->save();
                }
            }

            $product    = $this->load($this->getId());
            $model      = new self();

            if (!$product->hasVendors() && $product->getData('quantity') == 0) {
                $model->update([
                    'stock' => self::STOCK_NO
                ]);
            } elseif (!$product->hasVendors() && $product->getData('quantity') <= 10) {
                $model->update([
                    'stock' => self::STOCK_LIMITED
                ]);
            } elseif (!$product->hasVendors() && $product->getData('quantity') > 10) {
                $model->update([
                    'stock' => self::STOCK_YES
                ]);
            }

            $model->where([
                'id' => $product->getId()
            ])->save();

            $this->updateVirtualData();

            return true;
        } else {
            Flash::error('The request could not be processed! Please try again later.');
            return false;
        }
    }

    /**
     * @return bool
     */
    public function addToHistory()
    {
        if (Session::exists('user')) {
            $item = Shop_Model_Product_History::getInstance()->load([
                'user_id'       => Session::get('user'),
                'product_id'    => $this->getId()
            ]);
        } else {
            $item = Shop_Model_Product_History::getInstance()->load([
                'session_id'    => session_id(),
                'user_id'       => 'null',
                'product_id'    => $this->getId()
            ]);
        }

        if ($item) {
            if (Session::exists('user')) {
                return Shop_Model_Product_History::getInstance()->merge('update',
                    ['updated_at' => time()], ['user_id' => Session::get('user'), 'product_id' => $this->getId()]
                );
            } else {
                return Shop_Model_Product_History::getInstance()->merge('update',
                    ['updated_at' => time()], ['session_id' => session_id(), 'user_id' => 'null', 'product_id' => $this->getId()]
                );
            }
        } else {
            return Shop_Model_Product_History::getInstance()->merge('insert',
                ['session_id' => session_id(), 'user_id' => Session::exists('user') ? Session::get('user') : 'null', 'product_id' => $this->getId()]
            );
        }
    }

    /**
     * @return bool
     */
    public function addToWishList()
    {
        if (!Session::exists('user')) {
            Flash::error('You must be logged in to add a product to your WishList');
            return false;
        }

        $item = Shop_Model_WishList::getInstance()->load(['user_id' => Session::get('user'), 'product_id' => $this->getId(),]);

        if ($item === null) {
            return Shop_Model_WishList::getInstance()->merge('insert', [
                'user_id'       => Session::get('user'),
                'product_id'    => $this->getId()
            ]);
        }

        Flash::error('You already added this product to your WishList');
        return false;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function incrementViews()
    {
        return $this->update("view_count = view_count + 1")->where(['id' => $this->getId()])->save();
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function incrementSells()
    {
        return $this->update("sell_count = sell_count + 1")->where(['id' => $this->getId()])->save();
    }

    /**
     * @param array $data
     * @return bool
     */
    public function askStock($data = [])
    {
        return Shop_Model_Product_Enquiry::getInstance()->merge('insert', $data);
    }

    /**
     * @return bool|null
     */
    public function updateVirtualData()
    {
        $product = $this->load($this->getId());

        if ($this->hasVendors()/* && $this->getData('quantity') == 0 && $this->getData('stock') == self::STOCK_NO*/) {
            $vendor = $this->getVendor();

            return Database::getInstance('shop_products')->update([
                'virtual_price'     => $product->getFinalPrice(),
                'virtual_stock'     => $vendor->getQuantity() == 0 ? Shop_Model_Product::STOCK_NO : ($vendor->getQuantity() <= 10 ? Shop_Model_Product::STOCK_LIMITED : Shop_Model_Product::STOCK_YES),
                'virtual_quantity'  => $vendor->getQuantity()
            ])->where([
                'id' => $product->getId()
            ])->save();
        }

        return Database::getInstance('shop_products')->update([
            'virtual_price'     => $product->getFinalPrice(),
            'virtual_stock'     => $product->getData('stock'),
            'virtual_quantity'  => $product->getData('quantity')
        ])->where([
            'id' => $product->getId()
        ])->save();
    }

    /**
     * @return int|bool
     * @throws Exception
     */
    public function duplicate()
    {
        if ((int)$this->getId() > 0) {
            $categories = $this->getCategories();
            $vendors    = $this->getVendors();
            $attributes = $this->getAttributesJoined();
            $taxes      = $this->getTaxes();
            $discounts  = $this->getDiscounts();
            $media      = $this->getMedia();

            $database = new Database();

            try {
                $database->begin();

                $database->setTable('shop_products');
                $database->insert([
                    'currency_id'       => $this->getCurrencyId(),
                    'description'       => $this->getDescription(),
                    'price'             => $this->getData('price'),
                    'virtual_price'     => $this->getVirtualPrice(),
                    'stock'             => $this->getData('stock'),
                    'virtual_stock'     => $this->getVirtualStock(),
                    'quantity'          => $this->getData('quantity'),
                    'virtual_quantity'  => $this->getVirtualQuantity(),
                    'active'            => self::ACTIVE_NO,
                    'meta_title'        => $this->getMetaTitle(),
                    'meta_description'  => $this->getMetaDescription(),
                    'meta_keywords'     => $this->getMetaKeywords(),
                    'created_at'        => time(),
                    'updated_at'        => time(),
                ]);

                if ($id = $database->save()) {
                    if (!empty($categories)) {
                        $database->setTable('shop_products_categories_ring');

                        foreach ($categories as $category) {
                            $database->insert([
                                'category_id'   => $category->getId(),
                                'product_id'    => $id,
                            ])->save();
                        }
                    }

                    if (!empty($vendors)) {
                        $database->setTable('shop_products_vendors_ring');

                        foreach ($vendors as $vendor) {
                            $database->insert(array(
                                'product_id'    => $id,
                                'vendor_id'     => $vendor->getVendorId(),
                                'sku'           => $vendor->getSku(),
                                'quantity'      => $vendor->getQuantity(),
                                'price'         => $vendor->getPrice(),
                                'ord'           => $vendor->getOrd(),
                            ))->save();
                        }
                    }

                    if (!empty($attributes)) {
                        $database->setTable('shop_products_attributes_ring');

                        foreach ($attributes as $attribute) {
                            $database->insert([
                                'product_id'    => $id,
                                'attribute_id'  => $attribute->getId(),
                                'value'         => $attribute->getAttributeValue(),
                            ])->save();
                        }
                    }

                    if (!empty($taxes)) {
                        $database->setTable('shop_products_taxes_ring');

                        foreach ($taxes as $tax) {
                            $database->insert(array(
                                'product_id'    => $id,
                                'tax_id'        => $tax->getTaxId(),
                                'vendor_id'     => (int)$tax->getVendorId() > 0 ? $tax->getVendorId() : 'null',
                            ))->save();
                        }
                    }

                    if (!empty($discounts)) {
                        $database->setTable('shop_products_discounts_ring');

                        foreach ($discounts as $discount) {
                            $database->insert(array(
                                'product_id'    => $id,
                                'discount_id'   => $discount->getDiscountId(),
                                'vendor_id'     => (int)$discount->getVendorId() > 0 ? $discount->getVendorId() : 'null',
                            ))->save();
                        }
                    }

                    if (!empty($media)) {
                        $database->setTable('shop_products_media');

                        foreach ($media as $file) {
                            $database->insert(array(
                                'product_id'    => $id,
                                'media'         => $file->getMedia(),
                                'type'          => $file->getType(),
                                'ord'           => $file->getOrd(),
                            ))->save();
                        }
                    }
                }

                $database->commit();

                return $id;
            } catch (Exception $e) {
                $database->rollback();

                return false;
            }
        }

        return false;
    }

    /**
     * @return bool|void
     */
    public function deleteEntity()
    {
        if (Request::isGet() && Get::get('delete-id') && Get::get('delete-entity')) {
            $this->deleteUrl();

            parent::deleteEntity();
        }
    }

    /**
     * @return bool
     */
    public function deleteUrl()
    {
        if (!Database::getInstance('cms_urls')->delete()->where(['entity_id' => Get::get('delete-id'), 'entity' => Get::get('delete-entity')])->save()) {
            Flash::error('Could not delete the URL assigned for this product category!');
            Redirect::url(Url::getPreviousUrl());
        }
    }
}