<?php

class Shop_Model_Customer extends Cms_Model
{
    /**
     * @var string
     */
    protected $table = 'shop_customers';

    /**
     * @var array
     */
    protected $validates = [
        'validation' => [
            'first_name' => [
                'presence'
            ],
            'last_name' => [
                'presence',
            ],
            'email' => [
                'presence',
                'email'
            ],
            'phone' => [
                'presence',
            ],
        ],
    ];

    /**
     * @var array
     */
    protected $actsAs = [
        'timestamp'
    ];

    /**
     * @var array
     */
    protected $belongsTo = [
        'user' => [
            'model'         => 'Users_Model_User',
            'remote_key'    => 'user_id',
        ],
        'order' => [
            'model'         => 'Shop_Model_Order',
            'remote_key'    => 'order_id',
        ],
    ];

    /**
     * @set $class
     */
    public function __construct()
    {
        parent::__construct();

        $this->class = get_class();
    }
}