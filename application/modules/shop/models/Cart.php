<?php

class Shop_Model_Cart extends Cms_Model
{
    /**
     * @var string
     */
    protected $table = 'shop_cart';

    /**
     * @var array
     */
    protected $actsAs = [
        'timestamp'
    ];

    /**
     * @var array
     */
    protected $belongsTo = [
        'user' => [
            'model'         => 'Users_Model_User',
            'remote_key'    => 'user_id',
        ],
        'product' => [
            'model'         => 'Shop_Model_Product',
            'remote_key'    => 'product_id',
        ],
    ];

    /**
     * @set $class
     */
    public function __construct()
    {
        parent::__construct();

        $this->class = get_class();
    }

    /**
     * @return array|null
     */
    public function getCart()
    {
        Session::exists('user') ? $this->where(['user_id' => Session::get('user')]) : $this->where(['user_id' => 'null', 'session_id' => session_id()]);

        return $this->getCollection();
    }

    /**
     * @param string $currency
     * @return mixed
     */
    public function getTotal($currency = 'RON')
    {
        return Shop_Model_Order::getInstance()->getTotal($currency);
    }

    /**
     * @param string $currency
     * @return mixed
     */
    public function getSubTotal($currency = 'RON')
    {
        return Shop_Model_Order::getInstance()->getSubTotal($currency);
    }

    /**
     * @param string $currency
     * @return mixed
     */
    public function getGrandTotal($currency = 'RON')
    {
        return Shop_Model_Order::getInstance()->getGrandTotal($currency);
    }

    /**
     * @return int
     * @throws Exception
     */
    public function getProductsCount()
    {
        $count = 0;

        foreach ($this->getCart() as $cart) {
            $count =+ $count + $cart->getQuantity();
        }

        return $count;
    }

    /**
     * @param Shop_Model_Product $product
     * @param Shop_Model_Product_Discount $discount
     * @return bool
     */
    public function hasProductDiscount(Shop_Model_Product $product, Shop_Model_Product_Discount $discount)
    {
        $item = Session::exists('user') ?
            $this->load(['user_id' => Session::get('user'), 'product_id' => $product->getId()]) :
            $this->load(['session_id' => session_id(), 'user_id' => 'null', 'product_id' => $product->getId()]);

        if ($item && ($item->getQuantity() >= $discount->getDiscountMinimumValue())) {
            return true;
        }

        return false;
    }

    /**
     * @param Shop_Model_Product $product
     * @param Shop_Model_Product_Tax $tax
     * @return bool
     */
    public function hasProductTax(Shop_Model_Product $product, Shop_Model_Product_Tax $tax)
    {
        $item = Session::exists('user') ?
            $this->load(['user_id' => Session::get('user'), 'product_id' => $product->getId()]) :
            $this->load(['session_id' => session_id(), 'user_id' => 'null', 'product_id' => $product->getId()]);

        if ($item && ($item->getQuantity() < $tax->getTaxMaximumValue())) {
            return true;
        }

        return false;
    }
}