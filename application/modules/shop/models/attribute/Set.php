<?php

class Shop_Model_Attribute_Set extends Cms_Model
{
    /**
     * @var string
     */
    protected $table = 'shop_attributes_sets';

    /**
     * @var array
     */
    protected $validates = [
        'validation' => [
            'name' => [
                'presence',
            ],
        ],
        'unique' => [
            'name',
        ],
    ];

    /**
     * @var array
     */
    protected $actsAs = [
        'draggable',
    ];

    /**
     * @var array
     */
    protected $hasMany = [
        'attributes' => [
            'model'         => 'Shop_Model_Attribute',
            'remote_key'    => 'set_id',
            'order_field'   => 'ord',
            'order_dir'     => 'asc',
        ],
    ];

    /**
     * @var array
     */
    protected $hasAndBelongsToMany = [
        'categories' => [
            'model'         => 'Shop_Model_Category',
            'table'         => 'shop_categories_attributes_sets_ring',
            'local_key'     => 'set_id',
            'remote_key'    => 'category_id',
        ],
    ];

    /**
     * @set $class
     */
    public function __construct()
    {
        parent::__construct();

        $this->class = get_class();
    }
}