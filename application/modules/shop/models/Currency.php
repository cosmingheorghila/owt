<?php

class Shop_Model_Currency extends Cms_Model
{
    /**
     * @var string
     */
    protected $table = 'shop_currencies';

    /**
     * @var array
     */
    protected $validates = [
        'validation' => [
            'name' => [
                'presence'
            ],
            'code' => [
                'presence'
            ],
        ],
        'unique' => [
            'code'
        ],
    ];

    /**
     * @var array
     */
    protected $hasMany = [
        'products' => [
            'model'         => 'Shop_Model_Product',
            'remote_key'    => 'currency_id',
            'order_field'   => 'updated_at',
            'order_dir'     => 'desc',
        ],
    ];

    /**
     * @set $class
     */
    public function __construct()
    {
        parent::__construct();

        $this->class = get_class();
    }

    /**
     * @param int|float $amount
     * @param string $from
     * @param string $to
     * @return float
     * @throws Exception
     */
    public static function convert($amount, $from, $to)
    {
        if (!is_numeric($amount)) {
            throw new Exception('The AMOUNT parameter must be a number');
        }

        if (strtoupper($from) == strtoupper($to)) {
            return round($amount, 2);
        }

        $url        = 'http://www.google.com/finance/converter?a=' . urlencode($amount) . '&from=' . strtoupper(urlencode($from)) . '&to=' . strtoupper(urlencode($to));
        $ch         = curl_init();
        $timeout    = 0;

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);

        $rawData = curl_exec($ch);

        curl_close($ch);

        $data = explode("bld>", $rawData);
        $data = explode($to, $data[1]);

        return round($data[0], 2);
    }
}