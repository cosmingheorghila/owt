<?php

class Shop_Model_Vendor extends Cms_Model
{
    /**
     * @var string
     */
    protected $table = 'shop_vendors';

    /**
     * @var array
     */
    protected $validates = [
        'validation' => [
            'name' => [
                'presence',
            ],
        ],
        'unique' => [
            'name',
        ],
    ];

    /**
     * @var array
     */
    protected $hasAndBelongsToMany = [
        'products' => [
            'model'         => 'Shop_Model_Product',
            'table'         => 'shop_products_vendors_ring',
            'local_key'     => 'vendor_id',
            'remote_key'    => 'product_id',
            'order_field'   => 'ord',
            'order_dir'     => 'asc',
        ],
    ];

    /**
     * @set $class
     */
    public function __construct()
    {
        parent::__construct();

        $this->class = get_class();
    }

    /**
     * @return mixed
     */
    public function getFirstProduct()
    {
        return Arr::first(Shop_Model_Product::getInstance()->where(
            "id IN (SELECT product_id FROM shop_products_vendors_ring WHERE vendor_id = {$this->getId()})"
        )->order('updated_at', 'desc')->limit(1)->getCollection());
    }
}