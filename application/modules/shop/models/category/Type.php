<?php

class Shop_Model_Category_Type extends Cms_Model
{
    /**
     * @var string
     */
    protected $table = 'shop_types';

    /**
     * @var array
     */
    protected $validates = [
        'validation' => [
            'name' => [
                'presence',
            ],
        ],
        'unique' => [
            'name',
        ],
    ];

    /**
     * @var array
     */
    protected $actsAs = [
        'draggable',
    ];

    /**
     * @var array
     */
    protected $hasMany = [
        'categories' => [
            'model'         => 'Shop_Model_Category',
            'remote_key'    => 'type_id',
            'order_field'   => 'ord',
            'order_dir'     => 'asc',
        ],
    ];

    /**
     * @set $class
     */
    public function __construct()
    {
        parent::__construct();

        $this->class = get_class();
    }

    /**
     * @return bool
     */
    public function hasCurrentCategory()
    {
        foreach ($this->getCategories() as $category) {
            if (Url::getRawUrl() == $category->getUrl()) {
                return true;
            }
        }

        return false;
    }
}