<?php

class Cms_Url_CmsPages
{
    /**
     * @param string $url
     * @return array
     */
    public function resolve($url)
    {
        return [
            'module'        => 'cms',
            'controller'    => 'pages',
            'action'        => 'index',
            'id'            => $url->getEntityId(),
        ];
    }
}