<?php

class Cms_Url_ShopProducts
{
    /**
     * @param string $url
     * @return array
     */
    public function resolve($url)
    {
        return [
            'module'        => 'shop',
            'controller'    => 'index',
            'action'        => 'product',
            'id'            => $url->getEntityId(),
        ];
    }
}