<?php

class Cms_Url_ShopCategories
{
    /**
     * @param string $url
     * @return array
     */
    public function resolve($url)
    {
        return [
            'module'        => 'shop',
            'controller'    => 'index',
            'action'        => 'category',
            'id'            => $url->getEntityId(),
        ];
    }
}