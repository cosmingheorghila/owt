<?php

class Cms_Url_Cars
{
    /**
     * @param string $url
     * @return array
     */
    public function resolve($url)
    {
        return [
            'module'        => 'car',
            'controller'    => 'buy',
            'action'        => 'view',
            'id'            => $url->getEntityId(),
        ];
    }
}