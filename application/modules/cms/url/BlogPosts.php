<?php

class Cms_Url_BlogPosts
{
    /**
     * @param string $url
     * @return array
     */
    public function resolve($url)
    {
        return [
            'module'        => 'blog',
            'controller'    => 'index',
            'action'        => 'post',
            'id'            => $url->getEntityId(),
        ];
    }
}