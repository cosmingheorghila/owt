<?php

class Cms_Url_BlogCategories
{
    /**
     * @param string $url
     * @return array
     */
    public function resolve($url)
    {
        return [
            'module'        => 'blog',
            'controller'    => 'index',
            'action'        => 'category',
            'id'            => $url->getEntityId(),
        ];
    }
}