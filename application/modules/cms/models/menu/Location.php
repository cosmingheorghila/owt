<?php

class Cms_Model_Menu_Location extends Cms_Model
{
    /**
     * @var string
     */
    protected $table = 'cms_menus_locations';

    /**
     * @var array
     */
    protected $validates = [
        'validation' => [
            'location' => [
                'presence',
            ],
        ],
        'unique' => [
            'location',
        ],
    ];

    /**
     * @var array
     */
    protected $hasMany = [
        'menus' => [
            'model'         => 'Cms_Model_Menu',
            'remote_key'    => 'location_id',
        ],
    ];

    /**
     * @set $class
     */
    public function __construct()
    {
        parent::__construct();

        $this->class = get_class();
    }

    /**
     * @return bool|void
     */
    public function deleteEntity()
    {
        if (Request::isGet() && Get::exists('delete-id') && Get::exists('delete-entity')) {
            $this->verifyUsage();

            parent::deleteEntity();
        }
    }

    /**
     * @return void
     */
    public function verifyUsage()
    {
        if ($this->load(Get::get('delete-id'))->getMenus()->getCount() > 0) {
            Flash::error('Could not delete the menu location because there are menu items assigned to it!');
            Redirect::url(Url::getPreviousUrl());
        }
    }
}