<?php

class Cms_Model_Page extends Cms_Model
{
    /**
     * @var string
     */
    protected $table = 'cms_pages';

    /**
     * @var array
     */
    protected $validates = [
        'validation' => [
            'url' => [
                'presence',
            ],
            'name' => [
                'presence',
            ],
            'layout_id' => [
                'presence',
            ],
        ],
        'unique' => [
            'name',
            'identifier',
        ],
    ];

    /**
     * @var array
     */
    protected $belongsTo = [
        'layout' => [
            'model'         => 'Cms_Model_Layout',
            'remote_key'    => 'layout_id',
        ],
    ];

    /**
     * @var array
     */
    protected $actsAs = [
        'draggable',
        'timestamp',
        'url',

    ];

    /**
     * @const
     */
    const VISIBLE_NO    = 0;
    const VISIBLE_YES   = 1;

    /**
     * @var array
     */
    public static $visibilities = [
        self::VISIBLE_NO    => 'No',
        self::VISIBLE_YES   => 'Yes',
    ];

    /**
     * @set $class
     */
    public function __construct()
    {
        parent::__construct();

        $this->class = get_class();
    }

    /**
     * @return void
     */
    protected function beforeSave()
    {
        parent::beforeSave();

        if (Get::exists('parent_id') && is_numeric(Get::get('parent_id'))) {
            $this->params['parent_id'] = Get::get('parent_id');
            $parent = Cms_Model_Page::getInstance()->load($this->params['parent_id']);
        }

        if ($this->params['parent_id']) {
            if (!$this->model) {
                $this->params['url'] = ($parent->getIdentifier() == 'home' ? '/home' : $parent->getUri()) . (String::startsWith($this->params['url'], '/') ? '' : '/') . $this->params['url'];
            }
        } else {
            $this->params['url'] = (String::startsWith($this->params['url'], '/') ? '' : '/') . $this->params['url'];
        }
    }

    /**
     * @return bool
     */
    public function isChild()
    {
        return $this->getParentId() ? true : false;
    }

    /**
     * @return bool
     */
    public function isParent()
    {
        return $this->where([
            'parent_id' => $this->getId()
        ])->getCollection()->getCount() > 0 ? true : false;
    }

    /**
     * @return bool
     */
    public function isSuperParent()
    {
        return $this->getParentId() === null ? true : false;
    }

    /**
     * @return bool
     */
    public function hasChildren()
    {
        return $this->where([
            'parent_id' => $this->getId()
        ])->getCollection()->getCount() > 0 ? true : false;
    }

    /**
     * @return bool
     */
    public function hasParent()
    {
        return $this->getParentId() !== null ? true : false;
    }

    /**
     * @return bool|Cms_Model_Page
     */
    public function hasForwarding()
    {
        return $this->getModule() || $this->getController() || $this->getAction() ? $this : false;
    }
    
    /**
     * @return mixed
     */
    public function getUri()
    {
        return str_replace(Url::getPrefix(), '', $this->getUrl());
    }

    /**
     * @return array
     */
    public function getChildren()
    {
        return $this->where([
            'parent_id' => $this->getId()
        ])->getCollection();
    }

    /**
     * @return mixed
     */
    public function getParent()
    {
        return $this->load($this->getParentId());
    }

    /**
     * @return bool|void
     */
    public function deleteEntity()
    {
        if (Request::isGet() && Get::exists('delete-id') && Get::exists('delete-entity')) {
            $this->verifyUsage();
            $this->verifyChildren();
            $this->verifyForwarding();
            $this->deleteUrl();

            parent::deleteEntity();
        }
    }

    /**
     * @return void
     */
    public function verifyUsage()
    {
        $model = new Cms_Model_Menu();
        $model->where([
            'entity_id' => Get::get('delete-id'),
            'entity'    => 'page'
        ]);

        if ($model->getCollection()->getCount() > 0) {
            Flash::error('The page can not be deleted because it is assigned to menu items!');
            Redirect::url(Url::getPreviousUrl());
        }
    }

    /**
     * @return void
     */
    public function verifyChildren()
    {
        if (self::getInstance()->where(['parent_id' => Get::get('delete-id')])->getCollection()->getCount() > 0) {
            Flash::error('The page can not be deleted because it has children!');
            Redirect::url(Url::getPreviousUrl());
        }
    }

    /**
     * @return void
     */
    public function verifyForwarding()
    {
        if ($this->load(Get::get('delete-id'))->hasForwarding() && !Admin_Model_User::isDeveloper()) {
            Flash::error('The page can not be deleted because it has forwarding setup! Please ask the Developer.');
            Redirect::url(Url::getPreviousUrl());
        }
    }

    /**
     * @return bool
     */
    public function deleteUrl()
    {
        if (!Database::getInstance('cms_urls')->delete()->where(['entity_id' => Get::get('delete-id'), 'entity' => Get::get('delete-entity')])->save()) {
            Flash::error('Could not delete the URL assigned for this page!');
            Redirect::url(Url::getPreviousUrl());
        }
    }

    /**
     * @return array
     */
    public static function getTree()
    {
        $page   = new Cms_Model_Page();
        $pages  = [];

        foreach ($page->where("parent_id IS NULL")->order('ord')->getCollection() as $p1) {
            $pages[$p1->getUri()] = '>-- ' . $p1->getName();

            foreach ($p1->getChildren() as $p2) {
                $pages[$p2->getUri()] = '>-->-- ' . $p2->getName();

                foreach ($p2->getChildren() as $p3) {
                    $pages[$p3->getUri()] = '>-->-->-- ' . $p3->getName();

                    foreach ($p3->getChildren() as $p4) {
                        $pages[$p4->getUri()] = '>-->-->-->-- ' . $p4->getName();

                        foreach ($p4->getChildren() as $p5) {
                            $pages[$p5->getUri()] = '>-->-->-->-->-- ' . $p5->getName();

                            foreach ($p5->getChildren() as $p6) {
                                $pages[$p6->getUri()] = '>-->-->-->-->-->-- ' . $p6->getName();

                                foreach ($p6->getChildren() as $p7) {
                                    $pages[$p7->getUri()] = '>-->-->-->-->-->-->-- ' . $p7->getName();

                                    foreach ($p7->getChildren() as $p8) {
                                        $pages[$p8->getUri()] = '>-->-->-->-->-->-->-->-- ' . $p8->getName();

                                        foreach ($p8->getChildren() as $p9) {
                                            $pages[$p9->getUri()] = '>-->-->-->-->-->-->-->-->-- ' . $p9->getName();

                                            foreach ($p9->getChildren() as $p10) {
                                                $pages[$p10->getUri()] = '>-->-->-->-->-->-->-->-->-->-- ' . $p10->getName();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        !empty($pages) ?: $pages[''] = 'You have no pages';

        return $pages;
    }
}