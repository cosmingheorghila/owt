<?php

class Cms_Model_Newsletter_List extends Cms_Model
{
    /**
     * @var string
     */
    protected $table = 'cms_newsletter_lists';

    /**
     * @var array
     */
    protected $validates = [
        'validation' => [
            'subject' => [
                'presence',
            ],
            'message' => [
                'presence',
            ],
        ],
    ];

    /**
     * @var array
     */
    protected $actsAs = [
        'timestamp',
    ];

    /**
     * @set $class
     */
    public function __construct()
    {
        parent::__construct();

        $this->class = get_class();
    }

    /**
     * @return bool
     */
    public function send()
    {
        $email      = new Email('postmark');
        $addresses  = [];

        foreach (Cms_Model_Newsletter_Subscriber::getInstance()->getCollection() as $subscriber) {
            $addresses[] = $subscriber->getEmail();
        }

        $email->from(
            Setting::get('company-email')->getValue()
        )->to(
            implode(',', $addresses)
        )->subject(
            $this->getSubject()
        )->messageHtml(
            'emails/newsletter.phtml', [
                'theMessage' => $this->getMessage()
            ]
        );

        return $email->send();
    }
}