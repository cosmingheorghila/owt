<?php

class Cms_Model_Newsletter_Subscriber extends Cms_Model
{
    /**
     * @var string
     */
    protected $table = 'cms_newsletter_subscribers';

    /**
     * @var array
     */
    protected $validates = [
        'validation' => [
            'email' => [
                'presence',
                'email',
            ],
        ],
        'unique' => [
            'email',
        ],
    ];

    /**
     * @var array
     */
    protected $actsAs = [
        'timestamp',
    ];

    /**
     * @set $class
     */
    public function __construct()
    {
        parent::__construct();

        $this->class = get_class();
    }

    /**
     * @param string|email $email
     * @return bool
     */
    public function subscribe($email)
    {
        if (!$email) {
            Flash::error('Trebuie sa specifici un email valid!');
            return false;
        }

        return $this->merge('insert', [
            'email' => $email
        ]);
    }
}