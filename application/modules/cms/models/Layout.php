<?php

class Cms_Model_Layout extends Cms_Model
{
    /**
     * @var string
     */
    protected $table = 'cms_layouts';

    /**
     * @var array
     */
    protected $validates = [
        'validation' => [
            'file' => [
                'presence',
            ],
            'name' => [
                'presence',
            ],
        ],
        'unique' => [
            'identifier',
        ],
    ];

    /**
     * @var array
     */
    protected $hasMany = [
        'pages' => [
            'model'         => 'Cms_Model_Page',
            'remote_key'    => 'layout_id',
            'order_field'   => 'ord',
            'order_fir'     => 'asc',
        ],
    ];

    /**
     * @var array
     */
    public static $layouts  = [];
    public static $files    = [];

    /**
     * @set $class
     */
    public function __construct()
    {
        parent::__construct();

        $this->class = get_class();
    }

    /**
     * @return void
     */
    protected function beforeSave()
    {
        parent::beforeSave();

        if (!$this->params['file']) {
            Session::set('data', $this->params);

            Flash::error('You must have a layout file in order to create a layout!');
            Redirect::url(Url::getPreviousUrl());
        }
    }

    /**
     * @return array
     */
    public static function getLayouts()
    {
        foreach (Cms_Model_Layout::getInstance()->getCollection() as $layout) {
            self::$layouts[$layout->getId()] = $layout->getName();
        }

        if (empty(self::$layouts)) {
            self::$layouts['null'] = 'You have no layouts';
        }

        return self::$layouts;
    }

    /**
     * @return array
     */
    public static function getLayoutFiles()
    {
        foreach (glob('application/layouts/default/*.phtml') as $layout) {
            if (!String::startsWith(Arr::last(explode('/', $layout)), '_')) {
                self::$files[Arr::last(explode('/', $layout))] = Arr::last(explode('/', $layout));
            }
        }

        if (empty(self::$files)) {
            self::$files[''] = 'You have no layout files';
        }

        return self::$files;
    }

    /**
     * @param int $id
     * @return array
     * @throws Exception
     */
    public static function getBlocksLocations($id)
    {
        if (String::contains(Url::getUrl(), 'layouts')) {
            $layout = Cms_Model_Layout::getInstance()->load($id);
        } elseif (String::contains(Url::getUrl(), 'pages')) {
            $page   = Cms_Model_Page::getInstance()->load($id);
            $layout = Cms_Model_Layout::getInstance()->load($page->getLayoutId());
        } else {
            throw new Exception('BlockHolder behavior only available on LAYOUTS & PAGES');
        }

        $locations = [];

        foreach (explode(',', $layout->getBlockLocations()) as $section) {
            $locations[] = explode(' => ', trim($section))[0];
        }

        return $locations;
    }

    /**
     * @return bool|void
     */
    public function deleteEntity()
    {
        if (Request::isGet() && Get::exists('delete-id') && Get::exists('delete-entity')) {
            $this->verifyUsage();

            parent::deleteEntity();
        }
    }

    /**
     * @return void
     */
    public function verifyUsage()
    {
        if ($this->load(Get::get('delete-id'))->getPages()->getCount() > 0) {
            Flash::error('Could not delete the layout because there are pages that have this layout assigned!');
            Redirect::url(Url::getPreviousUrl());
        }
    }
}