<?php

class Cms_Model_Menu extends Cms_Model
{
    /**
     * @var string
     */
    protected $table = 'cms_menus';

    /**
     * @var array
     */
    protected $validates = [
        'validation' => [
            'entity' => [
                'presence',
            ],
            'name' => [
                'presence',
            ],
        ],
    ];

    /**
     * @var array
     */
    protected $belongsTo = [
        'location' => [
            'model'         => 'Cms_Model_Menu_Location',
            'remote_key'    => 'location_id',
        ],
    ];

    /**
     * @var array
     */
    protected $actsAs = [
        'draggable' => [
            'field' => 'location_id',
        ],
    ];

    /**
     * @const
     */
    const TYPE_NONE = '';
    const TYPE_URL  = 'url';
    const TYPE_PAGE = 'page';

    /**
     * @const
     */
    const WINDOW_SAME   = 0;
    const WINDOW_NEW    = 1;

    /**
     * @var array
     */
    public static $types = [
        self::TYPE_NONE => 'Please select',
        self::TYPE_URL  => 'URL',
        self::TYPE_PAGE => 'Page',
    ];

    /**
     * @var array
     */
    public static $windows = [
        self::WINDOW_SAME   => 'No',
        self::WINDOW_NEW    => 'Yes',
    ];

    /**
     * @set $class
     */
    public function __construct()
    {
        parent::__construct();

        $this->class = get_class();
    }

    /**
     * @return bool
     */
    public function isChild()
    {
        return $this->getParentId() ? true : false;
    }

    /**
     * @return bool
     */
    public function isParent()
    {
        return $this->where([
            'parent_id' => $this->getId()
        ])->getCollection()->getCount() > 0 ? true : false;
    }

    /**
     * @return bool
     */
    public function isSuperParent()
    {
        return $this->getParentId() === null ? true : false;
    }

    /**
     * @return bool
     */
    public function hasChildren()
    {
        return $this->where([
            'parent_id' => $this->getId()
        ])->getCollection()->getCount() > 0 ? true : false;
    }

    /**
     * @return bool
     */
    public function hasParent()
    {
        return $this->getParentId() !== null ? true : false;
    }

    /**
     * @return array
     */
    public function getChildren()
    {
        return $this->where([
            'parent_id' => $this->getId()
        ])->getCollection();
    }

    /**
     * @return mixed
     */
    public function getParent()
    {
        return $this->load($this->getParentId());
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        if (String::startsWith($this->getData('url'), 'http') || String::startsWith($this->getData('url'), 'www') || String::startsWith($this->getData('url'), '#')) {
            return $this->getData('url');
        }

        return Url::getPrefix() . $this->getData('url');
    }

    /**
     * @return bool|void
     */
    public function deleteEntity()
    {
        if (Request::isGet() && Get::exists('delete-id') && Get::exists('delete-entity')) {
            $this->verifyChildren();

            parent::deleteEntity();
        }
    }

    /**
     * @return void
     */
    public function verifyChildren()
    {
        if (self::getInstance()->where(['parent_id' => Get::get('delete-id')])->getCollection()->getCount() > 0) {
            Flash::error('The menu can not be deleted because it has children!');
            Redirect::url(Url::getPreviousUrl());
        }
    }
}