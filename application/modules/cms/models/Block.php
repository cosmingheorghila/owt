<?php

class Cms_Model_Block extends Cms_Model
{
    /**
     * @var array
     */
    public static $types = [
        'sell_menu' => [
            'label' => 'Sell Menu',
            'class' => 'SellMenu',
        ],
        'full_image' => [
            'label' => 'Full Image',
            'class' => 'FullImage',
        ],
        'team' => [
            'label' => 'Team',
            'class' => 'Team',
        ],
        'home_1' => [
            'label' => 'Home 1',
            'class' => 'Home1',
        ],
        'home_2' => [
            'label' => 'Home 2',
            'class' => 'Home2',
        ],
        'home_3' => [
            'label' => 'Home 3',
            'class' => 'Home3',
        ],
        'home_4' => [
            'label' => 'Home 4',
            'class' => 'Home4',
        ],
        'sell_1' => [
            'label' => 'Sell 1',
            'class' => 'Sell1',
        ],
        'sell_2' => [
            'label' => 'Sell 2',
            'class' => 'Sell2',
        ],
        'sell_3' => [
            'label' => 'Sell 3',
            'class' => 'Sell3',
        ],
        'sell_4' => [
            'label' => 'Sell 4',
            'class' => 'Sell4',
        ],
        'sell_5' => [
            'label' => 'Sell 5',
            'class' => 'Sell5',
        ],
        'concept_1' => [
            'label' => 'Concept 1',
            'class' => 'Concept1',
        ],
        'concept_2' => [
            'label' => 'Concept 2',
            'class' => 'Concept2',
        ],
        'concept_3' => [
            'label' => 'Concept 3',
            'class' => 'Concept3',
        ],
        'concept_4' => [
            'label' => 'Concept 4',
            'class' => 'Concept4',
        ],
        'concept_5' => [
            'label' => 'Concept 5',
            'class' => 'Concept5',
        ],
        'concept_6' => [
            'label' => 'Concept 6',
            'class' => 'Concept6',
        ],
        'concept_7' => [
            'label' => 'Concept 7',
            'class' => 'Concept7',
        ],
        'city_error_1' => [
            'label' => 'City Error 1',
            'class' => 'CityError1',
        ],
        'km_error_1' => [
            'label' => 'KM Error 1',
            'class' => 'KmError1',
        ],
        'about_1' => [
            'label' => 'About 1',
            'class' => 'About1',
        ],
        'about_2' => [
            'label' => 'About 2',
            'class' => 'About2',
        ],
        'about_3' => [
            'label' => 'About 3',
            'class' => 'About3',
        ],
        'area_1' => [
            'label' => 'Area 1',
            'class' => 'Area1',
        ],
        'order_1' => [
            'label' => 'Order 1',
            'class' => 'Order1',
        ],
        'support_1' => [
            'label' => 'Support 1',
            'class' => 'Support1',
        ],
        'support_2' => [
            'label' => 'Support 2',
            'class' => 'Support2',
        ],
        'support_3' => [
            'label' => 'Support 3',
            'class' => 'Support3',
        ],
        'video' => [
            'label' => 'Video',
            'class' => 'Video',
        ],
        'sell_success_1' => [
            'label' => 'Sell Success 1',
            'class' => 'SellSuccess1',
        ],
        'vin_error_1' => [
            'label' => 'Vin Error 1',
            'class' => 'VinError1',
        ],
        'header_menu' => [
            'label' => 'Header Menu',
            'class' => 'HeaderMenu',
        ],
        'faq_content' => [
            'label' => 'Faq Content',
            'class' => 'FaqContent',
        ],
        'faq_questions' => [
            'label' => 'Faq Questions',
            'class' => 'FaqQuestions',
        ],
        'faq_contact' => [
            'label' => 'Faq Contact',
            'class' => 'FaqContact',
        ],
        'rethinked' => [
            'label' => 'Rethinked',
            'class' => 'Rethinked',
        ],
        'content' => [
            'label' => 'Content',
            'class' => 'Content',
        ],
    ];

    /**
     * @var string
     */
    protected $table = 'cms_blocks';

    /**
     * @var array
     */
    protected $validates = [
        'validation' => [
            'name' => [
                'presence',
            ],
        ],
    ];

    /**
     * @var array
     */
    protected $actsAs = [
        'metadata',
    ];

    /**
     * @set $class
     */
    public function __construct()
    {
        parent::__construct();

        $this->class = get_class();
    }

    /**
     * @return void
     */
    protected function beforeSave()
    {
        parent::beforeSave();

        foreach ($this->params as $key => $val) {
            if (is_array($val)) {
                $this->params[$key] = implode(',', $val);
            }
        }

        if (!$this->model) {
            $this->params['type'] = Session::get('block-type');

            Session::disable('block-type');
        }
    }
}