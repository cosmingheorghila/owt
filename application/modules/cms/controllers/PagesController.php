<?php

class Cms_PagesController extends Controller
{
    /**
     * @return void
     */
    public function indexAction()
    {
        $model  = new Cms_Model_Page();
        $id     = $this->getParam('id');

        if ($id && $page = $model->load($id)) {
            $this->visible($page);

            if ($page->hasForwarding()) {
                $this->forwarding($page);
            } else {
                $this->setLayout('default/' . $page->getLayout()->getFile());
                $this->setView('cms/views/pages/index.phtml');

                $this->assign([
                    'layout'    => $page->getLayout(),
                    'page'      => $page
                ]);
            }

            $this->meta($page);
        } else {
            $this->redirectPage('not-found');
        }
    }

    /**
     * @param Cms_Model_Page $page
     * @return void
     */
    private function visible(Cms_Model_Page $page)
    {
        if ($page->getVisible() != 1) {
            Redirect::page('not-found');
        }
    }

    /**
     * @param Cms_Model_Page $page
     * @throws Exception
     * @return void
     */
    private function forwarding(Cms_Model_Page $page)
    {
        $mvc = $this->mvc([
            'module'        => $page->getModule(),
            'controller'    => $page->getController(),
            'action'        => $page->getAction()
        ]);

        if (!File::exists($mvc['fullControllerPath'])) {
            throw new Exception('CONTROLLER file not found: ' . $mvc['fullControllerPath']);
        }

        /*if (!File::exists($mvc['fullViewPath'])) {
            throw new Exception('VIEW file not found: ' . $mvc['fullViewPath']);
        }*/

        $this->render($mvc['fullControllerPath']);
        $class = new $mvc['controllerClass']();

        if (!method_exists($class, $mvc['controllerAction'])) {
            throw new Exception('Undefined METHOD ' . $mvc['controllerAction'] . ' in CONTROLLER ' . $mvc['fullControllerPath']);
        }

        if (method_exists($class, 'init') && !in_array($mvc['controllerAction'], $class->freeActions)) {
            $class->init();
        }

        $class->{$mvc['controllerAction']}();


        $this->setLayout('default/' . $page->getLayout()->getFile());
        $this->setFullView($mvc['fullViewPath']);
    }

    /**
     * @param Cms_Model_Page $page
     * @return void
     */
    private function meta(Cms_Model_Page $page)
    {
        $metaTitle          = $page->getMetaTitle();
        $metaDescription    = $page->getMetaDescription();
        $metaKeywords       = $page->getMetaKeywords();

        if (isset($metaTitle) && !empty($metaTitle)) {
            !empty(Bootstrap::getInstance()->meta['title']) ?: $this->setMeta('title', $page->getMetaTitle());
        } else {
            !empty(Bootstrap::getInstance()->meta['title']) ?: $this->setMeta('title', $page->getName());
        }

        if (isset($metaDescription) && !empty($metaDescription)) {
            !empty(Bootstrap::getInstance()->meta['description']) ?: $this->setMeta('description', $page->getMetaDescription());
        } else {
            !empty(Bootstrap::getInstance()->meta['description']) ?: $this->setMeta('description', null);
        }

        if (isset($metaKeywords) && !empty($metaKeywords)) {
            !empty(Bootstrap::getInstance()->meta['keywords']) ?: $this->setMeta('keywords', $page->getMetaKeywords());
        } else {
            !empty(Bootstrap::getInstance()->meta['keywords']) ?: $this->setMeta('keywords', null);
        }
    }
}