<?php

class Cms_Block_About1 extends Behavior_Block
{
    /**
     * {@inheritDoc}
     */
    protected $template = 'about_1.phtml';

    /**
     * @param Model|Database $model
     * @return void
     */
    protected function execute($model = null)
    {
        $this->set('data', $this->metadata($model));
    }
}