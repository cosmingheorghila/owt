<?php

class Cms_Block_Sell2 extends Behavior_Block
{
    /**
     * {@inheritDoc}
     */
    protected $template = 'sell_2.phtml';

    /**
     * @param Model|Database $model
     * @return void
     */
    protected function execute($model = null)
    {
        $this->set('data', $this->metadata($model));
    }
}