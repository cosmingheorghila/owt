<?php

class Cms_Block_Sell3 extends Behavior_Block
{
    /**
     * {@inheritDoc}
     */
    protected $template = 'sell_3.phtml';

    /**
     * @param Model|Database $model
     * @return void
     */
    protected function execute($model = null)
    {
        $this->set('data', $this->metadata($model));
    }
}