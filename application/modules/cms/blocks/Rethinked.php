<?php

class Cms_Block_Rethinked extends Behavior_Block
{
    /**
     * {@inheritDoc}
     */
    protected $template = 'rethinked.phtml';

    /**
     * @param Model|Database $model
     * @return void
     */
    protected function execute($model = null)
    {
        $this->set('data', $this->metadata($model));
    }
}