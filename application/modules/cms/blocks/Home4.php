<?php

class Cms_Block_Home4 extends Behavior_Block
{
    /**
     * {@inheritDoc}
     */
    protected $template = 'home_4.phtml';

    /**
     * @param Model|Database $model
     * @return void
     */
    protected function execute($model = null)
    {
        $this->set('posts', Blog_Model_Post::getLatestPosts());
    }
}