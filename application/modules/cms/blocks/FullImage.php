<?php

class Cms_Block_FullImage extends Behavior_Block
{
    /**
     * {@inheritDoc}
     */
    protected $template = 'full_image.phtml';

    /**
     * @param Model|Database $model
     * @return void
     */
    protected function execute($model = null)
    {
        $this->set('data', $this->metadata($model));
    }
}