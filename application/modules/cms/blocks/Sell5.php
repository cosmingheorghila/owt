<?php

class Cms_Block_Sell5 extends Behavior_Block
{
    /**
     * {@inheritDoc}
     */
    protected $template = 'sell_5.phtml';

    /**
     * @param Model|Database $model
     * @return void
     */
    protected function execute($model = null)
    {
        $this->set('data', $this->metadata($model));
    }
}