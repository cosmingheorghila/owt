<?php

class Cms_Block_SellMenu extends Behavior_Block
{
    /**
     * {@inheritDoc}
     */
    protected $template = 'sell_menu.phtml';

    /**
     * @param Model|Database $model
     * @return void
     */
    protected function execute($model = null)
    {
        $this->set('sell_menu', Cms_Model_Menu::getInstance()->where(['location_id' => 3])->order('ord')->getCollection());
    }
}