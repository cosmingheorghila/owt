<?php

class Cms_Block_Home1 extends Behavior_Block
{
    /**
     * {@inheritDoc}
     */
    protected $template = 'home_1.phtml';

    /**
     * @param Model|Database $model
     * @return void
     */
    protected function execute($model = null)
    {
        $this->set('data', $this->metadata($model));
    }
}