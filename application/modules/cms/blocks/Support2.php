<?php

class Cms_Block_Support2 extends Behavior_Block
{
    /**
     * {@inheritDoc}
     */
    protected $template = 'support_2.phtml';

    /**
     * @param Model|Database $model
     * @return void
     */
    protected function execute($model = null)
    {
        $this->set('data', $this->metadata($model));
    }
}