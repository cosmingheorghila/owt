<?php

class Cms_Block_FaqQuestions extends Behavior_Block
{
    /**
     * {@inheritDoc}
     */
    protected $template = 'faq_questions.phtml';

    /**
     * @param Model|Database $model
     * @return void
     */
    protected function execute($model = null)
    {
        $this->set('data', $this->metadata($model));
    }
}