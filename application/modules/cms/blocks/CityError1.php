<?php

class Cms_Block_CityError1 extends Behavior_Block
{
    /**
     * {@inheritDoc}
     */
    protected $template = 'city_error_1.phtml';

    /**
     * @param Model|Database $model
     * @return void
     */
    protected function execute($model = null)
    {
        $this->set('data', $this->metadata($model));
        $this->set('form', new Form());
    }
}