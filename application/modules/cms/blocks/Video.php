<?php

class Cms_Block_Video extends Behavior_Block
{
    /**
     * {@inheritDoc}
     */
    protected $template = 'video.phtml';

    /**
     * @param Model|Database $model
     * @return void
     */
    protected function execute($model = null)
    {
        $this->set('data', $this->metadata($model));
    }
}