<?php

class Cms_Block_Test extends Behavior_Block
{
    /**
     * {@inheritDoc}
     */
    protected $template = 'test.phtml';

    /**
     * @param Model|Database $model
     * @return void
     */
    protected function execute($model = null)
    {
        $this->set('data', $this->metadata($model));
    }
}