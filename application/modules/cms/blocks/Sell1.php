<?php

class Cms_Block_Sell1 extends Behavior_Block
{
    /**
     * {@inheritDoc}
     */
    protected $template = 'sell_1.phtml';

    /**
     * @param Model|Database $model
     * @return void
     */
    protected function execute($model = null)
    {
        $this->set('slides', Default_Model_SellSlide::getInstance()->order('ord')->getCollection());
    }
}