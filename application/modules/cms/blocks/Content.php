<?php

class Cms_Block_Content extends Behavior_Block
{
    /**
     * {@inheritDoc}
     */
    protected $template = 'content.phtml';

    /**
     * @param Model|Database $model
     * @return void
     */
    protected function execute($model = null)
    {
        $this->set('data', $this->metadata($model));
    }
}