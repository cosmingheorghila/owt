<?php

class Cms_Block_TopMenu extends Behavior_Block
{
    /**
     * {@inheritDoc}
     */
    protected $template = 'top_menu.phtml';

    /**
     * @param Model|Database $model
     * @return void
     */
    protected function execute($model = null)
    {
        $this->set('top_menu', Cms_Model_Menu::getInstance()->where(['location_id' => 1])->order('ord')->getCollection());
    }
}