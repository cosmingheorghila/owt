<?php

class Cms_Block_StepBreadcrumb extends Behavior_Block
{
    /**
     * {@inheritDoc}
     */
    protected $template = 'step_breadcrumb.phtml';

    /**
     * @param Model|Database $model
     * @return void
     */
    protected function execute($model = null)
    {
        if (Cookie::exists('ad')) {
            $this->set('ad', Car_Model_Ad::getInstance()->load(Cookie::get('ad')));
        }
    }
}