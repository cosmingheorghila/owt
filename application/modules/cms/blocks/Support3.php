<?php

class Cms_Block_Support3 extends Behavior_Block
{
    /**
     * {@inheritDoc}
     */
    protected $template = 'support_3.phtml';

    /**
     * @param Model|Database $model
     * @return void
     */
    protected function execute($model = null)
    {
        $this->set('data', $this->metadata($model));
    }
}