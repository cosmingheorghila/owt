<?php

class Cms_Block_HeaderMenu extends Behavior_Block
{
    /**
     * {@inheritDoc}
     */
    protected $template = 'header_menu.phtml';

    /**
     * @param Model|Database $model
     * @return void
     */
    protected function execute($model = null)
    {
        $this->set('data', $this->metadata($model));
    }
}