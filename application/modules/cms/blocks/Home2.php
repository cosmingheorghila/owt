<?php

class Cms_Block_Home2 extends Behavior_Block
{
    /**
     * {@inheritDoc}
     */
    protected $template = 'home_2.phtml';

    /**
     * @param Model|Database $model
     * @return void
     */
    protected function execute($model = null)
    {
        $this->set('partners', Default_Model_Partner::getInstance()->order('ord')->getCollection());
    }
}