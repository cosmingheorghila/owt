<?php

class Cms_Block_Order1 extends Behavior_Block
{
    /**
     * {@inheritDoc}
     */
    protected $template = 'order_1.phtml';

    /**
     * @param Model|Database $model
     * @return void
     */
    protected function execute($model = null)
    {
        $this->set('form', new Form());
        $this->set('data', $this->metadata($model));
    }
}