<?php

class Cms_Block_BottomMenu extends Behavior_Block
{
    /**
     * {@inheritDoc}
     */
    protected $template = 'bottom_menu.phtml';

    /**
     * @param Model|Database $model
     * @return void
     */
    protected function execute($model = null)
    {
        $this->set('bottom_menu', Cms_Model_Menu::getInstance()->where(['location_id' => 2])->order('ord')->getCollection());
    }
}