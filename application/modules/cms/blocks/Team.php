<?php

class Cms_Block_Team extends Behavior_Block
{
    /**
     * {@inheritDoc}
     */
    protected $template = 'team.phtml';

    /**
     * @param Model|Database $model
     * @return void
     */
    protected function execute($model = null)
    {
        $this->set('team', Default_Model_Team::getInstance()->order('ord')->getCollection());
        $this->set('data', $this->metadata($model));
    }
}