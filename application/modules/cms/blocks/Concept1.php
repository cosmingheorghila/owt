<?php

class Cms_Block_Concept1 extends Behavior_Block
{
    /**
     * {@inheritDoc}
     */
    protected $template = 'concept_1.phtml';

    /**
     * @param Model|Database $model
     * @return void
     */
    protected function execute($model = null)
    {
        $this->set('data', $this->metadata($model));
    }
}