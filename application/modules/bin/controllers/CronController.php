<?php

class Bin_CronController extends Controller
{
    /**
     * @return void
     */
    public function databaseBackupsAction()
    {
        $this->disableView();
        $this->disableLayout();

        foreach (glob(Dir::getBackups() . '/*') as $file) {
            File::delete($file);
        }

        print('Backups cleaned successfully');
    }

    /**
     * @return void
     */
    public function shopCartAction()
    {
        $this->disableView();
        $this->disableLayout();

        Shop_Model_Cart::getInstance()->delete()->where(
            "(user_id IS NULL AND created_at < '" . strtotime("-1 day", time()) . "') OR (user_id IS NOT NULL AND created_at < '" . strtotime("-1 month", time()) . "')"
        )->save();

        print('Cart cleaned successfully');
    }

    /**
     * @return void
     */
    public function shopWishListAction()
    {
        $this->disableView();
        $this->disableLayout();

        Shop_Model_WishList::getInstance()->delete()->where(
            "created_at < '" . strtotime("-1 month", time()) . "'"
        )->save();

        print('WishList cleaned successfully');
    }

    /**
     * @return void
     */
    public function shopProductsHistoryAction()
    {
        $this->disableView();
        $this->disableLayout();

        Shop_Model_Product_History::getInstance()->delete()->where(
            "(user_id IS NULL AND created_at < '" . strtotime("-1 day", time()) . "') OR (user_id IS NOT NULL AND created_at < '" . strtotime("-1 month", time()) . "')"
        )->save();

        print('Products History cleaned successfully');
    }
}