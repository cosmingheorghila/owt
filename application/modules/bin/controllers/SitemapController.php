<?php

class Bin_SitemapController extends Controller
{
    /**
     * @return void
     */
    public function indexAction()
    {
        $this->disableView();
        $this->disableLayout();

        $map = new Sitemap();

        Http_Auth::authorize(function() use($map) {
            $map->build();

            print('Sitemap generated successfully!');
        });
    }
}