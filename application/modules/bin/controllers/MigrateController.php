<?php

class Bin_MigrateController extends Controller
{
    /**
     * @return void
     */
    public function indexAction()
    {
        $this->disableView();
        $this->disableLayout();

        $migration = new Migration();

        Http_Auth::authorize(function() use($migration) {
            $migration->run();
        });
    }

    /**
     * @return void
     */
    public function newAction()
    {
        $this->disableView();
        $this->disableLayout();

        $migration = new Migration();

        Http_Auth::authorize(function() use($migration) {
            $migration->create();
        });
    }

    /**
     * @return void
     */
    public function rollbackAction()
    {
        $this->disableView();
        $this->disableLayout();

        $migration = new Migration();

        Http_Auth::authorize(function() use ($migration) {
            $migration->rollback();
        });
    }
}