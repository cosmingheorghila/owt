<?php

class Default_Model_City extends Cms_Model
{
    /**
     * @var string
     */
    protected $table = 'cms_cities';

    /**
     * @var array
     */
    protected $validates = [
        'validation' => [
            'name' => [
                'presence',
            ],
        ],
    ];

    /**
     * @var array
     */
    protected $belongsTo = [
        'county' => [
            'model'         => 'Default_Model_County',
            'remote_key'    => 'county_id',
        ],
    ];

    /**
     * @const
     */
    const VALID_NO  = 0;
    const VALID_YES = 1;

    /**
     * @var array
     */
    public static $valid = [
        self::VALID_NO  => 'No',
        self::VALID_YES => 'Yes',
    ];

    /**
     * @set $class
     */
    public function __construct()
    {
        parent::__construct();

        $this->class = get_class();
    }

    /**
     * @param int|null $county
     * @return array
     */
    public static function toAutoComplete($county = null)
    {
        $model  = new self();
        $cities = [];

        $cities['suggestions']  = [];
        $cities['data']         = [];

        if ($county) {
            $model->where([
                'county_id' => $county
            ]);
        }

        foreach ($model->search(Get::get('query'), ['name'])->order('name')->getCollection() as $city) {
            $cities['suggestions'][] = $cities['data'][] = [
                'id'    => $city->getId(),
                'value' => $city->getName(),
            ];
        }

        return $cities;
    }
}