<?php

class Default_Model_County extends Cms_Model
{
    /**
     * @var string
     */
    protected $table = 'cms_counties';

    /**
     * @var array
     */
    protected $validates = [
        'validation' => [
            'name' => [
                'presence',
            ],
        ],
    ];

    /**
     * @var array
     */
    protected $belongsTo = [
        'country' => [
            'model'         => 'Default_Model_Country',
            'remote_key'    => 'county_id',
        ],
    ];

    /**
     * @var array
     */
    protected $hasMany = [
        'cities' => [
            'model'         => 'Default_Model_City',
            'remote_key'    => 'county_id',
            'order_field'   => 'name',
            'order_dir'     => 'asc',
        ],
    ];

    /**
     * @set $class
     */
    public function __construct()
    {
        parent::__construct();

        $this->class = get_class();
    }

    /**
     * @return array
     */
    public static function toAutoComplete()
    {
        $model      = new self();
        $counties   = [];

        $counties['suggestions']  = [];
        $counties['data']         = [];

        foreach ($model->search(Get::get('query'), ['name'])->order('name')->getCollection() as $county) {
            $counties['suggestions'][] = $counties['data'][] = [
                'id'    => $county->getId(),
                'value' => $county->getName(),
                'cost' => $county->getDeliveryCost(),
            ];
        }

        return $counties;
    }
}