<?php

class Default_Model_Country extends Cms_Model
{
    /**
     * @var string
     */
    protected $table = 'cms_countries';

    /**
     * @var array
     */
    protected $validates = [
        'validation' => [
            'name' => [
                'presence',
            ],
        ],
    ];

    /**
     * @var array
     */
    protected $hasMany = [
        'counties' => [
            'model'         => 'Default_Model_County',
            'remote_key'    => 'country_id',
            'order_field'   => 'name',
            'order_dir'     => 'asc',
        ],
    ];

    /**
     * @set $class
     */
    public function __construct()
    {
        parent::__construct();

        $this->class = get_class();
    }
}