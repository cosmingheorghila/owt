<?php

class Default_Model_Team extends Cms_Model
{
    /**
     * @var string
     */
    protected $table = 'team';

    /**
     * @var array
     */
    protected $validates = [
        'validation' => [
            'first_name' => [
                'presence',
            ],
            'last_name' => [
                'presence',
            ],
            'job_title' => [
                'presence',
            ],
        ],
    ];

    /**
     * @var array
     */
    protected $actsAs = [
        'draggable',
        'timestamp',
    ];

    /**
     * @set $class
     */
    public function __construct()
    {
        parent::__construct();

        $this->class = get_class();
    }
}