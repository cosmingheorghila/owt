<?php

class Default_Model_BuySlide extends Cms_Model
{
    /**
     * @var string
     */
    protected $table = 'buy_slides';

    /**
     * @var array
     */
    protected $validates = [
        'validation' => [
            'name' => [
                'presence',
            ],
        ],
    ];

    /**
     * @var array
     */
    protected $actsAs = [
        'draggable',
        'timestamp',
    ];

    /**
     * @set $class
     */
    public function __construct()
    {
        parent::__construct();

        $this->class = get_class();
    }

    /**
     * @return array|mixed
     * @throws Exception
     */
    public function getRandom()
    {
        $randoms = $this->select()->order('Rand()')->limit(1)->getCollection();

        if ($randoms->getCount()) {
            return Arr::first($randoms->getData());
        }

        return null;
    }
}