<?php

class Default_ContactController extends Cms_Controller
{
    /**
     * @return void
     */
    public function mailAction()
    {
        if ($this->getRequest()->isPost()) {
            $data = Post::get('data');
            $mail = new Email('postmark');

            if ($data['name'] == '' || $data['email'] == '' || $data['phone'] == '' || $data['message'] == '') {
                Session::set('inputData', $data);
                Flash::error('Se pare ca ai omis ceva. Completeaza, te rog, toate campurile.');
                Redirect::url(Url::getPreviousUrl());
            }

            $mail->from(
                Setting::get('company-email')->getValue()
            )->to(
                Setting::get('company-email')->getValue()
            )->subject(
                'Support Mail Request'
            )->messageHtml(
                'emails/support.phtml', [
                    'theName' => $data['name'],
                    'theEmail' => $data['email'],
                    'thePhone' => $data['phone'],
                    'theMessage' => $data['message'],
                ]
            );

            if ($mail->send()) {
                Flash::success('Am primit mesaujul tau. Ofera-ne cateva ore pentru a-ti raspunde.');
            } else {
                Session::set('inputData', $data);
                Flash::error('Din pacate, mesajul nu a fost trimis. Te rugam, incearca din nou.');
            }

            Redirect::url(Url::getPreviousUrl());
        }

        Redirect::url(Url::getPreviousUrl());
    }
}