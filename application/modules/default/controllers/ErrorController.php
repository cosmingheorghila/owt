<?php

class Default_ErrorController extends Cms_Controller
{
    /**
     * @return void
     */
    public function notFoundAction()
    {
        header("HTTP/1.0 404 Not Found");
    }
}