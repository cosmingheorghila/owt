<?php

class Admin_Blog_CategoriesController extends Admin_Controller
{
    /**
     * @var Model
     */
    public $modelClass = 'Blog_Model_Category';

    /**
     * @var string
     */
    public $redirectUrl = 'blog/categories/index';

    /**
     * @set Model
     * @return void
     */
    public function init()
    {
        $this->model = $this->getNewModel();
    }

    /**
     * @return void
     */
    public function indexAction()
    {
        $this->records = $this->model->getRecords([
            'name'
        ], null, 'ord', 'asc');

        $this->paginate     = false;
        $this->movable      = true;
        $this->title        = 'Categories';
        $this->breadcrumbs  = [
            'Blog Area',
            'Categories',
        ];

        parent::index($this->model);
    }

    /**
     * @return void
     */
    public function addAction()
    {
        $this->title        = 'Add Category';
        $this->breadcrumbs  = [
            'Blog Area',
            'Categories',
            'Add',
        ];

        parent::add($this->model);
    }

    /**
     * @return void
     */
    public function editAction()
    {
        $this->title        = 'Edit Category';
        $this->breadcrumbs  = [
            'Blog Area',
            'Categories',
            'Edit',
        ];

        parent::edit($this->model);
    }
}