<?php

class Admin_Blog_Posts_CommentsController extends Admin_Controller
{
    /**
     * @var Model
     */
    public $modelClass = 'Blog_Model_Post_Comment';

    /**
     * @var string
     */
    public $redirectUrl = 'blog/posts/comments/index';

    /**
     * @set Model
     * @return void
     */
    public function init()
    {
        $this->model = $this->getNewModel();

        $this->assign([
            'posts' => Blog_Model_Post::getInstance()->getCollection()->toSelectArray(),
            'flags' => Blog_Model_Post_Comment::$flags,
        ]);
    }

    /**
     * @return void
     */
    public function indexAction()
    {
        $this->records = $this->model->getRecords([
            'name', 'content'
        ], null, 'created_at', 'desc');

        $this->title        = 'Comments';
        $this->breadcrumbs  = [
            'Blog Area',
            'Comments',
        ];

        parent::index($this->model);
    }

    /**
     * @return void
     */
    public function addAction()
    {
        if (Blog_Model_Post::getInstance()->getCollection()->getCount() == 0) {
            $this->flashError('You must first add at least one Blog Post');
            $this->redirectUrl(Url::getAdminUrl() . '/' . $this->redirectUrl);
        }

        $this->title        = 'Add Comment';
        $this->breadcrumbs  = [
            'Blog Area',
            'Comments',
            'Add',
        ];

        parent::add($this->model);
    }

    /**
     * @return void
     */
    public function editAction()
    {
        $this->title        = 'Edit Comment';
        $this->breadcrumbs  = [
            'Blog Area',
            'Comments',
            'Edit',
        ];

        parent::edit($this->model);
    }
}