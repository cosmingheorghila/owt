<?php

class Admin_Blog_Posts_MediaController extends Admin_Controller
{
    /**
     * @var Model
     */
    public $modelClass = 'Blog_Model_Post_Media';

    /**
     * @var string
     */
    public $redirectUrl = 'blog/posts/media/index';

    /**
     * @set Model
     * @return void
     */
    public function init()
    {
        if (!Get::exists('post')) {
            $this->flashError('You are trying to access the media of a non-post!');
            $this->redirectUrl(Url::getPreviousUrl());
        }

        $this->model        = $this->getNewModel();
        $this->redirectUrl  = $this->redirectUrl . '?post=' . Get::get('post');

        $this->assign([
            'types' => Blog_Model_Post_Media::$types,
            'post'  => Get::get('post'),
        ]);
    }

    /**
     * @return void
     */
    public function indexAction()
    {
        $this->movable  = true;
        $this->paginate = false;
        $this->records  = $this->model->getRecords(
            ['media', 'type'],
            ['post_id' => Get::get('post')],
            'ord', 'asc'
        );

        $this->title        = 'Post Media';
        $this->breadcrumbs  = [
            'Blog Area',
            'Posts',
            'Media',
        ];

        parent::index($this->model);
    }

    /**
     * @return void
     */
    public function addAction()
    {
        $this->title        = 'Add Post Media';
        $this->breadcrumbs  = [
            'Blog Area',
            'Posts',
            'Media',
            'Add',
        ];

        parent::add($this->model);
    }

    /**
     * @return void
     */
    public function editAction()
    {
        $this->title        = 'Edit Post Media';
        $this->breadcrumbs  = [
            'Blog Area',
            'Posts',
            'Media',
            'Edit',
        ];

        parent::edit($this->model);
    }
}