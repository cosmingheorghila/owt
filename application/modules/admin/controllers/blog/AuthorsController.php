<?php

class Admin_Blog_AuthorsController extends Admin_Controller
{
    /**
     * @var Model
     */
    public $modelClass = 'Blog_Model_Author';

    /**
     * @var string
     */
    public $redirectUrl = 'blog/authors/index';

    /**
     * @set Model
     * @return void
     */
    public function init()
    {
        $this->model = $this->getNewModel();
    }

    /**
     * @return void
     */
    public function indexAction()
    {
        $this->records = $this->model->getRecords([
            'name', 'position'
        ], null, 'name', 'asc');

        $this->title        = 'Authors';
        $this->breadcrumbs  = [
            'Blog Area',
            'Authors',
        ];

        parent::index($this->model);
    }

    /**
     * @return void
     */
    public function addAction()
    {
        $this->title        = 'Add Author';
        $this->breadcrumbs  = [
            'Blog Area',
            'Authors',
            'Add',
        ];

        parent::add($this->model);
    }

    /**
     * @return void
     */
    public function editAction()
    {
        $this->title        = 'Edit Author';
        $this->breadcrumbs  = [
            'Blog Area',
            'Authors',
            'Edit',
        ];

        parent::edit($this->model);
    }
}