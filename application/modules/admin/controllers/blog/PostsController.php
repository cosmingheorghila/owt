<?php

class Admin_Blog_PostsController extends Admin_Controller
{
    /**
     * @var Model
     */
    public $modelClass = 'Blog_Model_Post';

    /**
     * @var string
     */
    public $redirectUrl = 'blog/posts/index';

    /**
     * @set Model
     * @return void
     */
    public function init()
    {
        $this->model = $this->getNewModel();

        $this->assign([
            'authors' => Blog_Model_Author::getInstance()->getCollection()->toSelectArray(true),
        ]);
    }

    /**
     * @return void
     */
    public function indexAction()
    {
        $this->records = $this->model->getRecords([
            'name', 'content'
        ], null, 'created_at', 'desc');

        $this->title        = 'Posts';
        $this->breadcrumbs  = [
            'Blog Area',
            'Posts',
        ];

        parent::index($this->model);
    }

    /**
     * @return void
     */
    public function addAction()
    {
        $this->title        = 'Add Post';
        $this->breadcrumbs  = [
            'Blog Area',
            'Posts',
            'Add',
        ];

        parent::add($this->model);
    }

    /**
     * @return void
     */
    public function editAction()
    {
        $this->title        = 'Edit Post';
        $this->breadcrumbs  = [
            'Blog Area',
            'Posts',
            'Edit',
        ];

        parent::edit($this->model);
    }
}