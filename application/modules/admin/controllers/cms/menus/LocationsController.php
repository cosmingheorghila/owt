<?php

class Admin_Cms_Menus_LocationsController extends Admin_Controller
{
    /**
     * @var Model
     */
    public $modelClass = 'Cms_Model_Menu_Location';

    /**
     * @var string
     */
    public $redirectUrl = 'cms/menus/locations/index';

    /**
     * @set Model
     * @return void
     */
    public function init()
    {
        $this->model = $this->getNewModel();
    }

    /**
     * @return void
     */
    public function indexAction()
    {
        $this->records = $this->model->getRecords([
            'location'
        ]);

        $this->title        = 'Menu Locations';
        $this->breadcrumbs  = [
            'Manage Content',
            'Menu Locations',
        ];

        parent::index($this->model);
    }

    /**
     * @return void
     */
    public function addAction()
    {
        $this->title        = 'Add Menu Location';
        $this->breadcrumbs  = [
            'Manage Content',
            'Menu Locations',
            'Add',
        ];

        parent::add($this->model);
    }

    /**
     * @return void
     */
    public function editAction()
    {
        $this->title        = 'Edit Menu Location';
        $this->breadcrumbs  = [
            'Manage Content',
            'Menu Locations',
            'Edit',
        ];

        parent::edit($this->model);
    }
}