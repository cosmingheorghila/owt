<?php

class Admin_Cms_LayoutsController extends Admin_Controller
{
    /**
     * @var Model
     */
    public $modelClass = 'Cms_Model_Layout';

    /**
     * @var string
     */
    public $redirectUrl = 'cms/layouts/index';

    /**
     * @set Model
     * @return void
     */
    public function init()
    {
        $this->model = $this->getNewModel();
    }

    /**
     * @return void
     */
    public function indexAction()
    {
        $this->records = $this->model->getRecords([
            'name', 'file', 'identifier'
        ], null, 'name', 'asc');

        $this->title        = 'Layouts';
        $this->breadcrumbs  = [
            'Manage Content',
            'Layouts',
        ];

        parent::index($this->model);
    }

    /**
     * @return void
     */
    public function addAction()
    {
        $this->title        = 'Add Layout';
        $this->breadcrumbs  = [
            'Manage Content',
            'Layouts',
            'Add',
        ];

        parent::add($this->model);
    }

    /**
     * @return void
     */
    public function editAction()
    {
        $this->title        = 'Edit Layout';
        $this->breadcrumbs  = [
            'Manage Content',
            'Layouts',
            'Edit',
        ];

        parent::edit($this->model);
    }
}