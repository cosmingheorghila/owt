<?php

class Admin_Cms_BlocksController extends Admin_Controller
{
    /**
     * @var Model
     */
    public $modelClass = 'Cms_Model_Block';

    /**
     * @var string
     */
    public $redirectUrl = 'cms/blocks/index';

    /**
     * @set Model
     * @return void
     */
    public function init()
    {
        $this->model = $this->getNewModel();
    }

    /**
     * @return void
     */
    public function indexAction()
    {
        Session::disable('block-type');

        $this->records = $this->model->getRecords([
            'name', 'label', 'type'
        ], null, 'name', 'asc');

        $this->title        = 'Blocks';
        $this->breadcrumbs  = [
            'Manage Content',
            'Blocks',
        ];

        parent::index($this->model);
    }

    /**
     * @return void
     */
    public function addAction()
    {
        if (Session::exists('block-type')) {
            $this->title        = 'Add Block';
            $this->breadcrumbs  = [
                'Manage Content',
                'Blocks',
                'Add',
            ];

            parent::add($this->model);
        } else {
            $this->setAdmin();

            $this->model->verifyValidUser();
            $this->model->verifyAccessLevel();

            if ($this->getRequest()->isPost()) {
                if (Post::exists('data.type')) {
                    Session::set('block-type', Post::get('data.type'));
                    $this->redirectUrl(Url::getPreviousUrl());
                } else {
                    $this->flashError('You must choose a block type!');
                    $this->redirectUrl(Url::getCurrentUrl());
                }
            }

            $this->assign('form', new Form());
            $this->assign('admin', $this->admin);
            $this->assign('types', Behavior_Block::getTypes());
            $this->assign('title', 'Add Block');
            $this->assign('breadcrumbs', [
                'Manage Content',
                'Blocks',
                'Add',
            ]);
        }
    }

    /**
     * @return void
     */
    public function editAction()
    {
        $this->title        = 'Edit Block';
        $this->breadcrumbs  = [
            'Manage Content',
            'Blocks',
            'Edit',
        ];

        parent::edit($this->model);
    }
}