<?php

class Admin_Cms_PagesController extends Admin_Controller
{
    /**
     * @var Model
     */
    public $modelClass = 'Cms_Model_Page';

    /**
     * @var string
     */
    public $redirectUrl = 'cms/pages/index';

    /**
     * @var Cms_Model_Page
     */
    public $page;

    /**
     * @set Model
     * @return void
     */
    public function init()
    {
        $this->model = $this->getNewModel();

        $this->assign([
            'layouts'       => Cms_Model_Layout::getLayouts(),
            'visibilities'  => Cms_Model_Page::$visibilities
        ]);
    }

    /**
     * @return void
     */
    public function indexAction()
    {
        $this->records = $this->model->getRecords(
            ['name', 'url', 'subtitle', 'h1', 'identifier', 'created_at'],
            'parent_id ' . ($this->isParent() ? '= "' . $this->getAdapter()->escape(Get::get('parent_id')) . '"' : 'IS NULL') . '',
            'ord'
        );

        $this->assign('parent', $this->isParent() ? Cms_Model_Page::getInstance()->load(Get::get('parent_id')) : null);

        $this->paginate     = false;
        $this->movable      = true;
        $this->title        = 'Pages';
        $this->breadcrumbs  = [
            'Manage Content',
            'Pages',
        ];

        parent::index($this->model);
    }

    /**
     * @return void
     */
    public function addAction()
    {
        $this->title        = 'Add Page';
        $this->breadcrumbs  = [
            'Manage Content',
            'Pages',
            'Add',
        ];

        parent::add($this->model);
    }

    /**
     * @return void
     */
    public function editAction()
    {
        $this->title        = 'Edit Page';
        $this->breadcrumbs  = [
            'Manage Content',
            'Pages',
            'Edit',
        ];

        parent::edit($this->model);
    }

    /**
     * @param Model|null $model
     * @return void
     */
    protected function beforeAdd($model = null)
    {
        $this->data = Post::get('data');

        if ($this->hasLayout() && !$this->data['layout_id']) {
            Session::set('data', $this->data);

            $this->flashError('You must create a layout first!');
            $this->redirectUrl(Url::getPreviousUrl());
        }

        $this->redirectUrl .= $this->isParent() ? '?parent_id=' . Get::get('parent_id') : '';
    }

    /**
     * @param Model|null $model
     * @return void
     */
    protected function beforeEdit($model = null)
    {
        $this->redirectUrl .= $this->isParent() ? '?parent_id=' . Get::get('parent_id') : '';
    }

    /**
     * @return bool
     */
    private function isParent()
    {
        return Get::exists('parent_id') && is_numeric(Get::get('parent_id'));
    }

    /**
     * @return bool
     */
    private function hasLayout()
    {
        return $this->data['layout_id'] && is_array($this->data['layout_id']);
    }
}