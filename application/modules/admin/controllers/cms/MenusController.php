<?php

class Admin_Cms_MenusController extends Admin_Controller
{
    /**
     * @var Model
     */
    public $modelClass = 'Cms_Model_Menu';

    /**
     * @var string
     */
    public $redirectUrl = 'cms/menus/index';

    /**
     * @set Model
     * @return void
     */
    public function init()
    {
        $this->model = $this->getNewModel();

        $this->assign([
            'types'     => Cms_Model_Menu::$types,
            'windows'   => Cms_Model_Menu::$windows,
            'pages'     => Cms_Model_Page::getTree()
        ]);
    }

    /**
     * @return void
     */
    public function indexAction()
    {
        $this->verifyLocation();

        if ($this->isParent()) {
            $this->records = $this->model->getRecords(['name', 'url'], ['location_id' => Get::get('location'), 'parent_id' => Get::get('parent_id')], 'ord', 'asc');

            $this->assign('parent', Cms_Model_Menu::getInstance()->load(Get::get('parent_id')));
        } else {
            $this->records = $this->model->getRecords(['name', 'url'], 'location_id = ' . Get::get('location') . ' AND parent_id IS NULL', 'ord');
        }

        $this->paginate     = false;
        $this->movable      = true;
        $this->title        = 'Menus';
        $this->breadcrumbs  = [
            'Manage Content',
            'Menus',
        ];

        parent::index($this->model);
    }

    /**
     * @return void
     */
    public function addAction()
    {
        $this->title        = 'Add Menu';
        $this->breadcrumbs  = [
            'Manage Content',
            'Menus',
            'Add',
        ];

        parent::add($this->model);
    }

    /**
     * @return void
     */
    public function editAction()
    {
        $this->title        = 'Edit Menu';
        $this->breadcrumbs  = [
            'Manage Content',
            'Menus',
            'Edit',
        ];

        parent::edit($this->model);
    }

    /**
     * @param Model|null $model
     * @return void
     */
    protected function beforeAdd($model = null)
    {
        $this->data = Post::get('data');
        $params     = Post::get('params');

        if (Get::exists('location') && is_numeric(Get::get('location'))) {
            $this->data['location_id'] = Get::get('location');

            Session::set('data', $this->data);
        } else {
            Session::set('data', $this->data);

            $this->flashError('Invalid location!');
            $this->redirectUrl(Url::getPreviousUrl());
        }

        switch ($this->data['entity']) {
            case 'page':
                $this->data['url'] = $params['url-page'];

                if (!$this->data['url']) {
                    Session::set('data', $this->data);

                    $this->flashError('You need to create a page first!');
                    $this->redirectUrl(Url::getPreviousUrl());
                }

                $this->data['entity_id'] = Database::getInstance('cms_urls')->load([
                    'url' => $this->data['url']
                ])->getEntityId();

                Session::set('data', $this->data);
                break;
            case 'url':
                $this->data['entity_id']    = 'null';
                $this->data['url']          = $params['url-url'];

                Session::set('data', $this->data);
                break;
            default:
                $this->flashError('Please choose a type for the menu!');
                $this->redirectUrl(Url::getPreviousUrl());
                break;
        }

        if ($this->isParent()) {
            $this->data['parent_id'] = Get::get('parent_id');

            Session::set('data', $this->data);
        }

        $this->redirectUrl = 'cms/menus/index?location=' . Get::get('location') . ($this->isParent() ? '&parent_id=' . Get::get('parent_id') : '');
    }

    /**
     * @param Model|null $model
     * @return void
     */
    protected function beforeEdit($model = null)
    {
        $this->data = Post::get('data');
        $params     = Post::get('params');

        switch ($this->data['entity']) {
            case 'page':
                $this->data['url'] = $params['url-page'];

                if (!$this->data['url']) {
                    $this->flashError('You need to create a page first!');
                    $this->redirectUrl(Url::getPreviousUrl());
                }

                $this->data['entity_id'] = Database::getInstance('cms_urls')->load([
                    'url' => $params['url-page']
                ])->getEntityId();
                break;
            case 'url':
                $this->data['entity_id']    = 'null';
                $this->data['url']          = $params['url-url'];
                break;
            default:
                $this->flashError('Please choose a TYPE for the menu!');
                $this->redirectUrl(Url::getPreviousUrl());
                break;
        }

        if ($this->isParent()) {
            $this->data['parent_id']    = Get::get('parent_id');
            $this->redirectUrl          = 'cms/menus/index?location=' . $this->data['location_id'] . '&parent_id=' . Get::get('parent_id');
        } else {
            $this->redirectUrl = 'cms/menus/index?location=' . $this->data['location_id'];
        }
    }

    /**
     * @return bool
     */
    private function isParent()
    {
        return Get::exists('parent_id') && is_numeric(Get::get('parent_id'));
    }

    /**
     * @return void
     */
    private function verifyLocation()
    {
        $location = Cms_Model_Menu_Location::getInstance()->where([
            'id' => Get::get('location')
        ])->getCollection();

        if (!Get::exists('location') || count($location) == 0) {
            $this->flashError('No such location was found!');
            $this->redirectUrl(Url::getAdminUrl() . '/cms/menus/locations/index');
        }
    }
}