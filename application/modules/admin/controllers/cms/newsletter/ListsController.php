<?php

class Admin_Cms_Newsletter_ListsController extends Admin_Controller
{
    /**
     * @var Model
     */
    public $modelClass = 'Cms_Model_Newsletter_List';

    /**
     * @var string
     */
    public $redirectUrl = 'cms/newsletter/lists/index';

    /**
     * @set Model
     * @return void
     */
    public function init()
    {
        $this->model = $this->getNewModel();
    }

    /**
     * @return void
     */
    public function indexAction()
    {
        $this->records = $this->model->getRecords([
            'subject', 'message'
        ], null, 'created_at', 'desc');

        $this->title        = 'Email Lists';
        $this->breadcrumbs  = [
            'Newsletter',
            'Email Lists',
        ];

        parent::index($this->model);
    }

    /**
     * @return void
     */
    public function addAction()
    {
        $this->title        = 'Add Email List';
        $this->breadcrumbs  = [
            'Newsletter',
            'Email Lists',
            'Add',
        ];

        parent::add($this->model);
    }

    /**
     * @return void
     */
    public function editAction()
    {
        $this->title        = 'Edit Email List';
        $this->breadcrumbs  = [
            'Newsletter',
            'Email Lists',
            'Edit',
        ];

        parent::edit($this->model);
    }

    /**
     * @return void
     */
    public function sendAction()
    {
        if (Get::exists('list_id')) {
            if ($list = $this->model->load(Get::get('list_id'))) {
                if ($list->send()) {
                    $this->flashSuccess('Email List sent successfully to all the Subscribers');
                    $this->redirectUrl(Url::getAdminUrl() . '/' . $this->redirectUrl);
                }

                $this->flashError('Could not send the Email List');
                $this->redirectUrl(Url::getPreviousUrl() ? Url::getPreviousUrl() : Url::getAdminUrl() . '/' . $this->redirectUrl);
            }

            $this->flashError('Could not load the Email List');
            $this->redirectUrl(Url::getPreviousUrl() ? Url::getPreviousUrl() : Url::getAdminUrl() . '/' . $this->redirectUrl);
        }

        $this->flashError('Error sending the Email List');
        $this->redirectUrl(Url::getPreviousUrl() ? Url::getPreviousUrl() : Url::getAdminUrl() . '/' . $this->redirectUrl);
    }
}