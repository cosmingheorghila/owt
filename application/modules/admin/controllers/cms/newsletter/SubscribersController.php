<?php

class Admin_Cms_Newsletter_SubscribersController extends Admin_Controller
{
    /**
     * @var Model
     */
    public $modelClass = 'Cms_Model_Newsletter_Subscriber';

    /**
     * @var string
     */
    public $redirectUrl = 'cms/newsletter/subscribers/index';

    /**
     * @set Model
     * @return void
     */
    public function init()
    {
        $this->model = $this->getNewModel();
    }

    /**
     * @return void
     */
    public function indexAction()
    {
        $this->records = $this->model->getRecords([
            'email'
        ], null, 'created_at', 'desc');

        $this->title        = 'Subscribers';
        $this->breadcrumbs  = [
            'Newsletter',
            'Subscribers',
        ];

        parent::index($this->model);
    }

    /**
     * @return void
     */
    public function addAction()
    {
        $this->title        = 'Add Subscriber';
        $this->breadcrumbs  = [
            'Newsletter',
            'Subscribers',
            'Add',
        ];

        parent::add($this->model);
    }

    /**
     * @return void
     */
    public function editAction()
    {
        $this->title        = 'Edit Subscriber';
        $this->breadcrumbs  = [
            'Newsletter',
            'Subscribers',
            'Edit',
        ];

        parent::edit($this->model);
    }
}