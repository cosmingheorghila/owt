<?php

class Admin_IndexController extends Admin_Controller
{
    /**
     * @return void
     */
    public function indexAction()
    {
        $this->setAdmin();
        $this->setLayout('admin/login.phtml');

        if (Get::exists('logout')) {
            $this->admin->logout();
        }

        if ($this->admin && $this->admin->isLogged()) {
            $this->redirectUrl(Url::getAdminUrl() . '/index/dashboard');
        }

        if ($this->getRequest()->isPost()) {
            if (Admin_Model_User::login(Post::get('data.username'), Post::get('data.password'))) {
                if ($url = Session::get('redirect-url')) {
                    Session::disable('redirect-url');
                    $this->redirectUrl($url);
                }

                $this->redirectUrl(Url::getAdminUrl() . '/index/dashboard');
            } else {
                Session::set('flash-message', '<div class="flash-home"><span class="error">Invalid username or password!</span></div>');
                $this->redirectUrl(Url::getPreviousUrl());
            }
        }

        $this->assign([
            'admin' => $this->admin,
            'form'  => new Form(),
        ]);
    }

    /**
     * @return void
     */
    public function dashboardAction()
    {
        $this->setAdmin();
        $this->setLayout('admin/home.phtml');

        if (!($this->admin && $this->admin->isLogged())) {
            $this->redirectUrl(Url::getAdminUrl());
        }

        (new Sitemap())->build();

        $this->assign('admin', $this->admin);
        $this->assign('title', 'Dashboard');
        //$this->assign('analytics', (new Google())->analytics());
        //$this->assign('breadcrumbs', 'Track website progress');
    }
}