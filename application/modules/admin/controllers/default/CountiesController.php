<?php

class Admin_Default_CountiesController extends Admin_Controller
{
    /**
     * @var Model
     */
    public $modelClass = 'Default_Model_County';

    /**
     * @var string
     */
    public $redirectUrl = 'default/counties/index';

    /**
     * @set Model
     * @return void
     */
    public function init()
    {
        $this->model = $this->getNewModel();

        $this->assign('countries', Default_Model_Country::getInstance()->getCollection()->toSelectArray());
    }

    /**
     * @return void
     */
    public function indexAction()
    {
        $this->records = $this->model->getRecords([
            'name'
        ], null, 'name', 'asc');

        $this->title        = 'Counties';
        $this->breadcrumbs  = [
            'Geo Location',
            'Counties',
        ];

        parent::index($this->model);
    }

    /**
     * @return void
     */
    public function addAction()
    {
        $this->title        = 'Add County';
        $this->breadcrumbs  = [
            'Geo Location',
            'Counties',
            'Add',
        ];

        parent::add($this->model);
    }

    /**
     * @return void
     */
    public function editAction()
    {
        $this->title        = 'Edit County';
        $this->breadcrumbs  = [
            'Geo Location',
            'Counties',
            'Edit',
        ];

        parent::edit($this->model);
    }
}