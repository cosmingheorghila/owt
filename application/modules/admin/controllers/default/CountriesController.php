<?php

class Admin_Default_CountriesController extends Admin_Controller
{
    /**
     * @var Model
     */
    public $modelClass = 'Default_Model_Country';

    /**
     * @var string
     */
    public $redirectUrl = 'default/countries/index';

    /**
     * @set Model
     * @return void
     */
    public function init()
    {
        $this->model = $this->getNewModel();
    }

    /**
     * @return void
     */
    public function indexAction()
    {
        $this->records = $this->model->getRecords([
            'name'
        ], null, 'name', 'asc');

        $this->title        = 'Countries';
        $this->breadcrumbs  = [
            'Geo Location',
            'Countries',
        ];

        parent::index($this->model);
    }

    /**
     * @return void
     */
    public function addAction()
    {
        $this->title        = 'Add Country';
        $this->breadcrumbs  = [
            'Geo Location',
            'Countries',
            'Add',
        ];

        parent::add($this->model);
    }

    /**
     * @return void
     */
    public function editAction()
    {
        $this->title        = 'Edit Country';
        $this->breadcrumbs  = [
            'Geo Location',
            'Countries',
            'Edit',
        ];

        parent::edit($this->model);
    }
}