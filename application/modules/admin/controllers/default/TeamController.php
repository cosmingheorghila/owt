<?php

class Admin_Default_TeamController extends Admin_Controller
{
    /**
     * @var Model
     */
    public $modelClass = 'Default_Model_Team';

    /**
     * @var string
     */
    public $redirectUrl = 'default/team/index';

    /**
     * @set Model
     * @return void
     */
    public function init()
    {
        $this->model = $this->getNewModel();
    }

    /**
     * @return void
     */
    public function indexAction()
    {
        $this->records = $this->model->getRecords([
            'first_name', 'last_name', 'job_title'
        ], null, 'ord', 'asc');

        $this->paginate     = false;
        $this->movable      = true;
        $this->title        = 'Team';
        $this->breadcrumbs  = [
            'Custom Entities',
            'Team',
        ];

        parent::index($this->model);
    }

    /**
     * @return void
     */
    public function addAction()
    {
        $this->title        = 'Add Team Member';
        $this->breadcrumbs  = [
            'Custom Entities',
            'Team',
            'Add',
        ];

        parent::add($this->model);
    }

    /**
     * @return void
     */
    public function editAction()
    {
        $this->title        = 'Edit Team Member';
        $this->breadcrumbs  = [
            'Custom Entities',
            'Team',
            'Edit',
        ];

        parent::edit($this->model);
    }
}