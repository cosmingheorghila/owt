<?php

class Admin_Default_PartnersController extends Admin_Controller
{
    /**
     * @var Model
     */
    public $modelClass = 'Default_Model_Partner';

    /**
     * @var string
     */
    public $redirectUrl = 'default/partners/index';

    /**
     * @set Model
     * @return void
     */
    public function init()
    {
        $this->model = $this->getNewModel();
    }

    /**
     * @return void
     */
    public function indexAction()
    {
        $this->records = $this->model->getRecords([
            'name'
        ], null, 'ord', 'asc');

        $this->paginate     = false;
        $this->movable      = true;
        $this->title        = 'Partners';
        $this->breadcrumbs  = [
            'Custom Entities',
            'Partners',
        ];

        parent::index($this->model);
    }

    /**
     * @return void
     */
    public function addAction()
    {
        $this->title        = 'Add Partner';
        $this->breadcrumbs  = [
            'Custom Entities',
            'Partners',
            'Add',
        ];

        parent::add($this->model);
    }

    /**
     * @return void
     */
    public function editAction()
    {
        $this->title        = 'Edit Partner';
        $this->breadcrumbs  = [
            'Custom Entities',
            'Partners',
            'Edit',
        ];

        parent::edit($this->model);
    }
}