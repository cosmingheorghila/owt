<?php

class Admin_Default_CitiesController extends Admin_Controller
{
    /**
     * @var Model
     */
    public $modelClass = 'Default_Model_City';

    /**
     * @var string
     */
    public $redirectUrl = 'default/cities/index';

    /**
     * @set Model
     * @return void
     */
    public function init()
    {
        $this->model = $this->getNewModel();

        $this->assign('counties', Default_Model_County::getInstance()->getCollection()->toSelectArray());
        $this->assign('valid', Default_Model_City::$valid);
    }

    /**
     * @return void
     */
    public function indexAction()
    {
        $this->records = $this->model->getRecords([
            'name'
        ], null, 'name', 'asc');

        $this->title        = 'Cities';
        $this->breadcrumbs  = [
            'Custom Entities',
            'Cities',
        ];

        parent::index($this->model);
    }

    /**
     * @return void
     */
    public function addAction()
    {
        $this->title        = 'Add City';
        $this->breadcrumbs  = [
            'Geo Location',
            'Cities',
            'Add',
        ];

        parent::add($this->model);
    }

    /**
     * @return void
     */
    public function editAction()
    {
        $this->title        = 'Edit City';
        $this->breadcrumbs  = [
            'Geo Location',
            'Cities',
            'Edit',
        ];

        parent::edit($this->model);
    }
}