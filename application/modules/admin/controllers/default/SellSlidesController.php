<?php

class Admin_Default_SellSlidesController extends Admin_Controller
{
    /**
     * @var Model
     */
    public $modelClass = 'Default_Model_SellSlide';

    /**
     * @var string
     */
    public $redirectUrl = 'default/sell-slides/index';

    /**
     * @set Model
     * @return void
     */
    public function init()
    {
        $this->model = $this->getNewModel();
    }

    /**
     * @return void
     */
    public function indexAction()
    {
        $this->records = $this->model->getRecords([
            'name', 'intro', 'primary_button_text', 'primary_button_url', 'secondary_button_text', 'secondary_button_url'
        ], null, 'ord', 'asc');

        $this->paginate     = false;
        $this->movable      = true;
        $this->title        = 'Sell Slides';
        $this->breadcrumbs  = [
            'Custom Entities',
            'Sell Slides',
        ];

        parent::index($this->model);
    }

    /**
     * @return void
     */
    public function addAction()
    {
        $this->title        = 'Add Sell Slide';
        $this->breadcrumbs  = [
            'Custom Entities',
            'Sell Slides',
            'Add',
        ];

        parent::add($this->model);
    }

    /**
     * @return void
     */
    public function editAction()
    {
        $this->title        = 'Edit Sell Slide';
        $this->breadcrumbs  = [
            'Custom Entities',
            'Sell Slides',
            'Edit',
        ];

        parent::edit($this->model);
    }
}