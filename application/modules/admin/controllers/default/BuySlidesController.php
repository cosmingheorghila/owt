<?php

class Admin_Default_BuySlidesController extends Admin_Controller
{
    /**
     * @var Model
     */
    public $modelClass = 'Default_Model_BuySlide';

    /**
     * @var string
     */
    public $redirectUrl = 'default/buy-slides/index';

    /**
     * @set Model
     * @return void
     */
    public function init()
    {
        $this->model = $this->getNewModel();
    }

    /**
     * @return void
     */
    public function indexAction()
    {
        $this->records = $this->model->getRecords([
            'name', 'intro', 'button_text', 'button_url'
        ], null, 'ord', 'asc');

        $this->paginate     = false;
        $this->movable      = true;
        $this->title        = 'Buy Slides';
        $this->breadcrumbs  = [
            'Custom Entities',
            'Buy Slides',
        ];

        parent::index($this->model);
    }

    /**
     * @return void
     */
    public function addAction()
    {
        $this->title        = 'Add Buy Slide';
        $this->breadcrumbs  = [
            'Custom Entities',
            'Buy Slides',
            'Add',
        ];

        parent::add($this->model);
    }

    /**
     * @return void
     */
    public function editAction()
    {
        $this->title        = 'Edit Buy Slide';
        $this->breadcrumbs  = [
            'Custom Entities',
            'Buy Slides',
            'Edit',
        ];

        parent::edit($this->model);
    }
}