<?php

class Admin_Car_FeaturesController extends Admin_Controller
{
    /**
     * @var Model
     */
    public $modelClass = 'Car_Model_Feature';

    /**
     * @var string
     */
    public $redirectUrl = 'car/features/index';

    /**
     * @set Model
     * @return void
     */
    public function init()
    {
        $this->model = $this->getNewModel();
    }

    /**
     * @return void
     */
    public function indexAction()
    {
        $this->records = $this->model->getRecords([
            'name'
        ], null, 'ord', 'asc');

        $this->paginate     = false;
        $this->movable      = true;
        $this->title        = 'Car Features';
        $this->breadcrumbs  = [
            'Cars Details',
            'Features',
        ];

        parent::index($this->model);
    }

    /**
     * @return void
     */
    public function addAction()
    {
        $this->title        = 'Add Car Feature';
        $this->breadcrumbs  = [
            'Cars Details',
            'Features',
            'Add',
        ];

        parent::add($this->model);
    }

    /**
     * @return void
     */
    public function editAction()
    {
        $this->title        = 'Edit Car Feature';
        $this->breadcrumbs  = [
            'Cars Details',
            'Features',
            'Edit',
        ];

        parent::edit($this->model);
    }
}