<?php

class Admin_Car_ColorsController extends Admin_Controller
{
    /**
     * @var Model
     */
    public $modelClass = 'Car_Model_Color';

    /**
     * @var string
     */
    public $redirectUrl = 'car/colors/index';

    /**
     * @set Model
     * @return void
     */
    public function init()
    {
        $this->model = $this->getNewModel();

        $this->assign('models', Car_Model_Model::getInstance()->getCollection()->toSelectArray(true));
    }

    /**
     * @return void
     */
    public function indexAction()
    {
        if (Get::exists('data.car_model.0')) {
            $this->movable  = true;
            $this->paginate = false;
            $this->records = $this->model->getRecords([
                'name'
            ], null, 'ord', 'asc');
        } else {
            $this->records = $this->model->getRecords([
                'name'
            ], null, 'name', 'asc');
        }

        $this->title        = 'Car Colors';
        $this->breadcrumbs  = [
            'Cars Details',
            'Colors',
        ];

        parent::index($this->model);
    }

    /**
     * @return void
     */
    public function addAction()
    {
        $this->title        = 'Add Car Colors';
        $this->breadcrumbs  = [
            'Cars Details',
            'Colors',
            'Add',
        ];

        parent::add($this->model);
    }

    /**
     * @return void
     */
    public function editAction()
    {
        $this->title        = 'Edit Car Colors';
        $this->breadcrumbs  = [
            'Cars Details',
            'Colors',
            'Edit',
        ];

        parent::edit($this->model);
    }
}