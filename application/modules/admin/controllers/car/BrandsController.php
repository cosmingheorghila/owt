<?php

class Admin_Car_BrandsController extends Admin_Controller
{
    /**
     * @var Model
     */
    public $modelClass = 'Car_Model_Brand';

    /**
     * @var string
     */
    public $redirectUrl = 'car/brands/index';

    /**
     * @set Model
     * @return void
     */
    public function init()
    {
        $this->model = $this->getNewModel();
    }

    /**
     * @return void
     */
    public function indexAction()
    {
        $this->records = $this->model->getRecords([
            'name'
        ], null, 'ord', 'asc');

        $this->paginate     = false;
        $this->movable      = true;
        $this->title        = 'Car Brands';
        $this->breadcrumbs  = [
            'Cars Details',
            'Brands',
        ];

        parent::index($this->model);
    }

    /**
     * @return void
     */
    public function addAction()
    {
        $this->title        = 'Add Car Brand';
        $this->breadcrumbs  = [
            'Cars Details',
            'Brands',
            'Add',
        ];

        parent::add($this->model);
    }

    /**
     * @return void
     */
    public function editAction()
    {
        $this->title        = 'Edit Car Brand';
        $this->breadcrumbs  = [
            'Cars Details',
            'Brands',
            'Edit',
        ];

        parent::edit($this->model);
    }
}