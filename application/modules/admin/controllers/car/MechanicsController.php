<?php

class Admin_Car_MechanicsController extends Admin_Controller
{
    /**
     * @var Model
     */
    public $modelClass = 'Car_Model_Mechanic';

    /**
     * @var string
     */
    public $redirectUrl = 'car/mechanics/index';

    /**
     * @set Model
     * @return void
     */
    public function init()
    {
        $this->model = $this->getNewModel();

        $this->assign([
            'certifications' => Car_Model_Certification::getInstance()->getCollection()->toSelectArray(),
        ]);
    }

    /**
     * @return void
     */
    public function indexAction()
    {
        $this->records = $this->model->getRecords([
            'first_name', 'last_name', 'email'
        ], null, 'created_at', 'asc');

        $this->paginate     = false;
        $this->movable      = true;
        $this->title        = 'Mechanics';
        $this->breadcrumbs  = [
            'Cars Details',
            'Mechanics',
        ];

        parent::index($this->model);
    }

    /**
     * @return void
     */
    public function addAction()
    {
        $this->title        = 'Add Mechanic';
        $this->breadcrumbs  = [
            'Cars Details',
            'Mechanics',
            'Add',
        ];

        parent::add($this->model);
    }

    /**
     * @return void
     */
    public function editAction()
    {
        $this->title        = 'Edit Mechanic';
        $this->breadcrumbs  = [
            'Cars Details',
            'Mechanics',
            'Edit',
        ];

        parent::edit($this->model);

        $this->assign('currentCertifications', array_keys($this->model->getItem()->getCertifications()->toSelectArray()));
    }
}