<?php

class Admin_Car_AdsController extends Admin_Controller
{
    /**
     * @var Model
     */
    public $modelClass = 'Car_Model_Ad';

    /**
     * @var string
     */
    public $redirectUrl = 'car/ads/index';

    /**
     * @set Model
     * @return void
     */
    public function init()
    {
        $this->model = $this->getNewModel();
    }

    /**
     * @return void
     */
    public function indexAction()
    {
        $this->records      = $this->model->getRecords([
            'identifier'
        ], null, 'created_at', 'desc');
        
        $this->title        = 'Ads';
        $this->breadcrumbs  = [
            'Cars Details',
            'Ads',
        ];

        parent::index($this->model);
    }

    /**
     * @return void
     */
    public function viewAction()
    {
        $this->title        = 'Edit Ad';
        $this->breadcrumbs  = [
            'Cars Details',
            'Ads',
            'Edit',
        ];

        parent::edit($this->model);

        $item = $this->model->getItem();

        $this->assign('brands', Car_Model_Brand::getInstance()->getCollection()->toSelectArray());
        $this->assign('models', Car_Model_Model::getInstance()->getCollection()->toSelectArray());
        $this->assign('colors', Car_Model_Color::getInstance()->getCollection()->toSelectArray());
        $this->assign('stats', Car_Model_Stat::getInstance()->getCollection()->toSelectArray());
        $this->assign('cities', Default_Model_City::getInstance()->getCollection()->toSelectArray());
        $this->assign('owner', $item->getOwner());
        $this->assign('address', $item->getAddress());
        $this->assign('availabilities', $item->getAvailabilities());
        $this->assign('stats', $item->getStats());
    }

    /**
     * @return void
     */
    public function convertAction()
    {
        if (Get::exists('ad')) {
            if ($ad = Car_Model_Ad::getInstance()->load(Get::get('ad'))) {
                if ($ad->convert()) {
                    $this->flashSuccess('Ad converted successfully!');
                } else {
                    $this->flashError('Could not convert the ad.');
                }
            } else {
                $this->flashError('You must specify a valid ad entity.');
            }

            $this->redirectUrl(Url::getAdminUrl() . '/' . $this->redirectUrl);
        } else {
            $this->redirectUrl(Url::getAdminUrl());
        }
    }
}