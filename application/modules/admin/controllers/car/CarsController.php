<?php

class Admin_Car_CarsController extends Admin_Controller
{
    /**
     * @var Model
     */
    public $modelClass = 'Car_Model_Car';

    /**
     * @var string
     */
    public $redirectUrl = 'car/cars/index';

    /**
     * @set Model
     * @return void
     */
    public function init()
    {
        $this->model = $this->getNewModel();

        $this->assign([
            'brands' => Car_Model_Brand::getInstance()->getCollection()->toSelectArray(),
            'models' => Car_Model_Model::getInstance()->getCollection()->toSelectArray(),
            'mechanics' => Car_Model_Mechanic::getInstance()->getCollection()->toSelectArray(true, 'id', 'first_name.last_name'),
            'colors' => Car_Model_Color::getInstance()->getCollection()->toSelectArray(),
            'regularColors' => Car_Model_Color::getInstance()->where("model_id IS NULL")->getCollection()->toSelectArray(true),
            'features' => Car_Model_Feature::getInstance()->getCollection()->toSelectArray(),
            'options' => Car_Model_Option::getInstance()->getCollection()->toSelectArray(),
            'currencies' => Shop_Model_Currency::getInstance()->getCollection()->toSelectArray(false, 'id', 'code'),
        ]);
    }

    /**
     * @return void
     */
    public function indexAction()
    {
        $this->records = $this->model->getRecords([
            'name', 'price', 'year', 'kilometers', 'vin'
        ], null, 'ord', 'asc');

        $this->paginate     = false;
        $this->movable      = true;
        $this->title        = 'Cars';
        $this->breadcrumbs  = [
            'Cars Details',
            'Cars',
        ];

        parent::index($this->model);
    }

    /**
     * @return void
     */
    public function addAction()
    {
        $this->title        = 'Add Car';
        $this->breadcrumbs  = [
            'Cars Details',
            'Cars',
            'Add',
        ];

        parent::add($this->model);
    }

    /**
     * @return void
     */
    public function editAction()
    {
        $this->title        = 'Edit Car';
        $this->breadcrumbs  = [
            'Cars Details',
            'Cars',
            'Edit',
        ];

        parent::edit($this->model);

        $this->assign('currentFeatures', array_keys($this->model->getItem()->getFeatures()->toSelectArray()));
        $this->assign('currentOptions', array_keys($this->model->getItem()->getOptions()->toSelectArray()));
    }
}