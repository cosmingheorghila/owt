<?php

class Admin_Car_Cars_PerksController extends Admin_Controller
{
    /**
     * @var Model
     */
    public $modelClass = 'Car_Model_Car_Perk';

    /**
     * @var string
     */
    public $redirectUrl = 'car/cars/perks/index';

    /**
     * @set Model
     * @return void
     */
    public function init()
    {
        if (!Get::exists('car')) {
            $this->flashError('You are trying to access the perks of a non-existent car!');
            $this->redirectUrl(Url::getPreviousUrl());
        }

        $this->model        = $this->getNewModel();
        $this->redirectUrl  = $this->redirectUrl . '?car=' . Get::get('car');

        $this->assign([
            'car' => Get::get('car'),
        ]);
    }

    /**
     * @return void
     */
    public function indexAction()
    {
        $this->movable  = true;
        $this->paginate = false;
        $this->records  = $this->model->getRecords(
            ['name', 'content'], ['car_id' => Get::get('car')], 'ord', 'asc'
        );

        $this->title        = 'Car Perks';
        $this->breadcrumbs  = [
            'Cars Details',
            'Perks',
            'Images',
        ];

        parent::index($this->model);
    }

    /**
     * @return void
     */
    public function addAction()
    {
        $this->title        = 'Add Car Perk';
        $this->breadcrumbs  = [
            'Cars Details',
            'Cars',
            'Perks',
            'Add',
        ];

        parent::add($this->model);
    }

    /**
     * @return void
     */
    public function editAction()
    {
        $this->title        = 'Edit Car Perk';
        $this->breadcrumbs  = [
            'Cars Details',
            'Cars',
            'Perks',
            'Edit',
        ];

        parent::edit($this->model);
    }
}