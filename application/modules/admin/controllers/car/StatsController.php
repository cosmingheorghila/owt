<?php

class Admin_Car_StatsController extends Admin_Controller
{
    /**
     * @var Model
     */
    public $modelClass = 'Car_Model_Stat';

    /**
     * @var string
     */
    public $redirectUrl = 'car/stats/index';

    /**
     * @set Model
     * @return void
     */
    public function init()
    {
        $this->model = $this->getNewModel();

        $this->assign('important', Car_Model_Stat::$important);
    }

    /**
     * @return void
     */
    public function indexAction()
    {
        $this->records = $this->model->getRecords([
            'name'
        ], null, 'ord', 'asc');

        $this->paginate     = false;
        $this->movable      = true;
        $this->title        = 'Car Stats';
        $this->breadcrumbs  = [
            'Cars Details',
            'Stats',
        ];

        parent::index($this->model);
    }

    /**
     * @return void
     */
    public function addAction()
    {
        $this->title        = 'Add Car Stat';
        $this->breadcrumbs  = [
            'Cars Details',
            'Stats',
            'Add',
        ];

        parent::add($this->model);
    }

    /**
     * @return void
     */
    public function editAction()
    {
        $this->title        = 'Edit Car Stat';
        $this->breadcrumbs  = [
            'Cars Details',
            'Stats',
            'Edit',
        ];

        parent::edit($this->model);
    }
}