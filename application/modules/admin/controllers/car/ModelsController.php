<?php

class Admin_Car_ModelsController extends Admin_Controller
{
    /**
     * @var Model
     */
    public $modelClass = 'Car_Model_Model';

    /**
     * @var string
     */
    public $redirectUrl = 'car/models/index';

    /**
     * @set Model
     * @return void
     */
    public function init()
    {
        $this->model = $this->getNewModel();

        $this->assign('brands', Car_Model_Brand::getInstance()->getCollection()->toSelectArray());
    }

    /**
     * @return void
     */
    public function indexAction()
    {
        if (Get::exists('data.car_brand.0')) {
            $this->movable  = true;
            $this->paginate = false;
            $this->records = $this->model->getRecords([
                'name'
            ], null, 'ord', 'asc');
        } else {
            $this->records = $this->model->getRecords([
                'name'
            ], null, 'name', 'asc');
        }

        $this->title        = 'Car Models';
        $this->breadcrumbs  = [
            'Cars Details',
            'Models',
        ];

        parent::index($this->model);
    }

    /**
     * @return void
     */
    public function addAction()
    {
        $this->title        = 'Add Car Model';
        $this->breadcrumbs  = [
            'Cars Details',
            'Models',
            'Add',
        ];

        parent::add($this->model);
    }

    /**
     * @return void
     */
    public function editAction()
    {
        $this->title        = 'Edit Car Model';
        $this->breadcrumbs  = [
            'Cars Details',
            'Models',
            'Edit',
        ];

        parent::edit($this->model);
    }
}