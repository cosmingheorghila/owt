<?php

class Admin_Car_OptionsController extends Admin_Controller
{
    /**
     * @var Model
     */
    public $modelClass = 'Car_Model_Option';

    /**
     * @var string
     */
    public $redirectUrl = 'car/options/index';

    /**
     * @set Model
     * @return void
     */
    public function init()
    {
        $this->model = $this->getNewModel();
    }

    /**
     * @return void
     */
    public function indexAction()
    {
        $this->records = $this->model->getRecords([
            'name'
        ], null, 'ord', 'asc');

        $this->paginate     = false;
        $this->movable      = true;
        $this->title        = 'Car Options';
        $this->breadcrumbs  = [
            'Cars Details',
            'Options',
        ];

        parent::index($this->model);
    }

    /**
     * @return void
     */
    public function addAction()
    {
        $this->title        = 'Add Car Option';
        $this->breadcrumbs  = [
            'Cars Details',
            'Options',
            'Add',
        ];

        parent::add($this->model);
    }

    /**
     * @return void
     */
    public function editAction()
    {
        $this->title        = 'Edit Car Option';
        $this->breadcrumbs  = [
            'Cars Details',
            'Options',
            'Edit',
        ];

        parent::edit($this->model);
    }
}