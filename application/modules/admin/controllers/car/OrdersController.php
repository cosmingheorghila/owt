<?php

class Admin_Car_OrdersController extends Admin_Controller
{
    /**
     * @var Model
     */
    public $modelClass = 'Car_Model_Order';

    /**
     * @var string
     */
    public $redirectUrl = 'car/orders/index';

    /**
     * @set Model
     * @return void
     */
    public function init()
    {
        $this->model = $this->getNewModel();
    }

    /**
     * @return void
     */
    public function indexAction()
    {
        $this->records = $this->model->getRecords([
            'identifier', 'name'
        ], null, 'created_at', 'desc');

        $this->title        = 'Orders';
        $this->breadcrumbs  = [
            'Shop Panel',
            'Orders',
        ];

        parent::index($this->model);
    }

    /**
     * @return void
     */
    public function viewAction()
    {
        $this->setAdmin();

        $model  = new Car_Model_Order();
        $item   = $model->load(Get::get('id'));
        $car    = Car_Model_Car::getInstance()->load($item->getCarId());

        $model->update([
            'viewed' => Shop_Model_Order::VIEWED_YES
        ])->where([
            'id' => Get::get('id')
        ])->save();

        $this->assign('admin', $this->admin);
        $this->assign('item', $item);
        $this->assign('car', $car);
        $this->assign('title', 'View Order');
        $this->assign('breadcrumbs', [
            'Shop Panel',
            'Orders',
            'View',
        ]);
    }

    /**
     * @return void
     */
    public function editAction()
    {
        $this->title        = 'Edit Order';
        $this->breadcrumbs  = [
            'Shop Panel',
            'Orders',
            'Edit',
        ];

        parent::edit($this->model);
    }
}