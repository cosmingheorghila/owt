<?php

class Admin_Car_CertificationsController extends Admin_Controller
{
    /**
     * @var Model
     */
    public $modelClass = 'Car_Model_Certification';

    /**
     * @var string
     */
    public $redirectUrl = 'car/certifications/index';

    /**
     * @set Model
     * @return void
     */
    public function init()
    {
        $this->model = $this->getNewModel();
    }

    /**
     * @return void
     */
    public function indexAction()
    {
        $this->records = $this->model->getRecords([
            'name'
        ], null, 'ord', 'asc');

        $this->paginate     = false;
        $this->movable      = true;
        $this->title        = 'Mechanics Certifications';
        $this->breadcrumbs  = [
            'Cars Details',
            'Certifications',
        ];

        parent::index($this->model);
    }

    /**
     * @return void
     */
    public function addAction()
    {
        $this->title        = 'Add Mechanic Certification';
        $this->breadcrumbs  = [
            'Cars Details',
            'Certifications',
            'Add',
        ];

        parent::add($this->model);
    }

    /**
     * @return void
     */
    public function editAction()
    {
        $this->title        = 'Edit Mechanic Certification';
        $this->breadcrumbs  = [
            'Cars Details',
            'Certifications',
            'Edit',
        ];

        parent::edit($this->model);
    }
}