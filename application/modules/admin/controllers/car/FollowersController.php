<?php

class Admin_Car_FollowersController extends Admin_Controller
{
    /**
     * @var Model
     */
    public $modelClass = 'Car_Model_Follower';

    /**
     * @var string
     */
    public $redirectUrl = 'car/followers/index';

    /**
     * @set Model
     * @return void
     */
    public function init()
    {
        $this->model = $this->getNewModel();

        $this->assign('cars', Car_Model_Car::getInstance()->order('ord', 'asc')->getCollection()->toSelectArray());
    }

    /**
     * @return void
     */
    public function indexAction()
    {
        $this->records = $this->model->getRecords([
            'email'
        ], null, 'created_at', 'desc');

        $this->title        = 'Followers';
        $this->breadcrumbs  = [
            'Cars Details',
            'Followers',
        ];

        parent::index($this->model);
    }

    /**
     * @return void
     */
    public function addAction()
    {
        $this->title        = 'Add Follower';
        $this->breadcrumbs  = [
            'Cars Details',
            'Followers',
            'Add',
        ];

        parent::add($this->model);
    }

    /**
     * @return void
     */
    public function editAction()
    {
        $this->title        = 'Edit Followers';
        $this->breadcrumbs  = [
            'Cars Details',
            'Followers',
            'Edit',
        ];

        parent::edit($this->model);
    }
}