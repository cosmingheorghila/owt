<?php

class Admin_Users_UsersController extends Admin_Controller
{
    /**
     * @var Model
     */
    public $modelClass = 'Users_Model_User';

    /**
     * @var string
     */
    public $redirectUrl = 'users/users/index';

    /**
     * @set Model
     * @return void
     */
    public function init()
    {
        $this->model = $this->getNewModel();

        $this->assign([
            'statuses'  => Users_Model_User::$statuses,
            'types'     => Users_Model_User::$types
        ]);
    }

    /**
     * @return void
     */
    public function indexAction()
    {
        $this->records = $this->model->getRecords([
            'username', 'email', 'first_name', 'last_name'
        ], null, 'created_at', 'desc');

        $this->title        = 'Users';
        $this->breadcrumbs  = [
            'Access Control',
            'Users',
        ];

        parent::index($this->model);
    }

    /**
     * @return void
     */
    public function addAction()
    {
        $this->title        = 'Add User';
        $this->breadcrumbs  = [
            'Access Control',
            'Users',
            'Add',
        ];

        parent::add($this->model);
    }

    /**
     * @return void
     */
    public function editAction()
    {
        $this->title        = 'Edit User';
        $this->breadcrumbs  = [
            'Access Control',
            'Users',
            'Edit',
        ];

        parent::edit($this->model);
    }
}