<?php

class Admin_Admin_UsersController extends Admin_Controller
{
    /**
     * @var Model
     */
    public $modelClass = 'Admin_Model_User';

    /**
     * @var string
     */
    public $redirectUrl = 'admin/users/index';

    /**
     * @set Model
     * @return void
     */
    public function init()
    {
        $this->model = $this->getNewModel();
    }

    /**
     * @return void
     */
    public function indexAction()
    {
        $this->records = $this->model->getRecords([
            'username', 'email', 'first_name', 'last_name'
        ]);

        $this->title        = 'Admin Users';
        $this->breadcrumbs  = [
            'Access Control',
            'Admin Users',
        ];

        parent::index($this->model);
    }

    /**
     * @return void
     */
    public function addAction()
    {
        $this->title        = 'Add Admin User';
        $this->breadcrumbs  = [
            'Access Control',
            'Admin Users',
            'Add',
        ];

        parent::add($this->model);
    }

    /**
     * @return void
     */
    public function editAction()
    {
        $this->title        = 'Edit Admin User';
        $this->breadcrumbs  = [
            'Access Control',
            'Admin Users',
            'Edit',
        ];

        parent::edit($this->model);

        if (Get::get('id') == 1 && !$this->admin->isDeveloper()) {
            $this->redirectUrl(Url::getAdminUrl() . '/' . $this->redirectUrl);
        }

        $this->assign('levels', explode(',', Admin_Model_User::getInstance()->load(Get::get('id'))->getAccessLevel()));
    }

    /**
     * @return void
     */
    public function forgotPasswordAction()
    {
        $this->setLayout('admin/login.phtml');

        if ($this->getRequest()->isPost()) {
            Admin_Model_User::forgotPassword($_POST['data']['email']);
            $this->redirectUrl(Url::getAdminUrl());
        }

        $this->assign([
            'form' => new Form()
        ]);
    }
}