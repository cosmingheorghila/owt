<?php

class Admin_Admin_AnalyticsController extends Admin_Controller
{
    /**
     * @var Model
     */
    public $modelClass = 'Admin_Model_Analytic';

    /**
     * @var string
     */
    public $redirectUrl = 'admin/analytics/index';

    /**
     * @set Model
     * @return void
     */
    public function init()
    {
        $this->model = $this->getNewModel();
    }

    /**
     * @return void
     */
    public function indexAction()
    {
        $this->title        = 'Google Analytics Code';
        $this->breadcrumbs  = [
            'System Settings',
            'Google Analytics',
        ];

        parent::edit($this->model);
    }
}