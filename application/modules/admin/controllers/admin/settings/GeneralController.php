<?php

class Admin_Admin_Settings_GeneralController extends Admin_Controller
{
    /**
     * @var Model
     */
    public $modelClass = 'Admin_Model_Setting';

    /**
     * @var string
     */
    public $redirectUrl = 'admin/settings/general/index';

    /**
     * @var array
     */
    public $settings = [
        'company-name',
        'company-email',
        'facebook',
        'google',
        'linkedin',
        'pinterest',
        'twitter',
        'instagram',
        'car-registration-documents',
        'car-finishing-touches',
        'car-registration-documents-info',
        'car-finishing-touches-info'
    ];

    /**
     * @set Model
     * @return void
     */
    public function init()
    {
        $this->model = $this->getNewModel();
    }

    /**
     * @throws Exception
     * @return void
     */
    public function indexAction()
    {
        $this->setAdmin();
        $this->model->verifyValidUser();

        $items  = [];
        $status = true;

        foreach ($this->settings as $setting) {
            $item = $this->model->load([
                'name' => $setting
            ]);

            if (!$item) {
                throw new Exception('There is no setting with the name: ' . $setting);
            }

            $items[] = $item;
        }

        if ($this->getRequest()->isPost()) {
            $data = Post::get('data');

            foreach ($data as $name => $value) {
                $this->model->update([
                    'value' => $value
                ])->where([
                    'name' => $name
                ]);

                if (!$this->model->save()) {
                    $status = false;
                }
            }

            if ($status) {
                $this->flashSuccess('Settings updated successfully!');
                $this->redirectUrl(Url::getPreviousUrl());
            } else {
                $this->flashError('System failed! Please check your fields values.');
                $this->redirectUrl(Url::getPreviousUrl());
            }
        }

        $this->assign('form', new Form());
        $this->assign('admin', $this->admin);
        $this->assign('items', $items);
        $this->assign('title', 'General Settings');
        $this->assign('breadcrumbs', [
            'System Settings',
            'General Settings',
        ]);
    }
}