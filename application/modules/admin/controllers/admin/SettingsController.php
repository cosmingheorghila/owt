<?php

class Admin_Admin_SettingsController extends Admin_Controller
{
    /**
     * @var Model
     */
    public $modelClass = 'Admin_Model_Setting';

    /**
     * @var string
     */
    public $redirectUrl = 'admin/settings/index';

    /**
     * @set Model
     * @return void
     */
    public function init()
    {
        $this->model = $this->getNewModel();
    }

    /**
     * @return void
     */
    public function indexAction()
    {
        $this->records = $this->model->getRecords([
            'name', 'value'
        ]);

        $this->title        = 'Website Settings';
        $this->breadcrumbs  = [
            'System Settings',
            'Website Settings',
        ];

        parent::index($this->model);
    }

    /**
     * @return void
     */
    public function addAction()
    {
        $this->setAdmin();

        if (!$this->admin->isDeveloper()) {
            $this->flashError('You do not have permission to add new settings!');
            $this->redirectUrl(Url::getAdminUrl() . '/settings');
        }

        $this->title        = 'Add Website Setting';
        $this->breadcrumbs  = [
            'System Settings',
            'Website Settings',
            'Add',
        ];

        parent::add($this->model);
    }

    /**
     * @return void
     */
    public function editAction()
    {
        $this->title        = 'Edit Website Setting';
        $this->breadcrumbs  = [
            'System Settings',
            'Website Settings',
            'Edit',
        ];

        parent::edit($this->model);
    }
}