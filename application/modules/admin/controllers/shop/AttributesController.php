<?php

class Admin_Shop_AttributesController extends Admin_Controller
{
    /**
     * @var Model
     */
    public $modelClass = 'Shop_Model_Attribute';

    /**
     * @var string
     */
    public $redirectUrl = 'shop/attributes/index';

    /**
     * @set Model
     * @return void
     */
    public function init()
    {
        $this->model = $this->getNewModel();

        $this->assign([
            'sets'              => Shop_Model_Attribute_Set::getInstance()->getCollection()->toSelectArray(),
            'categories'        => Shop_Model_Category::getInstance()->getCategoriesArray(),
            'types'             => Shop_Model_Attribute::$types,
            'filters'           => Shop_Model_Attribute::$filters,
            'everywhereFilters' => Shop_Model_Attribute::$everywhereFilters,
        ]);
    }

    /**
     * @return void
     */
    public function indexAction()
    {
        if (Get::exists('data.shop_attributes_set.0')) {
            $this->movable  = true;
            $this->paginate = false;
            $this->records = $this->model->getRecords([
                'name', 'label', 'value', 'type'
            ], null, 'ord', 'asc');
        } else {
            $this->records = $this->model->getRecords([
                'name', 'label', 'value', 'type'
            ], null, 'set_id', 'asc');
        }

        $this->title        = 'Attributes';
        $this->breadcrumbs  = [
            'Shop Panel',
            'Attributes',
        ];

        parent::index($this->model);
    }

    /**
     * @return void
     */
    public function addAction()
    {
        if (Shop_Model_Attribute_Set::getInstance()->getCollection()->getCount() == 0) {
            $this->flashError('You must first add at least one Attribute Set');
            $this->redirectUrl(Url::getAdminUrl() . '/' . $this->redirectUrl);
        }

        $this->title        = 'Add Attribute';
        $this->breadcrumbs  = [
            'Shop Panel',
            'Attributes',
            'Add',
        ];

        parent::add($this->model);
    }

    /**
     * @return void
     */
    public function editAction()
    {
        $this->title        = 'Edit Attribute';
        $this->breadcrumbs  = [
            'Shop Panel',
            'Attributes',
            'Edit',
        ];

        parent::edit($this->model);

        $this->assign('currentCategories', array_keys($this->model->getItem()->getCategories()->toSelectArray()));
    }

    /**
     * @return void
     */
    public function duplicateAction()
    {
        $model = $this->getNewModel();

        if (Get::exists('id') && ($attribute = $model->load(Get::get('id')))) {
            if ($id = $attribute->duplicate()) {
                $this->flashSuccess('Attribute duplicated successfully! You have been redirected to the duplicated attribute edit page.');
                $this->redirectUrl(Url::getAdminUrl() . '/shop/attributes/edit?id=' . $id);
            }

            $this->flashError('Could not duplicate the attribute!');
            $this->redirectUrl(Url::getPreviousUrl());
        }

        $this->flashError('You are trying to duplicate a non-existent attribute!');
        $this->redirectUrl(Url::getPreviousUrl());
    }
}