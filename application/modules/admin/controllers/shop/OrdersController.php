<?php

class Admin_Shop_OrdersController extends Admin_Controller
{
    /**
     * @var Model
     */
    public $modelClass = 'Shop_Model_Order';

    /**
     * @var string
     */
    public $redirectUrl = 'shop/orders/index';

    /**
     * @set Model
     * @return void
     */
    public function init()
    {
        $this->model = $this->getNewModel();

        $this->assign([
            'currencies'    => Shop_Model_Currency::getInstance()->getCollection()->toSelectArray(false, 'id', 'code'),
            'statuses'      => Shop_Model_Order::$statuses,
            'shippings'     => Shop_Model_Order::$shippingOptions,
            'payments'      => Shop_Model_Order::$paymentTypes,
        ]);
    }

    /**
     * @return void
     */
    public function indexAction()
    {
        $this->records = $this->model->getRecords([
            'identifier', 'raw_total', 'sub_total', 'grand_total', 'status'
        ], null, 'created_at', 'desc');

        $this->title        = 'Orders';
        $this->breadcrumbs  = [
            'Shop Panel',
            'Orders',
        ];

        parent::index($this->model);
    }

    /**
     * @return void
     */
    public function viewAction()
    {
        $this->setAdmin();

        $model  = new Shop_Model_Order();
        $item   = $model->load(Get::get('id'));

        $model->update([
            'viewed' => Shop_Model_Order::VIEWED_YES
        ])->where([
            'id' => Get::get('id')
        ])->save();

        $this->assign('admin', $this->admin);
        $this->assign('item', $item);
        $this->assign('customer', $item->getCustomer());
        $this->assign('products', $item->getItems());
        $this->assign('title', 'View Order');
        $this->assign('breadcrumbs', [
            'Shop Panel',
            'Orders',
            'View',
        ]);
    }

    /**
     * @return void
     */
    public function editAction()
    {
        $this->title        = 'Edit Order';
        $this->breadcrumbs  = [
            'Shop Panel',
            'Orders',
            'Edit',
        ];

        parent::edit($this->model);

        $this->assign([
            'items'     => $this->model->getItem()->getItems(),
            'customer'  => $this->model->getItem()->getCustomer(),
        ]);
    }
}