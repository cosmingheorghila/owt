<?php

class Admin_Shop_Attributes_SetsController extends Admin_Controller
{
    /**
     * @var Model
     */
    public $modelClass = 'Shop_Model_Attribute_Set';

    /**
     * @var string
     */
    public $redirectUrl = 'shop/attributes/sets/index';

    /**
     * @set Model
     * @return void
     */
    public function init()
    {
        $this->model = $this->getNewModel();
    }

    /**
     * @return void
     */
    public function indexAction()
    {
        $this->records = $this->model->getRecords([
            'name', 'label'
        ], null, 'ord', 'asc');

        $this->paginate     = false;
        $this->movable      = true;
        $this->title        = 'Attribute Sets';
        $this->breadcrumbs  = [
            'Shop Panel',
            'Attributes',
            'Sets',
        ];

        parent::index($this->model);
    }

    /**
     * @return void
     */
    public function addAction()
    {
        $this->title        = 'Add Attribute Set';
        $this->breadcrumbs  = [
            'Shop Panel',
            'Attributes',
            'Sets',
            'Add',
        ];

        parent::add($this->model);
    }

    /**
     * @return void
     */
    public function editAction()
    {
        $this->title        = 'Edit Attribute Set';
        $this->breadcrumbs  = [
            'Shop Panel',
            'Attributes',
            'Sets',
            'Edit',
        ];

        parent::edit($this->model);
    }
}