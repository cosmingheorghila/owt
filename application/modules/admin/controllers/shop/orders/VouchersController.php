<?php

class Admin_Shop_Orders_VouchersController extends Admin_Controller
{
    /**
     * @var Model
     */
    public $modelClass = 'Shop_Model_Order_Voucher';

    /**
     * @var string
     */
    public $redirectUrl = 'shop/orders/vouchers/index';

    /**
     * @set Model
     * @return void
     */
    public function init()
    {
        $this->model = $this->getNewModel();

        $this->assign([
            'types'             => Shop_Model_Order_Voucher::$types,
            'availabilities'    => Shop_Model_Order_Voucher::$availability,
        ]);
    }

    /**
     * @return void
     */
    public function indexAction()
    {
        $this->records = $this->model->getRecords([
            'code', 'rate', 'type', 'active', 'maximum_usage'
        ], null, 'created_at', 'desc');

        $this->title        = 'Vouchers';
        $this->breadcrumbs  = [
            'Shop Panel',
            'Vouchers',
        ];

        parent::index($this->model);
    }

    /**
     * @return void
     */
    public function addAction()
    {
        $this->title        = 'Add Voucher';
        $this->breadcrumbs  = [
            'Shop Panel',
            'Vouchers',
            'Add',
        ];

        parent::add($this->model);
    }

    /**
     * @return void
     */
    public function editAction()
    {
        $this->title        = 'Edit Voucher';
        $this->breadcrumbs  = [
            'Shop Panel',
            'Vouchers',
            'Edit',
        ];

        parent::edit($this->model);
    }
}