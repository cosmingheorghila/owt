<?php

class Admin_Shop_Products_TaxesController extends Admin_Controller
{
    /**
     * @var Model
     */
    public $modelClass = 'Shop_Model_Product_Tax';

    /**
     * @var string
     */
    public $redirectUrl = 'shop/products/taxes/index';

    /**
     * @set Model
     * @return void
     */
    public function init()
    {
        $this->model = $this->getNewModel();

        $this->assign([
            'types'             => Shop_Model_Product_Tax::$types,
            'applicabilities'   => Shop_Model_Product_Tax::$applicabilities,
            'availabilities'    => Shop_Model_Product_Tax::$availability,
        ]);
    }

    /**
     * @return void
     */
    public function indexAction()
    {
        $this->records = $this->model->getRecords([
            'name', 'rate', 'type', 'active', 'applicability'
        ], null, 'name', 'asc');

        $this->title        = 'Taxes';
        $this->breadcrumbs  = [
            'Shop Panel',
            'Taxes',
        ];

        parent::index($this->model);
    }

    /**
     * @return void
     */
    public function addAction()
    {
        $this->title        = 'Add Tax';
        $this->breadcrumbs  = [
            'Shop Panel',
            'Taxes',
            'Add',
        ];

        parent::add($this->model);
    }

    /**
     * @return void
     */
    public function editAction()
    {
        $this->title        = 'Edit Tax';
        $this->breadcrumbs  = [
            'Shop Panel',
            'Taxes',
            'Edit',
        ];

        parent::edit($this->model);
    }
}