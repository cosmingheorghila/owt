<?php

class Admin_Shop_Products_EnquiriesController extends Admin_Controller
{
    /**
     * @var Model
     */
    public $modelClass = 'Shop_Model_Product_Enquiry';

    /**
     * @var string
     */
    public $redirectUrl = 'shop/products/enquiries/index';

    /**
     * @set Model
     * @return void
     */
    public function init()
    {
        $this->model = $this->getNewModel();
    }

    /**
     * @return void
     */
    public function indexAction()
    {
        $this->records = $this->model->getRecords([
            'email', 'phone'
        ], null, 'created_at', 'desc');

        $this->title        = 'Enquiries';
        $this->breadcrumbs  = [
            'Shop Panel',
            'Enquiries',
        ];

        parent::index($this->model);
    }

    /**
     * @return void
     */
    public function editAction()
    {
        Shop_Model_Product_Enquiry::getInstance()->update([
            'viewed' => Shop_Model_Order::VIEWED_YES
        ])->where([
            'id' => Get::get('id')
        ])->save();

        $this->title        = 'Edit Enquiry';
        $this->breadcrumbs  = [
            'Shop Panel',
            'Enquiries',
            'Edit',
        ];

        parent::edit($this->model);
    }
}