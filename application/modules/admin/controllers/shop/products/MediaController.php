<?php

class Admin_Shop_Products_MediaController extends Admin_Controller
{
    /**
     * @var Model
     */
    public $modelClass = 'Shop_Model_Product_Media';

    /**
     * @var string
     */
    public $redirectUrl = 'shop/products/media/index';

    /**
     * @set Model
     * @return void
     */
    public function init()
    {
        if (!Get::exists('product') || !Shop_Model_Product::getInstance()->load(Get::get('product'))) {
            $this->flashError('You are trying to access the media of a non-product!');
            $this->redirectUrl(Url::getPreviousUrl());
        }

        $this->model        = $this->getNewModel();
        $this->redirectUrl  = $this->redirectUrl . '?product=' . Get::get('product');

        $this->assign([
            'types'     => Shop_Model_Product_Media::$types,
            'product'   => Get::get('product'),
        ]);
    }

    /**
     * @return void
     */
    public function indexAction()
    {
        $this->movable  = true;
        $this->paginate = false;
        $this->records  = $this->model->getRecords(
            ['media', 'type'],
            ['product_id' => Get::get('product')],
            'ord', 'asc'
        );

        $this->title        = 'Product Media';
        $this->breadcrumbs  = [
            'Shop Panel',
            'Products',
            'Media',
        ];

        parent::index($this->model);
    }

    /**
     * @return void
     */
    public function addAction()
    {
        $this->title        = 'Add Product Media';
        $this->breadcrumbs  = [
            'Shop Panel',
            'Products',
            'Media',
            'Add',
        ];

        parent::add($this->model);
    }

    /**
     * @return void
     */
    public function editAction()
    {
        $this->title        = 'Edit Product Media';
        $this->breadcrumbs  = [
            'Shop Panel',
            'Products',
            'Media',
            'Edit',
        ];

        parent::edit($this->model);
    }
}