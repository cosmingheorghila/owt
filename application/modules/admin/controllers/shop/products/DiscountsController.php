<?php

class Admin_Shop_Products_DiscountsController extends Admin_Controller
{
    /**
     * @var Model
     */
    public $modelClass = 'Shop_Model_Product_Discount';

    /**
     * @var string
     */
    public $redirectUrl = 'shop/products/discounts/index';

    /**
     * @set Model
     * @return void
     */
    public function init()
    {
        $this->model = $this->getNewModel();

        $this->assign([
            'types'             => Shop_Model_Product_Discount::$types,
            'applicabilities'   => Shop_Model_Product_Discount::$applicabilities,
            'availabilities'    => Shop_Model_Product_Discount::$availability,
        ]);
    }

    /**
     * @return void
     */
    public function indexAction()
    {
        $this->records = $this->model->getRecords([
            'name', 'rate', 'type', 'active', 'applicability'
        ], null, 'name', 'asc');

        $this->title        = 'Discounts';
        $this->breadcrumbs  = [
            'Shop Panel',
            'Discounts',
        ];

        parent::index($this->model);
    }

    /**
     * @return void
     */
    public function addAction()
    {
        $this->title        = 'Add Discount';
        $this->breadcrumbs  = [
            'Shop Panel',
            'Discounts',
            'Add',
        ];

        parent::add($this->model);
    }

    /**
     * @return void
     */
    public function editAction()
    {
        $this->title        = 'Edit Discount';
        $this->breadcrumbs  = [
            'Shop Panel',
            'Discounts',
            'Edit',
        ];

        parent::edit($this->model);
    }
}