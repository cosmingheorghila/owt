<?php

class Admin_Shop_ProductsController extends Admin_Controller
{
    /**
     * @var Model
     */
    public $modelClass = 'Shop_Model_Product';

    /**
     * @var string
     */
    public $redirectUrl = 'shop/products/index';

    /**
     * @set Model
     * @return void
     */
    public function init()
    {
        $this->model = $this->getNewModel();

        $this->assign('stocks', Shop_Model_Product::$stocks);
        $this->assign('availabilities', Shop_Model_Product::$availability);
        $this->assign('categories', Shop_Model_Category::getInstance()->getCategoriesArray());
        $this->assign('sets', Shop_Model_Attribute_Set::getInstance()->getCollection());
        $this->assign('vendors', Shop_Model_Vendor::getInstance()->getCollection());
        $this->assign('currencies', Shop_Model_Currency::getInstance()->getCollection()->toSelectArray(false, 'id', 'code'));
        $this->assign('discounts', Shop_Model_Product_Discount::getInstance()->where([
            'active' => Shop_Model_Product_Discount::ACTIVE_YES,
            'applicability' => Shop_Model_Product_Discount::APPLICABILITY_PRODUCT,
        ])->getCollection());
        $this->assign('taxes', Shop_Model_Product_Tax::getInstance()->where([
            'active'        => Shop_Model_Product_Tax::ACTIVE_YES,
            'applicability' => Shop_Model_Product_Tax::APPLICABILITY_PRODUCT,
        ])->getCollection());
    }

    /**
     * @return void
     */
    public function indexAction()
    {
        $this->records = $this->model->getRecords([
            'sku', 'name', 'price', 'stock', 'quantity'
        ], null, 'sku', 'asc');

        $this->title        = 'Products';
        $this->breadcrumbs  = [
            'Shop Panel',
            'Products',
        ];

        parent::index($this->model);
    }

    /**
     * @return void
     */
    public function addAction()
    {
        $this->title        = 'Add Product';
        $this->breadcrumbs  = [
            'Shop Panel',
            'Products',
            'Add',
        ];

        parent::add($this->model);
    }

    /**
     * @return void
     */
    public function editAction()
    {
        $this->title        = 'Edit Product';
        $this->breadcrumbs  = [
            'Shop Panel',
            'Products',
            'Edit',
        ];

        parent::edit($this->model);

        $this->assign('currentCategories', array_keys($this->model->getItem()->getCategories()->toSelectArray()));
        $this->assign('currentAttributes', $this->model->getItem()->getAttributesJoined());
        $this->assign('currentVendors', $this->model->getItem()->getVendors());
        $this->assign('currentDiscounts', $this->model->getItem()->getDiscounts());
        $this->assign('currentTaxes', $this->model->getItem()->getTaxes());
    }

    /**
     * @return void
     */
    public function duplicateAction()
    {
        $model = $this->getNewModel();

        if (Get::exists('id') && ($product = $model->load(Get::get('id')))) {
            if ($id = $product->duplicate()) {
                $this->flashSuccess('Product duplicated successfully! You have been redirected to the duplicated product edit page.');
                $this->redirectUrl(Url::getAdminUrl() . '/shop/products/edit?id=' . $id);
            } else {
                $this->flashError('Could not duplicate the product!');
                $this->redirectUrl(Url::getPreviousUrl());
            }
        } else {
            $this->flashError('You are trying to duplicate a non-existent product!');
            $this->redirectUrl(Url::getPreviousUrl());
        }
    }
}