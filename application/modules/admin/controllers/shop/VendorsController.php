<?php

class Admin_Shop_VendorsController extends Admin_Controller
{
    /**
     * @var Model
     */
    public $modelClass = 'Shop_Model_Vendor';

    /**
     * @var string
     */
    public $redirectUrl = 'shop/vendors/index';

    /**
     * @set Model
     * @return void
     */
    public function init()
    {
        $this->model = $this->getNewModel();
    }

    /**
     * @return void
     */
    public function indexAction()
    {
        $this->records = $this->model->getRecords([
            'name'
        ], null, 'name', 'asc');

        $this->title        = 'Vendors';
        $this->breadcrumbs  = [
            'Shop Panel',
            'Vendors',
        ];

        parent::index($this->model);
    }

    /**
     * @return void
     */
    public function addAction()
    {
        $this->title        = 'Add Vendor';
        $this->breadcrumbs  = [
            'Shop Panel',
            'Vendors',
            'Add',
        ];

        parent::add($this->model);
    }

    /**
     * @return void
     */
    public function editAction()
    {
        $this->title        = 'Edit Vendor';
        $this->breadcrumbs  = [
            'Shop Panel',
            'Vendors',
            'Edit',
        ];

        parent::edit($this->model);
    }
}