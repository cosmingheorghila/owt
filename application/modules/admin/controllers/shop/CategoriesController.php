<?php

class Admin_Shop_CategoriesController extends Admin_Controller
{
    /**
     * @var Model
     */
    public $modelClass = 'Shop_Model_Category';

    /**
     * @var string
     */
    public $redirectUrl = 'shop/categories/index';

    /**
     * @set Model
     * @return void
     */
    public function init()
    {
        $this->model = $this->getNewModel();

        $this->assign([
            'sets'  => Shop_Model_Attribute_Set::getInstance()->getCollection()->toSelectArray(),
            'types' => Shop_Model_Category_Type::getInstance()->getCollection()->toSelectArray(true),
        ]);
    }

    /**
     * @return void
     */
    public function indexAction()
    {
        if (Get::exists('data.shop_product_types.0')) {
            $this->movable  = true;
            $this->paginate = false;
            $this->records  = $this->model->getRecords([
                'name', 'description'
            ], null, 'ord', 'asc');
        } else {
            $this->records = $this->model->getRecords([
                'name', 'description'
            ], null, 'name', 'asc');
        }

        $this->title        = 'Categories';
        $this->breadcrumbs  = [
            'Shop Panel',
            'Categories',
        ];

        parent::index($this->model);
    }

    /**
     * @return void
     */
    public function addAction()
    {
        $this->title        = 'Add Category';
        $this->breadcrumbs  = [
            'Shop Panel',
            'Categories',
            'Add',
        ];

        parent::add($this->model);
    }

    /**
     * @return void
     */
    public function editAction()
    {
        $this->title        = 'Edit Category';
        $this->breadcrumbs  = [
            'Shop Panel',
            'Categories',
            'Edit',
        ];

        parent::edit($this->model);

        $this->assign('currentSets', array_keys($this->model->getItem()->getSets()->toSelectArray()));
    }
}