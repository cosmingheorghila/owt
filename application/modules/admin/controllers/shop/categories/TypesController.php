<?php

class Admin_Shop_Categories_TypesController extends Admin_Controller
{
    /**
     * @var Model
     */
    public $modelClass = 'Shop_Model_Category_Type';

    /**
     * @var string
     */
    public $redirectUrl = 'shop/categories/types/index';

    /**
     * @set Model
     * @return void
     */
    public function init()
    {
        $this->model = $this->getNewModel();
    }

    /**
     * @return void
     */
    public function indexAction()
    {
        $this->records = $this->model->getRecords([
            'name', 'description'
        ], null, 'ord', 'asc');

        $this->paginate     = false;
        $this->movable      = true;
        $this->title        = 'Types';
        $this->breadcrumbs  = [
            'Shop Panel',
            'Types',
        ];

        parent::index($this->model);
    }

    /**
     * @return void
     */
    public function addAction()
    {
        $this->title        = 'Add Type';
        $this->breadcrumbs  = [
            'Shop Panel',
            'Types',
            'Add',
        ];

        parent::add($this->model);
    }

    /**
     * @return void
     */
    public function editAction()
    {
        $this->title        = 'Edit Type';
        $this->breadcrumbs  = [
            'Shop Panel',
            'Types',
            'Edit',
        ];

        parent::edit($this->model);
    }
}