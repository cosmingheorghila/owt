<?php

class Admin_Model_User extends Cms_Model
{
    /**
     * @var string
     */
    protected $table = 'admin_users';

    /**
     * @var array
     */
    protected $validates = [
        'validation' => [
            'username' => [
                'presence',
            ],
            'password' => [
                'presence',
            ],
            'email' => [
                'presence',
                'email',
            ],
            'first_name' => [
                'presence',
                'letter',
            ],
            'last_name' => [
                'presence',
                'letter',
            ],
        ],
        'unique' => [
            'username',
        ],
    ];

    /**
     * @const
     */
    const SUPER_NO  = 0;
    const SUPER_YES = 1;

    /**
     * @var array
     */
    public static $super = [
        self::SUPER_NO  => 'No',
        self::SUPER_YES => 'Yes',
    ];

    /**
     * @set $class
     */
    public function __construct()
    {
        parent::__construct();

        $this->class = get_class();
    }

    /**
     * @return void
     */
    protected function beforeSave()
    {
        parent::beforeSave();

        if ($this->params['password']) {
            $this->params['password'] = Hash::make($this->params['password']);
        }

        if ($this->model) {
            if (!$this->params['password']) {
                $this->params['password'] = $_POST['data']['password'] = $this->model->getPassword();
            }

            if ($this->params['access_level']) {
                $this->params['access_level'] = implode(',', $this->params['access_level']);
            } else {
                $this->params['access_level'] = '';
            }
        }
    }

    /**
     * @return bool
     */
    public static function isLogged()
    {
        return Session::exists('admin-user') ? true : false;
    }

    /**
     * @return bool
     */
    public static function isDeveloper()
    {
        return self::getInstance()->load(
            Session::get('admin-user')
        )->getId() == 1 ? true : false;
    }

    /**
     * @return bool
     */
    public static function isSuperAdmin()
    {
        return self::getInstance()->load(
            Session::get('admin-user')
        )->getSuperAdmin() == self::SUPER_YES ? true : false;
    }

    /**
     * @param string $username
     * @param string $password
     * @return bool
     */
    public static function login($username, $password)
    {
        $item = self::getInstance()->load([
            'username' => $username,
        ]);

        if ($item && Hash::check($password, $item->getPassword())) {
            Session::set('admin-user', $item->getId());
            return true;
        }

        Flash::error('Invalid credentials supplied!');
        return false;
    }

    /**
     * @return void
     */
    public static function logout()
    {
        Session::set('admin-user', false);
        Redirect::url(Url::getAdminUrl());
    }

    /**
     * @param string $email
     * @return bool
     */
    public static function forgotPassword($email)
    {
        $item = self::getInstance()->load([
            'email' => $email
        ]);

        if ($item) {
            $password = String::random();

            $mail = new Email('postmark');
            $mail->from(
                Setting::get('company-email')->getValue()
            )->to(
                trim($email)
            )->subject(
                Setting::get('company-name')->getValue() . ' - Admin Password Recovery'
            )->messageHtml('emails/forgot_password.phtml', [
                'thePassword' => $password
            ]);

            if ($mail->send()) {
                Database::getInstance('admin_users')->update([
                    'password' => Hash::make($password)
                ])->where([
                    'id' => $item->getId()
                ])->save();

                Session::set('flash-message', '<div class="flash-home"><span class="success">Your new admin password has been sent to your email!</span></div>');
                return true;
            }

            Session::set('flash-message', '<div class="flash-home"><span class="error">Could not send the email! Please try again later.</span></div>');
            return false;
        }

        Session::set('flash-message', '<div class="flash-home"><span class="error">There is no admin user with that email!</span></div>');
        return false;
    }
}