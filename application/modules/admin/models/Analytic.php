<?php

class Admin_Model_Analytic extends Cms_Model
{
    /**
     * @var string
     */
    protected $table = 'admin_analytics';

    /**
     * @set $class
     */
    public function __construct()
    {
        parent::__construct();

        $this->class = get_class();
    }
}