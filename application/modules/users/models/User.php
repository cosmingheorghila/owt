<?php

class Users_Model_User extends Cms_Model
{
    /**
     * @var string
     */
    protected $table = 'users';

    /**
     * @var array
     */
    protected $validates = [
        'validation' => [
            'username' => [
                'presence',
            ],
            'email' => [
                'presence',
                'email',
            ],
            'password' => [
                'presence',
            ],
        ],
        'unique' => [
            'email',
            'username',
        ],
    ];

    /**
     * @var array
     */
    protected $hasMany = [
        'orders' => [
            'model'         => 'Shop_Model_Order',
            'remote_key'    => 'user_id',
            'order_field'   => 'updated_at',
            'order_dir'     => 'desc',
        ],
        'carts' => [
            'model'         => 'Shop_Model_Cart',
            'remote_key'    => 'user_id',
            'order_field'   => 'updated_at',
            'order_dir'     => 'desc',
        ],
        'customers' => [
            'model'         => 'Shop_Model_Customer',
            'remote_key'    => 'user_id',
            'order_field'   => 'created_at',
            'order_dir'     => 'desc',
        ],
    ];

    /**
     * @var array
     */
    protected $actsAs = [
        'timestamp',
    ];

    /**
     * @var string|bool
     */
    private static $usernameField   = 'username';
    private static $passwordField   = 'password';
    private static $codeField       = 'code';
    private static $statusField     = 'status';

    /**
     * @var string
     */
    private static $verificationUrl = '/users/index/validate';

    /**
     * @const
     */
    const VERIFIED_NO   = 0;
    const VERIFIED_YES  = 1;

    /**
     * @const
     */
    const TYPE_ACTIVE       = 1;
    const TYPE_INACTIVE     = 2;
    const TYPE_SUSPENDED    = 3;

    /**
     * @var array
     */
    public static $statuses = [
        self::VERIFIED_NO   => 'No',
        self::VERIFIED_YES  => 'Yes',
    ];

    /**
     * @var array
     */
    public static $types = [
        self::TYPE_ACTIVE       => 'Active',
        self::TYPE_INACTIVE     => 'Inactive',
        self::TYPE_SUSPENDED    => 'Suspended',
    ];

    /**
     * @set $class
     */
    public function __construct()
    {
        parent::__construct();

        $this->class = get_class();
    }

    /**
     * @return void
     */
    protected function beforeSave()
    {
        parent::beforeSave();

        if ($this->params['password']) {
            $this->params['password'] = Hash::make($this->params['password']);
        }

        if ($this->model) {
            if (!$this->params['password']) {
                $this->params['password'] = $_POST['data']['password'] = $this->model->getPassword();
            }
        }
    }

    /**
     * @return bool
     */
    public static function isLogged()
    {
        return Session::exists('user') || (Cookie::exists('username') && Cookie::exists('password')) ? true : false;
    }

    /**
     * @return bool
     */
    public function isSubscribed()
    {
        return Cms_Model_Newsletter_Subscriber::getInstance()->load([
            'email' => $this->getEmail()
        ]) ? true : false;
    }

    /**
     * @param int $id
     */
    public static function setUser($id)
    {
        Session::set('user', $id);
    }

    /**
     * @return mixed
     */
    public static function getUser()
    {
        if (Session::exists('user')) {
            return Bootstrap::getInstance()->user ?: self::getInstance()->load(Session::get('user'));
        } elseif (Cookie::exists('username') && Cookie::exists('password')) {
            return self::getInstance()->load([
                self::$usernameField => Cookie::get('username'),
                self::$passwordField => Cookie::get('password'),
            ]);
        }

        return null;
    }

    /**
     * @param string $username
     * @param string $password
     * @param bool $remember
     * @return bool
     * @throws Exception
     */
    public static function login($username, $password, $remember = false)
    {
        $user = self::getInstance()->load([
            self::$usernameField    => $username,
            self::$statusField      => self::VERIFIED_YES
        ]);

        if ($user && Hash::check($password, $user->getPassword())) {
            if ($user->getType() != self::TYPE_ACTIVE) {
                Flash::error('Your account status is ' . self::$types[$user->getType()] . ', therefore you cannot login!');
                return false;
            }

            if ($remember) {
                setcookie('username', $username, time() + 60 * 60 * 24 * 30);
                setcookie('password', Hash::make($password), time() + 60 * 60 * 24 * 30);
            } else {
                Bootstrap::getInstance()->user = $user;
                Session::set('user', $user->getId());
            }

            Flash::success('Login successful!');
            return $user;
        }

        Flash::error('Invalid credentials supplied!');
        return false;
    }

    /**
     * @return void
     */
    public static function logout()
    {
        Session::set('user', false);
        Cookie::set('username', false);
        Cookie::set('password', false);
    }

    /**
     * @param array $data
     * @param email $email
     * @return bool
     * @throws Exception
     */
    public static function register($data, $email)
    {
        if (!$email) {
            Flash::error('The EMAIL parameter cannot be empty!');
            return false;
        }

        if ($data[self::$usernameField] == $data[self::$passwordField]) {
            Flash::error('The ' . self::$usernameField . '\'s value cannot be the same as the ' . self::$passwordField . '\'s value!');
            return false;
        }

        $code   = String::random(20);
        $values = [];

        foreach ($data as $key => $value) {
            $values[$key] = self::$passwordField ? Hash::make($value) : $value;
        }

        $values['code'] = $code;

        if ($id = self::getInstance()->merge('insert', $values)) {
            $mail = new Email('postmark');
            $mail->from(
                Setting::get('company-email')->getValue()
            )->to(
                $email
            )->subject(
                'User Registration Verification'
            )->messageHtml(
                'emails/user_validation.phtml', [
                    'theUrl'    => Url::getHostUrl() . Url::getPrefix() . self::$verificationUrl,
                    'theCode'   => $code
                ]
            );

            if ($mail->send()) {
                Flash::success('You registered successfully! Please check your email to verify your account.');
                return true;
            } else {
                self::getInstance()->delete()->where([
                    'id' => $id
                ])->save();

                Flash::error('The email could not be sent. Please try again later.');
                return false;
            }
        }

        return false;
    }

    /**
     * @return bool
     */
    public static function validate()
    {
        if (!Get::exists('code')) {
            Flash::error('The code is missing. Check your URL!');
            return false;
        }

        $code = Get::get('code');
        $user = self::getInstance()->load([
            self::$codeField    => $code,
            self::$statusField  => self::VERIFIED_NO,
        ]);

        if ($user) {
            if (!$user->getCode()) {
                Flash::error('The code you supplied is invalid!');
                return false;
            }

            $database = new Database('users');
            $database->update([
                self::$codeField    => '',
                self::$statusField  => self::VERIFIED_YES,
            ])->where([
                'code' => $code
            ]);

            if ($database->save()) {
                Flash::success('User verification successful!');
                return true;
            }

            Flash::error('User verification failed!');
            return false;
        }

        Flash::error('The user with that code is already verified!');
        return false;
    }
}