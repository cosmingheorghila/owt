<?php

class Users_IndexController extends Cms_Controller
{
    /**
     * @return void
     */
    public function indexAction()
    {

    }

    /**
     * @return void
     */
    public function validateAction()
    {
        $this->disableLayout();
        $this->disableView();

        (new Users_Model_User())->validate();
        $this->redirectPage('home');
    }
}