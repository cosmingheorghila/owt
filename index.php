<?php

error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT);
define('BASE_PATH', realpath(dirname(__FILE__)));
session_start(); ob_start();

require_once('library/_Autoloader.php');
require_once('library/_Initialize.php');
require_once('library/_Redirects.php');

if (Http_Auth::required()) {
    Http_Auth::authorize(function(){
        Bootstrap::getInstance()->execute();
    });
} else {
    Bootstrap::getInstance()->execute();
}