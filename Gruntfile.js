module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        concat: {
            backend_js: {
                src: [
                    'public/assets/admin/js/libs/jquery.js',
                    'public/assets/admin/js/libs/jquery-ui.js',
                    'public/assets/admin/js/libs/nano-scroller.js',
                    'public/assets/admin/js/libs/chosen.js',
                    'public/assets/admin/js/libs/prism.js',
                    'public/assets/admin/js/libs/table-draggable.js',
                    'public/assets/admin/js/libs/timepicker.js',
                    'public/assets/admin/js/partials/main.js'
                ],
                dest: 'public/assets/admin/js/super.js'
            },
            backend_css: {
                src: [
                    'public/assets/admin/css/partials/style.css',
                    'public/assets/admin/css/libs/nano-scroller.css',
                    'public/assets/admin/css/libs/jquery-ui.css',
                    'public/assets/admin/css/libs/prism.css',
                    'public/assets/admin/css/libs/chosen.css',
                    'public/assets/admin/css/libs/timepicker.css'
                ],
                dest: 'public/assets/admin/css/super.css'
            },
            frontend_js: {
                src: [
                    'public/assets/default/js/libs/jquery.js',
                    'public/assets/default/js/libs/jquery-ui.js',
                    'public/assets/default/js/helpers/mousewheel.js',
                    'public/assets/default/js/helpers/number.js',
                    'public/assets/default/js/helpers/slider.js',
                    'public/assets/default/js/helpers/autocomplete.js',
                    'public/assets/default/js/helpers/video.js',
                    'public/assets/default/js/helpers/easing.js',
                    'public/assets/default/js/helpers/tooltip.js',
                    'public/assets/default/js/helpers/scroll.js',
                    'public/assets/default/js/helpers/format.js',
                    'public/assets/default/js/helpers/fancybox.js',
                    'public/assets/default/js/partials/main.js'
                ],
                dest: 'public/assets/default/js/super.js'
            },
            frontend_css: {
                src: [
                    'public/assets/default/css/partials/general.css',
                    'public/assets/default/css/partials/header.css',
                    'public/assets/default/css/partials/content.css',
                    'public/assets/default/css/partials/footer.css',
                    'public/assets/default/css/partials/responsive.css',
                    'public/assets/default/css/helpers/slider.css',
                    'public/assets/default/css/helpers/autocomplete.css',
                    'public/assets/default/css/helpers/tooltip.css',
                    'public/assets/default/css/helpers/tooltip.css',
                    'public/assets/default/css/helpers/scroll.css',
                    'public/assets/default/css/helpers/gallery.css',
                    'public/assets/default/css/helpers/fancybox.css',
                    'public/assets/default/css/helpers/structure.css',
                    'public/assets/default/css/helpers/theme.css'
                ],
                dest: 'public/assets/default/css/super.css'
            }
        },
        uglify: {
            backend_js: {
                files: {
                    'public/assets/admin/js/super.min.js': ['public/assets/admin/js/super.js']
                }
            },
            frontend_js: {
                files: {
                    'public/assets/default/js/super.min.js': ['public/assets/default/js/super.js']
                }
            }
        },
        cssmin: {
            options: {
                rebase: false
            },
            backend_css: {
                files: {
                    'public/assets/admin/css/super.min.css': ['public/assets/admin/css/super.css']
                }
            },
            frontend_css: {
                files: {
                    'public/assets/default/css/super.min.css': ['public/assets/default/css/super.css']
                }
            },
            not_found_css: {
                files: {
                    'public/assets/admin/css/not-found.min.css': ['public/assets/admin/css/partials/not-found.css']
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');

    grunt.registerTask('default', ['concat', 'uglify', 'cssmin']);
};