<?php

get_header();

if ( have_posts() ){

    the_post();

	// display header section
	get_template_part('sections/header_section'); 
	
	// display search section
	get_template_part('sections/search_section');

?>
	<!-- Page Content -->
	<div id="page-content">
	
    <!-- Main -->
	<div id="main">
    	<div id="main-content" class="hidden">
			<div class="blog-title-container hidden">
				<h1><?php  echo wp_kses_post( $soho_theme_options['clapat_soho_blog_default_title'] ); ?></h1>
			</div>
		
			<!-- Blog -->
			<div id="blog">
		<?php 
				
			$soho_paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
			
			$soho_args = array(
				'post_type' => 'post',
				'paged' => $soho_paged
			);
			$soho_posts_query = new WP_Query( $soho_args );

			// the loop
			while( $soho_posts_query->have_posts() ){

				$soho_posts_query->the_post();

				get_template_part( 'sections/blog_post_section' );
				
			}
					
		?>
			<!-- /Blog -->
			</div>
		<?php
				
			soho_pagination( $soho_posts_query );

			wp_reset_postdata();
		?>	
		</div>
	</div>
    <!--/Main -->

<?php

}
	
get_footer();

?>