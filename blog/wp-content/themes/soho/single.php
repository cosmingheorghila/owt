<?php

get_header();

while ( have_posts() ){

    the_post();

?>

	<?php get_template_part('sections/header_section'); ?>
	
	<?php get_template_part('sections/search_section'); 	?>
	
	<!-- Page Content -->
	<div id="page-content">
	<?php 
		
		// display hero section
		$soho_hero_image = soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-blog-hero-image' );
		
		if( !empty( $soho_hero_image['url'] ) ){
		
			get_template_part('sections/hero_section'); 
		}
		else {
			
			get_template_part('sections/blog_post_no_hero_section');
		}
		
	?>
	
	<div id="main">
       	<div id="main-content" class="hidden">
        	<!-- Blog -->
        	<div id="blog">
						
				<!-- Post Content -->
	            <div <?php post_class('post-content-full'); ?>>
					
					<?php if ( !function_exists( 'vc_set_as_theme' ) ) { // if Visual Composer is not installed, add a container?>
					<div class="container_post">
					<?php } ?>
					<?php the_content(); ?>
					<?php if ( !function_exists( 'vc_set_as_theme' ) ) { // if Visual Composer is not installed, add a container?>
					</div>
					<?php } ?>
						
					<div class="page-links">
					<?php
						wp_link_pages();
					?>
					</div>
	            </div>
	            <!--/Post Content -->
            
            </div>        
        	<!--/Blog -->
					
			<!-- Article Meta -->
			<div class="article-meta text-align-center">
				
				<div class="article-date">
                    <div class="outer">
                        <div class="inner">
                        	<ul class="entry-meta">
                                <li class="entry-date"><?php the_time('F j, Y'); ?></li>
                            </ul>
                        </div>
                    </div>
                </div>
				
				<?php if ( has_tag() ) { ?>
				<?php the_tags('<div class="article-tags"><div class="outer"><div class="inner"><ul class="entry-meta"><li class="entry-tags">', '</li><li class="entry-tags">', '</li></ul></div></div></div>'); ?>
				<?php } else { ?>
					<div class="article-tags"><div class="outer"><div class="inner"><ul class="entry-meta"><li class="entry-tags"><?php esc_html_e('No Tags', 'soho'); ?></li></ul></div></div></div>
				<?php } ?>
				
				<div class="article-comments">
					<div class="outer">
						<div class="inner">
							<ul class="entry-meta">
								<li class="entry-comments"><?php comments_popup_link(); ?></li>
							</ul>
						</div>
					</div>
				</div>
				
			</div>
			<!--/Article Meta -->
			
			<!-- Article Next -->
			<?php previous_post_link( 'blog-post', $soho_theme_options['clapat_soho_blog_next_post_caption']); ?>
            <!--/Article Next -->
			
			<?php comments_template(); ?>
		</div>
	</div>	
	
<?php

}

get_footer();

?>
