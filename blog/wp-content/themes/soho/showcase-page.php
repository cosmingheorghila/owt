<?php
/*
Template name: Showcase Template
*/
get_header();

require_once ( get_template_directory() . '/include/util_functions.php');

while ( have_posts() ){

the_post();

?>
		<?php 
		
		// display header section
		get_template_part('sections/header_section'); 		
		
		?>
		
		<!-- Page Content -->
		<div id="page-content" class="show-no-footer">
		
			<!--Showcase Slider -->
			<div class="pagepillingslider hidden">   

						<?php

						$soho_args = array(
							'post_type' => 'soho_showcase',
							'orderby'   => 'menu_order',
							'order'     => 'ASC',
							'posts_per_page' => -1
						);

						$soho_query_slides = new WP_Query( $soho_args );
						$arr_slides_titles = array();
						
						if( $soho_query_slides->have_posts() ){

							$soho_first_slide = true;
							while ( $soho_query_slides->have_posts() ) {

								$soho_query_slides->the_post();

								$soho_slide_type = soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-showcase-slide-type');
								
								$soho_image = soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-showcase-slider-image');
								$soho_image_path   = trim( $soho_image['url'] );

								$soho_bknd_repeat  = soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-showcase-slider-bknd-repeat' );
								$soho_style_repeat = '';
								if( $soho_bknd_repeat ){

									$soho_style_repeat = 'background-repeat: repeat; ';
								}
								
								$soho_slide_video_bknd_webm = soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-showcase-slide-video-webm');
								$soho_slide_video_bknd_mp4  = soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-showcase-slide-video-mp4');

								$soho_caption_alignment = soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-showcase-slider-caption-alignment' );

								$soho_overlay_color   = soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-showcase-slider-overlay-color' );
								$soho_overlay_opacity = soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-showcase-slider-overlay-opacity' );
								$soho_overlay_rgba    = clapat_hex2rgba( $soho_overlay_color, $soho_overlay_opacity );

								$soho_content_type    = soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-showcase-slider-content-type' );
								$soho_class_content_type = '';
								$soho_class_slide_background = '';
								if( $soho_content_type == 'light' ){

									$soho_class_content_type = ' light-content';
									$soho_class_slide_background = 'class="dark-bg" ';
								}
								
								$soho_display_scroll_arrow = soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-showcase-show-scroll' );
								
								$soho_animated_class = '';
								if( $soho_first_slide ){
									
									$soho_animated_class = 'animated';
									$soho_first_slide = false;
								}
								
								$bknd_image = soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-showcase-slide-video-image' );
								
								$arr_slides_titles[] = get_the_title();
						?>

							<?php if( $soho_slide_type == "video" ){ ?>
							
							<!-- Video Slide -->
							<div class="section video <?php echo sanitize_html_class( $soho_class_content_type ); ?>" style="background-image:url(<?php echo esc_url( $bknd_image['url'] ); ?>)">
								<div class="video-wrapper">
				                	<video autoplay loop id="bgvid">
				                        <source src="<?php echo esc_url($soho_slide_video_bknd_webm); ?>" />
										<source src="<?php echo esc_url($soho_slide_video_bknd_mp4); ?>" />
				                    </video>
			                	</div>
                				<div class="overlay" style="background-color:<?php echo esc_attr( $soho_overlay_rgba ); ?>">
	                    			<!-- Slide Caption -->
	                    			<div class="clapat-caption <?php echo sanitize_html_class( $soho_animated_class ); ?>">
	                        			<div class="caption-content <?php echo sanitize_html_class( $soho_caption_alignment ); ?>">                                        
	                            			<?php the_content(); ?>
	                        			</div>
	                        			<?php if( $soho_display_scroll_arrow ) { ?>
				                        <div class="icon-scroll"></div>
				                    	<div class="scrolltotop"><span class="holder"></span></div>
				                        <div class="icon-scroll-line"></div>
				                        <?php } ?>
	                    			</div>
	                    			<!--/Slide Caption -->
                				</div>
            				</div>
            				<!--/Video Slide -->
							
							<?php } else { ?>
							
							<!-- Image Slide -->
							<div class="section <?php echo sanitize_html_class( $soho_class_content_type ); ?>" style="background-image:url(<?php echo esc_url( $soho_image_path ); ?>)">
                				<div class="overlay" style="background-color:<?php echo esc_attr( $soho_overlay_rgba ); ?>">
	                    			<!-- Slide Caption -->
	                    			<div class="clapat-caption <?php echo sanitize_html_class( $soho_animated_class ); ?>">
	                        			<div class="caption-content <?php echo sanitize_html_class( $soho_caption_alignment ); ?>">                                        
	                            			<?php the_content(); ?>
	                        			</div>
	                        			<?php if( $soho_display_scroll_arrow ) { ?>
				                        <div class="icon-scroll"></div>
				                    	<div class="scrolltotop"><span class="holder"></span></div>
				                        <div class="icon-scroll-line"></div>
				                        <?php } ?>
	                    			</div>
	                    			<!--/Slide Caption -->
                				</div>
            				</div>
            				<!--/Image Slide -->
														
							<?php } ?>

						<?php

							} // while posts
							
							wp_localize_script( 'soho_scriptsjs', 'ClapatSohoShowcaseSlides', $arr_slides_titles );
							
						}
						else{

							esc_html_e('There are no showcase slides defined. You can create them in admin dashboard under Showcase Slider.', 'soho');
						}

						wp_reset_postdata();

						?>
			</div>
			<!--/Showcase Slider -->
<?php
}

get_footer();

?>