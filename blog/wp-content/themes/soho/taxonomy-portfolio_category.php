<?php

// archive template for portfolio categories

get_header();

$soho_columns 			= 3;
$soho_show_filters		= true;
$soho_hover_effect		= 'revealed';
$soho_margins			= '';
$soho_use_lightbox		= false;
$soho_filters_class 	= '';


?>

		<?php 
		
		// display header section
		get_template_part('sections/header_section'); 		
		
		?>
		
		
		<!-- Page Content -->
		<div id="page-content">
		
			<!-- Page Title -->
			<div class="page-title text-align-center">                
				<h1><?php single_cat_title(); ?></h1>
			</div>
			<!-- Page Title -->
		
			<!-- Main --> 
			<div id="main">
				<div id="main-content" class="hidden">
					<!-- Container -->
					<div class="portfolio">
						<!-- Portfolio Filters --> 
						<ul id="filters" class="<?php echo sanitize_html_class( $soho_filters_class ); ?>">
							<li class="filter-line"></li>
							<li><a id="all" href="#" data-filter="*" class="active"><?php esc_html_e('All', 'soho'); ?></a></li>	
							<?php
							$soho_portfolio_category = get_terms('portfolio_category', array( 'hide_empty' => 0 ));

							if($soho_portfolio_category){

								foreach($soho_portfolio_category as $portfolio_cat){
							?>
							<li><a href="#" data-filter=".<?php echo sanitize_title( $portfolio_cat->slug ); ?>"><?php echo wp_kses_post( $portfolio_cat->name ); ?></a></li>
							<?php
								}
							}
							?>
						</ul>
						<!--/Portfolio Filters -->
						<!-- Portfolio -->
						<div id="portfolio-wrap" class="<?php echo esc_attr( $soho_margins ); ?>">
							<div id="portfolio" <?php if( $soho_use_lightbox ){ echo 'class="mfp-gallery"'; } ?> data-col="<?php echo esc_attr( $soho_columns ); ?>">
							<?php
							
								while( have_posts() ){
								
									the_post();
								
									if( $soho_use_lightbox ){
										
										get_template_part('sections/portfolio_section_lightbox_item');
									}
									else {
									
										get_template_part('sections/portfolio_section_item');
									}
									
								}
							
							?>
							</div>
						</div>
						<!--/Portfolio -->
					</div> 
					<!--/Container -->
				</div>
			</div>
			<!--/Main -->					
		
<?php

get_footer();

?>