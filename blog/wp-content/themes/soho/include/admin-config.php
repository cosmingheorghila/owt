<?php
/**
 * Soho Theme Config File
 */

if ( ! class_exists( 'soho_options_config' ) ) {

    class soho_options_config {

        public $args = array();
        public $sections = array();
        public $theme;
        public $ReduxFramework;

        public function __construct() {

            if ( ! class_exists( 'ReduxFramework' ) ) {
                return;
            }

            // This is needed. Bah WordPress bugs.  ;)
            if ( true == Redux_Helpers::isTheme( __FILE__ ) ) {
                $this->initSettings();
            } else {
                add_action( 'plugins_loaded', array( $this, 'initSettings' ), 10 );
            }

        }

        public function initSettings() {

            // Just for demo purposes. Not needed per say.
            $this->theme = wp_get_theme();

            // Set the default arguments
            $this->setArguments();

            // Set a few help tabs so you can see how it's done
            $this->setHelpTabs();

            // Create the sections and fields
            $this->setSections();

            if ( ! isset( $this->args['opt_name'] ) ) { // No errors please
                return;
            }

            // If Redux is running as a plugin, this will remove the demo notice and links
            //add_action( 'redux/loaded', array( $this, 'remove_demo' ) );

            // Function to test the compiler hook and demo CSS output.
            // Above 10 is a priority, but 2 in necessary to include the dynamically generated CSS to be sent to the function.
            //add_filter('redux/options/'.$this->args['opt_name'].'/compiler', array( $this, 'compiler_action' ), 10, 3);

            // Change the arguments after they've been declared, but before the panel is created
            //add_filter('redux/options/'.$this->args['opt_name'].'/args', array( $this, 'change_arguments' ) );

            // Change the default value of a field after it's been set, but before it's been useds
            //add_filter('redux/options/'.$this->args['opt_name'].'/defaults', array( $this,'change_defaults' ) );

            // Dynamically add a section. Can be also used to modify sections/fields
            //add_filter('redux/options/' . $this->args['opt_name'] . '/sections', array($this, 'dynamic_section'));

            $this->ReduxFramework = new ReduxFramework( $this->sections, $this->args );
        }

        /**
         * This is a test function that will let you see when the compiler hook occurs.
         * It only runs if a field    set with compiler=>true is changed.
         * */
        function compiler_action( $options, $css, $changed_values ) {
            echo '<h1>The compiler hook has run!</h1>';
            echo "<pre>";
            print_r( $changed_values ); // Values that have changed since the last save
            echo "</pre>";
           
        }

        /**
         * Custom function for filtering the sections array. Good for child themes to override or add to the sections.
         * Simply include this function in the child themes functions.php file.
         * NOTE: the defined constants for URLs, and directories will NOT be available at this point in a child theme,
         * so you must use get_template_directory_uri() if you want to use any of the built in icons
         * */
        function dynamic_section( $sections ) {
            //$sections = array();
            $sections[] = array(
                'title'  => esc_html__( 'Section via hook', 'soho'),
                'desc'   => esc_html__( '<p class="description">This is a section created by adding a filter to the sections array. Can be used by child themes to add/remove sections from the options.</p>', 'soho'),
                'icon'   => 'el-icon-paper-clip',
                // Leave this as a blank section, no options just some intro text set above.
                'fields' => array()
            );

            return $sections;
        }

        /**
         * Filter hook for filtering the args. Good for child themes to override or add to the args array. Can also be used in other functions.
         * */
        function change_arguments( $args ) {
            //$args['dev_mode'] = true;

            return $args;
        }

        /**
         * Filter hook for filtering the default value of any given field. Very useful in development mode.
         * */
        function change_defaults( $defaults ) {
            $defaults['str_replace'] = 'Testing filter hook!';

            return $defaults;
        }

        // Remove the demo link and the notice of integrated demo from the redux-framework plugin
        function remove_demo() {

            // Used to hide the demo mode link from the plugin page. Only used when Redux is a plugin.
            if ( class_exists( 'ReduxFrameworkPlugin' ) ) {
                remove_filter( 'plugin_row_meta', array(
                    ReduxFrameworkPlugin::instance(),
                    'plugin_metalinks'
                ), null, 2 );

                // Used to hide the activation notice informing users of the demo panel. Only used when Redux is a plugin.
                remove_action( 'admin_notices', array( ReduxFrameworkPlugin::instance(), 'admin_notices' ) );
            }
        }

        public function setSections() {

            /**
             * Used within different fields. Simply examples. Search for ACTUAL DECLARATION for field examples
             * */
            // Background Patterns Reader
            $sample_patterns_path = ReduxFramework::$_dir . '../sample/patterns/';
            $sample_patterns_url  = ReduxFramework::$_url . '../sample/patterns/';
            $sample_patterns      = array();

            if ( is_dir( $sample_patterns_path ) ) :

                if ( $sample_patterns_dir = opendir( $sample_patterns_path ) ) :
                    $sample_patterns = array();

                    while ( ( $sample_patterns_file = readdir( $sample_patterns_dir ) ) !== false ) {

                        if ( stristr( $sample_patterns_file, '.png' ) !== false || stristr( $sample_patterns_file, '.jpg' ) !== false ) {
                            $name              = explode( '.', $sample_patterns_file );
                            $name              = str_replace( '.' . end( $name ), '', $sample_patterns_file );
                            $sample_patterns[] = array(
                                'alt' => $name,
                                'img' => $sample_patterns_url . $sample_patterns_file
                            );
                        }
                    }
                endif;
            endif;

            ob_start();

            $ct          = wp_get_theme();
            $this->theme = $ct;
            $item_name   = $this->theme->get( 'Name' );
            $tags        = $this->theme->Tags;
            $screenshot  = $this->theme->get_screenshot();
            $class       = $screenshot ? 'has-screenshot' : '';

            $customize_title = sprintf( esc_html__( 'Customize &#8220;%s&#8221;', 'soho'), $this->theme->display( 'Name' ) );

            ?>
            <div id="current-theme" class="<?php echo esc_attr( $class ); ?>">
                <?php if ( $screenshot ) : ?>
                    <?php if ( current_user_can( 'edit_theme_options' ) ) : ?>
                        <a href="<?php echo wp_customize_url(); ?>" class="load-customize hide-if-no-customize"
                           title="<?php echo esc_attr( $customize_title ); ?>">
                            <img src="<?php echo esc_url( $screenshot ); ?>"
                                 alt="<?php esc_attr_e( 'Current theme preview', 'soho'); ?>"/>
                        </a>
                    <?php endif; ?>
                    <img class="hide-if-customize" src="<?php echo esc_url( $screenshot ); ?>"
                         alt="<?php esc_attr_e( 'Current theme preview', 'soho'); ?>"/>
                <?php endif; ?>

                <h4><?php echo $this->theme->display( 'Name' ); ?></h4>

                <div>
                    <ul class="theme-info">
                        <li><?php printf( esc_html__( 'By %s', 'soho'), $this->theme->display( 'Author' ) ); ?></li>
                        <li><?php printf( esc_html__( 'Version %s', 'soho'), $this->theme->display( 'Version' ) ); ?></li>
                        <li><?php echo '<strong>' . esc_html__( 'Tags', 'soho') . ':</strong> '; ?><?php printf( $this->theme->display( 'Tags' ) ); ?></li>
                    </ul>
                    <p class="theme-description"><?php echo $this->theme->display( 'Description' ); ?></p>
                    <?php
                    if ( $this->theme->parent() ) {
                        printf( ' <p class="howto">' . esc_html__( 'This <a href="%1$s">child theme</a> requires its parent theme, %2$s.', 'soho') . '</p>', esc_html__( 'http://codex.wordpress.org/Child_Themes', 'soho'), $this->theme->parent()->display( 'Name' ) );
                    }
                    ?>

                </div>
            </div>

            <?php
            $item_info = ob_get_contents();

            ob_end_clean();

            // ACTUAL DECLARATION OF SECTIONS
            $this->sections[] = array(
                'title'     => esc_html__('General Settings', 'soho'),
                'desc'      => esc_html__('General theme settings.', 'soho'),
                'icon'      => 'el-icon-wrench',
                'fields'    => array(

					array(
                        'id'        => 'clapat_soho_enable_preloader',
                        'type'      => 'switch',
                        'title'     => esc_html__('Enable Page Preloader', 'soho'),
                        'subtitle'  => esc_html__('Enable preloader mask while the page is loading.', 'soho'),
                        'default'   => false,
						'on'        => 'Yes',
                        'off'       => 'No',
                    ),
					
					array(
                        'id'        => 'clapat_soho_enable_preloader_img',
                        'type'      => 'media',
                        'url'       => true,
                        'title'     => esc_html__('Preloader Image', 'soho'),
                        'desc'      => esc_html__('Leave empty for default loading icon.', 'soho'),
                        'subtitle'  => esc_html__('Upload your preloader image displayed when the page is loading.', 'soho'),
						'required'	=> array('clapat_soho_enable_preloader', '=', true),
                        'default'   => array(),
                    ),
										
					array(
                        'id'            => 'clapat_soho_space_head',
                        'type'          => 'textarea',
                        'title'         => wp_kses_post( __('Space before &lt;/head&gt;', 'soho') ),
                        'subtitle'      => wp_kses_post( __('Add code which is inserted before the &lt;/head&gt; tag.', 'soho') ),
                        'default'       => '',
                    ),

                ),
            );

            $this->sections[] = array(
                'title'     => esc_html__('Header Options', 'soho'),
                'desc'      => esc_html__('Header Options. Select the logo displayed in the header and settings related with the menu.', 'soho'),
                'icon'      => 'el-icon-lines',
                'fields'    => array(

                    array(
                        'id'        => 'clapat_soho_logo_light',
                        'type'      => 'media',
                        'url'       => true,
                        'title'     => esc_html__('Header Logo - Light', 'soho'),
                        'desc'      => '',
                        'subtitle'  => esc_html__('Upload your logo to be displayed in the header menu for dark (negative) backgrounds.', 'soho'),
                        'default'   => array('url' => get_template_directory_uri() . '/images/logo.png'),
                    ),
					
					array(
                        'id'        => 'clapat_soho_logo_dark',
                        'type'      => 'media',
                        'url'       => true,
                        'title'     => esc_html__('Header Logo - Dark', 'soho'),
                        'desc'      => '',
                        'subtitle'  => esc_html__('Upload your logo to be displayed in the header menu for light (positive) backgrounds.', 'soho'),
                        'default'   => array('url' => get_template_directory_uri() . '/images/logo-black.png'),
                    ),
					
                ),

            );

			global $soho_social_links;
			$footer_fields = array();
			$social_network_ids = array_keys( $soho_social_links );
			
			$footer_fields[] = array(
											'id'            => 'clapat_soho_footer_copyright',
											'type'          => 'editor',
											'title'         => esc_html__('Copyright text', 'soho'),
											'subtitle'      => esc_html__('This is the copyright text.', 'soho'),
											'default'       => '2016 &copy; Soho Template. All rights reserved.',
										);
										
			$footer_fields[] = array(
											'id'        => 'clapat_soho_footer_layout',
											'type'      => 'switch',
											'title'     => esc_html__('Footer Layout', 'soho'),
											'subtitle'  => esc_html__('Select the layout for the footer: normal or alternate.', 'soho'),
											'on'		=> 'Normal',
											'off'		=> 'Alternate',
											'default'   => true,
										);
										
			for( $idx = 1; $idx <= SOHO_MAX_SOCIAL_LINKS; $idx++ ){
			
				$footer_fields[] =	array(
											'id'        => 'clapat_soho_footer_social_' . $idx,
											'type'      => 'select',
											'title'     => esc_html__('Social Network Name ' . $idx, 'soho' ),
											'options'   => $soho_social_links,
											'default'   => 'facebook'
										);
				$footer_fields[] = array(
											'id'        => 'clapat_soho_footer_social_url_' . $idx,
											'type'      => 'text',
											'title'     => esc_html__('Social Link URL ' . $idx, 'soho' ),
											'default'   => '',
										);
			}
			
            $this->sections[] = array(
                'title'     => esc_html__('Footer Options', 'soho'),
                'desc'      => esc_html__('Settings concerning the footer such as social links and copyright text.', 'soho'),
                'icon'      => 'el-icon-check-empty',
                'fields'    => $footer_fields
            );
			
			$this->sections[] = array(
                'title'     => esc_html__('Main Slider Options', 'soho'),
                'desc'      => esc_html__('Settings concerning the Main Slider.', 'soho'),
                'icon'      => 'el-icon-tasks',
                'fields'    => array(

					array(
                        'id'        => 'clapat_soho_slider_direction',
                        'type'      => 'radio',
                        'title'     => esc_html__('Slider Direction', 'soho'),

                        //Must provide key => value pairs for radio options
                        'options'   => array(
                            'horizontal' => esc_html__('Horizontal', 'soho'),
                            'vertical' => esc_html__('Vertical', 'soho')
                        ),
                        'default'   => 'horizontal'
                    ),
					
                    array(
                        'id'        => 'clapat_soho_slider_loop_top',
                        'type'      => 'switch',
                        'title'     => esc_html__('Loop Top', 'soho'),
						'desc'      => esc_html__('Defines whether scrolling up in the first section should scroll to the last one or not.', 'soho'),
                        'default'   => false,
                    ),
					
					array(
                        'id'        => 'clapat_soho_slider_loop_bottom',
                        'type'      => 'switch',
                        'title'     => esc_html__('Loop Bottom', 'soho'),
						'desc'      => esc_html__('Defines whether scrolling down in the last section should scroll to the first one or not.', 'soho'),
                        'default'   => false,
                    ),
					
                    array(
                        'id'            => 'clapat_soho_slider_speed',
                        'type'          => 'slider',
                        'title'         => esc_html__('Scrolling Speed', 'soho'),
                        'subtitle'      => esc_html__('Enter a value between 100 and 1000.', 'soho'),
                        'desc'          => esc_html__('Speed in milliseconds for the scrolling transitions.', 'soho'),
                        'default'       => 700,
                        'min'           => 100,
                        'step'          => 100,
                        'max'           => 1000,
                        'display_value' => 'text'
                    )
				),
			);
			
						
			$this->sections[] = array(
                'title'     => esc_html__('Portfolio Options', 'soho'),
                'desc'      => esc_html__('Settings concerning the portfolio section or portfolio pages.', 'soho'),
                'icon'      => 'el-icon-folder-open',
                'fields'    => array(

                    array(
                        'id'        => 'clapat_soho_portfolio_custom_slug',
                        'type'      => 'text',
                        'title'     => esc_html__('Custom Slug', 'soho'),
                        'desc'      => esc_html__('If you want your portfolio post type to have a custom slug in the url, please enter it here. You will still have to refresh your permalinks after saving this! This is done by going to Settings > Permalinks and clicking save.', 'soho'),
                        'default'   => '',
                    ),
					
					array(
                        'id'        => 'clapat_soho_portfolio_return_url',
                        'type'      => 'text',
                        'title'     => esc_html__('Return URL', 'soho'),
                        'subtitle'  => esc_html__('The URL to return from the individual project pages.', 'soho'),
                        'default'   => esc_url( get_home_url() ),
                    ),
					
					array(
                        'id'        => 'clapat_soho_portfolio_next_caption',
                        'type'      => 'text',
                        'title'     => esc_html__('Next Project Caption', 'soho'),
                        'subtitle'  => esc_html__('Caption of the next project navigation.', 'soho'),
                        'default'   => esc_html__('Next Project', 'soho'),
                    ),
					
					array(
                        'id'        => 'clapat_soho_portfolio_prev_caption',
                        'type'      => 'text',
                        'title'     => esc_html__('Prev Project Caption', 'soho'),
                        'subtitle'  => esc_html__('Caption of the previous project navigation.', 'soho'),
                        'default'   => esc_html__('Prev Project', 'soho'),
                    ),
										
					array(
                        'id'        => 'clapat_soho_portfolio_social_sharing',
                        'type'      => 'text',
                        'title'     => esc_html__('Social Sharing Shortcode', 'soho'),
                        'subtitle'  => esc_html__('Enter here the shortcode of the social sharing plugin to enable it in the project pages. Leave empty for none.', 'soho'),
                        'default'   => '',
                    ),
					
                    array(
                        'id'        => 'clapat_soho_portfolio_menu_filters_caption_open',
                        'type'      => 'text',
                        'title'     => esc_html__('Filters Menu Caption - Open', 'soho'),
                        'subtitle'  => esc_html__('Enter here the menu caption to show the filters in portfolio pages.', 'soho'),
                        'default'   => esc_html__('Filters', 'soho')
                    ),
                    
                    array(
                        'id'        => 'clapat_soho_portfolio_menu_filters_caption_close',
                        'type'      => 'text',
                        'title'     => esc_html__('Filters Menu Caption - Close', 'soho'),
                        'subtitle'  => esc_html__('Enter here the menu caption to hide/close the filters in portfolio pages.', 'soho'),
                        'default'   => esc_html__('Close', 'soho')
                    ),
                    
                    array(
                        'id'        => 'clapat_soho_portfolio_menu_share_caption_open',
                        'type'      => 'text',
                        'title'     => esc_html__('Share Project Menu Caption - Open', 'soho'),
                        'subtitle'  => esc_html__('Enter here the menu caption to share project pages.', 'soho'),
                        'default'   => esc_html__('Share', 'soho')
                    ),
                    
                    array(
                        'id'        => 'clapat_soho_portfolio_menu_share_caption_close',
                        'type'      => 'text',
                        'title'     => esc_html__('Share Project Menu Caption - Close', 'soho'),
                        'subtitle'  => esc_html__('Enter here the menu caption to close sharing section in project pages.', 'soho'),
                        'default'   => esc_html__('Close', 'soho')
                    ),
                    
					array(
                        'id'        => 'clapat_soho_portfolio_social_sharing_caption',
                        'type'      => 'text',
                        'title'     => esc_html__('Social Sharing Caption', 'soho'),
                        'subtitle'  => esc_html__('Enter here the caption of the fullscreen overlay social sharing section in project pages.', 'soho'),
                        'default'   => esc_html__('Share this project', 'soho')
                    ),
                    
                    array(
                        'id'        => 'clapat_soho_portfolio_social_sharing_thanks',
                        'type'      => 'text',
                        'title'     => esc_html__('Thank You Note', 'soho'),
                        'subtitle'  => esc_html__('Displayed under fullscreen overlay social sharing section.', 'soho'),
                        'default'   => esc_html__('Thank You', 'soho')
                    ),
                ),
            );

            $this->sections[] = array(
                'title'     => esc_html__('Blog Options', 'soho'),
                'desc'      => esc_html__('Settings concerning the blog section or blog pages.', 'soho'),
                'icon'      => 'el-icon-bold',
                'fields'    => array(

                    array(
                        'id'        => 'clapat_soho_blog_read_more_caption',
                        'type'      => 'text',
                        'title'     => esc_html__('Read More Button Caption', 'soho'),
                        'subtitle'  => esc_html__('The caption of the button displayed underneath each blog post in blog and archive pages', 'soho'),
                        'default'   => esc_html__('Read More', 'soho')
                    ),
					
					array(
                        'id'        => 'clapat_soho_blog_show_search',
                        'type'      => 'switch',
                        'title'     => esc_html__('Show Search Form In Blog Page Templates', 'soho'),
                        'subtitle'  => esc_html__('Shows the search form in blog template pages allowing you to search through blog posts.', 'soho'),
                        'on'        => 'Yes',
                        'off'       => 'No',
                        'default'   => true,
                    ),

                    array(
                        'id'        => 'clapat_soho_blog_search_caption_open',
                        'type'      => 'text',
                        'title'     => esc_html__('Search Menu Caption - Open', 'soho'),
                        'desc'      => esc_html__('Caption of the secondary menu item opening the search posts section.', 'soho'),
                        'default'   => esc_html__('Search', 'soho'),
                    	'required'	=> array('clapat_soho_blog_show_search', '=', true)
                    ),
					
					array(
                        'id'        => 'clapat_soho_blog_search_caption_close',
                        'type'      => 'text',
                        'title'     => esc_html__('Search Menu Caption - Close', 'soho'),
                        'desc'      => esc_html__('Caption of the secondary menu item sclosing the search posts section.', 'soho'),
                        'default'   => esc_html__('Close', 'soho'),
                    	'required'	=> array('clapat_soho_blog_show_search', '=', true)
                    ),
                    
					array(
                        'id'        => 'clapat_soho_blog_next_posts_caption',
                        'type'      => 'text',
                        'title'     => esc_html__('Next Posts Caption', 'soho'),
                        'desc'      => esc_html__('Caption linking to the next blog posts page.', 'soho'),
                        'default'   => esc_html__('Older Posts', 'soho'),
                    ),
					
					array(
                        'id'        => 'clapat_soho_blog_prev_posts_caption',
                        'type'      => 'text',
                        'title'     => esc_html__('Prev Posts Caption', 'soho'),
                        'desc'      => esc_html__('Caption linking to the prev blog posts page.', 'soho'),
                        'default'   => esc_html__('Newer Posts', 'soho'),
                    ),
															
                    array(
                        'id'        => 'clapat_soho_blog_next_post_caption',
                        'type'      => 'text',
                        'title'     => esc_html__('Next Post Caption', 'soho'),
                        'desc'      => esc_html__('Caption of the button linking to the next single blog post page.', 'soho'),
                        'default'   => 'Next Post',
                    ),
										
					array(
                        'id'        => 'clapat_soho_blog_social_sharing',
                        'type'      => 'text',
                        'title'     => esc_html__('Social Sharing Shortcode', 'soho'),
                        'subtitle'  => esc_html__('Enter here the shortcode of the social sharing plugin to enable it in the project pages. Leave empty for none.', 'soho'),
                        'default'   => '',
                    ),
					
					array(
                        'id'        => 'clapat_soho_blog_social_sharing_caption',
                        'type'      => 'text',
                        'title'     => esc_html__('Social Sharing Caption', 'soho'),
                        'subtitle'  => esc_html__('Enter here the caption of the social sharing section in single blog post pages.', 'soho'),
                        'default'   => esc_html__('Share this story', 'soho')
                    ),
					
					array(
                        'id'        => 'clapat_soho_blog_default_title',
                        'type'      => 'text',
                        'title'     => esc_html__('Default Posts Page Title', 'soho'),
                        'desc'      => esc_html__('Title of the default blog posts page. The default blog posts page is the page displaying blog posts when there is no front page set in Settings -> Reading.', 'soho'),
                        'default'   => 'Blog',
                    ),
                )
            );

			$this->sections[] = array(
                'title'     => esc_html__('Styling Options', 'soho'),
                'desc'      => esc_html__('Settings concerning colors and custom CSS.', 'soho'),
                'icon'      => 'el-icon-brush',
                'fields'    => array(

					array(
                        'id'        => 'clapat_soho_styling_custom_css',
                        'type'      => 'textarea',
                        'title'     => esc_html__('Custom CSS', 'soho'),
                        'subtitle'  => esc_html__('Add here your custom CSS code.', 'soho'),
                        'validate'  => 'css',
                    ),
				),
			);
				
			$this->sections[] = array(
                'title'     => esc_html__('Typography', 'soho'),
                'desc'      => esc_html__('Font options to render the typography style of your website.', 'soho'),
                'icon'      => 'el-icon-fontsize',
                'fields'    => array(

                    array(
                        'id'        => 'clapat_soho_body_font',
                        'type'      => 'typography',
                        'output'    => array('html, body, .bigger, nav > ul > li > a, input[type="text"], input[type="password"], textarea'),
                        'title'     => esc_html__('Body Font Options', 'soho'),
                        'subtitle'  => esc_html__('Select font options for the body', 'soho'),
                        'google'    => true,
                        'text-align'=> false,
                        'subsets'   => true,
                        'font-weight' => true,
						'color' => false,
                        'default'   => array(
                            'google'      => true,
                            'font-size'     => '15px',
                            'font-family'   => 'Open Sans, sans-serif',
                            'line-height'   => '25px',
                            'font-weight'   => '400',
                        ),
                    ),

                    array(
                        'id'        => 'clapat_soho_heading_font',
                        'type'      => 'typography',
                        'output'    => array('h1, h2, h3, h4, h5, h6, .smaller, .pp-tooltip, #menu-overlay nav > ul > li > a, .showcase-name .sh-name, .sh-view, .slider-projects-nav li span, #filters li a, .item-cat, .item-title, .revealed .item-cat, .carousel-caption .item-cat, .revealed .item-title, .carousel-caption .item-title, .post-title, .meta-categories li a, .entry-meta li, .nav-button p, .reply, input#search, input[type="submit"], .wp-caption-text, .animated-module span, .clapat-button, .clapat-button-full, .more, .clapat-counter .number, .clapat-counter .subject, .p-table-title, .p-table-num, .p-table-num sup, .radial-counter input'),
                        'title'     => esc_html__('Headings Font Options', 'soho'),
                        'subtitle'  => esc_html__('Select font options for headings.', 'soho'),
                        'google'    => true,
                        'text-align'=> false,
                        'subsets'   => false,
						'font-size'	=> false,
						'line-height' => false,
                        'font-weight' => true,
						'font-style' => false,
						'all_styles' => true,
						'color' => false,
						'default'   => array(
							'google'      => true,
                            'font-family' => 'Montserrat, sans-serif',
							'font-weight' => '700',
                        ),
                    ),
					
				)
			);
			
            $this->sections[] = array(
                'title'     => esc_html__('Map Options', 'soho'),
                'desc'      => esc_html__('Settings concerning the map section.', 'soho'),
                'icon'      => 'el-icon-map-marker',
                'fields'    => array(

					array(
                        'id'        => 'clapat_soho_map_marker',
                        'type'      => 'media',
                        'url'       => true,
                        'title'     => esc_html__('Map Marker', 'soho'),
                        'subtitle'  => esc_html__('Please choose an image file for the marker.', 'soho'),
                        'default'   => array('url' => get_template_directory_uri() . '/images/marker.png'),
                    ),
					array(
                        'id'        => 'clapat_soho_map_address',
                        'type'      => 'text',
                        'title'     => esc_html__('Google Map Address', 'soho'),
                        'subtitle'  => esc_html__('Example: 775 New York Ave, Brooklyn, Kings, New York 11203. Or you can enter latitude and longitude for greater precision. Example: 41.40338, 2.17403 (in decimal degrees - DDD)', 'soho'),
                        'default'   => '775 New York Ave, Brooklyn, Kings, New York 11203',
                    ),
					array(
                        'id'        => 'clapat_soho_map_zoom',
                        'type'      => 'text',
                        'title'     => esc_html__('Map Zoom Level', 'soho'),
                        'subtitle'  => esc_html__('Higher number will be more zoomed in.', 'soho'),
                        'default'   => '16',
                    ),
                    array(
                        'id'        => 'clapat_soho_map_company_name',
                        'type'      => 'text',
                        'title'     => esc_html__('Pop-up marker title', 'soho'),
                        'default'   => 'soho',
                    ),
                    array(
                        'id'        => 'clapat_soho_map_company_info',
                        'type'      => 'text',
                        'title'     => esc_html__('Pop-up marker text', 'soho'),
                        'default'   => 'Here we are. Come to drink a coffee!',
                    ),
                    array(
                        'id'        => 'clapat_soho_map_type',
                        'type'      => 'switch',
                        'title'     => esc_html__('Map type', 'soho'),
                        'subtitle'  => esc_html__('Set the map type as road map or satellite.', 'soho'),
                        'on'        => 'Road',
                        'off'       => 'Satellite',
                        'default'   => true,
                    ),                    
                )
            );

            $this->sections[] = array(
                'title'     => esc_html__('Error Page Options', 'soho'),
                'desc'      => esc_html__('Settings concerning the "error not found page (404)" section.', 'soho'),
                'icon'      => 'el-icon-error-alt',
                'fields'    => array(

                    array(
                        'id'        => 'clapat_soho_error_title',
                        'type'      => 'text',
                        'title'     => esc_html__('Error Page Title', 'soho'),
                        'default'   => '404',
                    ),
					array(
                        'id'        => 'clapat_soho_error_info',
                        'type'      => 'textarea',
                        'title'     => esc_html__('Error Page Info text', 'soho'),
                        'validate'  => 'html',
                        'default'   => 'The page you are looking for could not be found.',
                    ),
                    array(
                        'id'        => 'clapat_soho_error_back_button',
                        'type'      => 'text',
                        'title'     => esc_html__('Back Button Caption', 'soho'),
                        'default'   => 'Home Page',
                    ),
					
					array(
                        'id'        => 'clapat_soho_error_back_button_url',
                        'type'      => 'text',
                        'title'     => esc_html__('Back Button URL', 'soho'),
                        'default'   => esc_url( get_home_url() ),
                    ),

                )
            );

            $this->sections[] = array(
                'title'     => esc_html__('Import / Export', 'soho'),
                'desc'      => esc_html__('Import and Export your settings from file, text or URL.', 'soho'),
                'icon'      => 'el-icon-refresh',
                'fields'    => array(
                    array(
                        'id'            => 'opt-import-export',
                        'type'          => 'import_export',
                        'title'         => 'Import Export',
                        'subtitle'      => 'Save and restore your Redux options',
                        'full_width'    => false,
                    ),
                ),
            );

        }

        public function setHelpTabs() {

            // Custom page help tabs, displayed using the help API. Tabs are shown in order of definition.
            $this->args['help_tabs'][] = array(
                'id'      => 'redux-help-tab-1',
                'title'   => esc_html__( 'Theme Information 1', 'soho'),
                'content' => esc_html__( '<p>This is the tab content, HTML is allowed.</p>', 'soho')
            );

            $this->args['help_tabs'][] = array(
                'id'      => 'redux-help-tab-2',
                'title'   => esc_html__( 'Theme Information 2', 'soho'),
                'content' => esc_html__( '<p>This is the tab content, HTML is allowed.</p>', 'soho')
            );

            // Set the help sidebar
            $this->args['help_sidebar'] = esc_html__( '<p>This is the sidebar content, HTML is allowed.</p>', 'soho');
        }

        /**
         * All the possible arguments for Redux.
         * For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
         * */
        public function setArguments() {

            $theme = wp_get_theme(); // For use with some settings. Not necessary.

            $this->args = array(
                // TYPICAL -> Change these values as you need/desire
                'opt_name'             => SOHO_THEME_OPTIONS,
                // This is where your data is stored in the database and also becomes your global variable name.
                'display_name'         => $theme->get( 'Name' ),
                // Name that appears at the top of your panel
                'display_version'      => $theme->get( 'Version' ),
                // Version that appears at the top of your panel
                'menu_type'            => 'submenu',
                //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
                'allow_sub_menu'       => true,
                // Show the sections below the admin menu item or not
                'menu_title'        => esc_html__('Theme Options', 'soho'),
                'page_title'        => $theme->get('Name') . esc_html__(' Theme Options', 'soho'),
                // You will need to generate a Google API key to use this feature.
                // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
                'google_api_key'       => 'AIzaSyCo4U6ficbcj__7_LC1YpFDxtmTJ-pzoQ0',
                // Set it you want google fonts to update weekly. A google_api_key value is required.
                'google_update_weekly' => false,
                // Must be defined to add google fonts to the typography module
                'async_typography'     => false,
                // Use a asynchronous font on the front end or font string
                //'disable_google_fonts_link' => true,                    // Disable this in case you want to create your own google fonts loader
                'admin_bar'            => true,
                // Show the panel pages on the admin bar
                'admin_bar_icon'     => 'dashicons-portfolio',
                // Choose an icon for the admin bar menu
                'admin_bar_priority' => 50,
                // Choose an priority for the admin bar menu
                'global_variable'      => 'soho_theme_options',
                // Set a different name for your global variable other than the opt_name
                'dev_mode'             => false,
                // Show the time the page took to load, etc
                'update_notice'        => true,
                // If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo
                'customizer'           => true,
                // Enable basic customizer support
                //'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
                //'disable_save_warn' => true,                    // Disable the save warning when a user changes a field

                // OPTIONAL -> Give you extra features
                'page_priority'        => null,
                // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
                'page_parent'          => 'themes.php',
                // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
                'page_permissions'     => 'manage_options',
                // Permissions needed to access the options panel.
                'menu_icon'            => '',
                // Specify a custom URL to an icon
                'last_tab'             => '',
                // Force your panel to always open to a specific tab (by id)
                'page_icon'            => 'icon-themes',
                // Icon displayed in the admin panel next to your menu_title
                'page_slug'            => '_options',
                // Page slug used to denote the panel
                'save_defaults'        => true,
                // On load save the defaults to DB before user clicks save or not
                'default_show'         => false,
                // If true, shows the default value next to each field that is not the default value.
                'default_mark'         => '',
                // What to print by the field's title if the value shown is default. Suggested: *
                'show_import_export'   => true,
                // Shows the Import/Export panel when not used as a field.

                // CAREFUL -> These options are for advanced use only
                'transient_time'       => 60 * MINUTE_IN_SECONDS,
                'output'               => true,
                // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
                'output_tag'           => true,
                // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
                // 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.

                // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
                'database'             => '',
                // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
                'system_info'          => false,
                // REMOVE

                // HINTS
                'hints'                => array(
                    'icon'          => 'icon-question-sign',
                    'icon_position' => 'right',
                    'icon_color'    => 'lightgray',
                    'icon_size'     => 'normal',
                    'tip_style'     => array(
                        'color'   => 'light',
                        'shadow'  => true,
                        'rounded' => false,
                        'style'   => '',
                    ),
                    'tip_position'  => array(
                        'my' => 'top left',
                        'at' => 'bottom right',
                    ),
                    'tip_effect'    => array(
                        'show' => array(
                            'effect'   => 'slide',
                            'duration' => '500',
                            'event'    => 'mouseover',
                        ),
                        'hide' => array(
                            'effect'   => 'slide',
                            'duration' => '500',
                            'event'    => 'click mouseleave',
                        ),
                    ),
                )
            );

            $this->args['footer_credit'] = $theme->get( 'Name' ) . ' ' . $theme->get( 'Version' ) . ' by Clapat';

            // ADMIN BAR LINKS -> Setup custom links in the admin bar menu as external items.
            /*$this->args['admin_bar_links'][] = array(
                'id'    => 'redux-docs',
                'href'   => 'http://docs.reduxframework.com/',
                'title' => esc_html__( 'Documentation', 'soho'),
            );

            $this->args['admin_bar_links'][] = array(
                //'id'    => 'redux-support',
                'href'   => 'https://github.com/ReduxFramework/redux-framework/issues',
                'title' => esc_html__( 'Support', 'soho'),
            );

            $this->args['admin_bar_links'][] = array(
                'id'    => 'redux-extensions',
                'href'   => 'reduxframework.com/extensions',
                'title' => esc_html__( 'Extensions', 'soho'),
            );*/

            // SOCIAL ICONS -> Setup custom links in the footer for quick links in your panel footer icons.
            /*$this->args['share_icons'][] = array(
                'url'   => 'https://github.com/ReduxFramework/ReduxFramework',
                'title' => 'Visit us on GitHub',
                'icon'  => 'el-icon-github'
                //'img'   => '', // You can use icon OR img. IMG needs to be a full URL.
            );
            $this->args['share_icons'][] = array(
                'url'   => 'https://www.facebook.com/pages/Redux-Framework/243141545850368',
                'title' => 'Like us on Facebook',
                'icon'  => 'el-icon-facebook'
            );
            $this->args['share_icons'][] = array(
                'url'   => 'http://twitter.com/reduxframework',
                'title' => 'Follow us on Twitter',
                'icon'  => 'el-icon-twitter'
            );
            $this->args['share_icons'][] = array(
                'url'   => 'http://www.linkedin.com/company/redux-framework',
                'title' => 'Find us on LinkedIn',
                'icon'  => 'el-icon-linkedin'
            );*/

            // Panel Intro text -> before the form
            /*if ( ! isset( $this->args['global_variable'] ) || $this->args['global_variable'] !== false ) {
                if ( ! empty( $this->args['global_variable'] ) ) {
                    $v = $this->args['global_variable'];
                } else {
                    $v = str_replace( '-', '_', $this->args['opt_name'] );
                }
                $this->args['intro_text'] = sprintf( esc_html__( '<p>Did you know that Redux sets a global variable for you? To access any of your saved options from within your code you can use your global variable: <strong>$%1$s</strong></p>', 'soho'), $v );
            } else {
                $this->args['intro_text'] = esc_html__( '<p>This text is displayed above the options panel. It isn\'t required, but more info is always better! The intro_text field accepts all HTML.</p>', 'soho');
            }*/

            // Add content after the form.
            //$this->args['footer_text'] = esc_html__( '<p>This text is displayed below the options panel. It isn\'t required, but more info is always better! The footer_text field accepts all HTML.</p>', 'soho');
        }

        public function validate_callback_function( $field, $value, $existing_value ) {
            $error = true;
            $value = 'just testing';

            /*
          do your validation

          if(something) {
            $value = $value;
          } elseif(something else) {
            $error = true;
            $value = $existing_value;

          }
         */

            $return['value'] = $value;
            $field['msg']    = 'your custom error message';
            if ( $error == true ) {
                $return['error'] = $field;
            }

            return $return;
        }

        public function class_field_callback( $field, $value ) {
            print_r( $field );
            echo '<br/>CLASS CALLBACK';
            print_r( $value );
        }

    }

    $SohoConfig = new soho_options_config();
} else {
    echo "The class named Redux_Framework_sample_config has already been called. <strong>Developers, you need to prefix this class with your company name or you'll run into problems!</strong>";
}

/**
 * Custom function for the callback referenced above
 */
if ( ! function_exists( 'redux_my_custom_field' ) ):
    function redux_my_custom_field( $field, $value ) {
        print_r( $field );
        echo '<br/>';
        print_r( $value );
    }
endif;

/**
 * Custom function for the callback validation referenced above
 * */
if ( ! function_exists( 'redux_validate_callback_function' ) ):
    function redux_validate_callback_function( $field, $value, $existing_value ) {
        $error = true;
        $value = 'just testing';

        /*
      do your validation

      if(something) {
        $value = $value;
      } elseif(something else) {
        $error = true;
        $value = $existing_value;

      }
     */

        $return['value'] = $value;
        $field['msg']    = 'your custom error message';
        if ( $error == true ) {
            $return['error'] = $field;
        }

        return $return;
    }
endif;
