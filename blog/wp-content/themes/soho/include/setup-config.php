<?php
/**
 * Created by Clapat.
 * Date: 28/03/16
 * Time: 6:21 AM
 */

// register navigation menus
if ( ! function_exists( 'soho_register_nav_menus' ) ){
	
	function soho_register_nav_menus() {
		
	  register_nav_menus(
		array(
		  'primary-menu' => esc_html__( 'Primary Menu', 'soho')
		)
	  );
	}
}
add_action( 'init', 'soho_register_nav_menus' );
 
// custom excerpt length
if ( ! function_exists( 'soho_theme_setup' ) ){

    function soho_theme_setup() {

        // Set content width
        if ( ! isset( $content_width ) ) $content_width = 1180;

        // add support for multiple languages
        load_theme_textdomain( 'soho', get_template_directory() . '/languages' );
	
	}

} // soho_theme_setup

/**
 * Tell WordPress to run soho_theme_setup() when the 'after_setup_theme' hook is run.
 */
add_action( 'after_setup_theme', 'soho_theme_setup' );