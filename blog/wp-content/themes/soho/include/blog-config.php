<?php
/**
 * Created by Clapat
 * Date: 26/03/16
 * Time: 13:16 AM
 */

// pagination
if( !function_exists('soho_pagination') ){

    function soho_pagination( $current_query = null ){

        // pages represent the total number of pages
        global $wp_query;
        if( $current_query == null )
            $current_query = $wp_query;
			
		$pages = ($current_query->max_num_pages) ? $current_query->max_num_pages : 1;

		if ( get_query_var('paged') ) { $paged = get_query_var('paged'); }
		elseif ( get_query_var('page') ) { $paged = get_query_var('page'); }
		else { $paged = 1; }
		
        if( $pages > 1 )
        {
            echo '<!-- Blog Navigation -->';
			echo '<div class="blog-nav">';
						
			global $soho_theme_options;
			
			if( get_next_posts_link( '', $current_query->max_num_pages ) ) {
				
				echo '<div class="nav-button older-posts">';
				$nav_caption = '<div class="outer">';
                $nav_caption .= '<div class="inner">';
                $nav_caption .= '<div class="icon-older"><img src="' . get_template_directory_uri() . '/images/prev.png" alt=""></div>';
                $nav_caption .= '<p>' . wp_kses_post( $soho_theme_options['clapat_soho_blog_next_posts_caption'] ) . '</p>';
                $nav_caption .= '</div>';
                $nav_caption .= '</div>';
				next_posts_link( $nav_caption, $current_query->max_num_pages );
				echo '</div>';			 
			}
			else{
				
				echo '<div class="nav-button older-posts">';
				$nav_caption = '<div class="outer">';
                $nav_caption .= '<div class="inner">';
                $nav_caption .= '<div class="icon-older"><img src="' . get_template_directory_uri() . '/images/prev.png" alt=""></div>';
                $nav_caption .= '<p>' . esc_html__('No Posts', 'soho') . '</p>';
                $nav_caption .= '</div>';
                $nav_caption .= '</div>';
				echo wp_kses_post( $nav_caption );
				echo '</div>';
			}
			
			if ( get_previous_posts_link() ){
				
				echo '<div class="nav-button newer-posts">';
				$nav_caption = '<div class="outer">';
                $nav_caption .= '<div class="inner">';
                $nav_caption .= '<div class="icon-newer"><img src="' . get_template_directory_uri() . '/images/next.png" alt=""></div>';
                $nav_caption .= '<p>' . wp_kses_post( $soho_theme_options['clapat_soho_blog_prev_posts_caption'] ) . '</p>';
                $nav_caption .= '</div>';
                $nav_caption .= '</div>';
				previous_posts_link( $nav_caption, $current_query->max_num_pages );
				echo '</div>';
				
            }
			else{
				
				echo '<div class="nav-button newer-posts">';
				$nav_caption = '<div class="outer">';
                $nav_caption .= '<div class="inner">';
                $nav_caption .= '<div class="icon-newer"><img src="' . get_template_directory_uri() . '/images/next.png" alt=""></div>';
                $nav_caption .= '<p>' . esc_html__('No Posts', 'soho') . '</p>';
                $nav_caption .= '</div>';
                $nav_caption .= '</div>';
				echo wp_kses_post( $nav_caption );
				echo '</div>';
			}
					
			echo '</div>';
            echo '<!-- /Blog Navigation -->';
        }
    }

} // pagination function


// comments
if( !function_exists('soho_comment') ){

    function soho_comment($comment, $args, $depth) {

        $GLOBALS['comment'] = $comment;
        $add_below = '';
		if( $depth > 1 ){ //reply comment
			echo '<div class="user_comment_reply" ';
		}
		else{ //top comment
			echo '<div class="user_comment" ';
		}
        comment_class();
        echo ' id="div-comment-';
        comment_ID();
        echo '">';
		echo '<div class="user-image">'. get_avatar($comment, 54) . '</div>';
		echo '<div class="comment-box">';
        echo '<h5 class="comment-name">'. get_comment_author_link() . ' ' . esc_html__('says:', 'soho') . '</h5>';
        echo '<p class="comment-date">' . get_comment_date() . ' ' . esc_html__('at', 'soho') . ' ' . get_comment_time() . '</p>';

        echo '<div class="comment-text">';
        if ($comment->comment_approved == '0'){
            echo '<em>' . esc_html__("Your comment is awaiting moderation", 'soho') . '</em><br />';
        }
        comment_text();
		comment_reply_link(array_merge( $args, array('reply_text' => esc_html__('Reply', 'soho'), 'before' => '', 'after' => '', 'add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth'])));
		echo '</div>'; // div comment-box
        echo '</div>'; // div user_comment

    }
}

// the caption displayed within single blog post hero pages
if( !function_exists('soho_blog_post_hero_caption') ){

    function soho_blog_post_hero_caption() {

    	// should be called in the loop
		$hero_caption = '';
		$hero_caption .= '<div class="meta-categories">';
		$hero_caption .= '<ul class="post-categories">';
		
		$post_categories = get_the_category();
		foreach( $post_categories as $post_category ){
			
			if( $post_category ){
				
				$hero_caption .= "<li>";
				$hero_caption .= '<a href="' . get_category_link( $post_category->term_id ) .'" rel="category tag">' . $post_category->name . '</a>';
				$hero_caption .= "</li>";
			}
		}
		
		$hero_caption .= '</ul>';						
    	$hero_caption .= '</div>';
        $hero_caption .= '<h1 class="big-title post-title-no-link">' . get_the_title() . '</h2>';

        return $hero_caption;
    }
}


?>