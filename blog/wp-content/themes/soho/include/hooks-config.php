<?php
/**
 * Created by Clapat
 * Date: 28/03/16
 * Time: 6:29 AM
 */

 // Extra classes to the body
if ( ! function_exists( 'soho_body_class' ) ){

	function soho_body_class( $classes ) {
		
		$classes[] = 'animsition';
		
		// return the $classes array
		return $classes;
	}
}
add_filter( 'body_class', 'soho_body_class' );
	
// wp_head hook
if ( ! function_exists( 'soho_wp_head_hook' ) ){

    function soho_wp_head_hook(){

		global $soho_theme_options;
		
		// some of the css attributes which could not be linked directly though theme options
		if( isset( $soho_theme_options['clapat_soho_styling_custom_css'] ) ){

            if( trim($soho_theme_options['clapat_soho_styling_custom_css']) ){

                echo '<style type="text/css">';
                echo trim($soho_theme_options['clapat_soho_styling_custom_css']);
                echo '</style>';
            }
        }
		
		if( isset( $soho_theme_options['clapat_soho_enable_preloader'] ) && $soho_theme_options['clapat_soho_enable_preloader'] ){

            if( $soho_theme_options['clapat_soho_enable_preloader_img']['url'] ) {
			echo '<style type="text/css">';
			echo '.animsition-loading { ';
			echo 'background-image: url(' . $soho_theme_options['clapat_soho_enable_preloader_img']['url'] . ')!important;';
			echo 'background-size:30px 30px!important;';
			echo 'background-position:center!important;';
			echo '}';
			echo '</style>';
			}
        }
		
		if( !empty( $soho_theme_options['clapat_soho_space_head'] ) ){
            echo $soho_theme_options['clapat_soho_space_head'];
        }
    }

}
add_action('wp_head', 'soho_wp_head_hook');

// site/blog title
if ( ! function_exists( '_wp_render_title_tag' ) ) {

	function soho_wp_title() {

		echo wp_title( '|', false, 'right' );

	}
	add_action( 'wp_head', 'soho_wp_title' );
}

if ( ! function_exists( 'soho_menu_classes' ) ){

    function soho_menu_classes(  $classes , $item, $args ){

		if( isset( $args->theme_location ) ){
			if( in_array( 'current-menu-item', $item->classes ) || in_array( 'current-menu-ancestor', $item->classes ) ){
			
				$classes[] = 'active';
			}
			if( in_array( 'menu-item-has-children', $item->classes ) ){
			
				$classes[] = 'has-sub';
			}
		}
		
		return $classes;
    }

}
if ( ! function_exists( 'soho_menu_link_attributes' ) ){

    function soho_menu_link_attributes(  $atts, $item, $args ){

		$arr_classes = array();
		$arr_classes[] = 'animation-link';
		if( isset( $args->theme_location ) ){
			
			$arr_classes[] = 'menu-link';
		}
		else{
			if( !empty($item->classes) ){
				if( in_array( 'current-menu-item', $item->classes ) || in_array( 'current-menu-ancestor', $item->classes ) ){
			
					$arr_classes[] = 'active';
				}
			}
		}
		
		if( !empty($item->classes) ){
			
			if( in_array( 'menu-item-has-children', $item->classes ) ){
				// if the menu item is a parent item, just use an empty <a> tag
				$arr_classes = null;
				$atts['href'] = null;
			}
		}
		if( !empty( $arr_classes ) ){
		
			$atts['class'] = implode( ' ', $arr_classes );
		}
				
		return $atts;
    }

}
// change priority here if there are more important actions associated with the hook
add_action('nav_menu_css_class', 'soho_menu_classes', 10, 3);
add_filter('nav_menu_link_attributes', 'soho_menu_link_attributes', 10, 3 );

// hooks to add extra classes for next & prev portfolio projects and single blog posts
if ( ! function_exists( 'soho_prev_post_link' ) ){

    function soho_prev_post_link( $output, $format, $link, $post ){
	
		if( $format == 'soho_portfolio' ){
			
			$output = '';
			if( $post ){
				
				global $soho_theme_options;
				
				$post_image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full');
				$prev_image = '';
				if( $post_image ){
					
					$prev_image = $post_image[0];
				}
				
				$item_caption_bknd = soho_get_post_meta( SOHO_THEME_OPTIONS, $post->ID, 'soho-opt-portfolio-caption-bknd' );
				
				$output .= '<div class="item revealed ' . $item_caption_bknd . '">';
				$output .= '<a href="' . get_permalink( $post ) . '" class="animation-link">';
				$output .= '<div style="background-image:url(' . esc_url($prev_image) . ')" class="item-content"></div>';
				$output .= '<div class="item-overlay">';
                $output .= '<div class="outer"><div class="inner">';
                $output .= '<span class="item-cat">' . wp_kses_post( $soho_theme_options['clapat_soho_portfolio_prev_caption'] ) . '</span>';
                $output .= '<span class="item-title">' . get_the_title( $post ) . '</span>';
				$output .= '</div></div>'; 
                $output .= '</div>';
				$output .= '</a>';
				$output .= '</div>';
			}
		}
		else if(  $format == 'blog-post' ){

			$output = '';
			if( $post ){
				
				global $soho_theme_options;

				$output .= '<div class="article-next-container">';
				$output .= '<div class="article-next"';
					
				$post_image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full');
				if( $post_image ){
					$output .= ' style="background-image:url(' . esc_url( $post_image[0] ) .'"';
				}
					
				$output .= '>';
				$output .= '<a href="' . get_permalink( $post ) . '" class="animation-link">';
				$output .= '<div class="overlay">';
				$output .= '<div class="outer">';
				$output .= '<div class="inner">';
				$output .= '<div class="next-post">';
				$output .= '<h1>' . $soho_theme_options['clapat_soho_blog_next_post_caption'] . '</h1>';
				$output .= '<p>' . get_the_title( $post ) . '</p>';
				$output .= '</div>';
				$output .= '</div>';
				$output .= '</div>';
				$output .= '</div>';
				$output .= '</a>';
				$output .= '</div>';
				
				$output .= '</div>';				
			}
			
			return $output;
		}
		else {
			
			if( $post ){
				
				$output = get_permalink( $post );
			}
		}

		return $output;
    }

}
if ( ! function_exists( 'soho_next_post_link' ) ){

    function soho_next_post_link( $output, $format, $link, $post ){
		
		if( $format == 'soho_portfolio' ){
			
			$output = '';
			if( $post ){
				
				global $soho_theme_options;
				
				$post_image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full');
				$next_image = '';
				if( $post_image ){
					
					$next_image = $post_image[0];
				}
				
				$item_caption_bknd = soho_get_post_meta( SOHO_THEME_OPTIONS, $post->ID, 'soho-opt-portfolio-caption-bknd' );
				
				$output .= '<div class="item revealed ' . $item_caption_bknd . '">';
				$output .= '<a href="' . get_permalink( $post ) . '" class="animation-link">';
				$output .= '<div style="background-image:url(' . esc_url($next_image) . ')" class="item-content"></div>';
				$output .= '<div class="item-overlay">';
                $output .= '<div class="outer"><div class="inner">';
                $output .= '<span class="item-cat">' . wp_kses_post( $soho_theme_options['clapat_soho_portfolio_next_caption'] ) . '</span>';
                $output .= '<span class="item-title">' . get_the_title( $post ) . '</span>';
				$output .= '</div></div>'; 
                $output .= '</div>';
				$output .= '</a>';
				$output .= '</div>';
			} 
			
		}
		else if( $format == 'blog-post' ){
			
			// nothing here for the moment
		}
		else {
			
			if( $post ){
				
				$output = get_permalink( $post );
			}
		}
		
		return $output;
	}

}
// change priority here if there are more important actions associated with the hook
add_filter('next_post_link', 'soho_next_post_link', 10, 4);
add_filter('previous_post_link', 'soho_prev_post_link', 10, 4);

// hooks to add extra classes for next & prev blog posts 
if ( ! function_exists( 'soho_next_posts_link_attributes' ) ){
	
	function soho_next_posts_link_attributes(){
		
		return 'class="page-nav animation-link"';
	}
}
if ( ! function_exists( 'soho_prev_posts_link_attributes' ) ){
	
	function soho_prev_posts_link_attributes(){
		
		return 'class="page-nav animation-link"';
	}
}
// change priority here if there are more important actions associated with the hook
add_filter('next_posts_link_attributes', 'soho_next_posts_link_attributes', 10, 4);
add_filter('previous_posts_link_attributes', 'soho_prev_posts_link_attributes', 10, 4);

if ( ! function_exists( 'soho_comment_reply_link' ) ){
	
	function soho_comment_reply_link($link, $args, $comment, $post){
		
		return str_replace("class='comment-reply-link", "class='comment-reply-link reply", $link);
	}
}
// change priority here if there are more important actions associated with the hook
add_filter('comment_reply_link', 'soho_comment_reply_link', 99, 4);

// search filter
if( !function_exists('soho_searchfilter') ){

    function soho_searchfilter( $query ) {

    	if ( !is_admin() && $query->is_main_query() ) {
        	
    		if ($query->is_search ) {

    			$post_types = get_query_var('post_type');
    			
    			if( empty( $post_types ) ){
    			
            		$query->set('post_type', array('post'));
    			}
        	}

        }
        
        return $query;

    }
    add_filter('pre_get_posts','soho_searchfilter');

}