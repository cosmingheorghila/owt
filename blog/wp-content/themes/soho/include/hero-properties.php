<?php

if ( ! class_exists( 'SohoHeroProperties' ) ) {
	
	class SohoHeroProperties 
	{
		public $size;
		public $position;
		public $opacity;
		public $type;
		public $scroll_arrow;
		public $use_main_slider;
		public $custom_slider;
		
		public $image;
		public $image_overlay_color;
		public $image_overlay_color_opacity; 
		public $image_caption;
		public $image_caption_position;
		public $content_type;
		
		public $video_url;
		public $video_placeholder;
	}
}

$soho_hero_properties = new SohoHeroProperties();

?>