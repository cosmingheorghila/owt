<?php
/**
 * Created by Clapat.
 * Date: 17/09/15
 * Time: 5:51 AM
 */

// Featured images support
add_theme_support( 'post-thumbnails', array( 'post', 'soho_portfolio' ) ); 
// Automatic feed links support
add_theme_support( 'automatic-feed-links' );
// menus
add_theme_support( 'menus' );
// title
add_theme_support( 'title-tag' );

?>