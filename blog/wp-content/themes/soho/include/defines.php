<?php

define('SOHO_THEME_ID', 'soho');
define('SOHO_THEME_OPTIONS', 'clapat_' . SOHO_THEME_ID . '_theme_options');

$soho_social_links = array('facebook' => 'Facebook',
							'twitter' => 'Twitter',
							'behance' => 'Behance',
							'dribbble' => 'Dribbble',
							'google-plus' => 'Google Plus',
							'instagram' => 'Instagram',
							'linkedin' => 'Linkedin',
							'whatsapp' => 'WhatsApp',
							'deviantart' => 'DeviantArt',
							'digg' => 'Digg',
							'flickr' => 'Flickr',
							'foursquare' => 'Foursquare',
							'git' => 'Git',
							'pinterest' => 'Pinterest',
							'reddit' => 'Reddit',
							'skype' => 'Skype',
							'stumbleupon' => 'Stumbleupon',
							'tumblr' => 'Tumblr',
							'yahoo' => 'Yahoo',
							'yelp' => 'Yelp',
							'youtube' => 'Youtube',
							'xing'	=> 'Xing' );
							
define ('SOHO_MAX_SOCIAL_LINKS', 8 );

?>