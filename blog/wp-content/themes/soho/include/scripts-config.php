<?php
/**
 * Created by Clapat.
 * Date: 15/01/16
 * Time: 6:26 AM
 */

if ( ! function_exists( 'soho_load_scripts' ) ){

    function soho_load_scripts() {

        // Register css files
        wp_register_style( 'soho_flexslider', get_template_directory_uri() . '/css/flexslider.css', TRUE);

        wp_register_style( 'soho_fontawesome', get_template_directory_uri() . '/css/font-awesome.min.css', TRUE);

        wp_register_style( 'soho_sliders', get_template_directory_uri() . '/css/sliders.css', TRUE);
        
		wp_register_style( 'soho_animsition', get_template_directory_uri() . '/css/animsition.css', TRUE);
		
		wp_register_style( 'soho_photoswipe', get_template_directory_uri() . '/css/photoswipe.css', TRUE);
		
		wp_register_style( 'soho_owlcarousel', get_template_directory_uri() . '/css/owl.carousel.css', TRUE);
		
		// Register scripts
        wp_register_script(
            'soho_plugins',
            get_template_directory_uri() . '/js/plugins.js',
            array('jquery'),
            false,
            true
        );

        wp_register_script(
            'soho_scriptsjs',
            get_template_directory_uri() . '/js/scripts.js',
            array('jquery'),
            false,
            true
        );
        
        wp_enqueue_style('soho_flexslider');
        wp_enqueue_style('soho_sliders');
        wp_enqueue_style('soho_animsition');
		wp_enqueue_style('soho_photoswipe');
		wp_enqueue_style('soho_owlcarousel');
		wp_enqueue_style('soho-theme', get_stylesheet_uri(), array('soho_sliders', 'soho_animsition', 'soho_photoswipe', 'soho_owlcarousel'));
        wp_enqueue_style('soho_fontawesome');

		// enqueue standard font style
		$font_url = '';
		/*
		Translators: If there are characters in your language that are not supported
		by chosen font(s), translate this to 'off'. Do not translate into your own language.
		 */
		if ( 'off' !== _x( 'on', 'Google font: on or off', 'soho') ) {
			$font_url = add_query_arg( 'family', urlencode( 'Open+Sans:700,400' ), "//fonts.googleapis.com/css" );
		}
		wp_enqueue_style( 'clapat-soho-font', $font_url, array(), '1.0.0' );

        // enqueue scripts
        wp_enqueue_script(
            'soho_plugins'
        );

        if ( is_singular() ) wp_enqueue_script( 'comment-reply' );
		
        wp_enqueue_script(
            'soho_scriptsjs'
        );
			
			
		global $soho_theme_options;

		wp_localize_script( 'soho_scriptsjs',
                    'ClapatSohoThemeOptions',
                    array(  "enable_preloader"   => $soho_theme_options['clapat_soho_enable_preloader'] ) 
					);
					
		wp_localize_script( 'soho_scriptsjs',
                    'ClapatSecondaryMenuOptions',
					array(  "filters_open_caption"  => $soho_theme_options['clapat_soho_portfolio_menu_filters_caption_open'],
                     		"filters_close_caption" => $soho_theme_options['clapat_soho_portfolio_menu_filters_caption_close'],
                    		"share_open_caption"   	=> $soho_theme_options['clapat_soho_portfolio_menu_share_caption_open'],
                    		"share_close_caption"  	=> $soho_theme_options['clapat_soho_portfolio_menu_share_caption_close'],
                    		"search_open_caption"   => $soho_theme_options['clapat_soho_blog_search_caption_open'],
                    		"search_close_caption"  => $soho_theme_options['clapat_soho_blog_search_caption_close'] ) 
					);			
							
		wp_localize_script( 'soho_scriptsjs',
                    'FullScreenSliderOptions',
                    array(  "slider_direction"   => $soho_theme_options['clapat_soho_slider_direction'],
                            "slider_speed"       => $soho_theme_options['clapat_soho_slider_speed'],
                            "slider_loop_top"    => ($soho_theme_options['clapat_soho_slider_loop_top'] ? true : false),
							"slider_loop_bottom" => ($soho_theme_options['clapat_soho_slider_loop_bottom'] ? true : false) ) );
		
		wp_localize_script( 'soho_scriptsjs',	
							'ClapatMapOptions',
							array(  "map_marker_image"   	=> esc_js( esc_url ( $soho_theme_options["clapat_soho_map_marker"]["url"] ) ),
									"map_address"       	=> $soho_theme_options['clapat_soho_map_address'],
									"map_zoom"    			=> $soho_theme_options['clapat_soho_map_zoom'],
									"marker_title"  		=> $soho_theme_options['clapat_soho_map_company_name'],
									"marker_text"  			=> $soho_theme_options['clapat_soho_map_company_info'],
									"map_type" 				=> $soho_theme_options['clapat_soho_map_type'] ) );
    }

}

add_action('wp_enqueue_scripts', 'soho_load_scripts');

if ( ! function_exists( 'soho_admin_load_scripts' ) ){

    function soho_admin_load_scripts() {

        wp_enqueue_script( 'wpb_composer_front_js' );

    }

}

add_action( 'admin_enqueue_scripts', 'soho_admin_load_scripts' );