<?php

// You may replace $redux_opt_name with a string if you wish. If you do so, change loader.php
// as well as all the instances below.
$redux_opt_name = 'clapat_' . SOHO_THEME_ID . '_theme_options';


if ( !function_exists( "soho_add_metaboxes" ) ){

    function soho_add_metaboxes( $metaboxes ) {

    $metaboxes = array();


    ////////////// Page Options //////////////
    $page_options = array();
    $page_options[] = array(
        'title'         => esc_html__('General', 'soho'),
        'icon_class'    => 'icon-large',
        'icon'          => 'el-icon-wrench',
        'desc'          => esc_html__('Options concerning all page templates.', 'soho'),
        'fields'        => array(

			array(
                'id'        => 'soho-opt-page-header-background',
                'type'      => 'select',
                'title'     => esc_html__('Header background', 'soho'),
                'desc'      => esc_html__('The background of the header for this page when scrolling page content. The logo will revert accordingly. The hero section is not concerned by this setting only the page content.', 'soho'),
				'options'   => array(
                    'black' => esc_html__('Dark', 'soho'),
                    'header-light' => esc_html__('Light', 'soho')
                ),
                'default'   => 'black'
            ),
			
			array(
                'id'        => 'soho-opt-page-footer-background',
                'type'      => 'select',
                'title'     => esc_html__('Footer background', 'soho'),
                'desc'      => esc_html__('The background of the footer for this page.', 'soho'),
				'options'   => array(
                    'footer-default' => esc_html__('Dark', 'soho'),
                    'alternate' => esc_html__('Light', 'soho')
                ),
                'default'   => 'footer-default'
            ),
			
			/**************************HERO SECTION OPTIONS**************************/
			array(
                'id'        => 'soho-opt-page-hero-type',
                'type'      => 'select',
                'title'     => esc_html__('Hero Type', 'soho'),
                'desc'      => esc_html__('Type of the "hero" section displayed as page header. Does not apply to Contact Page Template, where "hero" section is the map.', 'soho'),
				'options'   => array(
                    'image' => esc_html__('Image', 'soho'),
                    'slider' => esc_html__('Slider', 'soho'),
					'video' => esc_html__('Video', 'soho'),
                    'none' => esc_html__('None', 'soho'),
                ),
                'default'   => 'none'
            ),
		
			array(
                'id'        	=> 'soho-opt-page-logo-hero',
                'type'      	=> 'select',
                'title'     	=> esc_html__('Logo Type In Hero Section', 'soho'),
                'desc'      	=> esc_html__('The type of logo displayed in hero section. Depends on hero\'s background', 'soho'),
				'options'   	=> array(
                    'dark' 		=> esc_html__('Dark', 'soho'),
                    'negative' 	=> esc_html__('Light', 'soho')
                ),
				'required' 	=> array('soho-opt-page-hero-type', '!=', 'none'),
                'default'   => 'black'
            ),
			
			
			// Image Hero
			array(
                'id'        => 'soho-opt-page-hero-image',
                'type'      => 'media',
                'required'  => array('soho-opt-page-hero-type', '=', 'image'),
                'url'       => true,
                'title'     => esc_html__('Upload Hero Image', 'soho'),
                'desc'      => '',
            ),
			
			// Video hero
			array(
                 'id'       => 'soho-opt-page-hero-video-url',
                 'type'     => 'text',
                 'title'    => esc_html__( 'Video ID Code', 'soho'),
                 'subtitle' => wp_kses_post( __( 'The ID of the video displayed in the hero section. Only youtube videos are allowed. You can find the video ID in the video URL after <b>v=</b>', 'soho') ),
                 'required' => array('soho-opt-page-hero-type', '=', 'video'),
             ),
			
			array(
                'id'        => 'soho-opt-page-hero-video-placeholder',
                'type'      => 'media',
                'required'  => array('soho-opt-page-hero-type', '=', 'video'),
                'url'       => true,
                'title'     => esc_html__('Upload Video Placeholder Image', 'soho'),
                'desc'      => esc_html__('Video placeholder image will be displayed on top of the video when it is stopped', 'soho'),
				'required'  => array('soho-opt-page-hero-type', '=', 'video'),
            ),
			
			// Options for image and video overlay color and caption
			array(
                'id'        => 'soho-opt-page-hero-image-overlay-color',
                'type'      => 'color',
                'required'  => array('soho-opt-page-hero-type', '=', array('image', 'video')),
                'title'     => esc_html__('Hero Image Overlay Color', 'soho'),
                'subtitle'  => esc_html__('Pick an overlay color for your hero image (default: #FFFFFF).', 'soho'),
                'transparent' => false,
                'default'   => '#FFFFFF',
                'validate'  => 'color',
            ),
			
            array(
                'id'            => 'soho-opt-page-hero-image-overlay-color-opacity',
                'type'          => 'slider',
                'required'  	=> array('soho-opt-page-hero-type', '=', array('image', 'video')),
                'title'         =>  esc_html__('Hero Image Overlay Color Opacity', 'soho'),
                'subtitle'      => esc_html__('Specifies the opacity. From 0.0 (fully transparent) to 1.0 (fully opaque)', 'soho'),
                'default'       => 0,
                'min'           => 0,
                'step'          => .1,
                'max'           => 1,
                'resolution'    => 0.1,
                'display_value' => 'text'
            ),
			
			array(
                'id'        => 'soho-opt-page-hero-image-caption',
                'type'      => 'textarea',
				'required'  => array('soho-opt-page-hero-type', '=', array('image', 'video')),
                'title'     => esc_html__('Hero Image Caption', 'soho'),
                'subtitle'  => esc_html__('Caption displayed over hero\'s image. HTML code allowed in this field.', 'soho'),
                'validate'  => 'html', //see http://codex.wordpress.org/Function_Reference/wp_kses_post
	        ),
		
			array(
                'id'        => 'soho-opt-page-hero-image-caption-position',
                'type'      => 'select',
                'title'     => esc_html__('Hero Image Caption Position', 'soho'),
                'options'   => array(
                    'center-center' => 'Center-Center',
                    'center-left' => 'Center-Left',
                    'center-right' => 'Center-Right',
                    'bottom-center' => 'Bottom-Center',
                    'bottom-left' => 'Bottom-Left',
                    'bottom-right' => 'Bottom-Right',
                    'top-center' => 'Top-Center',
                    'top-left' => 'Top-Left',
                    'top-right' => 'Top-Right',
                ),
				'required'  => array('soho-opt-page-hero-type', '=', array('image', 'video')),
                'default'   => 'center-center'
            ),
			
			// General hero options
			array(
                 'id'       => 'soho-opt-page-hero-scrolling-arrow',
                 'type'     => 'switch',
                 'title'    => esc_html__( 'Show Scrolling Icon', 'soho'),
                 'subtitle' => esc_html__( 'Shows the scroll down icon.', 'soho'),
                 'on'       => 'Yes',
				 'off'      => 'No',
				 'default'	=> true,
                 'required' => array('soho-opt-page-hero-type', '!=', 'none')
            ),
            
			array(
                'id'        => 'soho-opt-page-hero-height',
                'type'      => 'select',
                'title'     => esc_html__('Hero Height', 'soho'),
                'desc'      => esc_html__('Height of the "hero" section displayed as page header.', 'soho'),
				'options'   => array(
					'small' => esc_html__('Small', 'soho'),
                    'large' => esc_html__('Large', 'soho'),
                    'full'  => esc_html__('Full', 'soho')
                ),
                'default'   => 'full',
				'required'  => array( 'soho-opt-page-hero-type', '!=', 'none' ),
            ),
			
			array(
                'id'        => 'soho-opt-page-hero-position',
                'type'      => 'select',
                'title'     => esc_html__('Hero Position', 'soho'),
                'desc'      => esc_html__('Position of the "hero" section displayed as page header.', 'soho'),
				'options'   => array(
                    'static' 	=> esc_html__('Static', 'soho'),
                    'relative' 	=> esc_html__('Relative', 'soho'),
					'parallax' 	=> esc_html__('Parallax', 'soho')
                ),
                'default'   => 'static',
				'required'  => array( 'soho-opt-page-hero-type', '!=', 'none' ),
            ),
			
			array(
                 'id'       => 'soho-opt-page-hero-opacity',
                 'type'     => 'switch',
                 'title'    => esc_html__( 'Hero Scrolling Opacity', 'soho'),
                 'subtitle' => esc_html__( 'Enable hero section scrolling opacity.', 'soho'),
                 'on'       => 'Yes',
				 'off'      => 'No',
				 'default'	=> true,
                 'required' => array('soho-opt-page-hero-type', '!=', 'none')
            ),
			
			array(
                'id'        => 'soho-opt-page-hero-content',
                'type'      => 'select',
                'title'     => esc_html__('Hero Content', 'soho'),
                'desc'      => esc_html__('Type of content in "hero" section.', 'soho'),
				'options'   => array(
                    'dark' => esc_html__('Dark', 'soho'),
                    'light' => esc_html__('Light', 'soho')
                ),
                'default'   => 'dark',
				'required'  => array('soho-opt-page-hero-type', '=', array('image', 'video') ),
            ),
			
			array(
                 'id'       => 'soho-opt-page-hero-use-main-slider',
                 'type'     => 'switch',
                 'title'    => esc_html__( 'Use Main Slider', 'soho'),
                 'subtitle' => esc_html__( 'Use the main slider or use other slider type.', 'soho'),
                 'on'       => 'Yes',
				 'off'      => 'No',
				 'default'	=> true,
                 'required' => array('soho-opt-page-hero-type', '=', 'slider')
            ),
			 
			array(
                 'id'       => 'soho-opt-page-hero-custom-slider',
                 'type'     => 'text',
                 'title'    => esc_html__( 'Your slider shortcode', 'soho'),
                 'subtitle' => esc_html__( 'Most of the popular slider plugins use shortcodes to insert sliders into content. Paste your shortcode here.', 'soho'),
                 'required' => array('soho-opt-page-hero-use-main-slider', '=', false)
             ),
			 
			/**************************END - HERO SECTION OPTIONS**************************/
			
        ),
    );
	
	$page_options[] = array(
        'title'         => esc_html__('Portfolio Templates', 'soho'),
        'icon_class'    => 'icon-large',
        'icon'          => 'el-icon-folder-open',
        'desc'          => esc_html__('Options concerning only Portfolio templates (Portfolio and Portfolio Mixed).', 'soho'),
        'fields'        => array(

			array(
                'id'        => 'soho-opt-page-portfolio-columns',
                'type'      => 'select',
                'title'     => esc_html__('Portfolio Columns', 'soho'),
                'desc'      => esc_html__('Number of portfolio items columns displayed in the portfolio page.', 'soho'),
				'options'   => array( '2' => '2', '3' => '3' ),
                'default'   => '3'
            ),
						
			array(
                'id'        => 'soho-opt-page-portfolio-lightbox',
                'type'      => 'switch',
                'title'     => esc_html__('Lightbox Popup Items', 'soho'),
                'desc'      => esc_html__('If selected displays all portfolio items as lightbox popups.', 'soho'),
                'default'   => false,
				'on'        => 'Yes',
                'off'       => 'No'
            ),
            
			array(
                'id'        => 'soho-opt-page-portfolio-margins',
                'type'      => 'switch',
                'title'     => esc_html__('Enable Portfolio Thumbnail Margins', 'soho'),
                'desc'      => esc_html__('Enable margins for portfolio items thumbnails displayed in portfolio page.', 'soho'),
                'default'   => true
            ),
			
			array(
                'id'        => 'soho-opt-page-portfolio-hover-effect',
                'type'      => 'select',
                'title'     => esc_html__('Hover Effect', 'soho'),
                'desc'      => esc_html__('Select the type of the effect when hovering over portfolio items.', 'soho'),
				'options'   => array( 'revealed' => 'Permanent', 'zoomin' => 'Zoom In', 'normal' => 'Normal' ),
                'default'   => 'normal'
            ),
			
			array(
                'id'        => 'soho-opt-page-portfolio-show-filters',
                'type'      => 'switch',
                'title'     => esc_html__('Show Portfolio Filters', 'soho'),
                'desc'      => esc_html__('Shows or hides by default the portfolio category filters in the portfolio page.', 'soho'),
                'default'   => true,
				'on'        => 'Yes',
                'off'       => 'No'
            ),
						
			array(
                 'id'       => 'soho-opt-page-portfolio-mixed-items',
                 'type'     => 'text',
                 'title'    => esc_html__( 'Maximum Number Of Items In Portfolio Mixed', 'soho' ),
                 'subtitle' => esc_html__( 'Available only for Portfolio Mixed Template: the maximum number of portfolio items displayed. Leave empty for ALL.', 'soho' )
             ),
			
        ),
    );

    $page_options[] = array(
            'title'         => esc_html__('Contact Template', 'soho'),
            'icon_class'    => 'icon-large',
            'icon'          => 'el-icon-map-marker',
            'desc'          => esc_html__('Options concerning only Contact template.', 'soho'),
            'fields'        => array(

                array(
                    'id'        => 'soho-opt-page-map-height',
                    'type'      => 'select',
                    'title'     => esc_html__('Map Height', 'soho'),
                    'desc'      => esc_html__('Height of the map section displayed as page header.', 'soho'),
                    'options'   => array(
										'small' => esc_html__('Small', 'soho'),
										'large' => esc_html__('Large', 'soho'),
										'full'  => esc_html__('Full', 'soho')
									),
                    'default'   => 'full'
                ),
                
                array(
	                'id'        => 'soho-opt-page-map-show-overlay',
	                'type'      => 'switch',
	                'title'     => esc_html__('Show Map Overlay', 'soho'),
	                'desc'      => esc_html__('Shows map overlay when mouse cursor outside map area.', 'soho'),
	                'default'   => true,
					'on'        => 'Yes',
	                'off'       => 'No'
            	),
            
                array(
	                'id'        => 'soho-opt-page-map-overlay-color',
	                'type'      => 'color',
	                'title'     => esc_html__('Map Overlay Color', 'soho'),
	                'subtitle'  => esc_html__('Pick an overlay color for your hero image (default: #FFFFFF).', 'soho'),
	                'transparent' => false,
	                'default'   => '#000000',
	                'validate'  => 'color',
	                'required'  => array('soho-opt-page-map-show-overlay', '=', true),
	            ),
				
	            array(
	                'id'            => 'soho-opt-page-map-overlay-color-opacity',
	                'type'          => 'slider',
	                'title'         =>  esc_html__('Map Overlay Color Opacity', 'soho'),
	                'subtitle'      => esc_html__('Specifies the opacity. From 0.0 (fully transparent) to 1.0 (fully opaque)', 'soho'),
	                'default'       => 0.8,
	                'min'           => 0,
	                'step'          => .1,
	                'max'           => 1,
	                'resolution'    => 0.1,
	                'display_value' => 'text',
	                'required'  => array('soho-opt-page-map-show-overlay', '=', true),
	            ),
				
				array(
	                'id'        => 'soho-opt-page-map-caption',
	                'type'      => 'textarea',
					'title'     => esc_html__('Map Overlay Caption', 'soho'),
	                'subtitle'  => esc_html__('Caption displayed over hero\'s image. HTML code allowed in this field.', 'soho'),
	                'validate'  => 'html', //see http://codex.wordpress.org/Function_Reference/wp_kses_post
					'required'  => array('soho-opt-page-map-show-overlay', '=', true),
		        ),
				
		        array(
	                'id'        => 'soho-opt-page-map-caption-content',
	                'type'      => 'select',
	                'title'     => esc_html__('Map Overlay Caption Content', 'soho'),
	                'desc'      => esc_html__('Type of content in the map overlay.', 'soho'),
					'options'   => array(
	                    'dark' => esc_html__('Dark', 'soho'),
	                    'light' => esc_html__('Light', 'soho')
	                ),
	                'default'   => 'light',
					'required'  => array('soho-opt-page-map-show-overlay', '=', true),
            	),
            
				array(
	                'id'        => 'soho-opt-page-map-caption-position',
	                'type'      => 'select',
	                'title'     => esc_html__('Map Caption Position', 'soho'),
	                'options'   => array(
	                    'center-center' => 'Center-Center',
	                    'center-left' => 'Center-Left',
	                    'center-right' => 'Center-Right',
	                    'bottom-center' => 'Bottom-Center',
	                    'bottom-left' => 'Bottom-Left',
	                    'bottom-right' => 'Bottom-Right',
	                    'top-center' => 'Top-Center',
	                    'top-left' => 'Top-Left',
	                    'top-right' => 'Top-Right',
	                ),
					'required'  => array('soho-opt-page-map-show-overlay', '=', true),
	                'default'   => 'center-center'
	            ),
            ),
    );
	
	$page_options[] = array(
            'title'         => esc_html__('Showcase Alt Template', 'soho'),
            'icon_class'    => 'icon-large',
            'icon'          => 'el-icon-folder-open',
            'desc'          => esc_html__('Options concerning only Showcase Alt template.', 'soho'),
            'fields'        => array(

                array(
					'id'       => 'soho-opt-page-showcase-alt-title',
					'type'     => 'text',
					'title'    => esc_html__( 'Showcase Title', 'soho' ),
					'default'  => esc_html__( 'See our featured Case Studies', 'soho' )
				),
				
				array(
					'id'       => 'soho-opt-page-showcase-alt-url',
					'type'     => 'text',
					'title'    => esc_html__( 'Showcase URL', 'soho' ),
					'subtitle' => esc_html__( 'Url to the projects or portfolio items page featuring this showcase. Leave empty for none.', 'soho' )
				),
				
				array(
					'id'       => 'soho-opt-page-showcase-alt-url-caption',
					'type'     => 'text',
					'title'    => esc_html__( 'Showcase URL Caption', 'soho' ),
					'subtitle' => esc_html__( 'Caption of the link to the projects or portfolio items page featuring this showcase.', 'soho' ),
					'default'  => esc_html__( 'View All Works', 'soho' )
				),
            ),
    );

    $metaboxes[] = array(
        'id'            => 'clapat_' . SOHO_THEME_ID . '_page_options',
        'title'         => esc_html__( 'Page Options', 'soho'),
        'post_types'    => array( 'page' ),
        'position'      => 'normal', // normal, advanced, side
        'priority'      => 'high', // high, core, default, low
        'sidebar'       => false, // enable/disable the sidsebar in the normal/advanced positions
        'sections'      => $page_options,
    );

    $blog_post_options = array();
    $blog_post_options[] = array(

         'icon_class'    => 'icon-large',
         'icon'          => 'el-icon-wrench',
         'fields'        => array(
		 
			array(
                'id'        => 'soho-opt-blog-header-background',
                'type'      => 'select',
                'title'     => esc_html__('Header background', 'soho'),
                'desc'      => esc_html__('The background of the header for this post when scrolling page content. The logo will revert accordingly. The hero section is not concerned by this setting only the page content.', 'soho'),
				'options'   => array(
                    'black' => esc_html__('Dark', 'soho'),
                    'header-light' => esc_html__('Light', 'soho')
                ),
                'default'   => 'black'
            ),
			
			array(
                'id'        => 'soho-opt-blog-footer-background',
                'type'      => 'select',
                'title'     => esc_html__('Footer background', 'soho'),
                'desc'      => esc_html__('The background of the footer for this post.', 'soho'),
				'options'   => array(
                    'footer-default' => esc_html__('Dark', 'soho'),
                    'alternate' => esc_html__('Light', 'soho')
                ),
                'default'   => 'footer-default'
            ),
			
			/**************************HERO SECTION OPTIONS**************************/
			array(
                'id'        	=> 'soho-opt-blog-logo-hero',
                'type'      	=> 'select',
                'title'     	=> esc_html__('Logo Type In Header Section', 'soho'),
                'desc'      	=> esc_html__('The type of logo displayed in header section. Depends on header\'s background', 'soho'),
				'options'   	=> array(
                    'logodark' 	=> esc_html__('Dark', 'soho'),
                    'negative' 	=> esc_html__('Light', 'soho')
                ),
				'default'   => 'logodark'
            ),
		
			// Image Hero
			array(
                'id'        => 'soho-opt-blog-hero-image',
                'type'      => 'media',
                'url'       => true,
                'title'     => esc_html__('Upload Header Image. If empty the featured image is going to be used.', 'soho'),
                'desc'      => '',
            ),
			
			// Options for image and video overlay color and caption
			array(
                'id'        => 'soho-opt-blog-hero-image-overlay-color',
                'type'      => 'color',
                'title'     => esc_html__('Hero Image Overlay Color', 'soho'),
                'subtitle'  => esc_html__('Pick an overlay color for your header image (default: #FFFFFF).', 'soho'),
                'transparent' => false,
                'default'   => '#FFFFFF',
                'validate'  => 'color',
            ),
			
            array(
                'id'            => 'soho-opt-blog-hero-image-overlay-color-opacity',
                'type'          => 'slider',
                'title'         => esc_html__('Header Image Overlay Color Opacity', 'soho'),
                'subtitle'      => esc_html__('Specifies the opacity. From 0.0 (fully transparent) to 1.0 (fully opaque)', 'soho'),
                'default'       => 0,
                'min'           => 0,
                'step'          => .1,
                'max'           => 1,
                'resolution'    => 0.1,
                'display_value' => 'text'
            ),
			
			array(
                'id'        => 'soho-opt-blog-hero-image-caption-position',
                'type'      => 'select',
                'title'     => esc_html__('Header Caption Position (title and categories)', 'soho'),
                'options'   => array(
                    'center-center' => 'Center-Center',
                    'center-left' => 'Center-Left',
                    'center-right' => 'Center-Right',
                    'bottom-center' => 'Bottom-Center',
                    'bottom-left' => 'Bottom-Left',
                    'bottom-right' => 'Bottom-Right',
                    'top-center' => 'Top-Center',
                    'top-left' => 'Top-Left',
                    'top-right' => 'Top-Right',
                ),
				'default'   => 'center-center'
            ),
			
			// General hero options
			array(
                 'id'       => 'soho-opt-blog-hero-scrolling-arrow',
                 'type'     => 'switch',
                 'title'    => esc_html__( 'Show Scrolling Icon', 'soho'),
                 'subtitle' => esc_html__( 'Shows the scroll down icon.', 'soho'),
                 'on'       => 'Yes',
				 'off'      => 'No',
				 'default'	=> true
            ),
            
			array(
                'id'        => 'soho-opt-blog-hero-height',
                'type'      => 'select',
                'title'     => esc_html__('Hero Height', 'soho'),
                'desc'      => esc_html__('Height of the "hero" section displayed as page header.', 'soho'),
				'options'   => array(
					'small' => esc_html__('Small', 'soho'),
                    'large' => esc_html__('Large', 'soho'),
                    'full'  => esc_html__('Full', 'soho')
                ),
                'default'   => 'full'
            ),
			
			array(
                'id'        => 'soho-opt-blog-hero-position',
                'type'      => 'select',
                'title'     => esc_html__('Header Position', 'soho'),
                'desc'      => esc_html__('Position of the header section displayed as page header.', 'soho'),
				'options'   => array(
                    'static' 	=> esc_html__('Static', 'soho'),
                    'relative' 	=> esc_html__('Relative', 'soho'),
					'parallax' 	=> esc_html__('Parallax', 'soho')
                ),
                'default'   => 'static'
            ),
			
			array(
                 'id'       => 'soho-opt-blog-hero-opacity',
                 'type'     => 'switch',
                 'title'    => esc_html__( 'Header Scrolling Opacity', 'soho'),
                 'subtitle' => esc_html__( 'Enable header section scrolling opacity.', 'soho'),
                 'on'       => 'Yes',
				 'off'      => 'No',
				 'default'	=> true
            ),
			
			array(
                'id'        => 'soho-opt-blog-hero-content',
                'type'      => 'select',
                'title'     => esc_html__('Header Content Type', 'soho'),
                'desc'      => esc_html__('Type of content in header section. Usually the opposite of the image background', 'soho'),
				'options'   => array(
                    'dark' => esc_html__('Dark', 'soho'),
                    'light' => esc_html__('Light', 'soho')
                ),
                'default'   => 'dark'
            ),
			 
			/**************************END - HERO SECTION OPTIONS**************************/

         )
    );

    $metaboxes[] = array(
       'id'            => 'clapat_' . SOHO_THEME_ID . '_post_options',
       'title'         => esc_html__( 'Post Options', 'soho'),
       'post_types'    => array( 'post' ),
       'position'      => 'normal', // normal, advanced, side
       'priority'      => 'high', // high, core, default, low
       'sidebar'       => false, // enable/disable the sidebar in the normal/advanced positions
       'sections'      => $blog_post_options,
    );


    $portfolio_options = array();
    $portfolio_options[] = array(

        'icon_class'    => 'icon-large',
        'icon'          => 'el-icon-wrench',
        'fields'        => array(

			array(
                'id'        => 'soho-opt-portfolio-header-background',
                'type'      => 'select',
                'title'     => esc_html__('Header background', 'soho'),
                'desc'      => esc_html__('The background of the header for this portfolio item page when scrolling the content. The logo will revert accordingly. The hero section is not concerned by this setting only the page content.', 'soho'),
				'options'   => array(
                    'black' => esc_html__('Dark', 'soho'),
                    'header-light' => esc_html__('Light', 'soho')
                ),
                'default'   => 'black'
            ),
			
			array(
                'id'        => 'soho-opt-portfolio-footer-background',
                'type'      => 'select',
                'title'     => esc_html__('Footer background', 'soho'),
                'desc'      => esc_html__('The background of the footer for this portfolio item page.', 'soho'),
				'options'   => array(
                    'footer-default' => esc_html__('Dark', 'soho'),
                    'alternate' => esc_html__('Light', 'soho')
                ),
                'default'   => 'footer-default'
            ),
			
			array(
                'id'        => 'soho-opt-portfolio-thumbnail-size',
                'type'      => 'select',
                'title'     => esc_html__('Thumbnail Size', 'soho'),
                'desc'      => esc_html__('Size of the thumbnail for this item as it appears in portfolio page. The thumbnail image is the featured image assigned for this item.', 'soho'),
				'options'   => array(
                    'normal' => esc_html__('Normal', 'soho'),
                    'wide' => esc_html__('Wide', 'soho'),
					'tall' => esc_html__('Tall', 'soho')
                ),
                'default'   => 'normal'
            ),
			
			array(
                'id'        => 'soho-opt-portfolio-popup-image',
                'type'      => 'media',
                'title'     => esc_html__('Upload Pop Up Image', 'soho'),
                'desc'      => esc_html__('If the portfolio page has lightbox popups layout. The pop up image is usually a higher resolution image shown with greater detail. The thumbnail image is the featured image assigned for this item.', 'soho')
            ),
            
            array(
                'id'        => 'soho-opt-portfolio-caption-bknd',
                'type'      => 'select',
                'title'     => esc_html__('Caption Background Type', 'soho'),
                'desc'      => esc_html__('Depending on the thumbnail background, the caption will have opposite foreground when hover type is set \'Permanent\' in portfolio page options.', 'soho'),
				'options'   => array(
                    'light-content' => esc_html__('Light Caption', 'soho'),
                    'dark-content' => esc_html__('Dark Caption', 'soho')
                ),
                'default'   => 'light-content'
            ),
			
			array(
                'id'        => 'soho-opt-portfolio-layout',
                'type'      => 'select',
                'title'     => esc_html__('Page Layout', 'soho'),
                'desc'      => esc_html__('Layout of the portfolio item or project page. The normal layout can have a hero section and any page content. The slider layout displays only the project slides.', 'soho'),
				'options'   => array(
                    'normal' => esc_html__('Normal', 'soho'),
                    'slider' => esc_html__('Slider', 'soho')
                ),
                'default'   => 'normal'
            ),
			
			array(
				'id'       => 'soho-opt-portfolio-slider-gallery',
				'type'     => 'gallery',
				'title'    => esc_html__('Add/Edit Slides Gallery', 'soho'),
				'desc' 	   => esc_html__('Create a new Slides Gallery by selecting existing or uploading new images using the WordPress native uploader', 'soho'),
				'required' 	=> array('soho-opt-portfolio-layout', '=', 'slider')
			),
			
			/**************************HERO SECTION OPTIONS**************************/
			array(
                'id'        => 'soho-opt-portfolio-hero-type',
                'type'      => 'select',
                'title'     => esc_html__('Hero Type', 'soho'),
                'desc'      => esc_html__('Type of the "hero" section displayed as page header.', 'soho'),
				'options'   => array(
                    'image' => esc_html__('Image', 'soho'),
                    'portfolio_slider' => esc_html__('Slider', 'soho'),
					'video' => esc_html__('Video', 'soho'),
                    'none' => esc_html__('None', 'soho'),
                ),
				'default'   => 'none',
				'required' 	=> array('soho-opt-portfolio-layout', '=', 'normal')
            ),
			
			array(
                'id'        	=> 'soho-opt-portfolio-logo-hero',
                'type'      	=> 'select',
                'title'     	=> esc_html__('Logo Type In Hero Section', 'soho'),
                'desc'      	=> esc_html__('The type of logo displayed in hero section. Depends on hero\'s background', 'soho'),
				'options'   	=> array(
                    'logodark' 	=> esc_html__('Dark', 'soho'),
                    'negative' 	=> esc_html__('Light', 'soho')
                ),
				'required' 	=> array('soho-opt-portfolio-hero-type', '!=', 'none'),
                'default'   => 'logodark'
            ),
		
			// Image Hero
			array(
                'id'        => 'soho-opt-portfolio-hero-image',
                'type'      => 'media',
                'required'  => array('soho-opt-portfolio-hero-type', '=', 'image'),
                'url'       => true,
                'title'     => esc_html__('Upload Hero Image', 'soho'),
                'desc'      => '',
            ),
			
		
			// Video hero
			array(
                 'id'       => 'soho-opt-portfolio-hero-video-url',
                 'type'     => 'text',
                 'title'    => esc_html__( 'Video ID Code', 'soho'),
                 'subtitle' => wp_kses_post( __( 'The ID of the video displayed in the hero section. Only youtube videos are allowed. You can find the video ID in the video URL after <b>v=</b>', 'soho') ),
                 'required' => array('soho-opt-portfolio-hero-type', '=', 'video'),
             ),
			
			array(
                'id'        => 'soho-opt-portfolio-hero-video-placeholder',
                'type'      => 'media',
                'required'  => array('soho-opt-portfolio-hero-type', '=', 'video'),
                'url'       => true,
                'title'     => esc_html__('Upload Video Placeholder Image', 'soho'),
                'desc'      => esc_html__('Video placeholder image will be displayed on top of the video when it is stopped', 'soho'),
				'required'  => array('soho-opt-portfolio-hero-type', '=', 'video'),
            ),
			
			// Options for image and video overlay color and caption
			array(
                'id'        => 'soho-opt-portfolio-hero-image-overlay-color',
                'type'      => 'color',
                'required'  => array('soho-opt-portfolio-hero-type', '=', array('image', 'video')),
                'title'     => esc_html__('Hero Image Overlay Color', 'soho'),
                'subtitle'  => esc_html__('Pick an overlay color for your hero image (default: #FFFFFF).', 'soho'),
                'transparent' => false,
                'default'   => '#FFFFFF',
                'validate'  => 'color',
            ),
			
            array(
                'id'            => 'soho-opt-portfolio-hero-image-overlay-color-opacity',
                'type'          => 'slider',
                'required'  	=> array('soho-opt-portfolio-hero-type', '=', array('image', 'video')),
                'title'         =>  esc_html__('Hero Image Overlay Color Opacity', 'soho'),
                'subtitle'      => esc_html__('Specifies the opacity. From 0.0 (fully transparent) to 1.0 (fully opaque)', 'soho'),
                'default'       => 0,
                'min'           => 0,
                'step'          => .1,
                'max'           => 1,
                'resolution'    => 0.1,
                'display_value' => 'text'
            ),
			
			array(
                'id'        => 'soho-opt-portfolio-hero-image-caption',
                'type'      => 'textarea',
				'required'  => array('soho-opt-portfolio-hero-type', '=', array('image', 'video')),
                'title'     => esc_html__('Hero Image Caption', 'soho'),
                'subtitle'  => esc_html__('Caption displayed over hero\'s image. HTML code allowed in this field.', 'soho'),
                'validate'  => 'html' //see http://codex.wordpress.org/Function_Reference/wp_kses_post
            ),
		
			array(
                'id'        => 'soho-opt-portfolio-hero-image-caption-position',
                'type'      => 'select',
                'title'     => esc_html__('Hero Image Caption Position', 'soho'),
                'options'   => array(
                    'center-center' => 'Center-Center',
                    'center-left' => 'Center-Left',
                    'center-right' => 'Center-Right',
                    'bottom-center' => 'Bottom-Center',
                    'bottom-left' => 'Bottom-Left',
                    'bottom-right' => 'Bottom-Right',
                    'top-center' => 'Top-Center',
                    'top-left' => 'Top-Left',
                    'top-right' => 'Top-Right',
                ),
				'required'  => array('soho-opt-portfolio-hero-type', '=', array('image', 'video')),
                'default'   => 'center-center'
            ),
			
			// General hero options
			array(
                 'id'       => 'soho-opt-portfolio-hero-scrolling-arrow',
                 'type'     => 'switch',
                 'title'    => esc_html__( 'Show Scrolling Icon', 'soho'),
                 'subtitle' => esc_html__( 'Shows the scroll down icon.', 'soho'),
                 'on'       => 'Yes',
				 'off'      => 'No',
				 'default'	=> true,
                 'required' => array('soho-opt-portfolio-hero-type', '!=', 'none')
            ),
            
			array(
                'id'        => 'soho-opt-portfolio-hero-content',
                'type'      => 'select',
                'title'     => esc_html__('Hero Content', 'soho'),
                'desc'      => esc_html__('Type of content in "hero" section.', 'soho'),
				'options'   => array(
                    'dark' => esc_html__('Dark', 'soho'),
                    'light' => esc_html__('Light', 'soho')
                ),
                'default'   => 'dark',
				'required'  => array('soho-opt-portfolio-hero-type', '!=', 'none' ),
            ),
			
			array(
                'id'        => 'soho-opt-portfolio-hero-height',
                'type'      => 'select',
                'title'     => esc_html__('Hero Height', 'soho'),
                'desc'      => esc_html__('Height of the "hero" section displayed as page header.', 'soho'),
				'options'   => array(
					'small' => esc_html__('Small', 'soho'),
                    'large' => esc_html__('Large', 'soho'),
                    'full'  => esc_html__('Full', 'soho')
                ),
                'default'   => 'full',
				'required'  => array( 'soho-opt-portfolio-hero-type', '!=', 'none' ),
            ),
			
			array(
                'id'        => 'soho-opt-portfolio-hero-position',
                'type'      => 'select',
                'title'     => esc_html__('Hero Position', 'soho'),
                'desc'      => esc_html__('Position of the "hero" section displayed as page header.', 'soho'),
				'options'   => array(
                    'static' 	=> esc_html__('Static', 'soho'),
                    'relative' 	=> esc_html__('Relative', 'soho'),
					'parallax' 	=> esc_html__('Parallax', 'soho')
                ),
                'default'   => 'static',
				'required'  => array( 'soho-opt-portfolio-hero-type', '!=', 'none' ),
            ),
			
			array(
                 'id'       => 'soho-opt-portfolio-hero-opacity',
                 'type'     => 'switch',
                 'title'    => esc_html__( 'Hero Scrolling Opacity', 'soho'),
                 'subtitle' => esc_html__( 'Enable hero section scrolling opacity.', 'soho'),
                 'on'       => 'Yes',
				 'off'      => 'No',
				 'default'	=> true,
                 'required' => array('soho-opt-portfolio-hero-type', '!=', 'none')
            ),
			
			array(
				'id'       => 'soho-opt-portfolio-hero-slider',
				'type'     => 'gallery',
				'title'    => esc_html__('Add/Edit Slides for Hero Section', 'soho'),
				'desc' 	   => esc_html__('Create a new Slides Gallery by selecting existing or uploading new images using the WordPress native uploader', 'soho'),
				'required'  => array('soho-opt-portfolio-hero-type', '=', 'portfolio_slider' ),
			),
			
			/**************************END - HERO SECTION OPTIONS**************************/
			
        ),
    );

    $metaboxes[] = array(
        'id'            => 'clapat_' . SOHO_THEME_ID . '_portfolio_options',
        'title'         => esc_html__( 'Portfolio Item Options', 'soho'),
        'post_types'    => array( 'soho_portfolio' ),
        'position'      => 'normal', // normal, advanced, side
        'priority'      => 'high', // high, core, default, low
        'sidebar'       => false, // enable/disable the sidebar in the normal/advanced positions
        'sections'      => $portfolio_options,
    );

    ////////////// Main Slider Options//////////////
    $slider_options = array();
    $slider_options[] = array(
        //'title'         => esc_html__('Add here a title if you want to show it as a section', 'soho'),
        'icon_class'    => 'icon-large',
        'icon'          => 'el-icon-wrench',
        'fields'        => array(

    		array(
                'id'        => 'soho-opt-slider-image',
                'type'      => 'media',
                'url'       => true,
                'title'     => esc_html__('Upload background image for the slide', 'soho'),
                'desc'      => '',
            ),
            array(
                'id'        => 'soho-opt-slider-bknd-repeat',
                'type'      => 'switch',
                'title'     => esc_html__('Background Repeat', 'soho'),
                'default'   => 0,
                'on'        => 'Yes',
                'off'       => 'No',
            ),
            array(
                'id'            => 'soho-opt-slider-overlay-color',
                'type'          => 'color',
                'title'         => esc_html__('Slide Overlay Color', 'soho'),
                'transparent'   => false,
                'default'       => '#FFFFFF',
                'validate'      => 'color',
            ),
            array(
                'id'            => 'soho-opt-slider-overlay-opacity',
                'type'          => 'slider',
                'title'         =>  esc_html__('Slide Overlay Color Opacity', 'soho'),
                'subtitle'      => esc_html__('Specifies the opacity. From 0.0 (fully transparent) to 1.0 (fully opaque)', 'soho'),
                'default'       => 0,
                'min'           => 0,
                'step'          => .1,
                'max'           => 1,
                'resolution'    => 0.1,
                'display_value' => 'text'
            ),
            array(
                'id'        => 'soho-opt-slider-caption-alignment',
                'type'      => 'select',
                'title'     => esc_html__('Slider Caption Alignment', 'soho'),
                'options'   => array(
                    'center-center' => 'Center-Center',
                    'center-left' => 'Center-Left',
                    'center-right' => 'Center-Right',
                    'bottom-center' => 'Bottom-Center',
                    'bottom-left' => 'Bottom-Left',
                    'bottom-right' => 'Bottom-Right',
                    'top-center' => 'Top-Center',
                    'top-left' => 'Top-Left',
                    'top-right' => 'Top-Right',
                ),
                'default'   => 'center-center'
            ),
            array(
                'id'        => 'soho-opt-slider-content-type',
                'type'      => 'radio',
                'title'     => esc_html__('Content type', 'soho'),
                'options'   => array(
                    'light' => esc_html__('Light', 'soho'),
                    'dark' => esc_html__('Dark', 'soho'),
                ),
                'default'   => 'dark'
            ),


        ),
    );

    $metaboxes[] = array(
        'id'            => 'clapat_' . SOHO_THEME_ID . '_main_slider_options',
        'title'         => esc_html__( 'Main Slider Options', 'soho'),
        'post_types'    => array(  'soho_main_slider' ),
        'position'      => 'normal', // normal, advanced, side
        'priority'      => 'high', // high, core, default, low
        'sidebar'       => false, // enable/disable the sidebar in the normal/advanced positions
        'sections'      => $slider_options,
    );
	
	////////////// Showcase Slider Options//////////////
    $showcase_slider_options = array();
    $showcase_slider_options[] = array(
        //'title'         => esc_html__('Add here a title if you want to show it as a section', 'soho'),
        'icon_class'    => 'icon-large',
        'icon'          => 'el-icon-wrench',
        'fields'        => array(

            array(
                'id'        => 'soho-opt-showcase-slide-type',
                'type'      => 'select',
                'title'     => esc_html__('Slide Type', 'soho'),
            	'options'   => array(
                    'image' => 'Image',
                    'video' => 'Video'
                ),
                'default'   => 'image'
            ),
    		array(
                'id'        => 'soho-opt-showcase-slider-image',
                'type'      => 'media',
                'url'       => true,
                'title'     => esc_html__('Upload background image for the slide', 'soho'),
                'desc'      => '',
    			'required'	=> array('soho-opt-showcase-slide-type', '=', 'image')
            ),
            array(
                'id'        => 'soho-opt-showcase-slider-bknd-repeat',
                'type'      => 'switch',
                'title'     => esc_html__('Background Repeat', 'soho'),
                'default'   => 0,
                'on'        => 'Yes',
                'off'       => 'No',
            	'required'	=> array('soho-opt-showcase-slide-type', '=', 'image')
            ),
            array(
                'id'        => 'soho-opt-showcase-slide-video-webm',
                'type'      => 'text',
                'title'     => esc_html__('Webm Video URL', 'soho'),
                'desc'   	=> esc_html__('URL of the slide background webm video. Webm format is previewed in Chrome and Firefox.', 'soho'),
                'required'	=> array('soho-opt-showcase-slide-type', '=', 'video')
            ),
			array(
                'id'        => 'soho-opt-showcase-slide-video-mp4',
                'type'      => 'text',
                'title'     => esc_html__('MP4 Video URL', 'soho'),
                'desc'   	=> esc_html__('URL of the slide background MP4 video. MP4 format is previewed in IE, Safari and other browsers.', 'soho'),
                'required'	=> array('soho-opt-showcase-slide-type', '=', 'video')
            ),
			array(
                'id'        => 'soho-opt-showcase-slide-video-image',
                'type'      => 'media',
                'url'       => true,
                'title'     => esc_html__('Background image on mobile devices', 'soho'),
                'required'	=> array('soho-opt-showcase-slide-type', '=', 'video')
            ),
            array(
                'id'            => 'soho-opt-showcase-slider-overlay-color',
                'type'          => 'color',
                'title'         => esc_html__('Slide Overlay Color', 'soho'),
                'transparent'   => false,
                'default'       => '#FFFFFF',
                'validate'      => 'color',
            ),
            array(
                'id'            => 'soho-opt-showcase-slider-overlay-opacity',
                'type'          => 'slider',
                'title'         =>  esc_html__('Slide Overlay Color Opacity', 'soho'),
                'subtitle'      => esc_html__('Specifies the opacity. From 0.0 (fully transparent) to 1.0 (fully opaque)', 'soho'),
                'default'       => 0,
                'min'           => 0,
                'step'          => .1,
                'max'           => 1,
                'resolution'    => 0.1,
                'display_value' => 'text'
            ),
            array(
                'id'        => 'soho-opt-showcase-slider-caption-alignment',
                'type'      => 'select',
                'title'     => esc_html__('Slider Caption Alignment', 'soho'),
                'options'   => array(
                    'center-center' => 'Center-Center',
                    'center-left' => 'Center-Left',
                    'center-right' => 'Center-Right',
                    'bottom-center' => 'Bottom-Center',
                    'bottom-left' => 'Bottom-Left',
                    'bottom-right' => 'Bottom-Right',
                    'top-center' => 'Top-Center',
                    'top-left' => 'Top-Left',
                    'top-right' => 'Top-Right',
                ),
                'default'   => 'center-center'
            ),
            array(
                'id'        => 'soho-opt-showcase-show-scroll',
                'type'      => 'switch',
                'title'     => esc_html__('Show Scrolling Arrow', 'soho'),
            	'desc'		=> esc_html__('Shows (or hides) scrolling arrow to the next (or previous, if last) slide', 'soho'),
                'default'   => 1,
                'on'        => 'Yes',
                'off'       => 'No'
            ),
            array(
                'id'        => 'soho-opt-showcase-slider-content-type',
                'type'      => 'radio',
                'title'     => esc_html__('Content type', 'soho'),
                'options'   => array(
                    'light' => esc_html__('Light', 'soho'),
                    'dark' => esc_html__('Dark', 'soho'),
                ),
                'default'   => 'dark'
            ),


        ),
    );

    $metaboxes[] = array(
        'id'            => 'clapat_' . SOHO_THEME_ID . '_showcase_slider_options',
        'title'         => esc_html__( 'Showcase Slider Options', 'soho'),
        'post_types'    => array(  'soho_showcase' ),
        'position'      => 'normal', // normal, advanced, side
        'priority'      => 'high', // high, core, default, low
        'sidebar'       => false, // enable/disable the sidebar in the normal/advanced positions
        'sections'      => $showcase_slider_options,
    );
    
	////////////// Showcase Alt Slider Options//////////////
    $showcase_alt_slider_options = array();
    $showcase_alt_slider_options[] = array(
        //'title'         => esc_html__('Add here a title if you want to show it as a section', 'soho'),
        'icon_class'    => 'icon-large',
        'icon'          => 'el-icon-wrench',
        'fields'        => array(

            array(
                'id'        => 'soho-opt-showcase-alt-slider-image',
                'type'      => 'media',
                'url'       => true,
                'title'     => esc_html__('Upload background image for the slide', 'soho'),
                'desc'      => '',
            ),
			array(
                'id'        => 'soho-opt-showcase-alt-slider-link-url',
                'type'      => 'text',
                'title'     => esc_html__('Link url', 'soho'),
				'subtitle'  => esc_html__('The url of the page this slide links to', 'soho'),
            ),
			array(
                'id'            => 'soho-opt-showcase-alt-slider-overlay-opacity',
                'type'          => 'slider',
                'title'         =>  esc_html__('Slide Overlay Color Opacity', 'soho'),
                'subtitle'      => esc_html__('Specifies the opacity. From 0.0 (fully transparent) to 1.0 (fully opaque)', 'soho'),
                'default'       => 0,
                'min'           => 0,
                'step'          => .1,
                'max'           => 1,
                'resolution'    => 0.1,
                'display_value' => 'text'
            ),
            
        ),
    );

    $metaboxes[] = array(
        'id'            => 'clapat_' . SOHO_THEME_ID . '_showcase_alt_slider_options',
        'title'         => esc_html__( 'Showcase Alt Slider Options', 'soho'),
        'post_types'    => array(  'soho_altshowcase' ),
        'position'      => 'normal', // normal, advanced, side
        'priority'      => 'high', // high, core, default, low
        'sidebar'       => false, // enable/disable the sidebar in the normal/advanced positions
        'sections'      => $showcase_alt_slider_options,
    );
	
    return $metaboxes;
  }

  add_action('redux/metaboxes/'.$redux_opt_name.'/boxes', 'soho_add_metaboxes');

}
