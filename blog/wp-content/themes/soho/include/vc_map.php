<?php

if ( function_exists( 'vc_map' ) ) {
	
vc_map( array(
	'name' => 'Title',
	'base' => 'title',
	'icon' => 'icon-vc-clapat-soho',
	'is_container' => 'true',
	'category' => esc_html__('Soho - Typo Elements', 'soho'),
	'description' => esc_html__('Title', 'soho'),
	'admin_enqueue_css' => array( get_template_directory_uri() . '/include/vc-extend.css' ),
	'params' => array(
		array(
			'type' => 'dropdown',
			'holder' => 'div',
			'heading' => esc_html__('Title Size', 'soho'),
			'description' => '',
			'param_name' => 'size',
			'value' => array( 'h1', 'h2', 'h3', 'h4', 'h5', 'h6'),
		),
		array(
			'type' => 'dropdown',
			'holder' => 'div',
			'heading' => esc_html__('Has Underline?', 'soho'),
			'description' => esc_html__('If the title is displayed underlined or without underline', 'soho'),
			'param_name' => 'underline',
			'value' => array( 'no', 'yes'),
		),
		array(
			'type' => 'dropdown',
			'holder' => 'div',
			'heading' => esc_html__('Big Title?', 'soho'),
			'description' => esc_html__('This parameter applies only for H1 headings. If the title is normal or bigger title font size.', 'soho'),
			'param_name' => 'big',
			'value' => array( 'no', 'yes'),
		),
		array(
			'type' => 'textarea_html',
			'holder' => 'div',
			'heading' => esc_html__('Content', 'soho'),
			'param_name' => 'content',
			'value' => esc_html__('Content goes here', 'soho'),
		),
	)
) );

vc_map( array(
	'name' => 'Line Divider',
	'base' => 'hr',
	'icon' => 'icon-vc-clapat-soho',
	'category' => esc_html__('Soho - Typo Elements', 'soho'),
	'description' => esc_html__('Line Divider', 'soho'),
	'params' => array(
	array(
		'type' => 'dropdown',
		'holder' => 'div',
		'heading' => esc_html__('Size', 'soho'),
		'description' => '',
		'param_name' => 'size',
		'value' => array( 'normal', 'small'),
	),
	)
) );

vc_map( array(
	'name' => 'Button',
	'base' => 'button',
	'icon' => 'icon-vc-clapat-soho',
	'is_container' => 'true',
	'category' => esc_html__('Soho - Typo Elements', 'soho'),
	'description' => esc_html__('Button', 'soho'),
	'params' => array(
		array(
			'type' => 'textfield',
			'holder' => 'div',
			'heading' => esc_html__('Button Link', 'soho'),
			'description' => esc_html__('URL for the button', 'soho'),
			'param_name' => 'link',
			'value' => 'http://',
		),
		array(
			'type' => 'dropdown',
			'holder' => 'div',
			'heading' => esc_html__('Target Window', 'soho'),
			'description' => esc_html__('Button link opens in a new or current window', 'soho'),
			'param_name' => 'target',
			'value' => array( '_blank', '_self'),
		),
		array(
			'type' => 'dropdown',
			'holder' => 'div',
			'heading' => esc_html__('Button type', 'soho'),
			'description' => '',
			'param_name' => 'type',
			'value' => array( 'normal', 'outline'),
		),
		array(
			'type' => 'textarea_html',
			'holder' => 'div',
			'heading' => esc_html__('Content', 'soho'),
			'param_name' => 'content',
			'value' => esc_html__('Content goes here', 'soho'),
		),
		)
) );

vc_map( array(
	'name' => 'Full Width Button',
	'base' => 'full_button',
	'icon' => 'icon-vc-clapat-soho',
	'is_container' => 'true',
	'category' => esc_html__('Soho - Typo Elements', 'soho'),
	'description' => esc_html__('Button covering whole screen width', 'soho'),
	'params' => array(
		array(
			'type' => 'textfield',
			'holder' => 'div',
			'heading' => esc_html__('Button Link', 'soho'),
			'description' => esc_html__('URL for the button', 'soho'),
			'param_name' => 'link',
			'value' => 'http://',
		),
		array(
			'type' => 'dropdown',
			'holder' => 'div',
			'heading' => esc_html__('Target Window', 'soho'),
			'description' => esc_html__('Button link opens in a new or current window', 'soho'),
			'param_name' => 'target',
			'value' => array( '_blank', '_self'),
		),
		array(
			'type' => 'textarea_html',
			'holder' => 'div',
			'heading' => esc_html__('Content', 'soho'),
			'param_name' => 'content',
			'value' => esc_html__('Content goes here', 'soho'),
		),
		)
) );

vc_map( array(
	'name' => 'Space Between Buttons',
	'base' => 'space_between_buttons',
	'icon' => 'icon-vc-clapat-soho',
	'category' => esc_html__('Soho - Typo Elements', 'soho'),
	'description' => esc_html__('Adds a space between two button shortcodes', 'soho'),
	'show_settings_on_create' => false
) );

vc_map( array(
	'name' => 'Animated Link',
	'base' => 'animated_link',
	'icon' => 'icon-vc-clapat-soho',
	'is_container' => 'true',
	'category' => esc_html__('Soho - Typo Elements', 'soho'),
	'description' => esc_html__('Link with animated arrow on hover', 'soho'),
	'params' => array(
		array(
			'type' => 'textfield',
			'holder' => 'div',
			'heading' => esc_html__('Link', 'soho'),
			'description' => esc_html__('URL of the link', 'soho'),
			'param_name' => 'link',
			'value' => 'http://',
		),
		array(
			'type' => 'dropdown',
			'holder' => 'div',
			'heading' => esc_html__('Target Window', 'soho'),
			'description' => esc_html__('Link opens in a new or current window', 'soho'),
			'param_name' => 'target',
			'value' => array( '_blank', '_self'),
		),
		array(
			'type' => 'textarea_html',
			'holder' => 'div',
			'heading' => esc_html__('Content', 'soho'),
			'param_name' => 'content',
			'value' => esc_html__('Content goes here', 'soho'),
		),
		)
) );

vc_map( array(
	'name' => 'Animated Text',
	'base' => 'animated_text',
	'icon' => 'icon-vc-clapat-soho',
	'is_container' => 'true',
	'category' => esc_html__('Soho - Typo Elements', 'soho'),
	'description' => esc_html__('Animated text with title and content', 'soho'),
	'params' => array(
		array(
			'type' => 'textfield',
			'holder' => 'div',
			'heading' => esc_html__('Title', 'soho'),
			'description' => esc_html__('Title for this animated block.', 'soho'),
			'param_name' => 'title',
			'value' => '',
		),
		array(
			'type' => 'textarea_html',
			'holder' => 'div',
			'heading' => esc_html__('Content', 'soho'),
			'param_name' => 'content',
			'value' => esc_html__('Content goes here', 'soho'),
		),
		)
) );

vc_map( array(
	'name' => 'Accordion',
	'base' => 'accordion',
	'icon' => 'icon-vc-clapat-soho',
	'as_parent' => array('only' => 'accordion_item'),'category' => esc_html__('Soho - Elements', 'soho'),
	'description' => esc_html__('Accordion', 'soho'),
	'content_element' => true,
	'show_settings_on_create' => false,
	"params" => array(
        // add params same as with any other content element
        array(
            "type" => "textfield",
            "heading" => esc_html__("Placeholder Parameter", 'soho' ),
            "param_name" => "accordion_placeholder_param",
			"value" => "Accordion Container",
            "description" => esc_html__("This is a placeholder parameter of the accordion container. It has no role or effect. Visual Composer does not display shortcodes without parameters.", 'soho')
        )
    ),
	'js_view' => 'VcColumnView'
) );

if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {class WPBakeryShortCode_accordion extends WPBakeryShortCodesContainer {}}

vc_map( array(
	'name' => 'Accordion Item',
	'base' => 'accordion_item',
	'icon' => 'icon-vc-clapat-soho',
	'as_child' => array('only' => 'accordion' ),'category' => esc_html__('Soho - Elements', 'soho'),
	'description' => esc_html__('Accordion Item', 'soho'),
	'content_element' => true,
	'params' => array(
		array(
			'type' => 'textfield',
			'holder' => 'div',
			'heading' => esc_html__('Title', 'soho'),
			'description' => '',
			'param_name' => 'title',
			'value' => 'Accordion Item Title',
		),
		array(
			'type' => 'textarea_html',
			'holder' => 'div',
			'heading' => esc_html__('Content', 'soho'),
			'param_name' => 'content',
			'value' => esc_html__('Content goes here', 'soho'),
		),
	)
) );

if ( class_exists( 'WPBakeryShortCode' ) ) {class WPBakeryShortCode_accordion_item extends WPBakeryShortCode {}}

vc_map( array(
	'name' => 'Toggle',
	'base' => 'toggle',
	'icon' => 'icon-vc-clapat-soho',
	'is_container' => 'true',
	'category' => esc_html__('Soho - Elements', 'soho'),
	'description' => esc_html__('Toggle', 'soho'),
	'params' => array(
		array(
			'type' => 'textfield',
			'holder' => 'div',
			'heading' => esc_html__('Title', 'soho'),
			'description' => '',
			'param_name' => 'title',
			'value' => '',
		),
		array(
			'type' => 'textarea_html',
			'holder' => 'div',
			'heading' => esc_html__('Content', 'soho'),
			'param_name' => 'content',
			'value' => esc_html__('Content goes here', 'soho'),
		),
	)
) );

vc_map( array(
	'name' => 'Alert Box',
	'base' => 'alert',
	'icon' => 'icon-vc-clapat-soho',
	'is_container' => 'true',
	'category' => esc_html__('Soho - Elements', 'soho'),
	'description' => esc_html__('Alert Box', 'soho'),
	'params' => array(
		array(
			'type' => 'dropdown',
			'holder' => 'div',
			'heading' => esc_html__('Color', 'soho'),
			'description' => esc_html__('Background color for the alert box', 'soho'),
			'param_name' => 'color',
			'value' => array( 'red', 'blue', 'yellow', 'green' ),
		),
		array(
			'type' => 'textarea_html',
			'holder' => 'div',
			'heading' => esc_html__('Content', 'soho'),
			'param_name' => 'content',
			'value' => esc_html__('Content goes here', 'soho'),
		),
	)
) );

vc_map( array(
	'name' => 'Progress Bars',
	'base' => 'progress',
	'icon' => 'icon-vc-clapat-soho',
	'as_parent' => array('only' => 'progress_bar'),'category' => esc_html__('Soho - Elements', 'soho'),
	'description' => esc_html__('Progress Bars', 'soho'),
	'content_element' => true,
	'show_settings_on_create' => false,
	"params" => array(
        // add params same as with any other content element
        array(
            "type" => "textfield",
            "heading" => esc_html__("Placeholder Parameter", 'soho'),
            "param_name" => "progress_placeholder_param",
			"value" => "Progress Container",
            "description" => esc_html__("This is a placeholder parameter of the progress container. It has no role or effect. Visual Composer does not display shortcodes without parameters.", 'soho')
        )
    ),
	'js_view' => 'VcColumnView'
) );

if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {class WPBakeryShortCode_progress extends WPBakeryShortCodesContainer {}}

vc_map( array(
	'name' => 'Progress Bar',
	'base' => 'progress_bar',
	'icon' => 'icon-vc-clapat-soho',
	'is_container' => 'true',
	'as_child' => array('only' => 'progress' ),'category' => esc_html__('Soho - Elements', 'soho'),
	'description' => esc_html__('Progress Bar', 'soho'),
	'params' => array(
		array(
			'type' => 'textfield',
			'holder' => 'div',
			'heading' => esc_html__('Percentage', 'soho'),
			'description' => esc_html__('Progress Bar Percentage', 'soho'),
			'param_name' => 'percentage',
			'value' => '100',
		),
		array(
			'type' => 'textarea_html',
			'holder' => 'div',
			'heading' => esc_html__('Content', 'soho'),
			'param_name' => 'content',
			'value' => esc_html__('Content goes here', 'soho'),
		),
	)
) );

if ( class_exists( 'WPBakeryShortCode' ) ) {class WPBakeryShortCode_progress_bar extends WPBakeryShortCode {}}

vc_map( array(
	'name' => 'Pricing Table',
	'base' => 'pricing_table',
	'icon' => 'icon-vc-clapat-soho',
	'as_parent' => array('only' => 'pricing_row'),'category' => esc_html__('Soho - Elements', 'soho'),
	'description' => esc_html__('Pricing Table', 'soho'),
	'content_element' => true,
	'show_settings_on_create' => true,
	'js_view' => 'VcColumnView',
	'params' => array(
		array(
			'type' => 'dropdown',
			'holder' => 'div',
			'heading' => esc_html__('Active', 'soho'),
			'description' => esc_html__('If the pricing table is highlighted or not', 'soho'),
			'param_name' => 'active',
			'value' => array( 'no', 'yes'),
		),
		array(
			'type' => 'textfield',
			'holder' => 'div',
			'heading' => esc_html__('Icon', 'soho'),
			'description' => esc_html__('Icon displayed at top of the price table', 'soho'),
			'param_name' => 'icon',
			'value' => '',
		),
		array(
			'type' => 'textfield',
			'holder' => 'div',
			'heading' => esc_html__('Title', 'soho'),
			'description' => esc_html__('Pricing table title. Usually the name of the category of services being priced', 'soho'),
			'param_name' => 'title',
			'value' => '',
		),
		array(
			'type' => 'textfield',
			'holder' => 'div',
			'heading' => esc_html__('Price', 'soho'),
			'description' => esc_html__('Price for the services being offered', 'soho'),
			'param_name' => 'price',
			'value' => '99.99',
		),
		array(
			'type' => 'textfield',
			'holder' => 'div',
			'heading' => esc_html__('Currency', 'soho'),
			'description' => esc_html__('Pricing table title. Usually the name of the category of services being priced', 'soho'),
			'param_name' => 'currency',
			'value' => '$',
		),
		array(
			'type' => 'textfield',
			'holder' => 'div',
			'heading' => esc_html__('Time', 'soho'),
			'description' => esc_html__('Period of time the price is applied for', 'soho'),
			'param_name' => 'time',
			'value' => 'per month',
		),
		array(
			'type' => 'textfield',
			'holder' => 'div',
			'heading' => esc_html__('Details URL', 'soho'),
			'description' => esc_html__('Url offering more details about the service(s)', 'soho'),
			'param_name' => 'url',
			'value' => 'http://',
		),
		array(
			'type' => 'dropdown',
			'holder' => 'div',
			'heading' => esc_html__('Details URL target', 'soho'),
			'description' => esc_html__('Target window for details URL', 'soho'),
			'param_name' => 'target',
			'value' => array( '_blank', '_self'),
		),
		array(
			'type' => 'textfield',
			'holder' => 'div',
			'heading' => esc_html__('Buy Now Caption', 'soho'),
			'description' => esc_html__('Caption or slogan for the button displayed at the bottom of the pricing table', 'soho'),
			'param_name' => 'buy_now_text',
			'value' => 'Buy Now',
		),
	)
) );

if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {class WPBakeryShortCode_pricing_table extends WPBakeryShortCodesContainer {}}

vc_map( array(
	'name' => 'Pricing Row',
	'base' => 'pricing_row',
	'icon' => 'icon-vc-clapat-soho',
	'is_container' => 'true',
	'as_child' => array('only' => 'pricing_table' ),'category' => esc_html__('Soho - Elements', 'soho'),
	'description' => esc_html__('Pricing Row', 'soho'),
	'params' => array(
		array(
			'type' => 'textarea_html',
			'holder' => 'div',
			'heading' => esc_html__('Content', 'soho'),
			'param_name' => 'content',
			'value' => esc_html__('Info about priced services here.', 'soho')
		),
	)
) );

if ( class_exists( 'WPBakeryShortCode' ) ) {class WPBakeryShortCode_pricing_row extends WPBakeryShortCode {}}

vc_map( array(
	'name' => 'Counters',
	'base' => 'counters',
	'icon' => 'icon-vc-clapat-soho',
	'as_parent' => array('only' => 'counter'),'category' => esc_html__('Soho - Elements', 'soho'),
	'description' => esc_html__('Counters', 'soho'),
	'content_element' => true,
	'show_settings_on_create' => false,
	"params" => array(
        // add params same as with any other content element
        array(
            "type" => "textfield",
            "heading" => esc_html__("Placeholder Parameter", 'soho'),
            "param_name" => "counter_placeholder_param",
			"value" => "Counters Container",
            "description" => esc_html__("This is a placeholder parameter of the counters container. It has no role or effect. Visual Composer does not display shortcodes without parameters.", 'soho')
        )
    ),
	'js_view' => 'VcColumnView'
) );

if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {class WPBakeryShortCode_counters extends WPBakeryShortCodesContainer {}}

vc_map( array(
	'name' => 'Counter Box',
	'base' => 'counter',
	'icon' => 'icon-vc-clapat-soho',
	'is_container' => 'true',
	'as_child' => array('only' => 'counters' ),'category' => esc_html__('Soho - Elements', 'soho'),
	'description' => esc_html__('Counter Box', 'soho'),
	'params' => array(
		array(
			'type' => 'textfield',
			'holder' => 'div',
			'heading' => esc_html__('Counts', 'soho'),
			'description' => esc_html__('Number of counts', 'soho'),
			'param_name' => 'counts',
			'value' => '100',
		),
		array(
			'type' => 'textarea_html',
			'holder' => 'div',
			'heading' => esc_html__('Content', 'soho'),
			'param_name' => 'content',
			'value' => esc_html__('Content goes here', 'soho'),
		),
	)
) );

if ( class_exists( 'WPBakeryShortCode' ) ) {class WPBakeryShortCode_counter extends WPBakeryShortCode {}}

vc_map( array(
	'name' => 'Radial Counter Box',
	'base' => 'radial_counter',
	'icon' => 'icon-vc-clapat-soho',
	'is_container' => 'true',
	'category' => esc_html__('Soho - Elements', 'soho'),
	'description' => esc_html__('Radial Counter Box', 'soho'),
	'params' => array(
		array(
			'type' => 'textfield',
			'holder' => 'div',
			'heading' => esc_html__('Counts', 'soho'),
			'description' => esc_html__('Number of counts', 'soho'),
			'param_name' => 'counts',
			'value' => '100',
		),
		array(
			'type' => 'textfield',
			'holder' => 'div',
			'heading' => esc_html__('Title', 'soho'),
			'description' => '',
			'param_name' => 'title',
			'value' => '',
		),
		array(
			'type' => 'textarea_html',
			'holder' => 'div',
			'heading' => esc_html__('Content', 'soho'),
			'param_name' => 'content',
			'value' => esc_html__('Content goes here', 'soho'),
		),
	)
) );

vc_map( array(
	'name' => 'Icon Service',
	'base' => 'service',
	'icon' => 'icon-vc-clapat-soho',
	'is_container' => 'true',
	'category' => esc_html__('Soho - Elements', 'soho'),
	'description' => esc_html__('Service Box', 'soho'),
	'params' => array(
		array(
			'type' => 'textfield',
			'holder' => 'div',
			'heading' => esc_html__('Icon', 'soho'),
			'description' => esc_html__('Icon displayed within service box. Type in the class of the icon in this edit box. The complete and latest list: http://fortawesome.github.io/Font-Awesome/icons/', 'soho'),
			'param_name' => 'icon',
			'value' => '',
		),
		array(
			'type' => 'textfield',
			'holder' => 'div',
			'heading' => esc_html__('Service Title', 'soho'),
			'description' => esc_html__('Title of the service', 'soho'),
			'param_name' => 'title',
			'value' => '',
		),
		array(
			'type' => 'textarea_html',
			'holder' => 'div',
			'heading' => esc_html__('Content', 'soho'),
			'param_name' => 'content',
			'value' => esc_html__('Content goes here', 'soho'),
		),
	)
) );

vc_map( array(
	'name' => 'Numbered Service',
	'base' => 'service_number',
	'icon' => 'icon-vc-clapat-soho',
	'is_container' => 'true',
	'category' => esc_html__('Soho - Elements', 'soho'),
	'description' => esc_html__('Service Box', 'soho'),
	'params' => array(
		array(
			'type' => 'textfield',
			'holder' => 'div',
			'heading' => esc_html__('Number', 'soho'),
			'description' => esc_html__('Number displayed within service box.', 'soho'),
			'param_name' => 'number',
			'value' => '',
		),
		array(
			'type' => 'textfield',
			'holder' => 'div',
			'heading' => esc_html__('Service Title', 'soho'),
			'description' => esc_html__('Title of the service', 'soho'),
			'param_name' => 'title',
			'value' => '',
		),
		array(
			'type' => 'textarea_html',
			'holder' => 'div',
			'heading' => esc_html__('Content', 'soho'),
			'param_name' => 'content',
			'value' => esc_html__('Content goes here', 'soho'),
		),
	)
) );

vc_map( array(
	'name' => 'Info Box',
	'base' => 'clapat_info_box',
	'icon' => 'icon-vc-clapat-soho',
	'is_container' => 'true',
	'category' => esc_html__('Soho - Elements', 'soho'),
	'description' => esc_html__('Info Box', 'soho'),
	'params' => array(
		array(
			'type' => 'textfield',
			'holder' => 'div',
			'heading' => esc_html__('Header Icon', 'soho'),
			'description' => esc_html__('Info box header icon. Type directly the name of the icon in the edit box. The complete and latest list: http://fortawesome.github.io/Font-Awesome/icons/', 'soho'),
			'param_name' => 'header_icon',
			'value' => '',
		),
		array(
			'type' => 'textfield',
			'holder' => 'div',
			'heading' => esc_html__('Footer', 'soho'),
			'description' => esc_html__('Info Box Footer Text', 'soho'),
			'param_name' => 'footer_text',
			'value' => '',
		),
		array(
			'type' => 'textarea_html',
			'holder' => 'div',
			'heading' => esc_html__('Info Box Content', 'soho'),
			'param_name' => 'content',
			'value' => esc_html__('Content goes here', 'soho'),
		),
	)
) );


vc_map( array(
	'name' => 'Video Popup',
	'base' => 'video_popup',
	'icon' => 'icon-vc-clapat-soho',
	'category' => esc_html__('Soho - Elements', 'soho'),
	'description' => esc_html__('Video Popup', 'soho'),
	'params' => array(
		array(
			'type' => 'textfield',
			'holder' => 'div',
			'heading' => esc_html__('Video URL', 'soho'),
			'description' => esc_html__('Url of the video to play in popup window', 'soho'),
			'param_name' => 'video_url',
			'value' => 'http://',
		)
	)
) );


vc_map( array(
	'name' => 'FontAwesome Icon',
	'base' => 'fontawesome_icon',
	'icon' => 'icon-vc-clapat-soho',
	'category' => esc_html__('Soho - Elements', 'soho'),
	'description' => esc_html__('FontAwesome Icon', 'soho'),
	'params' => array(
		array(
			'type' => 'textfield',
			'holder' => 'div',
			'heading' => esc_html__('Icon', 'soho'),
			'description' => esc_html__('Type in the class of the icon in this edit box. The complete and latest list: http://fortawesome.github.io/Font-Awesome/icons/', 'soho'),
			'param_name' => 'icon',
			'value' => '',
		),
		array(
			'type' => 'dropdown',
			'holder' => 'div',
			'heading' => esc_html__('Size', 'soho'),
			'description' => esc_html__('Icon size relative to their container. See http://fortawesome.github.io/Font-Awesome/examples/#larger for more information', 'soho'),
			'param_name' => 'size',
			'value' => array( 'none', 'lg', '2x', '3x', '4x', '5x'),
		),
		array(
			'type' => 'textfield',
			'holder' => 'div',
			'heading' => esc_html__('Color', 'soho'),
			'description' => esc_html__('Icon color', 'soho'),
			'param_name' => 'color',
			'value' => '#000000',
		),
	)
) );

vc_map( array(
	'name' => 'Lightbox Image Gallery',
	'base' => 'clapat_lightbox_gallery',
	'icon' => 'icon-vc-clapat-soho',
	'as_parent' => array('only' => 'clapat_lightbox_image'),
	'category' => esc_html__('Soho - Elements', 'soho'),
	'description' => esc_html__('Lightbox Image Gallery', 'soho'),
	'content_element' => true,
	'show_settings_on_create' => false,
	"params" => array(
        // add params same as with any other content element
        array(
            "type" => "textfield",
            "heading" => esc_html__("Placeholder Parameter", 'soho'),
            "param_name" => "lightbox_gallery_placeholder_param",
			"value" => "Lightbox Gallery Container",
            "description" => esc_html__("This is a placeholder parameter of the lightbox gallery container. It has no role or effect. Visual Composer does not display shortcodes without parameters.", 'soho')
        )
    ),
	'js_view' => 'VcColumnView'
) );

if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {class WPBakeryShortCode_clapat_lightbox_gallery extends WPBakeryShortCodesContainer {}}

vc_map( array(
	'name' => 'Lightbox Image',
	'base' => 'clapat_lightbox_image',
	'icon' => 'icon-vc-clapat-soho',
	'content_element' => true,
	'as_child' => array('only' => 'clapat_lightbox_gallery' ),
	'category' => esc_html__('Soho - Elements', 'soho'),
	'description' => esc_html__('Lightbox Image', 'soho'),
	'params' => array(
		array(
			'type' => 'attach_image',
			'holder' => 'div',
			'heading' => esc_html__('Thumbnail Image', 'soho'),
			'description' => esc_html__('Thumbnail image for lightbox popup', 'soho'),
			'param_name' => 'thumb_img_id',
			'value' => '',
		),
		array(
			'type' => 'attach_image',
			'holder' => 'div',
			'heading' => esc_html__('Pop up Image', 'soho'),
			'description' => esc_html__('Image representing lightbox popup', 'soho'),
			'param_name' => 'img_id',
			'value' => '',
		),
		array(
			'type' => 'textfield',
			'holder' => 'div',
			'heading' => esc_html__('Thumbnail Image ALT', 'soho'),
			'description' => esc_html__('The ALT attribute specifies an alternate text for an image, if the image cannot be displayed', 'soho'),
			'param_name' => 'alt',
			'value' => '',
		)
	)
) );

if ( class_exists( 'WPBakeryShortCode' ) ) {class WPBakeryShortCode_clapat_lightbox_image extends WPBakeryShortCode {}}

vc_map( array(
	'name' => 'Map',
	'base' => 'soho_map',
	'icon' => 'icon-vc-clapat-soho',
	'category' => esc_html__('Soho - Elements', 'soho'),
	'description' => esc_html__('Adds a google map with settings from theme options.', 'soho'),
	'show_settings_on_create' => false
) );

vc_map( array(
	'name' => 'Normal Image Slider',
	'base' => 'general_slider',
	'icon' => 'icon-vc-clapat-soho',
	'as_parent' => array('only' => 'general_slide'),'category' => esc_html__('Soho - Sliders', 'soho'),
	'description' => esc_html__('Normal Image Slider', 'soho'),
	'content_element' => true,
	'show_settings_on_create' => false,
	"params" => array(
        // add params same as with any other content element
        array(
            "type" => "textfield",
            "heading" => esc_html__("Placeholder Parameter", 'soho'),
            "param_name" => "slider_placeholder_param",
			"value" => "Image Sliders Container",
            "description" => esc_html__("This is a placeholder parameter of the image sliders container. It has no role or effect. Visual Composer does not display shortcodes without parameters.", 'soho')
        )
    ),
	'js_view' => 'VcColumnView'
) );

if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {class WPBakeryShortCode_general_slider extends WPBakeryShortCodesContainer {}}

vc_map( array(
	'name' => 'Slide',
	'base' => 'general_slide',
	'icon' => 'icon-vc-clapat-soho',
	'as_child' => array('only' => 'general_slider' ),'category' => esc_html__('Soho - Sliders', 'soho'),
	'description' => esc_html__('Slide', 'soho'),
	'params' => array(
		array(
			'type' => 'attach_image',
			'holder' => 'div',
			'heading' => esc_html__('Slider Image', 'soho'),
			'description' => esc_html__('Image representing this slide', 'soho'),
			'param_name' => 'img_id',
			'value' => '',
		),
		array(
			'type' => 'textfield',
			'holder' => 'div',
			'heading' => esc_html__('Image ALT', 'soho'),
			'description' => esc_html__('The ALT attribute specifies an alternate text for an image, if the image cannot be displayed', 'soho'),
			'param_name' => 'alt',
			'value' => '',
		),
	)
) );

if ( class_exists( 'WPBakeryShortCode' ) ) {class WPBakeryShortCode_general_slide extends WPBakeryShortCode {}}



vc_map( array(
	'name' => 'Carousel Slider',
	'base' => 'clapat_carousel',
	'icon' => 'icon-vc-clapat-soho',
	'as_parent' => array('only' => 'clapat_carousel_item'),
	'category' => esc_html__('Soho - Sliders', 'soho'),
	'description' => esc_html__('Carousel Slider', 'soho'),
	'content_element' => true,
	'show_settings_on_create' => false,
	"params" => array(
        // add params same as with any other content element
        array(
            "type" => "textfield",
            "heading" => esc_html__("Placeholder Parameter", 'soho'),
            "param_name" => "carousel_placeholder_param",
			"value" => "Carousel Slider Container",
            "description" => esc_html__("This is a placeholder parameter of the carousel slider container. It has no role or effect. Visual Composer does not display shortcodes without parameters.", 'soho')
        )
    ),
	'js_view' => 'VcColumnView'
) );

if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {class WPBakeryShortCode_clapat_carousel extends WPBakeryShortCodesContainer {}}

vc_map( array(
	'name' => 'Carousel Item',
	'base' => 'clapat_carousel_item',
	'icon' => 'icon-vc-clapat-soho',
	'as_child' => array('only' => 'clapat_carousel' ),
	'category' => esc_html__('Soho - Sliders', 'soho'),
	'description' => esc_html__('Carousel Item', 'soho'),
	'params' => array(
		array(
			'type' => 'attach_image',
			'holder' => 'div',
			'heading' => esc_html__('Carousel Item Image', 'soho'),
			'description' => esc_html__('Image representing this carousel item', 'soho'),
			'param_name' => 'img_id',
			'value' => '',
		),
		array(
			'type' => 'textfield',
			'holder' => 'div',
			'heading' => esc_html__('Carousel Item Links To', 'soho'),
			'description' => esc_html__('A link (url) this carousel item points to', 'soho'),
			'param_name' => 'link',
			'value' => '',
		),
		array(
			'type' => 'textfield',
			'holder' => 'div',
			'heading' => esc_html__('Category', 'soho'),
			'description' => esc_html__('Category name for this carousel item', 'soho'),
			'param_name' => 'category',
			'value' => '',
		),
		array(
			'type' => 'dropdown',
			'holder' => 'div',
			'heading' => esc_html__('Caption Type', 'soho'),
			'description' => '',
			'param_name' => 'caption_type',
			'value' => array( 'dark', 'light' ),
		),
		array(
			'type' => 'textarea_html',
			'holder' => 'div',
			'heading' => esc_html__('Title', 'soho'),
			'param_name' => 'content',
			'value' => esc_html__('Title for this carousel item', 'soho'),
		),
	)
) );

if ( class_exists( 'WPBakeryShortCode' ) ) {class WPBakeryShortCode_clapat_carousel_item extends WPBakeryShortCode {}}


vc_map( array(
	'name' => 'Collage',
	'base' => 'clapat_collage',
	'icon' => 'icon-vc-clapat-soho',
	'as_parent' => array('only' => 'clapat_collage_item'),
	'category' => esc_html__('Soho - Sliders', 'soho'),
	'description' => esc_html__('Collage', 'soho'),
	'content_element' => true,
	'show_settings_on_create' => false,
	"params" => array(
        // add params same as with any other content element
        array(
            "type" => "textfield",
            "heading" => esc_html__("Placeholder Parameter", 'soho'),
            "param_name" => "colage_placeholder_param",
			"value" => "Collage Container",
            "description" => esc_html__("This is a placeholder parameter of the collage container. It has no role or effect. Visual Composer does not display shortcodes without parameters.", 'soho')
        )
    ),
	'js_view' => 'VcColumnView'
) );

if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {class WPBakeryShortCode_clapat_collage extends WPBakeryShortCodesContainer {}}

vc_map( array(
	'name' => 'Collage Item',
	'base' => 'clapat_collage_item',
	'icon' => 'icon-vc-clapat-soho',
	'as_child' => array('only' => 'clapat_collage' ),
	'category' => esc_html__('Soho - Sliders', 'soho'),
	'description' => esc_html__('Collage Item', 'soho'),
	'params' => array(
		array(
			'type' => 'attach_image',
			'holder' => 'div',
			'heading' => esc_html__('Zoomed Image', 'soho'),
			'description' => esc_html__('Zoomed image - usually a higher res image', 'soho'),
			'param_name' => 'img_id',
			'value' => '',
		),
		array(
			'type' => 'attach_image',
			'holder' => 'div',
			'heading' => esc_html__('Thubnail Image', 'soho'),
			'description' => esc_html__('The thumbnail', 'soho'),
			'param_name' => 'thumb_id',
			'value' => '',
		),
		array(
			'type' => 'textarea_html',
			'holder' => 'div',
			'heading' => esc_html__('Title', 'soho'),
			'param_name' => 'content',
			'value' => esc_html__('Title for this collage item. It will be wrapped in a h5 header.', 'soho'),
		),
	)
) );

if ( class_exists( 'WPBakeryShortCode' ) ) {class WPBakeryShortCode_clapat_collage_item extends WPBakeryShortCode {}}


vc_map( array(
	'name' => 'Team Members',
	'base' => 'team',
	'icon' => 'icon-vc-clapat-soho',
	'as_parent' => array('only' => 'team_member'),
	'category' => __('Soho - Elements', 'soho'),
	'description' => __('Team Members', 'soho'),
	'content_element' => true,
	'show_settings_on_create' => false,
	"params" => array(
        // add params same as with any other content element
        array(
            "type" => "textfield",
            "heading" => __("Placeholder Parameter", 'soho'),
            "param_name" => "team_placeholder_param",
			"value" => "Team Members Container",
            "description" => __("This is a placeholder parameter of the team members container. It has no role or effect. Visual Composer does not display shortcodes without parameters.", 'soho')
        )
    ),
	'js_view' => 'VcColumnView'
) );

if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {class WPBakeryShortCode_team extends WPBakeryShortCodesContainer {}}

vc_map( array(
	'name' => 'Team Member',
	'base' => 'team_member',
	'icon' => 'icon-vc-clapat-soho',
	'is_container' => 'true',
	'as_child' => array('only' => 'team' ),
	'category' => __('Soho - Elements', 'soho'),
	'description' => __('Team Member', 'soho'),
	'params' => array(
		array(
			'type' => 'textfield',
			'holder' => 'div',
			'heading' => __('Team Member Name', 'soho'),
			'description' => '',
			'param_name' => 'name',
			'value' => '',
		),
		array(
			'type' => 'textfield',
			'holder' => 'div',
			'heading' => __('Job Title', 'soho'),
			'description' => '',
			'param_name' => 'title',
			'value' => '',
		),
		array(
			'type' => 'attach_image',
			'holder' => 'div',
			'heading' => __('Picture', 'soho'),
			'description' => __('Team member\'s picture', 'soho'),
			'param_name' => 'picture_id',
			'value' => '',
		),
		array(
			'type' => 'textfield',
			'holder' => 'div',
			'heading' => __('Social Icon 1', 'soho'),
			'description' => __('Social Icon. Type in the class of the icon in this edit box. The complete and latest list: http://fortawesome.github.io/Font-Awesome/icons/', 'soho'),
			'param_name' => 'social_icon1',
			'value' => '',
		),
		array(
			'type' => 'textfield',
			'holder' => 'div',
			'heading' => __('Social Link 1 URL', 'soho'),
			'description' => '',
			'param_name' => 'social_link1_url',
			'value' => '',
		),
		array(
			'type' => 'textfield',
			'holder' => 'div',
			'heading' => __('Social Icon 2', 'soho'),
			'description' => __('Social Icon. Type in the class of the icon in this edit box. The complete and latest list: http://fortawesome.github.io/Font-Awesome/icons/', 'soho'),
			'param_name' => 'social_icon2',
			'value' => '',
		),
		array(
			'type' => 'textfield',
			'holder' => 'div',
			'heading' => __('Social Link 2 URL', 'soho'),
			'description' => '',
			'param_name' => 'social_link2_url',
			'value' => '',
		),
		array(
			'type' => 'textfield',
			'holder' => 'div',
			'heading' => __('Social Icon 3', 'soho'),
			'description' => __('Social Icon. Type in the class of the icon in this edit box. The complete and latest list: http://fortawesome.github.io/Font-Awesome/icons/', 'soho'),
			'param_name' => 'social_icon3',
			'value' => '',
		),
		array(
			'type' => 'textfield',
			'holder' => 'div',
			'heading' => __('Social Link 3 URL', 'soho'),
			'description' => '',
			'param_name' => 'social_link3_url',
			'value' => '',
		),
		array(
			'type' => 'textarea_html',
			'holder' => 'div',
			'heading' => __('Content', 'soho'),
			'param_name' => 'content',
			'value' => __('Content goes here', 'soho'),
		),
	)
) );

if ( class_exists( 'WPBakeryShortCode' ) ) {class WPBakeryShortCode_team_member extends WPBakeryShortCode {}}

vc_map( array(
	'name' => 'Team Members - Carousel',
	'base' => 'team_carousel',
	'icon' => 'icon-vc-clapat-soho',
	'as_parent' => array('only' => 'team_member_carousel'),
	'category' => __('Soho - Elements', 'soho'),
	'description' => __('Team Members - Carousel', 'soho'),
	'content_element' => true,
	'show_settings_on_create' => false,
	"params" => array(
        // add params same as with any other content element
        array(
            "type" => "textfield",
            "heading" => __("Placeholder Parameter", 'soho'),
            "param_name" => "team_placeholder_param",
			"value" => "Team Members Container",
            "description" => __("This is a placeholder parameter of the team members container. It has no role or effect. Visual Composer does not display shortcodes without parameters.", 'soho')
        )
    ),
	'js_view' => 'VcColumnView'
) );

if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {class WPBakeryShortCode_team_carousel extends WPBakeryShortCodesContainer {}}

vc_map( array(
	'name' => 'Team Member - Carousel',
	'base' => 'team_member_carousel',
	'icon' => 'icon-vc-clapat-soho',
	'is_container' => 'true',
	'as_child' => array('only' => 'team' ),
	'category' => __('Soho - Elements', 'soho'),
	'description' => __('Team Member', 'soho'),
	'params' => array(
		array(
			'type' => 'textfield',
			'holder' => 'div',
			'heading' => __('Team Member Name', 'soho'),
			'description' => '',
			'param_name' => 'name',
			'value' => '',
		),
		array(
			'type' => 'textfield',
			'holder' => 'div',
			'heading' => __('Job Title', 'soho'),
			'description' => '',
			'param_name' => 'title',
			'value' => '',
		),
		array(
			'type' => 'attach_image',
			'holder' => 'div',
			'heading' => __('Picture', 'soho'),
			'description' => __('Team member\'s picture', 'soho'),
			'param_name' => 'picture_id',
			'value' => '',
		),
		array(
			'type' => 'textfield',
			'holder' => 'div',
			'heading' => __('Social Icon 1', 'soho'),
			'description' => __('Social Icon. Type in the class of the icon in this edit box. The complete and latest list: http://fortawesome.github.io/Font-Awesome/icons/', 'soho'),
			'param_name' => 'social_icon1',
			'value' => '',
		),
		array(
			'type' => 'textfield',
			'holder' => 'div',
			'heading' => __('Social Link 1 URL', 'soho'),
			'description' => '',
			'param_name' => 'social_link1_url',
			'value' => '',
		),
		array(
			'type' => 'textfield',
			'holder' => 'div',
			'heading' => __('Social Icon 2', 'soho'),
			'description' => __('Social Icon. Type in the class of the icon in this edit box. The complete and latest list: http://fortawesome.github.io/Font-Awesome/icons/', 'soho'),
			'param_name' => 'social_icon2',
			'value' => '',
		),
		array(
			'type' => 'textfield',
			'holder' => 'div',
			'heading' => __('Social Link 2 URL', 'soho'),
			'description' => '',
			'param_name' => 'social_link2_url',
			'value' => '',
		),
		array(
			'type' => 'textfield',
			'holder' => 'div',
			'heading' => __('Social Icon 3', 'soho'),
			'description' => __('Social Icon. Type in the class of the icon in this edit box. The complete and latest list: http://fortawesome.github.io/Font-Awesome/icons/', 'soho'),
			'param_name' => 'social_icon3',
			'value' => '',
		),
		array(
			'type' => 'textfield',
			'holder' => 'div',
			'heading' => __('Social Link 3 URL', 'soho'),
			'description' => '',
			'param_name' => 'social_link3_url',
			'value' => '',
		),
	)
) );

if ( class_exists( 'WPBakeryShortCode' ) ) {class WPBakeryShortCode_team_member_carousel extends WPBakeryShortCode {}}





vc_map( array(
	'name' => 'Clients',
	'base' => 'clients',
	'icon' => 'icon-vc-clapat-soho',
	'as_parent' => array('only' => 'client'),
	'category' => __('Soho - Elements', 'soho'),
	'description' => __('Clients', 'soho'),
	'content_element' => true,
	'show_settings_on_create' => false,
	"params" => array(
        // add params same as with any other content element
        array(
            "type" => "textfield",
            "heading" => __("Placeholder Parameter", 'soho'),
            "param_name" => "team_placeholder_param",
			"value" => "Team Members Container",
            "description" => __("This is a placeholder parameter of the clients container. It has no role or effect. Visual Composer does not display shortcodes without parameters.", 'soho')
        )
    ),
	'js_view' => 'VcColumnView'
) );

if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {class WPBakeryShortCode_clients extends WPBakeryShortCodesContainer {}}


vc_map( array(
	'name' => 'Clients - Carousel',
	'base' => 'clients_carousel',
	'icon' => 'icon-vc-clapat-soho',
	'as_parent' => array('only' => 'client'),
	'category' => __('Soho - Elements', 'soho'),
	'description' => __('Clients - Carousel', 'soho'),
	'content_element' => true,
	'show_settings_on_create' => false,
	"params" => array(
        // add params same as with any other content element
        array(
            "type" => "textfield",
            "heading" => __("Placeholder Parameter", 'soho'),
            "param_name" => "team_placeholder_param",
			"value" => "Team Members Container",
            "description" => __("This is a placeholder parameter of the team members container. It has no role or effect. Visual Composer does not display shortcodes without parameters.", 'soho')
        )
    ),
	'js_view' => 'VcColumnView'
) );

if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {class WPBakeryShortCode_clients_carousel extends WPBakeryShortCodesContainer {}}

vc_map( array(
	'name' => 'Client',
	'base' => 'client',
	'icon' => 'icon-vc-clapat-soho',
	'is_container' => 'true',
	'as_child' => array('only' => 'clients', 'only' => 'clients_carousel' ),
	'category' => __('Soho - Elements', 'soho'),
	'description' => __('Client', 'soho'),
	'params' => array(
		array(
			'type' => 'attach_image',
			'holder' => 'div',
			'heading' => __('Picture', 'soho'),
			'description' => __('Client\'s picture', 'soho'),
			'param_name' => 'picture_id',
			'value' => '',
		),
		array(
			'type' => 'textfield',
			'holder' => 'div',
			'heading' => __('Client URL', 'soho'),
			'description' => '',
			'param_name' => 'url',
			'value' => '',
		),
	)
) );

if ( class_exists( 'WPBakeryShortCode' ) ) {class WPBakeryShortCode_client extends WPBakeryShortCode {}}




vc_map( array(
	'name' => 'Testimonials',
	'base' => 'testimonials',
	'icon' => 'icon-vc-clapat-soho',
	'as_parent' => array('only' => 'testimonial'),
	'category' => esc_html__('Soho - Elements', 'soho'),
	'description' => esc_html__('Testimonials Carousel', 'soho'),
	'content_element' => true,
	'show_settings_on_create' => false,
	"params" => array(
        // add params same as with any other content element
        array(
            "type" => "textfield",
            "heading" => esc_html__("Placeholder Parameter", 'soho'),
            "param_name" => "slider_placeholder_param",
			"value" => "Image Sliders Container",
            "description" => esc_html__("This is a placeholder parameter of the image sliders container. It has no role or effect. Visual Composer does not display shortcodes without parameters.", 'soho')
        )
    ),
	'js_view' => 'VcColumnView'
) );

if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {class WPBakeryShortCode_testimonials extends WPBakeryShortCodesContainer {}}

vc_map( array(
	'name' => 'Testimonial',
	'base' => 'testimonial',
	'icon' => 'icon-vc-clapat-soho',
	'as_child' => array('only' => 'testimonials' ),
	'category' => esc_html__('Soho - Elements', 'soho'),
	'description' => esc_html__('Testimonial', 'soho'),
	'params' => array(
		array(
			'type' => 'textfield',
			'holder' => 'div',
			'heading' => esc_html__('Name', 'soho'),
			'description' => esc_html__('Name of the person or company this testimonial belongs to.', 'soho'),
			'param_name' => 'name',
			'value' => '',
		),
		array(
			'type' => 'textarea_html',
			'holder' => 'div',
			'heading' => __('Content', 'soho'),
			'param_name' => 'content',
			'value' => __('Content goes here', 'soho'),
		),
	)
) );

if ( class_exists( 'WPBakeryShortCode' ) ) {class WPBakeryShortCode_testimonial extends WPBakeryShortCode {}}

} // if vc_map function exists
?>