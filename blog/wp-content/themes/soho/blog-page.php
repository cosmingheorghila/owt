<?php
/*
Template name: Blog Template
*/
get_header();

while ( have_posts() ){

the_post();

?>
	<?php 
		
		// display header section
		get_template_part('sections/header_section'); 		
		
	?>
	
	<?php 
		
		// display search section
		get_template_part('sections/search_section'); 		
		
	?>
	
	<!-- Page Content -->
	<div id="page-content">
	<?php 
		
		// display hero section, if any
		$soho_hero_type = soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-page-hero-type' );
		if( $soho_hero_type != 'none' ){
		
			get_template_part('sections/hero_section'); 
		}
		
	?>
	<!-- Main -->
	<div id="main">
    	<div id="main-content" class="hidden">
			<!-- Blog -->
			<div id="blog">
		<?php 
				
			$soho_paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
			
			$soho_args = array(
				'post_type' => 'post',
				'paged' => $soho_paged
			);
			$soho_posts_query = new WP_Query( $soho_args );

			// the loop
			while( $soho_posts_query->have_posts() ){

				$soho_posts_query->the_post();

				get_template_part( 'sections/blog_post_section' );
				
			}
					
		?>
			<!-- /Blog -->
			</div>
			
		<?php
				
			soho_pagination( $soho_posts_query );

			wp_reset_postdata();
		?>	
		</div>
	</div>
    <!--/Main -->

<?php

}
	
get_footer();

?>
