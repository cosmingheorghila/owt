<?php

require_once ( get_template_directory() . '/include/defines.php' );

// Admin framework
if( class_exists( 'ReduxFramework' ) ){
	
	if( file_exists( get_template_directory() . '/extensions/loader.php' ) ) {
		
		// The loader will load all of the extensions automatically based on your $redux_opt_name
		require_once( get_template_directory() . '/extensions/loader.php' );
		
		// Metaboxes
		require_once ( get_template_directory() . '/include/metabox-config.php');
		
	}

	// Admin theme options
	if ( !isset( $soho_theme_options ) && file_exists( get_template_directory() . '/include/admin-config.php' ) ) {
		require_once( get_template_directory() . '/include/admin-config.php' );
	}
}
else {
	
	// Load the default options, looks like Redux has not been installed
	require_once( get_template_directory() . '/include/default-theme-options.php' );
}

if( !function_exists('soho_get_theme_options') ){
	
	function soho_get_theme_options( $option_id ){
		
		global $soho_theme_options;
		$option_value = "";
		if( isset( $soho_theme_options ) ){
				
			$option_value = $soho_theme_options[ $option_id ];
		}
			
		return $option_value;

	}
}

if( !function_exists('soho_get_post_meta') ){

	function soho_get_post_meta( $opt_name = "", $thePost = array(), $meta_key = "", $def_val = "" ) {
			
		if( !function_exists('redux_post_meta') ){
			
			global $soho_theme_options;
			return $soho_theme_options[ $meta_key ];
		}
		else {
			
			return redux_post_meta( $opt_name, $thePost, $meta_key, $def_val );
		}
		
		return "";
	}
}

if( !function_exists('soho_get_theme_options') ){
	
	function soho_get_theme_options( $option_id ){
		
		global $soho_theme_options;
		$option_value = "";
		if( isset( $soho_theme_options ) ){
				
			$option_value = $soho_theme_options[ $option_id ];
		}
			
		return $option_value;

	}
}

if( !function_exists('soho_get_current_query') ){
	
	function soho_get_current_query(){
		
		global $soho_posts_query;
		global $wp_query;
		if( $soho_posts_query == null ){
			return $wp_query;
		}
		else{
			return $soho_posts_query;
		}
			
	}
}

if( !function_exists('soho_get_hover_effect') ){
	
	function soho_get_hover_effect(){
		
		global $soho_hover_effect;
		return $soho_hover_effect;
	}
}

// Hero properties
require_once ( get_template_directory() . '/include/hero-properties.php');
if( !function_exists('soho_get_hero_properties') ){
	
	function soho_get_hero_properties(){
		
		global $soho_hero_properties;
		if( !isset( $soho_hero_properties ) || ( $soho_hero_properties == null ) ){
			$soho_hero_properties = new SohoHeroProperties();
		}
		return $soho_hero_properties;
	}
}

// Page header background
if( !function_exists('soho_header_background') ){
	
	function soho_header_background( $background = null ){
		
		static $soho_header_background = "dark";
		
		if( $background != null ){
			
			$soho_header_background = $background;
		}
		
		return $soho_header_background;
	}
}

// Support for automatic plugin installation
require_once(  get_template_directory() . '/components/helper_classes/tgm-plugin-activation/class-tgm-plugin-activation.php');
require_once(  get_template_directory() . '/components/helper_classes/tgm-plugin-activation/required_plugins.php');

// Widgets
require_once(  get_template_directory() . '/components/widgets/widgets.php');

// Util functions
require_once ( get_template_directory() . '/include/util_functions.php');

// Add theme support
require_once ( get_template_directory() . '/include/theme-support-config.php');

// Theme setup
require_once ( get_template_directory() . '/include/setup-config.php');

// Enqueue scripts
require_once ( get_template_directory() . '/include/scripts-config.php');

// Hooks
require_once ( get_template_directory() . '/include/hooks-config.php');

// Blog comments and pagination
require_once ( get_template_directory() . '/include/blog-config.php');

// Visual Composer
if ( function_exists( 'vc_set_as_theme' ) ) {
	require_once ( get_template_directory() . '/include/vc-config.php');
}

?>