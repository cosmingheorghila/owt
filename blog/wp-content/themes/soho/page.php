<?php

get_header();

while ( have_posts() ){

the_post();

?>
		<?php 
		
		// display header section
		get_template_part('sections/header_section'); 		
		
		?>
				
		<!-- Page Content -->
		<div id="page-content">
		
		<?php
		
		// display hero section, if any
		$soho_hero_type = soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-page-hero-type' );
		if( $soho_hero_type != 'none' ){
		
			get_template_part('sections/hero_section'); 
		}
		
		?>
			<!-- Main --> 
			<div id="main">
				<div id="main-content" class="hidden">
					
					<?php if ( !function_exists( 'vc_set_as_theme' ) ) { // if Visual Composer is not installed, add a container?>
					<div class="container no-composer">
					<?php } ?>
					<?php the_content(); ?>
					<?php if ( !function_exists( 'vc_set_as_theme' ) ) { // if Visual Composer is not installed, add a container?>
					</div>
					<?php } ?>
					
				</div>
			</div>
			<!--/Main -->

<?php
			// any comments?
			if ( $post->comment_count > 0 ){
				
				comments_template();
			
			}

}
	
get_footer();

?>