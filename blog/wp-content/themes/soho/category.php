<?php
/**
 * The template for displaying Category Search Results pages
 */

get_header();

?>
	<?php 
		
		// display header section
		get_template_part('sections/header_section'); 		
		
	?>
	
	<?php 
		
		// display search section
		get_template_part('sections/search_section'); 		
		
	?>
	
	<!-- Page Content -->
	<div id="page-content">
	
	<div class="blog-title-container hidden">
		<h1 class="search_results"><?php  single_cat_title(); ?></h1>
	</div>
	
	<!-- Main -->
	<div id="main">
    	<div id="main-content" class="hidden">
			<!-- Blog -->
			<div id="blog">
		<?php 
				
			// the loop
			if( have_posts() ){
			
				while( have_posts() ){

					the_post();

					get_template_part( 'sections/blog_post_section' );
					
				}
			}
			else {
				
				echo '<h4 class="search_results">' . esc_html__('No posts found in this category', 'soho') . '</h4>';
			}
		?>
		
			<!-- /Blog -->
			</div>
			<?php
				
			soho_pagination();

			?>
		</div>
	</div>
    <!--/Main -->

<?php

get_footer();

?>