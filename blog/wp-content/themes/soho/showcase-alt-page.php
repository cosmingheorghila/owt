<?php
/*
Template name: Showcase Alt Template
*/
get_header();

require_once ( get_template_directory() . '/include/util_functions.php');

while ( have_posts() ){

the_post();

?>
		<?php 
		
		// display header section
		get_template_part('sections/header_section'); 		
		
		?>
		
		
		<!-- Page Content -->
		<div id="page-content" class="transparent-footer">
		
			<!-- Showcase -->
			<div id="showcase" class="hidden">
			
				<!-- Showcase Images-->
				<ul class="showcase-images">

<?php		
			$soho_args = array(
				'post_type' => 'soho_altshowcase',
				'orderby'   => 'menu_order',
				'order'     => 'ASC',
				'posts_per_page' => -1
			);

			$soho_query_slides = new WP_Query( $soho_args );

			$soho_slide_content 	= '';
			if( $soho_query_slides->have_posts() ){
				
				while ( $soho_query_slides->have_posts() ) {
					
					$soho_query_slides->the_post();

					$soho_image = soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-showcase-alt-slider-image');
					$soho_image_path   = trim( $soho_image['url'] );
					
					$soho_link_url = soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-showcase-alt-slider-link-url');
					
					$soho_overlay_color   = '#000000';
					$soho_overlay_opacity = soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-showcase-alt-slider-overlay-opacity' );
					$soho_overlay_rgba    = clapat_hex2rgba( $soho_overlay_color, $soho_overlay_opacity );
					
					echo '<li style="background-image: url(' . esc_url( $soho_image_path ) . ');"><div class="overlay" style="background-color:' . esc_attr( $soho_overlay_rgba ) . '"></div></li>';
					$soho_slide_content  .= '<li class="showcase-name"><a class="animation-link " href="' . esc_url( $soho_link_url ) . '"><div class="sh-name">' . get_the_title() . '</div><div class="sh-view">' . get_the_content() . '</div></a></li>';  
				}
			}

			wp_reset_postdata();
			
			$soho_showcase_title 			= soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-page-showcase-alt-title');
			$soho_showcase_link 			= soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-page-showcase-alt-url');
			$soho_showcase_link_caption 	= soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-page-showcase-alt-url-caption');
?>
	
				</ul>
				<!--/Showcase Images-->
				
			</div>
			<!--/Showcase -->
			
			<!-- Showcase Name-->	
			<div class="showcase-list light-content">
				<div class="showcase-line"></div>
						
				<p class="showcase-title smaller"><?php echo wp_kses_post( $soho_showcase_title ); ?></p> 
											   
				<hr>
						
				<ul class="showcase-wrapper"> 
					<?php echo wp_kses_post( $soho_slide_content ); ?>                   
				</ul>
						
				<hr><br><br><br>
						
				<?php if( $soho_showcase_link ){ ?>
				<a class="clapat-button view-works outline-button animation-link " href="<?php echo esc_url( $soho_showcase_link ); ?>"><?php echo wp_kses_post( $soho_showcase_link_caption ); ?></a>
				<?php } ?>
			</div>
			<!--/Showcase Name-->
<?php

}
	
get_footer();

?>