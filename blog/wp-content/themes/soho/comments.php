<?php
		
	if ( post_password_required() ) { ?>
		<div class="bottom-post"><h4><?php esc_html_e('This post is password protected. Enter the password to view comments.', 'soho'); ?></h4></div>
	<?php
		return;
	}
        
?>



	
<!-- Article Discusion -->
<div id="comments" class="article-discusion">
	<div class="article-discusion-container">
		<h3><?php comments_number(esc_html__('No Comments', 'soho'), esc_html__('One Comment', 'soho'), esc_html__('% Comments', 'soho'));?></h3>
			
		<?php if ( have_comments() ) { ?>

			<div class="comments-navigation">
				<div class="alignleft"><?php previous_comments_link(); ?></div>
				<div class="alignright"><?php next_comments_link(); ?></div>
			</div>

			<?php wp_list_comments('callback=soho_comment&style=div'); ?>
					
			<div class="comments-navigation">
				<div class="alignleft"><?php previous_comments_link(); ?></div>
				<div class="alignright"><?php next_comments_link(); ?></div>
			</div>

		<?php } // if have comments ?>

		<?php if ( !comments_open() ) { ?>
		<!-- If comments are closed. -->
		<p><?php esc_html_e('Comments are closed.', 'soho'); ?></p>
		<?php } ?>
	</div>
</div>
<!-- Article Discusion -->




<?php if ( comments_open() ) { ?>

	<!-- Post Comments Formular -->
	<div class="article-formular">
		<div class="article-formular-container">

<?php  

        $class_message_area = '';
        if( is_user_logged_in() ){

            $class_message_area = 'comment_area_loggedin';
        }

        $comment_id = "comment";
                            
        $args = array(
                        'id_form'           => 'commentsform',
                        'id_submit'         => 'submit',
                        'title_reply'       => wp_kses_post( __( '<h3>Leave a comment</h3>', 'soho') ),
                        'title_reply_to'    => wp_kses_post( __( '<h3>Leave a comment to %s </h3>', 'soho') ),
                        'cancel_reply_link' => wp_kses_post( __( '<h4>Cancel Reply</h4>', 'soho') ),
                        'label_submit'      => wp_kses_post( __( 'Post Comment', 'soho') ),

                        'comment_field' =>  '<div class="post-comments-textarea ' . $class_message_area . '"><textarea id="' . $comment_id . '" name="comment" onfocus="if(this.value == \'Comment\') { this.value = \'\'; }" onblur="if(this.value == \'\') { this.value = \'Comment\'; }" >Comment</textarea><label class="input_label slow"></label></div>',

                        'must_log_in' => '<p class="must-log-in">' .
                                         sprintf(
                                        wp_kses_post( __( '<h4>You must be <a href="%s">logged in</a> to post a comment.</h4>', 'soho') ),
                                        wp_login_url( apply_filters( 'the_permalink', get_permalink() ) )
                                        ) . '</p>',
            
                        'comment_notes_before' => '',

                        'comment_notes_after' => '',
            
                        'fields' => apply_filters( 'comment_form_default_fields', array(

                                                    'author' => '<div class="one_half"><input name="author" type="text" id="author" size="30"  onfocus="if(this.value == \'Name\') { this.value = \'\'; }" onblur="if(this.value == \'\') { this.value = \'Name\'; }" value=\'Name\' ><label class="input_label"></label></div>',

                                                    'email' =>  '<div class="one_half last"><input name="email" type="text" id="email" size="30"  onfocus="if(this.value == \'E-mail\') { this.value = \'\'; }" onblur="if(this.value == \'\') { this.value = \'E-mail\'; }" value=\'E-mail\' ><label class="input_label"></label></div>',

                                                    'url' => ''
                                                    )
                                                )
                    );
                
        comment_form( $args );

?>
			</div>
        <!-- /Post Comments Formular -->
        </div>

<?php

} // if comments are open
?>