<?php 

get_header(); 

// display header section
get_template_part('sections/header_section'); 	
	
?>
		
	<div id="hero" class="hidden">
		<div class="clapat-caption animated">
			<div class="caption-content center-center">
				<h1 class="big-title"><?php echo wp_kses( soho_get_theme_options('clapat_soho_error_title'), wp_kses_allowed_html( 'post' ) ); ?></h1>
				<p><?php echo wp_kses_post ( soho_get_theme_options('clapat_soho_error_info') ); ?></p>
				<a class="clapat-button" href="<?php echo esc_url( soho_get_theme_options('clapat_soho_error_back_button_url') ); ?>">
					<?php echo wp_kses( soho_get_theme_options('clapat_soho_error_back_button'), wp_kses_allowed_html( 'post' ) ); ?>
				</a>
			</div>
        </div>
    </div>

<?php 

get_footer(); 

?>