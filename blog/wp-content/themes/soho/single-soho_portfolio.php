<?php

get_header();

while ( have_posts() ){

	the_post();
			
	$soho_layout_type = soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-portfolio-layout' );
	if( $soho_layout_type == 'slider' ){
			
		get_template_part('sections/project_slider_section'); 
		
	} else { // just 'normal' layout
		
		get_template_part('sections/project_normal_section'); 
	}

}

	
get_footer();

?>

