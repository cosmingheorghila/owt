<?php
/**
 * Created by Clapat.
 * Date: 22/03/16
 * Time: 11:54 AM
 */

$soho_footer_layout = '';
// retrieve the footer background for this post
$post_type = get_post_type();
if( $post_type == 'soho_portfolio' ){
	
	$soho_footer_layout = soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-portfolio-footer-background' );
	
} else if( $post_type == 'post' ){
	
	$soho_footer_layout = soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-blog-footer-background' );
	
} else if( $post_type == 'page' ){
	
	$soho_footer_layout = soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-page-footer-background' );
		
} else {
	
	$soho_footer_layout = 'footer-default';
}

?>
	<!-- Footer -->
    <footer class="<?php echo sanitize_html_class( $soho_footer_layout ); ?> hidden">
		<div id="footer-container">
            <div class="outer">        
				<div class="inner"> 
					<?php get_template_part('sections/footer_content_section'); ?>
				</div>
			</div>
		</div>	
	</footer>
	<!--/Footer -->
</div>    
<!--/Page Content -->

<?php wp_footer(); ?>
</body>
</html>