<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>

    <meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
 
	<?php
	// if the `wp_site_icon` function does not exist (ie we're on < WP 4.3)
	$bClapatShowOptionsIcon = false;
	if ( ! function_exists( 'wp_site_icon' ) ) {
		// show the user your favicon theme option
		$bClapatShowOptionsIcon = true;
	}
	else {
		if ( ! ( function_exists( 'has_site_icon' ) && has_site_icon() ) ) {
			// output our theme options favicon to the page source
			$bClapatShowOptionsIcon = true;
		}
	}
	if( $bClapatShowOptionsIcon ) {
	?>	
	<link rel="shortcut icon" href="<?php echo get_template_directory_uri()."/images/favicon.ico"; ?>">
	<?php } ?>
	
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

	<div id="black-fade" class="display-hide"></div>