<?php
/*
Template name: Portfolio Mixed Template
*/

get_header();

while ( have_posts() ){

the_post();

$soho_columns 			= soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-page-portfolio-columns' );
$soho_show_filters		= soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-page-portfolio-show-filters' );
$soho_hover_effect		= soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-page-portfolio-hover-effect' );
$soho_margins			= soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-page-portfolio-margins' ) ? '' : 'no-gutter';
$soho_use_lightbox		= soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-page-portfolio-lightbox' );
$soho_max_items			= soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-page-portfolio-mixed-items' );
if( empty($soho_max_items) ){
	$soho_max_items = 1000;
}

$soho_filters_class = '';
if( $soho_show_filters ){
	
	$soho_filters_class = 'is-active';

}

?>

		<?php 
		
		// display header section
		get_template_part('sections/header_section'); 		
		
		?>
		
		
		<!-- Page Content -->
		<div id="page-content">
		
		<?php
		
		// display hero section, if any
		$soho_hero_type = soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-page-hero-type' );
		if( $soho_hero_type != 'none' ){
		
			get_template_part('sections/hero_section'); 
		}
		
		?>
			<!-- Main --> 
			<div id="main">
				<div id="main-content" class="hidden">
					<!-- Container -->
					<div class="portfolio">
						<!-- Portfolio Filters --> 
						<ul id="filters" class="<?php echo sanitize_html_class( $soho_filters_class ); ?>">
							<li class="filter-line"></li>
							<li><a id="all" href="#" data-filter="*" class="active"><?php esc_html_e('All', 'soho'); ?></a></li>	
							<?php
							$soho_portfolio_category = get_terms('portfolio_category', array( 'hide_empty' => 0 ));

							if($soho_portfolio_category){

								foreach($soho_portfolio_category as $portfolio_cat){
							?>
							<li><a href="#" data-filter=".<?php echo sanitize_title( $portfolio_cat->slug ); ?>"><?php echo wp_kses_post( $portfolio_cat->name ); ?></a></li>
							<?php
								}
							}
							?>
						</ul>
						<!--/Portfolio Filters -->
						<!-- Portfolio -->
						<div id="portfolio-wrap" class="<?php echo esc_attr( $soho_margins ); ?>">
							<div id="portfolio" <?php if( $soho_use_lightbox ){ echo 'class="mfp-gallery"'; } ?> data-col="<?php echo esc_attr( $soho_columns ); ?>">
							<?php
							
								$soho_paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
								$soho_args = array(
											'post_type' => 'soho_portfolio',
											'paged' => $soho_paged,
											'posts_per_page' => $soho_max_items,
										 );
							
								$soho_portfolio = new WP_Query( $soho_args );

								while( $soho_portfolio->have_posts() ){
								
									$soho_portfolio->the_post();
								
									if( $soho_use_lightbox ){
										
										get_template_part('sections/portfolio_section_lightbox_item');
									}
									else {
									
										get_template_part('sections/portfolio_section_item');
									}
									
								}
							
								wp_reset_postdata();
							
							?>
							</div>
						</div>
						<!--/Portfolio -->
					</div> 
					<!--/Container -->
					<?php if ( !function_exists( 'vc_set_as_theme' ) ) { // if Visual Composer is not installed, add a container?>
					<div class="container no-composer">
					<?php } ?>
					<?php the_content(); ?>
					<?php if ( !function_exists( 'vc_set_as_theme' ) ) { // if Visual Composer is not installed, add a container?>
					</div>
					<?php } ?>
				</div>
			</div>
			<!--/Main -->					
		
<?php

}
	
get_footer();

?>
