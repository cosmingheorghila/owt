<?php

if( !function_exists('soho_render_menu_social_links') ){

	function soho_render_menu_social_links(){
		
		global $soho_theme_options;
		global $soho_social_links;

		echo '<ul class="socials">';

		for( $idx = 1; $idx <= SOHO_MAX_SOCIAL_LINKS; $idx++ ){
			
			$social_name = $soho_theme_options['clapat_soho_footer_social_' . $idx];
			$social_url  = $soho_theme_options['clapat_soho_footer_social_url_' . $idx];
			
			if( $social_url ){
			   
				echo '<li><a href="' . esc_url( $social_url ) . '" target="_blank"><i class="fa fa-' . esc_attr( $social_name ) . '"></i></a></li>';
			}
			
		}

		echo '</ul>';
	}

}
		
?>
	<div class="menu-footer">
		<p class="copyright"><?php echo wp_kses_post( soho_get_theme_options('clapat_soho_footer_copyright') ); ?></p>
		<?php soho_render_menu_social_links(); ?>
	</div>