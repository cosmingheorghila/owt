<?php
/**
 * Created by Clapat.
 * Date: 22/03/16
 * Time: 11:54 AM
 */
?>

		<p class="copyright"><?php echo wp_kses_post( soho_get_theme_options('clapat_soho_footer_copyright') ); ?></p>
		<div class="scrolltotop hidden"><span class="holder"></span></div>
		<div class="icon-scroll-line"></div>
<?php					
get_template_part('sections/footer_social_links_section');
?>
