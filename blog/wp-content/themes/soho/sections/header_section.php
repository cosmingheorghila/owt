<?php

// retrieve the path to the logo displayed in the menu bar
$soho_logo_light = soho_get_theme_options( 'clapat_soho_logo_light' );
$soho_logo_light_path = $soho_logo_light['url'];
if( !$soho_logo_light_path ){
    $soho_logo_light_path = get_template_directory_uri() . "/images/logo.png";
}
$soho_logo_dark = soho_get_theme_options( 'clapat_soho_logo_dark' );
$soho_logo_dark_path = $soho_logo_dark['url'];
if( !$soho_logo_dark_path ){
    $soho_logo_dark_path = get_template_directory_uri() . "/images/logo-black.png";
}

// retrieve the header background for this post
$post_type = get_post_type();
if( $post_type == 'soho_portfolio' ){
	
	soho_header_background( soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-portfolio-header-background' ) . " " . soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-portfolio-logo-hero' ) );
	
} else if( $post_type == 'post' ){
	
	soho_header_background( soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-blog-header-background' ) . " " . soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-blog-logo-hero' ) );
	
} else if( $post_type == 'page' ){
	
	soho_header_background( soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-page-header-background' ) . " " . soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-page-logo-hero' ) );
	if( is_page_template("showcase-alt-page.php") ){
		
		soho_header_background( 'negative' );
	}
	
} else {
	
	soho_header_background( 'black logodark' );
}

?>
	<!-- Header -->
    <header class="hidden <?php echo soho_header_background(); ?>">
		<div id="header-container">
			<?php if( $post_type == 'soho_portfolio' ){ 
					$soho_social_sharing = soho_get_theme_options('clapat_soho_portfolio_social_sharing');	
					if( !empty( $soho_social_sharing ) ) {
			?>			
			<div class="page-share secondary-menu"><?php echo wp_kses_post( soho_get_theme_options('clapat_soho_portfolio_menu_share_caption_open') ); ?></div>
			<?php
				}
			}?>
			<!-- Menu Button-->
			<button class="hamburger hamburger-rotate fullscreen hidden"><span>toggle menu</span></button>
			<!--/Menu Button-->
			<?php if( is_page_template('portfolio-page.php') || is_page_template('portfolio-mixed-page.php') ) {
				$show_filters 			= soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-page-portfolio-show-filters' );
				$show_filters_caption	= soho_get_theme_options( 'clapat_soho_portfolio_menu_filters_caption_open' );
				$hide_filters_caption	= soho_get_theme_options( 'clapat_soho_portfolio_menu_filters_caption_close' );
				$filters_caption 		= $show_filters_caption;
				if( $show_filters ){
					$filters_caption		= $hide_filters_caption;
				}
			?>	
				<div class="page-filters secondary-menu"><?php echo wp_kses_post( $filters_caption ); ?></div>
			<?php } else if( (is_page_template('blog-page.php') || is_category() || is_search() || ($post_type == 'post') )  && soho_get_theme_options('clapat_soho_blog_show_search') ) { ?>
			<div class="page-search secondary-menu"><?php echo wp_kses_post( soho_get_theme_options('clapat_soho_blog_search_caption_open') ); ?></div>
			<?php } ?>
			<!-- Logo-->
			<div id="logo">
				<a class="animation-link" href="<?php echo esc_url( get_home_url() ); ?>">
					<img class="logo-negative" src="<?php echo esc_url( $soho_logo_light_path ); ?>" alt="<?php echo esc_html__('Light logo', 'soho'); ?>">
					<img class="logo-positive" src="<?php echo esc_url( $soho_logo_dark_path ); ?>" alt="<?php echo esc_html__('Dark logo', 'soho'); ?>">
				</a>
			</div>
			<!--/Logo-->
			
		</div>    
	</header>
    <!--/Header -->
	<?php
	
	get_template_part('sections/menu_overlay_section');
	
	?>