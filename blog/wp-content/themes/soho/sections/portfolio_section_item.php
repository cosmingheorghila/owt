<?php

$hover_class 	= '';
if( soho_get_hover_effect() != 'normal' ){
	
	$hover_class = soho_get_hover_effect();
}


if( has_post_thumbnail() ){
	
    $item_classes 		= '';
    $item_categories 	= '';
	$item_cats = get_the_terms($post->ID, 'portfolio_category');
	if($item_cats){
		
		foreach($item_cats as $item_cat) {
            $item_classes 		.= $item_cat->slug . ' ';
            $item_categories 	.= $item_cat->name . ', ';
        }
        
		$item_categories = rtrim($item_categories, ', ');

	}

    $full_image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full');
	
    $item_caption_bknd = soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-portfolio-caption-bknd' );
	$item_classes .= $item_caption_bknd . ' ';
	
	$item_height = soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-portfolio-thumbnail-size' );
	if( $item_height != 'normal' ){
	
		$item_classes .= $item_height;
	}
	
	$item_url = get_the_permalink();
	
?>


					<div class="item <?php echo esc_attr( $item_classes . ' ' . $hover_class ); ?>">
						<a class="animation-link" href="<?php echo esc_url( $item_url ); ?>">
                            <div class="item-content" style="background-image:url(<?php echo esc_url( $full_image[0] ); ?>)"></div>
							<div class="item-overlay">
								<div class="outer">
									<div class="inner"> 
										<span class="item-cat"><?php echo wp_kses_post( $item_categories ); ?></span>                    
										<span class="item-title"><?php the_title(); ?></span>
									</div>
								</div>
							</div>	
                        </a>     
                    </div>
					
<?php

}
?>					