	<?php 
		
		// display header section
		get_template_part('sections/header_section'); 		
		
	?>
	
	<?php 
		
		// display project navigation section
		get_template_part('sections/project_share_section'); 		
		
	?>
	
	<!-- Page Content -->
	<div id="page-content" class="show-no-footer">
		<!-- Hero -->
		<div id="hero" class="hidden">
			<!-- Full Screen Slider -->
			<div class="clapat-slider-project">
				<ul class="slides">
<?php

	// retrieve images from the attached gallery
	$gallery = soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-portfolio-slider-gallery' );
	$images_gallery = array();
	if( $gallery ){

		$images_gallery = explode(',', $gallery);
	}

	if( ! empty( $images_gallery ) ){

		foreach( $images_gallery as $image_id ){
					
			$image = wp_get_attachment_image_src( $image_id, "full" );
            $image_path = trim( $image[0] );
			
			if( $image_path ){
?>				
			<li style="background-image:url(<?php echo esc_url( $image_path ); ?>)">
				<div class="intense-zoom" data-image="<?php echo esc_url( $image_path ); ?>">
			</li>              

<?php			
			}
		}
	}
	else {
		
		echo wp_kses_post( __('<h5>There are no images in the gallery attached to this project. You need to create at least one under Portfolio item options.</h5>', 'soho') );
	}
			
?>

				</ul>
			</div>
			<!--/Full Screen Slider -->
			
			<div class="hero-scroll-icons">
                <div class="zoom">
                	<img src="<?php echo esc_url( get_template_directory_uri() . '/images/zoom.png' ); ?>" alt="zoom">
                	<img src="<?php echo esc_url( get_template_directory_uri() . '/images/zoom-white.png' ); ?>" alt="zoom">
                </div>
                <div class="icon-scroll-line"></div>
            </div>
            
            <div id="clapat-nav"><div class="outer"><div class="inner"></div></div></div>
            
            <ul class="slider-projects-nav">
            	<?php if( get_previous_post_link() ){ ?>
            	<li class="project-prev">
            		<a class="animation-link" href="<?php echo esc_url( get_previous_post_link() ); ?>">
            			<div class="arrow-left"></div><span><?php echo wp_kses_post( soho_get_theme_options('clapat_soho_portfolio_prev_caption') ); ?></span>
            		</a>
            	</li>
            	<?php } ?>
                <?php if( get_next_post_link() ){ ?>
                <li class="project-next">
                	<a class="animation-link" href="<?php echo esc_url( get_next_post_link() ); ?>">
                		<div class="arrow-right"></div><span><?php echo wp_kses_post( soho_get_theme_options('clapat_soho_portfolio_next_caption') ); ?></span>
                	</a>
                </li>
                <?php } ?>
            </ul>
          
		</div>
		<!--/Hero -->