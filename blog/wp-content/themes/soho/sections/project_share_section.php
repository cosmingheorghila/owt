	<?php
	$soho_social_sharing = soho_get_theme_options('clapat_soho_portfolio_social_sharing');	
	if( !empty( $soho_social_sharing ) ) { ?>
    <!-- Share -->
	<div id="share-overlay">
        <div class="outer">
            <div class="inner">
                <div id="close-share"></div>
                <h3><?php echo wp_kses_post( soho_get_theme_options('clapat_soho_portfolio_social_sharing_caption') ); ?></h3>
                <div class="list-share">
					<?php echo do_shortcode( $soho_social_sharing ); ?>
                </div>
                <p class="smaller"><?php echo wp_kses_post( soho_get_theme_options('clapat_soho_portfolio_social_sharing_thanks') ); ?></p>
            </div>
        </div>
    </div>
    <!-- Share -->
	<?php } ?>
