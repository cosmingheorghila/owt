<?php
/**
 * Created by Clapat.
 * Date: 17/09/15
 * Time: 1:54 PM
 */

$post_type = get_post_type();

// hero section container properties 
$soho_hero_properties = soho_get_hero_properties();
if( $post_type == 'post' ){
	
	$soho_hero_properties->type = 'image';
}

if( $post_type == 'soho_portfolio' ){
	$soho_hero_properties->type 			= soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-portfolio-hero-type' );
	$soho_hero_properties->size 			= soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-portfolio-hero-height' );
	$soho_hero_properties->position 		= soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-portfolio-hero-position' );
	$soho_hero_properties->opacity 			= soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-portfolio-hero-opacity' );
	$soho_hero_properties->content_type		= soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-portfolio-hero-content' );
	$soho_hero_properties->scroll_arrow		= soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-portfolio-hero-scrolling-arrow' );
} else if( $post_type == 'post' ){
	$soho_hero_properties->size 			= soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-blog-hero-height' );
	$soho_hero_properties->position 		= soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-blog-hero-position' );
	$soho_hero_properties->opacity 			= soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-blog-hero-opacity' );
	$soho_hero_properties->content_type		= soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-blog-hero-content' );
	$soho_hero_properties->scroll_arrow		= soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-blog-hero-scrolling-arrow' );
} else {
	$soho_hero_properties->type 			= soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-page-hero-type' );
	$soho_hero_properties->size 			= soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-page-hero-height' );
	$soho_hero_properties->position 		= soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-page-hero-position' );
	$soho_hero_properties->opacity 			= soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-page-hero-opacity' );
	$soho_hero_properties->content_type		= soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-page-hero-content' );
	$soho_hero_properties->use_main_slider	= soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-page-hero-use-main-slider' );
	$soho_hero_properties->custom_slider	= soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-page-hero-custom-slider' );
	$soho_hero_properties->scroll_arrow		= soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-page-hero-scrolling-arrow' );
}

// hero section image properties 
if( $post_type == 'soho_portfolio' ){
	$soho_hero_properties->image 						= soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-portfolio-hero-image' );
	$soho_hero_properties->image_overlay_color 			= soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-portfolio-hero-image-overlay-color' );
	$soho_hero_properties->image_overlay_color_opacity 	= soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-portfolio-hero-image-overlay-color-opacity' );
	$soho_hero_properties->image_caption 				= soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-portfolio-hero-image-caption' );
	$soho_hero_properties->image_caption_position 		= soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-portfolio-hero-image-caption-position' );
} else if( $post_type == 'post' ){
	$soho_hero_properties->image 						= soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-blog-hero-image' );
	$soho_hero_properties->image_overlay_color 			= soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-blog-hero-image-overlay-color' );
	$soho_hero_properties->image_overlay_color_opacity 	= soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-blog-hero-image-overlay-color-opacity' );
	$soho_hero_properties->image_caption 				= soho_blog_post_hero_caption();
	$soho_hero_properties->image_caption_position 		= soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-blog-hero-image-caption-position' );
} else {
	$soho_hero_properties->image 						= soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-page-hero-image' );
	$soho_hero_properties->image_overlay_color 			= soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-page-hero-image-overlay-color' );
	$soho_hero_properties->image_overlay_color_opacity  = soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-page-hero-image-overlay-color-opacity' );
	$soho_hero_properties->image_caption 				= soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-page-hero-image-caption' );
	$soho_hero_properties->image_caption_position 		= soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-page-hero-image-caption-position' );
}

// hero section video properties
if( $post_type == 'soho_portfolio' ){
	$soho_hero_properties->video_url 			= soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-portfolio-hero-video-url' );
	$soho_hero_properties->video_placeholder	= soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-portfolio-hero-video-placeholder' );
} else {
	$soho_hero_properties->video_url 			= soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-page-hero-video-url' );
	$soho_hero_properties->video_placeholder	= soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-page-hero-video-placeholder' );
}

if( $soho_hero_properties->type != 'none' ){

	get_template_part('sections/hero_section_container'); 
}

?>