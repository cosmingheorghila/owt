<?php
/**
 * Created by Clapat.
 * Date: 17/09/15
 * Time: 1:54 PM
 */

$soho_hero_properties = soho_get_hero_properties();

require_once ( get_template_directory() . '/include/util_functions.php');
$overlay_rgba = clapat_hex2rgba( $soho_hero_properties->image_overlay_color, $soho_hero_properties->image_overlay_color_opacity );

$class_content_type = '';
if( $soho_hero_properties->content_type == 'light' ){

    $class_content_type = ' light-content';
}

?>

				<!-- Video Container - Js will insert video in this div -->
                <div id="video-container">                
                
                    <!-- Stop movie Button -->
                    <a id="stopmovie" title="<?php _e('Click To Pause', 'soho'); ?>"></a>
                    <!--/Stop movie Button -->
                    
                    
                    <!-- Play movie button and video cover image in style.css -->            
                    <a id="playmovie" style="background-image:url(<?php echo esc_url( $soho_hero_properties->video_placeholder['url'] ); ?>);">                
                        
						<div class="overlay" style="background-color:<?php echo esc_attr( $overlay_rgba ); ?>">
							<!-- Image Caption -->
							<div class="clapat-caption animated<?php echo esc_attr( $class_content_type ); ?>">
								<div class="caption-content <?php echo esc_attr( $soho_hero_properties->image_caption_position ); ?>">
									<div class="play-icon"><i class="fa fa-play"></i></div>
									<?php echo wp_kses( $soho_hero_properties->image_caption, wp_kses_allowed_html( 'post' ) ); ?>
								</div>
							</div>
							<!--/Image Caption -->	
						</div>
                    </a>
                    <!--/Play movie Button -->
                    
                    
                    <!-- Video Background - Here you need to replace the videoURL with your youtube video URL -->
					<a id="bgndVideo" class="player" data-property="{videoURL:'<?php echo wp_kses_post( $soho_hero_properties->video_url ); ?>',containment:'#video-container', autoPlay:false, vol:100, opacity:1, showControls:false}"></a>
					<!--/Video Background --> 
                                    
                
                </div>
                <!--/Video Container -->