<?php

$post_image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full');

?>

					<!-- Article Post -->
                    <article id="post-<?php the_ID(); ?>" <?php post_class('post'); ?>>
						<?php if( !empty( $post_image[0] )  ){ ?>
						<div class="post-image" style="background-image:url(<?php echo esc_url( $post_image[0] ); ?>)"><div class="overlay"></div></div>
						<?php } ?>
						<div class="container">
							<div class="meta-categories">
								<?php the_category(); ?>
							</div>
							<a class="post-title animation-link" href="<?php the_permalink(); ?>"><?php the_title(); ?></a><br><br><br>
							<a href="<?php the_permalink(); ?>" class="clapat-button post-title animation-link"><?php echo( wp_kses_post( soho_get_theme_options('clapat_soho_blog_read_more_caption') ) ); ?></a>
							<div class="page-links">
							<?php
								wp_link_pages();
							?>
							</div>
						</div>
					</article>
                    <!--/Article Post -->
