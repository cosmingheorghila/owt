<?php

$args = array(
				'post_type' => 'soho_showcase',
				'orderby'   => 'menu_order',
				'order'     => 'ASC',
				'posts_per_page' => -1
			);

$query_slides = new WP_Query( $args );

$slide_counter 	= '';
$slide_thumbs 	= '';
if( $query_slides->have_posts() ){
	
	while ( $query_slides->have_posts() ) {
		
		$query_slides->the_post();

        $image = soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-showcase-slider-image');
        $image_path   = trim( $image['url'] );
		
		$slide_counter .= '<li><span class="current-slide"></span> / <span class="total-slides"></span></li>';
		$slide_thumbs  .= '<li><img src="' . esc_url( $image_path ) . '" alt="showcase-thumb"></li>';
	}
}

wp_reset_postdata();

?>
	
	<!-- Sidebar -->
    <div id="sidebar" class="hidden">
    
		<div id="open-sidebar"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/plus.png" alt="open"></div>
        
        <div class="counter-slider">
            <ul class="slides">
            	<?php echo wp_kses_post( $slide_counter ); ?>
            </ul>                
        </div>
        	
        <div class="sidebar-container">
    
            <div class="showcase-controls">
                <ol class="showcase-control-nav">
                    <?php echo wp_kses_post( $slide_thumbs ); ?>
                </ol>
            </div>	
        
        </div>        
		
    </div>
    <!--/Sidebar -->