	<?php if( soho_get_theme_options('clapat_soho_blog_show_search') ) { ?>
    <!-- Search -->
	<div id="search-overlay">
        <div class="outer">
            <div class="inner">
                <div id="close-search"></div>
				<?php get_template_part('searchform'); ?>
            </div>
        </div>
    
    </div>
    <!-- Search -->
	<?php } ?>