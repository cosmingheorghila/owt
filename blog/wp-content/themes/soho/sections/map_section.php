<?php
/**
 * Created by Clapat.
 * Date: 25/03/16
 * Time: 1:54 PM
 */

$hero_size 			    = soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-page-map-height' );

$hero_class = '';
if( $hero_size == 'small' ){
	
	$hero_class .= ' hero-small';
}
else if( $hero_size == 'large' ){
	
	$hero_class .= ' hero-large';
}

$hero_class = trim( $hero_class );
if( !empty( $hero_class ) ){

	$hero_class = ' ' . $hero_class;
} 

$show_overlay = soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-page-map-show-overlay' );

?>

		<div id="hero" class="hidden<?php echo esc_attr( $hero_class );?>">
			<?php
			if( $show_overlay ){ 
				get_template_part('sections/map_cover_section');
			} 
			?>
			<!-- Map -->
			<div id="map_canvas"></div>
			<!--/Map -->
		</div>