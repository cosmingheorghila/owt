<?php
/**
 * Created by Clapat.
 * Date: 25/03/15
 * Time: 1:54 PM
 */

require_once ( get_template_directory() . '/include/util_functions.php');

$gallery = soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-portfolio-hero-slider' );
$slides_gallery = array();
if( $gallery ){

	$slides_gallery = explode(',', $gallery);
}

$soho_hero_properties = soho_get_hero_properties();

$class_content_type = '';
if( $soho_hero_properties->content_type == 'light' ){

    $class_content_type = ' light-content';
}

?>


			<!-- Page Pilling Slider Hero -->
            <div class="pagepillingsliderhero hidden">

                    <?php

                    if( ! empty( $slides_gallery ) ){

                        foreach ( $slides_gallery as $slide_id ) {
                            

                            $image = wp_get_attachment_image_src( $slide_id, "full" );
                            $image_path = trim( $image[0] );

                    ?>

                        <!-- Slide -->
						<div class="section <?php echo sanitize_html_class( $class_content_type ); ?>" style="background-image:url(<?php echo esc_url( $image_path ); ?>)">
	                        <div class="overlay" style="background-color:rgba(0,0,0,0)">
	                            <!-- Slide Caption -->
	                            <div class="clapat-caption">
	                                <div class="caption-content"></div>
	                            </div>
	                            <!--/Slide Caption -->
	                        </div>
                    	</div>
						<!--/Slide -->

                    <?php

                        } // foreach slide in gallery
                        
                    }
                    else{

						esc_html_e('There are no slides defined. You can create them in admin dashboard under Portfolio Item options.', 'soho');
                    }

                    ?>

            	<ul class="pagepillingnav">
	            	<li class="pilling-prev"><div class="arrow-left"></div><div class="arrow-left-line"></div></li>
	                <li class="pilling-next"><div class="arrow-right"></div><div class="arrow-right-line"></div></li>
            	</ul>
            </div>
			<!-- /Page Pilling Slider Hero -->