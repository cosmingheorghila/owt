<?php
/**
 * Created by Clapat.
 * Date: 25/03/16
 * Time: 1:54 PM
 */

$image_overlay_color 			= soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-page-map-overlay-color' );
$image_overlay_color_opacity	= soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-page-map-overlay-color-opacity' );
$content_type					= soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-page-map-caption-content' );
$caption_content				= soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-page-map-caption' );
$caption_position				= soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-page-map-caption-position' );

require_once ( get_template_directory() . '/include/util_functions.php');
$overlay_rgba = clapat_hex2rgba( $image_overlay_color, $image_overlay_color_opacity );

$class_content_type = '';
if( $content_type == 'light' ){

    $class_content_type = ' light-content';
}

?>

			<!-- Map Cover-->
            <div id="map_cover">
             	<div class="overlay " style="background-color:<?php echo esc_attr( $overlay_rgba ); ?>;">
	                <!-- Image Caption -->
	                <div class="clapat-caption animated <?php echo esc_attr( $class_content_type ); ?>">
	                	<div class="caption-content <?php echo sanitize_html_class( $caption_position ); ?>">
	                    	<?php echo wp_kses_post( $caption_content ); ?>
	                        <div class="icon-scroll"></div>                    	
	                        <div class="icon-scroll-line"></div>                    
	                    </div>
	                </div>
	                <!--/Image Caption -->
                </div>
            </div>
			<!-- /Map Cover-->