<?php
/**
 * Created by Clapat.
 * Date: 25/03/16
 * Time: 1:54 PM
 */
$soho_hero_properties = soho_get_hero_properties();

require_once ( get_template_directory() . '/include/util_functions.php');
$overlay_rgba = clapat_hex2rgba( $soho_hero_properties->image_overlay_color, $soho_hero_properties->image_overlay_color_opacity );

$class_content_type = '';
if( $soho_hero_properties->content_type == 'light' ){

    $class_content_type = ' light-content';
}
							
?>

			<div class="hero-image" style="background-image:url(<?php echo esc_url( $soho_hero_properties->image['url'] ); ?>)">
            	<div class="overlay" style="background-color:<?php echo esc_attr( $overlay_rgba ); ?>">
                    <!-- Slide Caption -->
                    <div class="clapat-caption animated <?php echo esc_attr( $class_content_type ); ?>">
                        <div class="caption-content <?php echo sanitize_html_class( $soho_hero_properties->image_caption_position ); ?>">
                            <?php echo wp_kses( $soho_hero_properties->image_caption, wp_kses_allowed_html( 'post' ) ); ?>
                        </div>
                    </div>
                    <!--/Slide Caption -->
                </div>
            </div>
