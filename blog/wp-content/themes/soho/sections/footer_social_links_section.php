<?php

if( !function_exists('soho_render_footer_social_links' ) )
{
	function soho_render_footer_social_links(){
		
		global $soho_theme_options;
		global $soho_social_links;

		echo '<ul class="socials-text">';
		
		for( $idx = 1; $idx <= SOHO_MAX_SOCIAL_LINKS; $idx++ ){
			
			$social_name = $soho_theme_options['clapat_soho_footer_social_' . $idx];
			$social_url  = $soho_theme_options['clapat_soho_footer_social_url_' . $idx];
			
			if( $social_url ){
			   
				echo '<li><a href="' . esc_url( $social_url ) . '" target="_blank">' . wp_kses_post( $soho_social_links[$social_name] ) . '</a></li>';
			}
			
		}

		echo '</ul>';
	}
}

soho_render_footer_social_links();
		
?>		