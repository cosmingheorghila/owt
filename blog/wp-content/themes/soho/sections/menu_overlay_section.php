
		<!-- Menu Overlay -->
		<div id="menu-overlay">
			<div class="outer">
				<div class="inner">
					<div id="close-menu"></div>
					<?php
											
					wp_nav_menu(array(
								'theme_location' => 'primary-menu',
								'menu_class' 	=> 'main-menu',
								'container' 	=> 'nav'
								));
								
					?>
					<?php get_template_part('sections/menu_social_links_section'); ?>
				</div>
			</div>
		</div>
		<!-- Menu Overlay -->