	<?php 
		
	// display header section
	get_template_part('sections/header_section'); 		
		
	?>
	
	<?php 
		
		// display project navigation section
		get_template_part('sections/project_share_section'); 		
		
	?>
	
	 <!-- Page Content -->
	<div id="page-content">
    
    
        <?php
		
		// display hero section, if any
		$soho_hero_type = soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-portfolio-hero-type' );
		if( $soho_hero_type != 'none' ){
		
			get_template_part('sections/hero_section'); 
		}
		
		?>
                
        
        
        <!-- Main -->
        <div id="main">
		<div id="main-content" class="hidden">
			<?php if ( !function_exists( 'vc_set_as_theme' ) ) { // if Visual Composer is not installed, add a container?>
			<div class="container no-composer">
			<?php } ?>
			<?php the_content(); ?>
			<?php if ( !function_exists( 'vc_set_as_theme' ) ) { // if Visual Composer is not installed, add a container?>
			</div>
			<?php } ?>
			<?php 
			// display project navigation section
			get_template_part('sections/project_navigation_section'); 		
			?>
		</div>
        </div>
        <!--/Main -->