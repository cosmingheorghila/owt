<?php
/**
 * Created by Clapat.
 * Date: 25/03/16
 * Time: 1:54 PM
 */

require_once ( get_template_directory() . '/include/util_functions.php');

?>


			<!-- Page Pilling Slider Hero -->
            <div class="pagepillingsliderhero hidden">

                    <?php

                    $args = array(
                        'post_type' => 'soho_main_slider',
                        'orderby'   => 'menu_order',
                        'order'     => 'ASC',
						'posts_per_page' => -1
                    );

                    $query_slides = new WP_Query( $args );

                    if( $query_slides->have_posts() ){

                    	$first_slide = true;
                        while ( $query_slides->have_posts() ) {

                            $query_slides->the_post();

                            $image = soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-slider-image');
                            $image_path   = trim( $image['url'] );

                            $bknd_repeat  = soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-slider-bknd-repeat' );
                            $style_repeat = '';
                            if( $bknd_repeat ){

                                $style_repeat = 'background-repeat: repeat; ';
                            }

                            $caption_alignment = soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-slider-caption-alignment' );

                            $overlay_color   = soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-slider-overlay-color' );
                            $overlay_opacity = soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-slider-overlay-opacity' );
                            $overlay_rgba    = clapat_hex2rgba( $overlay_color, $overlay_opacity );

                            $content_type    = soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-slider-content-type' );
                            $class_content_type = '';
							$class_slide_background = '';
                            if( $content_type == 'light' ){

                                $class_content_type = ' light-content';
								$class_slide_background = 'class="dark-bg" ';
                            }
							
							$animated_class = '';
							if( $first_slide ){
								
								$animated_class = 'animated';
								$first_slide = false;
							}

                    ?>
                    	<!-- Slide -->
						<div class="section <?php echo sanitize_html_class( $class_content_type ); ?> <?php echo sanitize_html_class( $class_slide_background ); ?>" style="<?php echo esc_attr( $style_repeat ); ?>background-image:url(<?php echo esc_url( $image_path ); ?>)">
	                        <div class="overlay" style="background-color:<?php echo esc_attr( $overlay_rgba ); ?>">
	                            <!-- Slide Caption -->
	                            <div class="clapat-caption <?php echo sanitize_html_class( $animated_class ); ?>">
	                                <div class="caption-content <?php echo sanitize_html_class( $caption_alignment ); ?>">                                        
	                                    <?php the_content(); ?>
	                                </div>
	                            </div>
	                            <!--/Slide Caption -->
	                        </div>
                    	</div>
						<!--/Slide -->
						
                    <?php

                        } // while posts
                  
                    }
                    else{

                        esc_html_e('There are no slides defined. You can create them in admin dashboard under Main Slider.', 'soho');
                    }

                    wp_reset_postdata();

                    ?>

				<ul class="pagepillingnav">
	            	<li class="pilling-prev"><div class="arrow-left"></div><div class="arrow-left-line"></div></li>
	                <li class="pilling-next"><div class="arrow-right"></div><div class="arrow-right-line"></div></li>
            	</ul>
            </div>
			<!--/Page Pilling Slider Hero -->
