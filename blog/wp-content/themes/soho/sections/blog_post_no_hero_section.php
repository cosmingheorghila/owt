<?php

$post_image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full');
$image_path = "";
if( $post_image && !empty( $post_image[0] ) ){
	
	$image_path = $post_image[0];
}

$content_type		= soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-blog-hero-content' );
$scroll_arrow		= soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-blog-hero-scrolling-arrow' );

$header_size 		= soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-blog-hero-height' );
$header_position 	= soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-blog-hero-position' );
$header_opacity 	= soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-blog-hero-opacity' );

$header_class 	= '';
$header_styles 	= '';
if( $header_size == 'small' ){
	
	$header_class .= ' hero-small';
}
else if( $header_size == 'large' ){
	
	$header_class .= ' hero-large';
}
if( $header_position == 'static' ){
	
	$header_styles .= ' static-hero';
	
} else if( $header_position == 'parallax' ){
	
	$header_styles .= ' parallax-hero';
}
if( $header_opacity ){
	
	$header_styles .= ' opacity-hero';
}


$image_overlay_color 			= soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-blog-hero-image-overlay-color' );
$image_overlay_color_opacity 	= soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-blog-hero-image-overlay-color-opacity' );
$image_caption_position 		= soho_get_post_meta( SOHO_THEME_OPTIONS, get_the_ID(), 'soho-opt-blog-hero-image-caption-position' );

require_once ( get_template_directory() . '/include/util_functions.php');
$overlay_rgba = clapat_hex2rgba( $image_overlay_color, $image_overlay_color_opacity );

?>

		<!-- Hero -->
        <div id="hero" class="hidden<?php echo esc_attr( $header_class ); ?>">
			<div id="hero-styles" class="<?php echo esc_attr( $header_styles ); ?>">
        
                <!-- Full Screen Image -->
                <?php if( $image_path ) { ?>
                <div class="hero-image" style="background-image:url(<?php echo esc_url( $image_path ); ?>)">
                <?php } else { ?>
                <div class="hero-image">
                <?php } ?>
	                <div class="overlay " style="background-color:<?php echo esc_attr( $overlay_rgba ); ?>;">
	                    <!-- Image Caption -->
	                    <div class="clapat-caption animated <?php if( $content_type == "light" ) { echo "light-content"; } ?>">
	                        <div class="caption-content <?php echo sanitize_html_class( $image_caption_position ); ?>">
	                        	<div class="meta-categories">
									<?php the_category(); ?>						
	    						</div>
	                                        
	                    		<h1 class="big-title post-title-no-link"><?php the_title(); ?></h2>	                
	                        </div>
	                    </div>
	                    <!--/Image Caption -->
	                </div>
	            </div>            
	            <!--/Full Screen Image -->
                
				<?php if( $scroll_arrow ){ ?>
                <div class="hero-scroll-icons">
                    <div class="icon-scroll"></div>
                    <div class="icon-scroll-line"></div>
                </div>
				<?php } ?>
            </div>        
        </div>
        <!--/Hero -->