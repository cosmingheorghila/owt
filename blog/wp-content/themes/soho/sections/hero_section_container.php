<?php
/**
 * Created by Clapat.
 * Date: 27/03/16
 * Time: 1:54 PM
 */
$soho_hero_properties = soho_get_hero_properties();

$hero_class 	= '';
$hero_styles 	= '';
if( $soho_hero_properties->size == 'small' ){
	
	$hero_class .= ' hero-small';
}
else if( $soho_hero_properties->size == 'large' ){
	
	$hero_class .= ' hero-large';
}
if( $soho_hero_properties->position == 'static' ){
	
	$hero_styles .= ' static-hero';
	
} else if( $soho_hero_properties->position == 'parallax' ){
	
	$hero_styles .= ' parallax-hero';
}
if( $soho_hero_properties->opacity ){
	
	$hero_styles .= ' opacity-hero';
}
// when hero type is image the background class type is part of #hero div
if( ($soho_hero_properties->type == 'image') || ( ($soho_hero_properties->type == 'slider') && !$soho_hero_properties->use_main_slider ) ){
	
	if( $soho_hero_properties->content_type == 'light' ){
	
		$hero_class .= ' dark-bg';
	}
}

$hero_class = trim( $hero_class );
if( !empty( $hero_class ) ){

	$hero_class = ' ' . $hero_class;
} 

if( $soho_hero_properties->type != 'none' ){

?>

		<!-- Hero --> 
        <div id="hero" class="hidden<?php echo esc_attr( $hero_class ); ?>">
			<div id="hero-styles" class="<?php echo esc_attr( $hero_styles ); ?>">
			<?php
			
				if( $soho_hero_properties->type == 'slider' ){
					
					if( $soho_hero_properties->use_main_slider ){
						
						get_template_part('sections/hero_section_slider');
					}
					else {
						
						echo do_shortcode( $soho_hero_properties->custom_slider );
					}
				}
				else if( $soho_hero_properties->type == 'portfolio_slider' ){
					
					get_template_part('sections/hero_section_portfolio_slider');
				}
				else if( $soho_hero_properties->type == 'blog_slider' ){
					
					get_template_part('sections/hero_section_blog_slider');
				}
				else if( $soho_hero_properties->type == 'video' ){
					
					get_template_part('sections/hero_section_video');
				}
				else{
				
					if( $soho_hero_properties->image && $soho_hero_properties->image['url'] ){

						get_template_part('sections/hero_section_image');
					}
				}
			?>
			<?php
			if( $soho_hero_properties->scroll_arrow ){ 
			?>
			<div class="hero-scroll-icons">
            	<div class="icon-scroll"></div>
            	<div class="icon-scroll-line"></div>
            </div>
			<?php
			}
			?>
			</div>
		</div>
        <!--/Hero --> 
		
<?php
}
?>		