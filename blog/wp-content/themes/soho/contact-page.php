<?php
/*
Template name: Contact Template
*/

get_header();

while ( have_posts() ){

    the_post();

    ?>
	
		<?php get_template_part('sections/header_section'); ?>
		
		<!-- Page Content -->
		<div id="page-content">
	
		<?php get_template_part('sections/map_section'); ?>
		
			<!-- Main --> 
			<div id="main">
				<div id="main-content" class="hidden">
					
					<?php if ( !function_exists( 'vc_set_as_theme' ) ) { // if Visual Composer is not installed, add a container?>
					<div class="container no-composer">
					<?php } ?>
					<?php the_content(); ?>
					<?php if ( !function_exists( 'vc_set_as_theme' ) ) { // if Visual Composer is not installed, add a container?>
					</div>
					<?php } ?>
					
				</div>
			</div>
			<!--/Main -->

<?php

}

get_footer();

?>