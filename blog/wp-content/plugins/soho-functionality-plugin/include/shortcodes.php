<?php
// Soho shortcodes definition


function clapat_get_image_url( $image_id, $image_url ){
	
	if( !empty( $image_id ) ){
		
		$image_info = wp_get_attachment_image_src( $image_id, 'full' );
		if( $image_info[0] ){
			
			return $image_info[0];
		} 
	}
	
	return $image_url;
}


/* Columns */

//////////////////////////////////////////////////////////////////
// Column one_half shortcode
//////////////////////////////////////////////////////////////////
add_shortcode('one_half', 'shortcode_one_half');
function shortcode_one_half($atts, $content = null) {
    $atts = shortcode_atts(
        array(
            'last' => 'no',
            'text_align' => 'text-align-left'
        ), $atts);

    $class = $atts['text_align'];
    if( $atts['last'] == 'yes' ){

        $class .= ' last';
    }

    return '<div class="one_half ' . $class . '">' .do_shortcode($content). '</div>';

}

//////////////////////////////////////////////////////////////////
// Column one_third shortcode
//////////////////////////////////////////////////////////////////
add_shortcode('one_third', 'shortcode_one_third');
function shortcode_one_third($atts, $content = null) {
    $atts = shortcode_atts(
        array(
            'last' => 'no',
            'text_align' => 'text-align-left'
        ), $atts);

    $class = $atts['text_align'];
    if( $atts['last'] == 'yes' ){

        $class .= ' last';
    }

    return '<div class="one_third ' . $class . '">' .do_shortcode($content). '</div>';

}

//////////////////////////////////////////////////////////////////
// Column two_third shortcode
//////////////////////////////////////////////////////////////////
add_shortcode('two_third', 'shortcode_two_third');
function shortcode_two_third($atts, $content = null) {
    $atts = shortcode_atts(
        array(
            'last' => 'no',
            'text_align' => 'text-align-left'
        ), $atts);

    $class = $atts['text_align'];
    if( $atts['last'] == 'yes' ){

        $class .= ' last';
    }

    return '<div class="two_third ' . $class . '">' .do_shortcode($content). '</div>';

}

//////////////////////////////////////////////////////////////////
// Column one_fourth shortcode
//////////////////////////////////////////////////////////////////
add_shortcode('one_fourth', 'shortcode_one_fourth');
function shortcode_one_fourth($atts, $content = null) {
    $atts = shortcode_atts(
        array(
            'last' => 'no',
            'text_align' => 'text-align-left'
        ), $atts);

    $class = $atts['text_align'];
    if( $atts['last'] == 'yes' ){

        $class .= ' last';
    }

    return '<div class="one_fourth ' . $class . '">' .do_shortcode($content). '</div>';

}

//////////////////////////////////////////////////////////////////
// Column three_fourth shortcode
//////////////////////////////////////////////////////////////////
add_shortcode('three_fourth', 'shortcode_three_fourth');
function shortcode_three_fourth($atts, $content = null) {
    $atts = shortcode_atts(
        array(
            'last' => 'no',
            'text_align' => 'text-align-left'
        ), $atts);

    $class = $atts['text_align'];
    if( $atts['last'] == 'yes' ){

        $class .= ' last';
    }

    return '<div class="three_fourth ' . $class . '">' .do_shortcode($content). '</div>';

}

//////////////////////////////////////////////////////////////////
// Column one fifth shortcode
//////////////////////////////////////////////////////////////////
add_shortcode('one_fifth', 'shortcode_one_fifth');
function shortcode_one_fifth($atts, $content = null) {
    $atts = shortcode_atts(
        array(
            'last' => 'no',
            'text_align' => 'text-align-left'
        ), $atts);

    $class = $atts['text_align'];
    if( $atts['last'] == 'yes' ){

        $class .= ' last';
    }

    return '<div class="one_fifth ' . $class . '">' .do_shortcode($content). '</div>';

}

add_shortcode('two_fifth', 'shortcode_two_fifth');
function shortcode_two_fifth($atts, $content = null) {
    $atts = shortcode_atts(
        array(
            'last' => 'no',
            'text_align' => 'text-align-left'
        ), $atts);

    $class = $atts['text_align'];
    if( $atts['last'] == 'yes' ){

        $class .= ' last';
    }

    return '<div class="two_fifth ' . $class . '">' .do_shortcode($content). '</div>';

}

add_shortcode('three_fifth', 'shortcode_three_fifth');
function shortcode_three_fifth($atts, $content = null) {
    $atts = shortcode_atts(
        array(
            'last' => 'no',
            'text_align' => 'text-align-left'
        ), $atts);

    $class = $atts['text_align'];
    if( $atts['last'] == 'yes' ){

        $class .= ' last';
    }

    return '<div class="three_fifth ' . $class . '">' .do_shortcode($content). '</div>';

}

add_shortcode('four_fifth', 'shortcode_four_fifth');
function shortcode_four_fifth($atts, $content = null) {
    $atts = shortcode_atts(
        array(
            'last' => 'no',
            'text_align' => 'text-align-left'
        ), $atts);

    $class = $atts['text_align'];
    if( $atts['last'] == 'yes' ){

        $class .= ' last';
    }

    return '<div class="four_fifth ' . $class . '">' .do_shortcode($content). '</div>';

}

//////////////////////////////////////////////////////////////////
// Column one sixth shortcode
//////////////////////////////////////////////////////////////////
add_shortcode('one_sixth', 'shortcode_one_sixth');
function shortcode_one_sixth($atts, $content = null) {
    $atts = shortcode_atts(
        array(
            'last' => 'no',
            'text_align' => 'text-align-left'
        ), $atts);

    $class = $atts['text_align'];
    if( $atts['last'] == 'yes' ){

        $class .= ' last';
    }

    return '<div class="one_sixth ' . $class . '">' .do_shortcode($content). '</div>';

}

//////////////////////////////////////////////////////////////////
// Column five sixth shortcode
//////////////////////////////////////////////////////////////////
add_shortcode('five_sixth', 'shortcode_five_sixth');
function shortcode_five_sixth($atts, $content = null) {
    $atts = shortcode_atts(
        array(
            'last' => 'no',
            'text_align' => 'text-align-left'
        ), $atts);

    $class = $atts['text_align'];
    if( $atts['last'] == 'yes' ){

        $class .= ' last';
    }

    return '<div class="five_sixth ' . $class . '">' .do_shortcode($content). '</div>';

}

/* End Columns */


/* Typo Elements */

//////////////////////////////////////////////////////////////////
// Title
//////////////////////////////////////////////////////////////////
add_shortcode('title', 'shortcode_title');
function shortcode_title($atts, $content = null) {

    $atts = shortcode_atts( array(
        'size'   	=> 'h1',
        'underline' => 'no',
		'big'		=> 'no'
    ), $atts );

    $class_title = '';
    if( $atts['underline'] == 'yes' ){

        $class_title = 'title-has-line';
    }
	if( $atts['big'] == 'yes' ){
		
		$class_title .= ' big-title';
	}
	if( !empty( $class_title ) ){
		
		$class_title = ' class="' . $class_title . '"';
	}
	
    $html = '';
    $html .= '<'.$atts['size']. $class_title . '>'.do_shortcode($content).'</'.$atts['size'].'>';
    return $html;
}

//////////////////////////////////////////////////////////////////
// Breaking line
//////////////////////////////////////////////////////////////////
add_shortcode('br', 'shortcode_br');
function shortcode_br($atts, $content = null) {

    return '<br />';
}

//////////////////////////////////////////////////////////////////
// Line Divider
//////////////////////////////////////////////////////////////////
add_shortcode('hr', 'shortcode_hr');
function shortcode_hr($atts, $content = null) {

	$atts = shortcode_atts( array(
        'size' => 'normal'
    ), $atts );

    $class_title = '';
    if( $atts['size'] == 'small' ){

        $class_title = ' class="small"';
    }
    
    return '<hr'. $class_title .'>';
}

//////////////////////////////////////////////////////////////////
// Button shortcode
//////////////////////////////////////////////////////////////////
add_shortcode('button', 'shortcode_button');
function shortcode_button($atts, $content = null) {

    $atts = shortcode_atts(array(
                'link'      => '',
                'target'    => '_blank',
                'type'      => 'normal'
            ), $atts );

    $outline = '';
    if( $atts['type'] == 'outline' ){
        $outline .= ' outline-button';
    }
	if( $atts['target'] == '_self' ){
        $outline .= ' animation-link';
    }
	
    return '<a class="clapat-button'  . $outline . '" href="' . $atts['link'] . '" target="' . $atts['target'] . '">' . do_shortcode($content). '</a>';
}

//////////////////////////////////////////////////////////////////
// Full Button shortcode
//////////////////////////////////////////////////////////////////
add_shortcode('full_button', 'shortcode_full_button');
function shortcode_full_button($atts, $content = null) {

    $atts = shortcode_atts(array(
                'link'      => '',
                'target'    => '_blank'
            ), $atts );

    $outline = '';
    if( $atts['target'] == '_self' ){
        $outline .= ' animation-link';
    }
	
    return '<a class="clapat-button-full'  . $outline . '" href="' . $atts['link'] . '" target="' . $atts['target'] . '">' . do_shortcode($content). '</a>';
}

//////////////////////////////////////////////////////////////////
// Space Between Buttons
//////////////////////////////////////////////////////////////////
add_shortcode('space_between_buttons', 'shortcode_space_between_buttons');
function shortcode_space_between_buttons($atts, $content = null) {

	return '<span class="space-buttons"></span>';
}

//////////////////////////////////////////////////////////////////
// Animated link
//////////////////////////////////////////////////////////////////
add_shortcode('animated_link', 'shortcode_animated_link');
function shortcode_animated_link($atts, $content = null) {

    $atts = shortcode_atts(array(
                'link'      => '',
                'target'    => '_blank'
            ), $atts );

	return '<a href="' . $atts['link'] . '" target="' . $atts['target'] . '" class="animation-link more">' . do_shortcode($content). '<span class="icon"></span></a>';
}

/* End Typo Elements */


/* Elements */

//////////////////////////////////////////////////////////////////
// Accordion
//////////////////////////////////////////////////////////////////
add_shortcode('accordion', 'shortcode_accordion');
function shortcode_accordion($atts, $content = null) {

    $str = '<dl class="accordion">';
    $str .= do_shortcode( $content );
    $str .= '</dl>';

    return $str;
}

add_shortcode('accordion_item', 'shortcode_accordion_item');
function shortcode_accordion_item($atts, $content = null) {

    $atts = shortcode_atts(
                array(
                    'title'      => ''
                ), $atts );

    $str = '<dt><span>' . $atts['title'] . '</span></dt>';
    $str .= '<dd class="accordion-content">' . do_shortcode( $content ) . '</dd>';

    return $str;
}

//////////////////////////////////////////////////////////////////
// Tabs shortcode
//////////////////////////////////////////////////////////////////
add_shortcode('tabs', 'shortcode_tabs');
function shortcode_tabs( $atts, $content = null ) {

    $out = '';

    $out .= '<div class="tab-shortcode">';

    $out .= '<ul class="clearfix tabs">';
    $first = true;
    foreach ($atts as $key => $tab) {
        $out .= '<li><a href="#' . $key . '">' . $tab . '</a></li>';
        $first = false;
    }
    $out .= '</ul>';

    $out .= do_shortcode( $content );

    $out .= '</div>';

    return $out;
}


add_shortcode('tab', 'shortcode_tab');
function shortcode_tab( $atts, $content = null ) {

    $out = '';
    $out .= '<div id="tab' . $atts['id'] . '" class="tab_container">' . do_shortcode($content) .'</div>';

    return $out;
}

//////////////////////////////////////////////////////////////////
// Toggle shortcode
//////////////////////////////////////////////////////////////////
add_shortcode('toggle', 'shortcode_toggle');
function shortcode_toggle( $atts, $content = null ) {

    $atts = shortcode_atts(
                            array(
                                'title'      => ''
                            ), $atts );


    $html = '<div class="toggle-wrap"> ';

    $html .= '<span class="toggle-title"><a href="#">' . $atts['title'] . '</a></span>';
    $html .= '<div class="toggle_container">' . do_shortcode( $content ) . '</div>';

    $html .= '</div>';

    return $html;
}

//////////////////////////////////////////////////////////////////
// Alert Message
//////////////////////////////////////////////////////////////////
add_shortcode('alert', 'shortcode_alert');
function shortcode_alert($atts, $content = null) {

    $atts = shortcode_atts(
        array(
            'color'      => 'red'
        ), $atts );

    $out  = '';
    $out .= '<div class="alertboxes">';
    $out .= '<div class="shortcode_alertbox box_'. $atts['color'] .'">';
    $out .= do_shortcode($content);
    $out .= '<a class="box_close"></a>';
    $out .= '</div>';
    $out .= '</div>';

    return $out;
}

//////////////////////////////////////////////////////////////////
// Progress Bar
//////////////////////////////////////////////////////////////////
add_shortcode('progress', 'shortcode_progress');
function shortcode_progress($atts, $content = null) {

    $html = '';
    $html .= '<ul class="progress-bar">';
    $html .= do_shortcode( $content );
    $html .= '</ul>';

    return $html;
}

add_shortcode('progress_bar', 'shortcode_progress_bar');
function shortcode_progress_bar($atts, $content = null) {

    $atts = shortcode_atts(array(
        'percentage'  => '100'
    ), $atts );

    return '<li><p>' . $content . '</p><div class="bar-wrap"><span data-width="' . $atts['percentage'] . '"><strong><i>' . $atts['percentage'] . '</i>%</strong> </span></div></li>';
}

//////////////////////////////////////////////////////////////////
// Pricing tables
//////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////
// Pricing table
//////////////////////////////////////////////////////////////////
add_shortcode('pricing_table', 'shortcode_pricing_table');
function shortcode_pricing_table($atts, $content = null) {

    $atts = shortcode_atts(array(
        'active'    => 'no',
		'icon'    => '',
        'title'   => '',
        'price'    => '',
        'currency' => '$',
        'time'     => 'per month',
		'url'	   => '',
		'target'   => '_blank',
		'buy_now_text' => 'Buy Now'
    ), $atts );

    $active = '';
    if( $atts['active'] == 'yes' ){

        $active = ' active';
    }


    $str = '';
    $str .= '<div class="p-table-item' . $active . '">';
    $str .= '<div class="p-table-item-inner">';
    $str .= '<div class="p-table-wrap">';
    
	$str .= '<!-- Icon (Simple-line-icons) -->';
    $str .= '<div class="p-table-icon">';
    $str .= '<i class="' . $atts['icon'] . '"></i>';
    $str .= '</div>';
    $str .= '<!-- Pricing Title -->';
    $str .= '<div class="p-table-title">' . $atts['title'] . '</div>';
    $str .= '<!-- Pricing Title -->';

    $str .= '<!-- Price -->';
    $str .= '<div class="p-table-num">';
    $str .= '<sup>' . $atts['currency'] . '</sup>' . $atts['price'];
    $str .= '</div>';
    $str .= '<div class="p-table-per">' . $atts['time'] . '</div>';
    $str .= '<!--/Price -->';

    $str .= '<!-- Pricing Features -->';
    $str .= '<div class="p-table-features">';
    $str .= '<ul class="p-table-list">';
    $str .= do_shortcode( $content );
    $str .= '</ul>';
    $str .= '</div>';
	
	$str .= '<!-- Button -->';
    $str .= '<div class="p-table-button">';
    $str .= '<a class="clapat-button" href="' . $atts['url'] . '" target="' . $atts['target'] . '">' . $atts['buy_now_text'] . '</a>';
    $str .= '</div>';
    $str .= '<!--/Button -->';
	
    $str .= '</div>';
    $str .= '</div>';
    $str .= '</div>';

    return $str;
}


//////////////////////////////////////////////////////////////////
// Pricing Row
//////////////////////////////////////////////////////////////////
add_shortcode('pricing_row', 'shortcode_pricing_row');
function shortcode_pricing_row($atts, $content = null) {

    $str = '';
    $str .= '<li>';
    $str .= do_shortcode($content);
    $str .= '</li>';

    return $str;
}

//////////////////////////////////////////////////////////////////
// Counter
//////////////////////////////////////////////////////////////////
add_shortcode('counters', 'shortcode_counters');
function shortcode_counters($atts, $content = null) {

    $out = '<ul class="clapat-counters">';
    $out .= do_shortcode( $content );
    $out .= '</ul>';

    return $out;
}

add_shortcode('counter', 'shortcode_counter');
function shortcode_counter($atts, $content = null) {

    $atts = shortcode_atts(array(
        'counts'	=> '100'
    ), $atts );

	$str = '<li class="clapat-counter">';
	$str .= '<div class="number">' . $atts['counts'] . '</div>';
    $str .= '<div class="subject">' . $content . '</div>';
    $str .= '</li>';

    return $str;
}

//////////////////////////////////////////////////////////////////
// Radial Counter
//////////////////////////////////////////////////////////////////
add_shortcode('radial_counter', 'shortcode_radial_counter');
function shortcode_radial_counter($atts, $content = null) {

    $atts = shortcode_atts(
                array(
                    'counts'	=> '100',
                    'title'     => ''
                ), $atts );

    $str = '<div class="radial-counter">';
    $str .= '<input class="knob"  data-gal="' . $atts['counts'] . '" value="0">';
    $str .= '<h5 class="radial-counter-name">' . $atts['title'] . '</h5>';
    $str .= '<p class="radial-counter-info">' . do_shortcode( $content ) . '</p>';
    $str .= '</div>';

    return $str;
}

//////////////////////////////////////////////////////////////////
// Services
//////////////////////////////////////////////////////////////////
add_shortcode('service', 'shortcode_service');
function shortcode_service($atts, $content = null) {

    $atts = shortcode_atts( array(
                                'title' => '',
                                'icon' => '',
                            ), $atts );

    $out = '';

    $out .= '<div class="services-icon">';
    $out .= '<i class="' . $atts['icon'] . '"></i>';
	$out .= '<div class="service-info-text">';
    $out .= '<h5>' . $atts['title'] . '</h5>';
    $out .= '<p>' . do_shortcode( $content ) . '</p>';
    $out .= '</div>';
	$out .= '</div>';

    return $out;
}

add_shortcode('service_number', 'shortcode_service_number');
function shortcode_service_number($atts, $content = null) {

    $atts = shortcode_atts( array(
                                'title' => '',
                                'number' => '',
                            ), $atts );

    $out = '';

    $out .= '<div class="services-number">';
    $out .= '<h1 class="number">' . $atts['number'] . '</h1>';
	$out .= '<div class="service-info-text">';
    $out .= '<p class="bigger">' . $atts['title'] . '</h5>';
    $out .= '<p>' . do_shortcode( $content ) . '</p>';
    $out .= '</div>';
	$out .= '</div>';

    return $out;
}

//////////////////////////////////////////////////////////////////
// Fontawesome Icons
//////////////////////////////////////////////////////////////////
add_shortcode('fontawesome_icon', 'shortcode_fontawesome_icon');
function shortcode_fontawesome_icon($atts, $content = null) {

    $atts = shortcode_atts(array(
		'icon' => '',
        'size' => 'none',
		'color' => '#000000'
		), $atts);

    $str = '';

    $icon_size = '';
    if( $atts['size'] != 'none' ){
        $icon_size = "fa-" . $atts['size'];
    }

    $str .= '<i class="'. $atts['icon'] .' '. $icon_size . '" style="color:' . $atts['color'] . ';"></i>';

    return $str;
}

//////////////////////////////////////////////////////////////////
// Info Box
//////////////////////////////////////////////////////////////////
add_shortcode('clapat_info_box', 'shortcode_clapat_info_box');
function shortcode_clapat_info_box($atts, $content = null) {

    $atts = shortcode_atts(array(
		'header_icon' => '',
        'footer_text' => ''
		), $atts);

    $str = '<div class="hidden-box">';
    $str .= '<div class="header-box"><i class="' . $atts['header_icon'] . '"></i></div>';                            
    $str .= '<div class="content-box">' . do_shortcode( $content ) . '</div>';                            
    $str .= '<div class="footer-box"><p>' . $atts['footer_text'] . '</p></div>';                        
    $str .= '</div>';
	
    return $str;
}

/* End Elements */


//////////////////////////////////////////////////////////////////
// Lightbox Gallery
//////////////////////////////////////////////////////////////////
add_shortcode('clapat_lightbox_gallery', 'shortcode_clapat_lightbox_gallery');
function shortcode_clapat_lightbox_gallery($atts, $content = null) {

    $str = '<div class="collage content-gallery mfp-gallery">';
    $str .= do_shortcode( $content );
    $str .= '</div>';

    return $str;
}

add_shortcode('clapat_lightbox_image', 'shortcode_clapat_lightbox_image');
function shortcode_clapat_lightbox_image($atts, $content = null) {

    $atts = shortcode_atts(array(
		'thumb_img_url'	=> '',
    	'thumb_img_id'  => '',
        'img_url'		=> '',
    	'img_id'    	=> '',
		'alt'			=> ''
    ), $atts );

	$thumb_img_url 	= clapat_get_image_url($atts['thumb_img_id'], $atts['thumb_img_url']);
    $img_url 		= clapat_get_image_url($atts['img_id'], $atts['img_url']);

	$str = '<div class="collage-thumb">';
	$str .= '<a class="mf-zoom" href="' . esc_url( $img_url ) . '">'; 
	$str .= '<img src="' . esc_url( $thumb_img_url ) . '" alt="' . esc_attr( $atts['alt'] ) . '" />'; 
	$str .= '<div class="zoom"></div>';
	$str .= '</a>'; 
	$str .= '</div>';

    return $str;
}


/* Sliders */

//////////////////////////////////////////////////////////////////
// General Slider
//////////////////////////////////////////////////////////////////
add_shortcode('general_slider', 'shortcode_general_slider');
function shortcode_general_slider($atts, $content = null) {

    $str = '<div class="classic-slider flexslider">';
    $str .= '<ul class="slides">';
    $str .= do_shortcode( $content );
    $str .= '</ul>';
    $str .= '</div>';

    return $str;
}

add_shortcode('general_slide', 'shortcode_general_slide');
function shortcode_general_slide($atts, $content = null) {

    $atts = shortcode_atts(array(
        'img_url'	=> '',
    	'img_id'    => '',
        'alt'       => '',
    ), $atts );

    $img_url = clapat_get_image_url($atts['img_id'], $atts['img_url']);
    
    $str = '<li>';
	$str .= '<img src="' . esc_url( $img_url ) . '" alt="' . esc_attr( $atts['alt'] ) . '" />';
    $str .= '</li>';

    return $str;
}

//////////////////////////////////////////////////////////////////
// Carousel
//////////////////////////////////////////////////////////////////
add_shortcode('clapat_carousel', 'shortcode_clapat_carousel');
function shortcode_clapat_carousel($atts, $content = null) {

    $str = '<div class="centerd-carousel-container">';
    $str .= '<div id="centerd-carousel">';
    $str .= do_shortcode( $content );
    $str .= '</div>';
    $str .= '</div>';

    return $str;
}

add_shortcode('clapat_carousel_item', 'shortcode_clapat_carousel_item');
function shortcode_clapat_carousel_item($atts, $content = null) {

    $atts = shortcode_atts(array(
        'img_url'		=> '',
    	'img_id'    	=> '',
        'link'      	=> '',
		'category'  	=> '',
		'caption_type'	=> 'dark'
    ), $atts );

    $img_url = clapat_get_image_url($atts['img_id'], $atts['img_url']);
	
	$content_type = '';
	if( $atts['caption_type'] == 'light' ){
		$content_type = 'light-content';
	}
    
	$str = '<div class="centerd-carousel-item">';
    $str .=	'<a class="animation-link" href="' . $atts['link'] . '" target="_blank">';
    $str .= '<img alt="image" src="' . $img_url . '"/>';
    $str .= '<div class="carousel-caption ' . $content_type . '">';
    $str .= '<span class="item-cat">' . $atts['category'] . '</span>';
    $str .= '<span class="item-title">' . do_shortcode( $content ) . '</span>';
    $str .= '</div>';
    $str .= '</a>';
    $str .= '</div>';

    return $str;
}

//////////////////////////////////////////////////////////////////
// Collage
//////////////////////////////////////////////////////////////////
add_shortcode('clapat_collage', 'shortcode_clapat_collage');
function shortcode_clapat_collage($atts, $content = null) {

    $str = '<div class="collage mfp-gallery">';
    $str .= do_shortcode( $content );
    $str .= '</div>';

    return $str;
}

add_shortcode('clapat_collage_item', 'shortcode_clapat_collage_item');
function shortcode_clapat_collage_item($atts, $content = null) {

    $atts = shortcode_atts(array(
		'thumb_url'	=> '',
    	'thumb_id'  => '',
        'img_url'	=> '',
    	'img_id'    => '',
    ), $atts );

	$thumb_url 	= clapat_get_image_url($atts['thumb_id'], $atts['thumb_url']);
    $img_url 	= clapat_get_image_url($atts['img_id'], $atts['img_url']);
    
	$str = '<div class="collage-thumb">';
    $str .= '<a class="mf-zoom" href="' . $img_url . '">';
    $str .= '<img src="' . $thumb_url . '" alt="img" />';
    $str .= '<div class="thumb-info"><h5>' . do_shortcode( $content ) . '</h5></div>';
    $str .= '</a>';
    $str .= '</div>';

    return $str;
}
/* End Sliders */

/* Google Map */
add_shortcode('soho_map', 'shortcode_soho_map');
function shortcode_soho_map($atts, $content = null) {

    $str = '<!-- Map -->';
	$str .= '<div class="map-shortcode">';
    $str .= '<div class="map-header"></div>';
    $str .= '<div id="map_canvas"></div>';
    $str .= '</div>';
	$str .= '<!--/Map -->';

    return $str;
}
/* End Google Map */

//////////////////////////////////////////////////////////////////
// Our Team
//////////////////////////////////////////////////////////////////
add_shortcode('team', 'shortcode_team');
function shortcode_team($atts, $content = null) {

    $out = '<ul class="our-team">';
    $out .= do_shortcode( $content );
    $out .= '</ul>';

    return $out;
}

add_shortcode('team_member', 'shortcode_team_member');
function shortcode_team_member($atts, $content = null) {

    $atts = shortcode_atts(array(
        'picture' => '',
    	'picture_id' => '',
        'name' => '',
        'title' => '',
        'social_icon1' => '',
        'social_link1_url' => '',
        'social_icon2' => '',
        'social_link2_url' => '',
        'social_icon3' => '',
        'social_link3_url' => ''
    ), $atts );


    $out = '';

    $picture_url = clapat_get_image_url( $atts['picture_id'], $atts['picture'] ); 
    
    $out .= '<li class="team-member" style="background-image:url(' . $picture_url . ');">';
    $out .= '<div class="team-overflow">';

    $out .= '<div class="team-info">';
    $out .= '<p class="team-name bigger">' . $atts['name'] . '</p>';
    $out .= '<p class="team-function">' . $atts['title'] . '</p>';
    $out .= '<p class="team-description">' . $content . '</p>';

    $out .= '<ul class="socials">';
    if($atts['social_icon1'] && $atts['social_link1_url'] ) {
        $out .= '<li><a href="' . $atts['social_link1_url'] . '" target="_blank"><i class="' . $atts['social_icon1'] . '"></i></a></li> ';
    }
    if($atts['social_icon2'] && $atts['social_link2_url']) {
        $out .= '<li><a href="' . $atts['social_link2_url'] . '" target="_blank"><i class="' . $atts['social_icon2'] . '"></i></a></li> ';
    }
    if($atts['social_icon3'] && $atts['social_link3_url']) {
        $out .= '<li><a href="' . $atts['social_link3_url'] . '" target="_blank"><i class="' . $atts['social_icon3'] . '"></i></a></li> ';
    }
    $out .= '</ul>';

    $out .= '</div>'; // div class team info
    $out .= '</div>'; // div class team overflow
    $out .= '</li>'; // li team person

    return $out;
}

add_shortcode('team_carousel', 'shortcode_team_carousel');
function shortcode_team_carousel($atts, $content = null) {

    $out = '<div class="team-carousel">';
    $out .= do_shortcode( $content );
    $out .= '</div>';

    return $out;
}

add_shortcode('team_member_carousel', 'shortcode_team_member_carousel');
function shortcode_team_member_carousel($atts, $content = null) {

    $atts = shortcode_atts(array(
        'picture' => '',
    	'picture_id' => '',
        'name' => '',
        'title' => '',
        'social_icon1' => '',
        'social_link1_url' => '',
        'social_icon2' => '',
        'social_link2_url' => '',
        'social_icon3' => '',
        'social_link3_url' => ''
    ), $atts );


    $out = '';

    $picture_url = clapat_get_image_url( $atts['picture_id'], $atts['picture'] ); 
    
    $out .= '<div class="team-member-carousel" style="background-image:url(' . $picture_url . ');">';
    $out .= '<div class="team-overflow-carousel">';

    $out .= '<div class="inner">';
    $out .= '<p class="bigger">' . $atts['name'] . '</p>';
    $out .= '<p class="team-function">' . $atts['title'] . '</p>';
    
	$out .= '<div class="bottom-team">';
    $out .= '<ul class="socials">';
    if($atts['social_icon1'] && $atts['social_link1_url'] ) {
        $out .= '<li><a href="' . $atts['social_link1_url'] . '" target="_blank"><i class="' . $atts['social_icon1'] . '"></i></a></li> ';
    }
    if($atts['social_icon2'] && $atts['social_link2_url']) {
        $out .= '<li><a href="' . $atts['social_link2_url'] . '" target="_blank"><i class="' . $atts['social_icon2'] . '"></i></a></li> ';
    }
    if($atts['social_icon3'] && $atts['social_link3_url']) {
        $out .= '<li><a href="' . $atts['social_link3_url'] . '" target="_blank"><i class="' . $atts['social_icon3'] . '"></i></a></li> ';
    }
    $out .= '</ul>';
	$out .= '<div class="icon-scroll-line"></div>';
	$out .= '</div>'; // div class bottom team

    $out .= '</div>'; // div class inner
    $out .= '</div>'; // div class team overflow
    $out .= '</div>'; // div team person

    return $out;
}

//////////////////////////////////////////////////////////////////
// Clients
//////////////////////////////////////////////////////////////////
add_shortcode('clients', 'shortcode_clients');
function shortcode_clients($atts, $content = null) {

    $out = '<ul class="clients-table">';
    $out .= do_shortcode( $content );
    $out .= '</ul>';

    return $out;
}

add_shortcode('client', 'shortcode_client');
function shortcode_client($atts, $content = null) {

    $atts = shortcode_atts(array(
        'picture' => '',
    	'picture_id' => '',
        'url' => ''
    ), $atts );


    $out = '';

    $picture_url = clapat_get_image_url( $atts['picture_id'], $atts['picture'] ); 
    
    $out .= '<li>';
	if( $atts['url'] ){
		$out .= '<a href="'. $atts['url'] .'" target="_blank">';
	}
	$out .= '<img src="' . esc_url( $picture_url ) .'" alt="client">';
	if( $atts['url'] ){
		$out .= '</a>';
	}
    $out .= '</li>';
	
    return $out;
}

add_shortcode('clients_carousel', 'shortcode_clients_carousel');
function shortcode_clients_carousel($atts, $content = null) {

    $out = '<div class="clients-carousel">';
    $out .= do_shortcode( $content );
    $out .= '</div>';

    return $out;
}
// end clients

//////////////////////////////////////////////////////////////////
// Testimonials
//////////////////////////////////////////////////////////////////
add_shortcode('testimonials', 'shortcode_testimonials');
function shortcode_testimonials($atts, $content = null) {

    $out = '<div class="text-carousel">';
    $out .= do_shortcode( $content );
    $out .= '</div>';

    return $out;
}

add_shortcode('testimonial', 'shortcode_testimonial');
function shortcode_testimonial($atts, $content = null) {

    $atts = shortcode_atts(array(
        'name' => ''
    ), $atts );


    $out = '';

	$out .= '<div class="text-carousel-item">';
    $out .= '<h3>"' . $content . '"</h3>';
    $out .= '<p>' . $atts['name'] . '</p>';
    $out .= '</div>';
	
    return $out;
}
// end testimonials

//////////////////////////////////////////////////////////////////
// Animated Text
//////////////////////////////////////////////////////////////////
add_shortcode('animated_text', 'shortcode_animated_text');
function shortcode_animated_text($atts, $content = null) {

    $atts = shortcode_atts(array(
        'title' => ''
    ), $atts );


    $out = '';

    $out .= '<div class="animated-module">';
    $out .= '<span class="bigger">' . $atts['title'] . '</span>';
    $out .= '<p>' . $content . '</p>'; 
    $out .= '</div>';
	
    return $out;
}


//////////////////////////////////////////////////////////////////
// Video Popup
//////////////////////////////////////////////////////////////////
add_shortcode('video_popup', 'shortcode_video_popup');
function shortcode_video_popup($atts, $content = null) {

    $atts = shortcode_atts(array(
        'video_url'	    => '',
    ), $atts );

    $str = '<!-- Video PopUp -->';
	$str .= '<div class="play-icon-video">';
	$str .= '<a class="popup-video" href="' . $atts['video_url'] . '">';
	$str .= '<i class="fa fa-play"></i>';
	$str .= '</a>';
	$str .= '</div>';
    $str .= '<!-- Video PopUp -->';

    return $str;
}

//////////////////////////////////////////////////////////////////
// Add shortcodes buttons to tinyMCE
//////////////////////////////////////////////////////////////////

add_action('init',          'init_shortcodes_menu');
add_action('admin_init',    'admin_init_shortcodes_menu');
	
function init_shortcodes_menu(){
	
    if ( ! current_user_can('edit_posts') && ! current_user_can('edit_pages') )
        return;
	
	// register the tinyMCE buttons in case visual composer is not installed 
	// otherwise just use the shortcodes from there
    if( function_exists('vc_map') ){
    	
    	return;
    }
        
    if ( get_user_option('rich_editing') == 'true' )
    {
        add_filter( 'mce_external_plugins', 'add_shortcode_plugins' );
        add_filter( 'mce_buttons', 'register_shortcode_menu_buttons' );
    }
}
	
function add_shortcode_plugins( $plugin_array ){
	
    $plugin_array['SohoShortcodes'] = SOHO_SHORTCODES_DIR_URL . '/include/shortcodes.js';
    return $plugin_array;
}
	
function register_shortcode_menu_buttons( $buttons ){
	
    array_push( $buttons, "|", 'soho_shortcode_button' );
    return $buttons;
}
	
function admin_init_shortcodes_menu(){
	
    wp_localize_script( 'jquery', 'ShortcodeAttributes', array('shortcode_folder' => SOHO_SHORTCODES_DIR_URL . '/include' ) );
}
