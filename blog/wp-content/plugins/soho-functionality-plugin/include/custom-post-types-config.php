<?php
/**
 * Created by Clapat
 * Date: 21/03/16
 */

// Register custom post types
if ( ! function_exists( 'clapat_soho_custom_types' ) ){

    function clapat_soho_custom_types() {

        global $soho_theme_options;

        $custom_slug = null;
		if( isset($soho_theme_options['clapat_soho_portfolio_custom_slug']) && !empty($soho_theme_options['clapat_soho_portfolio_custom_slug']) )
            $custom_slug = $soho_theme_options['clapat_soho_portfolio_custom_slug'];
		
        register_post_type(
            'soho_portfolio',
            array(
                'labels' => array(
                    'name' => __('Portfolio', 'clapat_soho_plugin_text_domain'),
                    'singular_name' => __('Portfolio', 'clapat_soho_plugin_text_domain'),
                    'all_items' => __('Portfolio Items', 'clapat_soho_plugin_text_domain'),
                    'add_new' => __( 'Add New', 'clapat_soho_plugin_text_domain' ),
                    'add_new_item' => __( 'Add New Portfolio Item', 'clapat_soho_plugin_text_domain' ),
                    'edit_item' => __( 'Edit Portfolio Item', 'clapat_soho_plugin_text_domain' ),
                    'new_item' => __( 'New Portfolio Item', 'clapat_soho_plugin_text_domain' ),
                    'view_item' => __( 'View Portfolio Item', 'clapat_soho_plugin_text_domain' ),
                    'search_items' => __( 'Search Portfolio Items', 'clapat_soho_plugin_text_domain' ),
                    'not_found' => __( 'No portfolio items found', 'clapat_soho_plugin_text_domain' ),
                    'not_found_in_trash' => __( 'No portfolio items found in Trash', 'clapat_soho_plugin_text_domain' ),
                    'menu_name' => __( 'Portfolio', 'clapat_soho_plugin_text_domain' ),
                ),
                'rewrite' => array('slug' => $custom_slug, 'with_front' => false),
                'description' => 'Add your Portfolio',
                'menu_icon' =>  'dashicons-portfolio',
                'public' => true,
                'supports' => array('title', 'editor', 'thumbnail'),
            )
        );

        register_taxonomy('portfolio_category', 'soho_portfolio', array('hierarchical' => true, 'label' => __('Categories', 'clapat_soho_plugin_text_domain'), 'query_var' => true, 'rewrite' => true));

        register_post_type(
            'soho_main_slider',
            array(
                'labels' => array(
                    'name' => __('Main Slider', 'clapat_soho_plugin_text_domain'),
                    'singular_name' => __('Main Slider', 'clapat_soho_plugin_text_domain'),
                    'all_items' => __('Slides', 'clapat_soho_plugin_text_domain'),
                    'add_new' => __( 'Add New', 'clapat_soho_plugin_text_domain' ),
                    'add_new_item' => __( 'Add New Slide', 'clapat_soho_plugin_text_domain' ),
                    'edit_item' => __( 'Edit Slide', 'clapat_soho_plugin_text_domain' ),
                    'new_item' => __( 'New Slide', 'clapat_soho_plugin_text_domain' ),
                    'view_item' => __( 'View Slide', 'clapat_soho_plugin_text_domain' ),
                    'search_items' => __( 'Search Slides', 'clapat_soho_plugin_text_domain' ),
                    'not_found' => __( 'No slides found', 'clapat_soho_plugin_text_domain' ),
                    'not_found_in_trash' => __( 'No slides found in Trash', 'clapat_soho_plugin_text_domain' ),
                    'menu_name' => __( 'Main Slider', 'clapat_soho_plugin_text_domain' ),
                ),
                'public'             => false,
                'publicly_queryable' => false,
                'show_ui'            => true,
                'show_in_menu'       => true,
                'menu_icon'          => 'dashicons-images-alt',
                'supports' => array('title', 'editor'),
            )
        );
		
		register_post_type(
            'soho_showcase',
            array(
                'labels' => array(
                    'name' => __('Showcase Slider', 'clapat_soho_plugin_text_domain'),
                    'singular_name' => __('Showcase Slider', 'clapat_soho_plugin_text_domain'),
                    'all_items' => __('Slides', 'clapat_soho_plugin_text_domain'),
                    'add_new' => __( 'Add New', 'clapat_soho_plugin_text_domain' ),
                    'add_new_item' => __( 'Add New Slide', 'clapat_soho_plugin_text_domain' ),
                    'edit_item' => __( 'Edit Slide', 'clapat_soho_plugin_text_domain' ),
                    'new_item' => __( 'New Slide', 'clapat_soho_plugin_text_domain' ),
                    'view_item' => __( 'View Slide', 'clapat_soho_plugin_text_domain' ),
                    'search_items' => __( 'Search Slides', 'clapat_soho_plugin_text_domain' ),
                    'not_found' => __( 'No slides found', 'clapat_soho_plugin_text_domain' ),
                    'not_found_in_trash' => __( 'No slides found in Trash', 'clapat_soho_plugin_text_domain' ),
                    'menu_name' => __( 'Showcase Slider', 'clapat_soho_plugin_text_domain' ),
                ),
                'public'             => false,
                'publicly_queryable' => false,
                'show_ui'            => true,
                'show_in_menu'       => true,
                'menu_icon'          => 'dashicons-images-alt',
                'supports' => array('title', 'editor'),
            )
        );
		
		register_post_type(
            'soho_altshowcase',
            array(
                'labels' => array(
                    'name' => __('Showcase Alternative Slider', 'clapat_soho_plugin_text_domain'),
                    'singular_name' => __('Showcase Alternative Slider', 'clapat_soho_plugin_text_domain'),
                    'all_items' => __('Slides', 'clapat_soho_plugin_text_domain'),
                    'add_new' => __( 'Add New', 'clapat_soho_plugin_text_domain' ),
                    'add_new_item' => __( 'Add New Slide', 'clapat_soho_plugin_text_domain' ),
                    'edit_item' => __( 'Edit Slide', 'clapat_soho_plugin_text_domain' ),
                    'new_item' => __( 'New Slide', 'clapat_soho_plugin_text_domain' ),
                    'view_item' => __( 'View Slide', 'clapat_soho_plugin_text_domain' ),
                    'search_items' => __( 'Search Slides', 'clapat_soho_plugin_text_domain' ),
                    'not_found' => __( 'No slides found', 'clapat_soho_plugin_text_domain' ),
                    'not_found_in_trash' => __( 'No slides found in Trash', 'clapat_soho_plugin_text_domain' ),
                    'menu_name' => __( 'Showcase Alternative Slider', 'clapat_soho_plugin_text_domain' ),
                ),
                'public'             => false,
                'publicly_queryable' => false,
                'show_ui'            => true,
                'show_in_menu'       => true,
                'menu_icon'          => 'dashicons-images-alt',
                'supports' => array('title', 'editor'),
            )
        );
    }

}
add_action('init', 'clapat_soho_custom_types');


// refresh rewrite rules for custom portfolio slugs
if ( ! function_exists( 'clapat_soho_activation_hook' ) ){

    function clapat_soho_activation_hook() {

		clapat_soho_custom_types();
		
        flush_rewrite_rules();
    }
}
register_activation_hook( __FILE__, 'clapat_soho_activation_hook' );


?>
