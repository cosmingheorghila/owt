<?php

$content_type = array( 'dark', 'light' );					
$text_align = array('text-align-left', 'text-align-center', 'text-align-right' );



$clapat_shortcodes = array(

	//columns
    'one_half' => array(
        'name' => __('Column', 'clapat_soho_plugin_text_domain'),
        'attributes' => array(
            'last' => array( 'title' => __('Last Column?', 'clapat_soho_plugin_text_domain'),
                'desc' => '',
                'type' => 'select',
                'values' => array('no', 'yes')
            ),
            'text_align' => array( 'title' => __('Text Alignment', 'clapat_soho_plugin_text_domain'),
                'desc' => '',
                'type' => 'select',
                'values' => $text_align
            )
        ),
        'has_content' => true,
        'default_content' => __('Content goes here', 'clapat_soho_plugin_text_domain')
    ),

    'one_third' => array(
        'name' => __('Column', 'clapat_soho_plugin_text_domain'),
        'attributes' => array(
            'last' => array( 'title' => __('Last Column?', 'clapat_soho_plugin_text_domain'),
                'desc' => '',
                'type' => 'select',
                'values' => array('no', 'yes')
            ),
            'text_align' => array( 'title' => __('Text Alignment', 'clapat_soho_plugin_text_domain'),
                'desc' => '',
                'type' => 'select',
                'values' => $text_align
            )
        ),
        'has_content' => true,
        'default_content' => __('Content goes here', 'clapat_soho_plugin_text_domain')
    ),

    'one_fourth' => array(
        'name' => __('Column', 'clapat_soho_plugin_text_domain'),
        'attributes' => array(
            'last' => array( 'title' => __('Last Column?', 'clapat_soho_plugin_text_domain'),
                'desc' => '',
                'type' => 'select',
                'values' => array('no', 'yes')
            ),
            'text_align' => array( 'title' => __('Text Alignment', 'clapat_soho_plugin_text_domain'),
                'desc' => '',
                'type' => 'select',
                'values' => $text_align
            )
        ),
        'has_content' => true,
        'default_content' => __('Content goes here', 'clapat_soho_plugin_text_domain')
    ),

    'one_fifth' => array(
        'name' => __('Column', 'clapat_soho_plugin_text_domain'),
        'attributes' => array(
            'last' => array( 'title' => __('Last Column?', 'clapat_soho_plugin_text_domain'),
                'desc' => '',
                'type' => 'select',
                'values' => array('no', 'yes')
            ),
            'text_align' => array( 'title' => __('Text Alignment', 'clapat_soho_plugin_text_domain'),
                'desc' => '',
                'type' => 'select',
                'values' => $text_align
            )
        ),
        'has_content' => true,
        'default_content' => __('Content goes here', 'clapat_soho_plugin_text_domain')
    ),

    'one_sixth' => array(
        'name' => __('Column', 'clapat_soho_plugin_text_domain'),
        'attributes' => array(
            'last' => array( 'title' => __('Last Column?', 'clapat_soho_plugin_text_domain'),
                'desc' => '',
                'type' => 'select',
                'values' => array('no', 'yes')
            ),
            'text_align' => array( 'title' => __('Text Alignment', 'clapat_soho_plugin_text_domain'),
                'desc' => '',
                'type' => 'select',
                'values' => $text_align
            )
        ),
        'has_content' => true,
        'default_content' => __('Content goes here', 'clapat_soho_plugin_text_domain')
    ),

    'two_third' => array(
        'name' => __('Column', 'clapat_soho_plugin_text_domain'),
        'attributes' => array(
            'last' => array( 'title' => __('Last Column?', 'clapat_soho_plugin_text_domain'),
                'desc' => '',
                'type' => 'select',
                'values' => array('no', 'yes')
            ),
            'text_align' => array( 'title' => __('Text Alignment', 'clapat_soho_plugin_text_domain'),
                'desc' => '',
                'type' => 'select',
                'values' => $text_align
            )
        ),
        'has_content' => true,
        'default_content' => __('Content goes here', 'clapat_soho_plugin_text_domain')
    ),

    'two_fifth' => array(
        'name' => __('Column', 'clapat_soho_plugin_text_domain'),
        'attributes' => array(
            'last' => array( 'title' => __('Last Column?', 'clapat_soho_plugin_text_domain'),
                'desc' => '',
                'type' => 'select',
                'values' => array('no', 'yes')
            ),
            'text_align' => array( 'title' => __('Text Alignment', 'clapat_soho_plugin_text_domain'),
                'desc' => '',
                'type' => 'select',
                'values' => $text_align
            )
        ),
        'has_content' => true,
        'default_content' => __('Content goes here', 'clapat_soho_plugin_text_domain')
    ),

    'three_fourth' => array(
        'name' => __('Column', 'clapat_soho_plugin_text_domain'),
        'attributes' => array(
            'last' => array( 'title' => __('Last Column?', 'clapat_soho_plugin_text_domain'),
                'desc' => '',
                'type' => 'select',
                'values' => array('no', 'yes')
            ),
            'text_align' => array( 'title' => __('Text Alignment', 'clapat_soho_plugin_text_domain'),
                'desc' => '',
                'type' => 'select',
                'values' => $text_align
            )
        ),
        'has_content' => true,
        'default_content' => __('Content goes here', 'clapat_soho_plugin_text_domain')
    ),

    'three_fifth' => array(
        'name' => __('Column', 'clapat_soho_plugin_text_domain'),
        'attributes' => array(
            'last' => array( 'title' => __('Last Column?', 'clapat_soho_plugin_text_domain'),
                'desc' => '',
                'type' => 'select',
                'values' => array('no', 'yes')
            ),
            'text_align' => array( 'title' => __('Text Alignment', 'clapat_soho_plugin_text_domain'),
                'desc' => '',
                'type' => 'select',
                'values' => $text_align
            )
        ),
        'has_content' => true,
        'default_content' => __('Content goes here', 'clapat_soho_plugin_text_domain')
    ),

    'four_fifth' => array(
        'name' => __('Column', 'clapat_soho_plugin_text_domain'),
        'attributes' => array(
            'last' => array( 'title' => __('Last Column?', 'clapat_soho_plugin_text_domain'),
                'desc' => '',
                'type' => 'select',
                'values' => array('no', 'yes')
            ),
            'text_align' => array( 'title' => __('Text Alignment', 'clapat_soho_plugin_text_domain'),
                'desc' => '',
                'type' => 'select',
                'values' => $text_align
            )
        ),
        'has_content' => true,
        'default_content' => __('Content goes here', 'clapat_soho_plugin_text_domain')
    ),

    'five_sixth' => array(
        'name' => __('Column', 'clapat_soho_plugin_text_domain'),
        'attributes' => array(
            'last' => array( 'title' => __('Last Column?', 'clapat_soho_plugin_text_domain'),
                'desc' => '',
                'type' => 'select',
                'values' => array('no', 'yes')
            ),
            'text_align' => array( 'title' => __('Text Alignment', 'clapat_soho_plugin_text_domain'),
                'desc' => '',
                'type' => 'select',
                'values' => $text_align
            )
        ),
        'has_content' => true,
        'default_content' => __('Content goes here', 'clapat_soho_plugin_text_domain')
    ),
    // end columns
     
	// typo elements
	'title' => array(
        'name' => __('Title', 'clapat_soho_plugin_text_domain'),
        'attributes' => array(
            'size' => array( 'title' => __('Title Size', 'clapat_soho_plugin_text_domain'),
                'desc' => '',
                'type' => 'select',
                'values' => array('h1', 'h2', 'h3', 'h4', 'h5', 'h6')
            ),
            'underline' => array( 'title' => __('Has Underline?', 'clapat_soho_plugin_text_domain'),
                'desc' => __('If the title is underlined or not', 'clapat_soho_plugin_text_domain'),
                'type' => 'select',
                'values' => array('no', 'yes')
            ),
			'big' => array( 'title' => __('Big Title?', 'clapat_soho_plugin_text_domain'),
                'desc' => __('This parameter applies only for H1 headings. If the title is normal or bigger title font size', 'clapat_soho_plugin_text_domain'),
                'type' => 'select',
                'values' => array('no', 'yes')
            )
        ),
        'has_content' => true,
        'default_content' => __('Title', 'clapat_soho_plugin_text_domain')
    ),
    
    'hr' => array(
        'name' => __('Line Divider', 'clapat_soho_plugin_text_domain'),
        'attributes' => array(
            'size' => array( 'title' => __('Size', 'clapat_soho_plugin_text_domain'),
                'desc' => '',
                'type' => 'select',
                'values' => array('normal', 'small')
            )
        ),
        'has_content' => false
    ),
    
    'button' => array(
        'name' => __('Button', 'clapat_soho_plugin_text_domain'),
        'attributes' => array(
            "link" => array(    "title" => __("Button Link", 'clapat_soho_plugin_text_domain'),
                "desc"  => __("URL for the button", 'clapat_soho_plugin_text_domain'),
                "type"  => "text",
                "default" => "http://"
            ),
            "target" => array(  "title" => __("Target Window", 'clapat_soho_plugin_text_domain'),
                "desc" => __("Button link opens in a new or current window", 'clapat_soho_plugin_text_domain'),
                "type" => "select",
                "values" => array("_blank", "_self")
            ),
            "type" => array( "title" => __("Button type", 'clapat_soho_plugin_text_domain'),
                "desc" => "",
                "type" => "select",
                "values" => array("normal", "outline")
            )
        ),
        'has_content' => true,
		'default_content' => __('Button Caption', 'clapat_soho_plugin_text_domain')
    ),
	
	'full_button' => array(
        'name' => __('Full Width Button', 'clapat_soho_plugin_text_domain'),
        'attributes' => array(
            "link" => array(    "title" => __("Button Link", 'clapat_soho_plugin_text_domain'),
                "desc"  => __("URL for the button", 'clapat_soho_plugin_text_domain'),
                "type"  => "text",
                "default" => "http://"
            ),
            "target" => array(  "title" => __("Target Window", 'clapat_soho_plugin_text_domain'),
                "desc" => __("Button link opens in a new or current window", 'clapat_soho_plugin_text_domain'),
                "type" => "select",
                "values" => array("_blank", "_self")
            ),
        ),
        'has_content' => true,
		'default_content' => __('Button Caption', 'clapat_soho_plugin_text_domain')
    ),
	
	'animated_link' => array(
        'name' => __('Animated Link', 'clapat_soho_plugin_text_domain'),
        'attributes' => array(
            "link" => array(    "title" => __("Link", 'clapat_soho_plugin_text_domain'),
                "desc"  => __("URL of the link", 'clapat_soho_plugin_text_domain'),
                "type"  => "text",
                "default" => "http://"
            ),
            "target" => array(  "title" => __("Target Window", 'clapat_soho_plugin_text_domain'),
                "desc" => __("Link opens in a new or current window", 'clapat_soho_plugin_text_domain'),
                "type" => "select",
                "values" => array("_blank", "_self")
            )
        ),
        'has_content' => true,
		'default_content' => __('Link Caption', 'clapat_soho_plugin_text_domain')
    ),
    // end typo elements
    
    'accordion' => array(
        'name' => __('Accordion', 'clapat_soho_plugin_text_domain'),
        'sub_items' => array(
            'accordion_item' => array(  'name' => __('Accordion Item', 'clapat_soho_plugin_text_domain'),
                'attributes' => array(
                    'title' => array( 'title' => __('Title', 'clapat_soho_plugin_text_domain'),
                        'desc' => '',
                        'type' => 'text',
                        'default' => __('Accordion Title', 'clapat_soho_plugin_text_domain')
                    )
                ),
                'has_content' => true,
                'default_content' => __('Accordion Content Here', 'clapat_soho_plugin_text_domain')
            )
        ),
        'has_content' => false
    ),
    
    'tabs' => array(    'name' => __('Tabs', 'clapat_soho_plugin_text_domain'),
        'sub_items' => array(
            'tab_item' => array(  'name' => __('Tab Item', 'clapat_soho_plugin_text_domain'),
                'attributes' => array(
                    'tab_name' => array( 'title' => __('Tab Name', 'clapat_soho_plugin_text_domain'),
                        'desc' => '',
                        'type' => 'text',
                        'default' => __('Tab Title', 'clapat_soho_plugin_text_domain')
                    )
                ),
                'has_content' => true,
                'default_content' => __('Tab Content Here', 'clapat_soho_plugin_text_domain')
            )
        ),
        'has_content' => false
    ),
    
    'toggle' => array(
        'name' => __('Toggle', 'clapat_soho_plugin_text_domain'),
        'attributes' => array(
            'title' => array( 'title' => __('Title', 'clapat_soho_plugin_text_domain'),
                'desc' => '',
                'type' => 'text',
                'default' => ''
            )
        ),
        'has_content' => true,
        'default_content' => __('Toggle Content Here', 'clapat_soho_plugin_text_domain')
    ),
        
    'alert' => array(
        'name' => __('Alert Box', 'clapat_soho_plugin_text_domain'),
        'attributes' => array(
            'color' => array( 'title' => __('Color', 'clapat_soho_plugin_text_domain'),
                'desc' => __('Background color for the alert box', 'clapat_soho_plugin_text_domain'),
                'type' => 'select',
                'values' => array("red", "blue", "yellow", "green")
            )
        ),
        'has_content' => true,
        'default_content' => __('YOUR MESSAGE HERE', 'clapat_soho_plugin_text_domain')
    ),
    
    'progress' => array(
        'name' => __('Progress Bars', 'clapat_soho_plugin_text_domain'),
        'sub_items' => array(
            'progress_bar' => array(  'name' => __('Progress Bar', 'clapat_soho_plugin_text_domain'),
                'attributes' => array(
                    'percentage' => array( 'title' => __('Percentage', 'clapat_soho_plugin_text_domain'),
                        'desc' => 'Progress Bar Percentage',
                        'type' => 'text',
                        'default' => '100'
                    ),
                ),
                'has_content' => true,
                'default_content' => __('Web Design', 'clapat_soho_plugin_text_domain')
            )
        ),
        'has_content' => false,
    ),
    
    'pricing_table' => array(  'name' => __('Pricing Table', 'clapat_soho_plugin_text_domain'),
        'sub_items' => array(
            'pricing_row' => array( 
								'name' => __('Pricing Row', 'clapat_soho_plugin_text_domain'),
								'has_content' => true,
								'default_content' => __('Info about priced services here.', 'clapat_soho_plugin_text_domain')
							)
						),
		'attributes' => array(
                    'active' => array( 'title' => __('Active', 'clapat_soho_plugin_text_domain'),
                        'desc' => __('If the pricing table is highlighted or not',  'clapat_soho_plugin_text_domain'),
                        'type' => 'select',
                        'values' => array('no', 'yes')
                    ),
                    'icon' => array( 'title' => __('Icon', 'clapat_soho_plugin_text_domain'),
                        'desc' => '',
                        'type' => 'icon',
                        'default' => ''
                    ),
					'title' => array( 'title' => __('Title', 'clapat_soho_plugin_text_domain'),
                        'desc' => __('Pricing table title. Usually the name of the category of services being priced', 'clapat_soho_plugin_text_domain'),
                        'type' => 'text',
                        'default' => ''
                    ),
					'price' => array( 'title' => __('Price', 'clapat_soho_plugin_text_domain'),
                        'desc' => __('Price for the services being offered', 'clapat_soho_plugin_text_domain'),
                        'type' => 'text',
                        'default' => '99.99'
                    ),
					'currency' => array( 'title' => __('Currency', 'clapat_soho_plugin_text_domain'),
                        'desc' => __('Pricing table title. Usually the name of the category of services being priced', 'clapat_soho_plugin_text_domain'),
                        'type' => 'text',
                        'default' => '$'
                    ),
					'time' => array( 'title' => __('Time', 'clapat_soho_plugin_text_domain'),
                        'desc' => __('Period of time the price is applied for', 'clapat_soho_plugin_text_domain'),
                        'type' => 'text',
                        'default' => 'per month'
                    ),
					'url' => array( 'title' => __('Details URL', 'clapat_soho_plugin_text_domain'),
                        'desc' => __('Url offering more detils about the service(s)', 'clapat_soho_plugin_text_domain'),
                        'type' => 'text',
                        'default' => 'http://'
                    ),
					'target' => array( 'title' => __('Details URL target', 'clapat_soho_plugin_text_domain'),
                        'desc' => __('Target window for details URL',  'clapat_soho_plugin_text_domain'),
                        'type' => 'select',
                        'values' => array('_blank', '_self')
                    ),
					'buy_now_text' => array( 'title' => __('Buy Now Caption', 'clapat_soho_plugin_text_domain'),
                        'desc' => __('Caption or slogan for the button displayed at the bottom of the pricing table', 'clapat_soho_plugin_text_domain'),
                        'type' => 'text',
                        'default' => 'Buy Now'
                    )
                ),
		'require_icon' => true,			
        'has_content' => false
    ),
    
    'counters' => array(
        'name' => __('Counters', 'clapat_soho_plugin_text_domain'),
        'sub_items' => array(
            'counter' => array(
                'name' => __('Counter Box', 'clapat_soho_plugin_text_domain'),
                'attributes' => array(
                                "counts" => array(  "title" => __("Counts", 'clapat_soho_plugin_text_domain'),
                                    "desc" => __("Number of counts", 'clapat_soho_plugin_text_domain'),
                                    "type" => "text",
                                    "default" => "100"
                                )
                ),
                'has_content' => true,
                'default_content' => __('TITLE GOES HERE', 'clapat_soho_plugin_text_domain')
            ),
        )
    ),
    
    'radial_counter' => array(
        'name' => __('Radial Counter Box', 'clapat_soho_plugin_text_domain'),
        'attributes' => array(
            "counts" => array(  "title" => __("Counts", 'clapat_soho_plugin_text_domain'),
                "desc" => __("Number of counts", 'clapat_soho_plugin_text_domain'),
                "type" => "text",
                "default" => "100"
            ),
            "title" => array(  "title" => __("Title", 'clapat_soho_plugin_text_domain'),
                "desc" => "",
                "type" => "text",
                "default" => ""
            )
        ),
        'has_content' => true,
        'default_content' => __('DESCRIPTION GOES HERE', 'clapat_soho_plugin_text_domain')
    ),
    
    'parallax_quote' => array(
        'name' => __('Parallax Quote', 'clapat_soho_plugin_text_domain'),
        'attributes' => array(  'author' => array( 'title' => __('Author', 'clapat_soho_plugin_text_domain'),
            'desc' => __('Author of the quote', 'clapat_soho_plugin_text_domain'),
            'type' => 'text',
            'default' => 'John Doe'
        )
        ),
        'has_content' => true,
        'default_content' => __('The Quote Here', 'clapat_soho_plugin_text_domain' )
    ),
    
    'service' => array(
        'name' => __('Service Box', 'clapat_soho_plugin_text_domain'),
        'attributes' => array(
            "icon" => array(  "title" => __("Icon", 'clapat_soho_plugin_text_domain'),
                "desc" => __("Icon displayed within service box", 'clapat_soho_plugin_text_domain'),
                "type" => "icon",
                "default" => ""
            ),
            "title" => array(  "title" => __("Service Title", 'clapat_soho_plugin_text_domain'),
                "desc" => __("Title of the service", 'clapat_soho_plugin_text_domain'),
                "type" => "text",
                "default" => ""
            )
        ),
        'require_icon' => true,
        'has_content' => true,
        'default_content' => __('Service Description', 'clapat_soho_plugin_text_domain')
    ),
	
	'service_number' => array(
        'name' => __('Numbered Service Box', 'clapat_soho_plugin_text_domain'),
        'attributes' => array(
            "number" => array(  "title" => __("Number", 'clapat_soho_plugin_text_domain'),
                "desc" => __("Number displayed within service box", 'clapat_soho_plugin_text_domain'),
                "type" => "text",
                "default" => ""
            ),
            "title" => array(  "title" => __("Service Title", 'clapat_soho_plugin_text_domain'),
                "desc" => __("Title of the service", 'clapat_soho_plugin_text_domain'),
                "type" => "text",
                "default" => ""
            )
        ),
        'has_content' => true,
        'default_content' => __('Service Description', 'clapat_soho_plugin_text_domain')
    ),
    
    'fontawesome_icon' => array(
        'name' => __('FontAwesome Icon', 'clapat_soho_plugin_text_domain'),
        'attributes' => array(
            "icon" => array(  "title" => __("Icon", 'clapat_soho_plugin_text_domain'),
                "desc" => __('Cannot find the icon in the list? You can type directly the name of the icon in the edit box. The complete and latest list: <a href="http://fortawesome.github.io/Font-Awesome/icons/" target="_blank">http://fortawesome.github.io/Font-Awesome/icons/</a>', 'clapat_soho_plugin_text_domain'),
                "type" => "icon",
                "default" => ""
            ),
            "size" => array(  "title" => __("Size", 'clapat_soho_plugin_text_domain'),
                "desc" => __('Icon size relative to their container. See <a href="http://fortawesome.github.io/Font-Awesome/examples/#larger" target="_blank">http://fortawesome.github.io/Font-Awesome/examples/#larger</a> for more information', 'clapat_soho_plugin_text_domain'),
                "type" => "select",
                "values" => array('none', 'lg', '2x', '3x', '4x', '5x')
            ),
            "color" => array(  "title" => __("Color", 'clapat_soho_plugin_text_domain'),
                "desc" => __("Icon color", 'clapat_soho_plugin_text_domain'),
                "type" => "text",
                "default" => "#000000"
            )
        ),
        'require_icon' => true,
        'has_content' => false
    ),
	
	'clapat_info_box' => array(
        'name' => __('Info Box', 'clapat_soho_plugin_text_domain'),
        'attributes' => array(
            "header_icon" => array(  "title" => __("Header Icon", 'clapat_soho_plugin_text_domain'),
                "desc" => __('Info box header icon. Type directly the name of the icon in the edit box. The complete and latest list: <a href="http://fortawesome.github.io/Font-Awesome/icons/" target="_blank">http://fortawesome.github.io/Font-Awesome/icons/</a>', 'clapat_soho_plugin_text_domain'),
                "type" => "text",
                "default" => ""
            ),
            "footer_text" => array(  "title" => __("Footer", 'clapat_soho_plugin_text_domain'),
                "desc" => __('Info box footer text', 'clapat_soho_plugin_text_domain'),
                "type" => "text",
                "default" => ""
            )
        ),
        'has_content' => true,
		'default_content' => __('Info Box Content', 'clapat_soho_plugin_text_domain')
    ),
    // end elements
    
	'clapat_lightbox_gallery' => array(
        'name' => __('Lightbox Image Gallery', 'clapat_soho_plugin_text_domain'),
        'sub_items' => array(
            'clapat_lightbox_image' => array(  'name' => __('Lightbox Image', 'clapat_soho_plugin_text_domain'),
                'attributes' => array(
					'thumb_img_url' => array( 'title' => __('Thumbnail Image URL', 'clapat_soho_plugin_text_domain'),
                        'desc' => '',
                        'type' => 'text',
                        'default' => ''
                    ),
                    'img_url' => array( 'title' => __('Full Image URL', 'clapat_soho_plugin_text_domain'),
                        'desc' => '',
                        'type' => 'text',
                        'default' => ''
                    ),
                    'alt' => array( 'title' => __('Thumbnail Image ALT', 'clapat_soho_plugin_text_domain'),
                        'desc' => '',
                        'type' => 'text',
                        'default' => ''
                    ),
                ),
                'has_content' => false
            )
        ),
        'has_content' => false
    ),
	
	'video_popup' => array(
        'name' => __('Video Popup', 'clapat_soho_plugin_text_domain'),
        'attributes' => array(  'video_url' => array( 'title' => __('Video URL', 'clapat_soho_plugin_text_domain'),
                                    'desc' => __('The url of the video', 'clapat_soho_plugin_text_domain'),
                                    'type' => 'text',
                                    'default' => ''
                                )
                        ),
        'has_content' => false,
        'require_icon' => false,
    ),
	
    //sliders
    'general_slider' => array(
        'name' => __('Normal Image Slider', 'clapat_soho_plugin_text_domain'),
        'sub_items' => array(
            'general_slide' => array(  'name' => __('Slide', 'clapat_soho_plugin_text_domain'),
                'attributes' => array(
                    'img_url' => array( 'title' => __('Slider Image URL', 'clapat_soho_plugin_text_domain'),
                        'desc' => '',
                        'type' => 'text',
                        'default' => ''
                    ),
                    'alt' => array( 'title' => __('Image ALT', 'clapat_soho_plugin_text_domain'),
                        'desc' => '',
                        'type' => 'text',
                        'default' => ''
                    ),
                ),
                'has_content' => false
            )
        ),
        'has_content' => false
    ),
	'clapat_carousel' => array(
        'name' => __('Carousel Slider', 'clapat_soho_plugin_text_domain'),
        'sub_items' => array(
            'clapat_carousel_item' => array(  'name' => __('Carousel Item', 'clapat_soho_plugin_text_domain'),
                'attributes' => array(
                    'img_url' => array( 'title' => __('Image URL', 'clapat_soho_plugin_text_domain'),
                        'desc' => '',
                        'type' => 'text',
                        'default' => ''
                    ),
                    'link' => array( 'title' => __('Item Links to', 'clapat_soho_plugin_text_domain'),
                        'desc' => '',
                        'type' => 'text',
                        'default' => 'http://'
                    ),
					'category' => array( 'title' => __('Item\'s category', 'clapat_soho_plugin_text_domain'),
                        'desc' => '',
                        'type' => 'text',
                        'default' => ''
                    ),
					"caption_type" => array(  "title" => __("Caption Type", 'clapat_soho_plugin_text_domain'),
							"desc" => __('The type of foreground for text captions. Depends on the image background.'),
							"type" => "select",
							"values" => array('dark', 'light')
					),
                ),
                'has_content' => true,
				'default_content' => __('Title goes here', 'clapat_soho_plugin_text_domain')
            )
        ),
        'has_content' => false
    ),
	'clapat_collage' => array(
        'name' => __('Collage', 'clapat_soho_plugin_text_domain'),
        'sub_items' => array(
            'clapat_collage_item' => array(  'name' => __('Collage Item', 'clapat_soho_plugin_text_domain'),
                'attributes' => array(
					'thumb_url' => array( 'title' => __('Thumbnail Image URL', 'clapat_soho_plugin_text_domain'),
                        'desc' => '',
                        'type' => 'text',
                        'default' => ''
                    ),
                    'img_url' => array( 'title' => __('Zoom Image URL', 'clapat_soho_plugin_text_domain'),
                        'desc' => '',
                        'type' => 'text',
                        'default' => ''
                    ),
                ),
                'has_content' => true,
				'default_content' => __('Title goes here - will be wrappped in a h5 tag', 'clapat_soho_plugin_text_domain')
            )
        ),
        'has_content' => false
    ),
    //end sliders
	
	//team members
	'team' => array(
        'name' => __('Team Members', 'clapat_soho_plugin_text_domain'),
        'sub_items' => array(
            'team_member' => array(  'name' => __('Team Member', 'clapat_soho_plugin_text_domain'),
                'attributes' => array(
                    'name' => array( 'title' => __('Team Member Name', 'clapat_soho_plugin_text_domain'),
                        'desc' => '',
                        'type' => 'text',
                        'default' => ''
                    ),
                    'title' => array( 'title' => __('Job Title', 'clapat_soho_plugin_text_domain'),
                        'desc' => '',
                        'type' => 'text',
                        'default' => ''
                    ),
                    'picture' => array( 'title' => __('Picture', 'clapat_soho_plugin_text_domain'),
                        'desc' => __('URL or path to team member\'s picture', 'clapat_soho_plugin_text_domain'),
                        'type' => 'text',
                        'default' => ''
                    ),
                    'social_icon1' => array( 'title' => __('Social Icon 1', 'clapat_soho_plugin_text_domain'),
                        'desc' => '',
                        'type' => 'icon',
                    	'default' => 'fa fa-facebook'
                    ),
                    'social_link1_url' => array( 'title' => __('Social Link 1 URL', 'clapat_soho_plugin_text_domain'),
                        'desc' => '',
                        'type' => 'text',
                        'default' => ''
                    ),
                    'social_icon2' => array( 'title' => __('Social Icon 2', 'clapat_soho_plugin_text_domain'),
                        'desc' => '',
                        'type' => 'icon',
                    	'default' => 'fa fa-twitter'
                    ),
                    'social_link2_url' => array( 'title' => __('Social Link 2 URL', 'clapat_soho_plugin_text_domain'),
                        'desc' => '',
                        'type' => 'text',
                        'default' => ''
                    ),
                    'social_icon3' => array( 'title' => __('Social Icon 1', 'clapat_soho_plugin_text_domain'),
                        'desc' => '',
                        'type' => 'icon',
                    	'default' => 'fa fa-linkedin'
                    ),
                    'social_link3_url' => array( 'title' => __('Social Link 3 URL', 'clapat_soho_plugin_text_domain'),
                        'desc' => '',
                        'type' => 'text',
                        'default' => ''
                    ),
                ),
                'has_content' => true,
                'require_icon' => true,
                'default_content' => __('Team member description', 'clapat_soho_plugin_text_domain')
            )
        ),
        'has_content' => false,
    ),
	'team_carousel' => array(
        'name' => __('Team Members', 'clapat_soho_plugin_text_domain'),
        'sub_items' => array(
            'team_member_carousel' => array(  'name' => __('Team Member', 'clapat_soho_plugin_text_domain'),
                'attributes' => array(
                    'name' => array( 'title' => __('Team Member Name', 'clapat_soho_plugin_text_domain'),
                        'desc' => '',
                        'type' => 'text',
                        'default' => ''
                    ),
                    'title' => array( 'title' => __('Job Title', 'clapat_soho_plugin_text_domain'),
                        'desc' => '',
                        'type' => 'text',
                        'default' => ''
                    ),
                    'picture' => array( 'title' => __('Picture', 'clapat_soho_plugin_text_domain'),
                        'desc' => __('URL or path to team member\'s picture', 'clapat_soho_plugin_text_domain'),
                        'type' => 'text',
                        'default' => ''
                    ),
                    'social_icon1' => array( 'title' => __('Social Icon 1', 'clapat_soho_plugin_text_domain'),
                        'desc' => '',
                        'type' => 'icon',
                    	'default' => 'fa fa-facebook'
                    ),
                    'social_link1_url' => array( 'title' => __('Social Link 1 URL', 'clapat_soho_plugin_text_domain'),
                        'desc' => '',
                        'type' => 'text',
                        'default' => ''
                    ),
                    'social_icon2' => array( 'title' => __('Social Icon 2', 'clapat_soho_plugin_text_domain'),
                        'desc' => '',
                        'type' => 'icon',
                    	'default' => 'fa fa-twitter'
                    ),
                    'social_link2_url' => array( 'title' => __('Social Link 2 URL', 'clapat_soho_plugin_text_domain'),
                        'desc' => '',
                        'type' => 'text',
                        'default' => ''
                    ),
                    'social_icon3' => array( 'title' => __('Social Icon 1', 'clapat_soho_plugin_text_domain'),
                        'desc' => '',
                        'type' => 'icon',
                    	'default' => 'fa fa-linkedin'
                    ),
                    'social_link3_url' => array( 'title' => __('Social Link 3 URL', 'clapat_soho_plugin_text_domain'),
                        'desc' => '',
                        'type' => 'text',
                        'default' => ''
                    ),
                ),
                'has_content' => false,
                'require_icon' => true
            )
        ),
        'has_content' => false,
    ),
	//end team members
    
	// clients
	'clients' => array(
        'name' => __('Clients', 'clapat_soho_plugin_text_domain'),
        'sub_items' => array(
            'client' => array(  'name' => __('Client', 'clapat_soho_plugin_text_domain'),
                'attributes' => array(
                    'picture' => array( 'title' => __('Picture', 'clapat_soho_plugin_text_domain'),
                        'desc' => __('URL or path to client\'s logo or picture', 'clapat_soho_plugin_text_domain'),
                        'type' => 'text',
                        'default' => ''
                    ),
                    'url' => array( 'title' => __('Client URL', 'clapat_soho_plugin_text_domain'),
                        'desc' => '',
                        'type' => 'text',
                        'default' => ''
                    ),
                ),
                'has_content' => false,
                'require_icon' => false
            )
        ),
        'has_content' => false,
    ),
	'clients_carousel' => array(
        'name' => __('Clients Carousel', 'clapat_soho_plugin_text_domain'),
        'sub_items' => array(
            'client' => array(  'name' => __('Client', 'clapat_soho_plugin_text_domain'),
                'attributes' => array(
                    'picture' => array( 'title' => __('Picture', 'clapat_soho_plugin_text_domain'),
                        'desc' => __('URL or path to client\'s logo or picture', 'clapat_soho_plugin_text_domain'),
                        'type' => 'text',
                        'default' => ''
                    ),
                    'url' => array( 'title' => __('Client URL', 'clapat_soho_plugin_text_domain'),
                        'desc' => '',
                        'type' => 'text',
                        'default' => ''
                    ),
                ),
                'has_content' => false,
                'require_icon' => false
            )
        ),
        'has_content' => false,
    ),
	// end clients
	'testimonials' => array(
        'name' => __('Testimonials Carousel', 'clapat_soho_plugin_text_domain'),
        'sub_items' => array(
            'testimonial' => array(  'name' => __('Testimonial', 'clapat_soho_plugin_text_domain'),
                'attributes' => array(
                    'name' => array( 'title' => __('Client Name', 'clapat_soho_plugin_text_domain'),
                        'desc' => '',
                        'type' => 'text',
                        'default' => ''
                    ),
                ),
                'has_content' => true,
                'require_icon' => false
            )
        ),
        'has_content' => false,
    ),			
);

?>