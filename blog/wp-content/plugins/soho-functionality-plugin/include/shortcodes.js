(function() {
    tinymce.create('tinymce.plugins.SohoShortcodes', {
        init : function(ed, url) {
            ed.addButton('soho_shortcode_button', {
				type: 'menubutton',
				text: 'S',
				title: 'Insert Soho Shortcode',
                icon: false,
				
				menu: [
				
					// Typo Elements
					{
						text: 'Typo Elements',
						menu: [
						
							{
								text: 'Title',
								onclick: function () {
									tb_show("Add Title", ShortcodeAttributes.shortcode_folder + '/shortcodes_popup.php?&sc=title&width=700&height=600');
								}
							},
														
							{
								text: 'Breaking Line',
								onclick: function () {
									ed.insertContent('[br]');
								}
							},
							
							{
								text: 'Line Divider',
								onclick: function () {
									tb_show("Add Line Divider", ShortcodeAttributes.shortcode_folder + '/shortcodes_popup.php?&sc=hr&width=700&height=600');
								}
							},
							
							{
								text: 'Buttons',
								onclick: function () {
									tb_show("Add Button", ShortcodeAttributes.shortcode_folder + '/shortcodes_popup.php?&sc=button&width=700&height=600');
								}
							},
							
							{
								text: 'Full Width Buttons',
								onclick: function () {
									tb_show("Add Full Width Button", ShortcodeAttributes.shortcode_folder + '/shortcodes_popup.php?&sc=full_button&width=700&height=600');
								}
							},
							
							{
								text: 'Space Between Buttons',
								onclick: function () {
									ed.insertContent('[space_between_buttons]');
								}
							},
							
							{
								text: 'Animated Link',
								onclick: function () {
									tb_show("Add Animated Link", ShortcodeAttributes.shortcode_folder + '/shortcodes_popup.php?&sc=animated_link&width=700&height=600');
								}
							},
							
							{
								text: 'Animated Text',
								onclick: function () {
									tb_show("Add Animated Text", ShortcodeAttributes.shortcode_folder + '/shortcodes_popup.php?&sc=animated_text&width=700&height=600');
								}
							},
							
						]
					},
					
					// Columns
					{
						text: 'Columns',
                        
						menu: [
						
							{
								text: 'One Half',
								onclick: function () {
									tb_show("Add One Half Column", ShortcodeAttributes.shortcode_folder + '/shortcodes_popup.php?&sc=one_half&width=400&height=500');
								}
							},
							
							{
								text: 'One Third',
								onclick: function () {
									tb_show("Add One Third Column", ShortcodeAttributes.shortcode_folder + '/shortcodes_popup.php?&sc=one_third&width=400&height=500');
								}
							},
							
							{
								text: 'One Fourth',
								onclick: function () {
									tb_show("Add One Fourth Column", ShortcodeAttributes.shortcode_folder + '/shortcodes_popup.php?&sc=one_fourth&width=400&height=500');
								}
							},
							
							{
								text: 'One Fifth',
								onclick: function () {
									tb_show("Add One Fifth Column", ShortcodeAttributes.shortcode_folder + '/shortcodes_popup.php?&sc=one_fifth&width=400&height=500');
								}
							},
							
							{
								text: 'One Sixth',
								onclick: function () {
									tb_show("Add One Sixth Column", ShortcodeAttributes.shortcode_folder + '/shortcodes_popup.php?&sc=one_sixth&width=400&height=500');
								}
							},

							{
								text: 'Two Third',
								onclick: function () {
									tb_show("Add Two Third Column", ShortcodeAttributes.shortcode_folder + '/shortcodes_popup.php?&sc=two_third&width=400&height=500');
								}
							},

							{
								text: 'Two Fifth',
								onclick: function () {
									tb_show("Add Two Fifth Column", ShortcodeAttributes.shortcode_folder + '/shortcodes_popup.php?&sc=two_fifth&width=400&height=500');
								}
							},

							{
								text: 'Three Fourth',
								onclick: function () {
									tb_show("Add Three Fourth Column", ShortcodeAttributes.shortcode_folder + '/shortcodes_popup.php?&sc=three_fourth&width=400&height=500');
								}
							},

							{
								text: 'Three Fifth',
								onclick: function () {
									tb_show("Add Three Fifth Column", ShortcodeAttributes.shortcode_folder + '/shortcodes_popup.php?&sc=three_fifth&width=400&height=500');
								}
							},

							{
								text: 'Four Fifth',
								onclick: function () {
									tb_show("Add Four Fifth Column", ShortcodeAttributes.shortcode_folder + '/shortcodes_popup.php?&sc=four_fifth&width=400&height=500');
								}
							},

							{
								text: 'Five Sixth',
								onclick: function () {
									tb_show("Add Five Sixth Column", ShortcodeAttributes.shortcode_folder + '/shortcodes_popup.php?&sc=five_sixth&width=400&height=500');
								}
							}
							
						]
					},
					
					// Elements
					{
						text: 'Elements',
                        menu: [
							                          
							{
								text: 'Accordions',
								onclick: function () {
										tb_show("Add Accordion", ShortcodeAttributes.shortcode_folder + '/shortcodes_popup.php?&sc=accordion&width=700&height=600');
								}
							},
							
							{
								text: 'Tabs',
								onclick: function () {
									tb_show("Add Tab", ShortcodeAttributes.shortcode_folder + '/shortcodes_popup.php?&sc=tabs&width=700&height=600');
								}
							},
							
							{
								text: 'Toggles',
								onclick: function () {
									tb_show("Add Toggle", ShortcodeAttributes.shortcode_folder + '/shortcodes_popup.php?&sc=toggle&width=700&height=600');
								}
							},

							{
								text: 'Alert Box',
								onclick: function () {
									tb_show("Add Alert Box", ShortcodeAttributes.shortcode_folder + '/shortcodes_popup.php?&sc=alert&width=400&height=500');
								}
							},
							
							{
								text: 'Progress Bar',
								onclick: function () {
									tb_show("Add Progress Bar", ShortcodeAttributes.shortcode_folder + '/shortcodes_popup.php?&sc=progress&width=700&height=600');
								}
							},
							
							{
								text: 'Pricing Table',
								onclick: function () {
									tb_show("Add Pricing Table", ShortcodeAttributes.shortcode_folder + '/shortcodes_popup.php?&sc=pricing_table&width=700&height=600');
								}
							},
							
							{
								text: 'Counters',
								onclick: function () {
									tb_show("Add Counters Box", ShortcodeAttributes.shortcode_folder + '/shortcodes_popup.php?&sc=counters&width=400&height=500');
								}
							},
							
							{
								text: 'Radial Counter',
								onclick: function () {
									tb_show("Add Radial Counter Box", ShortcodeAttributes.shortcode_folder + '/shortcodes_popup.php?&sc=radial_counter&width=400&height=500');
								}
							},
							
							{
								text: 'Icon Service',
								onclick: function () {
									tb_show("Add Service Box", ShortcodeAttributes.shortcode_folder + '/shortcodes_popup.php?&sc=service&width=400&height=500');
								}
							},
							
							{
								text: 'Number Service',
								onclick: function () {
									tb_show("Add Service Box", ShortcodeAttributes.shortcode_folder + '/shortcodes_popup.php?&sc=service_number&width=400&height=500');
								}
							},
							
							{
								text: 'Video Popup',
								onclick: function () {
									tb_show("Add Video Popup", ShortcodeAttributes.shortcode_folder + '/shortcodes_popup.php?&sc=video_popup&width=400&height=500');
								}
							},
							
							{
								text: 'FontAwesome Icon',
								onclick: function () {
									tb_show("Add FontAwesome Icon", ShortcodeAttributes.shortcode_folder + '/shortcodes_popup.php?&sc=fontawesome_icon&width=400&height=500');
								}
							},
							
							{
								text: 'Info Box',
								onclick: function () {
									tb_show("Add Info Box", ShortcodeAttributes.shortcode_folder + '/shortcodes_popup.php?&sc=clapat_info_box&width=400&height=500');
								}
							},
							
							{
								text: 'Lightbox Image Gallery',
								onclick: function () {
									tb_show("Add Lightbox Image Gallery", ShortcodeAttributes.shortcode_folder + '/shortcodes_popup.php?&sc=clapat_lightbox_gallery&width=700&height=600');
								}
							},
							
							{
								text: 'Map',
								onclick: function () {
									ed.insertContent('[soho_map]');
								}
							},
							
							{
								text: 'Testimonials',
								onclick: function () {
									tb_show("Add Testimonials", ShortcodeAttributes.shortcode_folder + '/shortcodes_popup.php?&sc=testimonials&width=700&height=600');
								}
							},
														
						]
					},
					
					{
						text: 'Team',
                        menu: [
						
							{
								text: 'Team Members',
								onclick: function () {
									tb_show("Add Team Members", ShortcodeAttributes.shortcode_folder + '/shortcodes_popup.php?&sc=team&width=700&height=600');
								}
							},
							
							{
								text: 'Team Members - Carousel',
								onclick: function () {
									tb_show("Add Team Members", ShortcodeAttributes.shortcode_folder + '/shortcodes_popup.php?&sc=team_carousel&width=700&height=600');
								}
							},
							
						]
					},
					
					{
						text: 'Clients',
                        menu: [
						
							{
								text: 'Clients',
								onclick: function () {
									tb_show("Add Clients", ShortcodeAttributes.shortcode_folder + '/shortcodes_popup.php?&sc=clients&width=700&height=600');
								}
							},
							
							{
								text: 'Clients - Carousel',
								onclick: function () {
									tb_show("Add Clients", ShortcodeAttributes.shortcode_folder + '/shortcodes_popup.php?&sc=clients_carousel&width=700&height=600');
								}
							},
							
						]
					},
					
					{
						text: 'Sliders',
                        menu: [
						
							{
								text: 'General Slider',
								onclick: function () {
									tb_show("Add General Slider", ShortcodeAttributes.shortcode_folder + '/shortcodes_popup.php?&sc=general_slider&width=700&height=600');
								}
							},
							
							{
								text: 'Carousel',
								onclick: function () {
									tb_show("Add Carousel", ShortcodeAttributes.shortcode_folder + '/shortcodes_popup.php?&sc=clapat_carousel&width=700&height=600');
								}
							},
							
							{
								text: 'Collage',
								onclick: function () {
									tb_show("Add Collage", ShortcodeAttributes.shortcode_folder + '/shortcodes_popup.php?&sc=clapat_collage&width=700&height=600');
								}
							},
							
						]
					}
					
				]
            });
             
        },
        
		getInfo: function () {
            return {
        
				longname: 'Soho Shortcodes',
                author: 'Clapat Studio',
                authorurl: 'http://themeforest.net/user/clapat/',
                infourl: 'http://clapat.ro/themes/soho-wordpress/',
                version: "1.0"
            }
        }
    });
    // Register plugin
    tinymce.PluginManager.add( 'SohoShortcodes', tinymce.plugins.SohoShortcodes );
})();