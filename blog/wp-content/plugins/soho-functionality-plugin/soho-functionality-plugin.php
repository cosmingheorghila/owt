<?php
/*
Plugin Name: Soho Functionality Plugin
Plugin URI: http://themeforest.net/user/ClaPat/portfolio
Description: Shortcodes and Custom Post Types for Soho WordPress Themes
Version: 1.1
Author: Clapat
Author URI: http://themeforest.net/user/ClaPat/
*/

if( !defined('SOHO_SHORTCODES_DIR_URL') ) define('SOHO_SHORTCODES_DIR_URL', plugin_dir_url(__FILE__));
if( !defined('SOHO_SHORTCODES_DIR') ) define('SOHO_SHORTCODES_DIR', plugin_dir_path(__FILE__));

// load plugin's text domain
add_action( 'plugins_loaded', 'soho_shortcodes_load_textdomain' );
function soho_shortcodes_load_textdomain() {
    load_plugin_textdomain( 'clapat_soho_plugin_text_domain', false, dirname( plugin_basename( __FILE__ ) ) . '/include/langs' );
}

// custom post types
require_once( SOHO_SHORTCODES_DIR . '/include/custom-post-types-config.php' );

// trent shortcodes
require_once( SOHO_SHORTCODES_DIR . '/include/shortcodes.php' );

?>