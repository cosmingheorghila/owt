$(window).load(function(){
    //generic
    loadContent();
    flashMessage();
    mobileMenu();
    showFooter();

    //setups
    tooltipSetup();
    autocompleteSetup();
    fancyboxSetup();

    //home
    homeMainSlider();
    homeBlogSlider();
    homeAnimations();

    //sell
    imageHeaderSlider();
    sellArticlesHeight();
    sellValidateForm();
    sellShowYear();
    sellSelectYear();
    sellSelectKilometers();
    sellSelectColor();
    sellSelectAddress();
    sellSelectDays();

    //team
    teamHover();

    //list
    switchListing();
    filters();
    hover();

    //singular
    certificationTabs();
    featuresHover();
    genericHeights();
    scrollableBoxes();
    tabbedDefects();

    //checkout
    carDetailsHeight();
    checkoutStep2();
    checkoutStep2ValidateForm();

    //faq
    openQuestions();
    scrollQuestions();
});

/**
 * @return void
 */
function loadContent()
{
    $('#loading').hide();
    $('header, main, footer, div.curtain, div.curtain-home').fadeIn();
}

/**
 * @return void
 */
function flashMessage()
{
    if ($('div.flash').length > 0) {
        setTimeout(function(){
            $('div.flash').slideUp();
        }, 2500);
    }
}

/**
 * @return void
 */
function mobileMenu()
{
    $('nav > span').click(function(e){
        e.preventDefault();

        $(this).toggleClass('active');
        $('nav > ul.menu').toggle();
    });

    $('html').click(function(e) {
        if ((!$('nav > span').is(e.target) && $('nav > span').has(e.target).length === 0) && $(window).width() <= 885) {
            $('nav > span').removeClass('active');
            $('nav > ul.menu').hide();
        }
    });

    $(window).scroll(function(e){
        e.preventDefault();

        if ($(window).scrollTop() > 0) {
            $('header').addClass('scrolled');
        } else {
            $('header').removeClass('scrolled');
        }
    });
}

/**
 * @return void
 */
function showFooter() {
    if ($("body").height() > $(window).height()) {
        $('footer').hide();

        $(window).on('scroll', function (e) {
            e.preventDefault();

            if ($(window).scrollTop() + $(window).height() + 285 >= $(document).height()) {
                $('footer').show();
            } else {
                $('footer').hide();
            }
        });
    }
}

/**
 * @return void
 */
function tooltipSetup()
{
    $('.tooltip').tooltipster({
        theme: 'tooltipster-punk',
        maxWidth: 450
    });
}

/**
 * @return void
 */
function autocompleteSetup()
{
    //https://github.com/devbridge/jQuery-Autocomplete

    var brand   = $('#car-brand');
    var model   = $('#car-model');
    var city    = $('#car-city');

    var brandInput  = $('input[name="data[brand_id]"]');
    var modelInput  = $('input[name="data[model_id]"]');
    var cityInput   = $('input[name="data[city_id]"]');

    //get cities
    if (city.length) {
        city.devbridgeAutocomplete({
            serviceUrl: city.data('url'),
            dataType: 'json',
            onSelect: function (_city) {
                cityInput.val(_city.id);

                $('figure#location-breadcrumb-icon').show();
                $('em#car-city-breadcrumb').text(_city.value);
            }
        });
    }

    //get brands
    if (brand.length) {
        brand.devbridgeAutocomplete({
            serviceUrl: brand.data('url'),
            dataType: 'json',
            onSelect: function (_brand) {
                brandInput.val(_brand.id);

                $('figure#car-breadcrumb-icon > img').attr('src', _brand.logo);
                $('figure#car-breadcrumb-icon').show();
                $('em#car-brand-breadcrumb').text(_brand.value);

                //get models
                if ($('#car-model').length) {
                    $('#car-model').devbridgeAutocomplete({
                        serviceUrl: model.data('url'),
                        dataType: 'json',
                        params: {
                            brand_id: brandInput.val()
                        },
                        onSelect: function (_model) {
                            modelInput.val(_model.id);

                            $('em#car-model-breadcrumb').text(' ' + _model.value);

                            //get colors
                            $.ajax({
                                type: 'GET',
                                url: $('#car-color-placeholder').data('url'),
                                dataType: 'json',
                                data: {
                                    model_id: _model.id
                                },
                                success: function(data){
                                    if (data.status) {
                                        $('#car-color-placeholder').html(data.content);
                                    }
                                }
                            });
                        }
                    });
                }
            }
        });
    }
}

/**
 * @return void
 */
function fancyboxSetup()
{
    $('.fancybox').fancybox();
}

/**
 * @return void
 */
function homeMainSlider()
{
    var slide;

    if ($(window).width() < 635) { slide = 260; }
    if ($(window).width() > 635) { slide = 450; }
    if ($(window).width() > 885) { slide = 550; }
    if ($(window).width() > 1335) { slide = 600; }
    if ($(window).width() > 1685) { slide = 700; }

    if ($('a.sell-slide').length && $('a.buy-slide').length) {
        $('body').on('mousemove', function (e) {
            if (e.pageX > slide) {
                if ($('a.sell-slide').hasClass('slided') && $('section.intro').hasClass('to-right')) {
                    $('a.sell-slide').removeClass('slided');
                    $('section.intro').removeClass('to-right');
                    $('section.intro-sell').animate({
                        left: "-=" + slide + "px"
                    }, 270);
                }
            }

            if (e.pageX < slide + 250) {
                if ($('a.buy-slide').hasClass('slided') && $('section.intro').hasClass('to-left')) {
                    $('a.buy-slide').removeClass('slided');
                    $('section.intro').removeClass('to-left');
                    $('section.intro-buy').animate({
                        right: "-=" + slide + "px"
                    }, 270);
                }
            }

            e.stopImmediatePropagation();
        });
    }

    $('a.sell-slide').click(function(e){
        e.preventDefault();

        $('section.intro').addClass('scrolled');
        $('section.intro').removeClass('to-left');
        $('section.intro').toggleClass('to-right');

        if ($(this).hasClass('slided')) {
            $(this).removeClass('slided');
            $('section.intro-sell').animate({
                left: "-=" + slide + "px"
            }, 270);
        } else {
            $(this).addClass('slided');
            $('section.intro-sell').animate({
                left: "+=" + slide + "px"
            }, 230);
        }
    });

    $('a.buy-slide').click(function(e){
        e.preventDefault();

        $('section.intro').addClass('scrolled');
        $('section.intro').removeClass('to-right');
        $('section.intro').toggleClass('to-left');

        if ($(this).hasClass('slided')) {
            $(this).removeClass('slided');
            $('section.intro-buy').animate({
                right: "-=" + slide + "px"
            }, 270);
        } else {
            $(this).addClass('slided');
            $('section.intro-buy').animate({
                right: "+=" + slide + "px"
            }, 230);
        }
    });

    $(window).scroll(function(e){
        e.preventDefault();

        if ($('section.intro').hasClass('scrolled')) {
            if ($(window).scrollTop() > 0) {
                $('a.sell-slide, a.buy-slide').removeClass('slided');
                $('section.intro').removeClass('to-left').removeClass('to-right');
                $('section.intro-sell').attr('style', '');
                $('section.intro-buy').attr('style', '');
            }

            $('section.intro').removeClass('scrolled');
        }
    });
}

/**
 * @return void
 */
function homeBlogSlider()
{
    if ($('#slider').length) {
        $(document).ready(function ($) {
            var options = {
                $AutoPlay: true,
                $SlideDuration: 800,
                $Idle: 3000,
                $FillMode: 1,
                $SlideEasing: $Jease$.$OutQuint,
                $CaptionSliderOptions: {
                    $Class: $JssorCaptionSlideo$
                },
                $ArrowNavigatorOptions: {
                    $Class: $JssorArrowNavigator$
                },
                $BulletNavigatorOptions: {
                    $Class: $JssorBulletNavigator$
                }
            };

            var slider = new $JssorSlider$("slider", options);

            function ScaleSlider() {
                var bodyWidth = document.body.clientWidth;

                if (bodyWidth) {
                    slider.$ScaleWidth(Math.min(bodyWidth, 1920));
                } else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }

            ScaleSlider();

            $(window).bind("load", ScaleSlider);
            $(window).bind("resize", ScaleSlider);
            $(window).bind("orientationchange", ScaleSlider);
        });
    }
}

/**
 * @return void
 */
function homeAnimations()
{
    $('.home-points > article').hover(function(){
        $(this).find('img.secondary').fadeIn(400);
        $(this).find('p').fadeIn(600);
    }, function(){
        $(this).find('img.secondary').fadeOut(400);
        $(this).find('p').fadeOut(600);
    });
}

/**
 * @return void
 */
function imageHeaderSlider()
{
    $('section.header-image-full').css('height', $(window).height() - 84);

    setInterval(function(){
        var current = $('div.slide-header-image').not('.hidden').length ? $('div.slide-header-image').not('.hidden') : $('div.slide-header-image:first-of-type');
        var other = current.next('div.slide-header-image').length ? current.next('div.slide-header-image') : $('div.slide-header-image:first-of-type');

        current.fadeOut(700).addClass('hidden');
        other.fadeIn(700).removeClass('hidden');

        $('section.slide-header-image-full ul.controls > li > a').removeClass('current');
        $('ul.slide-controls').find("[data-item='" + other.data('item') + "'] > a").addClass('current');
    }, 7000);

    $('section.header-image-full ul.controls > li > a').click(function(e){
        e.preventDefault();

        $('section.header-image-full div.header-image').fadeOut(500);
        $('section.header-image-full ' + $(this).attr('href')).fadeIn(500);

        $('section.header-image-full ul.controls > li > a').removeClass('current');
        $(this).addClass('current');
    });

    $('a.scroll-to').click(function(){
        $('html,body').animate({
            scrollTop: $($(this).attr('href')).offset().top - 40
        }, 600);
    });
}

/**
 * @return void
 */
function sellArticlesHeight()
{
    var maxHeight = Math.max.apply(null, $("section.sell-square article:not(.wide) > div").map(function () {
        return $(this).height();
    }).get());

    if ($(window).width() > 900) {
        $('section.sell-square article:not(.wide) div').css('height', maxHeight + 35);
    }
}

/**
 * @return void
 */
function sellValidateForm()
{
    var form = $("#sell-form");

    var carBrand        = $('#car-brand');
    var carModel        = $('#car-model');
    var carKilometers   = $('#car-kilometers');
    var carCity         = $('#car-city');
    var carStreet       = $('#car-street');
    var carYear         = $('#car-year');
    var carColor        = $('#car-color');
    var userName        = $('#user-name');
    var userSurname     = $('#user-surname');
    var userEmail       = $('#user-email');
    var userPhone       = $('#user-phone');

    form.submit(function(){
        if (
            validateSellField(carBrand) &&
            validateSellField(carModel) &&
            validateSellField(carKilometers) &&
            validateSellField(carCity) &&
            validateSellField(carStreet) &&
            validateSellField(carYear) &&
            validateSellField(carColor) &&
            validateSellField(userName) &&
            validateSellField(userSurname) &&
            validateSellField(userEmail) &&
            validateSellField(userPhone) &&
            validateSellCheckbox() &&
            validateSellHours()
        ) {
            if (validateSellEmail(userEmail.val())) {
                return true;
            } else {
                $('<div class="flash-hidden"><span class="error">Email-ul introdus nu este valid!</span></div>').prependTo('body');
                $('div.flash-hidden').slideDown();

                setTimeout(function(){
                    $('div.flash-hidden').slideUp(300);

                    setTimeout(function(){
                        $('div.flash-hidden').remove();
                    }, 300);
                }, 2500);

                return false;
            }
        } else {
            $('<div class="flash-hidden"><span class="error">Se pare ca ai omis ceva. Completeaza, te rog, toate campurile.</span></div>').prependTo('body');
            $('div.flash-hidden').slideDown();

            setTimeout(function(){
                $('div.flash-hidden').slideUp(300);

                setTimeout(function(){
                    $('div.flash-hidden').remove();
                }, 300);
            }, 2500);

            return false;
        }
    });

    /**
     * @param field
     * @returns {boolean}
     */
    function validateSellField(field) {
        if (field.length) {
            return field.val() ? true : false;
        }

        return true;
    }

    /**
     * @returns {boolean}
     */
    function validateSellCheckbox() {
        if ($('input.car-spec').length) {
            return $('input.car-spec:checked').length > 0 ? true : false;
        }

        return true;
    }

    /**
     * @returns {boolean}
     */
    function validateSellHours() {
        if ($('a.hours').length) {
            return $('a.hours.current').length > 0 ? true : false;
        }

        return true;
    }

    function validateSellEmail(field) {
        console.log(field);

        if (field.length) {
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;

            return regex.test(field);
        }
    }
}

/**
 * @return void
 */
function sellShowYear()
{
    $('a.car-year-more').click(function(e){
        e.preventDefault();

        $('#car-year').slideToggle(300);
    });
}

/**
 * @return void
 */
function sellSelectYear()
{
    $('a.car-year').click(function(e){
        e.preventDefault();

        $('a.car-year').removeClass('current');
        $(this).addClass('current');

        $('#car-year').attr('value', $(this).attr('data-year'));
        $('em#car-year-breadcrumb').text(' / ' + $(this).attr('data-year'));
    });

    $('#car-year').keyup(function(){
        $('em#car-year-breadcrumb').text(' / ' + $(this).val());
    });
}

/**
 * @return void
 */
function sellSelectKilometers()
{
    $('#car-kilometers').keyup(function(e){
        $('em#car-kilometers-breadcrumb').text(' / ' + $.number($(this).val(), 0) + ' km');
    });
}

/**
 * @return void
 */
function sellSelectColor()
{
    $('body').on('click', 'a.car-color', function (e){
        e.preventDefault();

        $('a.car-color').removeClass('current');
        $(this).addClass('current');

        $('#car-color').val($(this).attr('data-color'));
        $('em#car-color-breadcrumb').text(' / ' + $(this).text());
    });
}

/**
 * @return void
 */
function sellSelectAddress()
{
    $('#car-street').keyup(function(e){
        $('em#car-street-breadcrumb').text(' / ' + $(this).val());
    });
}

/**
 * @return void
 */
function sellSelectDays()
{
    $('a.hours').click(function(e){
        e.preventDefault();

        if ($('a.hours.current').length >= 3 && !$(this).hasClass('current')) {
            $('<div class="flash-hidden"><span class="error">Poti alege pana la maxim 3 intervale!</span></div>').prependTo('body');
            $('div.flash-hidden').slideDown();

            setTimeout(function(){
                $('div.flash-hidden').slideUp();
            }, 2500);
        } else {
            var month = $(this).attr('data-month');
            var day = $(this).attr('data-day');
            var hours = $(this).attr('data-hours');
            var index = $(this).attr('data-index');

            if ($(this).hasClass('current')) {
                $('#' + day + '-' + hours).remove();
            } else {
                $('<input type="hidden" name="data[' + index + '][month]" value="' + month + '" class="time-selected" />').prependTo('#sell-form');
                $('<input type="hidden" name="data[' + index + '][day]" value="' + day + '" class="time-selected" />').prependTo('#sell-form');
                $('<input type="hidden" name="data[' + index + '][hour]" value="' + hours + '" class="time-selected" />').prependTo('#sell-form');
            }

            $(this).toggleClass('current');
        }
    });
}

/**
 * @return void
 */
function teamHover()
{
    $('.member-avatar').mouseover(function(){
        $(this).find('.mask').fadeIn(300);
    });

    $('.member-avatar').mouseleave(function(){
        $(this).find('.mask').fadeOut(300);
    });
}

/**
 * @return void
 */
function switchListing()
{
    $('.format > a.list').click(function(e){
        e.preventDefault();

        $('.format > a.grid').removeClass('active');
        $('.format > a.list').addClass('active');

        $('.car-grid-container').hide();
        $('.car-list-container').show();
    });

    $('.format > a.grid').click(function(e){
        e.preventDefault();

        $('.format > a.grid').addClass('active');
        $('.format > a.list').removeClass('active');

        $('.car-list-container').hide();
        $('.car-grid-container').show();
    });
}

/**
 * @return void
 */
function certificationTabs()
{
    $('.car-certifications').css('height', $('.car-certifications').outerHeight() + $('.certification-details').outerHeight() + 30);


    $('.car-certifications .certification').click(function(e){
        e.preventDefault();

        $('.certification').removeClass('active');
        $(this).addClass('active');

        $('.certification-details').fadeOut(300);
        $($(this).attr('href')).fadeIn(300);
    });
}

/**
 * @return void
 */
function featuresHover()
{
    $('.car-features > div').mouseover(function(){
        $(this).css('background-image', 'url("' + $(this).attr('data-image-hover') + '")');
    }).mouseout(function(){
        $(this).css('background-image', 'url("' + $(this).attr('data-image') + '")');
    });
}

/**
 * @return void
 */
function genericHeights()
{
    if ($(window).width() > 545) {
        var maxHeight = 0;

        $('div.generic > div').each(function () {
            var height = $(this).height();

            if (height > maxHeight) {
                maxHeight = height;
            }
        });

        $('div.generic > div').css('height', maxHeight);
    }
}

/**
 * @return void
 */
function scrollableBoxes()
{
    $('#verifications > div').jScrollPane({
        verticalGutter: 20
    });

    $('#options > div').jScrollPane({
        verticalGutter: 20
    });
}

function tabbedDefects()
{
    $('#defects .tab > a').click(function(e){
        e.preventDefault();

        $('#defects .tab > a').removeClass('active');
        $(this).addClass('active');

        $('#defects > div > div').removeClass('active');
        $($(this).attr('href')).addClass('active');
    });
}

/**
 * @return void
 */
function carDetailsHeight()
{
    var height = $('.checkout > .car-details > .car-image img').height();

    $('.checkout > .car-details').css('height', height + 'px');
    $('.checkout > .car-details > .car-overview').css('height', height + 'px');
}

/**
 * @return void
 */
function checkoutStep2()
{
    var county  = $('#checkout-county');
    var city    = $('#checkout-city');

    var countyInput = $('input[name="data[county_id]"]');
    var cityInput   = $('input[name="data[city_id]"]');

    //get counties
    if (county.length) {
        county.devbridgeAutocomplete({
            serviceUrl: county.data('url'),
            dataType: 'json',
            onSelect: function (_county) {
                if (_county.id != countyInput.val()) {
                    cityInput.val('');
                    $('#checkout-city').val('');
                    $('#checkout-street').val('');
                    $('#checkout-number').val('');
                }

                countyInput.val(_county.id);

                $('#car-real-price').val($('#car-dummy-price').val());
                $('#car-real-price').val(parseInt($('#car-real-price').val()) + parseInt(_county.cost));
                $('#delivery-cost').html('&euro;' + _county.cost);
                $('#delivery-summary > .summary-left').html('<span>Livrare</span><p>' + _county.value + '</p>');
                $('#delivery-summary > .summary-right').html('<em>&euro;' + _county.cost + '</em>');
                $('#total-summary > em').html('&euro;' + $.number(parseInt($('#car-real-price').val()), 0, ',', '.'));

                //get cities
                if (city.length) {
                    city.devbridgeAutocomplete({
                        serviceUrl: city.data('url'),
                        dataType: 'json',
                        params: {
                            county_id: countyInput.val()
                        },
                        onSelect: function (_city) {
                            cityInput.val(_city.id);
                        }
                    });
                }
            }
        });
    }

    $('#checkout-first-name').keyup(function () {
        $('#first-name-details-summary').html($(this).val());
    });

    $('#checkout-last-name').keyup(function () {
        $('#last-name-details-summary').html($(this).val());
    });

    $('#checkout-email').keyup(function () {
        $('#email-details-summary').html($(this).val());
    });

    $('#checkout-phone').keyup(function () {
        $('#phone-details-summary').html($(this).val());
    });
}

/**
 * @return void
 */
function checkoutStep2ValidateForm()
{
    var form = $("#checkout-step2");

    var firstName = $('#checkout-first-name');
    var lastName = $('#checkout-last-name');
    var email = $('#checkout-email');
    var phone = $('#checkout-phone');
    var county = $('#checkout-county');
    var city = $('#checkout-city');
    var street = $('#checkout-street');
    var number = $('#checkout-number');

    form.submit(function(){
        if (
            validateCheckoutField(firstName) &&
            validateCheckoutField(lastName) &&
            validateCheckoutField(email) &&
            validateCheckoutField(phone) &&
            validateCheckoutField(county) &&
            validateCheckoutField(city) &&
            validateCheckoutField(street) &&
            validateCheckoutField(number)
        ) {
            return true;
        } else {
            $('<div class="flash-hidden"><span class="error">Se pare ca ai omis ceva. Completeaza, te rog, toate campurile.</span></div>').prependTo('body');
            $('div.flash-hidden').slideDown();

            setTimeout(function(){
                $('div.flash-hidden').slideUp();
            }, 2500);

            return false;
        }
    });

    /**
     * @param field
     * @returns {boolean}
     */
    function validateCheckoutField(field) {
        if (field.length) {
            return field.val() ? true : false;
        }

        return true;
    }
}

function filters()
{
    if ($('#search-filters').length) {
        $(window).on("scroll", function(e) {
            if ($(window).scrollTop() > 1000 && window.innerHeight > 875) {
                $("header").css("position", "static");
                $("#search-filters").addClass("sticky");
            } else {
                $("header").css("position", "fixed");
                $("#search-filters").removeClass("sticky");
            }

        });
    }

    $('#toggle-filters').click(function(e){
        e.preventDefault();

        $('#filters-container').slideToggle(600);
    });

    var rangePrice = $('#price-range');
    var rangeYear = $('#year-range');
    var rangeKm = $('#km-range');
    var rangeMotorisation = $('#motorisation-range');
    var rangeHp = $('#hp-range');

    rangePrice.slider({
        range: true,
        min: rangePrice.data('min'),
        max: rangePrice.data('max'),
        step: rangePrice.data('step'),
        values: [rangePrice.data('value-min'), rangePrice.data('value-max')],
        slide: function (event, ui) {
            $('#price-range-min').html('&euro;' + $.number(ui.values[0], 0, ',', '.'));
            $('#price-range-max').html('&euro;' + $.number(ui.values[1], 0, ',', '.'));
            $('#price-range-input-min').val(ui.values[0]);
            $('#price-range-input-max').val(ui.values[1]);
        }
    });

    rangeYear.slider({
        range: true,
        min: rangeYear.data('min'),
        max: rangeYear.data('max'),
        step: rangeYear.data('step'),
        values: [rangeYear.data('value-min'), rangeYear.data('value-max')],
        slide: function (event, ui) {
            $('#year-range-min').html(ui.values[0]);
            $('#year-range-max').html(ui.values[1]);
            $('#year-range-input-min').val(ui.values[0]);
            $('#year-range-input-max').val(ui.values[1]);
        }
    });

    rangeKm.slider({
        min: rangeKm.data('min'),
        max: rangeKm.data('max'),
        step: rangeKm.data('step'),
        value: rangeKm.data('value'),
        slide: function (event, ui) {
            $('#km-range-max').html($.number(ui.value, 0, ',', '.'));
            $('#km-range-input').val(ui.value);
        }
    });

    rangeMotorisation.slider({
        range: true,
        min: rangeMotorisation.data('min'),
        max: rangeMotorisation.data('max'),
        step: rangeMotorisation.data('step'),
        values: [rangeMotorisation.data('value-min'), rangeMotorisation.data('value-max')],
        slide: function (event, ui) {
            $('#motorisation-range-min').html(ui.values[0]);
            $('#motorisation-range-max').html(ui.values[1]);
            $('#motorisation-range-input-min').val(ui.values[0]);
            $('#motorisation-range-input-max').val(ui.values[1]);
        }
    });

    rangeHp.slider({
        range: true,
        min: rangeHp.data('min'),
        max: rangeHp.data('max'),
        step: rangeHp.data('step'),
        values: [rangeHp.data('value-min'), rangeHp.data('value-max')],
        slide: function (event, ui) {
            $('#hp-range-min').html(ui.values[0]);
            $('#hp-range-max').html(ui.values[1]);
            $('#hp-range-input-min').val(ui.values[0]);
            $('#hp-range-input-max').val(ui.values[1]);
        }
    });

    $('.filter-color').click(function(e){
        e.preventDefault();

        if ($('input[name="culoare[]"][value="' + $(this).data('color') + '"]').length) {
            $(this).removeClass('active');
            $('input[name="culoare[]"][value="' + $(this).data('color') + '"]').remove();
        } else {
            $(this).addClass('active');
            $("<input type='hidden' name='culoare[]' value='" + $(this).data('color') + "' /> ").prependTo('#filter-form');
        }
    });

    $('.filter-body').click(function(e){
        e.preventDefault();

        if ($('input[name="caroserie[]"][value="' + $(this).data('body') + '"]').length) {
            $(this).removeClass('active');
            $('input[name="caroserie[]"][value="' + $(this).data('body') + '"]').remove();
        } else {
            $(this).addClass('active');
            $("<input type='hidden' name='caroserie[]' value='" + $(this).data('body') + "' /> ").prependTo('#filter-form');
        }
    });

    $('.filter-fuel').click(function(e){
        e.preventDefault();

        if ($('input[name="combustibil[]"][value="' + $(this).data('fuel') + '"]').length) {
            $(this).removeClass('active');
            $('input[name="combustibil[]"][value="' + $(this).data('fuel') + '"]').remove();
        } else {
            $(this).addClass('active');
            $("<input type='hidden' name='combustibil[]' value='" + $(this).data('fuel') + "' /> ").prependTo('#filter-form');
        }
    });

    $('.filter-traction').click(function(e){
        e.preventDefault();

        if ($('input[name="tractiune[]"][value="' + $(this).data('traction') + '"]').length) {
            $(this).removeClass('active');
            $('input[name="tractiune[]"][value="' + $(this).data('traction') + '"]').remove();
        } else {
            $(this).addClass('active');
            $("<input type='hidden' name='tractiune[]' value='" + $(this).data('traction') + "' /> ").prependTo('#filter-form');
        }
    });

    $('.filter-transmission').click(function(e){
        e.preventDefault();

        if ($('input[name="transmisie[]"][value="' + $(this).data('transmission') + '"]').length) {
            $(this).css('background-image', 'url(' + $(this).data('image') + ')');
            $('input[name="transmisie[]"][value="' + $(this).data('transmission') + '"]').remove();
        } else {
            $(this).css('background-image', 'url(' + $(this).data('image-hover') + ')');
            $("<input type='hidden' name='transmisie[]' value='" + $(this).data('transmission') + "' /> ").prependTo('#filter-form');
        }
    });

    $('.filter-feature').click(function(e){
        e.preventDefault();

        if ($('input[name="dotari[]"][value="' + $(this).data('feature') + '"]').length) {
            $(this).css('background-image', 'url(' + $(this).data('image') + ')');
            $('input[name="dotari[]"][value="' + $(this).data('feature') + '"]').remove();
        } else {
            $(this).css('background-image', 'url(' + $(this).data('image-hover') + ')');
            $("<input type='hidden' name='dotari[]' value='" + $(this).data('feature') + "' /> ").prependTo('#filter-form');
        }
    });
}

/**
 * @return void
 */
function hover()
{
    $('.car-grid').mouseover(function(){
        $(this).find('.car-grid-details').fadeOut(500);
        $(this).find('.car-grid-hover').fadeIn(500);
    }).mouseleave(function(){
        $(this).find('.car-grid-details').fadeIn(500);
        $(this).find('.car-grid-hover').fadeOut(500);
    });
}

/**
 * @return void
 */
function openQuestions()
{
    $('.faq ul li a').click(function (e) {
        e.preventDefault();

        $('.faq-content').not($(this).next('.faq-content')).slideUp();
        $(this).next('.faq-content').slideToggle();
    });
}

/**
 * @return void
 */
function scrollQuestions()
{
    $('.faq-menu ul li a').bind('click', function(e) {
        $('html, body').animate({
            scrollTop: $($(this).attr('href')).offset().top - 40
        }, 600);

        e.preventDefault();
    });
}