$(function(){
    flashMessage();

    menu();
    login();

    tabs();
    tabsLength();

    scrollSetup();
    datepickerSetup();
    timepickerSetup();
    editorSetup();
    chosenSetup();

    menuTypes();
    blockTypes();
    attributeTypes();

    accessLevels();
    mirrorPageUrl();
    inputsWidth();

    deleteImage();
    deleteVideo();
    deleteAudio();
    deleteFile();

    productsAddAttribute();
    productsEditAttribute();
    productsDeleteAttribute();
    productsAddVendor();
    productsDeleteVendor();
    productsAddDiscount();
    productsDeleteDiscount();
    productsAddTax();
    productsDeleteTax();
    orderDeleteProduct();
});

/**
 * @return void
 */
function flashMessage()
{
    var flash = $('.flash');

    if (flash.length > 0) {
        setTimeout(function(){
            flash.slideUp();
        }, 4500);
    }
}

/**
 * @return void
 */
function menu()
{
    var menu        = $('#menu');
    var menuButton  = $('.main-menu li a');
    var menuBtn     = $('.menu-btn');
    var subMenu     = $('.sub-menu');

    menuButton.click(function(){
        menuButton.removeClass('selected');
        subMenu.slideUp();

        if ($(this).next().css('display') == 'none') {
            $(this).next().slideDown();
            $(this).addClass('selected');
        } else {
            $(this).next().slideUp();
            $(this).removeClass('selected');
        }
    });

    menuBtn.click(function(){
        if (menu.css('margin-left') == '-290px') {
            menu.animate({"margin-left": '0px'});
        } else {
            menu.animate({"margin-left": '-290px'});
        }
    });
}

/**
 * @return void
 */
function login()
{
    var form        = $("#loginForm");
    var username    = $("#username");
    var password    = $("#password");

    username.blur(validateUsername);
    password.blur(validatePassword);

    username.keyup(validateUsername);
    password.keyup(validatePassword);

    form.submit(function(){
        return !!(validateUsername() && validatePassword());
    });

    function validateUsername(){
        if(!username.val()){
            username.prev().addClass("input-error");
            username.prev().removeClass("input-success");
            return false;
        }else{
            username.prev().removeClass("input-error");
            username.prev().removeClass("input-success");
            return true;
        }
    }

    function validatePassword(){
        if(!password.val()){
            password.prev().addClass("input-error");
            password.prev().removeClass("input-success");
            return false;
        }else{
            password.prev().removeClass("input-error");
            password.prev().removeClass("input-success");
            return true;
        }
    }
}

/**
 * @return void
 */
function tabs()
{
    $('.tabs a:not(.real):first-child').addClass('selected');
    $('.form .hide:first-child').show();

    $(".tab").click(function(e){
        var target = $(this).attr("href");

        $('.tab').removeClass('selected');
        $($(this)).addClass('selected');
        $('.hide').hide();
        $(target).show();

        e.preventDefault();
    });
}

/**
 * @return void
 */
function tabsLength()
{
    var tab = $('.tab');

    if (isMobile()) {
        if (tab.length == 1) {
            tab.hide();
        }
    }
}

/**
 * @return void
 */
function scrollSetup()
{
    var scroll = $('.scroll');

    scroll.nanoScroller({
        scroll: 'top',
        tabIndex: 0
    });
}

/**
 * @return void
 */
function datepickerSetup()
{
    var date = $('.datepicker');

    date.datepicker({
        dateFormat: 'dd M, yy'
    });
}

/**
 * @return void
 */
function timepickerSetup()
{
    var time = $('.timepicker');

    time.timepicker({
        showLeadingZero: true,
        showPeriodLabels: false,
        defaultTime: '00:00'
    });
}

/**
 * @return void
 */
function editorSetup()
{
    tinymce.init({
        selector: "textarea.editor",
        plugins: [
            "advlist autolink lists link image charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste"
        ],
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
        height: 300
    });
}

/**
 * @return void
 */
function chosenSetup()
{
    $(".chosen-select").chosen();

    $('.partial-select').chosen({
        width: '17%'
    });

    $('.half-select').chosen({
        width: '50%'
    });

    $('.full-select').chosen({
        width: '100%'
    });
}

/**
 * @return void
 */
function menuTypes()
{
    if ($('#menu-type').length) {
        if ($('#menu-type option:selected').val() == 'url') {
            $('#url-selector').removeClass('hide');
            $('#open-selector').removeClass('hide');
        }

        if ($('#menu-type option:selected').val() == 'page') {
            $('#page-selector').removeClass('hide');
            $('#open-selector').removeClass('hide');
        }

        $('#menu-type').change(function(){
            if ($(this).val() == 'url') {
                $('#page-selector').addClass('hide');
                $('#url-selector').removeClass('hide');
                $('#open-selector').removeClass('hide');
            }

            if ($(this).val() == 'page') {
                $('#url-selector').addClass('hide');
                $('#page-selector').removeClass('hide');
                $('#open-selector').removeClass('hide');
            }

            if ($(this).val() == '') {
                $('#url-selector').addClass('hide');
                $('#page-selector').addClass('hide');
                $('#open-selector').addClass('hide');
            }
        });
    }
}

/**
 * @return void
 */
function blockTypes()
{
    var type            = $('#type');
    var preview         = $('#preview');
    var previewImage    = $('#preview-img');
    var previewLabel    = $('#preview .label-container');
    var imagePath       = '../../../public/assets/admin/img/blocks/';

    type.change(function(){
        previewImage.remove();

        if (type.val()) {
            preview.show();

            if ($('#preview-img').length > 0) {
                $('#preview-img').attr('src', imagePath + type.val() + '.jpg');
            } else {
                previewLabel.append('<img id="preview-img" src="' + imagePath + type.val() + '.jpg" style="max-width: 100%;" />');
                preview.show();
            }
        } else {
            preview.hide();
        }
    });
}

/**
 * @return void
 */
function attributeTypes()
{
    var typeSelector    = $('#shop_attribute_type');
    var type            = typeSelector.find(":selected").text().toLowerCase();
    var typeValue       = $('#shop_attribute_value_' + type);
    var typeLabel       = $('label[for=shop_attribute_value_' + type + ']');
    var allValues       = $('.all-values').children().attr('name', 'val');

    allValues.hide();
    typeLabel.show();

    if (type == 'editor') {
        typeValue.attr('name', 'data[value]').show();
    } else {
        if (type == 'file' || type == 'image' || type == 'video' || type == 'audio') {
            typeValue.attr('name', 'value').show();
        } else {
            typeValue.attr('name', 'data[value]').show();
        }
    }

    typeSelector.change(function(){
        allValues.hide();
        $('label[for=shop_attribute_value_' + $(this).val() + ']').show();

        if ($(this).val() == 'editor') {
            $('.mce-tinymce').show();
            $('#shop_attribute_value_' + $(this).val()).attr('name', 'data[value]');
        } else {
            if ($(this).val() == 'file' || $(this).val() == 'image' || $(this).val() == 'video' || $(this).val() == 'audio') {
                $('#shop_attribute_value_' + $(this).val()).attr('name', 'value').show();
            } else {
                $('#shop_attribute_value_' + $(this).val()).attr('name', 'data[value]').show();
            }
        }
    });
}

/**
 * @return void
 */
function accessLevels()
{
    var superAdmin  = $('#super_admin');
    var levels      = $('#access-levels-container');

    superAdmin.change(function(){
        if ($(this).val() == '0') {
            levels.show();
        } else {
            levels.hide();
        }
    });
}

/**
 * @return void
 */
function mirrorPageUrl()
{
    var pageName    = $('#page-name');
    var pageUrl     = $('#page-url');

    pageName.bind('keyup blur', function() {
        pageUrl.val('/' + $(this).val().replace(/\s+/g, '-').replace('&', 'and').replace('_', '-').toLowerCase());
    });
}

/**
 * @return void
 */
function inputsWidth()
{
    var width       = $('#form .row input:first').width();
    var input       = $('#form .row input');
    var textarea    = $('#form .row textarea');
    var select      = $('#form .row select');
    var chosen      = $('#form .row .chosen-container');

    input.css('width', width);
    textarea.css('width', width);
    select.css('width', width);
    chosen.css('width', width + 20);
}

/**
 * @return void
 */
function deleteImage()
{
    var deleteImageButton       = $('.image-delete');
    var deleteMetaImageButton   = $('.meta-image-delete');

    deleteImageButton.click(function(e){
        e.preventDefault();
        $(this).closest('.row-container').slideUp();

        $.ajax({
            type: 'POST',
            url: $(this).attr('href'),
            dataType: 'json',
            data: {
                delete_image : true
            },
            success: function(data){
                if (data.success == true) {
                    $(this).closest('.row-container').slideUp();
                }
            }
        });
    });

    deleteMetaImageButton.click(function(e){
        e.preventDefault();
        $(this).closest('.row-container').slideUp();

        $.ajax({
            type: 'POST',
            url: $(this).attr('href'),
            dataType: 'json',
            data: {
                delete_meta_image : true
            },
            success: function(data){
                if (data.success == true) {
                    $(this).closest('.row-container').slideUp();
                }
            }
        });
    });
}

/**
 * @return void
 */
function deleteVideo()
{
    var deleteVideoButton       = $('.video-delete');
    var deleteMetaVideoButton   = $('.meta-video-delete');

    deleteVideoButton.click(function(e){
        e.preventDefault();
        $(this).closest('.row-container').slideUp();

        $.ajax({
            type: 'POST',
            url: $(this).attr('href'),
            dataType: 'json',
            data: {
                delete_video : true
            },
            success: function(data){
                if (data.success == true) {
                    $(this).closest('.row-container').slideUp();
                }
            }
        });
    });

    deleteMetaVideoButton.click(function(e){
        e.preventDefault();
        $(this).closest('.row-container').slideUp();

        $.ajax({
            type: 'POST',
            url: $(this).attr('href'),
            dataType: 'json',
            data: {
                delete_meta_video : true
            },
            success: function(data){
                if (data.success == true) {
                    $(this).closest('.row-container').slideUp();
                }
            }
        });
    });
}

/**
 * @return void
 */
function deleteAudio()
{
    var deleteAudioButton       = $('.audio-delete');
    var deleteMetaAudioButton   = $('.meta-audio-delete');

    deleteAudioButton.click(function(e){
        e.preventDefault();
        $(this).closest('.row-container').slideUp();

        $.ajax({
            type: 'POST',
            url: $(this).attr('href'),
            dataType: 'json',
            data: {
                delete_audio : true
            },
            success: function(data){
                if (data.success == true) {
                    $(this).closest('.row-container').slideUp();
                }
            }
        });
    });

    deleteMetaAudioButton.click(function(e){
        e.preventDefault();
        $(this).closest('.row-container').slideUp();

        $.ajax({
            type: 'POST',
            url: $(this).attr('href'),
            dataType: 'json',
            data: {
                delete_meta_audio : true
            },
            success: function(data){
                if (data.success == true) {
                    $(this).closest('.row-container').slideUp();
                }
            }
        });
    });
}

/**
 * @return void
 */
function deleteFile()
{
    var deleteFileButton        = $('.file-delete');
    var deleteMetaFileButton    = $('.meta-file-delete');

    deleteFileButton.click(function(e){
        e.preventDefault();
        $(this).closest('.row-container').slideUp();

        $.ajax({
            type: 'POST',
            url: $(this).attr('href'),
            dataType: 'json',
            data: {
                delete_file : true
            },
            success: function(data){
                if (data.success == true) {
                    $(this).closest('.row-container').slideUp();
                }
            }
        });
    });

    deleteMetaFileButton.click(function(e){
        e.preventDefault();
        $(this).closest('.row-container').slideUp();

        $.ajax({
            type: 'POST',
            url: $(this).attr('href'),
            dataType: 'json',
            data: {
                delete_meta_file : true
            },
            success: function(data){
                if (data.success == true) {
                    $(this).closest('.row-container').slideUp();
                }
            }
        });
    });
}

/**
 * @return void
 */
function productsAddAttribute()
{
    var addAttributeButton = $('.add-attribute');
    var attributeError     = $('#attribute-error');

    var productId       = $('#product_id');
    var attributeId     = $('#product_attribute_id');
    var attributeValue  = $('#product_attribute_value');

    addAttributeButton.click(function(e){
        e.preventDefault();

        var attributeText = attributeId.find(":selected").text().split(' --- ');

        var productIdVal            = productId.val();
        var attributeIdVal          = attributeId.find(":selected").val();
        var attributeNameVal        = attributeText[0];
        var attributeValueVal       = attributeText[1];
        var attributeCustomValueVal = attributeValue.val();

        var url     = window.location.href;
        var viewUrl = window.location.pathname.replace(
            '/products/edit', '/attributes/edit?id=' + attributeIdVal
        );

        $.ajax({
            type: 'POST',
            url: url,
            dataType: 'json',
            data: {
                add_attribute   : true,
                product_id      : productIdVal,
                attribute_id    : attributeIdVal,
                attribute_value : attributeCustomValueVal
            },
            success: function(data) {
                if (data.status) {
                    attributeId.val($('#product_attribute_id option:first').val());
                    attributeValue.val('');

                    $('form .hide:visible .table-attributes tbody tr.no-records').hide();
                    $('form .hide:visible .table-attributes tbody').append(
                        '<tr class="light">' +
                        '<td>' + attributeNameVal + '</td>' +
                        '<td>' + attributeValueVal + '</td>' +
                        '<td><textarea name="attr[value]" style="width: 94%; height: 70px;" class="edit-value" data-id="' + data.id + '">' + attributeCustomValueVal + '</textarea></td>' +
                        '<td>' +
                        '<a target="_blank" href="' + viewUrl + '" class="btn view orange margin-right">View</a>' +
                        '<a data-id="' + data.id + '" class="delete-attribute btn delete red">Remove</a>' +
                        '</td>' +
                        '</tr>'
                    );

                    attributeError.slideUp();
                } else {
                    attributeError.html('<p>' + data.message + '</p>').slideDown();
                }
            }
        });
    });
}

/**
 * @return void
 */
function productsEditAttribute()
{
    var timer;

    $('body').on('keyup', '.edit-value', function(){
        var $this = $(this);

        clearInterval(timer);

        timer = setTimeout(function(){
            var url     = window.location.href;
            var id      = $this.attr('data-id');
            var value   = $this.val();

            $.ajax({
                type: 'POST',
                url: url,
                dataType: 'json',
                data: {
                    edit_attribute  : true,
                    id              : id,
                    value           : value
                },
                success: function(data) {

                }
            });
        }, 1000);
    });
}

/**
 * @return void
 */
function productsDeleteAttribute()
{
    $(document).on('click' , '.delete-attribute', function(e){
        e.preventDefault();

        var url         = window.location.href;
        var attributeId = $(this).attr('data-id');
        var productId   = $('input[name="identifier"]').val();
        var tableRow    = $(this).closest('tr');

        $.ajax({
            type: 'POST',
            url: url,
            dataType: 'json',
            data: {
                delete_attribute    : true,
                attribute_id        : attributeId,
                product_id          : productId
            },
            success: function(data) {
                if (data.status) {
                    tableRow.hide();

                    if ($('form .hide:visible .table-attributes tbody tr:visible').length == 0) {
                        $('form .hide:visible .table-attributes tbody tr.no-records').show();
                    }
                }
            }
        });
    });
}

/**
 * @return void
 */
function productsAddVendor()
{
    var addVendorButton = $('.add-vendor');
    var vendorError     = $('#vendor-error');

    var productId       = $('#product_id');
    var vendorId        = $('#product_vendor_id');
    var vendorSku       = $('#product_vendor_sku');
    var vendorPrice     = $('#product_vendor_price');
    var vendorQuantity  = $('#product_vendor_quantity');

    addVendorButton.click(function(e){
        e.preventDefault();

        var productIdVal        = productId.val();
        var vendorIdVal         = vendorId.find(":selected").val();
        var vendorNameVal       = vendorId.find(":selected").text();
        var vendorSkuVal        = vendorSku.val();
        var vendorPriceVal      = vendorPrice.val();
        var vendorQuantityVal   = vendorQuantity.val();

        var url     = window.location.href;
        var viewUrl = window.location.pathname.replace(
            '/products/edit', '/vendors/edit?id=' + vendorIdVal
        );

        $.ajax({
            type: 'POST',
            url: url,
            dataType: 'json',
            data: {
                add_vendor      : true,
                product_id      : productIdVal,
                vendor_id       : vendorIdVal,
                vendor_sku      : vendorSkuVal,
                vendor_price    : vendorPriceVal,
                vendor_quantity : vendorQuantityVal
            },
            success: function(data) {
                if (data.status) {
                    vendorId.val($('#product_vendor_id option:first').val());
                    vendorSku.val('');
                    vendorPrice.val('');
                    vendorQuantity.val('');

                    $('form .hide:visible .table-vendors tbody tr.no-records').hide();
                    $('form .hide:visible .table-vendors tbody').append(
                        '<tr id="' + data.id + '" class="light" style="cursor: move;">' +
                            '<td>' + vendorNameVal + '</td>' +
                            '<td>' + vendorSkuVal + '</td>' +
                            '<td>' + vendorPriceVal + '</td>' +
                            '<td>' + vendorQuantityVal + '</td>' +
                            '<td>' +
                            '<a target="_blank" href="' + viewUrl + '" class="btn view orange margin-right">View</a>' +
                            '<a data-id="' + data.id + '" class="delete-vendor btn delete red">Remove</a>' +
                            '</td>' +
                            '</tr>'
                    );

                    vendorError.slideUp();
                } else {
                    vendorError.html('<p>' + data.message + '</p>').slideDown();
                }
            }
        });
    });
}

/**
 * @return void
 */
function productsDeleteVendor()
{
    $(document).on('click' , '.delete-vendor', function(e){
        e.preventDefault();

        var url         = window.location.href;
        var vendorId    = $(this).attr('data-id');
        var productId   = $('input[name="identifier"]').val();
        var tableRow    = $(this).closest('tr');

        $.ajax({
            type: 'POST',
            url: url,
            dataType: 'json',
            data: {
                delete_vendor   : true,
                vendor_id       : vendorId,
                product_id      : productId
            },
            success: function(data) {
                if (data.status) {
                    tableRow.hide();

                    if ($('form .hide:visible .table-vendors tbody tr:visible').length == 0) {
                        $('form .hide:visible .table-vendors tbody tr.no-records').show();
                    }
                }
            }
        });
    });
}

/**
 * @return void
 */
function productsAddDiscount()
{
    var addDiscountButton   = $('.add-discount');
    var discountError       = $('#discount-error');

    var productId   = $('#product_id');
    var vendorId    = $('#product_discount_vendor_id');
    var discountId  = $('#product_discount_discount_id');

    addDiscountButton.click(function(e){
        e.preventDefault();

        var productIdVal       = productId.val();
        var vendorIdVal        = vendorId.find(":selected").val();
        var vendorNameVal      = vendorId.find(":selected").text();
        var discountIdVal      = discountId.find(":selected").val();
        var discountNameVal    = discountId.find(":selected").text();

        var url     = window.location.href;
        var viewUrl = window.location.pathname.replace(
            '/edit',
            '/discounts/edit?id=' + discountIdVal
        );

        $.ajax({
            type: 'POST',
            url: url,
            dataType: 'json',
            data: {
                add_discount    : true,
                product_id      : productIdVal,
                vendor_id       : vendorIdVal,
                discount_id     : discountIdVal
            },
            success: function(data) {
                if (data.status) {
                    if (vendorId.length > 0) {
                        vendorId.val($('#product_vendor_id option:first').val());
                        discountId.val($('#product_vendor_id option:first').val());

                        $('form .hide:visible .table-discounts tbody tr.no-records').hide();
                        $('form .hide:visible .table-discounts tbody').append(
                            '<tr id="' + data.id + '" class="light" style="cursor: move;">' +
                                '<td>' + vendorNameVal + '</td>' +
                                '<td>' + discountNameVal + '</td>' +
                                '<td></td>' +
                                '<td></td>' +
                                '<td></td>' +
                                '<td>' +
                                '<a target="_blank" href="' + viewUrl + '" class="btn view orange margin-right">View</a>' +
                                '<a data-id="' + data.id + '" class="delete-discount btn delete red">Remove</a>' +
                                '</td>' +
                                '</tr>'
                        );
                    } else {
                        discountId.val($('#product_vendor_id option:first').val());

                        $('form .hide:visible .table-discounts tbody tr.no-records').hide();
                        $('form .hide:visible .table-discounts tbody').append(
                            '<tr id="' + data.id + '" class="light" style="cursor: move;">' +
                                '<td>' + discountNameVal + '</td>' +
                                '<td></td>' +
                                '<td></td>' +
                                '<td></td>' +
                                '<td>' +
                                '<a target="_blank" href="' + viewUrl + '" class="btn view orange margin-right">View</a>' +
                                '<a data-id="' + data.id + '" class="delete-discount btn delete red">Remove</a>' +
                                '</td>' +
                                '</tr>'
                        );
                    }

                    discountError.slideUp();
                } else {
                    discountError.html('<p>' + data.message + '</p>').slideDown();
                }
            }
        });
    });
}

/**
 * @return void
 */
function productsDeleteDiscount()
{
    $(document).on('click' , '.delete-discount', function(e){
        e.preventDefault();

        var url         = window.location.href;
        var discountId  = $(this).attr('data-id');
        var tableRow    = $(this).closest('tr');

        $.ajax({
            type: 'POST',
            url: url,
            dataType: 'json',
            data: {
                delete_discount : true,
                discount_id     : discountId
            },
            success: function(data) {
                if (data.status) {
                    tableRow.hide();

                    if ($('form .hide:visible .table-discounts tbody tr:visible').length == 0) {
                        $('form .hide:visible .table-discounts tbody tr.no-records').show();
                    }
                }
            }
        });
    });
}

/**
 * @return void
 */
function productsAddTax()
{
    var addTaxButton    = $('.add-tax');
    var taxError        = $('#tax-error');

    var productId   = $('#product_id');
    var vendorId    = $('#product_tax_vendor_id');
    var taxId       = $('#product_tax_tax_id');

    addTaxButton.click(function(e){
        e.preventDefault();

        var productIdVal   = productId.val();
        var vendorIdVal    = vendorId.find(":selected").val();
        var vendorNameVal  = vendorId.find(":selected").text();
        var taxIdVal       = taxId.find(":selected").val();
        var taxNameVal     = taxId.find(":selected").text();

        var url     = window.location.href;
        var viewUrl = window.location.pathname.replace(
            '/edit',
            '/taxes/edit?id=' + taxId
        );

        $.ajax({
            type: 'POST',
            url: url,
            dataType: 'json',
            data: {
                add_tax     : true,
                product_id  : productIdVal,
                vendor_id   : vendorIdVal,
                tax_id      : taxIdVal
            },
            success: function(data) {
                if (data.status) {
                    if (vendorId.length > 0) {
                        vendorId.val($('#product_vendor_id option:first').val());
                        taxId.val($('#product_vendor_id option:first').val());

                        $('form .hide:visible .table-taxes tbody tr.no-records').hide();
                        $('form .hide:visible .table-taxes tbody').append(
                            '<tr id="' + data.id + '" class="light" style="cursor: move;">' +
                                '<td>' + vendorNameVal + '</td>' +
                                '<td>' + taxNameVal + '</td>' +
                                '<td></td>' +
                                '<td></td>' +
                                '<td>' +
                                '<a target="_blank" href="' + viewUrl + '" class="btn view orange margin-right">View</a>' +
                                '<a data-id="' + data.id + '" class="delete-tax btn delete red">Remove</a>' +
                                '</td>' +
                                '</tr>'
                        );
                    } else {
                        taxId.val($('#product_vendor_id option:first').val());

                        $('form .hide:visible .table-taxes tbody tr.no-records').hide();
                        $('form .hide:visible .table-taxes tbody').append(
                            '<tr id="' + data.id + '" class="light" style="cursor: move;">' +
                                '<td>' + taxNameVal + '</td>' +
                                '<td></td>' +
                                '<td></td>' +
                                '<td>' +
                                '<a target="_blank" href="' + viewUrl + '" class="btn view orange margin-right">View</a>' +
                                '<a data-id="' + data.id + '" class="delete-tax btn delete red">Remove</a>' +
                                '</td>' +
                                '</tr>'
                        );
                    }

                    taxError.slideUp();
                } else {
                    taxError.html('<p>' + data.message + '</p>').slideDown();
                }
            }
        });
    });
}

/**
 * @return void
 */
function productsDeleteTax()
{
    $(document).on('click' , '.delete-tax', function(e){
        e.preventDefault();

        var url         = window.location.href;
        var taxId       = $(this).attr('data-id');
        var tableRow    = $(this).closest('tr');

        $.ajax({
            type: 'POST',
            url: url,
            dataType: 'json',
            data: {
                delete_tax  : true,
                tax_id      : taxId
            },
            success: function(data) {
                if (data.status) {
                    tableRow.hide();

                    if ($('form .hide:visible .table-taxes tbody tr:visible').length == 0) {
                        $('form .hide:visible .table-taxes tbody tr.no-records').show();
                    }
                }
            }
        });
    });
}

/**
 * @return void
 */
function orderDeleteProduct()
{
    $(document).on('click' , '.delete-order-product', function(e){
        e.preventDefault();

        var url         = window.location.href;
        var productId   = $(this).attr('data-id');
        var tableRow    = $(this).closest('tr');

        $.ajax({
            type: 'POST',
            url: url,
            dataType: 'json',
            data: {
                delete_order_product    : true,
                product_id              : productId
            },
            success: function(data) {
                if (data.status) {
                    tableRow.hide();

                    if ($('form .hide:visible .table-order-products tbody tr:visible').length == 0) {
                        $('form .hide:visible .table-order-products tbody tr.no-records').show();
                    }
                }
            }
        });
    });
}

/**
 * @returns {boolean}
 */
function isMobile() {
    return('ontouchstart' in document.documentElement);
}

/**
 * @param e
 */
function saveSubmit(e) {
    e.preventDefault();
    $('#form').submit();
}

/**
 * @param e
 */
function saveStaySubmit(e) {
    e.preventDefault();
    $('#form').append('<input type="hidden" name="save-stay" value="1" />');
    $('#form').submit();
}

/**
 * @param e
 */
function clearForm(e) {
    e.preventDefault();
    $('#form')[0].reset();
}