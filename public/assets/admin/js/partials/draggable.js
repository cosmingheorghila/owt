$(function(){
    $('.table thead tr:first').addClass('nodrag');
    $('.table thead tr:first').addClass('nodrop');

    $('.table-partial thead tr:first').addClass('nodrag');
    $('.table-partial thead tr:first').addClass('nodrop');

    var url     = window.location.pathname;
    var query   = location.search;

    initDraggable(url + query);
});

/**
 * @param url
 */
function initDraggable(url) {
    $(".table").tableDnD({
        onDrop: function(table, row){
            var rows = table.tBodies[0].rows;
            var ord, item;
            var items = [];

            for (var i=0; i<rows.length; i++) {
                ord = i+1;
                item = rows[i].id;
                items[ord] = item;
            }

            $.ajax({
                type: 'POST',
                url: url,
                data: {items: items}
            });
        }
    });

    $(".table-block").tableDnD({
        onDrop: function(table, row){
            console.log(url.split('/'));
            var rows = table.tBodies[0].rows;
            var ord, item;
            var items = [];

            for (var i=0; i<rows.length; i++) {
                ord = i+1;
                item = rows[i].id;
                items[ord] = item;
            }

            $.ajax({
                type: 'POST',
                url: url,
                data: {
                    order_blocks: true,
                    items: items
                }
            });
        }
    });

    $(".table-vendors").tableDnD({
        onDrop: function(table, row){
            var rows = table.tBodies[0].rows;
            var ord, item;
            var items = [];

            for (var i=0; i<rows.length; i++) {
                ord = i+1;
                item = rows[i].id;
                items[ord] = item;
            }

            $.ajax({
                type: 'POST',
                url: url,
                data: {
                    order_vendors: true,
                    items: items
                }
            });
        }
    });

    $(".table-discounts").tableDnD({
        onDrop: function(table, row){
            var rows = table.tBodies[0].rows;
            var ord, item;
            var items = [];

            for (var i=0; i<rows.length; i++) {
                ord = i+1;
                item = rows[i].id;
                items[ord] = item;
            }

            $.ajax({
                type: 'POST',
                url: url,
                data: {
                    order_discounts: true,
                    items: items
                }
            });
        }
    });

    $(".table-taxes").tableDnD({
        onDrop: function(table, row){
            var rows = table.tBodies[0].rows;
            var ord, item;
            var items = [];

            for (var i=0; i<rows.length; i++) {
                ord = i+1;
                item = rows[i].id;
                items[ord] = item;
            }

            $.ajax({
                type: 'POST',
                url: url,
                data: {
                    order_taxes: true,
                    items: items
                }
            });
        }
    });
}